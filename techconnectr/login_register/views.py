import datetime
from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.http import HttpResponseRedirect, HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, render,redirect
from django.template.loader import render_to_string
from django.template import loader
from django.utils.html import strip_tags
from .form import loginForm
from user.models import *
from setupdata.models import *
from client.utils import RegisterNotification, noti_via_mail
from client.models import *
from campaign.choices import *

from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from .tokens import account_activation_token, password_reset_token

from django.contrib.auth import login, authenticate, logout, update_session_auth_hash
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from .form import *
from django.contrib.auth.forms import PasswordChangeForm
from django.contrib.auth.decorators import login_required

# Abhi 
# this default page call when any user come on website
def index(request):
    if 'is_login' in request.session:
        if request.session['usertype']==1 and request.session['usertype']== 8:
            return redirect('/client/')
        elif request.session['usertype']==2:
            return redirect('/vendor/')
        elif request.session['usertype']==4:
            return redirect('/superadmin/')
    else:
        form=loginForm(request.POST or None)
        if form.is_valid():
            username=form.cleaned_data_get("username")
            password=form.cleaned_data_get("password")
        return render(request, 'login_register/login.html',{"form":form})
    return render(request, 'login_register/login.html',{})

# Abhi
# it's just display registe page.
def register(request):
    usertypes=usertype.objects.filter(is_active=1)
    '''#return render(request, 'dashboard/demo.html',{'data':usertypes})'''
    return render(request, 'login_register/register.html',{'data':usertypes})

# Abhi
# this is login check function set alot's of session from here like(usertype,logo so on more..).
# according usertype system automatically redirect on that page.
def login_check(request):
    username = request.POST.get("username")
    password = request.POST.get("password")
    request.session['success']=''
    if username is not None and password is not None:
        username = username.lower()
        countrow = user.objects.filter(email=username,password=password).count()
        if countrow == 1 :
            usertype=user.objects.get(email=username,password=password)
            if ( usertype.is_active == 1):
                if client_vendor.objects.filter(user_id=usertype.id).count() > 0:
                    profile=client_vendor.objects.get(user_id=usertype.id)
                    if profile.company_logo:
                        try:
                            request.session['profilepic']=str(profile.company_logo_smartthumbnail.url)
                        except Exception as e:
                            pass

                request.session['username'] = username
                request.session['company'] = usertype.user_name
                request.session['is_login'] = True
                request.session['usertype'] = usertype.usertype_id
                request.session['userid']=usertype.id
                request.session['onBording']=usertype.status
                usertype.is_login=1
                usertype.save()
                if check_onBoarding_status(usertype.status,usertype.usertype_id) :
                    if usertype.usertype_id == 1 :
                        return HttpResponseRedirect('/client/')
                    elif usertype.usertype_id == 2 :
                        return HttpResponseRedirect('/vendor/')
                    elif usertype.usertype_id == 5 or request.session['usertype']== 9:
                        return HttpResponseRedirect('/vendor-portal/dashboard/')
                    elif usertype.usertype_id == 4 :
                        return HttpResponseRedirect('/superadmin/')
                    elif usertype.usertype_id == 6 :
                        return HttpResponseRedirect('/users-portal/')
                else:
                    if usertype.usertype_id  == 1 :
                        return HttpResponseRedirect('/client/client-Bording/')
                    elif usertype.usertype_id  == 2 :
                        return HttpResponseRedirect('/vendor/vendor-Bording/')
            else:
                error="Verification Is Incomplete, Check Your Inbox To Proceed!"
                return render(request, 'login_register/login.html',{"error":error})
        else:
            error="Username Or Password Is incorrect!"
            return render(request, 'login_register/login.html',{"error":error})
    else:
        error="All Fields Are Required!"
        return render(request, 'login_register/login.html',{"error":error})

# Abhi
# using this function we  get know onBoarding status 
# === Check On-Boarding Status ===
def check_onBoarding_status(onBording,usertype_id):
    ids=[1,2]
    if usertype_id in ids:
        if onBording != 3 :
            return False
    return True

# Abhi 
# this function will help to create new user id and send mail to verify mail
# === User Registration ===
def user_register(request):
#Even after refreshing page able to select usertype while new registration.

    user_type = request.POST.get("usertype")
    email = request.POST.get("email")
    password = request.POST.get("password")
    confirmpassword=request.POST.get("confirmpassword")
    countrow = user.objects.filter(email=email).count()
    if email is not None and password is not None and confirmpassword is not None:
        t = password_check(password)
        email = request.POST.get("email").lower()
        already_registered = email_present(email)
        if already_registered != True:
            if t is True:
                if match_password(password,confirmpassword) :
                    if countrow == 1 :
                        user_type=usertype.objects.filter(is_active=1)
                        error="This mailId is already registered, Proceed to login!"
                        return render(request, 'login_register/register.html',{"data":user_type, "error":error})
                    else:
                        email_list = HeadersValidation.objects.all()
                        email_splited = email.split('@')
                        if email_splited[1] in eval(email_list[0].email_list):
                            error = "Only Bussiness Domains are allowed."
                            return render(request, 'login_register/register.html',{"data":user_type, "error":error})

                        request.session['success']="Registered Successfully, Check Your Inbox For Verification!"
                        site_url=settings.BASE_URL
                        try:
                            user.objects.create(status='1',is_active='0',email=email,password=password,user_name=email,usertype_id=user_type)
                        except Exception as e:
                            user_type=usertype.objects.filter(is_active=1)
                            error="All fields are required!"
                            return render(request, 'login_register/register.html',{"data":user_type, "error":error})
                        last_id=user.objects.latest('id')
                        title="New User Registered"
                        desc="New"
                        if(user_type=='1'):
                            desc=desc+" Client"
                        elif(user_type=='2'):
                            desc=desc+" Vendor"
                        desc=desc+" "+email+" Registered Successfully"
                        super = user.objects.filter(usertype_id=4)
                        noti = noti_via_mail([i.id for i in super], title, desc, mail_client_register if user_type == '1' else mail_vendor_register)
                        for i in super:
                            RegisterNotification(last_id.id,i.id,desc,title,2,None,None)
                        # RegisterNotification(super[0].id,last_id.id,'Welcome to Techconnectr','Welcome '+last_id.user_name,2,None,None)
                        site_url = settings.BASE_URL
                        last_id='login/varifyemail/' +str(last_id.id)
                        subject="Thank you for connecting with Techconnectr"
                        html_message = render_to_string('email_templates/welcome_email.html', {'username':email,'password':password,'site_url':site_url,'last_id':last_id})
                        plain_message = strip_tags(html_message)
                        from_email = settings.EMAIL_HOST_USER
                        to_list = [email]
                        send_mail(subject, plain_message, from_email, to_list, fail_silently=True,html_message=html_message)
                        return render(request, 'login_register/login.html',{})
                else:
                    user_type=usertype.objects.filter(is_active=1)
                    error = "Passwords Do Not Match, Please Try Again!"
                    return render(request, 'login_register/register.html',{"data":user_type, "error":error})
            else:
                user_type = usertype.objects.filter(is_active=1)

                error = 'Password should be greater than 8 characters with a combination of upper and lowercase characters, special symbols and numbers only!'

                return render(request, 'login_register/register.html',{"data":user_type, "error":error})
        else:
            user_type = usertype.objects.filter(is_active=1)
            error = 'Sorry, This organisation is already registered with Techconnectr, Proceed to login.'
            return render(request, 'login_register/register.html',{"data":user_type, "error":error})
    else:
        user_type = usertype.objects.filter(is_active=1)
        error = 'Please enter valid data, fields cannot be empty!'
        return render(request, 'login_register/register.html',{"data":user_type, "error":error})

# Abhi
# this function used for check password .
# === Password Check ===
def password_check(passwd):
    SpecialSym =['$', '@', '#', '%', '^', '&', '*', '+', '-', '!']
    if len(passwd) < 8:
        return False

    # changes to not allow spaces in password - amey raje - 25 June
    if any(char.isspace() for char in passwd) is True:
        return False
    # changes end

    if not any(char.isdigit() for char in passwd):
        return False

    if not any(char.isupper() for char in passwd):
        return False

    if not any(char.islower() for char in passwd):
        return False

    if not any(char in SpecialSym for char in passwd):
        return False

    else:
        return True

# === Match Password ===
def match_password(pwd,cpwd):
    if pwd == cpwd:
        return True
    else:
        return False

# === Varifyemail ===
def varifyemail(request,user_id):
     request.session['success']=''
     t = user.objects.get(id=user_id)
     t.is_active = 1
     t.save()
     return HttpResponseRedirect('/login/')


# === Forgot Password ===
def forgot_password(request):
    if 'success' in request.session:
        del request.session['success']
    return render(request, 'login_register/forgot_password.html',{})

# === Reset Password ===
def reset_password(request):
    # check user exists and send reset password mail
    email = request.POST.get('username').lower()
    site_url = settings.BASE_URL
    if user.objects.filter(email=email).exists():
        user_details = user.objects.get(email=email)
        subject = "Request For Password Change"
        html_message = render_to_string('email_templates/reset_password_template.html', {
                                        'client':user_details.user_name,'site_url': f'{site_url}change_password/{user_details.id}/',})
        plain_message = strip_tags(html_message)
        from_email = settings.EMAIL_HOST_USER
        to_list = [email]
        if send_mail(subject, plain_message, from_email, to_list, fail_silently=True,html_message=html_message):
            request.session['success'] ="Request for reset password sent. Please check your email!"
            return render(request, 'login_register/login.html',{})
        else:
            return render(request, 'login_register/login.html',{})
    else:
        error="This email is not registered with Techconnectr!"
        return render(request, 'login_register/forgot_password.html',{"error":error})

# === Change Password ===
def change_password(request,userid):
    # return change password page
    request.session['success'] =""
    user_details= user.objects.get(id=userid)
    return render(request, 'login_register/reset_password.html',{"user":user_details})

# === Submit Reset Password ===
def submit_reset_password(request):
    # reset user password 
    password = request.POST.get("password")
    user_type = request.POST.get("usertype")
    userid = request.POST.get("userid")
    confirmpassword=request.POST.get("confirmpassword")
    current_user = user.objects.get(id=userid)
    if password is not None and confirmpassword is not None:
        if password_check(password):
            if match_password(password,confirmpassword):
                current_user.password = password
                current_user.save()
                return login_check(request)
            else:
                error = "Passwords do not match with each other!"
                return render(request, 'login_register/reset_password.html',{"user":current_user,"error":error})
        else:
            error = 'Password should be greater than 8 characters and a combination of upper and lowercase characters, special symbols and numbers!'
            return render(request, 'login_register/reset_password.html',{"user":current_user,"error":error})
    else:
        error = 'Please enter valid data, fields cannot be empty!'
        return render(request, 'login_register/reset_password.html',{"user":current_user,"error":error})

# === Email Present ===
def email_present(email):

    if type(email) == str:
        pass
    else:
        email = str(email)
    splited = email.split('@')
    query = user.objects.all()
    list = []
    for q in query:
        l = q.email.split('@')
        list.append(l[1])
    for i in list:
        if i == splited[1]:
            return True
    return False

# === Handler404 ===
def handler404(request):
    return render(request, 'errors/404.html', status=404)

# === Handler500 ===
def handler500(request):
    return render(request, 'errors/500.html', status=500)


# === New Registration ===
def new_register(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email'].lower()
            user_type = form.cleaned_data['user_type']

            email_list = HeadersValidation.objects.all()
            email_splited = email.split('@')
            if email_splited[-1] in eval(email_list[0].email_list):
                messages.error(request, 'Email addresses with bussiness domains are allowed only')
                return HttpResponseRedirect('/register/')

            if email_present(email):
                messages.error(request, 'This organisation is already registered with Techconnectr')
                return HttpResponseRedirect('/register/')

            user_obj = form.save(commit=False)
            user_obj.usertype = user_type
            user_obj.is_active = False
            user_obj.save()
            
            if user_type.id == '1':
                title = "New Client Registered"
                desc = "New client "+ email +" registered on Techconnectr"
            else:
                title = "New Vendor Registered"
                desc = "New vendor "+ email +" registered on Techconnectr"

            super = user.objects.filter(usertype_id=4)
            noti = noti_via_mail([i.id for i in super], title, desc, mail_client_register if user_type.id == '1' else mail_vendor_register)
            for i in super:
                RegisterNotification(user_obj.id,i.id,desc,title,2,None,None)

            current_site = get_current_site(request)
            subject = 'Activate Your Techconnectr Account'
            uid = urlsafe_base64_encode(force_bytes(user_obj.pk))
            message = render_to_string('email_templates/welcome_email.html', {
                'user': user_obj,
                'domain': current_site.domain,
                'uid': uid.decode('ascii'),
                'token': account_activation_token.make_token(user_obj),
            })
            plain_message = strip_tags(message)
            from_email = settings.EMAIL_HOST_USER
            to_list = [user_obj.email]
            send_mail(subject, plain_message, from_email, to_list, fail_silently=True,html_message=message)
            messages.success(request, 'Great, your account has been registered. Verification link has been sent to your email-id, Please check your mail for further process')
            return HttpResponseRedirect('/')
        else:
            return render(request, 'login_register/new_register.html', {'form': form})
    else:
        form = SignUpForm()
    return render(request, 'login_register/new_register.html', {'form': form})

# === Activate ===
def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user_obj = user.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, user.DoesNotExist) as e:
        user_obj = None
        print(e)

    if user_obj is not None:
        if account_activation_token.check_token(user_obj, token):
            user_obj.is_active = True
            user_obj.save()
            messages.success(request, 'Your email has been verified and now you can login')
        else:
            messages.error(request, 'Link is expired, looks like your account is already verified')
    else:
        messages.error(request, 'Sorry, there is something wrong with your account please contact admin for help')
    return HttpResponseRedirect('/')


# === New Login ===
def new_login(request):
    if request.user.is_authenticated:
        if check_onBoarding_status(request.user.status, request.user.usertype_id) :
            if request.user.usertype_id == 1 or request.session['usertype']== 8:
                return HttpResponseRedirect('/client/')
            elif request.user.usertype_id == 2 :
                return HttpResponseRedirect('/vendor/')
            elif request.user.usertype_id == 5 or request.session['usertype']== 9:
                return HttpResponseRedirect('/vendor-portal/dashboard/')
            elif request.user.usertype_id == 4 :
                return HttpResponseRedirect('/superadmin/')
            elif request.user.usertype_id == 6 :
                return HttpResponseRedirect('/users-portal/')
            elif request.user.usertype_id == 7 :
                messages.error(request, "Sorry, that didn't work. Please try again")
                return HttpResponseRedirect('/logout/')
        else:
            if request.user.usertype_id  == 1 or request.session['usertype']== 8:
                return HttpResponseRedirect('/client/client-Bording/')
            elif request.user.usertype_id  == 2 :
                return HttpResponseRedirect('/vendor/vendor-Bording/')

    else:
        if request.method == "POST":
            form = SignInForm(request.POST)
            if form.is_valid():
                email = form.cleaned_data['email'].lower()
                password = form.cleaned_data['password']
                user = authenticate(email=email, password=password)
                if user is not None:
                    if user.is_active is True:
                        user.is_login = 1
                        user.save()
                        login(request, user)
                        if client_vendor.objects.filter(user_id=user.id).count() > 0:
                            profile=client_vendor.objects.get(user_id=user.id)
                            from client.utils import fetch_default_logo
                            request.session['profilepic'] = fetch_default_logo(user.id)
                            try:
                                if profile.company_logo and  profile.company_logo.storage.exists(profile.company_logo.name):
                                    request.session['profilepic']=str(profile.company_logo_smartthumbnail.url)
                            except Exception as e:
                                pass
                        request.session['username'] = user.email
                        request.session['company'] = user.user_name
                        request.session['is_login'] = 1
                        request.session['usertype'] = user.usertype_id
                        request.session['userid']=user.id
                        request.session['onBording']=user.status

                        if check_onBoarding_status(user.status, user.usertype_id) :
                            if user.usertype_id == 1 or user.usertype_id == 8:
                                return HttpResponseRedirect('/client/')
                            elif user.usertype_id == 2 :
                                return HttpResponseRedirect('/vendor/')
                            elif user.usertype_id == 5 or request.session['usertype']== 9:
                                return HttpResponseRedirect('/vendor-portal/dashboard/')
                            elif user.usertype_id == 4 :
                                return HttpResponseRedirect('/superadmin/')
                            elif user.usertype_id == 6 :
                                return HttpResponseRedirect('/users-portal/')
                            elif user.usertype_id == 7 :
                                messages.error(request, "Sorry, that didn't work. Please try again")
                                return HttpResponseRedirect('/logout/')
                        else:
                            if user.usertype_id  == 1 or user.usertype_id == 8:
                                return HttpResponseRedirect('/client/client-Bording/')
                            elif user.usertype_id  == 2 :
                                return HttpResponseRedirect('/vendor/vendor-Bording/')
                    messages.error(request, 'Sorry, your email verification is incomplete. Please verify your email first')
                else:
                    messages.error(request, "Sorry, that didn't work. Please try again")
        else:
            form = SignInForm()
        return render(request, 'login_register/new_login.html', {'form': form})

# === New Logout ===
def new_logout(request):
    if request.session:
        if 'userid' in request.session:
            login=user.objects.get(id=request.session['userid'])
            login.is_login=0
            login.save()
    logout(request)
    return redirect('/')

# === Password Change ===
def password_change(request):
    if request.method == 'POST':
        form = PasswordChangeForm(data=request.POST, user=request.user)
        if form.is_valid() and password_check(request.POST.get('new_password1')):
            form.save()
            update_session_auth_hash(request, form.user)
            data = {'success': 'Your password has been changed successfully'}
        else:
            data = {'errors': form.errors}
    else:
        data = {}
    return JsonResponse(data)

# === Password Reset ===
def password_reset(request):

    if request.method == 'POST':
        form = passwordResetForm(request.POST)
        if form.is_valid():
            email = form.cleaned_data['email']
            try:
                user_obj = user.objects.get(email=email)
            except user.DoesNotExist as e:
                user_obj = None

            if user_obj is not None:
                current_site = get_current_site(request)
                subject = 'Reset Password For Your Techconnectr Account'
                uid = urlsafe_base64_encode(force_bytes(user_obj.pk))
                message = render_to_string('email_templates/reset_password_template.html', {
                    'user': user_obj,
                    'uid': uid.decode('ascii'),
                    'token': password_reset_token.make_token(user_obj),
                    'domain': current_site.domain,
                })
                plain_message = strip_tags(message)
                from_email = settings.EMAIL_HOST_USER
                to_list = [user_obj.email]
                send_mail(subject, plain_message, from_email, to_list, fail_silently=True,html_message=message)
                messages.success(request, 'Verification link has been sent to your registered email address for password reset request')
            else:
                messages.error(request, 'This email-id is not associated with Techconnectr Marketplace')
    else:
        form=passwordResetForm()
    return render(request, 'login_register/password_reset.html', {"form": form})

# === Password Reset Confirm ===
def password_reset_confirm(request, uidb64, token):
    if request.method == 'GET':
        try:
            uid = force_text(urlsafe_base64_decode(uidb64))
            user_obj = user.objects.get(pk=uid)
        except (TypeError, ValueError, OverflowError, user.DoesNotExist) as e:
            user_obj = None
            print(e)

        if user_obj is not None and password_reset_token.check_token(user_obj, token):
            form = NewPasswordForm()
            return render(request, 'login_register/password_reset_confirm.html', {'form': form, 'user': user_obj.id})
        messages.error(request, 'This link is no longer valid, Please try again')
        return HttpResponseRedirect('/password_reset/')
    elif request.method == 'POST':
        user_new = request.POST.get('user')
        form = NewPasswordForm(request.POST)
        if form.is_valid():
            password = form.cleaned_data['new_password2']
            user_obj1 = user.objects.get(id=user_new)
            user_obj1.set_password(password)
            user_obj1.save()
            messages.success(request, 'New password saved successfully, Please login with your new password to continue')
            return HttpResponseRedirect('/')
        else:
            return render(request, 'login_register/password_reset_confirm.html', {'form': form, 'user': user_new})
        form = NewPasswordForm()
        return render(request, 'login_register/password_reset_confirm.html', {'form': form, 'user': user_new})

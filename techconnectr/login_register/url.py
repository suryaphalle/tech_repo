from django.urls import path
from django.conf.urls import url
from . import views
# from django.contrib.auth import views as auth_views
from django.conf.urls import url
from django.contrib.auth.views import (
    PasswordResetView,
    PasswordResetDoneView,
    PasswordResetConfirmView,
    PasswordResetCompleteView
)

app_name="login_register"
urlpatterns=[
    # path('',views.index , name='index'),
    # path('login/',views.index , name='index'),
    # path('logout/',views.logout , name='logout'),
    # path('login/varifyemail/<int:user_id>',views.varifyemail , name='varifyemail'),
    # path('register/',views.register , name='register'),
    # path('forgot_password/',views.forgot_password , name='forgot_password'),
    # path('reset_password/',views.reset_password , name='reset_password'),
    # path('change_password/<int:userid>/',views.change_password , name='change_password'),
    # path('submit_reset_password/',views.submit_reset_password , name='submit_reset_password'),
    # path('user_register/',views.user_register , name='user_register'),
    # path('login_check/',views.login_check , name='login_check'),
    path('', views.new_login, name='signin'),
    path('login/', views.new_login, name='signin'),
    path('register/', views.new_register, name='new_register'),
    path('logout/', views.new_logout, name='logout'),
    path('password_change/', views.password_change, name='password_change'),
    path('password_reset/', views.password_reset, name='password_reset'),
    url(r'^password_reset/confirm/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$', views.password_reset_confirm, name='password_reset_confirm'),
    # path('password_reset/new', views.create_new_passord, name='new_password'),
]

handler404 = views.handler404
handler500 = views.handler500

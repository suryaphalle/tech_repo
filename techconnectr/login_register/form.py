from django import forms
from django.contrib.auth.forms import UserCreationForm
from user.models import *
from django.conf import settings
from django.contrib.auth.password_validation import validate_password
from superadmin.models import *
from django.core.validators import RegexValidator

class SignInForm(forms.Form):
    email=forms.EmailField(required=True);
    password=forms.CharField(required=True, widget=forms.PasswordInput, min_length=8, strip=True)
    class Meta:
        model = user
        fields = ('email',)

class loginForm(forms.Form):
    email=forms.CharField(required=True);
    password=forms.CharField(required=True, widget=forms.PasswordInput)
    class Meta:
        model = user
        fields = ('email',)

class SignUpForm(UserCreationForm):
    email = forms.EmailField(required=True)
    user_type = forms.ModelChoiceField(required=True, queryset=usertype.objects.filter(id__in=[1, 2]))
    class Meta:
        model = user
        fields = ('email', 'password1', 'password2')
    def clean_email(self):
        """
        ensure that email is always lower case.
        """
        return self.cleaned_data['email'].lower()

class NewPasswordForm(forms.Form):
    """
    A form that lets a user change set their password without entering the old
    password
    """
    error_messages = {
        'password_mismatch': ("The two password fields didn't match."),
        }
    new_password1 = forms.CharField(label=("New password"), widget=forms.PasswordInput, min_length=8, strip=True)
    new_password2 = forms.CharField(label=("Password confirmation"), widget=forms.PasswordInput, min_length=8, strip=True)

    def clean_new_password2(self):
        password1 = self.cleaned_data.get('new_password1')
        password2 = self.cleaned_data.get('new_password2')
        if password1 and password2:
            if password1 != password2:
                raise forms.ValidationError(
                    self.error_messages['password_mismatch'],
                    code='password_mismatch',
                    )
        try:
            validate_password(password2)
        except forms.ValidationError as error:
            self.add_error('new_password2', error)
        return password2

    class Meta:
        model = user


class passwordResetForm(forms.Form):
    email = forms.EmailField(required=True)


class ExternalVendorSignUpForm(UserCreationForm):
    alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')

    user_name = forms.CharField(required=True, validators=[alphanumeric], max_length=50)
    email = forms.EmailField(required=True)

    class Meta:
        model = user
        fields = ('email', 'user_name', 'password1', 'password2')

    def clean_email(self):
        email = self.cleaned_data['email']
        return email

class ExternalUserSignUpForm(UserCreationForm):
    alphanumeric = RegexValidator(r'^[0-9a-zA-Z]*$', 'Only alphanumeric characters are allowed.')

    email = forms.EmailField(required=True)
    user_name = forms.CharField(required=True, validators=[alphanumeric], max_length=50)

    class Meta:
        model = user
        fields = ('email', 'user_name', 'password1', 'password2')

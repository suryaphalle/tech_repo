from django.db import models
from campaign.models import *

# Create your models here.

# store html to replace when it dragged in another grid
class Element_html(models.Model):
    type = models.CharField(max_length=150) # tage type ex. text
    name = models.CharField(max_length=250, blank="TRUE") # show element name in span
    html = models.TextField(blank="TRUE") # html to replace
    selectors = models.TextField(blank="TRUE") # list of all modifiable selectors
    attributes = models.TextField(blank="TRUE") # attributes to modify
    edit_html = models.TextField(blank="TRUE") # html to render when user click on edit (in modal)

    def __str__(self):              # __unicode__ on Python 2
        return self.name

# store common html needed for all items
class Item_common_html(models.Model):
    type = models.CharField(max_length=50, blank="TRUE") # tage type ex. text
    name = models.CharField(max_length=50, blank="TRUE") # show element name in span
    selector_id = models.CharField(max_length=50, blank="TRUE") # id of the hidden text
    html = models.TextField(blank="TRUE") # html to replace
    attributes = models.TextField(blank="TRUE") # attributes to modify
    
    def __str__(self):              # __unicode__ on Python 2
        return self.name
    

class Question_forms(models.Model):
    campaign = models.ForeignKey(Campaign,on_delete=models.CASCADE)
    questions = models.TextField(blank=True)
    question_html = models.TextField(blank=True)
    question_numbers = models.IntegerField(null=True,blank=True)

    def __str__(self):
        return self.campaign.name


    
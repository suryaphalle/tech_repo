from django.contrib import admin


from form_builder.models import Element_html, Item_common_html, Question_forms


# Register your models here.

class ElementAdmin(admin.ModelAdmin):
    list_display = ('type', 'name', 'html', 'selectors',
                    'attributes', 'edit_html')  # specific fields


admin.site.register(Element_html, ElementAdmin)


class ItemCommonHtmlAdmin(admin.ModelAdmin):
    list_display = ('type', 'name', 'selector_id', 'html',
                    'attributes')  # specific fields

admin.site.register(Item_common_html, ItemCommonHtmlAdmin)

class Questionforms(admin.ModelAdmin):
    list_display = ('campaign', 'questions', 'question_numbers')


admin.site.register(Question_forms, Questionforms)
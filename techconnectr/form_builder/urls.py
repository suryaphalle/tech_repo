from django.urls import path

"""
   from current folder import all views
"""
from . import views

##
# namespacing URL names
# so we can use href="{% url 'dashboard:basic'%}"
##
app_name = 'form'

urlpatterns = [
    # path('builder', views.index, name='form_builder'),
    path('store_question/', views.store_question, name='store_question'),
    path('view_custome_question_pdf/<int:camp_id>', views.make_custome_question_pdf, name='make_custome_question_pdf'),
    # path('builder_example', views.example, name='form_builder_example'),
]
from io import BytesIO

from django.http import HttpResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.template.loader import get_template
from django.utils import translation
from django.utils.translation import activate
from django.utils.translation import gettext as _
from django.views.decorators.csrf import csrf_exempt

import xhtml2pdf.pisa as pisa
from campaign.models import *
from client.views.views import *

from .models import *

# Create your views here.


# def index(request):
#     elements = Element_html.objects.all()  # get all records
#     item_common_html = Item_common_html.objects.all()  # get all records
#     context = {
#         'item_common_html': item_common_html,
#         'elements': elements
#     }
#     return render(request, 'form_builder/index.html', context)

# === Custome Question ===
def custom_question(request,camp_id, Q_count = 0):
    questions = Mapping.objects.get(campaign_id=camp_id)
    # save total custom questions count..Akshay G.
    if Q_count != 0:
        questions.custom_question = Q_count
    questions.save()
    elements = Element_html.objects.all()  # get all records
    item_common_html = Item_common_html.objects.all()  # get all records
    campaign = Campaign.objects.get(id=camp_id)
    questions = Mapping.objects.get(campaign_id=camp_id)
    campaign_que = Question_forms.objects.filter(campaign=camp_id)
    if campaign_que:
        context = {

            'item_common_html': item_common_html,
            'elements': elements,
            'custom_question':questions.custom_question,
            'campaign':campaign_que[0].campaign,
            'questions':campaign_que[0].questions
        }
    else:
        context = {

            'item_common_html': item_common_html,
            'elements': elements,
            'custom_question':questions.custom_question,
            'campaign':campaign,
        }

    return render(request, 'form_builder/set_question_page.html', context)

##
# # draggable demo
# #
# def example(request):
#     return render(request, 'form_builder/example.html')

# === Store Custome Questuion ===
def store_question(request):
    # storing custom questions
    camp_id = Campaign.objects.get(id=request.POST.get('camp_id'))
    if Question_forms.objects.filter(campaign=camp_id):
        campaign = Question_forms.objects.get(campaign=camp_id)
        campaign.question_html = request.POST.get('html')
        campaign.questions = request.POST.get('question_form')
        campaign.save()
    else:
        campaign = Question_forms.objects.create(campaign=camp_id,questions = request.POST.get('question_form'),
            question_html = request.POST.get('html'),question_numbers = request.POST.get('question_numbers'))
        campaign.save()
    mapping = Mapping.objects.get(campaign_id=camp_id.id)
    mapping.custom_question = request.POST.get('question_numbers')
    mapping.custom_status = 1
    mapping.save()
    if camp_id.rfq == 1:
        data = {
            'success': 1,
            'url': "/client/pending-campaign/"
        }
    else:
        # for vendor suggestion page -Amey 25 June 2019
        if camp_id.tc_vendor == 1:
            # if camp_id.tc_raimainingleads > 0:
            data = {
            'success': 1,
            'url': "/client/campaign-vendor-list/"+str(camp_id.id)
            }
            return JsonResponse(data)
        data = {
        'success': 1,
        'url': "/client/pending-campaign/"
        }
    return JsonResponse(data)

# === Make Custome Question PDF ===
def make_custome_question_pdf(request,camp_id):
    # make custom question pdf
    camp_id =  Question_forms.objects.get(campaign_id=camp_id)
    template = get_template('form_builder/pdf.html')
    html = template.render({'campaign': camp_id.campaign.name, 'html': ast.literal_eval(camp_id.questions)})
    response = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
    print(html)
    if not pdf.err:
        return HttpResponse(response.getvalue(), content_type='application/pdf')
    else:
        return HttpResponse("Error Rendering PDF", status=400)
    # return render(request, 'form_builder/pdf.html', {'campaign': camp_id.campaign.name, 'html': ast.literal_eval(camp_id.questions)})

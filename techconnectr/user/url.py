from django.urls import path
from . import views
from user.views.views import *

urlpatterns = [
    path('chat', views.index, name='chat'),
    path('chat/<str:room_name>/', views.room, name='room'),
    
]
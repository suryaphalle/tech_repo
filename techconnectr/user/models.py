from datetime import datetime
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
# Create your models here.

class MyUserManager(BaseUserManager):

    def create_user(self, email, password):
        if not email:
            raise ValueError('User must have email address')

        if not password:
            raise ValueError('User must have a password')

        user = self.model(
            email = self.normalize_email(email),
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        user = self.create_user(
            email,
            password=password
        )
        user.is_active = True
        user.is_admin = True
        user.save(using=self._db)
        return user


class usertype(models.Model):
	type=models.CharField(max_length=30)
	is_active=models.BooleanField()
	created_date=models.DateTimeField(auto_now_add=True)
	updated_date=models.DateTimeField(auto_now_add=True)
	is_superadmin = models.BooleanField(default=False)
	is_client = models.BooleanField(default=False)

	def __str__(self):
		return self.type


class user(AbstractBaseUser):

	email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
	user_name=models.CharField(max_length=50,blank=True, null=True)
	contact=models.CharField(max_length=20, blank=True, null=True)
	alternative_contact = models.CharField(max_length=20, blank=True, null= True)
	address_line1=models.TextField(blank=True, null=True)
	address_line2=models.TextField(blank=True, null=True)
	country=models.IntegerField( blank=True, null=True)
	state=models.IntegerField( blank=True, null=True)
	city=models.IntegerField( blank=True, null=True)
	zip_code = models.CharField(max_length=20, blank=True, null=True)
	usertype=models.ForeignKey(usertype,on_delete=models.CASCADE, null=True, blank=True)
	created_date=models.DateTimeField(auto_now_add=True)
	updated_date=models.DateTimeField(auto_now_add=True)
	status=models.IntegerField(blank=True, null=True)
	approve=models.IntegerField(default=0)
	anonymous_status = models.BooleanField(default=False)
	is_login=models.IntegerField(default=0)
	is_admin=models.BooleanField(default=False)
	is_active=models.BooleanField(default=True)
	is_blocked=models.BooleanField(default=False)
	favorite_vendor=models.ManyToManyField('self', blank=True)
	child_user=models.ManyToManyField('self', blank=True)


	objects = MyUserManager()

	USERNAME_FIELD = 'email'
	REQUIRED_FIELDS = []

	def __str__(self):
	    return self.email

	def has_perm(self, perm, obj=None):
	    "Does the user have a specific permission?"
	    # Simplest possible answer: Yes, always
	    return True

	def has_module_perms(self, app_label):
	    "Does the user have permissions to view the app `app_label`?"
	    # Simplest possible answer: Yes, always
	    return True

	@property
	def is_staff(self):
	    "Is the user a member of staff?"
	    # Simplest possible answer: All admins are staff
	    return self.is_admin


#track user activities
class user_activities(models.Model):
	user=models.ForeignKey(user,on_delete=models.CASCADE)
	date=models.DateField()
	active_page=models.CharField(max_length=50,blank=True, null=True)
	arg=models.IntegerField(blank=True, null=True)
	activity_logs=models.TextField(blank=True,null=True)
	def __str__(self):
		return self.user.email


class UserTracking(models.Model):
	user=models.ForeignKey(user, on_delete=models.CASCADE)
	date=models.CharField(max_length=50)
	data=models.TextField(blank=True, null=True)

	def __str__(self):
		return f'{self.user.email} - {self.date}'

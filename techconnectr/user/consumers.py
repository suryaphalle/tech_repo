from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
from user.models import *
from setupdata.models import *
import json
import datetime


class ChatConsumer(WebsocketConsumer):

    # abhi 21-08-2019
    # handshak  with websocket
    def connect(self):
        self.room_name = self.scope["url_route"]["kwargs"]["room_name"]
        self.room_group_name = "chat_%s" % self.room_name
        async_to_sync(self.channel_layer.group_add)(
            self.room_group_name, self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.room_group_name, self.channel_name
        )

    # abhi 21-08-2019
    # fetch all data from database according to users
    def fetch_messages(self, data):
        ids = []
        ids.append(int(data["user1"]))
        ids.append(int(data["user2"]))
        if (
            conversation.objects.filter(
                user_id1_id__in=ids, user_id2_id__in=ids
            ).count()
            > 0
        ):
            user_chat_msg = conversation.objects.filter(
                user_id1_id__in=ids, user_id2_id__in=ids
            )[0]
            content = {"message": eval(user_chat_msg.data), "cammands": "message"}
        else:
            content = {"message": [], "cammands": "message"}
        self.send_message(content)

    # abhi 21-08-2019
    # store new messages in databases
    def new_messages(self, data):
        ids, prev_data, message_data = [], [], ""
        ids.append(int(data["user1"]))
        ids.append(int(data["user2"]))
        if (
            conversation.objects.filter(
                user_id1_id__in=ids, user_id2_id__in=ids
            ).count()
            > 0
        ):
            message = conversation.objects.filter(
                user_id1_id__in=ids, user_id2_id__in=ids
            )[0]
            message_data = self.create_msg(data, eval(message.data))
            message.data = message_data
            message.save()
        else:
            message = conversation.objects.create(
                user_id1_id=int(data["user1"]),
                user_id2_id=int(data["user2"]),
                data=self.create_msg(data, prev_data),
            )
            message_data = self.create_msg(data, prev_data)
        self.send_chat_messages(message_data[-1])

    # abhi 21-08-2019
    # create dict for new msg
    def create_msg(self, data, prev_data):
        msg = data["message"]
        time = datetime.datetime.now().strftime("%H:%M:%S")
        date = datetime.datetime.today().date()
        prev_data.append(
            {
                "sender_name": self.scope["session"]["username"],
                "profilepic": self.scope["session"]["profilepic"],
                "sender_id": self.scope["session"]["userid"],
                "message": msg,
                "time": time,
                "date": str(date),
                "media": "",
                "title": "",
                "receiver_ids": int(data["user2"]),
                "rank": 1 if len(prev_data) == 0 else int(prev_data[-1]["rank"]) + 1,
            }
        )
        return prev_data

    commands = {"fetch_messages": fetch_messages, "new_messages": new_messages}

    # abhi 21-08-2019
    # recieve request from web
    def receive(self, text_data):
        data = json.loads(text_data)
        # dyanamic way to call function
        self.commands[data["cammands"]](self, data)

    # abhi 21-08-2019
    # send response to web
    def chat_message(self, event):
        message = event["message"]
        self.send(text_data=json.dumps(message))

    def send_chat_messages(self, message):
        async_to_sync(self.channel_layer.group_send)(
            self.room_group_name, {"type": "chat_message", "message": message}
        )

    def send_message(self, message):
        self.send(text_data=json.dumps(message))


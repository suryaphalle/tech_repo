# Generated by Django 2.1.4 on 2019-07-25 12:27

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0004_auto_20190725_1226'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='parent',
        ),
        migrations.AddField(
            model_name='user',
            name='child',
            field=models.ManyToManyField(blank=True, related_name='_user_child_+', to=settings.AUTH_USER_MODEL),
        ),
    ]

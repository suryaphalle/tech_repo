from django.shortcuts import render
from django.shortcuts import get_object_or_404, render, redirect
from django.conf import settings
from django.urls import resolve
from django.http import HttpResponse, JsonResponse
from django.utils.safestring import mark_safe
from user.models import user
from client_vendor.models import *
import json



def index(request):
    return render(request, "chats/index.html", {})

# Abhi  19 Aug 2019
# when user start communication that time user need crate  room .
# and other person also chat with same room
# this function pass sender details like user name ,logo
def room(request, room_name):
    userlist = user.objects.filter(usertype_id__in=[1, 2])
    userlist = client_vendor.objects.filter(user_id__in=userlist)
    superadmin_list = user.objects.filter(usertype_id=4)

    return render(
        request,
        "chats/room.html",
        {
            "room_name_json": mark_safe(json.dumps(room_name)),
            "user_id": mark_safe(json.dumps(request.session["userid"])),
            "username": mark_safe(json.dumps(request.session["username"])),
            "profilepic": mark_safe(json.dumps(request.session["profilepic"])),
            "usertype": request.session["usertype"],
            "userlist": userlist,
            "superadmin_list": superadmin_list,
        },
    )



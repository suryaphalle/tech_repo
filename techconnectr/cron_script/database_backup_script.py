import urllib.request
import ssl

# This restores the same behavior as before.
# context = ssl._create_unverified_context()
# urllib.request.urlopen("https://marketplace.techconnectr.com/vendor/dumpdata/", context=context)
pg_dump -U 'trigen' 'techconnectr' -h '127.0.0.1' > exported_data.sql
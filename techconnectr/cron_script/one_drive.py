#!/bin/sh
import onedrivesdk
import datetime
from onedrivesdk.helpers import GetAuthCodeServer

redirect_uri = 'http://localhost:8080/'
client_secret = '3vGdneQ*sy38z4.X-@3FFL7xsN_PD7@Z'
client_app_id = '2370c48a-e865-4e17-8181-d0b1307dea5c'
scopes=['wl.signin', 'wl.offline_access', 'onedrive.readwrite']

client = onedrivesdk.get_default_client(client_id=client_app_id, scopes=scopes)

auth_url = client.auth_provider.get_auth_url(redirect_uri)

#code = GetAuthCodeServer.get_auth_code(auth_url, redirect_uri)
# client.auth_provider.save_session()

client.auth_provider.load_session()
client.auth_provider.refresh_token()

# Techconnectr folder id 
tech_folder_id = 'E0921872B22BD3A2!112'

date = datetime.datetime.now()
currant_date = date.strftime("%d_%B_%Y")
database_filename = 'Marketplace_db'+str(currant_date)+'.json'
path='../json_dump/database/'+database_filename

filename= path.split('/')[-1]
ctime = str(datetime.datetime.now().time().strftime("%H:%M:%S")).replace(':','_')
filename = f"{filename.split('.')[0]}{ctime}.{filename.split('.')[1]}"
print(filename)
returned_item = client.item(drive='me', id=tech_folder_id).children[filename].upload_async(path)
if returned_item.to_dict()['id']:
    print('file uploaded successfully.')

print('End script.')
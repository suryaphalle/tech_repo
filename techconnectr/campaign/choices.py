from setupdata.models import *

CAMPAIGN_TYPES_CHOICES = (
    ('', "Select Campaign Type"),
    ('1', "Varient Data"),
    ('2', "Lead Gen"),
    ('3', "Appointment"),
    ('4', "Survey"),
    ('5', "Waterfall"),
)

"""
CAMPAIGN_METHOD_CHOICES = (
    ('1', "Email"),
    ('2', "Tele"),
    ('3', "Hybrid"),
    ('4', "Social"),
    ('5', "Digital"),
    ('6', "Others   "),

)
"""

# PRIORITY_CHOICES = CAMPAIGN_METHOD_CHOICES =RFQ_CAMPAIGN_REVENUE_CHOICES = RFQ_CAMPAIGN_COMPANY_CHOICES = RFQ_CAMPAIGN_INDUSTRY_CHOICES = RFQ_CAMPAIGN_METHOD_CHOICES =(
#     ('1', "High"),
#     ('2', "Medium"),
#     ('3', "Low"),
# )

tup_int_id = tuple(source_touches.objects.all().values_list('id','type'))
tup_str_id = list()
for tup in tup_int_id:
    tup_list = list(tup)
    tup_list[0] = str(tup_list[0])
    tup_str_id.append(tup_list)

CAMPAIGN_METHOD_CHOICES = tuple(tup_str_id)

tup_int_id1 = tuple(source_touches.objects.all().values_list('type','type'))
tup_str_id1 = list()
for tup in tup_int_id1:
    tup_list = list(tup)
    tup_list[0] = str(tup_list[0])
    tup_str_id1.append(tup_list)

RFQ_CAMPAIGN_METHOD_CHOICES = tuple(tup_str_id1)

tup_int_id2 = tuple(industry_type.objects.filter(status=0, is_active=True).values_list('type', 'type'))
tup_str_id2 = list()
for tup in tup_int_id2:
    tup_list = list(tup)
    tup_list[0] = str(tup_list[0])
    tup_str_id2.append(tup_list)

RFQ_CAMPAIGN_INDUSTRY_CHOICES = tuple(tup_str_id2)

tup_int_id3 = tuple(company_size.objects.filter(is_active=True).values_list('range', 'range'))
tup_str_id3 = list()
for tup in tup_int_id3:
    tup_list = list(tup)
    tup_list[0] = str(tup_list[0])
    tup_str_id3.append(tup_list)

RFQ_CAMPAIGN_COMPANY_CHOICES = tuple(tup_str_id3)


tup_int_id4 = tuple(RevenueSize.objects.filter(is_active=True).values_list('range', 'range'))
tup_str_id4 = list()
for tup in tup_int_id4:
    tup_list = list(tup)
    tup_list[0] = str(tup_list[0])
    tup_str_id4.append(tup_list)

RFQ_CAMPAIGN_REVENUE_CHOICES = tuple(tup_str_id4)


PRIORITY_CHOICES = (
    ('1', "High"),
    ('2', "Medium"),
    ('3', "Low"),
)

# Amey Raje
# types of notification
# type = id in MailNotification table in setupdata.models
# to use in noti_via_mail as parameter
# values are ids in database table
# new ones
mail_client_register = 3
mail_vendor_register = 4
mail_external_vendor_register = 5
mail_external_user_register = 6
mail_client_onboarding = 7
mail_vendor_onboarding = 8
mail_new_campaign = 9
mail_new_rfq_campaign = 10
mail_onboarding_approval = 11
mail_new_group = 12
mail_rfq_quotation_request = 13
mail_vendor_rfq_quotaion = 14
mail_rfq_quotation_approval = 15
mail_rfq_quotation_reject = 16
mail_rfq_quotation_counter = 17
mail_lead_upload = 18
mail_lead_approval = 19
mail_lead_reject = 20
mail_lead_rectify = 21
mail_assign_more_leads = 22
mail_cancel_assigned_leads = 23
mail_vendor_assignment = 24
mail_vendor_approval_campaign = 25
mail_campaign_updates=26
mail_assets_and_scripts = 27
mail_pull_out_leads = 28
mail_counter_action_by_vendor = 29
mail_counter_action_by_client = 30
mail_delivery_data_upload = 31

#lead rejected status code
lead_rejected_header                            = '#TCL-101'
lead_rejected_abm_list                          = '#TCL-102'
lead_rejected_suppression_list                  = '#TCL-103'
lead_rejected_match_lead_with_currant_vendor    = '#TCL-104'
lead_rejected_duplicate_entry_match_with_vendor = '#TCL-105'
lead_rejected_Duplicate_entry_match_with_months = '#TCL-106'
lead_rejected_company_size                      = '#TCL-107'
lead_rejected_company_revenue                   = '#TCL-108'
lead_rejected_industry_type_validation          = '#TCL-109'    
lead_rejected_country_validation                = '#TCL-110'
lead_rejected_company_limitation                = '#TCL-111' 
lead_rejected_custom_question_validation        = '#TCL-112'   
lead_rejected_validate_custom_specs             = '#TCL-113'
lead_rejected_remove_duplicated_lead_csv        = '#TCL-114'

# kishor Outreach methods global var
# Email
email_out=1
# hybrid
Hybrid=5
# tele_out
tele_out=2

from django.urls import path, include, re_path
from . import views

urlpatterns = [
    path('rfq_to_archives/', views.rfq_to_archives, name='rfq_to_archives'),
    path('update_rfq_veiw_list/', views.update_rfq_veiw_list, name="update_rfq_veiw_list"),
    path('get_cpl_list/', views.get_cpl_list, name="get_cpl_list"),
    path('get_rfq_request/', views.get_rfq_request, name="get_rfq_request"),
    path('publish_rfqs/', views.publish_rfqs, name="publish_rfqs"),
    path('add_new_spec/', views.add_new_spec, name='add_new_spec'),
    path('save_custom_specs_data/', views.save_custom_specs_data, name='save_custom_specs_data'),
    path('save_selected_custom_specs_data/', views.save_selected_custom_specs_data, name='save_selected_custom_specs_data'),
    path('remove_custom_specs/', views.remove_custom_specs, name='remove_custom_specs'),
    path('add_custom_specs/', views.add_custom_specs, name = 'add_custom_specs'),
    path('update_city_state_list/', views.update_city_state_list, name = 'update_city_state_list'),
]

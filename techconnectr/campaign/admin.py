from django.contrib import admin
from django.apps import apps
from .models import *

# Register your models here.
app = apps.get_app_config("campaign")
for model_name, model in app.models.items():
    if model_name not in ["scripts", "vendor_agreement_status", 'campaignmappdata']:
        admin.site.register(model)


@admin.register(Scripts)
class ScriptsAdmin(admin.ModelAdmin):
    list_display = [
        "campaign",
        "user",
        "client_script",
        "modified_script",
        "approved_status",
        "rejected_status",
    ]


@admin.register(Vendor_agreement_status)
class Vendor_agreement_statusAdmin(admin.ModelAdmin):
    list_display = ["client", "vendor_id", "agreements", "status"]

@admin.register(CampaignMappData)
class CampaignMappDataAdmin(admin.ModelAdmin):
    list_display = ['campaign', 'client']
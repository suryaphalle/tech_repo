# Generated by Django 2.1.4 on 2019-09-16 05:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0031_campaign_custom_field_internal_qa_headers'),
    ]

    operations = [
        migrations.AddField(
            model_name='campaign',
            name='rfq_quotation',
            field=models.TextField(blank=True, null=True),
        ),
    ]

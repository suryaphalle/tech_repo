# Generated by Django 2.1.4 on 2019-08-30 13:43

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0024_campaign_custom_field_user_id_add_header'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campaign_custom_field',
            name='campaign',
            field=models.ForeignKey(blank='true', null='true', on_delete=django.db.models.deletion.CASCADE, to='campaign.Campaign'),
        ),
    ]

# Generated by Django 2.1.4 on 2019-08-01 14:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0011_auto_20190801_1426'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='external_vendor',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='internal_vendor',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='tc_vendor',
            field=models.BooleanField(default=False),
        ),
    ]

# Generated by Django 2.1.4 on 2019-08-29 06:37

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('campaign', '0021_auto_20190827_1123'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vendor_agreement_status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('vendor_id', models.IntegerField(blank=True, null=True)),
                ('agreements', models.TextField(blank=True, null=True)),
                ('status', models.BooleanField(default=False)),
                ('client', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='campaign_allocation',
            name='vendor_agreements',
            field=models.TextField(blank=True, null=True),
        ),
    ]

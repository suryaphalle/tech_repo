# Generated by Django 2.1.4 on 2019-09-23 10:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0035_auto_20190920_0641'),
    ]

    operations = [
        migrations.AddField(
            model_name='mapping',
            name='city',
            field=models.TextField(blank='true', null='true'),
            preserve_default='true',
        ),
        migrations.AddField(
            model_name='mapping',
            name='state',
            field=models.TextField(blank='true', null='true'),
            preserve_default='true',
        ),
    ]

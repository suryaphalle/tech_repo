# Generated by Django 2.1.4 on 2019-07-31 06:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0006_auto_20190730_1625'),
    ]

    operations = [
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='rfq_timer',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]

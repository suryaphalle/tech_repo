# Generated by Django 2.1.4 on 2019-07-30 07:10

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0003_rfq_campaigns'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='rfq_campaigns',
            name='adhoc',
        ),
        migrations.RemoveField(
            model_name='rfq_campaigns',
            name='custom_question_status',
        ),
        migrations.RemoveField(
            model_name='rfq_campaigns',
            name='status',
        ),
        migrations.AddField(
            model_name='rfq_campaigns',
            name='external_vendor',
            field=models.PositiveIntegerField(default=1),
        ),
        migrations.AddField(
            model_name='rfq_campaigns',
            name='internal_vendor',
            field=models.PositiveIntegerField(default=1),
        ),
        migrations.AddField(
            model_name='rfq_campaigns',
            name='tc_vendor',
            field=models.PositiveIntegerField(default=1),
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='created_date',
            field=models.DateField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='created_time',
            field=models.TimeField(auto_now_add=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='custom_question',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='description',
            field=models.TextField(blank=True, default=''),
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='end_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='industry_type',
            field=models.TextField(blank='true', null='true'),
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='job_title_function',
            field=models.TextField(blank='true', null='true'),
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='rfq_timer',
            field=models.DateTimeField(default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='start_date',
            field=models.DateField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='target_quantity',
            field=models.PositiveIntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='updated_date',
            field=models.DateField(auto_now=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='rfq_campaigns',
            name='updated_time',
            field=models.TimeField(auto_now=True, default=django.utils.timezone.now),
            preserve_default=False,
        ),
    ]

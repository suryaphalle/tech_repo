from django.db import models

from setupdata.models import *
from user.models import *
from client_vendor.models import *

from .choices import *

# Create your models here.


class Campaign(models.Model):

    user = models.ForeignKey(user, on_delete=models.CASCADE, null='true', blank='true')

    name = models.CharField(max_length=500)
    io_number = models.CharField(max_length=50, blank=True, default='')
    po_number = models.CharField(max_length=50, blank='true', default='')

    description = models.TextField(default='', blank=True)
    type = models.CharField(choices=CAMPAIGN_TYPES_CHOICES, max_length=300, null='true', blank='true')
    method = models.ManyToManyField(source_touches)

    start_date = models.DateField(blank='true', null='true')
    end_date = models.DateField(blank='true', null='true')
    rfq_timer = models.DateTimeField(null=True, blank=True)

    # 0 - draft(incomplete) ; 1 - live; 2 - pause;  3 - pending; 4 - completed; 5 - assign
    status = models.IntegerField(default=0)
    raimainingleads = models.IntegerField(default=0)
    approveleads = models.IntegerField(default=0)
    # suryakant 23-09-2019
    # changed cpl integer to float
    cpl = models.FloatField(default=0)
    target_quantity = models.IntegerField(default=0)

    rfq = models.BooleanField(default=False)
    adhoc = models.BooleanField(default=False)
    completion_status = models.BooleanField(default=False)
    geo = models.TextField(blank='true', null='true')
    priority = models.CharField(choices=PRIORITY_CHOICES, max_length=50, blank='true', default=3, null='true')

    tc_vendor = models.BooleanField(default=False)
    internal_vendor = models.BooleanField(default=False)
    external_vendor = models.BooleanField(default=False)

    created_date = models.DateField(auto_now_add=True, null='true')
    created_time = models.TimeField(auto_now_add=True, null='true')
    updated_date = models.DateField(auto_now=True, null='true')
    updated_time = models.TimeField(auto_now=True, null='true')
    rfq_quotation = models.TextField(blank = True, null = True)
    class Meta:
        verbose_name = 'Campaign'
        verbose_name_plural = 'Campaign'

    def __str__(self):
        return self.name


class RFQ_Campaigns(models.Model):
    name = models.CharField(max_length=500)
    io_number = models.CharField(max_length=50, blank='true', null='true')
    description = models.TextField(blank=True, default='')

    start_date = models.DateField(null=True, blank=True)
    end_date = models.DateField(null=True, blank=True)
    rfq_timer = models.DateTimeField(null=True, blank=True)
    # suryakant 23-09-2019
    # changed cpl integer to float
    cpl = models.FloatField(default=0)
    target_quantity = models.PositiveIntegerField(default=0)
    custom_question = models.PositiveIntegerField(default=0)

    abm_status = models.BooleanField(default=False)
    abm_file_content = models.TextField(blank='true', null='true')
    suppression_status = models.BooleanField(default=False)
    suppression_file_content = models.TextField(blank='true', null='true')

    method = models.TextField(blank=True, null=True)
    user = models.ForeignKey(user, on_delete=models.CASCADE)

    geo = models.TextField(blank='true', null='true')
    industry_type = models.TextField(blank='true', null='true')
    job_title_function = models.TextField(blank='true', null='true')
    special_instructions = models.TextField(blank='true', null='true')

    company_size = models.CharField(max_length=1200, blank='true', null='true')
    revenue_size = models.CharField(max_length=1200, blank='true', null='true')

    tc_vendor = models.BooleanField(default=True)
    internal_vendor = models.BooleanField(default=True)
    external_vendor = models.BooleanField(default=True)

    archive_status = models.BooleanField(default=False)

    created_date = models.DateField(auto_now_add=True)
    created_time = models.TimeField(auto_now_add=True)
    updated_date = models.DateField(auto_now=True)
    updated_time = models.TimeField(auto_now=True)

    def __str__(self):
        return self.name

class RFQ_campaign_quotes(models.Model):
    vendor = models.ForeignKey(user, on_delete=models.CASCADE, blank='true', null='true')
    campaign = models.ForeignKey(RFQ_Campaigns,on_delete=models.CASCADE, blank='true', null='true')
    cpl = models.FloatField(default=0, blank='true', null='true')
    volume = models.IntegerField(default=0, blank='true', null='true')
    comments =models.TextField(blank='true', null='true')
    created_date = models.DateField(auto_now=True)
    created_time = models.TimeField(auto_now=True)
    
    def __str__(self):
        return f'{self.campaign.name} - {self.vendor.user_name}'
'''Campaign Type
class Campaign_Type(models.Model):
	campaign_type = models.CharField(choices=CAMPAIGN_TYPES_CHOICES, max_length=300, null='true', blank='true')

	class Meta:
		verbose_name = 'Campaign_Type'
		verbose_name_plural = 'Campaign_Type'

	def __str__(self):
		return self.campaign_type
'''

class CampaignMappData(models.Model):
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE, null='true')
    client=models.ForeignKey(user, on_delete=models.CASCADE, null='true', blank='true')
    mapp_date=models.TextField(blank='true', null='true')
    temp_mapp_data=models.TextField(blank='true', null='true')

class Specification(models.Model):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, null='true')
    campaign_category = models.CharField(
        max_length=1200, blank='true', null='true')
    campaign_quantity = models.IntegerField(blank='true', null='true')
    campaign_pacing = models.CharField(
        max_length=1200, blank='true', null='true')
    touchbase_meeting = models.CharField(
        max_length=1100, blank='true', null='true')

    # check abm/suppression available or not for current campaign
    abm_status = models.IntegerField(blank='true', null='true', default=0)
    suppression_status = models.IntegerField(
        blank='true', null='true', default=0)

    # account based marketing
    abm_company_file = models.FileField(
        upload_to='campaign/', blank='true', max_length=500, null='true')
    abm_contact_file = models.FileField(
        upload_to='campaign/', blank='true', max_length=500, null='true')
    abm_file_content = models.TextField(
        blank='true', null='true')  # store all abm in list
    abm_count = models.IntegerField(default=0)  # get count of all abm list
    # suppression list
    suppression_company_file = models.FileField(
        upload_to='campaign/', blank='true', max_length=500, null='true')
    suppression_contact_file = models.FileField(
        upload_to='campaign/', blank='true', max_length=500, null='true')
    suppression_file_content = models.TextField(
        blank='true', null='true')  # store suppression in list
    suppression_count = models.IntegerField(
        default=0)  # get count of all suppression list

    class Meta:
        verbose_name = 'Specification'
        verbose_name_plural = 'Specification'

    def __str__(self):
        return self.campaign.name


class Mapping(models.Model):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, null='true')
    industry_type = models.CharField(
        max_length=1200, blank='true', null='true')
    job_title = models.TextField(blank='true', null='true')
    special_instructions = models.TextField(blank='true', null='true')
    job_level = models.CharField(max_length=1200, blank='true', null='true')
    job_function = models.CharField(max_length=1200, blank='true', null='true')
    job_title_function = models.CharField(
        max_length=1200, blank='true', null='true')
    country = models.TextField(blank='true', null='true')

    # how many custom_question Need
    custom_question = models.IntegerField(default=0)
    custom_status = models.IntegerField(default=0)
    #country=models.ForeignKey(country,on_delete=models.CASCADE, blank='true', null='true')
    company_size = models.CharField(
        max_length=1200, default=0, blank='true', null='true')
    revenue_size = models.CharField(
        max_length=1200, default=0, blank='true', null='true')
    city = models.TextField(blank='true', null='true')
    state = models.TextField(blank='true', null='true')
    class Meta:
        verbose_name = 'Mapping'
        verbose_name_plural = 'Mappings'

    def __str__(self):
        return f'{self.campaign.name}'


class Terms(models.Model):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, null='true')
    source_touches = models.CharField(
        max_length=1200, blank='true', null='true')
    level_intent = models.CharField(max_length=1200, blank='true', null='true')
    assets_name = models.CharField(max_length=1200, blank='true', null='true')
    assets_type = models.TextField(blank='true', null='true')
    assets = models.CharField(max_length=1200, blank='true', null='true')
    sponsers = models.CharField(max_length=1200, blank='true', null='true')
    asset_distributor = models.CharField(
        max_length=1200, blank='true', null='true')
    add_assetslink = models.CharField(
        max_length=1200, blank='true', null='true')

    def __str__(self):
        return self.campaign.name

    class Meta:
        verbose_name = 'Term'
        verbose_name_plural = 'Terms'


class Volume_CPL(models.Model):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, null='true')
    level_of_intent = models.CharField(
        blank='true', max_length=500, null='true')
    volume = models.CharField(blank='true', max_length=50, null='true')
    cpl = models.CharField(blank='true', max_length=50, null='true')

    def __str__(self):
        return str(self.campaign.name) + " " + self.level_of_intent + " " + self.volume


class Delivery(models.Model):
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, null='true')
    data_header = models.CharField(max_length=1200, blank='true', null='true')
    tc_header_status = models.IntegerField(default=0)
    custom_header = models.CharField(
        max_length=1200, blank='true', null='true')
    custom_header_status = models.IntegerField(default=0)
    custom_header_mapping = models.TextField(blank=True, null=True)
    custom_question = models.CharField(
        max_length=1000, blank='true', null='true')
    delivery_method = models.CharField(
        max_length=1000, blank='true', null='true')
    comments = models.TextField(max_length=1000, blank=True, null=True)
    campaign_delivery_time_type = models.CharField(
        max_length=1200, blank='true', null='true')
    campaign_delivery_time = models.TextField(blank='true', null='true')

    # def __str__(self):
    #     return self.campaign.name

    class Meta:
        verbose_name = 'Delivery'
        verbose_name_plural = 'Deliveries'

#  Campaign_allocation
class campaign_allocation(models.Model):
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    client_vendor = models.ForeignKey(
        user, on_delete=models.CASCADE, blank='true', null='true')
    rfqcpl = models.FloatField(blank='true', null='true')
    rfqvolume = models.IntegerField(blank='true', null='true')
    volume = models.IntegerField(blank='true', null='true')
    cpl = models.FloatField(blank='true', null='true')
    status = models.IntegerField(blank='true', null='true', default='0')
    old_volume = models.IntegerField(blank='true', null='true')
    client_cpl = models.FloatField(blank='true', null='true')
    upload_leads = models.TextField(blank='true', null='true')
    submited_lead = models.IntegerField(default=0)
    batch_count = models.IntegerField(default=0)
    approve_leads = models.IntegerField(default=0)
    return_lead = models.IntegerField(default=0)
    counter_status = models.IntegerField(default=0)
    assign_more_lead_status = models.IntegerField(default=0)
    suggest_status = models.IntegerField(default=0)
    # agreements to be signed by vendor..Akshay G.
    vendor_agreements = models.TextField(blank = True, null =True)
    reasons = models.CharField(max_length=1200,blank=True,null=True) # to identify pause campaign by client(1) or by vendor(2)
    
    def __str__(self):
        return f'{self.campaign.name} - {self.client_vendor.user_name}'

    class Meta:
        verbose_name = 'Campaign Allocation'
        verbose_name_plural = 'Campaign Allocation'


class More_allocation(models.Model):
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    campaign_allocation = models.ForeignKey(campaign_allocation, on_delete=models.CASCADE, null=True, blank=True)
    client_vendor = models.ForeignKey(user, on_delete=models.CASCADE, blank='true', null='true')
    volume = models.IntegerField(blank='true', null='true')
    cpl = models.FloatField(blank='true', null='true')
    status = models.IntegerField(blank='true', null='true', default='3')

    def __str__(self):
        return f'{self.campaign.name} - {self.client_vendor.user_name}'

    class Meta:
        verbose_name = 'More Allocation'
        verbose_name_plural = 'More Allocations'

class Campaign_commission(models.Model):
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    camp_alloc = models.ForeignKey(campaign_allocation,on_delete=models.CASCADE, blank='true', null='true')
    vendor = models.ForeignKey(
        user, on_delete=models.CASCADE, blank='true', null='true')
    ccpl = models.FloatField(blank='true', null='true')
    # suryakant 23-09-2019
    # changed cpl integer to float (cplLeadAllocation,lead_assign)
    vcpl = models.FloatField(blank='true', null='true')
    rev_share_percentage = models.FloatField(blank='true', null='true')    
    allocated_lead = models.IntegerField(blank='true', null='true')
    date = models.DateField(auto_now_add=True, null='true')

    def __str__(self):
        return self.campaign.name

class match_campaign_vendor(models.Model):
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    client_vendor = models.ForeignKey(
        user, on_delete=models.CASCADE, blank='true', null='true')
    is_active = models.IntegerField(default='1')

    def __str__(self):
        return self.campaign.name

    class Meta:
        verbose_name = 'Match vendor according campaign'
        verbose_name_plural = 'Match vendors'

class match_rfq_campaign_vendor(models.Model):
    campaign = models.ForeignKey(RFQ_Campaigns, on_delete=models.CASCADE)
    client_vendor = models.ForeignKey(
        user, on_delete=models.CASCADE, blank='true', null='true')
    is_active = models.IntegerField(default='1')

    def __str__(self):
        return self.campaign.name

    class Meta:
        verbose_name = 'Match vendor according rfq campaign'
        verbose_name_plural = 'Match rfq vendors'


class Counter_cpl(models.Model):
    """ to store counter cpl and volume """
    req_cpl = models.FloatField(null=True, blank=True)
    volume = models.IntegerField(null=True,blank=True)
    comment = models.CharField(max_length=256, blank=True, null=True)    
    user_id = models.ForeignKey(
        user, on_delete=models.CASCADE, blank=True, null=True)
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, blank=True, null=True)
    campaign_allocation = models.ForeignKey(
        campaign_allocation, on_delete=models.CASCADE, blank=True, null=True)

    def __str__(self):
        return self.campaign.name


# model for chat messages
class Campaign_chats(models.Model):
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    status = models.IntegerField(null=True, blank=True)
    data = models.TextField(null=True, blank=True)
    # total messages sent to user
    send_msg_count = models.TextField(null=True, blank=True)
    # total messages received by user
    received_msg_count = models.TextField(null=True, blank=True)
    def __str__(self):
        return self.campaign.name

# model for campaign Notification


class Campaign_notification(models.Model):
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    data = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.campaign.name

# model for campaign RFQ


class Campaign_rfq(models.Model):
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    old_cpl = models.FloatField(null=True, blank=True)
    cpl = models.FloatField(null=True, blank=True)
    volume = models.IntegerField(null=True, blank=True)
    remark = models.CharField(max_length=120, null=True, blank=True)
    old_volume = models.IntegerField(null=True, blank=True)
    status = models.IntegerField(default=0)

    def __str__(self):
        return self.campaign.name


class Scripts(models.Model):
    campaign = models.ForeignKey(Campaign, on_delete=models.CASCADE)
    campaign_allocation = models.ForeignKey(campaign_allocation, on_delete=models.CASCADE, null='true', blank='true')
    client_script = models.FileField(
        upload_to='scripts/', max_length=500, blank=True, null=True)
    user = models.ForeignKey(user, on_delete=models.CASCADE, null = True)
    modified_script = models.FileField(
        upload_to='scripts/', max_length=500, blank=True, null=True)
    approved_status = models.BooleanField(default = False)
    rejected_status = models.BooleanField(default = False)
    # vendor_scripts = models.TextField(null=True,blank=True)

    def __str__(self):
        return self.campaign.name

# status of agreements uploaded by vendor for the first while dealing with client...Akshay G.
class Vendor_agreement_status(models.Model):
    client = models.ForeignKey(user, on_delete=models.CASCADE)
    vendor_id = models.IntegerField(null = True, blank = True)
    # vendor agreements with path 
    agreements = models.TextField(null = True, blank =True)
    # status of all uploaded vendor agreements
    status = models.BooleanField(default = False)
    class Meta:
        verbose_name_plural = 'Vendor agreement status'


class Campaign_custom_field(models.Model):
    campaign = models.ForeignKey(Campaign,on_delete=models.CASCADE, blank='true', null='true')
    add_header_by_internal_team =  models.BooleanField(default=False)
    user_id_add_header = models.ForeignKey(user, on_delete=models.CASCADE, null = True)
    internal_QA_headers = models.TextField(null = True, blank =True)
    
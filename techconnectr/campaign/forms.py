
from django import forms
from campaign.models import *
from setupdata.models import *
import datetime, pandas
from campaign.choices import *

class CampaignForm(forms.ModelForm):

    class Meta:
        model = Campaign
        fields = "__all__"

class SpecificationForm(forms.ModelForm):
    class Meta:
        model = Specification
        fields = "__all__"


class MappingForm(forms.ModelForm):
    class Meta:
        model = Mapping
        fields = "__all__"


class TermsForm(forms.ModelForm):
    class Meta:
        model = Terms
        fields = "__all__"


class DeliveryForm(forms.ModelForm):
    class Meta:
        model = Delivery
        fields = "__all__"

class CounterCPLForm(forms.ModelForm):
    class Meta:
        model = Counter_cpl
        fields = "__all__"

class Script_Form(forms.ModelForm):
    class Meta:
        model = Scripts
        fields = "__all__"

# class CampaignType(forms.ModelForm):
#     class Meta:
#         model = Campaign_Type
#         fields = "__all__"



class RFQCampaignForm(forms.ModelForm):

    name = forms.CharField(max_length=500, strip=True)
    io_number = forms.CharField(required=False, max_length=50, strip=True)
    description = forms.CharField(required=False, widget=forms.Textarea, strip=True)

    method = forms.MultipleChoiceField(choices=RFQ_CAMPAIGN_METHOD_CHOICES)

    geo = forms.CharField(widget=forms.HiddenInput(), strip=True)
    # industry_type = forms.MultipleChoiceField(choices=RFQ_CAMPAIGN_INDUSTRY_CHOICES, required=False)
    job_title_function = forms.CharField(widget=forms.Textarea, strip=True)

    abm_status = forms.BooleanField(required=False)
    abm_file = forms.FileField(required=False)
    suppression_status = forms.BooleanField(required=False)
    suppression_file = forms.FileField(required=False)

    custom_question = forms.IntegerField(min_value=0, max_value=10)
    # company_size = forms.MultipleChoiceField(choices=RFQ_CAMPAIGN_COMPANY_CHOICES, required=False)
    # revenue_size = forms.MultipleChoiceField(choices=RFQ_CAMPAIGN_REVENUE_CHOICES, required=False)

    target_quantity = forms.IntegerField(min_value=0)
    cpl = forms.FloatField(min_value=0, required = False)
    start_date = forms.DateField(required=False)
    end_date = forms.DateField(required=False)
    rfq_timer = forms.CharField(max_length=50, strip=True)

    special_instructions = forms.CharField(required=False, widget=forms.Textarea, strip=True)

    def __init__(self, *args, **kwargs):
        # suryakant 04-10-2019
        # to add dynamic choices to the form fields
        super().__init__(*args, **kwargs)
        new_ind = RFQ_CAMPAIGN_INDUSTRY_CHOICES
        new_comp = RFQ_CAMPAIGN_COMPANY_CHOICES
        new_rev = RFQ_CAMPAIGN_REVENUE_CHOICES
        if self.instance.industry_type not in [None, '0', '']:
            for industry_type in eval(self.instance.industry_type):
                if [industry_type,industry_type] not in new_ind:
                    new_ind += ([industry_type,industry_type],)
        
        if self.instance.company_size not in [None, '0', '']:
            for company_size in eval(self.instance.company_size):
                if [company_size,company_size] not in new_comp:
                    new_comp += ([company_size,company_size],)

        if self.instance.revenue_size not in [None, '0', '']:
            for revenue_size in eval(self.instance.revenue_size):
                if [revenue_size,revenue_size] not in new_rev:
                    new_rev += ([revenue_size,revenue_size],)
        
        self.fields['industry_type'] = forms.MultipleChoiceField(choices=new_ind,required=False)
        self.fields['company_size'] = forms.MultipleChoiceField(choices=new_comp,required=False)
        self.fields['revenue_size'] = forms.MultipleChoiceField(choices=new_rev,required=False)
        
    def clean(self):
        cleaned_data = super().clean()
        # company_size = cleaned_data.get('company_size')
        # revenue_size = cleaned_data.get('revenue_size')
        campaign_name = cleaned_data.get('name')
        # if company_size is not None and revenue_size is not None:
        #     if len(company_size) == 0 and len(revenue_size) == 0:
        #         msg = "Either company size or revenue size is required"
        #         self.add_error('revenue_size', msg)
        if campaign_name is not None:
            if len(campaign_name) > 400:
                msg = "Enter campaign name of maximum 400 characters."
                self.add_error('name', msg)

    def clean_abm_file(self):
        abm_file = self.cleaned_data['abm_file']
        csv_data = None
        if abm_file is not None:
            if abm_file.name.endswith('.csv') or abm_file.name.endswith('.xlsx'):
                labels = ['DOMAIN_NAME','COMPANY_NAME']
                data, status, error = read_csv_xlsx_file(abm_file)
                if status:
                    header, csv_data = form_fetch_csv_data(data)
                    if set(header) != set(labels):
                        csv_data = None
                        msg = "Uploaded template is invalid."
                        self.add_error('abm_file', msg)
                    elif len(csv_data) == 0:
                        msg = "Uploaded file is empty."
                        self.add_error('abm_file', msg)
                else:
                    if error == 'empty':
                        msg = "Uploaded file is empty."
                        self.add_error('suppression_file', msg)  
                    if error == 'invalidFormat':
                        msg = "Please upload only .csv or .xlsx file.."
                        self.add_error('suppression_file', msg)
            else:
                msg = "Upload file with .csv or .xlsx format only"
                self.add_error('abm_file', msg)

        return csv_data

    def clean_suppression_file(self):
        suppression_file = self.cleaned_data['suppression_file']
        csv_data = None
        if suppression_file is not None:
            if suppression_file.name.endswith('.csv') or suppression_file.name.endswith('.xlsx'):
                labels = ['DOMAIN_NAME','COMPANY_NAME','EMAIL']
                data, status, error = read_csv_xlsx_file(suppression_file)
                if status:
                    header, csv_data = form_fetch_csv_data(data)
                    if set(header) != set(labels):
                        csv_data = None
                        msg = "Uploaded template is invalid."
                        self.add_error('suppression_file', msg)
                    elif len(csv_data) == 0:
                        msg = "Uploaded file is empty."
                        self.add_error('suppression_file', msg)
                else:
                    if error == 'empty':
                        msg = "Uploaded file is empty."
                        self.add_error('suppression_file', msg)  
                    if error == 'invalidFormat':
                        msg = "Please upload only .csv or .xlsx file."
                        self.add_error('suppression_file', msg)
            else:
                msg = "Upload file with .csv or .xlsx format only"
                self.add_error('suppression_file', msg)

        return csv_data

    

    class Meta:
        model = RFQ_Campaigns
        exclude = ('revenue_size','company_size','industry_type','rfq_timer', 'abm_file', 'suppression_file', 'user', 'tc_vendor', 'internal_vendor', 'external_vendor')


class NormalCampaignForm(forms.ModelForm):

    name = forms.CharField(max_length=500, strip=True)
    io_number = forms.CharField(max_length=50, strip=True, required=False)
    description = forms.CharField(widget=forms.Textarea, strip=True)
    type = forms.ChoiceField(choices=CAMPAIGN_TYPES_CHOICES)
    method = forms.ModelMultipleChoiceField(queryset=source_touches.objects.all())
    cpl = forms.FloatField(widget=forms.HiddenInput(), min_value=1)
    target_quantity = forms.IntegerField(widget=forms.HiddenInput(), min_value=1)
    start_date = forms.DateField()
    end_date = forms.DateField()
    tc_vendor = forms.BooleanField(required=False)
    internal_vendor = forms.BooleanField(required=False)
    external_vendor = forms.BooleanField(required=False)

    def clean(self):
        cleaned_data = super().clean()

        tc_vendor = cleaned_data.get('tc_vendor')
        internal_vendor = cleaned_data.get('internal_vendor')
        external_vendor = cleaned_data.get('external_vendor')
        campaign_name = cleaned_data.get('name')
        if tc_vendor == False and internal_vendor == False and external_vendor == False:
            msg = "Please select at least one vendor type to proceed"
            self.add_error('external_vendor', msg)
        if campaign_name is not None:
            if len(campaign_name) > 400:
                msg = "Enter campaign name of maximum 400 characters."
                self.add_error('name', msg)

    
    class Meta:
        model = Campaign
        fields = ('name', 'io_number', 'description', 'type', 'method', 'cpl', 'target_quantity', 'start_date', 'end_date', 'tc_vendor', 'internal_vendor', 'external_vendor')

# === Fetch CSV Data ===
def form_fetch_csv_data(data):
    import datetime
    # fetch csv file data ...Akshay G.
    data = data.dropna()
    # file headers
    header = []
    csv_data = []
    domain_list = ['http://www.','https://www.','www.', 'http://','https://', 'http:', 'https:']
    for row in data:
        if 'Unnamed:' in row:
            continue
        header.append(row)
    for i in range(len(data)):
        temp_data = []
        user_domain = []
        for head in header:
            if data[head][i] != 'non':
                if head.strip() == 'DOMAIN_NAME':
                    user_domain = [s for s in domain_list if data[head][i].lower().startswith(s)]
                    if len(user_domain) > 0:
                        data[head][i] = data[head][i].replace(user_domain[0], '')
                    if '.' not in data[head][i]:
                        data[head][i] = data[head][i] + '.com'
                temp_data.append(data[head][i])
        value = dict(zip(header, temp_data))
        value['timestamp'] = datetime.datetime.now()
        value['user_domain'] = user_domain[0] if len(user_domain) > 0 else ''
        csv_data.append(value)
    return header, csv_data

# read .csv and .xlsx file for abm & suppression upload..Akshay G.
def read_csv_xlsx_file(csv_file):
    if csv_file.name.endswith('.csv'):
        try:
            data = pandas.read_csv(csv_file,skip_blank_lines=True,na_filter=False,encoding ='latin1')
            data = data.dropna()
        except:
            return [], False, 'empty'
    elif csv_file.name.endswith('.xlsx'):
        try:
            xl = pandas.ExcelFile(csv_file)
            data = pandas.read_excel(xl,xl.sheet_names[0],dtype=str).fillna('')
            data = data.dropna()
        except:
            return [], False, 'empty'
    else:
        return [], False, 'invalidFormat'
    return data, True, ''
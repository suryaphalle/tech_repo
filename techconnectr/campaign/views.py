from django.shortcuts import render, HttpResponseRedirect
from .models import *
from setupdata.models import *
from client.models import *
from django.http import HttpResponse, JsonResponse

from django.conf import settings
import os
import ast
from campaign_mapping.models import *
# Create your views here.

# === RFQ To Achives ===
def rfq_to_archives(request):
    import datetime
    time_threshold = datetime.datetime.now() - datetime.timedelta(hours=72)
    RFQ_Campaigns.objects.filter(rfq_timer__lt=time_threshold).update(archive_status=True)
    return HttpResponseRedirect('/')

# === Get CPL List ===
def get_cpl_list(request):
    # for rfq quotes returns vendor list and fields value
    camp_id = request.GET.get('camp_id')
    # user_id = request.session['userid']
    get_details = []
    camp = RFQ_Campaigns.objects.get(id=camp_id)
    user_id = camp.user_id
    try:
        def_data = ClientCustomFields.objects.get(client_id=request.session['userid'])
        fields = eval(def_data.rfq_popup_fields)
    except Exception as e:
        def_data = SystemDefaultFields.objects.first()
        fields = eval(def_data.rfq_fields)
    for field in [d['db_name'] for d in fields]:
        datadict = [ d for d in fields if d['db_name'] == field]
        if len(datadict) > 0:
            f = eval(getattr(camp,datadict[0]['db_name'])) if datadict[0]['db_name'] in ['method','industry_type','company_size','revenue_size'] and getattr(camp,datadict[0]['db_name']) != None else getattr(camp,datadict[0]['db_name'])
            if type(f) != bool and f != None:
                d = ','.join(f).replace(',',', ') if type(f) == list else str(f).replace(',',', ')
            else:
                d = f if f != None else '--'
            get_details.append({'display':datadict[0]['display'],'db_name':datadict[0]['db_name'],'value': d})
    vendor_list = []
    # TC_vendor_list = user.objects.filter(usertype=2) | user.objects.get(id=user_id).child_user.all().exclude(usertype=6)
    TC_vendor_list = user.objects.filter(id__in=match_rfq_campaign_vendor.objects.filter(campaign=camp).values_list('client_vendor',flat=True)) | user.objects.get(id=user_id).child_user.all().exclude(usertype=6)
    requested_vendors = RFQ_campaign_quotes.objects.filter(campaign_id=camp_id)

    client_vendor_detail = client_vendor.objects.get(user_id=user_id)
    client_vendor_detail=str(client_vendor_detail.company_logo.url) if client_vendor_detail.company_logo != '' else os.path.join(settings.BASE_URL, 'media', 'default.jpg')

    ''' urldata = os.path.join(settings.MEDIA_ROOT, client_vendor_detail) '''
    
    for vendor in TC_vendor_list.order_by('usertype'):
        if vendor.usertype.id == 2:
            vtype = "(TC)"
        elif vendor.usertype.id == 5:
            vtype = "(Ext)"
        elif vendor.usertype.id == 9:
            vtype = "(Int)"
        
        if vendor.id in requested_vendors.values_list('vendor_id',flat=True):

            vendor_list.append({
                'checked':'checked',
                'id':vendor.id,
                'cpl':requested_vendors.get(vendor_id=vendor.id).cpl,
                'volume':requested_vendors.get(vendor_id=vendor.id).volume,
                'name':f'{vtype} {vendor.user_name}',
                'rev_share':def_data.rfq_rev_share,

                # Add Vendor List by...Kishor
                'email':vendor.email,
                'comment':requested_vendors.get(vendor_id=vendor.id).comments,
                'created_date':requested_vendors.get(vendor_id=vendor.id).created_date,
                'created_time':requested_vendors.get(vendor_id=vendor.id).created_time,
                'client_vendor_detail':client_vendor_detail,

            })
        else:
            vendor_list.append({
                'checked':'unchecked',
                'id':vendor.id,
                'cpl':0,
                'volume':0,
                'name':f'{vtype} {vendor.user_name}',
                'rev_share':def_data.rfq_rev_share,
                'email':vendor.email,
                'comment':0,
                'created_date':0,
                'created_time':0,
                'client_vendor_detail':client_vendor_detail,
            })
    print(camp.rfq_timer)
    data = {'rfq_timer': camp.rfq_timer,'success': 1,'ext_vendor_list':vendor_list,'details':get_details,'vendor_data':vendor_list,'fields':fields}
    return JsonResponse(data)

# === Update RFQ Cmpaign View list ===
def update_rfq_veiw_list(request):
    # update rfq campaign view list
    camp_id = request.POST.get('camp_id')
    user_id = request.session['userid']
    view_list = request.POST.getlist('view_list[]')
    client_data,created = ClientCustomFields.objects.get_or_create(client_id=user_id)
    if created:
        fields = eval(SystemDefaultFields.objects.all()[0].rfq_fields)
    else:
        try:
            fields = eval(client_data.rfq_popup_fields)
        except Exception as e:
            fields = eval(SystemDefaultFields.objects.all()[0].rfq_fields)

    for view in [d['db_name'] for d in fields]:
        vdict = [ d for d in fields if d['db_name'] == view]
        if vdict[0]['db_name'] in view_list:
            vdict[0]['status'] = 1
        else:
            vdict[0]['status'] = 0
    client_data.rfq_popup_fields = fields
    client_data.save()
    get_details = []
    camp = RFQ_Campaigns.objects.get(id=camp_id)
    for field in [d['db_name'] for d in fields]:
        datadict = [ d for d in fields if d['db_name'] == field]
        if len(datadict) > 0:
            f = eval(getattr(camp,datadict[0]['db_name'])) if datadict[0]['db_name'] in ['method','industry_type','company_size','revenue_size'] and getattr(camp,datadict[0]['db_name']) != None else getattr(camp,datadict[0]['db_name'])
            if type(f) != bool and f != None:
                d = ','.join(f).replace(',',', ') if type(f) == list else f.replace(',',', ')
            else:
                d = f if f != None else '--'
            get_details.append({'display':datadict[0]['display'],'db_name':datadict[0]['db_name'],'value':d})
    data = {'details':get_details,'fields':eval(ClientCustomFields.objects.get(client_id=user_id).rfq_popup_fields)}
    return JsonResponse(data)

# === Return RFQ Request ===
def get_rfq_request(request):
    # return rfq request and campaign details
    camp_id = request.GET.get('camp_id')
    campid = request.GET.get('campid')
    user_id = request.session['userid']
    get_details = []
    camp = RFQ_Campaigns.objects.get(id=campid)
    try:
        fields = eval(ClientCustomFields.objects.get(client_id=user_id).rfq_popup_fields)
    except Exception as e:
        fields = eval(SystemDefaultFields.objects.first().rfq_fields)
    for field in [d['db_name'] for d in fields]:
        datadict = [ d for d in fields if d['db_name'] == field]
        if len(datadict) > 0:
            f = eval(getattr(camp,datadict[0]['db_name'])) if datadict[0]['db_name'] in ['method','industry_type','company_size','revenue_size'] and getattr(camp,datadict[0]['db_name']) != None else getattr(camp,datadict[0]['db_name'])
            if f == None:
                f = ''
            if type(f) != bool:
                d = ','.join(f).replace(',',', ') if type(f) == list else f.replace(',',', ')
            else:
                d = f if f != None else '--'
            get_details.append({'display':datadict[0]['display'],'db_name':datadict[0]['db_name'],'value':d})
    vendor_list = []
    TC_vendor_list = RFQ_campaign_quotes.objects.filter(campaign_id=camp_id,vendor_id=user_id)
    for vendor in TC_vendor_list:
            vendor_list.append({
                'id':vendor.id,
                'cpl':vendor.cpl,
                'volume':vendor.volume,
                'name':vendor.vendor.user_name,
                'comments':vendor.comments if vendor.comments != None else '',
            })
    print(vendor_list)
    data = {'rfq_timer': camp.rfq_timer,'success': 1,'ext_vendor_list':vendor_list,'details':get_details,'vendor_data':vendor_list,'fields':fields}
    return JsonResponse(data)

# === Publish RFQ to Make it as Normal Campaign ===
def publish_rfqs(request):
    # to publish rfqs to make it as normal campaign
    ids = eval(request.POST.get('data'))
    rfq_campaigns = RFQ_Campaigns.objects.filter(id__in=ids)
    for rfq in rfq_campaigns:

        new_campaign = {
            'name': rfq.name,
            'io_number': rfq.io_number if rfq.io_number is not None else '',
            'description': rfq.description,
            'start_date': rfq.start_date,
            'end_date': rfq.end_date,
            'rfq_timer': rfq.rfq_timer,
            'target_quantity': rfq.target_quantity,
            'user': rfq.user,
            'geo': rfq.geo,
            'rfq': True,
            'tc_vendor': False,
            'external_vendor': False,
            'internal_vendor': False,
            'cpl': rfq.cpl,
        }
        campaign = Campaign.objects.create(**new_campaign)

        methods = source_touches.objects.filter(type__in=eval(rfq.method))
        campaign.method.add(*methods)
        campaign.save()
        # save rfq quotation sent by vendor..in campaign table to display while allocation..Akshay G.
        rfq_quotes_query = RFQ_campaign_quotes.objects.filter(campaign_id = rfq.id)
        L = []
        for obj in rfq_quotes_query:
            L.append({'vendor_id': obj.vendor_id, 'cpl': obj.cpl, 'lead': obj.volume})
        campaign.rfq_quotation = L
        campaign.save()
        import datetime
        specs = {
            'campaign': campaign,
            'abm_status': 1 if rfq.abm_status is True else 0,
            'suppression_status': 1 if rfq.suppression_status is True else 0,
            'abm_file_content': rfq.abm_file_content,
            'abm_count': len(eval(rfq.abm_file_content)) if rfq.abm_file_content else 0,
            'suppression_file_content': rfq.suppression_file_content,
            'suppression_count': len(eval(rfq.suppression_file_content)) if rfq.suppression_file_content else 0,
        }
        industry_type = ''
        company_size = ''
        revenue_size = ''
        if rfq.industry_type is not None:
            industry_type=','.join(eval(rfq.industry_type)) if rfq.industry_type.strip() != '' else ''
        if rfq.company_size is not None:
            company_size=', '.join(eval(rfq.company_size)) if rfq.company_size.strip() != '' else ''
        if rfq.revenue_size is not None:
            revenue_size=', '.join(eval(rfq.revenue_size)) if rfq.revenue_size.strip() != '' else ''
        mapping = {
            'campaign': campaign,
            'industry_type': industry_type,
            'special_instructions': rfq.special_instructions,
            'job_title': rfq.job_title_function,
            'job_title_function': rfq.job_title_function,
            'country': rfq.geo,
            'company_size': company_size,
            'revenue_size':revenue_size,
            'custom_question': rfq.custom_question,
        }
        from campaign_mapping.views.campaign_mapp import create_selected_component_obj
        # create objects for selected campaign specs...Akshay G...Date - 13th Nov, 2019
        specification_obj = Specification.objects.create(**specs)
        if specification_obj.abm_status:
            create_selected_component_obj('account_based_marketing', campaign)
        if specification_obj.suppression_status:
            create_selected_component_obj('account_supression', campaign)
        mapping_obj = Mapping.objects.create(**mapping)
        if mapping_obj.custom_question > 0:
            create_selected_component_obj('custom_field', campaign)
        if mapping_obj.revenue_size:
            create_selected_component_obj('revenue_size', campaign)
        component_list = [
            d.function_name
            for d in LeadValidationComponents.objects.filter(is_default=True)
        ]
        SelectedLeadValidation.objects.create(
            campaign=campaign, component_list=component_list
        )
        HeaderSpecsValidation.objects.create(campaign=campaign, company_limit=4)
        Terms.objects.create(campaign=campaign)
        Delivery.objects.create(campaign=campaign)

        RFQ_Campaigns.objects.get(id=rfq.id).delete()

    data = {'success': 1}
    return JsonResponse(data)

# === Add new client custom spec for campaign ===
def add_new_spec(request, csv_data = {}):
    # Add new client custom spec for campaign..Akshay G.
    user_id = request.session['userid']
    if len(csv_data) == 0:
        label = request.POST.get('label')
        invoke_div = request.POST.get('invoke_div').lower()
    else:
        label = csv_data['label']
        invoke_div = csv_data['invoke_div'].lower()
    custom_spec_query = Client_custom_specs.objects.filter(user_id = user_id, invoke_div = invoke_div)
    if custom_spec_query:
        return JsonResponse({'status': 'failure'})
    else:
        last_positon = 0
        if Client_custom_specs.objects.all():
            last_positon = Client_custom_specs.objects.latest('id').position
        Client_custom_specs.objects.create(user_id =user_id, invoke_div = invoke_div, label = label, position = last_positon+1)
    return JsonResponse({'status': 'success'})

# === Save Custome Specs Data ===
def save_custom_specs_data(request, csv_data = {}):
    # save data of client custom spec for campaign..Akshay G.
    if len(csv_data) == 0:
        div_id = request.POST.get('div_id').lower()
        specs_data = request.POST.get('specs_data')
    else:
        div_id = csv_data['div_id'].lower()
        specs_data = csv_data['specs_data']
    user_id = request.session['userid']
    custom_spec_obj = Client_custom_specs.objects.get(user_id = user_id, invoke_div = div_id)
    saved_data = []
    if custom_spec_obj.data != None:
        saved_data = ast.literal_eval(custom_spec_obj.data) if custom_spec_obj.data.strip() != '' else []
    if specs_data.lower() in [i.lower() for i in saved_data]:
        return JsonResponse({'status':'error'})
    else:
        saved_data.append(specs_data)
        custom_spec_obj.data = saved_data
        custom_spec_obj.save()
    return JsonResponse({'status':'succsss'})

# === Save Selected Custom Specs Data ===
def save_selected_custom_specs_data(request):
    # save selected data of custom specs of campaign for client...Akshay G.
    user_id = request.session['userid']
    div_id = request.POST.get('div_id')
    data = request.POST.get('data')
    camp_id = request.POST.get('camp_id')
    other_specs_obj = Campaign_others_specs.objects.get_or_create(user_id = user_id, campaign_id = camp_id)[0]
    other_specs = []
    if other_specs_obj.others_specs != None:
        other_specs = eval(other_specs_obj.others_specs) if other_specs_obj.others_specs.strip() != '' else []
    if len(other_specs) == 0:
        other_specs.append({div_id: data})
    else:
        other_specs[0][div_id] = data
    other_specs_obj.others_specs = other_specs
    other_specs_obj.save()
    return JsonResponse({})

# === Remove Custom Specs ===
def remove_custom_specs(request):
    # remove custom spec from selected specs list of campaign for client..Akshay G.
    user_id = request.session['userid']
    div_id = request.POST.get('div_id')
    camp_id = request.POST.get('camp_id')
    other_specs_query = Campaign_others_specs.objects.filter(user_id = user_id, campaign_id = camp_id)
    if other_specs_query:
        other_specs = []
        other_specs_obj = other_specs_query[0]
        if other_specs_obj.others_specs != None:
            other_specs = eval(other_specs_obj.others_specs) if other_specs_obj.others_specs.strip() != '' else []
        if len(other_specs) > 0:
            other_specs[0].pop(div_id, None)
        other_specs_obj.others_specs = other_specs
        other_specs_obj.save()
    return JsonResponse({})

# === Add Custom Specs ===
def add_custom_specs(request):
    # add custom spec into selected specs list of campaign for client..Akshay G.
    user_id = request.session['userid']
    div_id = request.POST.get('div_id')
    camp_id = request.POST.get('camp_id')
    other_specs_obj = Campaign_others_specs.objects.get_or_create(user_id = user_id, campaign_id = camp_id)[0]
    other_spec = []
    if other_specs_obj.others_specs != None:
        other_spec = eval(other_specs_obj.others_specs) if other_specs_obj.others_specs.strip() != '' else []
    if len(other_spec) > 0:
        other_spec[0][div_id] = ''
    else:
        other_spec.append({div_id: ''})
    other_specs_obj.others_specs = other_spec
    other_specs_obj.save()
    return JsonResponse({})

# add states & cities dynamically through ajax after selecting country or state resp...Akshay G.
def update_city_state_list(request):
    from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
    spec_id = request.POST.get('spec_id')
    campaign_id = eval(request.POST.get('camp_id'))
    selected_values = eval(request.POST.get('selected_values'))
    mapping = Mapping.objects.get(campaign_id=campaign_id)
    use_as_text = UseAsTxtMapping.objects.filter(campaign_id = campaign_id)
    use_as_text_dict = {}
    if spec_id == 'country':
        selected_countries = selected_values
        all_states = []
        if len(selected_countries) > 0:
            all_states = states1.objects.filter(country__name__in = selected_countries)
        selected_states = []
        unmapped_states = []
        if mapping.state is not None:
            if mapping.state.strip() != '':
                selected_states = mapping.state.split(',') if ',' in mapping.state else [mapping.state]
        if len(all_states) > 0 and len(selected_states) > 0:
            for state in selected_states:
                query = all_states.filter(name__iexact = state.strip())
                if len(query) == 0 and state.strip() != '':
                    unmapped_states.append(state)
        else:
            unmapped_states = selected_states
        all_states = list(all_states.values_list('name', flat = True)) if len(all_states) > 0 else []
        for obj in use_as_text:
            if obj.original_txt in all_states or obj.original_txt in unmapped_states:
                use_as_text_dict[obj.original_txt] = obj.alternative_txt
        # code commented for paginated states display
        # page = request.POST.get('page', 1)
        # # Set no of objects to be fetch on each ajax call to 'paginator'..here 3
        # paginator = Paginator(all_states, 50)
        # try:
        #     states_data = paginator.page(page)
        #     # logos = return_company_logo(notifs_details.object_list, userid)
        # except PageNotAnInteger:
        #     states_data = paginator.page(1)
        #     # logos = return_company_logo(notifs_details.object_list, userid)
        # except EmptyPage:
        #     states_data = paginator.page(paginator.num_pages)
        data = {'all_values': all_states, 'unmapped_values': unmapped_states, 'use_as_text_dict': use_as_text_dict,'selected_values': selected_states}
    else:
        selected_states = selected_values
        all_cities = []
        if len(selected_states) > 0:
            all_cities = cities1.objects.filter(state__name__in = selected_states)
        selected_cities = []
        unmapped_cities = []
        if mapping.city is not None:
            if  mapping.city.strip() != '':
                selected_cities = mapping.city.split(',') if ',' in mapping.city else [mapping.city]
        if len(all_cities) > 0 and len(selected_cities) > 0:
            for city in selected_cities:
                query = all_cities.filter(name__iexact = city.strip())
                if len(query) == 0 and city.strip() != '':
                    unmapped_cities.append(city)
        else:
            unmapped_cities = selected_cities
        all_cities = list(all_cities.values_list('name', flat = True)) if len(all_cities) > 0 else []
        for obj in use_as_text:
            if obj.original_txt in all_cities or obj.original_txt in unmapped_cities:
                use_as_text_dict[obj.original_txt] = obj.alternative_txt
        data = {'all_values': all_cities, 'unmapped_values': unmapped_cities, 'use_as_text_dict': use_as_text_dict, 'selected_values': selected_cities}
    return JsonResponse(data)
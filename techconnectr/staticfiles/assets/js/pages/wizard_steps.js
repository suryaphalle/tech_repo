/* ------------------------------------------------------------------------------
*
*  # Steps wizard
*
*  Specific JS code additions for wizard_steps.html page
*
*  Version: 1.1
*  Latest update: Dec 25, 2015
*
* ---------------------------------------------------------------------------- */

$(function () {

    var datamsg = "";

    // Wizard examples
    // ------------------------------

    // Basic wizard setup
    $(".steps-basic").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        titleTemplate: '<span class="number">#index#</span> #title#',
        labels: {
            finish: 'Submit'
        },
        onFinished: function (event, currentIndex) {
            alert("Form submitted.");
        }
    });


    // Async content loading
    $(".steps-async").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        titleTemplate: '<span class="number">#index#</span> #title#',
        labels: {
            finish: 'Submit'
        },
        onContentLoaded: function (event, currentIndex) {
            $(this).find('select.select').select2();

            $(this).find('select.select-simple').select2({
                minimumResultsForSearch: Infinity
            });

            $(this).find('.styled').uniform({
                radioClass: 'choice'
            });

            $(this).find('.file-styled').uniform({
                fileButtonClass: 'action btn bg-warning'
            });
        },
        onFinished: function (event, currentIndex) {
            alert("Form submitted.");
        }
    });

    $(".steps-starting-step").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        saveState: true,
        titleTemplate: '<span class="number">#index#</span> #title#',
        autoFocus: true,
        onFinished: function (event, currentIndex) {
           
        }
    });
    // Saving wizard state
    $(".steps-state-saving").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        saveState: true,
        titleTemplate: '<span class="number">#index#</span> #title#',
        autoFocus: true,
        onFinished: function (event, currentIndex) {

           

        }
    });


    // Specify custom starting step



    //
    // Wizard with validation
    //

    // Show form
    var form = $(".steps-validation").show();


    // Initialize wizard
    $(".steps-validation").steps({
        headerTag: "h6",
        bodyTag: "fieldset",
        transitionEffect: "fade",
        titleTemplate: '<span class="number">#index#</span> #title#',
        autoFocus: true,
        onInit: function (event, current) {
            $('.actions > ul > li:first-child').attr('style', 'display:none');
        },
        onStepChanging: function (event, currentIndex, newIndex) {

            // Allways allow previous action even if the current form is not valid!
            if (currentIndex > newIndex) {
                return true;
            }
            if (currentIndex === 0) {
                if (checkErrorCompany()) {
                    saveDataInDatabase()
                    return true;
                }
                else {
                    return false;
                }
            }
            else if (currentIndex == 1) {
                if (checkErrorBussiness()) {
                    saveDataInDatabase()
                    return true;
                }
                else {
                    return false;
                }
            } else if (currentIndex == 2) {
                if (checkErrorData()) {
                    saveDataInDatabase();
                    return true;
                }
                else {
                    return false;
                }
            } else if (currentIndex == 3) {
                saveDataInDatabase()
                return true;
            }



            // Forbid next action on "Warning" step if the user is to young
            if (newIndex === 3 && Number($("#age-2").val()) < 18) {
                return false;
            }

            // Needed in some cases if the user went back (clean up)
            if (currentIndex < newIndex) {

                // To remove error styles
                form.find(".body:eq(" + newIndex + ") label.error").remove();
                form.find(".body:eq(" + newIndex + ") .error").removeClass("error");
            }

            form.validate().settings.ignore = ":disabled,:hidden";
            return form.valid();
        },

        onStepChanged: function (event, currentIndex, priorIndex) {
            if (currentIndex > 0) {
                $('.actions > ul > li:first-child').attr('style', '');
            } else {
                $('.actions > ul > li:first-child').attr('style', 'display:none');
            }
            // Used to skip the "Warning" step if the user is old enough.
            if (currentIndex === 2 && Number($("#age-2").val()) >= 18) {

                form.steps("next");
            }

            // form.steps("previous");
            // Used to skip the "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 1 && priorIndex === 3) {

                form.steps("previous");
            }
        },

        onFinishing: function (event, currentIndex) {
            saveDataInDatabase()
            form.validate().settings.ignore = ":disabled";
            return form.valid();
        },

        onFinished: function (event, currentIndex) {
            //alert("Submitted!");
            swal({
                title: "Done! ",
                text: "Great, Data has been saved successfully",
                icon: "success",
                buttons: false,
            });

            location.href = site;
        }
    });


    function checkErrorData() {

            // changes for saving trimmed data - amey -20 june 2019
    $('#comp_overview').text($.trim($('#comp_overview').val()));
      $('#unique_value_prop').text($.trim($('#unique_value_prop').val()));
      $('#size_us').val($.trim($('#size_us').val()));
      $('#overall_size').val($.trim($('#overall_size').val()));
      $('#optin').val($.trim($('#optin').val()));


        if ($("._language_").val() == '') {
            $("._language_").addClass('validation_error');
            return false;
        }
        else if (language_()) {
            return false;
        }
        else {
            $("._language_").removeClass('validation_error');
            return true;
        }
    }
    function language_() {
        var lang = $("._language_").val();
        var flag = false;

        if (jQuery('._language_').length) {
            for (var i = 0; i < lang.length; i++) {
                if (lang[i] != ' ') {
                    flag = true;
                }
            }
            if (flag == false) {
                $("._language_").addClass('validation_error');
                return true;
            }
            else {
                return false;
            }
        }
        else {
            return false;
        }
    }

        // changes for saving trimmed data - amey -20 june 2019
    function checkErrorCompany() {
        //if company_name null that time
        if ($.trim($("#company_name").val()).length === 0) {
          $("#company_name").val('');
            $("#company_name").addClass('validation_error');
            return false;
        } else {
            $("#company_name").val($.trim($("#company_name").val()));
            $("#company_name").removeClass('validation_error');
        }

        // if Website null that time
        // if ($('#website').val().length === 0) {
        //     $("#website").addClass('validation_error');
        //     return false;
        // }
        // else {
        //     $("#website").removeClass('validation_error');

        // }

        //if Website format validation
        //var url = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
        var url = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-zA-Z\u00a1-\uffff0-9]-*)*[a-zA-Z\u00a1-\uffff0-9]+)(?:\.(?:[a-zA-Z\u00a1-\uffff0-9]-*)*[a-zA-Z\u00a1-\uffff0-9]+)*(?:\.(?:[a-zA-Z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;
        var urlexp = new RegExp(url)
        // var urlexp = new RegExp("(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})");
        var txtfield = $.trim($('#website').val());
        if (urlexp.test(txtfield)) {
            $('#website').val($.trim($('#website').val()))
            $("#website").removeClass('validation_error');
        }
        else {

            $("#website").addClass('validation_error');
            return false;
        }

        //if address_line1 null that time
        if ($.trim($("#address_line1").val()).length === 0) {
          $("#address_line1").val('')
            $("#address_line1").addClass('validation_error');
            return false;
        } else {
          $("#address_line1").val($.trim($("#address_line1").val()))
            $("#address_line1").removeClass('validation_error');

        }



        //if country null that time
        if ($("#country").val() == "0" || $("#country").val() == '') {
            $("#country_error").addClass('validation_error');
            return false;
        } else {
            $("#country_error").removeClass('validation_error');
        }

        //if vendor_type null that time
        if ($("#vendor_type").val() == "" || $("#vendor_type").val() == null) {
            $("#vendor_error").addClass('validation_error');
            return false;
        } else {
            $("#vendor_error").removeClass('validation_error');

        }

        //if industry_type_id null that time
        if ($("#industry_type_id").val() == "" || $("#industry_type_id").val() == null) {
            $("#industry_error").addClass('validation_error');
            return false;
        } else {
            $("#industry_error").removeClass('validation_error');
        }
        if ($.trim($("#phone").val()).length === 0){
        $("#phone").val('')
            $("#phone").addClass('validation_error');
            return false;
        } else {
          $("#phone").val($.trim($("#phone").val()))
            $("#phone").removeClass('validation_error');
        }

        $("#alternate_number").val($.trim($("#alternate_number").val()));


        //if state null that time
        if ($("#state").val() == "0" || $("#state").val() == '') {
            $("#state_error").addClass('validation_error');
            return false;
        } else {
            $("#state_error").removeClass('validation_error');
        }

        //if address_line2 null that time
        $("#address_line2").val($.trim($("#address_line2").val()));

        //if industry_speciality_id null that time
        if ($("#industry_speciality_id").val() == "" || $("#industry_speciality_id").val() == null) {
            $("#speciality_error").addClass('validation_error');
            return false;
        } else {
            $("#speciality_error").removeClass('validation_error');
        }
        if ($("#cities").val() == 0 || $("#cities").val() == '') {
            $("#city_error").addClass('validation_error');
            return false;
        } else {
            $("#city_error").removeClass('validation_error');
        }

        if ($.trim($("#zip_code").val()).length === 0) {
          $("#zip_code").val('')
            $("#zip_code").addClass('validation_error');
            return false;
        } else {
            var regex = new RegExp("^[a-zA-Z0-9]+$");
            var str = $.trim($("#zip_code").val())
            if (regex.test(str)) {
              $("#zip_code").val($.trim($("#zip_code").val()))
                $("#zip_code").removeClass('validation_error');
                // return true;
            } else {
                $("#zip_code").addClass('validation_error');
                return false;
            }
        }
        return true;
    }
    
    // changes for saving trimmed data - amey -20 june 2019
    function checkErrorBussiness() {
        // var urlexp = new RegExp('^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$');
        var url = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        var urlexp = new RegExp(url)
        if ($.trim($("#primary_name").val()).length === 0) {
          $("#primary_name").val('')
            $("#primary_name").addClass('validation_error');
            return false;
        } else {
          $("#primary_name").val($.trim($("#primary_name").val()))
            $("#primary_name").removeClass('validation_error');
        }

        if ($.trim($("#primary_directdial").val()).length === 0) {
          $("#primary_directdial").val('')
            $("#primary_directdial").addClass('validation_error');
            return false;
        } else {
          $("#primary_directdial").val($.trim($("#primary_directdial").val()))
            $("#primary_directdial").removeClass('validation_error');
        }


        if ($.trim($("#primary_designation").val()).length === 0) {
          $("#primary_designation").val('')
            $("#primary_designation").addClass('validation_error');
            return false;
        } else {
          $("#primary_designation").val($.trim($("#primary_designation").val()))
            $("#primary_designation").removeClass('validation_error');
        }
        var txtfield = $.trim($("#primary_email").val());
        var sample_mails = ['yahoo.com','hotmail.com','aol.com','gmail.com','msn.com','comcast.net','hotmail.co.uk','sbcglobal.net','yahoo.co.uk','yahoo.co.in','bellsouth.net','verizon.net','earthlink.net','cox.net','rediffmail.com','yahoo.ca','btinternet.com','charter.net','shaw.ca','ntlworld.com'];
        if (urlexp.test(txtfield)) {
            // validate for bussiness email address..Akshay G.
            if (jQuery.inArray(txtfield.split('@')[1], sample_mails) == -1){
                $("#primary_email").val($.trim($("#primary_email").val()))
                $("#primary_email").removeClass('validation_error');
            }
            else{
                $("#primary_email").val('');
                $("#primary_email").addClass('validation_error');
                return false;
            }
        }
        else {
          $("#primary_email").val('')
            $("#primary_email").addClass('validation_error');
            return false;
        }

        if ($.trim($("#secondary_name").val()).length === 0) {
          $("#secondary_name").val('')
            $("#secondary_name").addClass('validation_error');
            return false;
        } else {
          $("#secondary_name").val($.trim($("#secondary_name").val()))
            $("#secondary_name").removeClass('validation_error');
        }

        if ($.trim($("#secondary_directdial").val()).length === 0) {
          $("#secondary_directdial").val('')
            $("#secondary_directdial").addClass('validation_error');
            return false;
        } else {
          $("#secondary_directdial").val($.trim($("#secondary_directdial").val()))
            $("#secondary_directdial").removeClass('validation_error');
        }
        if ($.trim($("#secondary_designation").val()).length === 0) {
          $("#secondary_designation").val('')
            $("#secondary_designation").addClass('validation_error');
            return false;
        } else {
          $("#secondary_designation").val($.trim($("#secondary_designation").val()))
            $("#secondary_designation").removeClass('validation_error');
        }
        var txtfield = $.trim($("#secondary_email").val());
        if (urlexp.test(txtfield)) {
            // validate for bussiness email address..Akshay G.
            if (jQuery.inArray(txtfield.split('@')[1], sample_mails) == -1){
                $("#secondary_email").val($.trim($("#secondary_email").val()));
                $("#secondary_email").removeClass('validation_error');
            }
            else{
                $("#secondary_email").val('');
                $("#secondary_email").addClass('validation_error');
                return false;
            }
        }
        else {
          $("#secondary_email").val('')
            $("#secondary_email").addClass('validation_error');
            return false;
        }
        return true;

    }
    //ajax function to save data in saveDataInDatabase
    function saveDataInDatabase() {
        var formData = new FormData($("#onboradingform")[0])
        token = $("#onboradingform").find('input[name=csrfmiddlewaretoken]').val();
        $.ajax({
            type: 'post',
            url: '/vendor/onBording/',
            data: formData,
            contentType: false,
            processData: false,
            success: function (data) {
                if (data.success == 1) {
                    datamsg = data.msg;
                    site = data.site_url;
                    return true;
                }
            }
        });
        return true;
    }
    jQuery.validator.addMethod("files", function (value, element) {

        return (value != '' || $('.logo').length > 0)
    }, "This field is required.");


    // Initialize validation
    $(".steps-validation").validate({
        ignore: 'input[type=hidden], .select2-search__field', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
        },

        // Different components require proper error label placement
        errorPlacement: function (error, element) {

            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }

            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            }
            else {
                error.insertAfter(element);
            }
        },
        rules: {
            email: {
                email: true,
            },
            // company_logo: {
            //     files: true,
            // }
        }
    });



    // Initialize plugins
    // ------------------------------

    // Select2 selects
    $('.select').select2();


    // Simple select without search
    $('.select-simple').select2({
        minimumResultsForSearch: Infinity
    });


    // Styled checkboxes and radios
    $('.styled').uniform({
        radioClass: 'choice'
    });


    // Styled file input
    $('.file-styled').uniform({
        fileButtonClass: 'action btn bg-blue'
    });

});
function readURL(input) {

    if (input.files && input.files[0]) {

        //   document.getElementById('blah').style.display = "none";
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah')
                .attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

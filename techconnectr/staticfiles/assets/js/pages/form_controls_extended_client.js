/* ------------------------------------------------------------------------------
*
*  # Extended form controls
*
*  Specific JS code additions for form_controls_extended.html page
*
*  Version: 1.2
*  Latest update: Jul 4, 2016
*
* ---------------------------------------------------------------------------- */

$(function() {


    // Maxlength
    // ------------------------------

    // Basic example
    $('.maxlength').maxlength();

    // Threshold
    $('.maxlength-threshold').maxlength({
        threshold: 15
    });

    // Custom label color
    $('.maxlength-custom').maxlength({
        threshold: 10,
        warningClass: "label label-primary",
        limitReachedClass: "label label-danger"
    });

    // Options
    $('.maxlength-options').maxlength({
        alwaysShow: true,
        threshold: 10,
        warningClass: "text-success",
        limitReachedClass: "text-danger",
        separator: ' of ',
        preText: 'You have ',
        postText: ' chars remaining.',
        validate: true
    });



    // Label position
    $('.maxlength-label-position').maxlength({
        alwaysShow: true,
        placement: 'top'
    });

});

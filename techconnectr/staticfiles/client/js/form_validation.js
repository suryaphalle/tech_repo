/* ------------------------------------------------------------------------------
*
*  # Form validation
*
*  Specific JS code additions for form_validation.html page
*
*  Version: 1.3
*  Latest update: Feb 5, 2016
*
* ---------------------------------------------------------------------------- */
let rev_cmp_size = 0
$(function () {

    $("#id_method").on("change", function() {  // whenever a selection is made
        $(this).valid(); // force validation in order to remove any messages
    });
    $('#id_industry_type').on("change", function() {  // whenever a selection is made
        $(this).valid(); // force validation in order to remove any messages
    });
    $('#id_company_size').on("change", function() {  // whenever a selection is made
        rev_cmp_size = 1
        $('#id_revenue_size').valid()
        $(this).valid(); // force validation in order to remove any messages
    });
    $('#id_revenue_size').on("change", function() {  // whenever a selection is made
        rev_cmp_size = 2
        $('#id_company_size').valid()
        $(this).valid(); // force validation in order to remove any messages
    });
    $('#id_type').on("change", function() {  // whenever a selection is made
        $(this).valid(); // force validation in order to remove any messages
    });


    // $('.region_country_submite').on("click", function() {  // whenever a selection is made
    //     $('#sweet_spot').valid(); // force validation in order to remove any messages
    // });
    // Form components
    // ------------------------------



    // Setup validation
    // ------------------------------
    jQuery.validator.addMethod("noSpace", function(value, element) {
        return (value.trim() == value);
    }, "No blankspaces are allowed at start and end.");

    // company_size and revenue size validations
    jQuery.validator.addMethod("fieldCheck", function(value, element) {
        if ($('#id_company_size').val() != null || $('#id_revenue_size').val() != null){

            return (rev_cmp_size != 0);
        } else {
            return false
        }
    }, "Either of Employee or Revenue size is required");

    //custome question validations
    jQuery.validator.addMethod("custom_check", function(value, element) {
        // return (value.trim() == value);
        if($(".cut_check").prop('checked') == true){
            val = $('.cusome_ques_value').val()
            // if ($('.cusome_ques_value').data('old-no')) {
            //     old_no = $('.cusome_ques_value').data('old-no')
            //     if (parseInt(val) >= old_no && parseInt(val) > 0){
            //         return true;
            //     }
            // }else {
                if (val != "" && (val > 0 && val <= 10)){
                    return true;
                }
            // }
            return false;
        }else{
            val = $('.cusome_ques_value').val(0)
            return true;
        }
    }, "Invalid input, Please enter valid number of custom questions within range 1 to 10.");


    jQuery.validator.addMethod("abm_check", function(value, element) {

        if($("input[name='abm_status']").prop('checked')){
          if($("input[name='abm_company_file']").val() != '' || $('#abm_url').length > 0){
            return true;
          }
        }
        else{
          if($("input[name='abm_company_file']").val() == ''){
            return true;
          }
        }
    }, "This field is required.");

    jQuery.validator.addMethod("supp_check", function(value, element) {

        if($("input[name='suppression_status']").prop('checked')){
          if($("input[name='suppression_company_file']").val() != '' || $('#supp_url').length > 0){
            return true;
          }
        }
        else{
          if($("input[name='suppression_company_file']").val() == ''){
            return true;
          }
        }
    }, "This field is required.");

    // Initialize
    var validator = $("#validate_campaign").validate({
        ignore: '#id_tc_target_quantity,#id_target_quantity,#id_cpl,.target_ignore,.flatpickerEnd,#adhoc,#id_user,#id_priority,#id_rfq_status,#id_raimainingleads,#id_status,#id_rfq,.cpl_ignore,.flatpickerStart, .select2-search__field,.cls_io_number,#id_adhoc,.spins', // ignore hidden fields
        errorClass: 'validation-error-label',
        successClass: 'validation-valid-label',
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).parent().find(".errorlist").remove(); // newly added
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass);
            $(element).parent().find(".errorlist").remove(); // newly added
        },

        // Different components require proper error label placement
        errorPlacement: function (error, element) {
            $('#sweet_spot-error').addClass('hidden');
            // Styled checkboxes, radios, bootstrap switch
            if (element.parents('div').hasClass("checker") || element.parents('div').hasClass("choice") || element.parent().hasClass('bootstrap-switch-container')) {
                if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                    error.appendTo(element.parent().parent().parent().parent());
                }
                else {
                    error.appendTo(element.parent().parent().parent().parent().parent());
                }
            }

            // Unstyled checkboxes, radios
            else if (element.parents('div').hasClass('checkbox') || element.parents('div').hasClass('radio')) {
                error.appendTo(element.parent().parent().parent());
            }


            else if (element.hasClass('multiselect-campaign-method') || element.hasClass('multiselect-campaign-method1')) {
                // custom placement for hidden select
                error.insertAfter(element.parent())

            }

            else if(element.hasClass('multiselect-campaign-method2') || element.hasClass('multiselect-campaign-method3')){
                error.insertAfter(element.parent().parent())
            }
            // Input with icons and Select2
            else if (element.parents('div').hasClass('has-feedback') || element.hasClass('select2-hidden-accessible')) {
                error.appendTo(element.parent());
            }

            // Inline checkboxes, radios
            else if (element.parents('label').hasClass('checkbox-inline') || element.parents('label').hasClass('radio-inline')) {
                error.appendTo(element.parent().parent());
            }

            // Input group, styled file input
            else if (element.parent().hasClass('uploader') || element.parents().hasClass('input-group')) {
                error.appendTo(element.parent().parent());
            }

            else if (element.hasClass('start_date_selector') || element.hasClass('end_date_selector')) {
                error.appendTo(element.parent().parent());
            }


            else {
                error.insertAfter(element);
            }
        },
        validClass: "validation-valid-label",
        success: function (label) {

            label.addClass("validation-valid-label").text("Success.")
        },
        rules: {
            name: {
              noSpace: true,
              minlength: 5
            },
            description: {
              noSpace: true,
            },
            job_title_function: {
              noSpace: true,
            },
            company_size: {
                fieldCheck:true
            },
            repeat_password: {
                equalTo: "#password"
            },
            custom_question:{
                custom_check:true
            },
            // abm_company_file:{
            //   abm_check:true
            // },
            // suppression_company_file:{
            //   supp_check:true
            // },
            email: {
                email: true
            },
            repeat_email: {
                equalTo: "#email"
            },
            minimum_characters: {
                minlength: 10
            },
            maximum_characters: {
                maxlength: 10
            },
            minimum_number: {
                min: 10
            },
            maximum_number: {
                max: 10
            },
            number_range: {
                range: [10, 20]
            },
            url: {
                url: true
            },
            date: {
                date: true
            },
            date_iso: {
                dateISO: true
            },
            numbers: {
                number: true
            },
            digits: {
                digits: true
            },
            creditcard: {
                creditcard: true
            },
            basic_checkbox: {
                minlength: 2
            },
            styled_checkbox: {
                minlength: 2
            },
            switchery_group: {
                minlength: 2
            },
            switch_group: {
                minlength: 2
            }
        },
        messages: {
            custom: {
                required: "This is a custom error message",
            },
            agree: "Please accept our policy"
        },
        submitHandler: function (form) {

            // do other things for a valid form
            // check is one of the checkbox selected
            if (jQuery("#container_vendor_type input[type=checkbox]:checked").length) {
                console.log(rev_cmp_size)
                form.submit();
            }
            else {
                message = "Please select at least one of the vendor type to submit form"
                console.log(message);
                //alert(message)
                $("#vendor_error_msg").removeClass("hidden")
            }

        }
    });



    // Reset form
    $('#reset').on('click', function () {
        validator.resetForm();
    });

});

$(function () {
  $(document).on('change', '.checkbox_container input[type="checkbox"]', function () {
    //$('input[name="' + this.name + '"]').not(this).prop('checked', false);
    var current_checkbox = this;
    // check if it's a client custom spec..Akshay G.
    if ($(this).closest('.beefup__body').hasClass('custom_specs')){
      custom_specs_validation($(this), false);
      return;
    }
    var checkbox_name = this.name;
    var selected_values = [];
    if (checkbox_name == 'region_check') {
      $(".div_loader").fadeIn('fast')
    }
    // iterate in all chckbox with same name to get clicked values
    if (checkbox_name != 'region_check') {
      $('input[name="' + checkbox_name + '"]').each(function () {
        console.dir("this.value : " + this.value);
        if ($(this).is(':checked')) {
          console.dir("Checked");
          console.dir(this.value);

          selected_values.push(this.value);
          // push values into array
        }
        if(this.className == 'custome_specs'){
          if (!$(this).is(':checked')) {
            $(document).find(`.${$(this).val()}`).addClass('hidden')
          }
        }
      });
      if ($('input[name="' + checkbox_name + '"]:checked').length == $('input[name="' + checkbox_name + '"]').length) {
        $(current_checkbox).parents('article').find('.chk_bx_select_all').prop('checked', true)
      } else {
        $(current_checkbox).parents('article').find('.chk_bx_select_all').prop('checked', false)
      }
      if (checkbox_name == "country[]") {

        region_id = $(this).attr('class').split('_')[1]
        if ($('#country_table').find(`.country_${region_id}:checked`).length > 0) {
          $('#country_table').find(`#region_${region_id}`).prop('checked', true)
        } else {
          $('#country_table').find(`#region_${region_id}`).prop('checked', false)
        }
      }
      // each end
      checked_values_as_str(selected_values, current_checkbox);
    }
    else {
      $(this).prop('disabled', true);
      if ($(this).is(':checked')) {
        $('#country_table').find(`.country_${$(this).data('txt')}`).each(function () {
          $(this).prop('checked', true)
        })
      } else {
        $('#country_table').find(`.country_${$(this).data('txt')}`).each(function () {
          $(this).prop('checked', false)
        })
      }
      $('input[name="country[]"]').each(function () {
        if ($(this).is(':checked')) {
          selected_values.push(this.value);
        }
      })
      $(this).prop('disabled', false);
      checked_values_as_str(selected_values, current_checkbox);
      if ($('input[name="' + checkbox_name + '"]:checked').length == $('input[name="' + checkbox_name + '"]').length) {
        $(current_checkbox).parents('article').find('.chk_bx_select_all').prop('checked', true)
      } else {
        $(current_checkbox).parents('article').find('.chk_bx_select_all').prop('checked', false)
      }
      $(".div_loader").fadeOut('slow');
    }
    // // add city & states..Akshay G.
    // if ($(this).closest('.component_data').attr('id') == 'country' || $(this).closest('.component_data').attr('id') == 'state_id'){
    //   
    // }
  });

  // select
  $(document).on('click', '.chk_bx_select_all', function (e) {
    var selected_values = [];
    if ($(this).is(':checked')) {
      // checking all checkboxes
      // DEBUG && console.log('Selecting all checkboes of current checkbox container');
      $(this).closest(".beefup__body").find(".checkbox_container").find("input[type='checkbox']").prop('checked', true);
    }
    else {
      // deselecing all checkboxes
      // DEBUG && console.log('Removing selection of all checkboes of current checkbox container');
      $(this).closest(".beefup__body").find(".checkbox_container").find("input[type='checkbox']").prop('checked', false);
    }
    // check if it's a client custom spec..Akshay G.
    if ($(this).closest('.beefup__body').hasClass('custom_specs')){
      custom_specs_selectAll_validation($(this));
      return 
    }
    // iterate in all chckbox with same name to get clicked values
    $(this).closest(".beefup__body").find(".checkbox_container").find("input[type='checkbox']").each(function () {
      console.dir("this.value : " + this.value);
      if ($(this).is(':checked')) {
        DEBUG && console.dir("Checked");
        DEBUG && console.dir(this.value);
        if (this.className != "region_check"){
          selected_values.push(this.value);
        }
        // push values into array
      }
    });
    // each end
    checked_values_as_str(selected_values, this);
    // add city & states..Akshay G.
    // if ($(this).closest('.component_data').attr('id') == 'country' || $(this).closest('.component_data').attr('id') == 'state_id'){
    //   
    // }
  });

  // ad-hoc checkbox
  $(document).on('change', '.chk_put_vals', function () {
    //$('input[name="' + this.name + '"]').not(this).prop('checked', false);
    DEBUG && console.log(" chk_put_vals checkbox clicked");

    var value = undefined;
    if ($(this).is(':checked')) {
      DEBUG && console.dir("checkbox is checked");
      value = 1;
    }
    else {
      value = 0;
    }
    var hidden_input = $(this).parent().find('input[type="hidden"]')[0];
    $(hidden_input).val(value);
    DEBUG && console.log("value stored in hidden textbox : ", $(hidden_input).val())

  });
});

function checked_values_as_str(selected_values, current_element) {
  DEBUG && console.dir(selected_values);
  var concatcated_str = selected_values.join();
  DEBUG && console.dir("concatcated_str");
  DEBUG && console.dir(concatcated_str);
  //store it in input hidden element
  var closet_checkbox_container = $(current_element).closest(".beefup__body").find('.checkbox_container');
  //.find('input[type="hidden"]').value=
  DEBUG && console.dir("closet_checkbox_container : ");
  DEBUG && console.dir(closet_checkbox_container);
  var input_element = $(closet_checkbox_container).find('input[type="hidden"]').first();
  DEBUG && console.dir("input_element : ");
  DEBUG && console.dir(input_element);
  //set value
  $(input_element).val(concatcated_str);
  //check inserted value
  DEBUG && console.log(" Input element value : " + $(input_element).val());

  // trigger change event of input element
  $(input_element).trigger("change");
}



$(document).on('change', '.lead_choice input[type="checkbox"]', function () {
  console.log($(this))
  campaign_id = $('#id_camapaign').val()
  type = $(this).data('type')
  custom_header = 0
  tc_header = 0
  if (type == 'tc_header_status') {
    if ($(this).is(':checked')) {
      tc_header = 1
    } else {
      $('.tc_header_div').find('input[type="checkbox"]').each(function () {
        $(this).prop('checked', false)
        $(this).trigger('change')
      })
    }
  }
  if (type == 'custom_header_status') {
    if ($(this).is(':checked')) {
      custom_header = 1
      campaign_id = $('#id_camapaign').val()
      if (document.getElementById('id_custom_header_status').checked) {
        $(".default_header").toggleClass('hidden')
        window.location = `/client/custom-datafields/${campaign_id}`;
        return false;
      }
    } else {
      $('.custom_header_status_container').empty()
      $(".default_header").toggleClass('hidden')
    }
  }
  $.ajax({
    // context: document.body, // access form inside
    url: '/client/save_header_choice/',
    type: "POST",
    data: { 'campaign_id': campaign_id, 'custom_header': custom_header, 'tc_header': tc_header, 'type': type },
    dataType: 'json',
    success: function (data) {
      console.log(data)
      // location.reload()
      if (data.tc_header == 1) {
        $('#id_tc_header_status').prop('checked', true)
      } else {
        $('#id_tc_header_status').prop('checked', false)
      }
      if (data.custom_header == 1) {
        $('#id_custom_header_status').prop('checked', true)
      } else {
        $('#id_custom_header_status').prop('checked', false)
      }

    },// success end
  })
})

// save selected data of custom spec of campaign for client on selecting checkbox..Akshay G.
function custom_specs_validation(specs_data, flag){
    var checkbox_name = specs_data.attr('name');
    var select_all = $(specs_data).closest('.beefup__body').find('.chk_bx_select_all');
    if ($('input[name="' + checkbox_name + '"]:checked').length == $('input[name="' + checkbox_name + '"]').length){
        $(select_all).prop('checked', true);
    }
    else{
      $(select_all).prop('checked', false);
    }
    // add checked sign span, if element is checked..Akshay G.
    if ($('input[name="' + checkbox_name + '"]:checked').length == 0){
        if($(specs_data).closest('.component_data').find('.specs_status').length > 0){
          $(specs_data).closest('.component_data').find('.specs_status').remove();
        }
    }
    // remove checked sign span, if all elements are unchecked..Akshay G.
    if ($('input[name="' + checkbox_name + '"]:checked').length > 0){
        if($(specs_data).closest('.component_data').find('.specs_status').length == 0){
          $(specs_data).closest('.component_data').find('.beefup__head').append(`
              <span class="specs_status pull-right"><i class="far fa-check-circle chk-color" aria-hidden="true"></i></span>
              `)
        }
    }
    var data_element = specs_data.closest('.checkbox_container').find('.save_data');
    var url = specs_data.closest('.checkbox_container').find('.url_holder_save_data').val();
    var selected_values = data_element.val();
    //flag to check whether function is called by checkbox changes or manual trigger to save data
    // manual trigger- flag=true, checkbox change- flag=false
    if (flag){
        custom_specs_ajax(specs_data.attr('data-div_id'), data_element.val(), url);
        return;
    }
    if (selected_values.trim().length != 0){
        var L = selected_values.split(',');
        if (specs_data.prop('checked')){
            L.push(specs_data.val());
            data_element.val(L.toString());
        }
        else{
          var filteredAry = L.filter(function(e) { return e !== specs_data.val() });
          data_element.val(filteredAry.toString());
        }
    }
    else{
        if (specs_data.prop('checked')){ 
            data_element.val(specs_data.val());
        }
    }
    custom_specs_ajax(specs_data.attr('data-div_id'), data_element.val(), url);
}
// save selected data of custom spec of campaign for client on selecting 'select_all' checkbox..Akshay G.
function custom_specs_selectAll_validation(element){
    var values = [];
    if ($(element).prop('checked')){
        var child_ele = $(element).closest('.beefup__body').find('table input[type="checkbox"]');
        // add checked sign span, if element is checked..Akshay G. 
        if($(element).closest('.component_data').find('.specs_status').length == 0 && child_ele.length != 0){
            $(element).closest('.component_data').find('.beefup__head').append(`
                <span class="specs_status pull-right"><i class="far fa-check-circle chk-color" aria-hidden="true"></i></span>
                `)
        }
        $(element).closest('.beefup__body').find('table input[type="checkbox"]').each(function(){
            values.push($(this).val());
        })
    }
    else{
        // remove checked sign span, if all elements are unchecked..Akshay G.
        if($(element).closest('.component_data').find('.specs_status').length > 0){
          $(element).closest('.component_data').find('.specs_status').remove();
      }
    }
    values = values.toString();
    var url = $(element).closest('.beefup__body').closest('.beefup__body').find('.url_holder_save_data').val();
    var data_element = $(element).closest('.beefup__body').closest('.beefup__body').find('.save_data');
    data_element.val(values);
    custom_specs_ajax(data_element.attr('id'), values, url);
}
// make ajax call to save selected data of custom spec ..AKshay G.
function custom_specs_ajax(div_id, data, url){
    var token = getCookie('csrftoken');
    var camp_id = parseInt($('#id_camapaign').val());
    $.ajax({
        type: 'post',
        url: url,
        data: {'div_id': div_id, 'data': data, 'camp_id': camp_id, 'csrfmiddlewaretoken': token},
        success: function(data){

        },
        error: function(){
            $.confirm({
                title: 'Encountered an server error!',
                content: "Please report this issue to techconnectr technical team.",
                type: 'red',
                typeAnimated: true,
            });
        }
    })
}

$(document).on('click',".state_city_div",function(){
  serach_data = []
  spec_id = $(this).closest('.component_data').attr('id')
  if(!$(this).parent().hasClass('is-open')){
    $('.city_state_loader').fadeIn()
    if (spec_id.split('_id')[0] == 'state'){
      $('input[name="country[]"]:checked').each(function () {
        serach_data.push(this.value);        
      })
      Ajax_add_city_states('country', serach_data)      
    }
    if (spec_id.split('_id')[0] == 'city'){
      $('input[name="state_id[]"]:checked').each(function () {
        serach_data.push(this.value);
      })
      
      Ajax_add_city_states('state_id', serach_data)
    }
    // console.log(spec_id.split('_id')[1],serach_data.length)
  }
  $(this).closest('.table_scroller').addClass('scroll_check')
  $('.scroll_check').on('scroll', function() {
    if($(this).scrollTop() + $(this).innerHeight() >= $(this)[0].scrollHeight) {
        alert('end reached');
    }
    alert()
  })
})

// add city and states dynamically...Akshay G. 
function Ajax_add_city_states(spec_id, selected_values){
    var token = getCookie('csrftoken');
    var camp_id = parseInt($('#id_camapaign').val());
    $.ajax({
        type: 'post',
        url: '/utils/update_city_state_list/',
        data: {'spec_id': spec_id, 'camp_id': camp_id, 'selected_values': JSON.stringify(selected_values), 'csrfmiddlewaretoken': token},
        success: function(data){
          if (serach_key != ''){
            var unmapped_values = data['unmapped_values'];
            var all_values = []
            data['all_values'].filter(function(item) { 
              if (item.toLowerCase().indexOf(serach_key) > -1){all_values.push(item)};
            });
            var use_as_text_dict = data['use_as_text_dict'];
            var selected_values = data['selected_values'];
          } else{
            var unmapped_values = data['unmapped_values'];
            var all_values = data['all_values'].slice(0,60);
            var use_as_text_dict = data['use_as_text_dict'];
            var selected_values = data['selected_values'];
          }
          var c = 0;
          var div;
          var use_as_text_val = '';
          var class_name = '';
          var unchecked_element_count = 0;
          if(spec_id == 'country'){
              var div_id = 'state_id';
          }
          else{
              var div_id = 'city_id';
          }
          var data_element = $('#'+div_id).find('table tbody');
          data_element.children().remove();
          if (Object.keys(use_as_text_dict).length > 0){
              class_name = '';
              $('#'+div_id).find('.chkbx_use_as_txt').prop('checked', true);
          }
          else{
              class_name = 'hidden';
              $('#'+div_id).find('.chkbx_use_as_txt').prop('checked', false);
          }
          all_values.forEach(function(element){
              if(element in use_as_text_dict){
                  use_as_text_val = use_as_text_dict[element]; 
              }
              else{
                  use_as_text_val = '';
              }
              if (jQuery.inArray(element, selected_values) != -1){
                div = spec_checkbox('checked', element, div_id, c, use_as_text_val, class_name);
              }
              else{
                div = spec_checkbox('', element, div_id, c, use_as_text_val, class_name);
                unchecked_element_count += 1;
              }
              data_element.append(div);
              c += 1;
          });
          unmapped_values.forEach(function(element){
              if(element in use_as_text_dict){
                    use_as_text_val = use_as_text_dict[element];
                }
                else{
                    use_as_text_val = '';
                }
              div = spec_checkbox('checked', element, div_id, c, use_as_text_val, class_name);
              data_element.append(div);
          })
          if (unmapped_values.length == 0 && all_values.length == 0){
            // div = spec_checkbox('checked', "element", div_id, c, use_as_text_val, class_name);
            data_element.append('<label>No Result Found.');
          }
          if(unchecked_element_count >= 0){
              $('#'+div_id).find('.chk_bx_select_all').prop('checked', false);
          }
          else{
              $('#'+div_id).find('.chk_bx_select_all').prop('checked', true);
          }
          // $('#more').attr('href',`/utils/update_city_state_list/?page=${data.next_page}`)
          $('.city_state_loader').fadeOut('slow')
        },
        error: function(){
            $.confirm({
                title: 'Encountered an server error!',
                content: "Please report this issue to techconnectr technical team.",
                type: 'red',
                typeAnimated: true,
            });
        }
    })
}

// checkbox row for state and city..Akshay G.
function spec_checkbox(flag, name, div_id, counter, use_as_text_val, class_name){
    return  `
      <tr>
            <td>
                <div class="radio ml-10" data-txt="${name}">
                    <input id="${name}_${counter}"
                        name="${div_id}[]" data-txt="${name}"
                        value="${name}" type="checkbox"
                        ${flag}/>

                    <label for="${name}_${counter}"
                        class="lbl">${name}</label>
                </div>
            </td>
            <td>
                <!-- set data-txt attribute inside above lable to get original text in jquery -->
                <input type='text'
                    class="form-control-inline ${class_name} pull-right txtbx_use_as_txt"
                    value='${use_as_text_val}'>
            </td>
      </tr>
      `
}

// search city and states key
var serach_key = ''
$(document).on('keyup','.search_state_city',function(){
  search_type = $(this).data('type')
  serach_data = []
  serach_key = $(this).val()
  $('.city_state_loader').fadeIn()
  if (search_type == 'state'){
    $('input[name="country[]"]:checked').each(function () {
      serach_data.push(this.value);        
    })
    Ajax_add_city_states('country', serach_data)      
  }
  if (search_type == 'city'){
    $('input[name="state_id[]"]:checked').each(function () {
      serach_data.push(this.value);
    })
    
    Ajax_add_city_states('state_id', serach_data)
  }
})

if(typeof DEBUG === 'undefined')
      var DEBUG =  true;  // false

$(function(){

  // on click of change campaign status button
  // check is mandatory fields are checked
  $(document).on('click', '#btn_change_campaign_status', function(e){
    var abm_status = $('#validate_abm_status___').attr('data-abm_status');
    var res = true; // to stop bulk errors ckheck is function returns true


    //  check datafield is selected --suryakant
    // res  = header_check()

    // check is custome question is valid --suryakant
    if(res)
    {
      if ($('#board-drop').find("div #custom_field").length > 0){
        res = custom_ques_check()
      }
    }
    else
      res = false;

    //check  job title is empty
    if(res)
    {
      if ($('#board-drop').find("#id_job_title").length > 0){
        res = job_title_check()
      }
    }
    else
      res = false;

    // check is industry type selected
    if(res)
    {
      // if abm is selected, make industry type optional..Akshay G.
      if (abm_status != '1'){
        res = validate_element_value({
          field_name:'industry_type',
          error_msg: "Please Select Industry Type ",
        })
      }
    }
    else
      res = false;

    // to stop bulk errors ckheck is function returns true
    // check is Job level selected
    if(res)
    {
      res = validate_element_value({
        field_name:'job_level',
        error_msg: "Please Select Job Level",
      })
    }
    else
      res = false;

    // check is department selected --suryakant
    if(res)
    {
      res = validate_element_value({
        field_name:'job_function',
        error_msg: "Please Select Department",
      })
    }
    else
      res = false;
    // check is Country selected
    if(res)
    {
      res = validate_element_value({
        field_name:'country',
        error_msg: "Please Select Country",
      })
    }
    else
      res = false;

    // check is Company Size selected
    if(res)
    {
      // if abm is selected, make company size optional..Akshay G.
      if (abm_status != '1'){
        res = validate_element_value({
          field_name:'company_size',
          error_msg: "Please Select Employee Size",
        })
      }
    }
    else
      res = false;

    // check is Data Fields selected
    // if(res)
    // {
    //   res = validate_element_value({
    //     field_name:'data_header',
    //     error_msg: "Please Select Data Fields",
    //   })
    // }

    // trigger change status link click
   
    if(res)
    {
      DEBUG && console.log("setting window.location to href of link_change_campaign_status")
      window.location = $("#link_change_campaign_status").attr('href');
    }
    else
    {
      DEBUG && console.log("Problem while triggering link_change_campaign_status click")
    }
  });

});


function validate_element_value(options)
{
  var defaults = {
    field_name:'industry_type',
    err_title: "Encountered an error !",
    error_msg: "Please select field",
    msg_type: "red",
  };

  var params = $.extend({}, defaults, options);

  // console.log(options);
  // console.log(params);

  var value = $("input[name='"+ params.field_name +"']").val();
  DEBUG && console.log("Element with name :'", params.field_name, "' has value : ", value);

  if(value !== 'undefined')
  {
    if(value == "" || value == null || value =="['None']" || value=="None")
    {
      $.confirm({
        title: params.err_title,
        content: params.error_msg,
        type: params.msg_type,
      });
      return false;
    }
    else
    {
      DEBUG && console.log("Value of input textbox element with name '" + params.field_name + "' looks ok");
      return true;
    }
  }
  else
  {
    DEBUG && console.log("Problem while fetching "+ params.field_name +" input textbox element");
    return false;
  }

  return false
}


function header_check(){
  tc_header = $('#id_tc_header_status').is(':checked')
  custom_header = $('#id_custom_header_status').is(':checked')
  if (tc_header == false && custom_header == false){
    $.confirm({
      title:  "Encountered an error !",
      content:  "Please Select Data Fields !",
      type: "red",
    });
    return false;
  }
  if (tc_header == true){
    if ($('.data_field_div').children('.checkbox_container').find('input[type="checkbox"]:checked').length == 0){
      $.confirm({
        title:  "Encountered an error !",
        content:  "Please Select Data Fields !",
        type: "red",
      });
      return false;
    }
  }
  return true;
}

function custom_ques_check(){
  val = $('.custom_que').val()
  // old_no = $('.custom_que').data('old-no')
  // if (parseInt(val) < old_no){
  //   $.confirm({
  //     title:  "Encountered an error !",
  //     content:  "You can't reduce the custom question !",
  //     type: "red",
  //   });
  //   $('.custom_que').val(old_no)
  //   return false;
  // }
  if (parseInt(val) > 10 || parseInt(val) <= 0 || val == ""){
    // remove checked status icon for custom question....Akshay G...Date - 24th Oct, 2019
    $('.custom_que').closest('#custom_field').find('.specs_status').remove();
    $.confirm({
      title:  "Encountered an error !",
      content:  "Number of custom questions should be within range 1 to 10!",
      type: "red",
    });
    return false;
  }
  return true;
}
function job_title_check(){
  val = $('#id_job_title').val()
  if (val == ""){
    $.confirm({
      title:  "Encountered an error !",
      content:  "Please fill Job Title",
      type: "red",
    });
    return false;
  }
  return true;
}

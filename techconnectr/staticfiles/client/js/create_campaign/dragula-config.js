if(typeof DEBUG === 'undefined')
      var DEBUG =  true;  // false

// global
var id_board_drop = "board-drop";
var id_board_drag = "board-drag";
  
var id_component_data_holder = "#component-data-holder";


$(document).ready(function(){

  var containers = [
                    document.querySelector('#'+ id_board_drop), 
                    document.querySelector('#' + id_board_drag),
                   ]
  
  // board 1 
  var drake =  dragula(containers, {
    direction: 'vertical',             // Y axis is considered when determining where an element would be dropped
    copy: false,                       // elements are moved by default, not copied
    copySortSource: false,             // elements in copy-source containers can be reordered
    revertOnSpill: true,              // spilling will put the element back where it was dragged from, if this is true
    removeOnSpill: false,              // spilling will `.remove` the element, if this is true
    mirrorContainer: document.body,    // set the element that gets mirror elements appended
    ignoreInputTextSelection: true,     // allows users to select input text, see details below

    accepts: function (el, target) {
      // if target is drag board dont put element
      return target !== document.getElementById(id_board_drag)
    },
  }).on('drag', function (el) {
      //DEBUG && console.log("Drag event called");
      // Automatically Scroll the page 
      document.onmousemove = (e) => {
        let event = e || window.event;
        let mouseY = event['pageY'];
        let scrollTop = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop; // document.documentElement.scrollTop is undefined on the Edge browser
        let scrollBottom = scrollTop + window.innerHeight;
        let elementHeight = el.offsetHeight; // this is to get the height of the dragged element

        if (mouseY - elementHeight / 2 < scrollTop) {
            window.scrollBy(0, -15);
        } else if (mouseY + elementHeight > scrollBottom) {
            window.scrollBy(0, 15);
        }
    };
  }).on('dragend', function(el){
    // detach the mouse move event when the drag ends
    document.onmousemove = null;
  }).on('drop', function (el, target, source, sibling) {
      DEBUG && console.log("drop event called ");
      DEBUG && console.log("el : ", el);
      DEBUG && console.log("target : ", target);
      DEBUG && console.log("source : ", source);
      DEBUG && console.log("sibling : ", sibling);

      // call function
      after_element_drop(el, target, source, sibling)

     // var today = new Date();
     // $(el).append("<p>" + today + "</p>");
  })
  

  // on remove selected component from dropped area
  $(document).on("click", ".remove_component", function(event){
    var el_component = $(this).closest(".card").get();
    DEBUG && console.log("component : ", el_component);

    var id_component = $(el_component).data("component-id");
    DEBUG && console.log("Removing component with id : ", id_component);

    var component_data = $(el_component).find(".component_data").get();
    DEBUG && console.log("component_data : ", component_data);

    //jQuery(jQuery(id_component_data_holder).find('#' + invoke_div).detach()).appendTo($(el));
    jQuery($(component_data).detach()).appendTo(jQuery(id_component_data_holder));

    // show label
    $(el_component).find(".component_label").removeClass("hidden");

    // get default position or rank
    var rank = $(el_component).data("position");
    if(rank == 'undefined')
    {
      console.log("data Position  is undefined  in : ", el_component);
      rank = -1;
    }

    // also add card to drag box
    //jQuery($(el_component).detach()).appendTo(jQuery("#"+ id_board_drag));
    jQuery("#"+ id_board_drag).insertAt(rank, $(el_component).detach());
    if ($(this).closest('.beefup').find('.beefup__body').hasClass('custom_specs')){
        remove_custom_specs($(this));
        return;
    }
    if ($(el_component).attr('data-invoke-div') == 'account_based_marketing'){
      // save abm status...for industry, company size validation....Akshay G.
      $('#validate_abm_status___').attr('data-abm_status', '0');
      
    }
    // remove data from table by making ajax call
    selected_component_remove(el_component);
  });

});


function after_element_drop(el, target, source, sibling)
{
  // source element id 
  var source_id = $(source).attr("id");
  DEBUG && console.log("Source element id is : ", source_id);

  // target element id
  var target_id = $(target).attr("id");
  DEBUG && console.log("Target element id is : ", target_id);

  // if (string1.toLowerCase() === string2.toLowerCase())
  //if ((target_id.toLowerCase() === id_board_drop) && (source_id.toLowerCase() === id_board_drag))
  if(target_id.toLowerCase() != source_id.toLowerCase())
  {
    // console.log("Id of source is 'on_hold' ");
    var invoke_div = $(el).data("invoke-div");  // ex. product_category
    DEBUG && console.log("invoke_div : ", invoke_div);

    // check is element exists 
    var len_invoke_div = $('#' + invoke_div).length
    DEBUG && console.log("Length of invoke div : ", len_invoke_div);

    // $(component_data_holder).find("#" + )

    if(len_invoke_div === 1)
    {
      // everything ok // process next step
      // hide original label of component
      $(el).find(".component_label").addClass("hidden");

      // append div to element
      // jQuery(jQuery("#yourElement").detach()).appendTo("body")
      jQuery(jQuery(id_component_data_holder).find('#' + invoke_div).detach()).appendTo($(el))
      
      // save selected element in table
      if ($(el).find('.beefup__body').hasClass('custom_specs')){
          add_custom_specs_element($(el));
      }
      add_component_to_selected_list(el);
      if ($(el).attr('data-invoke-div') == 'account_based_marketing'){
        // save abm status...for industry, company size validation....Akshay G.
        $('#validate_abm_status___').attr('data-abm_status', '1');
      }
    }
    else if(len_invoke_div === 0)
    {
      console.log('Div with id : "'+ invoke_div + '" not found')
      alert('Div with id : "'+ invoke_div + '" not found')
    }
    else if(len_invoke_div > 1)
    {
      console.log("More than two div found with id : "+ invoke_div);
      $.confirm({
        title: 'Encountered an error!',
        content: "More than two div found with id : "+ invoke_div,
        type: 'red',
        typeAnimated: true,
      });
    }
  }
 return true;
}


function add_component_to_selected_list(el)
{
   var url = $("#url_add_selected_component").val();
   if(url == 'undefined')
   {
    DEBUG && console.log("Url undefined to component to selected list: ");
   }
   else
   {
    var id_campaign = $("#id_campaign").val();
    var id_selected_component = $(el).data("component-id");
    
    var ajaxTime= new Date().getTime();
   $.ajax({
       url: url,
       type: "POST",
       //dataType: "json",

       data: {
           id_campaign: id_campaign,  // "34",
           id_selected_component: id_selected_component,  // 1
       },

       /**
        * A function to be called if the request succeeds.
        */
       success: function(data) {
           var totalTime_in_ms = new Date().getTime() - ajaxTime;
           //console.log("Total time to complete ajax call : ", totalTime_in_ms ,' ms');
           var seconds = (totalTime_in_ms / 1000);
           console.log("Total time to complete ajax call : ", seconds, ' seconds');

           console.dir(data);
           console.dir(data.message);
           //alert("Data: " + data + "\nStatus: " + status);
       },

       /**
        * A function to be called if the request fails. 
        */
       error: function(jqXHR, textStatus, errorThrown) {
          show_ajax_error_info(jqXHR, textStatus, errorThrown);
       },

   });

 }
}


function selected_component_remove(el)
{
   var url = $("#url_remove_selected_component").val();
   if(url == 'undefined')
   {
    DEBUG && console.log("Url undefined to component to selected list: ");
   }
   else
   {
    var id_campaign = $("#id_campaign").val();
    var id_selected_component = $(el).data("component-id");
    
    var ajaxTime= new Date().getTime();
    
   $.ajax({
       url: url,
       type: "POST",
       //dataType: "json",

       data: {
           id_campaign: id_campaign,  // "34",
           id_selected_component: id_selected_component,  // 1
       },

       /**
        * A function to be called if the request succeeds.
        */
       success: function(data) {
           var totalTime_in_ms = new Date().getTime() - ajaxTime;
           //console.log("Total time to complete ajax call : ", totalTime_in_ms ,' ms');
           var seconds = (totalTime_in_ms / 1000);
           console.log("Total time to complete ajax call : ", seconds, ' seconds');

           console.dir(data);
           console.dir(data.message);
           //alert("Data: " + data + "\nStatus: " + status);
       },

       /**
        * A function to be called if the request fails. 
        */
       error: function(jqXHR, textStatus, errorThrown) {
          show_ajax_error_info(jqXHR, textStatus, errorThrown);
       },

   });

 }
}


function show_ajax_error_info(jqXHR, textStatus, errorThrown)
{
  //alert('An error occurred... Look at the console (F12 or Ctrl+Shift+I, Console tab) for more information!');

   console.log('jqXHR:');
   console.log(jqXHR);

   if (jqXHR.status == 500) 
   {
       // Server side error
       console.log("Server side Error");
   } 
   else if (jqXHR.status == 404)
   {
       // Not found
       console.log("Requested URL (page) not found");
   } 
   else if (jqXHR.status == 403) 
   {
       // Forbidden
       console.log("The request is for something forbidden. Authorization will not help.");
   } 
   else 
   {
       console.log("jqXHR.status : " + (jqXHR.status).toString() + " and errorThrown : " + errorThrown);
   }
}


// jQuery insert div as certain index
jQuery.fn.insertAt = function(index, element) {
  var lastIndex = this.children().size();
  if (index < 0) {
    index = Math.max(0, lastIndex + 1 + index);
  }
  this.append(element);
  if (index < lastIndex) {
    this.children().eq(index).before(this.children().last());
  }
  return this;
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

// campaign custom specs js..Akshay G.
$(function(){
  // toggle text box of adding campaign custom spec for client..Akshay G.
    $(document).on('click', '#add_new_specs', function(){
        $(this).toggleClass('hidden');
        $(this).next().toggleClass('hidden');
    })
    var k = '';
    var n = 0;
    // add new custom spec for campaign..Akshay G.
    $(document).on('click','#add_element', function(){
          var spec_title = $('#new_specs').val();
          if (spec_title.trim().length == 0){

          }
          else{
              var regex = new RegExp("^[a-zA-Z0-9 _-]+$");
              if(regex.test(spec_title) == false){
                $.confirm({
                        title: 'Invalid Specification title !',
                        content: "Expected input: title, title_1, title-2, title 3",
                        type: 'red',
                        typeAnimated: true,
                      });
                return;
              }
              var div_id = spec_title.split(' ').join('_');
              var default_specs = ['industry_type', 'industry', 'job_title', 'job_level', 'country', 'company_size', 'employee_size', 'job_function', 'department', 'campaign_pacing', 'pacing', 'touchbase_meeting', 'account_based_marketing', 'account_supression', 'level_intent', 'level_of_intent', 'assets', 'custom_field', 'custom_question', 'campaign_delivery_time', 'delivery_time', 'special_instructions', 'revenue_size', 'lead_validation', 'custom_lead_validation', 'city', 'city_id', 'state', 'state_id'];
              if (jQuery.inArray(div_id, default_specs) != -1){
                $.confirm({
                        title: 'Encountered an error!',
                        content: "Element already exists with \'"+spec_title+"\' name",
                        type: 'red',
                        typeAnimated: true,
                      });
                return;
              }
              var token = getCookie('csrftoken');
              $.ajax({
                type: 'post',
                url: '/utils/add_new_spec/',
                data: {'label':spec_title, 'invoke_div':div_id, 'csrfmiddlewaretoken': token},
                success: function(data){
                    if (data['status'] == 'success'){
                      n += 1;
                      k = `${div_id}_all_`+n;
                      $('#add_new_specs').before(
                            `
                            <div class="card custom_specs" data-invoke-div='${div_id}' data-is-user-set-default=0 data-component-id=='0' data-position='0'>
                            <span class="component_label">${spec_title}</span>
                            </div>
                            `
                        );
                        var save_data_url = '/utils/save_selected_custom_specs_data/'
                      $('#component-data-holder').append(custom_specs_div(div_id, spec_title, k, save_data_url));
                      // call beefup methods on dynamically added custom specs...Akshay G...Date - 18th Oct, 2019
                      $.openbeefup_dynamic();
                    }
                    else{
                      $.confirm({
                        title: 'Encountered an error!',
                        content: "Element already exists with \'"+spec_title+"\' name",
                        type: 'red',
                        typeAnimated: true,
                      });
                    }
                },
                error: function(){
                    $.confirm({
                        title: 'Encountered an server error!',
                        content: "Please report this issue to techconnectr technical team.",
                        type: 'red',
                        typeAnimated: true,
                    });
                }
              })
          }
          $('#new_specs').parent().toggleClass('hidden');
          $('#add_new_specs').toggleClass('hidden');
          $('#new_specs').val('');
      })
      var tr_length;
      // save new data of custom spec on 'save' button..Akshay G.
      $(document).on('click', '.save_custom_specs_data', function(){
          var specs_id = $(this).attr('data-specs_id');
          // var camp_id = parseInt($('#id_camapaign').val());
          var specs_data = $(this).prev().val();
          if (specs_data.trim().length == 0){
            return;
          }
          var regex = new RegExp("^[a-zA-Z0-9 _-]+$");
          if(regex.test(specs_data) == false){
            $.confirm({
                    title: 'Invalid Specification data !',
                    content: "Expected input: data, data_1, data-2, data 3",
                    type: 'red',
                    typeAnimated: true,
                  });
            return;
          }
          var element = $(this);
          $.ajax({
            type: 'post',
            url : '/utils/save_custom_specs_data/',
            data: {'div_id': specs_id, 'specs_data': specs_data, 'csrfmiddlewaretoken':getCookie('csrftoken')},
            success: function(data){
              if (data['status'] == 'error'){
                  $.confirm({
                    title: 'Encountered an error!',
                    content: "Specs data already exists with \'"+specs_data+"\' name",
                    type: 'red',
                    typeAnimated: true,
                  });
              }
              else{
                $(element).closest('.beefup__body').find('.chk_bx_select_all').prop('checked', false);
                tr_length = $(element).parents('.checkbox_container').find('table tbody tr').length;
                $(element).parents('.checkbox_container').find('table tbody').append(
                  `<tr><td>
                      <div class="radio ml-10" data-txt="{{ name }}">
                          <input id="data_${specs_id}_${tr_length+1}[]"
                              data-div_id="${specs_id}"
                              value="${specs_data}" type="checkbox" name="${specs_id}_name"
                              />
                          <label for="data_${specs_id}_${tr_length+1}[]"
                              class="lbl">${specs_data}</label>
                      </div>
                  </td></tr>`
                );
                tr_length += 1;
                $(element).prev().val('');
              }
            },
            error: function(){
                $.confirm({
                    title: 'Encountered an server error!',
                    content: "Please report this issue to techconnectr technical team.",
                    type: 'red',
                    typeAnimated: true,
                });
            }
          })
      })
      // add html for new spec into component div ..Akshay G.
      function custom_specs_div(div_id, spec_title, k, save_data_url){
          var div = `
          <div id="${div_id}" class="component_data">
                    <article class="beefup">
                        <h6 class="panel-title beefup__head">
                            <span>Select ${spec_title}</span>
                            <button type="button" class="close remove_component"><i class="far fa-times-circle cls-color"></i></button>
                        </h6>
                        <div class="beefup__body custom_specs">
                            <div class="row">
                                <div class="col-md-12 col-sm-12 mb-5 mt-5">
                                    <div class="radio display-inline-block">
                                        <input id="${k}" class="pointer chk_bx_select_all"
                                            type='checkbox' />
                                        <label for="${k}" class="lbl">Select All</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group checkbox_container">
                                <input type="hidden" class="url_holder_save_data" value="${save_data_url}" />
                                <input type="hidden" name="${spec_title}" class="save_data" value="" id="${div_id}"/>
                                <div class="table_scroller">
                                    <table class="table table-hover">
                                        <tbody>

                                        </tbody>
                                    </table>
                                </div>
                                <div class="row">
                                    <div class="row cancel_more_ind" style='display: none;'>
                                        <div class="col-md-12 col-sm-12 mb-5 mt-5" style="margin-left: 5%">
                                            <div class="radio pull-left">
                                                <input type="text" class="custom_input" name="" value="">
                                                <button type="button" class="btn btn-success save_custom_specs_data" name="save" data-specs_id="${div_id}">
                                                    SAVE
                                                </button>
                                                <button type="button" class="btn btn-danger cancel_ind">CANCEL</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>                                
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 mb-5 mt-5">
                                    <div class="radio pull-left">
                                        <button type="button" class="btn btn-primary add_more_ind" name="button"
                                            title="add more ${spec_title}"><i class="fas fa-plus"
                                                aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </article>
                </div>
        `
        return div
      }
})

// remove custom spec from selected specs list of campaign..Akshay G.
function remove_custom_specs(element){
    var div_id = $(element).closest('.component_data').prop('id');
    var camp_id = parseInt($('#id_camapaign').val());
    var token = getCookie('csrftoken');
    $.ajax({
      type: 'post',
      url: '/utils/remove_custom_specs/',
      data: {'div_id': div_id, 'camp_id': camp_id, 'csrfmiddlewaretoken': token},
      success: function(data){

      },
      error: function(){
          $.confirm({
              title: 'Encountered an server error!',
              content: "Please report this issue to techconnectr technical team.",
              type: 'red',
              typeAnimated: true,
          });
      }
    })
}
// add custom spec into selected specs list of campaign..Akshay G.
function add_custom_specs_element(element){
    var div_id = element.attr('data-invoke-div');
    var camp_id = parseInt($('#id_camapaign').val());
    var token = getCookie('csrftoken');
    $.ajax({
      type: 'post',
      url: '/utils/add_custom_specs/',
      data: {'div_id': div_id, 'camp_id': camp_id, 'csrfmiddlewaretoken': token},
      success: function(data){
        custom_specs_validation($('input[name="' + div_id+'_name' + '"]:first'), true);
      },
      error: function(){
          $.confirm({
              title: 'Encountered an server error!',
              content: "Please report this issue to techconnectr technical team.",
              type: 'red',
              typeAnimated: true,
          });
      }
    })
}
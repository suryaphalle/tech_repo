// alert("hii");
let form_array = []
let form = 1
$(document).ready(function () {
    no_que = $('#custome_question_no').text()
    prev_ques = ''
    if ($('#old_custome_questions').val() != '') {
        prev_ques = JSON.parse($('#old_custome_questions').val())
    }

    if (prev_ques != '') {
        old_ques(prev_ques)
        if ((no_que - prev_ques.length) != 0) {
            for (i = 0; i < (no_que - prev_ques.length); i++) {
                if ($('.survey_form').is(':empty')) {
                    $('.survey_form').html(`<div class="row form_div editing">
                                            <button type="button" class="btn remove legitRipple close">
                                                <i class="far fa-times-circle"></i>
                                            </button>
                                            <div class="col-md-1 que_no" >Q<span class="qno">${form}</span></div>
                                            <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style=""> Select Question Type </div>
                                            </div>`)
                    form_array.push(`question_${form}`)
                    form = form + 1;
                } else {
                    $('.form_div').removeClass('editing')
                    $('.survey_form').append(`<div class="row form_div editing">
                                            <button type="button" class="btn remove legitRipple close">
                                                <i class="far fa-times-circle"></i>
                                            </button>
                                            <div class="col-md-1 que_no" >Q<span class="qno">${form}</span></div>
                                            <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style=""> Select Question Type </div>
                                            </div>`)
                    form_array.push(`question_${form}`)
                    form = form + 1;
                }
            }
        }
    }
    else {

        for (i = 0; i < no_que; i++) {
            if ($('.survey_form').is(':empty')) {
                $('.survey_form').html(`<div class="row form_div editing">
                <button type="button" class="btn remove legitRipple close">
                                                <i class="far fa-times-circle"></i>
                                            </button>
                                        <div class="col-md-1 que_no" >Q<span class="qno">${form}</span></div>
                                        <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style=""> Select Question Type </div>
                                        </div>`)
                form_array.push(`question_${form}`)
                form = form + 1;
            } else {
                $('.form_div').removeClass('editing')
                $('.survey_form').append(`<div class="row form_div editing">
                <button type="button" class="btn remove legitRipple close">
                                                <i class="far fa-times-circle"></i>
                                            </button>
                                        <div class="col-md-1 que_no" >Q<span class="qno">${form}</span></div>
                                        <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style=""> Select Question Type </div>
                                        </div>`)
                form_array.push(`question_${form}`)
                form = form + 1;
            }
            $('.custom_ques_set').css('display', 'block')
            $('.custom_ques_set input[type="radio"]').each(function () {
                $("input:radio").attr("checked", false);
            });
        }
    }

    let question_numbers
    question_form = []
    $('.btn-save-html').click(function () {
        make_dict()
        $('#final').removeClass('hidden')
        data = $('#final').html()
        token = getCookie('csrftoken')
        console.log('questions'+question_form.length)
        // code for selection of answer mandatory for each question - Amey
        var question_with_error = ''
        var allow = true;
        if (question_form.length > 0){
            $.each(question_form, function(index, value){
                if(value['qualified'].length == 0 && value['type'] != 'single_answer'){
                    question_with_error = index+1;
                    allow = false;
                    return false;
                }
            });
        }
        if(allow == true && question_form.length != 0){
        swal({
            title: "Are you sure ?",
            text: "You are about to save custom questions.",
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((willDelete) => {
            if (willDelete) {
                $.ajax({
                    type: 'post',
                    url: '/form/store_question/',
                    dataType: 'json',
                    data: { camp_id: parseInt($('#campaign_id').text()), csrfmiddlewaretoken: token, 'question_form': JSON.stringify(question_form), 'html': data, 'question_numbers': question_numbers },
                    success: function (data) {
                        // console.log(data)
                        if (data.success == 1) {
                            question_form = [];
                            swal({
                                title: "Custom Questions",
                                text: "Great, Custom questions updated successfully.",
                                icon: "success",
                                button: 'ok',question_form
                            }).then(function () {
                                window.location.href = '/client/add_campaign_spec/' + parseInt($('#campaign_id').text());
                            });

                        }
                    }
                });
            } else {
                question_form = [];
            }
        });
        
    }else{
        if (question_form.length == 0){
            swal("Error!","Can not save empty custom question.","error");    
        }
        if(allow == false){
            swal({
                title: "Error!",
                text: "Please select atleast one qualified answer for Q"+question_with_error,
                icon: "warning",
                button: 'ok',
            });
        }
        question_form = [];
    }

    });
    function make_dict() {
        $('#final').empty()

        $(`.survey_form`).find('.form_div').each(function () {
            que_no = $(this).find('div.que_no').text();
            question = $(this).find('div.form-group').html();
            que_type = $(this).find('div.form-group').data('elementType')

            // console.log(que_no,question);
            $('#final').append(`
                <div class="row question" id="${que_no}" data-type="${que_type}">
                <div class="col-md-1">${que_no}</div>
                <div class="col-md-11">${question}</div>
                </div>
            `)
            $('#final').find('.editable').each(function () {
                $(this).removeAttr('contenteditable')
            })
            $('#final').find('.edit_dropdown').remove()
            $('#final').find('.editable').removeClass('editable')
            $('#final').find('.valid').remove()

        });

        convert()
    }


    //make dict of custome question form
    function convert() {

        $(`#final`).find('.question').each(function () {
            qdict = {}
            que_no = $(this).attr('id')
            que_text = $(this).find('.question_text').text();
            que_type = $(this).data('type')
            // console.log (que_no+':'+que_text)
            ans = {}
            $(this).find('.answer').each(function () {
                ans[parseInt($(this).attr('value'))] = $(this).text()
            });
            // console.log(ans)
            qualified = []
            $(this).find('.qualified').each(function () {
                qualified.push($(this).attr('value'))
            })
            question_numbers = que_no.split('Q')[1]
            qdict = { 'id': que_no, 'type': que_type, 'question_text': que_text, 'options': [ans], 'qualified': qualified }
            // console.log(qdict)
            question_form.push(qdict)
        });
    }

    $(document).on('change', "input[name='valid_option']", function () {
        if ($(this).is(":checked")) {

            $(this).parents('.radio').find('.answer').addClass('qualified')
            $(this).parents('.checkbox').find('.answer').addClass('qualified')

        } else {
            $(this).parents('.radio').find('.answer').removeClass('qualified')
            $(this).parents('.checkbox').find('.answer').removeClass('qualified')
        }
    });
    // $("input[name='valid_option']").change(function(){
    //     if ($(this).is(":checked")) {
    //         console.log('asdasd')
    //     }

    // });
});

// displaying previous questions
function old_ques(prev_ques) {
    prev_ques.forEach(function (ques) {
        if ($('.survey_form').is(':empty')) {
            if (ques.type == "checkbox") {
                $('.survey_form').html(`
                <div class="row form_div editing">

                        <button type="button" class="btn remove legitRipple close">
                        <i class="far fa-times-circle"></i>
                    </button>
                    <div class="col-md-1 que_no">Q<span class="qno">${form}</span></div>
                    <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style="">
                        <div class="panel question_block">
                            <div class="panel-body">

                                <button type="button" class="btn delete-option close legitRipple" data-element-type="check_box">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn add-option close legitRipple" data-element-type="check_box">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <div class="form-group" data-element-type="checkbox">
                                ${set_question(ques)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `)
                form_array.push(`question_${form}`)
                form = form + 1;
            }
            if (ques.type == "radio") {
                $('.survey_form').html(`
                <div class="row form_div editing">

                        <button type="button" class="btn remove legitRipple close">
                        <i class="far fa-times-circle"></i>
                    </button>
                    <div class="col-md-1 que_no">Q<span class="qno">${form}</span></div>
                    <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style="">
                        <div class="panel question_block">
                            <div class="panel-body">
                                <button type="button" class="btn delete-option close legitRipple" data-element-type="radio-group">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn add-option close legitRipple" data-element-type="radio-group">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <div class="form-group" data-element-type="radio">
                                ${set_question(ques)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `)
                form_array.push(`question_${form}`)
                form = form + 1;
            }
            if (ques.type == "single_answer") {
                $('.survey_form').html(`
                <div class="row form_div editing">
                    <div class="col-md-1 que_no">Q<span class="qno">${form}</span></div>
                    <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style="">
                        <div class="panel question_block">
                            <div class="panel-body">
                                <div class="form-group"  data-element-type="single_answer">
                                ${set_question(ques)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `)
                form_array.push(`question_${form}`)
                form = form + 1;
            }
            if (ques.type == 'undefined') {
                if ($('.survey_form').is(':empty')) {
                    $('.survey_form').html(`<div class="row form_div editing">
                                            <div class="col-md-1 que_no" >Q<span class="qno">${form}</span></div>
                                            <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style=""> Select Question Type </div>
                                            </div>`)
                    form_array.push(`question_${form}`)
                    form = form + 1;
                } else {
                    $('.form_div').removeClass('editing')
                    $('.survey_form').append(`<div class="row form_div editing">
                                            <div class="col-md-1 que_no" >Q<span class="qno">${form}</span></div>
                                            <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style=""> Select Question Type </div>
                                            </div>`)
                    form_array.push(`question_${form}`)
                    form = form + 1;
                }
            }
        } else {
            $('.form_div').removeClass('editing')
            if (ques.type == "checkbox") {
                $('.survey_form').append(`
                <div class="row form_div editing">

                        <button type="button" class="btn remove legitRipple close">
                        <i class="far fa-times-circle"></i>
                    </button>
                    <div class="col-md-1 que_no">Q<span class="qno">${form}</span></div>
                    <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style="">
                        <div class="panel question_block">
                            <div class="panel-body">
                                <button type="button" class="btn delete-option close legitRipple" data-element-type="check_box">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn add-option close legitRipple" data-element-type="check_box">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <div class="form-group" data-element-type="checkbox">
                                ${set_question(ques)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `)
                form_array.push(`question_${form}`)
                form = form + 1;
            }
            if (ques.type == "radio") {
                $('.survey_form').append(`
                <div class="row form_div editing">

                        <button type="button" class="btn remove legitRipple close">
                        <i class="far fa-times-circle"></i>
                    </button>
                    <div class="col-md-1 que_no">Q<span class="qno">${form}</span></div>
                    <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style="">
                        <div class="panel question_block">
                            <div class="panel-body">
                                <button type="button" class="btn delete-option close legitRipple" data-element-type="radio-group">
                                    <i class="fa fa-minus"></i>
                                </button>
                                <button type="button" class="btn add-option close legitRipple" data-element-type="radio-group">
                                    <i class="fa fa-plus"></i>
                                </button>
                                <div class="form-group" data-element-type="radio">
                                ${set_question(ques)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `)
                form_array.push(`question_${form}`)
                form = form + 1;
            }
            if (ques.type == "single_answer") {
                $('.survey_form').append(`
                <div class="row form_div editing">
                    <div class="col-md-1 que_no">Q<span class="qno">${form}</span></div>
                    <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style="">
                        <div class="panel question_block">
                            <div class="panel-body">
                                <div class="form-group"  data-element-type="single_answer">
                                ${set_question(ques)}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                `)
                form_array.push(`question_${form}`)
                form = form + 1;
            }
            if (ques.type == 'undefined') {
                if ($('.survey_form').is(':empty')) {
                    $('.survey_form').html(`<div class="row form_div editing">
                                            <div class="col-md-1 que_no" >Q<span class="qno">${form}</span></div>
                                            <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style=""> Select Question Type </div>
                                            </div>`)
                    form_array.push(`question_${form}`)
                    form = form + 1;
                } else {
                    $('.form_div').removeClass('editing')
                    $('.survey_form').append(`<div class="row form_div editing">
                                            <div class="col-md-1 que_no" >Q<span class="qno">${form}</span></div>
                                            <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style=""> Select Question Type </div>
                                            </div>`)
                    form_array.push(`question_${form}`)
                    form = form + 1;
                }
            }
        }
    })
}


// setting custome questtion according to type
function set_question(ques) {
    if (ques.type == 'checkbox') {
        checkbox_ques = `<div class="text-semibold question_text editable" contenteditable="true">${ques.question_text}</div>`
        checkbox_ques += set_option(ques)
        return checkbox_ques
    }
    if (ques.type == 'radio') {
        radio_ques = `<div class="text-semibold question_text editable" contenteditable="true">${ques.question_text}</div>`
        radio_ques += set_option(ques)
        return radio_ques
    }
    if (ques.type == 'single_answer') {
        single_ques = `
		<label class="text-semibold question_text editable"  contenteditable="true">${ques.question_text}</label>
		<input type="text" class="form-control hidden input-element" >`
        // radio_ques += set_option(ques)
        return single_ques
    }

}


//set options for question
function set_option(ques) {
    options = ques.options[0]
    // console.log(ques)
    // console.log(ques.qualified)
    var option = ''
    if (ques.type == 'checkbox') {
        for (var key in options) {
            // console.log(parseString(key))
            if (ques.qualified.includes(key)) {
                option += `
                <div class="col-md-12 checkbox">
                    <div class="col-md-6"> <input type="checkbox" name="unstyled-checkbox-left"
                        data-option_action="">
                        <div class=" answer editable qualified" contenteditable="true" value="${key}">${options[key]}</div>
                    </div>
                    <label class="col-md-6 valid"><input type="checkbox" name="valid_option" data-option_action="" checked> Qualified</label>
                </div>`
            } else {
                option += `
                <div class="col-md-12 checkbox">
                    <div class="col-md-6"> <input type="checkbox" name="unstyled-checkbox-left"
                        data-option_action="">
                        <div class=" answer editable" contenteditable="true" value="${key}">${options[key]}</div>
                    </div>
                    <label class="col-md-6 valid"><input type="checkbox" name="valid_option" data-option_action=""> Qualified</label>
                </div>`
            }
        }
        return option
    }
    if (ques.type == 'radio') {
        for (var key in options) {
            if (ques.qualified.includes(key)) {
                option += `
                <div class="col-md-12 radio"> <div class="col-md-6"> <input type="radio" name="unstyled-radio-left"
                        data-option_action="">
                    <div class="answer editable qualified" contenteditable="true" value="${key}">${options[key]}</div>
                </div>
                <label class="col-md-6 valid">
                    <input type="checkbox" name="valid_option" data-option_action="" checked> Qualified
                </label>
                </div>`
            } else {
                option += `
                <div class="col-md-12 radio"> <div class="col-md-6"> <input type="radio" name="unstyled-radio-left"
                        data-option_action="">
                    <div class="answer editable" contenteditable="true" value="${key}">${options[key]}</div>
                </div>
                <label class="col-md-6 valid">
                    <input type="checkbox" name="valid_option" data-option_action=""> Qualified
                </label>
                </div>`
            }
        }
        return option
    }
}

// on paste plain text without html syntax
$(document).on('paste','[contenteditable]', function (e) {
    e.preventDefault();
    var text = '';
    if (e.clipboardData || e.originalEvent.clipboardData) {
        text = (e.originalEvent || e).clipboardData.getData('text/plain');
    } else if (window.clipboardData) {
        text = window.clipboardData.getData('Text');
    }
    if (document.queryCommandSupported('insertText')) {
        document.execCommand('insertText', false, text);
    } else {
        document.execCommand('paste', false, text);
    }
});


//global variable
//when click of edit element icon calculate index of item in grid 
// detect active item to edit  from index on .item class among siblings 
//so we use it to append text when user changes label or other fields inside modal
active_item_to_edit = undefined;

// grids 
// $(function () {
//     //alert("Inside example.js");
//     grid1 = new Muuri('.grid-1', {
//         dragEnabled: true,
//         dragReleaseDuration: 200, // dragReleaseDuration: 300
//         showDuration: 10, //animation duration // value 0 will disable
//         dragSort: function () {
//             //return [grid1, grid2] // allow soring of this icon in both grid
//             return [grid2] //allow sorting or moving to only grid2 //not current
//         }
//     })

//     // another grid
//     var grid2Hash = {};
//     grid2 = new Muuri('.grid-2', {
//         dragEnabled: true,
//         dragReleaseDuration: 200, // dragReleaseDuration: 300
//         dragSort: true,
//         showDuration: 20, //animation duration // value 0 will disable

//         itemDraggingClass: 'nn',
//     })
//         .on('receive', function (data) {
//             //console.log("receive event");   
//             //console.log(data);   
//             //console.log(data.item); 
//             //console.log("id : " + data.item._id); 
//             grid2Hash[data.item._id] = function (item) {
//                 if (item === data.item) {
//                     var clone = cloneElem(data.item.getElement());
//                     data.fromGrid.add(clone, { index: data.fromIndex });
//                     data.fromGrid.show(clone); //show item in fromgrid(grig from where element dragged)

//                     // manipulate item
//                     var item_element = data.item.getElement();
//                     //$(item_element).find( "span" ).css("color", "red");
//                     // call function to add new html to item
//                     replace_html(grid2, item_element);
//                 }
//             };
//             grid2.once('dragReleaseStart', grid2Hash[data.item._id]);
//         })
//         .on('send', function (data) {
//             var listener = grid2Hash[data.item._id];
//             if (listener) {
//                 grid2.off('dragReleaseStart', listener);
//             }
//         })
//         .on('layoutStart', function (items) {
//             //important otherwise if delete element by index it may delete other element
//             // Synchronize the item elements to match the order of the items in the DOM.
//             grid2.synchronize();
//         })

// });

function cloneElem(elem) {
    //console.log(elem); // check data
    var clone = elem.cloneNode(true);
    clone.setAttribute('style', 'display:none;');
    clone.setAttribute('class', 'item');
    clone.children[0].setAttribute('style', '');
    return clone;
}

function replace_html(grid2, item_element) {
    //get html stored inside span
    var new_html = $(item_element).find("code").text(); // get text stored inside code tag

    //append new html
    var item_actions_html = $('#item_actions').text();
    var item_content = ".item-content";
    $(item_element).find(item_content).find("span").addClass("hidden");
    $(item_element).find(item_content).append(item_actions_html);

    $(item_element).find(item_content).append("<div class='html-element-holder'></div>"); // add holder div

    $(item_element).find(item_content).find(".html-element-holder").html(new_html); // add html / append

    // Refresh dimensions of all active item elements.
    grid2.refreshItems();

    //The grid's height/width (depends on the layout algorithm)
    // is also adjusted according to the position of the items
    // Layout items instantly (without animations).
    grid2.layout(true);
}


/*====================================================
**************************************************** 
     Item icon actions 
*****************************************************/
$(function () {

    var remove_item = '.remove-item'; //selectors 
    var edit_item = '.edit-item';
    var copy_item = '.copy-item';

    $(document).on("click", remove_item, function () {
        //alert("remove_item clicked");
        //console.log($(this).closest( ".item" ).html());
        var item_element = $(this).closest(".item");

        // find the index of item among their siblings
        var item_index = $(item_element).index();

        //alert( "Index: " + $(item_element).index() );
        // Remove items and the associated elements.
        //grid2.remove(0, {removeElements: true});
        grid2.remove(item_index, { removeElements: true });
    });

    $(document).on("click", edit_item, function () {
        //alert("edit_item clicked");
        var item_element = $(this).closest(".item");

        // find the index of item among their siblings
        var item_index = $(item_element).index();

        // for fruther use save value
        // use active_item_to_edit inside ontype events of model element
        //get current active item by index of ".item" class among their siblings
        active_item_to_edit = $(".item").get(item_index);
        //alert("active_item_to_edit "+ active_item_to_edit);

        //clone edit-html block //with holder div also
        //.clone( [withDataAndEvents ] [, deepWithDataAndEvents ] )
        var edit_html_block = $(item_element).find(".edit-html-holder").clone();

        //append edit-html-block to modal  
        $("#modal_edit_element").find(".modal-body").html(edit_html_block);

        //remove edit-html-holder div // append new edited html after close btn click of modal
        $(item_element).find(".edit-html-holder").remove();

        //alert($(item_element).find( ".model_launcher" ).html())
        $(item_element).find(".model_launcher").trigger("click"); // trigger click event of model which will launch model
    });

    $(document).on("click", copy_item, function () {
        //alert("copy_item clicked");
    });

});


/**************************************************************************************** 
--  modal close event
--  clone modal body data back to edit html holder
*****************************************************************************************/

$(document).on("click", ".btn-modal-close", function () {
    //alert("modal close btn clicked");

    //get currently edited values of modal body
    var modal_body_data = $("#modal_edit_element").find(".modal-body").find(".edit-html-holder").clone();

    //append modified html back to container
    $(active_item_to_edit).find(".edit-html-container").append(modal_body_data);
});



/**************************************************************************************** 
--  Onclick of edit html block inside modal
*****************************************************************************************/
$(function () {

    $(document).on("keyup", '.edit_label', function () {
        var value = $(this).val(); // textarea get value typed
        //alert( value ); 
        $(active_item_to_edit).find(".html-element-holder").find("label").first().text(value);
    });

    // edit placeholder clicked
    $(document).on("keyup", '.edit_placeholder', function () {
        var value = $(this).val(); //textbox get value
        //alert( $(this).val() );
        //console.log($active_item);
        $(active_item_to_edit).find(".html-element-holder").find(".input-element").attr("placeholder", value); //asign to placeholder
    });

    // edit value clicked
    $(document).on("keyup", '.edit_value', function () {
        var value = $(this).val(); //placeholder textbox value
        //alert( $(this).val() );

        //check is textbox or textarea
        var is_element_input = $(active_item_to_edit).find(".html-element-holder").find(".input-element").is("input"); //true or false
        //alert(is_element_input);

        //check is textarea
        var is_element_textarea = $(active_item_to_edit).find(".html-element-holder").find(".input-element").is("textarea"); //true or false
        //alert(is_element_textarea);

        if (is_element_input) {
            $(active_item_to_edit).find(".html-element-holder").find(".input-element").attr("value", value);
        }
        else if (is_element_textarea) {
            // if textarea change text attribute
            $(active_item_to_edit).find(".html-element-holder").find(".input-element").text(value);
        }
        else { }
    });

    //radio buttons
    // add more option
    $(document).on("click", ".add-option", function () {
        //alert("add option clicked");

        //append new row to table
        // var $table = $(this).parent().find('table');
        // var new_line = $table.find('tr:eq(1)').clone();
        // $table.append(new_line);
        // console.log($(this).attr('data-element-type'));

        //also add option to item radio group
        // html is stored inside #add_radio_option_html hidden field
        var option_html = $(`#${$(this).attr('data-element-type')}`).text();
        // console.log(option_html);

        if ($(this).attr('data-element-type') == "Dropdown") {
            opt_val = parseInt($(this).next().find('select').children().last().attr('value'))+1
            $(this).next().find('select').append(`<option value="${opt_val}">option ${opt_val}</option>`)
            if($(this).next().children().select('dropdown_list').find('ul')){
                $(this).next().children().select('dropdown_list').find('ul').append(`<li class="col-md-6" contenteditable="true" value="${opt_val}">option ${opt_val}</li> 
                <label class="col-md-6"><input type="checkbox" name="valid_option" data-option_action=""> valid</label>`)
            }
        } else {
            $(this).next().append(option_html)
        }
        $(this).parent().find('.answer').last().attr('value',$(this).parent().find('div.answer').length)

        //append to form group of item
        // $(active_item_to_edit).find(".form-group").append(option_html);
    });

    // set actions on radio button click
    $(document).on("click", ".set-option-action", function () {
        //alert("set-option-action clicked");
        //toggle hidden class
        $(this).parent().find('table').find(".option-action").each(function () {
            $(this).toggleClass("hidden");
        });
    });

    // edit option of radio click
    $(document).on("focusout", ".edit_modal_option", function () {
        //alert("edit option focusout");
        var value = $(this).val();
        //console.log(value);

        //get index among siblings
        var index = $(".modal-body").find(".edit_modal_option").index(this);
        //console.log("index : " + index);

        //get input element radio at that index in form grop of item // grid2
        var input_element = $(active_item_to_edit).find(".form-group").find("input").get(index);
        //console.log($input_element);

        //change parent element text i.e label text of radio option
        $(input_element).parent().find("span").text(value); // <label><input type="radio"> <span> Option </span></label> 
        //$(active_item_to_edit).find(".form-group").append(option_html);
    });


    // on select focus load options as form submit and question numbers
    $(document).on('focusin', ".select-action", function (e) {
        //alert("select opening");
        // store this in variable otherwise this keyword has other avlue in each method
        var current_select_box = this;

        // remove other element
        $(current_select_box).empty();

        var arr_option_collection = []; // declare array
        //var arr_option_collection = [{"value":1, "text":"NN"}, {"value":2, "text":"Maruti"}]; // static data init

        //create default actions
        var obj_select_option_text = { "value": "", "text": "Select an action" }; // default action
        var obj_submit_form_action = { "value": "submitform", "text": "Submit Form" }; // submit form action object

        //push these two objects with static data in array
        arr_option_collection.push(obj_select_option_text, obj_submit_form_action);
        //console.log(arr_option_collection);

        //append other options to  array depending upon items available in grid2
        $(".grid-2").find(".item").each(function (index) {
            //console.log("Item index :", index);

            //increment index by 1 so it start question from 1 insted of zero index
            var incremented_index = index + 1;

            var object = {
                "value": "question" + incremented_index, //no space between str and number
                "text": "Go to question " + incremented_index
            }
            //console.log(object);
            //push object into array
            arr_option_collection.push(object);
        });

        $.each(arr_option_collection, function (key, obj) {
            //console.log(key, obj); 
            //console.log(obj.text); 
            //append options
            $(current_select_box).append($("<option></option>")
                .attr("value", obj.value)
                .text(obj.text));

        });

    });

    // after selecting action in selectbox
    $(document).on("change", ".select-action", function () {
        //alert("selectbox value selected");
        //console.log(this.value);
        //get index among siblings
        var index = $(".modal-body").find(".select-action").index(this);
        //console.log("index : " + index);

        //set data attribute option_action  to radio button
        var input_element = $(active_item_to_edit).find(".form-group").find("input").get(index);
        //console.log(input_element);

        $(input_element).data("option_action", this.value);
        $(input_element).attr('data-option_action', this.value); //to display it in html after inspect

        //test
        //console.log($(active_item_to_edit).find(".form-group").find("input").data("option_action"));
    });


    /************************************************************************************************* 
     * -- get output as html or json
     *********************************************************************************************/

    //get html
    // $(document).on("click", ".btn-get-html", function () {
    //     var get_html_button = this;

    //     //remove previous html-output holder if any
    //     $(get_html_button).find(".output-html-holder").remove();

    //     //append output-holder as hidden div
    //     $(get_html_button).append('<div class="output-html-holder hidden"></div>');

    //     // iterate inside grid2 form group and clone form-group element
    //     $(".grid-2").find(".html-element-holder").each(function (index) {
    //         //console.log(this); // display form-group element
    //         var form_group = $(this).find(".form-group").clone();
    //         $(get_html_button).find(".output-html-holder").append(form_group); //append form group element
    //     });

    //     //test 
    //     //console.log($(get_html_button).find(".output-html-holder").html());
    // });

    //get output as json  
    $(document).on("click", ".btn-get-json", function () {

        // iterate inside grid2 form group and clone form-group element
        $(".grid-2").find(".html-element-holder").find(".form-group").each(function (index) {
            //console.log(this); // display form-group element
            // get input element type store in data attribute
            var data_element_type = $(this).data("element-type");
            console.log(data_element_type);

            //get first label text
            var label_text = $(this).find("label").first().text();
            console.log(label_text);

            if (data_element_type == "textfield") {

            }
        });

    });


    let form_array = []
    // let form = 1
    // layout 2 JS 
    // add question to form
    $(document).on('click', '.add_question', function () {
        console.log(form)
        if( form <= 10){
            if ($('.survey_form').is(':empty')) {
                $('.survey_form').html(`<div class="row form_div editing">
                                        <button type="button" class="btn remove legitRipple close">
                                        <i class="far fa-times-circle"></i>
                                        </button>
                                        <div class="col-md-1 que_no">Q<span class="qno">${form}</span></div>
                                        <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style=""> Select Question Type </div>
                                        </div>`)
                form_array.push(`question_${form}`)
                form = form + 1;
            } else {
                $('.form_div').removeClass('editing')
                $('.survey_form').append(`<div class="row form_div editing">
                                        <button type="button" class="btn remove legitRipple close">
                                            <i class="far fa-times-circle"></i>
                                        </button>
                                        <div class="col-md-1 que_no">Q<span class="qno">${form}</span></div>
                                        <div class="col-md-10 form_content" id="question_${form}" tabindex="-1" style=""> Select Question Type </div>
                                        </div>`)
                form_array.push(`question_${form}`)
                form = form + 1;
            }
            $('.custom_ques_set').css('display', 'block')
            $('.custom_ques_set input[type="radio"]').each(function(){
                $("input:radio").attr("checked", false); 
            });
        } else {
            swal({
                title: "Limit Alert!",
                text: "Custom question limit reahced.",
                icon: "warning",
                button: 'ok',
            });
        }
    })
    //

    // add question type
    $(document).on('click', '.questionType', function () {
        var element = document.getElementsByName($(this).attr('id'))
        if ($('.editing').length > 0){
            // if ($('.editing .form_content:last').is(':empty')) {
            if ($('.editing .form_content:last').html() == " Select Question Type "){
                $('.editing .form_content:last').empty()
                $('.editing .form_content:last').append(`<div class="panel question_block" >
                                                    <div class="panel-body"> ${element[0].innerHTML}
                                                    </div>
                                                    </div>`)
    
            } else {
                swal({
                    title: "Are you sure ?",
                    text: "You are about to change custom question. Data will not be recovered.",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((willDelete) => {
                    if (willDelete) {
                        $('.editing .form_content:last').html(`<div class="panel question_block" >
                        <div class="panel-body"> ${element[0].innerHTML}
                        </div>
                        </div>`)
                    }
                });
            }
        } else{
            if ($('.form_div').length > 0){
                swal("Error!","Select question.","error");
            } else {
                swal("Error!","Please add question.","error");
            }
        }
    })
    //

    // switching through questions
    $(document).on('click', '.form_div', function () {
        $('.form_div').removeClass('editing')
        $(this).addClass('editing')
        $('.custom_ques_set').css('display', 'block')
        $('.custom_ques_set input[type="radio"]').each(function(){
            $("input:radio").attr("checked", false); 
        });
    })
    //

    
    // removing questions and shffule the sequesnce
    $(document).on('click', '.remove', function () {
        qno = parseInt($(this).next('.que_no').text().split('Q')[1])
        // form > 10 ? form -= 1 : form
        console.log('before remove '+form)
        if($(this).parent().find('.form_content').text() != " Select Question Type "){
            swal({
                title: "Are you sure ?",
                text: "You are about to delete question. Data will not be recovered.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            }).then((willDelete) => {
                if (willDelete) {
                    if (qno == form){
                        // form -= 1
                        this.closest('.form_div').remove()
                        $('.custom_ques_set').css('display', 'none')
                    } else {
                        dele=this.closest('.form_div')
                        new_F = 1
                        $(this).parent().siblings('.form_div').each(function(){
                            $(this).find('.qno').text(new_F)
                            // $(this).css('background-color','aqua')
                            new_F +=1                    
                        })
                        dele.remove()
                        form == 1 ? form : form -= 1
                    }
                }
            })
        } else {
            if (qno == form){
                // form -= 1
                this.closest('.form_div').remove()
                $('.custom_ques_set').css('display', 'none')
            } else {
                dele=this.closest('.form_div')
                new_F = 1
                $(this).parent().siblings('.form_div').each(function(){
                    $(this).find('.qno').text(new_F)
                    // $(this).css('background-color','aqua')
                    new_F +=1                    
                })
                dele.remove()
                form == 1 ? form : form -= 1
            }
        }
        
        console.log('after remove '+form)
        question_form = [];
    })
    //

    // removing options from questions
    $(document).on("click", ".delete-option", function () {
        if ($(this).parent().find('div.dropdown_list').attr('class') == 'dropdown_list'){
            $(this).parent().find('div.dropdown_list').find('select').children().last().remove()
            if ($(this).parent().find('div.dropdown_list').has('ul'))
            {
                $(this).parent().find('div.dropdown_list').find('ul').children().last('li').remove()
                $(this).parent().find('div.dropdown_list').find('ul').children().last().remove()
            }    
        }else{
            console.log(this.parentElement.lastElementChild.lastElementChild)
            if (!this.parentElement.lastElementChild.lastElementChild.classList.contains('question_text')){
                this.parentElement.lastElementChild.lastElementChild.remove()
            }
        }
        
    });
    //
    
    // converting dropdown to unordered list for multiple edtiting options
    $(document).on('click','.edit_dropdown',function(){
        $list = $('<ul />');    
        $(this).prev().append($list)
        $(this).prev().children().find('option').each(function(){
            $list.append(`<li class="col-md-6" contenteditable="true" value="${$(this).attr('value')}">${$(this).text()}</li>
                <label class="col-md-6"><input type="checkbox" name="valid_option" data-option_action=""> valid</label>`);
        });
        $(this).prev().children().first().css('display','none')
        $(this).css('display','none')
        $(this).parent().parent().find('.save-dropdown').css('display','block')
    })
    //
    
    //saving dropdown options from list items
    $(document).on('click','.save-dropdown',function(){
        if($(this).parent().find('div.dropdown_list').children('ul')){
            let _this = $(this).parent().find('div.dropdown_list').children('select')
            _this.children().remove()

            $(this).parent().find('div.dropdown_list').children('ul').find('li').each(function(){
                _this.append('<option value="'+$(this).attr('value')+'">'+$(this).text()+'</option>')
            })
            _this.css('display','block')
            $(this).parent().find('div.dropdown_list').children('ul').remove()
            $('.edit_dropdown').css('display','block')
            $(this).css('display','none')
        }
    })
    // 

    // downloading form question html file.
    function downloadInnerHtml(filename, elId, mimeType) {
        var elHtml = document.getElementById(elId).innerHTML
        var link = document.createElement('a');
        mimeType = mimeType || 'text/plain';
    
        link.setAttribute('download', filename);
        link.setAttribute('href', 'data:' + mimeType  +  ';charset=utf-8,' + encodeURIComponent(elHtml));
        link.click(); 
    }
    
    var fileName =  'form.html';

    $('.btn-get-html').click(function(){

        // downloadInnerHtml(fileName, 'final','text/html');
        // $('#final').empty()
        
    });
    //
    
    //prepare file for download
    $('.make-ready').click(function(){
        $('#final').empty()

        view_code()
        // $(`.survey_form`).addClass('hidden')
        $('#final').removeClass('hidden')


        // $('.btn-get-html').removeAttr('disabled')
    })
    //


    function view_code(){
        $(`.survey_form`).find('.form_div').each(function(){
            que_no = $(this).find('div.que_no').text();
            question = $(this).find('div.form-group').html(); 
            que_type = $(this).find('div.form-group').data('elementType')

            // console.log(que_no,question);
            $('#final').append(`
                <div class="row question" id="${que_no}" data-type="${que_type}">
                <div class="col-md-1">${que_no}</div>
                <div class="col-md-11">${question}</div>
                </div>
            `)
            $('#final').find('.editable').each(function(){
                $(this).removeAttr('contenteditable')
            })
            $('#final').find('.edit_dropdown').remove()
            $('#final').find('.editable').removeClass('editable')
            $('#final').find('.valid').remove()

        })
    }
});
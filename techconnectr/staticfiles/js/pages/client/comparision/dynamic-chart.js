$(document).ready(function(){

	// set chart data
	chartData = [{
	  "country": "India",
	  "visits": 1882
	}, 
	{
	  "country": "Japan",
	  "visits": 1809
	}, 
	{
	  "country": "Germany",
	  "visits": 1322
	}, 
	{
	  "country": "UK",
	  "visits": 1122
	}, 
	{
	  "country": "France",
	  "visits": 1114
	}, 
	{
	  "country": "China",
	  "visits": 984
	}];

	// create serial chart
	chart = AmCharts.makeChart("chartdiv", {
	  "hideCredits":true,
	  "type": "serial",
	  "theme": "light",
	  "marginRight": 70,
	  "startDuration": 1,
	  "fontSize": 13,
	  
	  "dataProvider": chartData,

	  //
	  
	  "graphs": [{
		"balloonText": "<b>[[category]]: [[value]]</b>",
		"fillAlphas": 0.9,
		"lineAlpha": 0.2,
		"type": "column",
		"valueField": "visits"
	  }],
	  "chartCursor": {
		"categoryBalloonEnabled": false,
		"cursorAlpha": 0,
		"zoomable": false
	  },
	  "categoryField": "country",
	  "categoryAxis": {
		"gridPosition": "start"
	  },
	  "export": {
		"enabled": true
	  }
	});

	// create pie chart
	chart2 = AmCharts.makeChart("chartdiv2", {
	  "hideCredits":true,
	  "type": "pie",
	  "startDuration": 1,
	  "fontSize": 13,
	  
	  "dataProvider": chartData,
	  
	  "valueField": "visits",
	  "titleField": "country"
	});
       
});

function setType(type) {
	  switch (type) {
		case 'line':
		  chart.graphs[0].type = 'line';
		  chart.graphs[0].lineAlpha = 1;
		  chart.graphs[0].fillAlphas = 0;
		  chart.validateNow();
		  break;
		case 'area':
		  chart.graphs[0].type = 'line';
		  chart.graphs[0].lineAlpha = 1;
		  chart.graphs[0].fillAlphas = 0.8;
		  chart.validateNow();
		  break;
		case 'column':
		  chart.graphs[0].type = 'column';
		  chart.graphs[0].lineAlpha = 0;
		  chart.graphs[0].fillAlphas = 0.8;
		  chart.validateNow();
		  break;
		case 'pie':
		  document.getElementById('chartdiv').style.display = 'none';
		  document.getElementById('chartdiv2').style.display = 'block';
		  chart2.invalidateSize();
		  chart2.animateAgain();
		  return;
	  }

	  document.getElementById('chartdiv2').style.display = 'none';
	  document.getElementById('chartdiv').style.display = 'block';
	  chart.invalidateSize();
	  chart.animateAgain();

}
    
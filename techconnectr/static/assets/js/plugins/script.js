$(document).ready(function() {
    $(".my-progress-bar").circularProgress({
        line_width: 6,
        color: "{{color}}",
        starting_position: 0, // 12.00 o' clock position, 25 stands for 3.00 o'clock (clock-wise)
        percent: 0, // percent starts from
        percentage: true,
        text: "complete"
    }).circularProgress('animate', {{day_per}}, 2000);
});
$(document).ready(function() {
    $(".my-progress-bar1").circularProgress1({
        line_width: 6,
        color: "#0372c6",
        starting_position: 0, // 12.00 o' clock position, 25 stands for 3.00 o'clock (clock-wise)
        percent: 0, // percent starts from
        percentage: true,
        text: "   Delay"
    }).circularProgress1('animate', {{days}}, 2000);
});

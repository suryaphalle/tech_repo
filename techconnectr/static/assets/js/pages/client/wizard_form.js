/* ------------------------------------------------------------------------------
*
*  # Form wizard
*
*  Specific JS code additions for wizard_form.html page
*
*  Version: 1.1
*  Latest update: Dec 25, 2015
*
* ---------------------------------------------------------------------------- */

$(function() {


    // Wizard examples
    // ------------------------------

    // Basic setup
    $(".form-basic").formwizard({
        disableUIStyles: true,
        disableInputFields: false,
        inDuration: 150,
        outDuration: 150
    });



    // Initialize plugins
    // ------------------------------

    // Select2 selects
    $('.select').select2();


    // Simple select without search
    $('.select-simple').select2({
        minimumResultsForSearch: Infinity
    });


    // Styled checkboxes and radios
    $('.styled').uniform({
        radioClass: 'choice'
    });


    // Styled file input
    $('.file-styled').uniform({
        fileButtonClass: 'action btn bg-blue'
    });


    // Uncomment if you use styled checkboxes/radios to update them dynamically when step changed
    $(".form-basic, .form-validation, .form-add-steps, .form-ajax").on("step_shown", function(event, data){
        //$.uniform.update();
    });
    
});


$(function () {
    //alert("Inside example.js");
    var grid1 = new Muuri('.grid-1', {
        dragEnabled: true,
        dragSort: function () {
            return [grid1, grid2]
        }
    });
    
    var grid2Hash = {};
    var grid2 = new Muuri('.grid-2', {
        dragEnabled: true,
        dragContainer: document.body,
        dragSort: true
    })
    .on('receive', function (data) {
            grid2Hash[data.item._id] = function (item) {
                if (item === data.item) {
                    var clone = cloneElem(data.item.getElement());
                    data.fromGrid.add(clone, { index: data.fromIndex });
                    data.fromGrid.show(clone);
                }
            };
            grid2.once('dragReleaseStart', grid2Hash[data.item._id]);
    })
    .on('send', function (data) {
            var listener = grid2Hash[data.item._id];
            if (listener) {
                grid2.off('dragReleaseStart', listener);
            }
    })
});



function cloneElem(elem) {
    var clone = elem.cloneNode(true);
    clone.setAttribute('style', 'display:none;');
    clone.setAttribute('class', 'item');
    clone.children[0].setAttribute('style', '');
    return clone;
}

$(function(){
        var droparea_itemContainers = [].slice.call(document.querySelectorAll('.droparea-board-column-content'));
        droparea_columnGrids = [];// global var - without var keyword
        var droparea_boardGrid; 

        // Define the column droparea_grids so we can drag those
        // items around.
        droparea_itemContainers.forEach(function (container) {

		// droparea_grid -- global variable to access inside another js
        // Instantiate column droparea_grid.
        droparea_grid = new Muuri(container, {
            items: '.droparea-board-item',
            layoutDuration: 400,
            layoutEasing: 'ease',
            dragEnabled: true,
            dragSort: function () {
            return droparea_columnGrids;
            },
            dragSortInterval: 0,
            dragContainer: document.body,
            dragReleaseDuration: 400,
            dragReleaseEasing: 'ease'
        })
        .on('dragStart', function (item) {
            // Let's set fixed widht/height to the dragged item
            // so that it does not stretch unwillingly when
            // it's appended to the document body for the
            // duration of the drag.
            item.getElement().style.width = item.getWidth() + 'px';
            item.getElement().style.height = item.getHeight() + 'px';
        })
        .on('dragReleaseEnd', function (item) {
			// called when sorted inside same div
            // Let's remove the fixed width/height from the
            // dragged item now that it is back in a droparea_grid
            // column and can freely adjust to it's
            // surroundings.
			
			//alert("dragReleaseEnd"); 
			//alert(jQuery.type(item)); // object detected
			//convert to array to print in better way
			//var arr = Object.keys(item).map(function (key) { return item[key]; });
			//alert(jQuery.type(arr));
			//console.log(arr);
			//droparea_grid.remove(item, {removeElements: true}); // remove element
			
            item.getElement().style.width = '';
            item.getElement().style.height = '';
            // Just in case, let's refresh the dimensions of all items
            // in case dragging the item caused some other items to
            // be different size.
            droparea_columnGrids.forEach(function (droparea_grid) {
				droparea_grid.refreshItems();
            });
			
        })
        .on('layoutStart', function () {
            // Let's keep the board droparea_grid up to date with the
            // dimensions changes of column droparea_grids.
            droparea_boardGrid.refreshItems().layout();
        });

        // Add the column droparea_grid reference to the column droparea_grids
        // array, so we can access it later on.
        droparea_columnGrids.push(droparea_grid);

        });

        // Instantiate the board droparea_grid so we can drag those
        // columns around.
        droparea_boardGrid = new Muuri('.droparea-board', {
        layoutDuration: 400,
        layoutEasing: 'ease',
        dragEnabled: true,
        dragSortInterval: 0,
        dragStartPredicate: {
            handle: '.droparea-board-column-header'
        },
        dragReleaseDuration: 400,
        dragReleaseEasing: 'ease'
        });
  });
    
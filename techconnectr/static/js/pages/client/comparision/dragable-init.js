$(function(){
        var itemContainers = [].slice.call(document.querySelectorAll('.board-column-content'));
        var columnGrids = [];
        var boardGrid;

        // Define the column grids so we can drag those
        // items around.
        itemContainers.forEach(function (container) {

        // Instantiate column grid.
        var grid = new Muuri(container, {
            items: '.board-item',
            layoutDuration: 400,
            layoutEasing: 'ease',
            dragEnabled: true,
            dragSort: function () {
            return columnGrids;
            },
            dragSortInterval: 0,
            dragContainer: document.body,
            dragReleaseDuration: 400,
            dragReleaseEasing: 'ease'
        })
        .on('dragStart', function (item) {
            // Let's set fixed widht/height to the dragged item
            // so that it does not stretch unwillingly when
            // it's appended to the document body for the
            // duration of the drag.
            item.getElement().style.width = item.getWidth() + 'px';
            item.getElement().style.height = item.getHeight() + 'px';
        })
        .on('dragReleaseEnd', function (item) {
            // Let's remove the fixed width/height from the
            // dragged item now that it is back in a grid
            // column and can freely adjust to it's
            // surroundings.
            item.getElement().style.width = '';
            item.getElement().style.height = '';
            // Just in case, let's refresh the dimensions of all items
            // in case dragging the item caused some other items to
            // be different size.
            columnGrids.forEach(function (grid) {
            grid.refreshItems();
            });
			
			//nivratti 
			//alert("dragReleaseEnd");
			//console.log(item);
			//grid.remove(item, {removeElements: true}); // remove element
			
			// ======================================================
			// Move an item into another grid. == droparea grid
			//=======================================================
			// Do something after the item has been sent and the layout
			// processes have finished.
			//gridA.send(0, gridB, -1, {..});
			grid.send(item, droparea_grid, -1, {
			  layoutSender: function (isAborted, items) {
				//alert("layoutSender");
			  },
			  layoutReceiver: function (isAborted, items) {
				//alert("layoutReceiver"); // first called
				// place item in proper place = manage layout
				droparea_grid.layout();
                //console.log(item.getElement());
				/*
				* item.getElement() 
				* -- returns an dome element 
				* so we can use it as jquery selector
				*/
				var item_element = item.getElement();
				//$(item_element).find( "span" ).css("color", "red");
				//$(item_element).find( "span" ).append("<b>Appended text</b>");
				// call function to append html
				append_html_on_drop(item_element);
			  }
			});
			
        })
        .on('layoutStart', function () {
            // Let's keep the board grid up to date with the
            // dimensions changes of column grids.
            boardGrid.refreshItems().layout();
        });

        // Add the column grid reference to the column grids
        // array, so we can access it later on.
        columnGrids.push(grid);

        });

        // Instantiate the board grid so we can drag those
        // columns around.
        boardGrid = new Muuri('.board', {
        layoutDuration: 400,
        layoutEasing: 'ease',
        dragEnabled: true,
        dragSortInterval: 0,
        dragStartPredicate: {
            handle: '.board-column-header'
        },
        dragReleaseDuration: 400,
        dragReleaseEasing: 'ease'
        });
  });
  
  
  function append_html_on_drop(item_element)
  {
	  $(item_element).find( "span" ).css("color", "red"); //testing
	  
	  var heading_vendor_name = $(item_element).find( "span" ).text(); //get text
	  //alert(vendor_name);
	  
	  //file path
	  var path = 'assets/js/pages/client/comparision/load/html/';
	  // append vendor_info.html data to current element
	  //$(item_element).load(  path + "vendor_info.html" );
	  $(item_element).load(  path + "vendor_info2.html" );
	  
	  //change vendor_name by selecting class name of newly added html 
	  $(item_element).$('.vendor-name').append('NN');
  }
    
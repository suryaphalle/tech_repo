
    $( document ).ready(function() {
	
	/** chart 
	   * Campaigns status
	   * Active Campaigns and PAused Campaigns
	***/
	var chart = AmCharts.makeChart("campaignstatus", {
	  "type": "pie",
	  "theme": "light",
	  "hideCredits": true,
	  "labelsEnabled": false,
	  "autoMargins": false,
	  "marginTop": -30,
	  "marginBottom":20,
	  "marginLeft": 30,
	  "marginRight": 20,
	  "legend":{
		"horizontalGap":10,
		"verticalGap":3,
		"maxColumns": 5,
		"position": "bottom",
	  },
	  "titles":[ {
		"align": "left",
		"text": "",
		"size": 16,
	  }],
	  
	  "dataProvider": [{
		"Campaigns": "Active Campaigns",
		"visits": 304,
	  }, 
	  {
		"Campaigns": "Paused Campaigns",
		"visits": 84
	  }],
	  
	  "valueField": "visits",
	  "titleField": "Campaigns",
	  "colorField": "color",
	  "startEffect": "elastic",
	  "startDuration": 2,
	  "labelRadius": 15,
	  "innerRadius": "30%",  
	  "depth3D": 10,
	  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
	  "angle": 15,
	  "export": {
		"enabled": true
	  }
	} );

	
	var chart = AmCharts.makeChart("secondchart", {
	  "type": "pie",
	  "theme": "light",
	  "hideCredits": true,
	  "labelsEnabled": false,
	  "autoMargins": false,
	  "marginTop": -30,
	  "marginBottom":20,
	  "marginLeft": 30,
	  "marginRight": 20,
	  "legend":{
		"horizontalGap":10,
		"verticalGap":3,
		"maxColumns": 5,
		"position": "bottom",
	  },
	  "titles":[ {
		"align": "left",
		"text": "",
		"size": 16,
	  }],
	  
	  "dataProvider": [{
		"Campaigns": "Active Campaigns",
		"visits": 304
	  }, 
	  {
		"Campaigns": "Paused Campaigns",
		"visits": 84
	  }],
	  
	  "valueField": "visits",
	  "titleField": "Campaigns",
	  "startEffect": "elastic",
	  "startDuration": 2,
	  "labelRadius": 15,
	  "innerRadius": "30%",
	  "depth3D": 10,
	  "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
	  "angle": 15,
	  "export": {
		"enabled": true
	  }
	} );

	
	 
    });
if (typeof DEBUG === 'undefined')
	var DEBUG = true;  // false


// on abm, suppression file upload...Akshay G...Date- 18th Nov, 2019
$(function () {
	// on file upload
	$(document).on('change', "#button_abm_company_list_merge", function () {
		upload_abm_suppr_file($(this));
	});
	$(document).on('change', "#button_abm_company_list_overwrite", function () {
		upload_abm_suppr_file($(this));
	});
	$(document).on('change', "#button_abm_company_list_new", function () {
		upload_abm_suppr_file($(this));
	});
	$(document).on('change', "#id_suppression_company_file_merge", function () {
		upload_abm_suppr_file($(this));
	});
	$(document).on('change', "#id_suppression_company_file_overwrite", function () {
		upload_abm_suppr_file($(this));
	});
	$(document).on('change', "#id_suppression_company_file_new", function () {
		upload_abm_suppr_file($(this));
	});
});

function upload_abm_suppr_file($obj){

	var $fileName = $obj.val();
	DEBUG && console.log("$filename:", $fileName);

	$url = $obj.data("url-save-data");
	DEBUG && console.log("url to save file : ", $url);

	// You need to use standard javascript object here for formdata
	// var form = $obj.closest("form")[0]; // or get(0)
	var file_data = $obj[0].files[0];
	
	var $form_data = new FormData(); //Creates new FormData object
	$form_data.append('uploaded_file', file_data);
	//append campaign id to form data
	var $id_campaign = $("#id_campaign").val();
	DEBUG && console.log("$id_campaign : ", $id_campaign);

	// $form_data.append("id_campaign", "Groucho");
	$form_data.append("id_campaign", $id_campaign);

	// get field name and append to form data
	var $field_name = $obj.attr("name");
	DEBUG && console.log("$field_name : ", $field_name);
	$form_data.append("field_name", $field_name);
	// 'type' defines type of operation to perform i.e. merge, overwrite or new(for 1st time file upload)
	let type = $obj.data('type');
	$form_data.append("type", type);
	if (DEBUG) {
		for (var [key, value] of $form_data.entries()) {
			console.log(key, value);
		}
	}
	id = $obj.attr('id');
	// label obj for uploaded file input
	let label_obj = $(`[for = "${id}"]`);
	// for attr of label obj
	let for_attr = $(label_obj).prop('for');
	// make 'for' attr 'null' to prevent user from uploading other file before processing previous uploaded file
	$(label_obj).prop('for', 'null');
	// show disable icon on over on label
	$(label_obj).css('cursor', 'not-allowed');
	$(label_obj).html('<i class="fa fa-spinner fa-spin"></i> Processing');
	let other_label_attr;
	let other_label;
	// if clicked on merge file data
	if (type == 'merge'){
		// make 'for' attr 'null' for other file input's label to prevent user from uploading other file before processing previous uploaded file
		other_label = $(`[for = "button_abm_company_list_overwrite"]`);
		other_label_attr = $(other_label).prop('for');
		$(other_label).prop('for', 'null');
		$(other_label).css('cursor', 'not-allowed');
	}
	// if clicked on overwrite file data
	else if (type == 'overwrite'){
		// make 'for' attr 'null' for other file input's label to prevent user from uploading other file before processing previous uploaded file
		other_label = $(`[for = "button_abm_company_list_merge"]`);
		other_label_attr = $(other_label).prop('for');
		$(other_label).prop('for', 'null');
		$(other_label).css('cursor', 'not-allowed');
	}
	if ($url || $id_campaign) {
		// make ajax call and save data
		$.ajax({
			context: document.body, // access form inside
			url: $url, //'/ajax/validate_username/',
			type: "POST",
			data: $form_data,
			processData: false,  // tell jQuery not to process the data
			contentType: false,  // tell jQuery not to set contentType

			success: function (response) {
				// debugger
				// restore original 'for' attr for other file input's label to make it clickable
				$(other_label).prop('for', other_label_attr);
				$(other_label).css('cursor', '');
				if (response.success) {
					$obj.val('');
					// add checked sign span, if file is successfully uploaded..Akshay G.
					if($obj.closest('.component_data').find('.specs_status').length == 0){
						$obj.closest('.component_data').find('.beefup__head').append(`
							<span class="specs_status pull-right"><i class="far fa-check-circle chk-color" aria-hidden="true"></i></span>
						`)
					}
					if ($obj.closest('.component_data').attr('id') == 'account_based_marketing'){
						$('#validate_abm_status___').attr('data-abm_status', '1');
					}
					let files = response['file_paths'];
					id = $obj.attr('id');
					// for 1st time file uploading
					if (type == 'new'){
						abm_suppr_buttons($obj, $field_name, files);
						$obj.remove();
						$(label_obj).remove();
					
					}else{
						// for merge or overwrite
						hide_remove_file_button(type, for_attr, label_obj);
					}
				}
				else {
					$obj.val('');
					hide_remove_file_button(type, for_attr, label_obj);
					$.confirm({
						title: 'Encountered an error!',
						content: response.message,
						type: 'red',
						typeAnimated: true,
					});
				}
			},
			error: function (jqXHR, exception) {
	
				var msg = '';
				if (jqXHR.status === 0) {
					msg = 'Server Unreachable.\n 1) Verify Network. \n 2) Or Server Down';
				}
				else if (jqXHR.status == 404) {
					msg = 'Requested page not found. [404]';
				}
				else if (jqXHR.status == 500) {
					msg = 'Internal Server Error [500].';
				}
				else if (exception === 'parsererror') {
					msg = 'Requested JSON parse failed.';
				}
				else if (exception === 'timeout') {
					msg = 'Time out error.';
				}
				else if (exception === 'abort') {
					msg = 'Ajax request aborted.';
				}
				else {
					msg = 'Uncaught Error.\n' + jqXHR.responseText;
				}
				$obj.val('');
				$(other_label).prop('for', other_label_attr);
				$(other_label).css('cursor', '');
				hide_remove_file_button(type, for_attr, label_obj);
				$.confirm({
					title: 'Encountered an error!',
					content: msg,
					type: 'red',
					typeAnimated: true,
				});
			},
		})

		// hide previous_selected_file div
		$obj.closest("form").parent().find(".previous_selected_file").addClass("hidden");
	}
	else {
		console.log("Skipping ajax call because either $url or $id_campaign is empty");
		$obj.val('');
		// hide remove selected file button for abm & suppression ...Akshay G.
		hide_remove_file_button(type, for_attr, label_obj)
		$(other_label).prop('for', other_label_attr);
		$(other_label).css('cursor', '');
	}
}

// add merge and overwrite button on first file upload for abm & suppr..Akshay G...Date- 18th Nov, 2019
function abm_suppr_buttons($obj, $field_name, files){
	if ($field_name == 'abm_company_file'){
		$obj.closest('div').append(`
			<a href="${files['abm_path']}" download title="uploaded file">
				<span class="spn-color">
					<i class="fas fa-download"></i>
				</span>
			</a>
			<label class="btn btn-primary" for="button_abm_company_list_merge">merge</label>
			<input type="file" accept=".csv, .xlsx" name="abm_company_file"
				data-url-save-data="/client/upload_single_file/" data-type="merge"
				id="button_abm_company_list_merge" class="data_file hidden">
			
			<label class="btn btn-primary" for="button_abm_company_list_overwrite">overwrite</label>
			<input type="file" accept=".csv, .xlsx" name="abm_company_file"
				data-url-save-data="/client/upload_single_file/" data-type='overwrite'
				id="button_abm_company_list_overwrite" class="data_file hidden">
			
		`)
	}
	else{
		$obj.closest('div').append(`
			<a href="${files['suppression_path']}" download title="uploaded file">
				<span class="spn-color">
					<i class="fas fa-download"></i>
				</span>
			</a>
			<label class="btn btn-primary" for="id_suppression_company_file_merge">
				merge
			</label>
			<input type="file" accept=".csv, .xlsx" name="suppression_company_file"
				data-url-save-data="/client/upload_single_file/"
				id="id_suppression_company_file_merge" data-type="merge" class="data_file hidden">
			
			<label class="btn btn-primary" for="id_suppression_company_file_overwrite">
				overwrite
			</label>
			<input type="file" accept=".csv, .xlsx" name="suppression_company_file"
				data-url-save-data="/client/upload_single_file/" data-type='overwrite'
				id="id_suppression_company_file_overwrite" class="data_file hidden">
			
		`)
	}
	
}

// hide 'remove' selected file button for abm & suppression ..Akshay G...Date- 18th Nov, 2019
function hide_remove_file_button(type,for_attr, label_obj){
	let text;
	if (type == 'new'){
		text = 'upload';
	}else if (type == 'merge'){
		text = 'merge';
	}else{
		text = 'overwrite';
	}
	$(label_obj).text(text);
	// restore original 'for' attr for uploaded file input's label to make it clickable
	$(label_obj).prop('for', for_attr);
	$(label_obj).css('cursor', '');
}
from django.contrib import admin
from django.apps import apps
#from import_export import ImportExportModalAdmin
from .models import *

# Register your models here.
app= apps.get_app_config('vendors')
#@admin.register()
for model_name,model in app.models.items():
	admin.site.register(model)

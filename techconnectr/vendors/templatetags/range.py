from django import template
import datetime

import ast
from django.template.defaultfilters import stringfilter
from campaign.models import *
from decimal import Decimal
from dateutil.parser import parse

register = template.Library()


@register.filter(name="range")
def _range(_min, args=None):
    _max, _step = None, None
    if args:
        if not isinstance(args, int):
            _max, _step = map(int, args.split(","))
        else:
            _max = args
    args = filter(None, (_min, _max, _step))
    return range(*args)


@register.filter
@stringfilter
def trim(value):
    return value.strip()


@register.filter
def listTostring(list1):

    if len(list1) > 0:
        return ", ".join(list1)
    return ""


# abhi 2-09-2019
# have check internal qa team create header or not
@register.filter
def checkCustomHeader(header, list):
    if "qa_headers" in list[0].keys():
        return True
    return False


# abhi 2-09-2019
# get all type of lead status like client or internal QA team.
@register.filter
def getLeadStatus(header, list):
    if (
        "qa_headers" in list.keys()
        and list["status"] == 0
        and list[list["qa_headers"][0]] != 1
    ):
        return list[list["qa_headers"][0]]
    return list["status"]


# abhi 2-09-2019
# get all type of lead status desc like client or internal QA team.
@register.filter
def getLeadReasonDesc(header, list):
    if (
        "qa_headers" in list.keys()
        and list["status"] == 0
        and list[list["qa_headers"][0]] != 1
    ):
        return list[list["qa_headers"][0] + "reason_desc"]
    return list["lead_desc"]


@register.filter
def checkKeyInList(header, list):
    if "qa_headers" in list[0].keys():
        if header in list[0]["reason_list"]:
            return False
    return True


@register.filter
def checkInList(header, list):
    if "qa_headers" in list.keys():
        if header in list["qa_headers"]:
            return True
        else:
            return False


@register.filter
def add_current_year(int_value, digits=4):
    if digits == 2:
        return "%02d" % (int_value - datetime.now().year - 2000)
    return "%d" % (int_value - datetime.now().year)


@register.filter
def minus_current_year(int_value, digits=4):
    if digits == 2:
        return "%02d" % (datetime.now().year - 2000 - int_value)
    return "%d" % (datetime.now().year - int_value)


@register.filter
def minus(num1, num2):
    return num1 - num2


@register.filter
def timestampTotime(timestamp):
    return datetime.fromtimestamp(int(timestamp)).strftime("%H:%M:%S")


@register.filter
def change_type(dictionary, key):
    dict = ast.literal_eval(dictionary)
    return dict.get(str(key))


@register.filter
def get_key_from_dict(dictionary):
    dict = ast.literal_eval(dictionary)
    print(list(dict.keys()))
    return dict.keys()


@register.filter()
def get_data_frm_object(data_assesment, id):
    print("hii")
    for row in data_assesment:
        print(row)
    return id


@register.filter()
def revenue(volume, cpl):
    if volume and cpl:
        return round(Decimal(volume * cpl), 2)


@register.filter()
def dateFormate(date):
    dt = parse(str(date))
    return dt.strftime("%b %d %Y")


@register.filter()
def leadlist(key, dict):
    if key in dict:
        if key == "date":
            date = datetime.strptime(dict[key], "%B %d %Y").date()
            # return date.strftime('%b %d %Y')
            return date.strftime("%m/%d/%Y")
        else:
            return dict[key]


@register.filter()
def camp_header(camp_id, header):
    header_data = Delivery.objects.get(campaign_id=camp_id)
    camp_header = str(header_data.data_header)
    header_list = list(camp_header.split(","))
    for head in header_list:
        del header_list[0]
        return head


@register.filter
def in_list(value, the_list):
    value = str(value)
    return value in the_list.split(",")


@register.filter
def vendorlistsplit(vendor_list, value):
    newlist = []
    for vendor in vendor_list:
        if value in vendor["vendor_type"]:
            newlist.append(vendor)
    return newlist


@register.filter
def IncCplwith25(cpl):
    return cpl + cpl * 25 / 100


@register.filter
def DecCplwith25(cpl):
    return cpl - (cpl * 25 / 100)


@register.filter
def tc_amt(camp):
    vendor_amt = camp.vcpl * camp.lead
    rev_share = vendor_amt * (camp.rev_share_percentage / 100)
    diff = (camp.ccpl - camp.vcpl) * camp.lead
    return rev_share + diff


@register.filter
def tc_per(camp):
    return tc_amt(camp) / (camp.ccpl * camp.lead) * 100


@register.filter
def get_reason(reason):
    return eval(reason)['reason']

@register.filter
def display_step(value):
    if value == 'internal':
        return "Internal QA Approve"
    elif value == 'external':
        return "External QA Approve"
    elif value == 'client':
        return "Client Approve"

@register.filter
def check_country(value,clist):
    if clist != '' and clist != None and clist != '[]':
        return value in [int(i) for i in eval(clist)]
    else:
        return False

@register.filter
def check_loc(selected,id):
    if selected:
        return id in [d.id for d in selected]
    else:
        return False
from django.shortcuts import get_object_or_404, render, redirect
from django.conf import settings
from django.urls import resolve
from django.http import HttpResponse, JsonResponse
from django.core.files.storage import FileSystemStorage
import ast
import datetime
from campaign.models import *
from setupdata.models import *
from client_vendor.models import *
from user.models import *
from django.core import serializers
from client.models import *
from client.utils import get_logos


def campaign_chat_screen(request, camp_id):
    """ Campaign Chat Screen """
    camp = Campaign.objects.get(id=camp_id)
    camp_name = Campaign.objects.all()
    campaign_list = campaign_allocation.objects.filter(
        campaign_id__in=camp_name, client_vendor_id=request.session['userid'], status__in=[1,4,5])
    return render(request, 'campaign_chat/vendor_chat_screen.html', {'sender_id': request.session['userid'], 'campaign_list': campaign_list, 'campaign': camp})


def get_campaign_chat(request):
    """ Get Campaign Chat """
    data = {'success': 1}
    if request.method == "POST" :
        camp_id = request.POST.get('camp_id')
        if Campaign_chats.objects.filter(campaign_id=camp_id).count() > 0:
            chats = Campaign_chats.objects.filter(campaign_id=camp_id)
            data = get_chats_into_dict(chats, request.session['userid'])
            return JsonResponse(data, safe=False)
    return JsonResponse(data)


def get_chats_into_dict(chats, userid):
    """ Get Charts Into Dectionary """
    chat_data = []
    import os
    for msg in chats:
        data = ast.literal_eval(msg.data)
        for chat_msg in data:
            if userid in chat_msg['receiver_ids']:
                if user.objects.filter(email=chat_msg['sender_name']).values('anonymous_status')[0]['anonymous_status']:
                    sender = f"#TC-{user.objects.filter(email=chat_msg['sender_name']).values('id')[0]['id']}"
                    profile_pic  = os.path.join(settings.BASE_URL, 'media', 'default.jpg')
                else:
                    sender = user.objects.filter(email=chat_msg['sender_name']).values('user_name')[0]['user_name']
                    profile_pic = chat_msg['profilepic']
                chat_data.append({
                    'sender_name': sender,
                    'profilepic': profile_pic,
                    'sender_id': chat_msg['sender_id'],
                    'message': chat_msg['message'],
                    'time': chat_msg['time'],
                    'date': chat_msg['date'],
                    'media':chat_msg['media'],
                    'title':chat_msg['title'],
                    'chat_obj_id': msg.id,
                })
    return chat_data


def send_msg(request):
    """ Send Message """
    ids = []
    media=""
    title=""
    image = ''
    doc = ''
    link = ''
    sender_name=request.session['username']
    client_id = int(request.POST.get('client_id'))
    msg1 = request.POST.get('message')
    msg = msg1.replace("<","&lt;");
    camp_id = request.POST.get('camp_id')
    camp_alloc_id=request.POST.get('camp_alloc_id')
    # superadmin_id = user.objects.get(usertype_id=usertype.objects.filter(
    #     type='superadmin').values_list('id')[0])

    if len(Find(msg)) > 0:
        link = msg

    # ids.append(superadmin_id.id)

    ids.append(request.session['userid'])
    camp = campaign_allocation.objects.get(id=camp_alloc_id)

    if camp.status != 3:
        ids.append(client_id)

    superadmins = [i.id for i in user.objects.filter(usertype_id=4)]
    ids.extend(superadmins)
    ids = list(set(ids))

    if store_msg(request.session['userid'], ids, msg,media,image,doc,link,title, camp_id,sender_name):
        data = {'success': 1}
    return JsonResponse(request.POST)


def store_msg(sender_id, receiver_ids, msg,media,image,doc,link,title, camp_id,sender_name):
    """ Store Message """
    last_dict = {}
    list_count = 0
    time = datetime.datetime.now().strftime('%H:%M:%S')
    import os
    from client.utils import fetch_default_logo
    profilepic = fetch_default_logo(sender_id)
    if client_vendor.objects.filter(user_id=sender_id).count() > 0:
        profile = client_vendor.objects.get(user_id=sender_id)
        if profile.company_logo:
            try:
                profilepic = str(profile.company_logo_smartthumbnail.url)
            except Exception as e:
                pass
    if Campaign_chats.objects.filter(campaign_id=camp_id).count() == 1:
        chats_details = Campaign_chats.objects.get(campaign_id=camp_id)
        list = ast.literal_eval(chats_details.data)
        last_dict = list[-1]
        receiver_logos = get_logos(receiver_ids)
        list.append({'sender_id': sender_id,'sender_name':sender_name,'receiver_ids': receiver_ids, 'receiver_logos':receiver_logos,'profilepic': profilepic,
                     'message': msg, 'time': time,'media':media,'image':image,'doc':doc,'link':link,'title':title, 'first':1,'date': str(datetime.datetime.today().date()), 'rank': int(last_dict['rank'])+1})
        list_count = len(list)
        chats_details.data = list
        # update sent message count for receiver user ...Akshay G..Date - 11-7-2019
        if chats_details.send_msg_count != None:
            d = eval(chats_details.send_msg_count)
            for id in receiver_ids:
                if id != sender_id:
                    if id in d:
                        if sender_id in d[id]:
                            d[id][sender_id][0] += 1
                            d[id][sender_id].append(int(last_dict['rank'])+1)
                        else:
                            d[id][sender_id] = [1, int(last_dict['rank'])+1]
                        d[id]['count'] += 1
                    else:
                        d[id] = {sender_id: [1, int(last_dict['rank'])+1]}
                        d[id]['count'] = 1
            chats_details.send_msg_count = d
        else:
            d = {}
            for id in receiver_ids:
                if id != sender_id:
                    d[id] = {sender_id: [1,int(last_dict['rank'])+1], 'count': 1}
            chats_details.send_msg_count = d
        chats_details.save()
    else:
        list = []
        receiver_logos = get_logos(receiver_ids)
        list.append({'sender_id': sender_id,'sender_name':sender_name, 'receiver_ids': receiver_ids,'receiver_logos':receiver_logos, 'profilepic': profilepic,
                     'message': msg, 'time': time, 'media':media,'image':image,'doc':doc,'link':link,'title':title,'first':0,'date': str(datetime.datetime.today().date()), 'rank': 1})
        d = {}
        for id in receiver_ids:
            if id != sender_id:
                d[id] = {sender_id: [1, 1], 'count': 1}
        Campaign_chats.objects.create(campaign_id=camp_id, data=list, send_msg_count = d)
    send_campaign_notification(receiver_ids, sender_id, camp_id, list_count)
    return True

#send attachment to users
def send_attchment(request):
    ids=[]
    image = ''
    doc = ''
    link = ''
    if request.FILES:
        sender_id=request.session['userid']
        sender_name=request.session['username']
        camp_id = request.POST.get('camp_id')
        client_id=int(request.POST.get('client_id'))
        title=request.POST.get('title')
        msg="0"

        # superadmin_id = user.objects.get(usertype_id=usertype.objects.filter(
        # type='superadmin').values_list('id')[0])
        # ids.append(superadmin_id.id)

        ids.append(request.session['userid'])
        if campaign_allocation.objects.filter(campaign_id=camp_id,client_vendor_id=request.session['userid'],status__in=[1,4,5]):
            ids.append(client_id)

        superadmins = [i.id for i in user.objects.filter(usertype_id=4)]
        ids.extend(superadmins)
        ids = list(set(ids))

        myfile = request.FILES['filename']
        if myfile.name.split('.')[-1].lower() in ['jpg','png','jpeg','pdf','docx','doc','txt','csv','xlsx','xls']:
            fs = FileSystemStorage()
            filename = fs.save("chat_data/" + myfile.name,  myfile)
            media='/media/'+filename
            if myfile.name.split('.')[-1].lower() in ['pdf','doc','docx','txt','csv','xlsx','xls']:
                doc = '/media/'+filename
            if myfile.name.split('.')[-1].lower() in ['jpg','png','jpeg']:
                image = '/media/'+filename
            store_msg(sender_id,ids,msg,media,image,doc,link,title,camp_id,sender_name)
            return JsonResponse({'status': 1})
        else:
            return JsonResponse({'status': 2,'msg':"Only 'jpg','png','jpeg','pdf','docx','doc','txt','csv','xlsx','xls' formats are allowed. "})
    return JsonResponse({'success': 1})


def send_campaign_notification(receiver_ids, sender_id, camp_id, list_count):
    """ Send Campaign Notification """
    data = []
    receiver_ids.remove(sender_id)
    user_data = user_activities.objects.filter(user_id__in=receiver_ids)
    for row1 in user_data:
        if row1.active_page != 'chat':
            if Campaign_notification.objects.filter(campaign_id=camp_id).count() > 0:
                notification = Campaign_notification.objects.get(
                    campaign_id=camp_id)
                list = ast.literal_eval(notification.data)
                userid = create_userid_list(list)
                if row1.user_id in userid:
                    for row in list:
                        row['all_msg_cnt'] = list_count
                        row['start'] = row['end']
                        row['end'] = list_count
                        row['check'] = 0
                        row['read'] = 0
                else:
                    list.append({'user_id': row1.user_id, 'all_msg_cnt': list_count,
                                 'start': 15, 'end': list_count, 'check': 0, 'read': 0})
                    notification.data = list
                    notification.save()
            else:
                data.append({'user_id': row1.user_id, 'all_msg_cnt': list_count,
                             'start': 0, 'end': list_count, 'check': 0, 'read': 0})
                Campaign_notification.objects.create(
                    campaign_id=camp_id, data=data)

def Find(string):
    # findall() has been used
    # with valid conditions for urls in string
    import re
    url = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', string)
    return url

def create_userid_list(list):
    """ Create User Id List """
    userid = []
    for row in list:
        userid.append(row['user_id'])
    return userid

def get_camp_data_vendor(request):
    """ Get Camppaign Data Vendor """
    camp_details = campaign_allocation.objects.get(id=request.POST.get('camp_alloc_id'))
    aggrement = data_assesment.objects.get(user_id=request.session['userid'])
    # check if user has already uploaded script file for this campaign...Akshay G..Date - 4-9-2019
    script_query = Scripts.objects.filter(
        campaign_id= camp_details.campaign.id, user_id = request.session['userid'])
    script_name = ''
    if script_query:
        script_name = script_query[0].client_script.name
    # fetch vendor agreement data of this campaign..Akshay G..Date- 2-9-2019 
    queryset = ClientCustomFields.objects.filter(client_id = camp_details.campaign.user_id)
    vendor_agreements = []
    new_agreement_data = {}
    old_agreement_data = {}
    if camp_details.vendor_agreements:
        vendor_agreements = eval(camp_details.vendor_agreements)
    if queryset:
        if queryset[0].client_agreements:
            if queryset[0].client_agreements.strip() != '':
                data = ast.literal_eval(queryset[0].client_agreements)[0]
                new_agreement_data = data['new_vendor_agreements']
                old_agreement_data = data['old_vendor_agreements']
    client_agreements_data = []
    agreement_paths = {}
    if len(vendor_agreements) > 0:
        import os
        for d in vendor_agreements:
            for agr_name in d.keys():
                if d[agr_name] == '':
                    if agr_name in new_agreement_data:
                        client_agreements_data.append({agr_name: os.path.join(settings.BASE_URL, new_agreement_data[agr_name])})
                    elif agr_name in old_agreement_data:
                        client_agreements_data.append({agr_name: os.path.join(settings.BASE_URL, old_agreement_data[agr_name])})
                else:
                    agreement_paths[agr_name] = os.path.join(settings.BASE_URL, d[agr_name])
                    client_agreements_data.append({agr_name:''})
    data = {
        'agreement_paths': agreement_paths,
        'client_agreements_data': client_agreements_data,
        'script_name': script_name,
        'rfq_status': camp_details.campaign.rfq,
        'name': camp_details.campaign.name,
        'cpl': '$ '+format(camp_details.cpl, '.2f') if camp_details.cpl else '',
        'volume': camp_details.volume,
        'camp_io':camp_details.campaign.io_number,
        'start_date': camp_details.campaign.start_date,
        'end_date': camp_details.campaign.end_date,
        'expiry_date': camp_details.campaign.rfq_timer,
        'desc': camp_details.campaign.description,
        'camp_id': request.POST.get('id'),
        'sender_id': request.session['userid'],
        'client_id': camp_details.campaign.user_id,
        'nda': aggrement.nda_aggrement,
        'msa': aggrement.msa_aggrement,
        'gdpr': aggrement.gdpr_aggrement,
        'dpa': aggrement.dpa_aggrement,
        'io': aggrement.io_aggrement,
        'nda_path': aggrement.nda_aggrement_path.url if aggrement.nda_aggrement_path != '' else '',
        'msa_path': aggrement.msa_aggrement_path.url if aggrement.msa_aggrement_path != '' else '',
        'gdpr_path': aggrement.gdpr_aggrement_path.url if aggrement.gdpr_aggrement_path != '' else '',
        'dpa_path': aggrement.dpa_aggrement_path.url if aggrement.dpa_aggrement_path != '' else '',
        'io_path': aggrement.io_aggrement_path.url if aggrement.io_aggrement_path != '' else '',
    }
    return JsonResponse(data)

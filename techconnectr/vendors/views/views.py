from django.shortcuts import get_object_or_404, render, redirect, render_to_response
from django.http import HttpResponseBadRequest, Http404
from django.template import RequestContext
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from django.urls import resolve, reverse
import ast
from setupdata.models import *
from vendors.models import *
from client_vendor.models import *
from user.models import user
from django.http import HttpResponse, JsonResponse
from django.core.files.storage import FileSystemStorage
from campaign.models import *
from leads.models import *
from form_builder.models import *
import django_excel as excel
from django import forms
import csv
import codecs
import openpyxl
from django.core import management
# from datetime import datetime
import datetime
from djqscsv import render_to_csv_response
from campaign.forms import CounterCPLForm
from datetime import timedelta,date
from client.models import *
from setupdata.models import countries1 as countries_list
from client.decorators import *
# from datetime import date
from dateutil.relativedelta import relativedelta
from superadmin.utils import collect_campaign_details,cpl_decrease,cpl_increase
from itertools import chain
from operator import itemgetter
from client.utils import percentage, noti_via_mail
from vendors.utils import *
from client_vendor.models import *
from campaign.choices import *
from support.models import *
import json
import os
from django.conf import settings
from django.http import HttpResponse

from django.template.loader import get_template
from PyPDF2 import PdfFileMerger
import shutil
from xhtml2pdf import pisa
from login_register.views import *
from client.views.camp_status import *

# === Download excel file ===
def download(request):
    # Download excel file
    sheet = excel.pe.Sheet([[1, 2], [3, 4]])
    return excel.make_response(sheet, "csv")

# === Notification count ===
def notification(userid):
    # Notification count
    ids = Notification_Reciever.objects.filter(
        receiver=userid, status__in=['0','1']).values_list('Notification', flat=True)
    # notifs details of unread & read notifs..with status=0 & 1 resp
    notification_details = Notification.objects.filter(id__in=ids).order_by('-created')
    # 'ids_' get ids of unread notifs..i.e. having status=0
    ids_ = Notification_Reciever.objects.filter(
        receiver=userid, status = '0').values_list('Notification', flat=True)
    notification_details_ = Notification.objects.filter(id__in=ids_).order_by('-created')
    # get count of unread notifs..i.e. having status=0
    countrow1 = notification_details_.count()
    return {'notify': notification_details, 'notifycount': countrow1}

# === Session expire for vendor ===
@login_required
def session_expire():
    # Session expire for vendor
    return redirect('/logout/')

# === Vendor dashboard ===
@is_vendor
def dashboard(request):
    # Vendor dashboard

    userid = request.session['userid']
    # countries1 = countries.objects.all()
    countries1 = countries_list.objects.all()
    industry = industry_type.objects.all()
    expert = industry_speciality.objects.all()
    notifydic = notification(userid)
    marketing_method = source_touches.objects.filter(is_active=1).all()
    company_size_data = company_size.objects.all()
    capacity_type = source_touches.objects.all()
    onboarding=user.objects.get(id=userid)
    countrow = client_vendor.objects.filter(user_id=userid).count()
    delivery_time1 = delivery_time.objects.all()
    delivery_method1 = delivery_method.objects.all()
    complex_program = level_intent.objects.filter(is_active=1).all()
    registrtions_process1 = registration_process.objects.filter(
        is_active=1).all()
    database_attribute1 = database_attribute.objects.filter(
        is_active=1).all()
    language_supported1 = language_supported.objects.filter(
        is_active=1).all()
    networks_and_publishers1 = networks_and_publishers.objects.filter(
        is_active=1).all()
    vendor_type=VendorType.objects.all()
    currant_campaign_currant_month = no_of_currant_campaign(userid)
    last_month_all_campaign = no_of_last_month_campaign(userid)
    currant_campaign = campaign_allocation.objects.filter(client_vendor_id=userid).order_by('cpl')[:5]
    if countrow == 1:
        client_vendor_detail = client_vendor.objects.get(
            user_id=userid)
        userdetails = user.objects.get(id=userid)
        country=""
        if countries_list.objects.filter(id=userdetails.country).count() > 0:
            country = countries_list.objects.get(id=userdetails.country)
        # country = countries.objects.get(id=userdetails.country)
        state = states1.objects.get(id=userdetails.state)
        # state = states.objects.get(id=userdetails.state)
        industry = client_vendor_detail.industry_type.all()
        speciality = client_vendor_detail.industry_speciality.all()
        counter = data_assesment.objects.filter(user_id=userid).count()
        if counter == 1:
            data_assesment1 = data_assesment.objects.get(
                user_id=userid)
        else:
            data_assesment1 = {}
        return render(request, 'dashboard/vendor_dashboard.html', {'vendor_type':vendor_type,'networks_and_publishers': networks_and_publishers1, 'language_supported': language_supported1, 'onBording':onboarding.status, 'data_assesment': data_assesment1, 'database_attribute': database_attribute1, 'registrtions_process': registrtions_process1, 'complex_program': complex_program, 'delivery_time': delivery_time1, 'delivery_method': delivery_method1, 'capacity_type': capacity_type, 'company_size': company_size_data, 'marketing_method': marketing_method, 'speciality': speciality, 'industry': industry, 'state': state, 'country': country, 'userdetails': userdetails, 'client_vendor': client_vendor_detail, 'expert': expert, 'countries': countries1, 'industry': industry, 'notifycount': notifydic["notifycount"], 'notify': notifydic["notify"], 'current_camp': currant_campaign_currant_month, 'last_month': last_month_all_campaign, 'camp_alloc': currant_campaign})
    else:
        return render(request, 'dashboard/vendor_dashboard.html', {'vendor_type':vendor_type,'onBording': 0, 'database_attribute': database_attribute1, 'registrtions_process': registrtions_process1, 'complex_program': complex_program, 'delivery_time': delivery_time1, 'delivery_method': delivery_method1, 'capacity_type': capacity_type, 'company_size': company_size_data, 'marketing_method': marketing_method, 'expert': expert, 'countries': countries1, 'industry': industry, 'notifycount': notifydic["notifycount"], 'notify': notifydic["notify"],'language_supported': language_supported1 })

# === No Of Currant campaign ===
def no_of_currant_campaign(userid):
    # No Of Currant campaign
    from datetime import datetime
    currant_date = datetime.now()
    currant_end_date = last_day_of_month(
        datetime(currant_date.year, currant_date.month, 17))
    currant_start_date = datetime(
        currant_date.year, currant_date.month, 1)
    campaign_details = Campaign.objects.filter(
        updated_date__gt=currant_start_date, updated_date__lt=currant_end_date)
    current_camp = campaign_allocation.objects.filter(
        campaign_id__in=campaign_details, client_vendor_id=userid, status=1).count()
    return current_camp

# === No Of LastMonth Campaign ===
def no_of_last_month_campaign(userid):
    # No Of LastMonth Campaign
    today = date.today()
    d = today - relativedelta(months=1)
    currant_start_date = date(d.year, d.month, 1)
    # datetime.date(2008, 12, 1)
    currant_end_date = date(today.year, today.month, 1) - relativedelta(days=1)
    # datetime.date(2008, 12, 31)
    # currant_date = datetime.datetime.now()
    # currant_start_date = datetime.datetime(
    #     currant_date.year, currant_date.month-1, 1)
    # currant_end_date = datetime.datetime(
    #     currant_date.year, currant_date.month-1, 30)
    campaign_details = Campaign.objects.filter(
        updated_date__gt=currant_start_date, updated_date__lt=currant_end_date)
    last_month_camp = campaign_allocation.objects.filter(
        campaign_id__in=campaign_details, client_vendor_id=userid, status__in=[1, 4]).count()
    return last_month_camp

# === Currant month campaign ===
def currant_month_campaign(userid):
    # Currant month campaign
    import datetime
    currant_date = datetime.datetime.now()
    currant_start_date = datetime.datetime(
        currant_date.year, currant_date.month, 1)
    currant_end_date = last_day_of_month(
        datetime.date(currant_date.year, currant_date.month, 17))
    campaign_details = Campaign.objects.filter(
        updated_date__gt=currant_start_date, updated_date__lt=currant_end_date)
    return campaign_allocation.objects.filter(campaign_id__in=campaign_details, client_vendor_id=userid)

# === Last day of month ===
def last_day_of_month(date):
    # Last day of month
    if date.month == 12:
        return date.replace(day=31)
    # return date.replace(month=date.month+1, day=1) - datetime.timedelta(days=1)
    return date.replace(month=date.month+1, day=1) - timedelta(days=1)

# === Lead Error List ===
@login_required
def lead_error_list(request,camp_id,camp_alloc_id):
    import datetime
    userid=request.session['userid']
    date=datetime.datetime.today().strftime('%Y-%m-%d')
    lead_data={'success':1}
    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).exists():
        lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
            # store all leads in leads
        leads=[]
        if lead_data.all_lead_count == 0:
            if lead_data.uploaded_lead_count > 0:
                leads=ast.literal_eval(lead_data.uploaded_lead)
            if lead_data.remove_duplicate_lead_csv_cnt > 0:
                leads=leads+ast.literal_eval(lead_data.remove_duplicate_lead_csv)
            if lead_data.remove_lead_header_cnt > 0:
                leads=leads+ast.literal_eval(lead_data.remove_lead_header)
            if lead_data.duplicate_with_vendor_cnt > 0:
                leads=leads+ast.literal_eval(lead_data.duplicate_with_vendor)
            if lead_data.duplicate_with_our_cnt > 0:
                leads=leads+ast.literal_eval(lead_data.duplicate_with_our)
            lead_data.all_lead=leads
            lead_data.all_lead_count=len(leads)
            lead_data.save()
        else:
            leads=ast.literal_eval(lead_data.all_lead)

        list = []
        all_header = []
        all_lead_header = []
        labels=get_lead_header(camp_id)
        labels +=join_custom_question_header(camp_id)
        if len(leads) > 0:
            for dict in leads[0]:
                all_header.append(dict)
                all_lead_header.append(dict)
        return render(request, 'campaign/lead_error_list.html', {'edit_lead_header':labels,'leadlist': leads, 'all_lead_header': all_lead_header, 'all_header': all_header,'camp_id': camp_id,'camp_alloc_id':camp_alloc_id})

# === Update Specialization ===
@is_vendor
def update_specialization(request):
    # Update Specialization
    userid = request.session['userid']
    countrow = specialization.objects.filter(user_id_id=userid).count()
    if countrow > 0:
        specialization_details = specialization.objects.filter(
            user_id_id=userid)
        specialization_details_data = []
        for row in specialization_details:
            Country = countries_list.objects.get(id=row.country_id)
            # Country = countries.objects.get(id=row.country_id)
            state = states1.objects.get(id=row.state_id)
            # state = states.objects.get(id=row.state_id)
            city = cities.objects.get(id=row.city_id)
            specialization_name = industry_speciality.objects.get(
                id=row.specialization_id)
            specialization_details_data.append({
                'specialization_name': specialization_name.name,
                'country': Country.name,
                'state': state.name,
                'city': city.name,
            })

        expert = industry_speciality.objects.all()
        country = countries_list.objects.all()
        # country = countries.objects.all()
        return render(request, 'campaign/vendor_specialization.html', {'specialization_details': specialization_details_data, 'expert': expert, 'country': country})
    else:
        expert = industry_speciality.objects.all()
        country = countries_list.objects.all()
        # country = countries.objects.all()
        return render(request, 'campaign/vendor_specialization.html', {'expert': expert, 'country': country})

# === Update capacity ===
@is_vendor
def update_capacity(request):
    # Update capacity
    userid = request.session['userid']
    type1 = source_touches.objects.all()

    countrow = vendor_capacity.objects.filter(user_id=userid).count()
    if countrow > 0:
        capacity_details = vendor_capacity.objects.filter(user_id=userid)
        capacity_details_data = []
        for row in capacity_details:
            capacity_type = source_touches.objects.get(id=row.type_id)
            capacity_category = level_intent.objects.get(
                id=row.campaign_category_id)
            capacity_details_data.append({
                'Type_name': capacity_type.type,
                'category_name': capacity_category.type,
                'leads': row.no_of_leads,
            })
        return render(request, 'campaign/update_capacity.html', {'capacity_details': capacity_details_data, 'type': type1})
    else:
        return render(request, 'campaign/update_capacity.html', {'type': type1, 'msg': 'Record Not found!..'})

# === Get Update category ===
def get_update_category(request):
    # Get Update category
    type_id = request.POST.get('type_id')
    category_data = []
    type_name = request.POST.get('type_name').lower()
    if 'tele' == type_name:
        category = level_intent.objects.filter(is_active=1)
        for row in category:
            category_data.append({
                'category_id': row.id,
                'category_name': row.type,
            })
        data = {'success': 1, 'category': category_data}
        return JsonResponse(data)
    else:
        data = {'success': 2}
        return JsonResponse(data)

# === Update capacity form ===
def update_capacity_form(request):
    # Update capacity form
    userid = request.session['userid']
    type_id = request.POST.get('type_id')
    category_id = request.POST.get('category_id')
    Leads = request.POST.get('Leads')
    countrow = vendor_capacity.objects.filter(
        user_id=userid, type_id=type_id, campaign_category_id=category_id).count()
    if countrow == 1:
        t = vendor_capacity.objects.get(
            user_id=userid, type_id=type_id, campaign_category_id=category_id)
        t.no_of_leads = Leads
        t.is_active = 1
        t.campaign_category_id = category_id
        t.type_id = type_id
        t.save()
        vendor_capacity_history.objects.create(
            no_of_leads=Leads,
            is_active=1,
            campaign_category_id=category_id,
            type_id=type_id,
            user_id=userid,
        )
        site_url = settings.BASE_URL
        pages = 'vendor/update-capacity/'
        data = {'success': 1, 'site_url': site_url, 'pages': pages}
        return JsonResponse(data)
    else:
        vendor_capacity.objects.create(
            no_of_leads=Leads,
            is_active=1,
            campaign_category_id=category_id,
            type_id=type_id,
            user_id=userid,
        )
        vendor_capacity_history.objects.create(
            no_of_leads=Leads,
            is_active=1,
            campaign_category_id=category_id,
            type_id=type_id,
            user_id=userid,
        )
        site_url = settings.BASE_URL
        pages = 'vendor/update-capacity/'
        data = {'success': 1, 'site_url': site_url, 'pages': pages}
        return JsonResponse(data)

# === Add Specialization form ===
def add_specialization_form(request):
    # Add Specialization form
    userid = request.session['userid']
    country_id = request.POST.get('country_id')
    state_id = request.POST.get('state_id')
    city_id = request.POST.get('city_id')
    specialization_id = request.POST.get('specialization_id')
    specialization.objects.create(
        specialization_id=specialization_id,
        country_id=country_id,
        state_id=state_id,
        city_id=city_id,
        is_active=1,
        user_id_id=userid,
    )
    site_url = settings.BASE_URL
    pages = 'vendor/update-specialization/'
    data = {'success': 1, 'site_url': site_url, 'pages': pages}
    return JsonResponse(data)

# === demo ===
def demo(request):
    userid = request.session['userid']

# === Vendor Account ===
@is_vendor
def vendor_account(request):
    # Vendor Account
    userid = request.session['userid']
    marketing_method = source_touches.objects.all()
    company_size_data = company_size.objects.all()
    capacity_type = source_touches.objects.all()
    company_backgrounds = company_background.objects.all()
    pricing_flexibilitys = pricing_flexibility.objects.all()
    countrow = client_vendor.objects.filter(user_id=userid)
    try:
        obj = ClientCustomFields.objects.get(client_id=request.session['userid'])
        if obj.profile_view == None:
            view_list = eval(SystemDefaultFields.objects.get().profile_view)
        else:
            view_list = eval(obj.profile_view)
    except Exception as identifier:
        view_list = eval(SystemDefaultFields.objects.get().profile_view)
    if countrow:
        users = user.objects.filter(id=userid)
        clients = [item for item in client_vendor.objects.filter(
            user_id__in=users)][0]

        data_acqusitions_data = data_acqusitions.objects.filter(
            user_id_id=userid)
        built = opt_in = bought = bought_values = opt_in_values = built_values = ''
        built_display = opt_in_display = bought_display = 'none'
        for row in data_acqusitions_data:
            if row.type == 'built':
                built = 'checked'
                built_values = row.amt_millions
                built_display = 'block'
            elif row.type == 'opt_in':
                opt_in = 'checked'
                opt_in_values = row.amt_millions
                opt_in_display = 'block'
            elif row.type == 'bought':
                bought = 'checked'
                bought_values = row.amt_millions
                bought_display = 'block'
        
        data_acqusitions_list = {'built_display': built_display, 'opt_in_display': opt_in_display, 'bought_display': bought_display, 'built': built,
                                 'opt_in': opt_in, 'bought': bought, 'bought_values': bought_values, 'opt_in_values': opt_in_values, 'built_values': built_values}
        return render(request, 'account/vendor_account.html', {'pricing_flexibilitys': pricing_flexibilitys, 'data_acqusitions_data': data_acqusitions_list, 'company_backgrounds': company_backgrounds, 'capacity_type': capacity_type, 'company_size': company_size_data, 'marketing_method': marketing_method, 'user_info': clients, 'user': users, 'client_vendor': countrow[0],'view_list':view_list})
    else:
        return render(request, 'account/vendor_account.html', {'pricing_flexibilitys': pricing_flexibilitys, 'company_backgrounds': company_backgrounds, 'capacity_type': capacity_type, 'company_size': company_size_data, 'marketing_method': marketing_method})

# === Vnedor Billing Information ===
@is_vendor
def vendor_billing(request):
    # Vnedor Billing Information
    return render(request,'account/vendor_billing_info.html',{})

# === Vendor bank account deatail ===
@is_vendor
def vendor_bank_details(request):
    # Vendor bank account deatail
    return render(request,'aacount/vendor_bank_account_detail.html',{})

# === Vendor Change Password ===
@is_vendor
def vendor_change_password(request):
    # Vendor Change Password
    return render(request,'account/vendor_change_password.html',{})

# === User Information ===
@is_vendor
def user_info_frm(request):
    # User Information
    userid = request.session['userid']
    built = request.POST.get('built', False)
    opt_in = request.POST.get('optin', False)
    bought = request.POST.get('bought', False)
    if built == '1':
        countrow = data_acqusitions.objects.filter(
            user_id_id=userid, type='built').count()
        if countrow == 1:
            t = data_acqusitions.objects.get(user_id_id=userid, type='built')
            t.amt_millions = request.POST['built_values']
            t.save()
        else:
            data_acqusitions.objects.create(
                type='built', amt_millions=request.POST['built_values'], user_id_id=userid)

    if opt_in == '1':
        countrow = data_acqusitions.objects.filter(
            user_id_id=userid, type='opt_in').count()
        if countrow == 1:
            t = data_acqusitions.objects.get(user_id_id=userid, type='opt_in')
            t.amt_millions = request.POST['opt_in_values']
            t.save()
        else:
            data_acqusitions.objects.create(
                type='opt_in', amt_millions=request.POST['opt_in_values'], user_id_id=userid)
    if bought == '1':
        countrow = data_acqusitions.objects.filter(
            user_id_id=userid, type='bought').count()
        if countrow == 1:
            t = data_acqusitions.objects.get(user_id_id=userid, type='bought')
            t.amt_millions = request.POST['bought_values']
            t.save()
        else:
            data_acqusitions.objects.create(
                type='bought', amt_millions=request.POST['bought_values'], user_id_id=userid)

    t = client_vendor.objects.get(user_id=userid)
    t.lead_per_month = request.POST.get('lead_per_month')
    t.unique_reach_per_month = request.POST.get('unique_reach_per_month')
    t.marketing_method_id = request.POST.get('marketing_method')
    t.save()
    t = user.objects.get(id=userid)
    t.email = request.POST.get('email')
    t.contact = request.POST.get('contact')
    t.save()
    return redirect('/vendor/vendor-account/')

# === update vendor profile ===
@is_vendor
def vendor_profile_view_update(request):
    # update vendor profile
    obj, created = ClientCustomFields.objects.get_or_create(client_id=request.session['userid'])
    new_views = request.POST.getlist('view_opt[]')
    if created:
        view_list = eval(SystemDefaultFields.objects.get().profile_view)
    else:
        if obj.profile_view == None:
            view_list = eval(SystemDefaultFields.objects.get().profile_view)
        else:
            view_list = eval(obj.profile_view)
    for view in view_list:
        if view not in new_views:
            view_list[view] = 0
        else:
            view_list[view] = 1
    obj.profile_view = view_list
    obj.save()
    return redirect('/vendor/vendor-account/')

# === Campaign information form ===
@is_vendor
def comp_info_frm(request):
    # Campaign information form
    if request.session['userid']:
        if request.FILES.get('myfile', False):
            myfile = request.FILES['myfile']
            fs = FileSystemStorage()
            filename = fs.save(myfile.name, myfile)
            uploaded_file_url = fs.url(filename)
            t = client_vendor.objects.get(user_id=request.session['userid'])
            t.company_logo = filename
            t.save()
        t = client_vendor.objects.get_or_create(
            user_id=request.session['userid'])
        t[0].website = request.POST.get('website')
        if request.POST.get('registerd_date')=='':
            t[0].company_registerd_date = None
        else:
            t[0].company_registerd_date = request.POST.get('registerd_date')
        t[0].company_name = request.POST.get('com_name') if request.POST.get('com_name') else None
        t[0].company_size_id = request.POST.get('company_size') if request.POST.get('company_size') else None
        t[0].annual_revenue = request.POST.get('annual_revenue') if request.POST.get('annual_revenue') else None
        t[0].company_email = request.POST.get('email') if request.POST.get('email') else None
        t[0].company_background_id = request.POST.get('company_background') if request.POST.get('company_background') else None
        t[0].pricing_flexibility_id = request.POST.get('pricing_flexibility') if request.POST.get('pricing_flexibility') else None
        t[0].save()
        user_obj = user.objects.get(id=request.session['userid'])
        user_obj.contact = request.POST.get('contact') if request.POST.get('contact') != "" else user_obj.contact
        user_obj.user_name = request.POST.get('com_name') if request.POST.get('com_name') != "" else user_obj.user_name
        user_obj.save()
        return redirect('/vendor/vendor-account/')
    else:
        return redirect('/')

# === Change password success ===
def ChangePwd(request):
    # Change password success
    userid = request.session['userid']
    oldpassword = request.POST.get('opwd')
    # for extr user
    if request.session['usertype'] == 6:
        userid = request.session['ext_userid']
    # suryakant 02-10-2019
    # refactored change password code data
    countrow = user.objects.filter(id=userid)
    if countrow.count() == 1 and password_check(request.POST.get('npwd')) and countrow[0].check_password(oldpassword):
        t = user.objects.get(id=userid)
        t.set_password(request.POST.get('npwd'))
        t.save()
        data = {'success': 1}
        return JsonResponse(data)
    elif countrow.count() == 0:
        data = {'success': 2}
        return JsonResponse(data)
    else:
        data = {'success': 0}
        return JsonResponse(data)

# === Vendor manage campaign ===
@is_vendor
def vendor_managecampaign(request):
    # Vendor manage. campaign
    counter = campaign_allocation.objects.filter(client_vendor_id=request.session['userid']).count()
    data = []
    if counter > 0:
        campaign_allocation_datails = campaign_allocation.objects.filter(
            client_vendor_id=request.session['userid']).order_by('campaign__name','campaign__priority')
        for row in campaign_allocation_datails:
            campaign_details = Campaign.objects.get(id=row.campaign_id)
            delivery_obj = Delivery.objects.get(campaign_id=row.campaign_id)
            counter_obj = Counter_cpl.objects.filter(campaign_allocation=row.id, campaign=row.campaign_id)
            data1 = getCplNLead(row, campaign_details)
            if data1['camp'] == 1:
                if campaign_details.adhoc:
                    type = 'adhoc'
                else:
                    if campaign_details.rfq == 1:
                        type = 'RFQ'
                    else:
                        type = 'Normal'

                data.append(
                    {
                        'id': campaign_details.id,
                        'rfq_timer': campaign_details.rfq_timer,
                        'name': campaign_details.name,
                        'description': campaign_details.description,
                        'io_number': campaign_details.io_number,
                        'cpl': data1['cpl'],
                        'leads': data1['lead'],
                        'start_date': campaign_details.start_date,
                        'end_date': campaign_details.end_date,
                        'status': row.status,
                        'type': type,
                        'rfq': campaign_details.rfq,
                        'camp_alloc_id': row.id,
                        'counter_status':row.counter_status,
                        'counter_cpl':counter_obj[0].req_cpl if counter_obj.count() == 1 else 0,
                        'counter_vol':counter_obj[0].volume if counter_obj.count() == 1 else 0,
                        'comment':counter_obj[0].comment if counter_obj.count() == 1 else '',
                        'priority': campaign_details.priority,
                        'assign_more_lead_status':row.assign_more_lead_status,
                        'user':campaign_details.user,
                        'reasons':eval(row.reasons)['flag'] if row.reasons != None else 1,
                        'delivery_status': 1 if delivery_obj.tc_header_status == 1 or delivery_obj.custom_header_status == 1 else 0
                    }
                )
        # return render(request,'campaign/manage_campaign.html',{'camps':data})
        import operator
        data.sort(key=operator.itemgetter('priority'))
        return data
    else:
        return render(request, 'campaign/manage_campaign.html', {})

# === Vendor Live Campaign ===
@is_vendor
def vendor_live_campagin(request):
    # Vendor Live Campaign
    data = vendor_managecampaign(request)
    return render(request, 'campaign/vendor_live_campaign.html', {'camps': data})

# === Vendor pause campaign ===
@is_vendor
def vendor_paused_campagin(request):
    # Vendor pause campaign
    data = vendor_managecampaign(request)
    return render(request, 'campaign/vendor_pause_campaign.html', {'camps': data})

# === Vendor completed campaign ===
@login_required
def vendor_completed_campagin(request):
    # Vendor completed campaign
    data = vendor_managecampaign(request)
    return render(request, 'campaign/vendor_complete_campaign.html', {'camps': data})

# === Vendor pending campaign ===
@is_vendor
def pending_campaign(request):
    #  Vendor pending campaign
    data = vendor_managecampaign(request)
    more = More_allocation.objects.filter(client_vendor_id=request.session['userid'])
    return render(request, 'campaign/vendor_pending_campaign.html', {'camps': data, 'more':more})

# === Vendor rfq campaign ===
@is_vendor
def vendor_rfq_campaign(request):
    # Vendor rfq campaign
    data = RFQ_campaign_quotes.objects.filter(vendor_id=request.session['userid'])
    return render(request, 'campaign/vendor_rfq_campaign.html', {'camps': data})

# === Vendor Assiged campaign ===
@is_vendor
def vendor_assigned_campagin(request):
    # Vendor Assiged campaign
    data = vendor_managecampaign(request)
    return render(request, 'campaign/vendor_assigned_campaign.html', {'camps': data})

# === Get CPL and Lead ===
def getCplNLead(row, campaign_details):
    # Get CPL and Lead
    data = {}
    if row.status == 1:
        data = {'cpl': row.cpl, 'lead': row.volume, 'camp': 1}
    elif row.status == 4:
        data = {'cpl': row.client_cpl, 'lead': row.old_volume, 'camp': 1}
    elif row.status == 5:
        data = {'cpl': row.cpl, 'lead': row.volume, 'camp': 1}
    elif row.status == 2:
        data = {'cpl': row.client_cpl, 'lead': row.old_volume, 'camp': 1}
    elif row.status == 3 and row.cpl == -1 and row.volume == -1:
        data = {'cpl': 0, 'lead': 0, 'camp': 1}
    elif row.status == 3 and row.cpl == 0 and row.volume == -1:
        data = {'cpl': -1, 'lead': -1, 'camp': 1}
    elif row.status == 3 and row.cpl > 0 and row.volume > 0:
        data = {'cpl': row.cpl, 'lead': row.volume, 'camp': 1}
    else:
        data = {'cpl': 0, 'lead': 0, 'camp': 0}
    return data

# === Pause campaign allocation ===
def pause_campaign(request):
    # pause campaign allocation
    # alloc.reasons = 1 (paused by client)
    # alloc.reasons = 2 (paused by vendor)
    alloc = campaign_allocation.objects.get(id=request.POST['camp_alloc_id'])
    alloc.status = 2
    alloc.reasons = {'flag':2 if request.session['usertype'] in [2,5] else 1,'reason':request.POST['comment']}
    alloc.save()
    if request.session['usertype'] in [2,5]:
        desc = f"'{alloc.campaign.name}' campaign is paused by vendor."
        RegisterNotification(alloc.client_vendor.id,alloc.campaign.user.id, desc,'Campaign Paused', 1, alloc.campaign, None)
    else:
        desc = f"'{alloc.campaign.name}' campaign is paused by client."
        RegisterNotification(alloc.campaign.user.id,alloc.client_vendor.id, desc,'Campaign Paused', 1, alloc.campaign, None)
    return JsonResponse({'status':1})

# === Resume campaign allocation ===
def resume_campaign(request,campaign_id):
    # resume campaign allocation
    alloc = campaign_allocation.objects.get(id=campaign_id)
    alloc.status = 1
    alloc.reasons = None
    alloc.save()
    if request.session['usertype'] in [2,5]:
        desc = f"'{alloc.campaign.name}' campaign is resumed by vendor."
        RegisterNotification(alloc.client_vendor.id,alloc.campaign.user.id, desc,'Campaign Resumed', 1, alloc.campaign, None)
        if request.session['usertype'] == 2:
            return redirect(reverse('vendor_live_campagin'))
        else:
            return redirect('/vendor-portal/live-campaign/')
    else:
        desc = f"'{alloc.campaign.name}' campaign is resumed by client."
        RegisterNotification(alloc.campaign.user.id,alloc.client_vendor.id, desc,'Campaign Resumed', 1, alloc.campaign, None)
        return redirect('/client/campaigndesc/'+str(alloc.campaign.id))

# === Vendor Profile ===
@login_required
def vendorprofile(request, vendor_id):
    # Vendor Profile
    profile_data = get_profile_data(vendor_id)
    if client_vendor.objects.filter(user_id=vendor_id).count() > 0:
        vendor = client_vendor.objects.get(user_id=vendor_id)
        campaign = Campaign.objects.filter(user_id=request.session['userid'])
        return render(request, 'vendor1/vendor_profile.html', {'vendor': vendor, 'campaign': campaign, 'vendor_id': vendor_id,'profile_data':profile_data})
    return render(request, 'vendor1/vendor_profile.html',{})

# === collect vendor profile data ===
def get_profile_data(vendor_id):
    # collect vendor profile data
    Acceptance_rate = campaign_deliver = 0
    latest_connection =[]
    vendor = client_vendor.objects.get(user_id=vendor_id)
    assesment = data_assesment.objects.get(user_id=vendor_id)
    try:
        view_list = eval(ClientCustomFields.objects.get(client_id=vendor_id).profile_view)
    except Exception as e:
        view_list = eval(SystemDefaultFields.objects.first().profile_view)
        
    language = {}
    if eval(assesment.language_supported) != {}:
        language_data = eval(assesment.language_supported) if type(eval(assesment.language_supported)) == dict else {k:'0' for k in eval(assesment.language_supported)}
        for key, value in language_data.items():
            lead_gen = language_supported.objects.get(id=key).type
            if lead_gen:
                language.update({lead_gen:value})
    else:
        language = "--"

    lead_gen_method = {}
    if eval(assesment.lead_gen_capacity) != {}:
        for key, value in eval(assesment.lead_gen_capacity).items():
            lead_gen = source_touches.objects.get(id=key).type
            if lead_gen:
                lead_gen_method.update({lead_gen:value})
    else:
        lead_gen_method = "--"

    complex_program = {}
    if eval(assesment.complex_program_capacity) != {}:
        for key, value in eval(assesment.complex_program_capacity).items():
            if level_intent.objects.filter(id=key).exists():
                lead_gen = level_intent.objects.get(id=key).type
                if lead_gen:
                    complex_program.update({lead_gen:value})
    else:
        complex_program = "--"

    campaigns_allocated = campaign_allocation.objects.filter(client_vendor_id=vendor_id)
    if campaigns_allocated.count() > 0:
        Acceptance_rate = (int(campaigns_allocated.filter(status__in=[1,4,5]).count())/int(campaigns_allocated.count()))*100
        if int(campaigns_allocated.filter(status__in=[4]).count()) > 0:
            campaign_deliver =(int(campaigns_allocated.filter(status__in=[1,4,5]).count())/int(campaigns_allocated.filter(status__in=[4]).count()))*100

    users = [d.campaign.user for d in campaigns_allocated.filter(status__in=[1,4,5])]
    for user in users:
        if user not in latest_connection:
            latest_connection.append(user)

    logo = vendor.user.user_name[0]
    if vendor.company_logo.storage.exists(vendor.company_logo.name):
        try:
            logo = vendor.company_logo.url
        except:
            pass
    context= {
        'Acceptance_rate':Acceptance_rate,
        'average_month_cov': '2000',
        'vendor_type':[d for d in source_touches.objects.filter(id__in=eval(assesment.vendor_type)).values_list('type',flat=True)],
        'industry_type':[d for d in vendor.industry_type.all().values_list('type',flat=True)],
        'speciality':[d for d in vendor.industry_speciality.all().values_list('name',flat=True)],
        'sweets_spot_geo':assesment.sweet_spot_text.split(',') if assesment.sweet_spot_text else "Not Found",
        'year_incorporate':assesment.year_incorporated if assesment.year_incorporated else "Not Found",
        'language_supported':language,
        'lead_gen_mehtod':lead_gen_method,
        'complex_program':complex_program,
        'HQ_loc':[d.name for d in assesment.hq_location.all()] if assesment.hq_location.all().count() > 0 else ["Not Found"],
        'call_centre_loc':[d.name for d in assesment.call_center_location.all()] if assesment.call_center_location.all().count() > 0 else ["Not Found"],
        'data_processing_loc':[d.name for d in assesment.data_processing_location.all()] if assesment.data_processing_location.all().count() > 0 else ["Not Found"],
        'overview':assesment.company_overview if assesment.company_overview else "Not Found",
        'campaign_deliver':campaign_deliver,
        'latest_connection':latest_connection,
        'logo':logo,
    }
    vendor_view ={}
    for view in [d[0] for d in view_list.items() if d[1] == 1]:
        vendor_view.update({view:context[view]})
    return vendor_view

def getData(request):
    # Get Campaign Data
    cid = campaign_allocation.objects.get(
        client_vendor_id=request.session['userid'], campaign_id=request.POST.get('id'))
    data = {
        'cpl': cid.cpl,
        'volume': cid.volume,
    }
    return JsonResponse(data)

# === Send Agree Request ===
@is_campaigns_vendor
def sendAgreeRequest(request, camp_alloc_id):
    # Send Agree Request
    from datetime import datetime
    sender = request.session['userid']

    camp_alloc = campaign_allocation.objects.get(id=camp_alloc_id)
    camp = Campaign.objects.get(id=camp_alloc.campaign_id)
    campaign_id=camp_alloc.campaign_id
    #if assigned more lead check campaign allready running then ,mearge leads

    # if checkCampaignLive(sender,camp_alloc_id) and checkCampaignAssigned(sender, camp_alloc_id):
    camp_alloc.status = 5
    camp_alloc.save()

    # check if camp has filled all specs to send it to assigned..Akshay G..Date - 19-09-2019
    pending_camp_specs = pending_camp_status(camp)
    camp.approveleads = camp.approveleads+camp_alloc.volume

    if all(pending_camp_specs[camp.id].values()) and camp.status != 1:
        camp.status = 5
    camp.save()


    today = datetime.now().date()

    client = Campaign.objects.get(id=campaign_id)
    superadmin = user.objects.filter(usertype_id=4)

    title = "Campaign Request Accept"
    desc = 'Campaign request has been accepted by '+request.session['username']+' for '+client.name
    from client.utils import noti_via_mail
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    super.append(client.user_id)
    noti = noti_via_mail(super, title, desc, mail_vendor_approval_campaign)
    for j in super:
        RegisterNotification(sender, j, desc, title, 2,client,None)
    if camp.status == 1 and camp_alloc.status == 1:
        return redirect('/vendor/live-campaign/')
    else:
        return redirect('/vendor/assigned-campaign/')

# === Check Assigned Campaign ===
def checkCampaignAssigned(userid,camp_alloc_id):
    ''' Check Assigned Campaign '''
    from datetime import datetime
    camp_alloc = campaign_allocation.objects.get(id=camp_alloc_id)
    campaign_id=camp_alloc.campaign_id
    camp = Campaign.objects.get(id=camp_alloc.campaign_id)
    if campaign_allocation.objects.filter(campaign_id=campaign_id,client_vendor_id=userid,status=5).count() == 1:
        campaign_details=campaign_allocation.objects.get(campaign_id=campaign_id,client_vendor_id=userid,status=5)
        campaign_details.volume=int(campaign_details.volume)+int(camp_alloc.volume)
        campaign_details.save()
        campaign_allocation.objects.get(id=camp_alloc_id).delete()

        today = datetime.now().date()
        if today == camp.start_date or today > camp.start_date:
            camp_alloc.status = 1
            camp_alloc.save()
            camp.status = 1
        camp.save()
        return False
    return True

# === Check Campaign Live ===
def checkCampaignLive(userid,camp_alloc_id):
    # Check Campaign Live
    from datetime import datetime
    camp_alloc = campaign_allocation.objects.get(id=camp_alloc_id)
    campaign_id=camp_alloc.campaign_id
    camp = Campaign.objects.get(id=camp_alloc.campaign_id)
    if campaign_allocation.objects.filter(campaign_id=campaign_id,client_vendor_id=userid,status=1).count() == 1:
        campaign_details=campaign_allocation.objects.get(campaign_id=campaign_id,client_vendor_id=userid,status=1)
        campaign_details.volume=int(campaign_details.volume)+int(camp_alloc.volume)
        campaign_details.save()
        campaign_allocation.objects.get(id=camp_alloc_id).delete()

        today = datetime.now().date()
        if today == camp.start_date or today > camp.start_date:
            camp_alloc.status = 1
            camp_alloc.save()
            camp.status = 1
        camp.save()
        return False
    return True

# === Registration Notification ===
def RegisterNotification(sender_id, receiver_id, desc, title, notification_type_id, campaign, camp_alloc):
    # Registration Notification
    try:
        Notification.objects.create(title=title, description=desc, notification_type_id=notification_type_id,
                                    sender_id=sender_id, campaign_id=campaign, camp_alloc=camp_alloc)
    except:
        if camp_alloc:
            Notification.objects.create(title=title, description=desc,
                                        notification_type_id=notification_type_id, sender_id=sender_id, camp_alloc=camp_alloc)
        if campaign:
            Notification.objects.create(title=title, description=desc,
                                        notification_type_id=notification_type_id, sender_id=sender_id, campaign_id=campaign)
        else:
            Notification.objects.create(title=title, description=desc,
                                        notification_type_id=notification_type_id, sender_id=sender_id)
    Nlast_id = Notification.objects.latest('id')
    Notification_Reciever.objects.create(
        status='0', Notification_id=Nlast_id.id, receiver_id=receiver_id)

# === Vendor raise Ticket ===
@is_vendor
def raiseticket(request):
    # Vendor raise Ticket
    tc_cat = Ticket_Category.objects.all()
    tickets = Raised_Tickets.objects.all()
    return render(request, 'help/vendor_raiseticket.html', {'tc_cat':tc_cat,'tickets':tickets})

# === faq vendor ===
@is_vendor
def faq(request):
    # faq vendor
    return render(request, 'help/vendor_faq.html', {})

# === Vendor Terms ===
@is_vendor
def terms(request):
    # Vendor Terms
    return render(request, 'help/vendor_terms.html', {})


@is_vendor
def manualupload(request):
    # Manual Upload
    return render(request, 'leads/manual_upload.html', {})

# === Bulkup Load ===
@is_vendor
def bulkupload(request):
    # Bulkup Load
    return render(request, 'leads/bulk_upload.html', {})

# === Vendor lead list ===
@is_vendor
def leadlist(request):
    # Vendor lead list
    return render(request, 'leads/leadlist.html', {})

# === Vendor onboarding ===
@csrf_exempt
def onBording(request):
    # Vendor onboarding
    userid = request.session['userid']
    countrow = client_vendor.objects.filter(user_id=userid).count()
    if countrow == 1:
        new_user = client_vendor.objects.get(user_id=userid)
        new_user.primary_name = request.POST.get('primary_name')
        new_user.company_name = request.POST.get('user_name')
        new_user.company_email = new_user.user.email
        new_user.primary_designation = request.POST.get('primary_designation')
        new_user.primary_email = request.POST.get('primary_email') if isValidEmail(request.POST.get('primary_email')) else ''
        new_user.primary_directdial = request.POST.get('primary_directdial')
        new_user.secondary_name = request.POST.get('secondary_name')
        new_user.secondary_designation = request.POST.get('secondary_designation')
        new_user.secondary_email = request.POST.get('secondary_email') if isValidEmail(request.POST.get('secondary_email')) else ''
        new_user.secondary_directdial = request.POST.get('secondary_directdial')
        new_user.website = request.POST.get('website')

        industry_type_list = request.POST.getlist('industry_type_id')
        if industry_type_list:
            new_user.industry_type.clear()
            new_user.industry_type.add(*industry_type_list)

        industry_speciality_list = request.POST.getlist('industry_speciality_id')
        if industry_speciality_list:
            new_user.industry_speciality.clear()
            new_user.industry_speciality.add(*industry_speciality_list)

        new_user.company_size_id = request.POST.get('company_size')
        if request.FILES:
            fs = FileSystemStorage()
            if 'company_logo' in request.FILES:
                myfile = request.FILES['company_logo']
                filename = fs.save('company_logo/' + myfile.name, myfile)
                uploaded_file_url = fs.url(filename)
                new_user.company_logo = filename
            if 'recommendations' in request.FILES:
                medikit = request.FILES['recommendations']
                medi = fs.save(medikit.name, medikit)
                new_user.media_kit = medi
        if new_user.company_logo == '':
            # set default logo for user, if user didn't upload image
            # filter default logo for user according to user's username's 1st character..Akshay G..Date - 21st Oct, 2019
            user_name = request.POST.get('user_name')
            import re
            fs = FileSystemStorage()
            s = re.search(r'[a-zA-Z]+', user_name)
            if s is not None:
                alpha_char = s.group()
                query = User_default_logo.objects.filter(title = alpha_char[0].lower())
                if query:
                    filename = fs.save('company_logo/' + query[0].logo.name.replace('default_logo/',''), query[0].logo)
                    uploaded_file_url = fs.url(filename)
                    new_user.company_logo = filename
                else:
                    new_user.company_logo = 'default.jpg'
            else:
                new_user.company_logo = 'default.jpg'
            # create thumbnail for default uploaded logo...Akshay G.
            new_user.company_logo_thumbnail = ImageSpecField(source=new_user.company_logo,id="client_vendor:client_vendor:company_logo_thumbnail")
            new_user.company_logo_thumbnail.generate()
            new_user.company_logo_smartthumbnail = ImageSpecField(source=new_user.company_logo,id="client_vendor:client_vendor:company_logo_smartthumbnail")
            new_user.company_logo_smartthumbnail.generate()
        new_user.save()

        t = user.objects.get(id=userid)
        t.user_name = request.POST.get('user_name')
        t.contact = request.POST.get('contact')
        t.alternative_contact = request.POST.get('alternate_number')
        t.address_line1 = request.POST.get('address_line1')
        t.address_line2 = request.POST.get('address_line2')
        t.country = request.POST.get('country')
        t.state = request.POST.get('state')
        t.city = request.POST.get('cities')
        t.zip_code = request.POST['zip_code']
        t.anonymous_status = 1 if request.POST.get('anonymous_status') == 'on' else 0
        t.save()

        #check Aggrement checked or not
        if request.POST.get('nda_aggrement'):
            nda=1
        else:
            nda=0

        if request.POST.get('msa_aggrement'):
            msa=1
        else:
            msa=0

        if request.POST.get('gdpr_aggrement'):
            gdpr=1
        else:
            gdpr=0

        if request.POST.get('dpa_aggrement'):
            dpa=1
        else:
            dpa=0

        if request.POST.get('io_aggrement'):
            io=1
        else:
            io=0
        data_assess_object,created = data_assesment.objects.get_or_create(user_id=userid)
        if data_assess_object:
            t = data_assess_object
            t.company_overview = request.POST.get('comp_overview')
            if request.POST.get('hq_location') != '0':
                t.hq_location.clear()
                t.hq_location.add(*request.POST.getlist('hq_location'))
            if request.POST.get('call_center_location') != '0':
                t.call_center_location.clear()
                t.call_center_location.add(*request.POST.getlist('call_center_location'))
            if request.POST.get('data_processing_location') != '0':
                t.data_processing_location.clear()
                t.data_processing_location.add(*request.POST.getlist('data_processing_location'))
            if request.POST.get('year_incorporated') != '0':
                t.year_incorporated = request.POST.get('year_incorporated')
            t.unique_value_prop = request.POST.get('unique_value_prop')
            t.sweet_spot = request.POST.getlist('region_country')
            t.sweet_spot_text = request.POST.get('sweet_spot')
            overall_size = request.POST.get('overall_size')
            if overall_size is not None:
                if overall_size.strip() != '':
                    if ast.literal_eval(overall_size) > 0:
                        # save overall size value multiplied with user selected multiplier..Akshay G..Date- 6th Nov, 2019
                        t.database_overall_size = ast.literal_eval(overall_size)*ast.literal_eval(request.POST.get('overall_size_multiplier'))
                    else:
                        t.database_overall_size = 0
            size_us = request.POST.get('size_us')
            if size_us is not None:
                if size_us.strip() != '':
                    if ast.literal_eval(size_us) > 0:
                        # save US size value multiplied with user selected multiplier..Akshay G..Date- 6th Nov, 2019
                        t.database_size_us = ast.literal_eval(size_us)*ast.literal_eval(request.POST.get('us_size_multiplier'))
                    else:
                        t.database_size_us = 0
            optin = request.POST.get('optin')
            if optin is not None:
                if optin.strip() != '':
                    if ast.literal_eval(optin) > 0:
                        # save Opt-In value multiplied with user selected multiplier..Akshay G..Date- 6th Nov, 2019
                        t.database_opt_in = ast.literal_eval(optin)*ast.literal_eval(request.POST.get('opt_in_size_multiplier'))
                    else:
                        t.database_opt_in = 0
            t.database_attribute = request.POST.getlist(u'database_attribute')
            t.delivery_time = request.POST.getlist(u'delivery_time')
            t.delivery_method = request.POST.getlist(u'delivery_method')

            # lead_gen_cap = ['0' if i == '' else i for i in request.POST.getlist("lead_gen_method_values")]
            # lead_gen = request.POST.getlist("lead_gen_method")
            # t.lead_gen_capacity = dict(zip(lead_gen, lead_gen_cap))

            lead_gen = request.POST.getlist("lead_gen_method")
            d = {}
            for id in lead_gen:
                # complex program values for various fields....Akshay G...Date - 24th Oct, 2019
                lead_gen_value = request.POST.get('lead_gen_method_values{}_'+id)
                if lead_gen_value != '':
                    d[id] = lead_gen_value
                else:
                    d[id] = '0'
            t.lead_gen_capacity = d

            # complex_program = request.POST.getlist("complex_program")
            # complex_program_values = ['0' if i == '' else i for i in request.POST.getlist("complex_program_values")]
            # t.complex_program_capacity = dict(zip(complex_program, complex_program_values))

            complex_program = request.POST.getlist("complex_program")
            d = {}
            for id in complex_program:
                # complex program values for various fields....Akshay G...Date - 24th Oct, 2019
                complex_prog_value = request.POST.get('complex_program_values{}_'+id)
                if complex_prog_value != '':
                    d[id] = complex_prog_value
                else:
                    d[id] = '0'
            t.complex_program_capacity = d

            type_data = request.POST.getlist(u'vendor_type')
            type_data.remove('') if ('') in type_data else type_data

            t.vendor_type = type_data
            t.registration_process = request.POST.getlist(
                u'registrtions_process')
            # get new languages added by user..Akshay G..Date - 25th Oct, 2019
            new_languages = request.POST.getlist('new_languages')
            new_language_ids = get_language_support_list(new_languages)
            # get user selected language ids...Akshay G..Date - 25th Oct, 2019
            selected_language_ids = request.POST.getlist('language_supported')
            new_language_data = dict(zip(new_language_ids, new_languages)) if len(new_languages) > 0 else {}
            language_data = {}
            if len(selected_language_ids) > 0:
                for id in selected_language_ids:
                    # fetch team size for particular user language..Date - 25th Oct, 2019
                    language_value = request.POST.get('language_supported_values_{}'+id)
                    if language_value != '':
                        language_data[id] = language_value
                    else:
                        language_data[id] = '0'
            for id, value in new_language_data.items():
                # fetch team size for particular user language..Date - 25th Oct, 2019
                language_value = request.POST.get('new_languages_values_{}'+value)
                if language_value != '':
                    language_data[id] = language_value
                else:
                    language_data[id] = '0'
            t.language_supported = language_data
            t.networks_and_publishers = request.POST.getlist(
                u'networks_and_publishers')
            t.networks_and_publishers_other =request.POST.get('networks_and_publishers_other')
            t.nda_aggrement=nda
            t.msa_aggrement=msa
            t.gdpr_aggrement=gdpr
            t.dpa_aggrement=dpa
            t.io_aggrement=io
            t.user_id = userid
            t.save()
        welcome = user.objects.get(id=userid)

        if request.session['usertype'] == 1:
            pages = 'client'
            # suryakant 
            # set lead approve flow for client
            # client,created = ClientCustomFields.objects.get_or_create(client_id=request.session['userid'])
            # if client and request.POST['lead_approve_flow_step_1'] and request.POST['lead_approve_flow_step_2'] and request.POST['lead_approve_flow_step_3']:
            #     client.lead_process  = [request.POST['lead_approve_flow_step_1'],request.POST['lead_approve_flow_step_2'],request.POST['lead_approve_flow_step_3']]
            #     client.save()
        else:
            pages = 'vendor'

        if welcome.status == 2:

            site_url = settings.BASE_URL + 'WelcomeOnBoard'
            data = {'success': 1, 'site_url': site_url, 'pages': pages, 'msg':"Your account is now under review, We will get back to you soon, Thank You!!"}
        else:
            site_url = settings.BASE_URL + pages + '/dashboard/'
            data = {'success': 1, 'site_url': site_url, 'pages': pages, 'msg':"Great, Your data has been updated successfully !!"}
        # superadmin = user.objects.filter(usertype_id=4)
        # title = "OnBoarding Filled"
        # desc = "Action required " +request.POST.get('user_name')
        # super = [i.id for i in user.objects.filter(usertype_id=4)]
        # for j in super:
            # RegisterNotification(userid, j, desc, title, 2,None,None)
        # data = {'success': 1, 'site_url': site_url, 'pages': pages, 'msg':"Great, Your data has been updated successfully !!"}
        return JsonResponse(data)
    else:

        t = user.objects.get(id=userid)
        t.user_name = request.POST.get('user_name')
        t.contact = request.POST.get('contact')
        t.alternative_contact = request.POST.get('alternate_number')
        t.address_line1 = request.POST.get('address_line1')
        t.address_line2 = request.POST.get('address_line2')
        t.country = request.POST.get('country')
        t.state = request.POST.get('state')
        t.status = '2'
        t.save()
        request.session['onBording']=2
        new_user = client_vendor.objects.create(
            # primary_name=request.POST.get('primary_name'),
            # primary_designation=request.POST.get('primary_designation'),
            # primary_email=request.POST.get('primary_email'),
            # primary_directdial=request.POST.get('primary_directdial'),
            # primary_contact=request.POST.get('primary_directdial'),
            # secondary_name=request.POST.get('secondary_name'),
            # secondary_designation=request.POST.get('secondary_designation'),
            # secondary_email=request.POST.get('secondary_email'),
            # secondary_directdial=request.POST.get('secondary_directdial'),
            # secondary_contact=request.POST.get('secondary_directdial'),
            # alt_number1=request.POST.get('primary_directdial'),
            # alt_number2=request.POST.get('secondary_directdial'),
            website=request.POST.get('website'),

            # lead_per_month=request.POST.get('lead_per_month'),
            # unique_reach_per_month=request.POST.get('unique_reach_per_month'),
            # marketing_method_id=request.POST.get('marketing_method'),
            user_id=userid,
        )

        industry_type_list=request.POST.getlist('industry_type_id')
        if industry_type_list:
            new_user.industry_type.clear()
            new_user.industry_type.add(*industry_type_list)

        industry_speciality_list=request.POST.getlist('industry_speciality_id')
        if industry_speciality_list:
            new_user.industry_speciality.clear()
            new_user.industry_speciality.add(*industry_speciality_list)

        new_user.save()
        # site_url = settings.BASE_URL
        if request.session['usertype'] == 1:
            pages = 'client'
        else:
            pages = 'vendor'

        site_url = settings.BASE_URL + 'WelcomeOnBoard'
        data = {'success': 1, 'site_url': site_url, 'pages': pages,'msg':"Your account is under review, We will let you know by mail. Thank You!!"}
        return JsonResponse(data)

# === Get language support list ===
def get_language_support_list(list):
    #store runtime language in database
    # Get language support list
    ids=[]
    for language in list:
        if language_supported.objects.filter(type=language).count() == 0:
            language_supported.objects.create(type=language)
            last_id=language_supported.objects.latest('id')
            ids.append(str(last_id.id))
        else:
            lang=language_supported.objects.get(type=language)
            ids.append(str(lang.id))
    return ids

# === Client Boarding ===
@login_required
def client_Bording(request):
    # Client Boarding
    userid = request.session['userid']
    countries1 = countries_list.objects.all()
    # countries1 = countries.objects.all()
    industry = industry_type.objects.filter(status=0)
    expert = industry_speciality.objects.all()
    city=cities1.objects.all()
    countrow = client_vendor.objects.filter(user_id=userid).count()
    if countrow == 1:
        client_vendor_detail = client_vendor.objects.get(
            user_id=userid)
        userdetails = user.objects.get(id=userid)
        country = countries_list.objects.get(id=userdetails.country)
        # country = countries.objects.get(id=userdetails.country)
        if userdetails.city:
            city=cities1.objects.get(id=userdetails.city)
        else:
            city=cities1.objects.filter(state_id=userdetails.state)
        state = states1.objects.get(id=userdetails.state)
        # state = states.objects.get(id=userdetails.state)
        industry1 = client_vendor_detail.industry_type.all()
        # speciality = industry_speciality.objects.get(
        #     id=client_vendor_detail.industry_speciality_id)

        documents = Document.objects.all()
        doc1 = documents[0].nda_mnda_document.url
        doc2 = documents[0].msa_document.url
        doc3 = documents[0].gdpr_document.url
        doc4 = documents[0].dpa_document.url
        doc5 = documents[0].io_document.url
        
        # removed lead flow # suryakant 16-10-2019
        # leadflow = eval(ClientCustomFields.objects.get(client_id=userid).lead_process) if ClientCustomFields.objects.filter(client_id=userid).exists() else []
        queryset = userdetails.data_assesment_set.all()
        doc =  {
                    'doc1': doc1,
                    'doc2': doc2,
                    'doc3': doc3,
                    'doc4': doc4,
                    'doc5': doc5
                }
        for obj in queryset:
            if obj.nda_aggrement == 1:
                pdf1=str(obj.nda_aggrement_path)
                pdf1=pdf1[:-1]
                doc['doc1'] = '/media/'+pdf1

            if obj.msa_aggrement == 1:
                pdf2=str(obj.msa_aggrement_path)
                pdf2=pdf2[:-1]
                doc['doc2'] = '/media/'+pdf2

            if obj.gdpr_aggrement == 1:
                pdf3=str(obj.gdpr_aggrement_path)
                pdf3=pdf3[:-1]
                doc['doc3'] = '/media/'+pdf3

            if obj.dpa_aggrement == 1:
                pdf4=str(obj.dpa_aggrement_path)
                pdf4=pdf4[:-1]
                doc['doc4'] = '/media/'+pdf4

            if obj.io_aggrement == 1:
                pdf5=str(obj.io_aggrement_path)
                pdf5=pdf5[:-1]
                doc['doc5'] = '/media/'+pdf5
        counter = data_assesment.objects.filter(user_id=userid).count()
        if counter == 1:
            data_assesment1 = data_assesment.objects.get(
                user_id=userid)

        else:
            data_assesment1 = {}
            # 'speciality': speciality,
        return render(request, 'dashboard/client_boarding.html', {'data_assesment':data_assesment1,'onBording':request.session['onBording'], 'industry': industry, 'industry1': industry1, 'state': state,'cities':city, 'country': country, 'userdetails': userdetails, 'client_vendor': client_vendor_detail, 'expert': expert, 'countries': countries1, 'industry': industry, 'docpdf': doc})
    else:
        documents = Document.objects.all()
        doc1 = documents[0].nda_mnda_document.url
        doc2 = documents[0].msa_document.url
        doc3 = documents[0].gdpr_document.url
        doc4 = documents[0].dpa_document.url
        doc5 = documents[0].io_document.url
        doc =  {
                    'doc1': doc1,
                    'doc2': doc2,
                    'doc3': doc3,
                    'doc4': doc4,
                    'doc5': doc5
                }
        return render(request, 'dashboard/client_boarding.html', { 'onBording':0, 'expert': expert,'countries': countries1, 'industry': industry, 'docpdf': doc})

# === Create Report PDF ===
@login_required
def create_report_pdf(request, userid, vendor_name):
    # Akshay G...Date- 3-6-2019
    # vendor onboarding html into pdf
    countries1 = countries_list.objects.all()
    industry = industry_type.objects.all()
    expert = industry_speciality.objects.all()
    marketing_method = source_touches.objects.filter(is_active=1).all()
    onboarding=user.objects.get(id=userid)
    region_list=region1.objects.all()
    complex_program = level_intent.objects.filter(is_active=1).all()
    vendor_type=VendorType.objects.all()
    currant_campaign_currant_month = no_of_currant_campaign(userid)
    last_month_all_campaign = no_of_last_month_campaign(userid)
    currant_campaign = currant_month_campaign(userid)
    t = data_assesment.objects.get(user_id=userid)
    
    sweet_spot = t.sweet_spot_text
    sweet_spot_geo = sweet_spot.split(',')
    sweet_spot = ', '.join(sweet_spot_geo)
    registration_process_ = []
    if eval(t.registration_process):
        l = eval(t.registration_process)
        for i in l:
            registration_process_.extend([registration_process.objects.get(id = int(i)).type,', '])
        registration_process_.pop()
    client_vendor_detail = client_vendor.objects.get(user_id=userid)
    userdetails = user.objects.get(id=userid)
    networks_and_publishers_ = []
    if eval(t.networks_and_publishers):
        l = eval(t.networks_and_publishers)
        for i in l:
            networks_and_publishers_.extend([networks_and_publishers.objects.get(id = int(i)).type,', '])
        networks_and_publishers_.pop()
    networks_and_publishers_other_ = t.networks_and_publishers_other
    country = countries_list.objects.get(id=userdetails.country).name
    database_attribute_ = []
    if eval(t.database_attribute):
        l = eval(t.database_attribute)
        for i in l:
            database_attribute_.extend([database_attribute.objects.get(id =int(i)).type,', '])
        database_attribute_.pop()
    call_center_location_ = ''
    if t.call_center_location.all().count() > 0:
        call_center_location_ = [d.name for d in t.call_center_location.all()]
    
    hq_location_ = ''
    if t.hq_location.all().count() > 0:
        hq_location_ = [d.name for d in t.hq_location.all()]
    
    data_processing_location_ = ''
    if t.data_processing_location.all().count() > 0:
        data_processing_location_  = [d.name for d in t.data_processing_location.all()]
    
    state = states1.objects.get(id=userdetails.state)
    city_ = cities1.objects.filter(id=userdetails.city)
    if city_:
        city = city_[0].name
    else:
        city = None
    industry1 = client_vendor_detail.industry_type.all()
    speciality = client_vendor_detail.industry_speciality.all()
    documents = Document.objects.all()
    doc1 = documents[0].nda_mnda_document.url
    doc2 = documents[0].msa_document.url
    doc3 = documents[0].gdpr_document.url
    doc4 = documents[0].dpa_document.url
    doc5 = documents[0].io_document.url
    temp_dict = {
        'us_size_value': None,
        'us_size_multiplier': None,
        'overall_size_value': None,
        'overall_size_multiplier': None,
        'opt_in_size_value': None,
        'opt_in_size_multiplier': None,
    }
    queryset = userdetails.data_assesment_set.all()
    doc =  {
                'doc1': doc1,
                'doc2': doc2,
                'doc3': doc3,
                'doc4': doc4,
                'doc5': doc5
            }
    for obj in queryset:
        if obj.nda_aggrement == 1:
            pdf1=str(obj.nda_aggrement_path)
            pdf1=pdf1[:-1]
            doc['doc1'] = '/media/'+pdf1

        if obj.msa_aggrement == 1:
            pdf2=str(obj.msa_aggrement_path)
            pdf2=pdf2[:-1]
            doc['doc2'] = '/media/'+pdf2

        if obj.gdpr_aggrement == 1:
            pdf3=str(obj.gdpr_aggrement_path)
            pdf3=pdf3[:-1]
            doc['doc3'] = '/media/'+pdf3

        if obj.dpa_aggrement == 1:
            pdf4=str(obj.dpa_aggrement_path)
            pdf4=pdf4[:-1]
            doc['doc4'] = '/media/'+pdf4

        if obj.io_aggrement == 1:
            pdf5=str(obj.io_aggrement_path)
            pdf5=pdf5[:-1]
            doc['doc5'] = '/media/'+pdf5

    counter = data_assesment.objects.filter(user_id=userid).count()
    if counter == 1:
        data_assesment1 = data_assesment.objects.get(
            user_id=userid)
        from superadmin.utils import vendor_database_size_multiplier
        # fetch database size value with mutiplier...Akshay G..Date 6th Nov, 2019
        temp_dict = vendor_database_size_multiplier(data_assesment1)
    else:
        data_assesment1 = {}

    languages = []
    if t.language_supported is not None:
        if eval(t.language_supported):
            language_data = eval(t.language_supported) if type(eval(t.language_supported)) == dict else {k:'0' for k in eval(t.language_supported)}
            for key, value in language_data.items():
                lead_gen = language_supported.objects.get(id=key).type
                if lead_gen:
                    languages.extend([f'{lead_gen} - {value}'])
    lead_gen_method_ = []
    if eval(data_assesment1.lead_gen_capacity):
        data_assess = eval(data_assesment1.lead_gen_capacity)
        for row in marketing_method.all():
            if str(row.id) in data_assess:
                if row.id:
                    lead_gen_method_.extend([row.type + ' - '+ data_assess[str(row.id)],', '])
        if lead_gen_method_:
            lead_gen_method_.pop()
    complex_prog = []
    if data_assesment1.complex_program_capacity != None:
        if eval(data_assesment1.complex_program_capacity):
            d = eval(data_assesment1.complex_program_capacity)
            for row in complex_program.all():
                if str(row.id) in d:
                    if row.id:
                        complex_prog.extend([row.type + ' - '+ d[str(row.id)],', '])
            if complex_prog:
                complex_prog.pop()
    # append Agreement pdf's path to 'pdfs'
    pdfs = []
    for pdf_ in doc:
        datafile = doc[pdf_]
        l = datafile.split('/')
        for i in l:
            if i == '':
                del l[l.index(i)]
        del l[0]
        datafile = '/'.join(l)
        urldata = os.path.join(settings.MEDIA_ROOT, datafile)
        pdfs.append(urldata)
    vendor_types = []
    for row in vendor_type.all():
        if str(row.id) in data_assesment1.vendor_type and row.is_active == 1:
            vendor_types.extend([row.type,', '])
    vendor_types.pop()
    logo_file = settings.STATICFILES_DIRS[0]
    techconnectr_logo = os.path.join(logo_file,'assets/images/logo/tc.jpg')
    # get company logo image path
    urldata = os.path.join(settings.MEDIA_ROOT, client_vendor_detail.company_logo.name)
    # data to be rendered into html
    data = {**temp_dict,'hq_location':hq_location_,'techconnectr_logo':techconnectr_logo,'complex_program':complex_prog,'lead_gen_method':lead_gen_method_,'vendor_types':vendor_types,'company_logo':urldata,'data_processing_location':data_processing_location_,'call_center_location':call_center_location_,'database_attribute':database_attribute_,'networks_and_publishers_other':networks_and_publishers_other_,'networks_and_publishers':networks_and_publishers_,'registration_process': registration_process_,'sweet_spot_geo': sweet_spot,'data_assesment':data_assesment1, 'region_list':region_list,'vendor_type':vendor_type, 'language_supported': languages, 'data_assesment': data_assesment1, 'marketing_method': marketing_method, 'speciality': speciality, 'industry1':industry1, 'city':city, 'industry': industry, 'state': state, 'country': country, 'userdetails': userdetails, 'client_vendor': client_vendor_detail, 'expert': expert, 'countries': countries1, 'industry': industry}
    template = get_template('vendor/onboarding_pdf.html')
    html  = template.render(data)
    # create new pdf and write html data into it
    file = open(os.path.expanduser('test_new.pdf'), "w+b")
    pisaStatus = pisa.CreatePDF(html.encode('utf-8'), dest=file,
            encoding='utf-8')
    file.seek(0)
    pdf = file.read()
    file.close()
    pdfs.insert(0,'test_new.pdf')
    pdfs.insert(1,client_vendor_detail.media_kit)
    # try-except block to check, if media kit is there or not
    # "try" executes, if media kit is uploaded
    try:
        merger = PdfFileMerger()
        # merger all pdfs into single pdf
        for pdf in pdfs:
            PDF = pdf
            merger.append(pdf)
        merger.write("result.pdf")
        new_file = open('result.pdf', 'r+b')
        new_file.seek(0)
        pdf = new_file.read()
        return HttpResponse(pdf, 'application/pdf')
    # "except" executes, if media kit is not uploaded
    except:
        # if media file does not exists, remove it from merging
        pdfs.remove(PDF)
        merger = PdfFileMerger()
        # merge all pdfs into single pdf
        for pdf in pdfs:
            merger.append(pdf)
        merger.write("result.pdf")
        new_file = open('result.pdf', 'r+b')
        new_file.seek(0)
        pdf = new_file.read()
        return HttpResponse(pdf, 'application/pdf')

# === Vendor Boarding ===
@is_vendor
def vendorBording(request):
    # Vendor Boarding

    userid = request.session['userid']
    countries1 = countries_list.objects.all()
    # countries1 = countries.objects.all()
    industry = industry_type.objects.filter(status=0)
    expert = industry_speciality.objects.all()
    notifydic = notification(userid)
    marketing_method = source_touches.objects.filter(is_active=1).all()
    company_size_data = company_size.objects.all()
    capacity_type = source_touches.objects.all()
    onboarding=user.objects.get(id=userid)
    countrow = client_vendor.objects.filter(user_id=userid).count()
    delivery_time1 = delivery_time.objects.all()
    delivery_method1 = delivery_method.objects.all()
    # region_list=region.objects.all()
    region_list=region1.objects.all()
    complex_program = level_intent.objects.filter(is_active=1).all()
    registrtions_process1 = registration_process.objects.filter(
        is_active=1).all()
    database_attribute1 = database_attribute.objects.filter(
        is_active=1).all()
    language_supported1 = language_supported.objects.filter(
        is_active=1).all()
    networks_and_publishers1 = networks_and_publishers.objects.filter(
        is_active=1).all()
    vendor_type=VendorType.objects.all()
    currant_campaign_currant_month = no_of_currant_campaign(userid)
    last_month_all_campaign = no_of_last_month_campaign(userid)
    currant_campaign = currant_month_campaign(userid)
    temp_dict = {
        'us_size_value': None,
        'us_size_multiplier': None,
        'overall_size_value': None,
        'overall_size_multiplier': None,
        'opt_in_size_value': None,
        'opt_in_size_multiplier': None,
    }
    if countrow == 1:
        client_vendor_detail = client_vendor.objects.get(
            user_id=userid)
        userdetails = user.objects.get(id=userid)
        country = countries_list.objects.get(id=userdetails.country)
        # country = countries.objects.get(id=userdetails.country)
        state = states1.objects.get(id=userdetails.state)
        if userdetails.city:
            city=cities1.objects.get(id=userdetails.city)
        else:
            city=cities1.objects.filter(state_id=userdetails.state)
        # state = states.objects.get(id=userdetails.state)
        industry1 = client_vendor_detail.industry_type.all()
        speciality = client_vendor_detail.industry_speciality.all()

        documents = Document.objects.all()
        doc1 = documents[0].nda_mnda_document.url
        doc2 = documents[0].msa_document.url
        doc3 = documents[0].gdpr_document.url
        doc4 = documents[0].dpa_document.url
        doc5 = documents[0].io_document.url

        queryset = userdetails.data_assesment_set.all()
        doc =  {
                    'doc1': doc1,
                    'doc2': doc2,
                    'doc3': doc3,
                    'doc4': doc4,
                    'doc5': doc5
                }
        for obj in queryset:
            if obj.nda_aggrement == 1:
                pdf1=str(obj.nda_aggrement_path)
                pdf1=pdf1[:-1]
                doc['doc1'] = '/media/'+pdf1

            if obj.msa_aggrement == 1:
                pdf2=str(obj.msa_aggrement_path)
                pdf2=pdf2[:-1]
                doc['doc2'] = '/media/'+pdf2

            if obj.gdpr_aggrement == 1:
                pdf3=str(obj.gdpr_aggrement_path)
                pdf3=pdf3[:-1]
                doc['doc3'] = '/media/'+pdf3

            if obj.dpa_aggrement == 1:
                pdf4=str(obj.dpa_aggrement_path)
                pdf4=pdf4[:-1]
                doc['doc4'] = '/media/'+pdf4

            if obj.io_aggrement == 1:
                pdf5=str(obj.io_aggrement_path)
                pdf5=pdf5[:-1]
                doc['doc5'] = '/media/'+pdf5

        counter = data_assesment.objects.filter(user_id=userid).count()
        if counter == 1:
            data_assesment1 = data_assesment.objects.get(
                user_id=userid)
            from superadmin.utils import vendor_database_size_multiplier
            # fetch database size value with mutiplier...Akshay G..Date 6th Nov, 2019
            temp_dict = vendor_database_size_multiplier(data_assesment1)
        else:
            data_assesment1 = {}
        context = {'data_assesment':data_assesment1, 
                'docpdf': doc, 
                'region_list':region_list,
                'vendor_type':vendor_type,
                'networks_and_publishers': networks_and_publishers1, 
                'language_supported': language_supported1, 
                'onBording':onboarding.status, 
                'data_assesment': data_assesment1, 
                'database_attribute': database_attribute1, 
                'registrtions_process': registrtions_process1, 
                'complex_program': complex_program, 
                'delivery_time': delivery_time1, 
                'delivery_method': delivery_method1, 
                'capacity_type': capacity_type, 
                'company_size': company_size_data, 
                'marketing_method': marketing_method, 
                'speciality': speciality, 
                'industry1':industry1, 
                'industry': industry, 
                'state': state, 
                'cities':city, 
                'country': country, 
                'userdetails': userdetails,
                'client_vendor': client_vendor_detail,
                'expert': expert,
                'countries': countries1,
                'industry': industry,
                'notifycount': notifydic["notifycount"],
                'notify': notifydic["notify"],
                'current_camp': currant_campaign_currant_month,
                'last_month': last_month_all_campaign,
                **temp_dict,
                'camp_alloc': currant_campaign}
        return render(request, 'vendor/vendor_boarding.html',context)
    else:
        documents = Document.objects.all()
        doc1 = documents[0].nda_mnda_document.url
        doc2 = documents[0].msa_document.url
        doc3 = documents[0].gdpr_document.url
        doc4 = documents[0].dpa_document.url
        doc5 = documents[0].io_document.url
        doc =  {
                    'doc1': doc1,                    
                    'doc2': doc2,
                    'doc3': doc3,
                    'doc4': doc4,
                    'doc5': doc5
                }
        return render(request, 'vendor/vendor_boarding.html', {**temp_dict,'networks_and_publishers': networks_and_publishers1,'region_list':region_list,'vendor_type':vendor_type,'onBording': 0, 'database_attribute': database_attribute1, 'registrtions_process': registrtions_process1, 'complex_program': complex_program, 'delivery_time': delivery_time1, 'delivery_method': delivery_method1, 'capacity_type': capacity_type, 'company_size': company_size_data, 'marketing_method': marketing_method, 'expert': expert, 'countries': countries1, 'industry': industry, 'notifycount': notifydic["notifycount"], 'notify': notifydic["notify"],'language_supported': language_supported1, 'docpdf': doc })

# === Show On Boarding Data ===
def show_onbording_data(request):
    # Show On Boarding Data
    userid = request.POST.get('id', None)
    data = {'success': 2}
    countrow = client_vendor.objects.filter(user_id=userid).count()
    if countrow == 1:
        users = user.objects.filter(id=userid)
        clients = [item for item in client_vendor.objects.filter(
            user_id__in=users)][0]
        country = countries_list.objects.get(id=clients.user.country)
        # country = countries.objects.get(id=clients.user.country)
        state = states1.objects.get(id=clients.user.state)
        # state = states.objects.get(id=clients.user.state)
        data = {
            'comp_name': clients.user.user_name,
            'website': clients.website,
            'add1': clients.user.address_line1,
            'add2': clients.user.address_line2,
            'country_name': country.name,
            'state_name': state.name,
            'phone': clients.user.contact,
            'primary_name': clients.primary_name,
            'primary_designation': clients.primary_designation,
            'primary_email': clients.primary_email,
            'primary_contact': clients.primary_contact,
            'secondary_name': clients.secondary_name,
            'secondary_email': clients.secondary_email,
            'secondary_contact': clients.secondary_contact,
            'secondary_designation': clients.secondary_designation,
            'address': clients.user.address_line1 + clients.user.address_line2,
            'industry_name': clients.industry_type.type,
            'speciality_name': clients.industry_speciality.name,
            'lead_per_month': clients.lead_per_month,
            'unique_reach_per_month': clients.unique_reach_per_month,
            'marketing_method': clients.marketing_method.type,

        }

        return JsonResponse(data)
    else:
        return JsonResponse(data)


# === Vendor list ===
@is_vendor
def vendor_list(request):
    # Vendor list
    users = user.objects.filter(usertype_id=2)
    clients = match_campaign_vendor.objects.filter(client_vendor_id__in=users)
    return render(request, 'vendor/vendorlist.html', {'clients': clients})


# === Vendor profile ===
@is_vendor
def vendor_profile(request, vid):
    # Vendor profile
    vendr = client_vendor.objects.get(id=vid)
    vendor_name = vendr.user.username
    return render(request, 'vendor/vendor_profile.html', {'data': vendor_name})

# === Suggest Vendor ===
def vendor_suggest(request):
    # Suggest Vendor
    campaign_id = request.POST.get('campaign_id', None)
    vendor_id = request.POST.get('vendor_id', None)
    data = {'success': 0}
    counter = campaign_allocation.objects.filter(
        campaign_id=campaign_id, client_vendor_id=vendor_id, status=0).count()
    if counter == 1:
        t = campaign_allocation.objects.get(
            campaign_id=campaign_id, client_vendor_id=vendor_id)
        t.status = 0
        t.save()
        data = {'success': 1}
    else:
        campaign_allocation.objects.create(
            status=0, client_vendor_id=vendor_id, campaign_id=campaign_id)
        data = {'success': 1}

    return JsonResponse(data)

# === Vendor Campaign Completed by client ===
'''# abhi 3-09-2019 '''
'''# Following function used for vendor campaign goes in completed stage. '''
@login_required
def completed_lead(request, camp_alloc_id):
    # Vendor Campaign Completed by client
    t = campaign_allocation.objects.get(id=camp_alloc_id)
    camp_id = t.campaign_id
    if t:
        t.status = 4
        t.save()
        # send Notification to vendor for completed campaign
        title = 'Campaign Completed'
        desc = f"{t.campaign.user.user_name} Completed  {t.campaign.name} campaign"
        # check all campaign allocation are completed or not
        # and match campaign target_qut with all vendor approve leads
        # if match then automatically campaign goes to completed stage either campaign stay in live stage
        no_of_camp_alloc = campaign_allocation.objects.filter(campaign_id=t.campaign_id,status__in=[5,1,4])
        no_of_camp_completed = campaign_allocation.objects.filter(campaign_id=t.campaign_id,status__in=[4])

        if no_of_camp_alloc.count() == no_of_camp_completed.count() :
            completed_camp_approve_lead = 0
            for camp in no_of_camp_completed:
                completed_camp_approve_lead += camp.approve_leads
            camp = Campaign.objects.get(id=t.campaign_id)
            if camp.campaign.target_quantity == completed_camp_approve_lead :
                camp.status = 4
                camp.save()

        RegisterNotification(request.session['userid'], t.client_vendor_id, desc, title, 1, None, t)
        complete_by_vendor(t.campaign_id, t.client_vendor.user_name, t.client_vendor.id)
    return redirect('/client/campaigndesc/'+str(camp_id))

# === Completed Campaign ===
@login_required
def completed_campaign(request, camp_id):
    # Completed Campaign
    # all allocation also will completed

    if campaign_allocation.objects.filter(campaign_id=camp_id,status__in=[1,2]).update(status=4):
        t = Campaign.objects.get(id=camp_id)
        t.status = 4
        t.save()
        campaign_allocation.objects.filter(campaign_id=camp_id,status__in=[3,5]).delete()

    return redirect('/client/completed-campaign/')

# === Remove campaign from pending ===
@is_vendor
def remove_campaign_from_pending(request, camp_id, leads):
    # Remove campaign from pending
    vendor_id = request.session['userid']
    campaign_details = campaign_allocation.objects.get(id=camp_id)
    data = Campaign.objects.get(id=campaign_details.campaign_id)
    data.raimainingleads = data.raimainingleads+leads
    data.save()
    campaign_allocation.objects.get(id=camp_id).delete()
    vendor = user.objects.get(id=vendor_id)
    title = "Campaign Request Reject"
    desc = f"Campaign request has been rejected by {vendor.user_name} for {data.name}"
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    super.append(data.user.id)
    for j in super:
        RegisterNotification(vendor_id, j, desc, title, 2,data,None)
    # suryakant 17-10-2019 # raised email notification on reject allocations
    from client.utils import noti_via_mail
    noti_via_mail(super, title, desc, mail_vendor_approval_campaign)
    return redirect('/vendor/pending-campaign/')

# === Generate lead capacity add ===
def lead_gen_capacity_add(request):
    # Generate lead capacity add
    vendor_id = request.session['userid']
    type_id = request.POST.get('type_id')
    leads = request.POST.get('lead')
    data = dict()
    data[type_id] = leads
    counter = data_assesment.objects.filter(user_id=vendor_id).count()
    if counter == 1:
        old = data_assesment.objects.get(user_id=vendor_id)
        if old.lead_gen_capacity == None :
            t = data_assesment.objects.get(user_id=vendor_id)
            t.lead_gen_capacity = str(data)
            t.save()
        else:
            old_data = ast.literal_eval(old.lead_gen_capacity)
            old_data.update(data)
            t = data_assesment.objects.get(user_id=vendor_id)
            t.lead_gen_capacity = str(old_data)
            t.save()
    else:
        data_assesment.objects.create(
            user_id=vendor_id, lead_gen_capacity=data)

    data = {'success': 1}
    return JsonResponse(data)

# === Complete program capacity add ===
def complex_program_capacity_add(request):
    # Complete program capacity add

    vendor_id = request.session['userid']
    type_id = request.POST.get('type_id')
    leads = request.POST.get('lead')
    data = dict()
    data[type_id] = leads
    counter = data_assesment.objects.filter(user_id=vendor_id).count()
    if counter == 1:
        old = data_assesment.objects.get(user_id=vendor_id)
        if old.complex_program_capacity == None :
            t = data_assesment.objects.get(user_id=vendor_id)
            t.complex_program_capacity = str(data)
            t.save()
        else:
            old_data = ast.literal_eval(old.complex_program_capacity)
            old_data.update(data)
            t = data_assesment.objects.get(user_id=vendor_id)
            t.complex_program_capacity = str(old_data)
            t.save()
    else:
        data_assesment.objects.create(
            user_id=vendor_id, complex_program_capacity=data)

    data = {'success': 1}
    return JsonResponse(data)

# === Download lead ===
def download_lead(request):
    # Download lead
    camp_id = request.POST.get('campaign_id')
    file = Campaign.objects.get(id=camp_id)
    
    file_name =''.join(e for e in file.name if e.isalnum()) #remove all special char
    file_name = file_name + '.csv'

    if Delivery.objects.filter(campaign_id=camp_id).count() > 0:
        camp_lead = Delivery.objects.get(campaign_id=camp_id)
        if camp_lead.custom_header_status == 0 :
            labels = camp_lead.data_header
            labels = labels.split(',')
        else:
            labels = camp_lead.custom_header
            labels = labels.split(',')
        labels +=join_custom_question_header(camp_id)

        with open(file_name, 'w', newline='') as myfile:
            wr = csv.writer(myfile, quoting=csv.QUOTE_ALL)
            wr.writerow(labels)
        with open(file_name, 'rb') as myfile:
            response = HttpResponse(myfile, content_type='text/csv')
            response['Content-Disposition'] = 'attachment; filename='+file_name
            return response

# === Create Template with Custom Question ===
def join_custom_question_header(camp_id):
    # create Template with Custom Question
    labels=[]
    if Question_forms.objects.filter(campaign_id=camp_id).count() > 0:
        cust_header=Question_forms.objects.get(campaign_id=camp_id)
        for i in range(cust_header.question_numbers):
            labels.append('Q'+str(i+1))
    return labels

# === Get lead header ===
def get_lead_header(camp_id):
    # Get lead header
    camp_lead=Delivery.objects.get(campaign_id=camp_id)
    if camp_lead.custom_header_status == 0 :
        labels = camp_lead.data_header        
        labels = list(labels.split(','))
    else:
        labels = camp_lead.custom_header
        labels = labels.split(',')
    return labels

# === Lead List ===
@is_vendor
def lead_list(request, camp_id, status):
    # Lead List
    leadlist = []
    all_header = []
    all_lead_header = []
    labels=[]
    batchlist = []
    leadlist =[]
    count = []
    camp_alloc = campaign_allocation.objects.filter(id=camp_id)
    Custom_question_status = Mapping.objects.filter(campaign_id=camp_alloc[0].campaign_id)[0].custom_status
    if camp_alloc.count() == 1 and status == 1:
        camp_alloc = campaign_allocation.objects.get(id=camp_id)
        header = create_custom_header(
            camp_alloc.campaign_id, request.session['userid'])
        is_upload=check_lead_uploadable(int(camp_alloc.volume),int(camp_alloc.submited_lead),int(camp_alloc.return_lead))
        data = {'camp_id': camp_alloc.campaign_id,'client_name':camp_alloc.campaign.user.user_name, 'camp_alloc_id': camp_id, 'camp_name': camp_alloc.campaign.name, 'cpl': camp_alloc.cpl,
                'lead': camp_alloc.volume,'is_upload':is_upload, 'submited_lead': camp_alloc.submited_lead, 'return_lead': camp_alloc.return_lead}
        if camp_alloc.submited_lead > 0:
            leadlist = ast.literal_eval(camp_alloc.upload_leads)
            batchlist = batch_list(leadlist)
            if len(header) == 0:
                count=0
                for dict in leadlist:
                    if len(dict.keys()) > count :
                        count = len(dict.keys())
                        all_header,all_lead_header=[],[]
                        for key in dict:
                            all_header.append(key)
                            all_lead_header.append(key)
                all_header = create_header(all_header)
        labels=get_lead_header(camp_alloc.campaign_id)
        labels +=join_custom_question_header(camp_alloc.campaign_id)
        if batchlist != []:
            count = batchlist[1]
            batchlist = batchlist[0]
        if len(all_header) == 0:
            all_header=labels
        return render(request, 'campaign/leadlist.html', {'Custom_question_status':Custom_question_status,'edit_lead_header':labels,'campaigns': data, 'leadlist': leadlist, 'all_lead_header': all_lead_header,
                 'all_header': all_header, 'header': header, 'status': camp_alloc.status, 'camp_id': camp_id,'batch':batchlist,'count':count})
    elif camp_alloc.count() == 1 and status == 4:
        camp_alloc = campaign_allocation.objects.get(id=camp_id)
        header = create_custom_header(
            camp_alloc.campaign_id, request.session['userid'])
        data = {'camp_id': camp_alloc.campaign_id, 'camp_alloc_id': camp_id, 'camp_name': camp_alloc.campaign.name, 'cpl': camp_alloc.old_cpl,
                'lead': camp_alloc.old_volume, 'submited_lead': camp_alloc.submited_lead, 'return_lead': camp_alloc.return_lead}
        if camp_alloc.upload_leads != None:
            list = ast.literal_eval(camp_alloc.upload_leads)
            if len(header) == 0:
                count=0
                for dict in list:
                    if len(dict.keys()) > count :
                        count = len(dict.keys())
                        all_header,all_lead_header=[],[]
                        for key in dict:
                            all_header.append(key)
                            all_lead_header.append(key)
                all_header = create_header(all_header)

        labels=get_lead_header(camp_alloc.campaign_id)
        labels +=join_custom_question_header(camp_alloc.campaign_id)
        if len(all_header) == 0:
            all_header=labels
        return render(request, 'campaign/leadlist.html', {'Custom_question_status':Custom_question_status,'edit_lead_header':labels,'campaigns': data, 'leadlist': list, 'all_lead_header': all_header, 'all_header': all_header, 'header': header, 'status': status, 'camp_id': camp_id})
    return render(request, 'campaign/leadlist.html', {'Custom_question_status':Custom_question_status,'camp_id': camp_id,'batch':batchlist,'count':count,'status': status})

# === Lead History ===
@is_vendor
def lead_history(request,camp_id,camp_alloc_id):
    userid=request.session['userid']
    lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid)
    return render(request, 'campaign/lead_history.html', {'lead_history':lead_data,'camp_alloc_id':camp_alloc_id})

# === Check lead uploadable ===
def check_lead_uploadable(volume,submited_lead,return_lead):
    #check bulk lead  uploadable or not if yes how many lead can vendor upload
    #following function give exact number of lead
    # Check lead uploadable
    if submited_lead >= volume :
        more_leads_uploads=submited_lead - volume
        if return_lead >= more_leads_uploads:
            more_leads_uploads = return_lead - more_leads_uploads
    else:
        more_leads_uploads=(volume-submited_lead)+(return_lead)
    return more_leads_uploads

# === Create Header ===
def create_header(dict):
    # Create Header
    list = []
    for row in dict:
        if row == 'reason':
            list.append('Status Code')
        else:
            list.append(row)
    return list

# === Create Custom Header ===
def create_custom_header(camp_id, userid):
    # Create Custom Header
    fav_lead = []
    fav_lead_details = favorite_leads_header.objects.filter(
        campaign_id=camp_id, user_id=userid)
    if fav_lead_details.count() == 1:
        fav_lead_details = favorite_leads_header.objects.get(
            campaign_id=camp_id, user_id=userid)
        return ast.literal_eval(fav_lead_details.header)
    else:
        return fav_lead

# === RFQ CPL ===
@csrf_exempt
def rfqcpl(request):
    # RFQ CPL
    camp_quote_id = request.POST.get('camp_alloc_id')
    t = RFQ_campaign_quotes.objects.get(id=camp_quote_id)
    if t.cpl != 0 :
        data = {'success': 2, 'msg': 'You have already submitted cpl and volume'}
    else:
        t.cpl = request.POST.get('cpl')
        t.volume = request.POST.get('volume')
        t.comments = request.POST.get('comment')
        t.save()
        data = {'success': 1}
        title = "RFQ Suggestions On "+str(t.campaign.name)+' By '+str(t.vendor.user_name)
        desc =  str(t.vendor.user_name)+" Suggested: CPL is "+str(request.POST.get('cpl'))+" & Volume is "+str(request.POST.get('volume'))+' for '+str(t.campaign.name)
        from client.utils import noti_via_mail
        noti_via_mail([i.id for i in user.objects.filter(usertype_id=4)], title, desc, mail_vendor_rfq_quotaion)
        super = [i.id for i in user.objects.filter(usertype_id=4)]
        #suryakant 11_oct_2019
        # raised rfq quote send notification to client
        super.append(t.campaign.user.id)
        for j in super:
            RegisterNotification(t.vendor.id, j, desc, title, 2,None,None)
    return JsonResponse(data)

# === Counter Request for CPL ===
def Counter_CPL_form(request):
    # Counter Request for CPL
    campaign = campaign_allocation.objects.filter(id=request.POST.get('camp_all_id'))
    counter_obj = Counter_cpl.objects.filter(campaign_allocation=request.POST.get('camp_all_id'), campaign=campaign[0].campaign.id,user_id=request.session['userid'])
    if counter_obj.count() == 0:
        obj = Counter_cpl.objects.create()
        obj.volume = request.POST.get('volume')
        obj.req_cpl = request.POST.get('req_cpl') if request.POST.get('req_cpl') else None
        obj.user_id = user.objects.get(id=request.session['userid'])
        obj.campaign = campaign[0].campaign
        obj.campaign_allocation = campaign[0]
        obj.comment = request.POST.get('desc')
        obj.save()
        title = "Counter Request"
        desc = f'Counter request raised by {campaign[0].client_vendor.user_name} on {str(campaign[0].campaign.name)} campaign.'
        camp = campaign_allocation.objects.get(id=request.POST.get('camp_all_id'))
        camp.counter_status = 1
        camp.save()
        # suryakant 16-10-2019
        # raised mail and refactored code
        super = [i.id for i in user.objects.filter(usertype_id=4)]
        super.append(campaign[0].campaign.user.id)
        for j in super:
            RegisterNotification(campaign[0].client_vendor.id, j, desc, title, 2,campaign[0].campaign,camp)
        noti_via_mail(super, title, desc, mail_counter_action_by_vendor)
        data = {'success': 1, }

    else:
        data = {'success': 3}
    return JsonResponse(data)

# === Update campaign status ===
def update_campaign_status(request):
    # celery task to be added
    # Update campaign status
    # today = datetime.strftime(datetime.now().date(),'%Y-%m-%d')
    import datetime
    today = datetime.datetime.strftime(datetime.datetime.now().date(),'%Y-%m-%d')
    today_camp = Campaign.objects.filter(start_date = today,status=5)
    capm_list = []
    for camp in today_camp:
        camp.status = 1
        camp_allocations = campaign_allocation.objects.filter(campaign=camp,status=5)
        for alloc in camp_allocations:
            alloc.status = 1
            alloc.save()
        camp.save()
        capm_list.append(camp.name)

# === Vendor Campaign Notebook ===
@is_vendor
def vendor_campaign_notebook(request):
    # Vendor Campaign Notebook
    campaign_details = campaign_allocation.objects.filter(client_vendor=request.session['userid'])
    vendor = user.objects.filter(id=request.session['userid'])
    if campaign_details:
        pending_camp_count = campaign_allocation.objects.filter(client_vendor=request.session['userid'],status=3).count()
        live_camp_count = campaign_allocation.objects.filter(client_vendor=request.session['userid'],status=1).count()
        assigned_camp_count = campaign_allocation.objects.filter(client_vendor=request.session['userid'],status=5).count()
        complete_camp_count = campaign_allocation.objects.filter(client_vendor=request.session['userid'],status=4).count()
        return render(request,'campaign/vendor_campaign_notebook.html',{'camps':campaign_details,'user':vendor[0].user_name if vendor else None,
                            'pending_count':pending_camp_count,
                    'live_count':live_camp_count,'assigned_count':assigned_camp_count,'complete_count':complete_camp_count})
    else:
        return render(request,'campaign/vendor_campaign_notebook.html',{'user':vendor[0].user_name if vendor else None})

# ===  Get Cpl List ===
def get_cpl_list(request):
    camp_id = request.GET.get('camp_id')
    vendor_list = []
    get_details = collect_campaign_details(camp_id)
    rfq_status = list(Campaign_rfq.objects.filter(campaign_id=camp_id).values())
    vendor = campaign_allocation.objects.get(campaign_id=camp_id, client_vendor_id=request.session['userid'],status=3, cpl__in=[0,-1], volume=-1)
    vendor_list.append({
        'id':vendor.campaign_id,
        'cpl':vendor.rfqcpl if vendor.rfqcpl else -1,
        'volume':vendor.rfqvolume if vendor.rfqvolume else -1,
        'name':vendor.campaign.name,
    })
    data = {'success': 1,'cpl_list':vendor_list,'details':get_details,'rfq_status':rfq_status }
    return JsonResponse(data)

# === Complete by Vendor===
def complete_by_vendor(camp_id, vendor_name, vendor_id):
    '''# view to storing record of campaign complete by vendor in campaign track - by Amey Raje'''
    from datetime import datetime
    if CampaignTrack.objects.filter(campaign_id=camp_id).exists():
        camp_alloc = campaign_allocation.objects.get(campaign_id=camp_id, client_vendor_id=vendor_id)
        track = CampaignTrack.objects.get(campaign_id=camp_id)
        if track.complete_status_count > 0:
            list = eval(track.complete_status)
            d = {}
            d['type'] = 'complete'
            d['vendor_id'] = vendor_id
            d['vendor_name'] = vendor_name
            t = datetime.now()
            d['date'] = t.isoformat()
            d['client_percentage'] = percentage(camp_id)
            d['vendor_percentage'] = vendorper(camp_alloc.id)
            list.append(d)
            track.complete_status = list
            track.complete_status_count += 1
            track.save()
        else:
            list = []
            d = {}
            d['type'] = 'complete'
            d['vendor_id'] = vendor_id
            d['vendor_name'] = vendor_name
            t = datetime.now()
            d['date'] = t.isoformat()
            d['client_percentage'] = percentage(camp_id)
            d['vendor_percentage'] = vendorper(camp_alloc.id)
            list.append(d)
            track.complete_status = list
            track.complete_status_count += 1
            track.save()


# === vendor side life cycle ===
@is_vendor_of_campaign
def vendorside_life_cycle(request, camp_id):
    '''vendor side life cycle - by Amey Raje'''

    import datetime
    if CampaignTrack.objects.filter(campaign_id=camp_id).exists():
        intro = CampaignTrack.objects.get(campaign_id=camp_id)
        # getting vendor objects in live state
        camp_alloc = campaign_allocation.objects.filter(campaign_id=camp_id, client_vendor_id=request.session['userid'], status__in=[1,2,4])
        if camp_alloc:
            list_for_uploaded = []
            # history of uploaded leads
            uploaded = Lead_Uploaded_Error.objects.filter(campaign_id=camp_id, user_id=request.session['userid'])
            for upload in uploaded:
                if upload.user.id == request.session['userid']:
                    # total_lead = int(upload.duplicate_with_our_cnt) + int(upload.duplicate_with_vendor_cnt) + int(upload.remove_lead_header_cnt) + int(upload.remove_duplicate_lead_csv_cnt)
                    total_lead =int(upload.all_lead_count)
                    if total_lead == 0:
                        total_lead = upload.uploaded_lead_count
                    

                    d = {}
                    d['type'] = 'upload'
                    d['vendor_id'] = upload.user.id
                    d['vendor'] = upload.user.user_name
                    t = upload.exact_time
                    d['date'] = t.isoformat()
                    d['lead_row_id'] = upload.id
                    d['all_uploaded_leads'] = total_lead
                    d['valid_leads'] = upload.uploaded_lead_count
                    d['vendor_percentage'] = upload.vendor_percentage
                    d['client_percentage'] = upload.client_percentage
                    list_for_uploaded.append(d)

            info = list()

            dic = {'a': intro.data_vendor_assign_count, 'b': intro.client_action_count, 'c': intro.complete_status_count}
    # merging leads
            for i in dic:
                if dic[i] > 0:
                    if i == 'a':
                        info += list(chain(eval(intro.data_vendor_assign)))
                    elif i == 'b':
                        info += list(chain(eval(intro.client_action)))
                    elif i == 'c':
                        info += list(chain(eval(intro.complete_status)))

            for i in info:
                if i['vendor_id'] == request.session['userid']:
                    list_for_uploaded.append(i)
    # sorting accoding to date
            newlist = sorted(list_for_uploaded, key=itemgetter('date'))

            target = int(camp_alloc[0].volume)
            submitted = int(camp_alloc[0].submited_lead)
            camp = Campaign.objects.get(id=camp_id)
            approve = int(camp_alloc[0].approve_leads)
            import datetime
            start = datetime.datetime.strptime(str(camp.start_date), '%Y-%m-%d')
            end = datetime.datetime.strptime(str(camp.end_date), '%Y-%m-%d')
            today = datetime.datetime.now()

    # finding campaign status is ahead or behind
            lapsed = (today-start).days
            if lapsed < 0:
                lapsed = 0
            total_days = (end-start).days
            per_day = int(target)/int(total_days)
            if lapsed > total_days:
                till_date_to_be_approved = per_day*total_days
                day_per = 100
                days = int(total_days)
            else:
                till_date_to_be_approved = per_day*lapsed
                day_per = (lapsed/total_days)*100
                days = int(lapsed)
            per = vendorper(camp_alloc[0].id)
    # passing color acc to state
            if till_date_to_be_approved > int(approve):
                message = 'Warning: Campaign is lagging behind the Schedule.'
                color = 'bg-danger'
            elif till_date_to_be_approved == int(approve):
                message = 'Good! Campaign is on time.'
                color = 'bg-info'

            else:
                message = 'Great! Campaign is ahead of Schedule.'
                color = 'bg-success'
            d1 = []
            for i in newlist:
                d1.append({str(i['vendor_id']):str(i['date'])[0:10]})
            context = {
                'campaign_dates': json.dumps(d1),
                'camp_alloc': camp_alloc[0],
                'intro': intro,
                'info': newlist,
                'color': color,
                'message': message,
                'per': per,
                'day_per': int(day_per),
                'days': days,
                'total': int(total_days),
                'target': target,
                'submitted_leads': submitted,
                'approve': approve,
            }
            return render(request, 'life_cycle/vendor_life_cycle.html', context)
    context = {}
    return render(request, 'errors/raise404.html', context)

# === show all notification types for vendors ===
def vendor_noti(request):
    '''show all notification types for vendors - by Amey Raje'''

    mail = MailNotification.objects.all().order_by('-id')
    access = []
    user_instance = user.objects.get(id=request.session['userid'])
    type = usertype.objects.get(id=request.session['usertype'])
    for m in mail:
        if type in m.usertype.all():
            if user_instance in m.user.all():
                access.append({
                    'id': m.id,
                    'access': m.noti_field,
                    'value': 1,
                    'parent': m.parent,
                })
            else:
                access.append({
                    'id': m.id,
                    'access': m.noti_field,
                    'value': 0,
                    'parent': m.parent,
                })
    return render(request, 'campaign/notiset_vendor.html', {'mail': access})

# === Sign ===
def sign(request):
    import datetime
    img = request.POST.get("img")
    imgdata = img.split(',')
    with open('dumy.txt','w+') as f:
        f.write(imgdata[1])
     # I assume you have a way of picking unique filenames
    image = 'sign1.jpg'
    with open(image, 'wb') as f:
        f.write(base64.decodebytes(open('dumy.txt', 'rb').read()))

    packet = io.BytesIO()
    # create a new PDF with Reportlab
    can = canvas.Canvas(packet, pagesize=letter)
    can.drawImage(image, 500, 80, width=100, height=100)
    can.save()

    datapdf = request.POST.get("pdfdata")
    datafile = request.POST.get("pdfFile")
    datafile3 = request.POST.get("pdfFile3")
    datafile4 = request.POST.get("pdfFile4")
    datafile5 = request.POST.get("pdfFile5")

    # import pdb;pdb.set_trace()
    user_ = user.objects.get(id=request.session['userid'])
    obj_ = data_assesment.objects.get_or_create(user_id = request.session['userid'])
    if type(obj_) == tuple:
        id_ = obj_[0].id
    else:
        id_ = obj_.id

    dataurl = {
        'pdf1': datapdf,
        'pdf2': datafile,
        'pdf3': datafile3,
        'pdf4': datafile4,
        'pdf5': datafile5,
        }
    data_ = {}
    for pdf_ in dataurl:
        datafile = dataurl[pdf_]

        url = datafile.split('/')
        url.remove('')
        url.remove('media')
        data =('/').join(url)


        # urldata = os.path.join(os.path.dirname(os.path.dirname(__file__)),'data')
        urldata = os.path.join(settings.MEDIA_ROOT, data)


        newpdf, pdf = path.split(urldata)

        now = str(datetime.datetime.now())[:19]

        newdata = urldata + now + ".pdf"

        L = newdata.split('/')
        new_pdf_path = '/'

        for i in range(len(L)):
            if i > L.index('media'):
                new_pdf_path += L[i] + '/'

        newpath = shutil.copy(urldata, newdata)
        # url = dataurl.lstrip('/media/')
        # pdfFile = os.path.join(settings.MEDIA_ROOT,os.path.abspath(urldata)) #request.POST.get("pdfdata")
        file = newpath

        # import pdb;pdb.set_trace()

        # file = '
        # file = document
        # move to the beginning of the StringIO buffer
        packet.seek(0)
        new_pdf = PdfFileReader(packet)
        # read your existing PDF
        existing_pdf = PdfFileReader(open(file, 'rb'))
        c = existing_pdf.getNumPages()

        output = PdfFileWriter()
        # addatermark" (which is the new pdf) on the existing page

        for i in range(c):
            page = existing_pdf.getPage(i)
            if i == c-1:
                page.mergePage(new_pdf.getPage(0))
            output.addPage(page)

        # finally, write "output" to a real file
        outputStream = open(file, "ab")
        output.write(outputStream)
        outputStream.close()


        if pdf_ == 'pdf1':
            user_.data_assesment_set.filter(id = id_).update(nda_aggrement_path = new_pdf_path)
            data_["pdf1"] = new_pdf_path

        elif pdf_ == 'pdf2':
            user_.data_assesment_set.filter(id = id_).update(msa_aggrement_path = new_pdf_path)
            data_["pdf2"] = new_pdf_path

        elif pdf_ == 'pdf3':
            user_.data_assesment_set.filter(id = id_).update(gdpr_aggrement_path = new_pdf_path)
            data_["pdf3"] = new_pdf_path

        elif pdf_ == 'pdf4':
            user_.data_assesment_set.filter(id = id_).update(dpa_aggrement_path = new_pdf_path)
            data_["pdf4"] = new_pdf_path

        elif pdf_ == 'pdf5':
            user_.data_assesment_set.filter(id = id_).update(io_aggrement_path = new_pdf_path)
            data_["pdf5"] = new_pdf_path

    return JsonResponse(data_, safe=False)
    # return render(request, 'dashboard/client_boarding.html',{})

    return render(request, 'dashboard/client_boarding.html',{})

# === Add Vendor to Campaign Track ===
def add_vendor_to_campaign_track(instance, more):
    '''for saving data of assigned more leads to a allocated vendor in table camapign track for life cycle - by Amey Raje'''

    from datetime import datetime
    if CampaignTrack.objects.filter(campaign_id=instance.campaign_id).exists():
        track = CampaignTrack.objects.get(campaign_id=instance.campaign_id)
        camp_all = campaign_allocation.objects.get(id=instance.id)
        if track.data_vendor_assign_count > 0:
            list = eval(track.data_vendor_assign)
            d = {}
            d['type'] = 'vendor'
            d['vendor_id'] = instance.client_vendor.id
            d['Name'] = instance.client_vendor.user_name
            t = datetime.now()
            d['date'] = t.isoformat()
            d['camp_alloc_id'] = instance.id
            d['assigned_leads'] = more
            d['cpl'] = instance.cpl
            d['vendor_percentage'] = vendorper(camp_all.id)
            d['client_percentage'] = percentage(instance.campaign_id)
            # d['percentage'] = percentage(instance.campaign_id)
            list.append(d)
            track.data_vendor_assign = list
            track.data_vendor_assign_count += 1
            track.save()
        else:
            # executes if this is first vendor to be assigned
            L = []
            d = {}
            d['type'] = 'vendor'
            d['vendor_id'] = instance.client_vendor.id
            d['Name'] = instance.client_vendor.user_name
            t = datetime.now()
            d['date'] = t.isoformat()
            d['camp_alloc_id'] = instance.id
            d['assigned_leads'] = more
            d['cpl'] = instance.cpl
            d['vendor_percentage'] = vendorper(camp_all.id)
            d['client_percentage'] = percentage(instance.campaign_id)
            L.append(d)
            track.data_vendor_assign = L
            track.data_vendor_assign_count += 1
            track.save()

# === for assign more lead request ===
def agree_more_leads(request, more_id):
    '''for assign more lead request - by Amey Raje'''

    more_alloc_object = More_allocation.objects.get(id=more_id)
    campaign_obj = Campaign.objects.get(id=more_alloc_object.campaign.id)
    campaign_allocation_obj = campaign_allocation.objects.get(id=more_alloc_object.campaign_allocation.id)

    camp_all_id = campaign_allocation_obj.id
    more_leads = int(more_alloc_object.volume)

    campaign_allocation_obj.volume += int(more_alloc_object.volume)
    status = campaign_allocation_obj.status
    campaign_obj.approveleads += int(more_alloc_object.volume)
    # if int(campaign_obj.tc_target_quantity) == int(campaign_obj.tc_approveleads):
    # changes according to new tc target and client target quantity
    if int(campaign_obj.target_quantity) == int(campaign_obj.approveleads):
        campaign_obj.status = 5
    campaign_obj.save()
    campaign_allocation_obj.save()

    # for storing data to campaign track table for life cycle
    add_vendor_to_campaign_track(campaign_allocation.objects.get(id=camp_all_id), more_leads)

    # update leads on commision table
    comm_obj = Campaign_commission.objects.get(camp_alloc_id = campaign_allocation_obj.id)
    comm_obj.allocated_lead += int(more_alloc_object.volume)
    comm_obj.save()
    more_alloc_object.delete()

    title = "Additional Lead Allocation"
    desc = f"Additional lead allocation request on '{campaign_obj.name}' campaign accepted by vendor {campaign_allocation_obj.client_vendor.user_name}."
    # noti_via_mail(sender_id, superad.id, desc, 1)
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    super.append(campaign_obj.user.id)
    for j in super:
        RegisterNotification(request.session['userid'], j, desc, title, 2,None,None)

    if status == 5:
        url = 'vendor_assigned_campagin' if request.session['usertype'] == 2 else '/vendor-portal/assigned-campaign/'
        return redirect(url)
    elif status == 1:
        url = 'vendor_live_campagin' if request.session['usertype'] == 2 else '/vendor-portal/live-campaign/'
        return redirect(url)
    else:
        url = 'pending_campaign' if request.session['usertype'] == 2 else '/vendor-portal/pending-campaign/'
        return redirect(url)

# === reject assigned more leads ===
def reject_more_leads(request, more_id):
    '''reject assigned more leads - by Amey Raje'''

    more_alloc_object = More_allocation.objects.get(id=more_id)
    campaign_obj = Campaign.objects.get(id=more_alloc_object.campaign.id)
    campaign_allocation_obj = campaign_allocation.objects.get(id=more_alloc_object.campaign_allocation.id)
    campaign_obj.raimainingleads += int(more_alloc_object.volume)
    more_alloc_object.delete()
    campaign_obj.save()
    title = "Additional Lead Allocation"
    desc = f"Additional lead allocation request on '{campaign_obj.name}' campaign rejected by vendor {campaign_allocation_obj.client_vendor.user_name}."
    # noti_via_mail(sender_id, superad.id, desc, 1)
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    super.append(campaign_obj.user.id)
    for j in super:
        RegisterNotification(request.session['userid'], j, desc, title, 2,None,None)
    url = 'pending_campaign' if request.session['usertype'] == 2 else '/vendor-portal/pending-campaign/'
    return redirect(url)

# === Save Agreements ===
def save_agreements(request):
    # save agreements uploaded by vendor for the given campaign..Akshay G...Date - 31-08-2019
    vendor_id = request.session['userid']
    agreement_file = request.FILES['file']
    agreement_name = request.POST.get('agreement_name')
    camp_alloc_id = ast.literal_eval(request.POST.get('camp_alloc_id'))
    client_id = ast.literal_eval(request.POST.get('client_id'))
    client_dir = os.path.join(settings.MEDIA_ROOT, 'agreements', 'tc-client-id-'+str(client_id))
    vendor_dir = os.path.join(client_dir, 'tc-vendor-id-'+str(vendor_id))
    if 'tc-vendor-id-'+str(vendor_id) not in os.listdir(client_dir):
        os.mkdir(vendor_dir)
    fs = FileSystemStorage()
    filename = fs.save(vendor_dir+'/'+agreement_file.name, agreement_file)
    uploaded_file_name = os.path.basename(fs.url(filename))
    file_path = os.path.join('media', 'agreements', 'tc-client-id-'+str(client_id), 'tc-vendor-id-'+str(vendor_id), uploaded_file_name)
    camp_alloc_obj = campaign_allocation.objects.get(id = camp_alloc_id)
    data = eval(camp_alloc_obj.vendor_agreements)
    for d in data:
        if agreement_name in d:
            prev_path = d[agreement_name]
            d[agreement_name] = file_path
    camp_alloc_obj.vendor_agreements = data
    camp_alloc_obj.save()
    if len(prev_path) > 0:
        prev_path_ = os.path.join(settings.BASE_DIR, prev_path)
        if fs.exists(prev_path_):
            fs.delete(prev_path_)
    obj = Vendor_agreement_status.objects.get(client_id = client_id, vendor_id = vendor_id)
    if obj.status == False:
        vendor_agr_data = eval(obj.agreements)[0]
        if agreement_name in vendor_agr_data:
            vendor_agr_data[agreement_name] = file_path
        if all(i != '' for i in vendor_agr_data.values()):
            obj.status = True
        obj.agreements = [vendor_agr_data]
        obj.save()
    # suryakant 12-10-2019
    # raised notification to client on agreement uploaded by vendor
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    super.append(client_id)
    for j in super:
        RegisterNotification(request.session['userid'], j, f"{agreement_name} agreement uploaded by {camp_alloc_obj.client_vendor.user_name}", 'Agreement Uploded', 2,None,None)
    return JsonResponse({})

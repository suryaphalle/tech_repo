from django.shortcuts import get_object_or_404,render, redirect ,render_to_response
from django.http import HttpResponseBadRequest,HttpResponse,JsonResponse,HttpResponseRedirect
from django.template import RequestContext
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.template.loader import render_to_string
import csv
from io import StringIO
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.core.mail import EmailMessage, send_mail
import ast,operator
import datetime
import itertools
import pandas
import json
import re
from xlrd import open_workbook
from setupdata.models import *
from client.models import *
from vendors.models import *
from client_vendor.models import *
from user.models import *
from campaign.models import *
from leads.models import *
from form_builder.models import *
from .views import RegisterNotification,join_custom_question_header
from django.views.decorators.csrf import csrf_exempt
from client.utils import noti_via_mail
from client.utils import percentage
from vendors.utils import *
from campaign.choices import *
import logging
import logging.config
import ast


# Abhi 
# cronjob for get database backup and store on google drive
# === getDumpDatabase ===
@csrf_exempt
def script_mail(request):
    message = request.POST.get('message')
    to = [request.POST.get('to')]
    from_email = settings.EMAIL_HOST_USER
    send_mail("CronJon-Backup", message, from_email, to, fail_silently=True,html_message=message)
    
def getDumpDatabase(request):
    import sys
    from django.core.management import call_command
    import socket
    import time

    date = datetime.now()
    currant_date = date.strftime("%d_%B_%Y")
    django_host = socket.gethostname().split('-')[0]
    if django_host == 'ip':
        print(django_host,"django_host")
        print(socket.gethostname(),"hostname")
        if socket.gethostname() == 'ip-172-31-30-161':
            filename='Live_Testing_db'+str(currant_date)+'.json'
        else:
            filename='Marketplace_db'+str(currant_date)+'.json'
    else :
        filename='local_testing_db'+str(currant_date)+'.json'

    filepath='json_dump/database/'+filename

    sysout = sys.stdout
    sys.stdout = open(filepath, 'w')
    call_command('dumpdata')
    time.sleep(.500)
    sys.stdout = sysout
    time.sleep(.500)


    # send file on email
    # subject='Techconnectr Send Database '+filename
    # from_email = settings.EMAIL_HOST_USER
    # plain_message = 'PFA'
    # email = EmailMessage(subject,plain_message,from_email,['suryakant.phalle@trigensoft.com','abhishek@trigensoft.com'])
    # email.attach_file(filepath)
    # email.send()

    # Send file on google drive
    from googleapiclient.discovery import build
    from httplib2 import Http
    from oauth2client import file, client, tools
    from googleapiclient.http import MediaFileUpload,MediaIoBaseDownload
    from techconnectr.settings import database_upload_file

    # Setup the Drive v3 API
    SCOPES = 'https://www.googleapis.com/auth/drive.file'
    # Get api_token.json file containing token credentials
    token_filePath = database_upload_file + 'api_token.json'
    store = file.Storage(token_filePath)
    creds = store.get()
    # 'if' part is run when we don't have token file and is used to fetch & store token credentials

    if not creds or creds.invalid:
        # Get client_secret.json file containing client id, secret key, etc if doesn't have token file
        client_secret_filepath = database_upload_file + 'client_secret.json'
        flow = client.flow_from_clientsecrets(client_secret_filepath, SCOPES)
        creds = tools.run_flow(flow, store)
    drive_service = build('drive', 'v3', http=creds.authorize(Http()))

    # 'file_metadata' -> 'name' -> save as <filename> in google drive

    file_metadata = {
    'name': filename,
    'mimeType': '*/*'
    }
    media = MediaFileUpload(filepath,
                            mimetype='*/*',
                            resumable=True)
    file = drive_service.files().create(body=file_metadata, media_body=media, fields='id').execute()
    # return HttpResponseRedirect('/')

# Remove Junk file in projects like Zip,csv,files.
# folder : static,staticfiles,techconnectr.
# abhi :9-05-2019
# === removeJunkFiles ===
def removeJunkFiles(request):
    
    import glob, os ,sys
    base_dir=settings.BASE_DIR

    #collect zip files
    static_zip = base_dir+'/static/*.zip'
    staticfiles_zip = base_dir+'/staticfiles/*.zip'
    zipfiles = base_dir+'/*.zip'

    #collect csv file
    static_csv = base_dir+'/static/*.csv'
    staticfiles_csv = base_dir+'/staticfiles/*.csv'
    csvfiles = base_dir+'/*.csv'

    #collect json file
    json_files=base_dir+'/json_dump/database/*.json'

    #append all junk files in list
    r  = glob.glob(static_zip)
    r += glob.glob(staticfiles_zip)
    r += glob.glob(zipfiles)
    r += glob.glob(static_csv)
    r += glob.glob(staticfiles_csv)
    r += glob.glob(csvfiles)
    r += glob.glob(json_files)

    #Remove all junk files
    for i in r:
        os.remove(i)
# Abhi
# get all errors,info,warning .
# === readlogs ===
def readlogs(request):

    # from datetime import datetime
    # from pytz import timezone

    # fmt = "%Y-%m-%d %H:%M:%S"

    # # Current time in UTC
    # now_utc = datetime.now(timezone('UTC'))
    # print(now_utc.strftime(fmt))

    # # Convert to US/Pacific time zone
    # now_pacific = now_utc.astimezone(timezone('US/Pacific'))
    # print(now_pacific.strftime(fmt))

    # # Convert to Europe/Berlin time zone
    # now_berlin = now_pacific.astimezone(timezone('Europe/Berlin'))
    # print(now_berlin.strftime(fmt))

    # # Convert to india time zone
    # now_berlin = now_pacific.astimezone(timezone('Asia/Kolkata'))
    # print(now_berlin.strftime(fmt))
    
    base_dir = settings.BASE_DIR
    filepath = base_dir+'/APPNAME.log'
    data = []
    with open(filepath) as f:
        lines = f.readlines()
    lines = [line.rstrip('\n') for line in open(filepath)]

    # amey changes
    dummy = []
    dum = []
    # sorting according to each error in one list and adding that list to parent
    if lines:
        for i in lines:
            if i.startswith('{'):
                dummy.append(dum)
                dum = []
                dum.append(i)
            else:
                dum.append(i)
    final = [j for j in dummy if j != []]

    cnt = len(final)
    return render(request, 'reports/bug_report.html', {'data': final[::-1],'cnt': cnt})

# Amey
# clear log
# === Clear Logs ===
def clear_logs(request):
    '''Clear log file - by Amey Raje'''

    base_dir = settings.BASE_DIR
    filepath = base_dir+'/APPNAME.log'
    with open(filepath, 'w') as f:
        pass
# abhi
# === Display all lead header to insert lead(form) ===
def insert_lead(request,camp_id,camp_alloc_id):
    # Add lead
    labels=get_lead_header(camp_id)
    labels +=join_custom_question_header(camp_id)
    camp_alloc = campaign_allocation.objects.get(id=camp_alloc_id)
    if camp_alloc.submited_lead >= camp_alloc.volume :
        more_leads_uploads= camp_alloc.submited_lead - camp_alloc.volume
        if camp_alloc.return_lead >= more_leads_uploads:
            more_leads_uploads = camp_alloc.return_lead - more_leads_uploads
    else:
        more_leads_uploads=(camp_alloc.volume-camp_alloc.submited_lead)+(camp_alloc.return_lead)
    return render(request, 'leads/addleads.html', {'lead_header':labels,'more_leads_uploads':more_leads_uploads,'camp_id':camp_id,'camp_alloc_id':camp_alloc_id,'camp_alloc':camp_alloc})
 
# abhi  
# ===  Create Custom Header ===
def create_custom_header(request):
    # Create Custom Header
    custom_leads = [str(i) for i in request.POST.getlist('id[]')]
    camp_id=request.POST.get('camp_id')
    fav_lead=favorite_leads_header.objects.filter(campaign_id=camp_id,user_id=request.session['userid'])
    if fav_lead.count() == 1:
        fav_lead_update=favorite_leads_header.objects.get(campaign_id=camp_id,user_id=request.session['userid'])
        fav_lead_update.header=custom_leads
        fav_lead_update.save()
    else:
        favorite_leads_header.objects.create(header=custom_leads,campaign_id=camp_id,user_id=request.session['userid'])

    data={'success':1,'msg':'Your lead headers are save Successfully!...'}
    return JsonResponse(data)

# === Excel lead ===
def excel_lead(request):
    #upload lead by excel
    # Upload Lead by Excel
    
    from client.utils import noti_via_mail
    is_validate='true'
    if 'file1' in request.FILES:
        filehandle = request.FILES['file1']
        camp_id=int(request.POST.get('camp_id'))
        camp_alloc_id=int(request.POST.get('camp_alloc_id'))
        userid = request.session['userid']
        submited_lead = get_last_id_of_lead(int(request.POST.get('camp_alloc_id')))
        last_batch_id = get_last_batch_id(int(request.POST.get('camp_alloc_id')))
        dict = []
        header=[]
        datamsg = {'success': 2, 'msg': 'You Are Using Wrong Template.'}
        print('start-process',datetime.now())
        if filehandle.name.endswith('.csv'):
            data = pandas.read_csv(filehandle,skip_blank_lines=True,na_filter=False,encoding ='latin1')
            data = data.dropna()
            for row in data: #get Template header
                header.append(row)
            if checkTemplate(camp_id, header) == 1:
                dict = create_csv_dict(data,header,submited_lead)
                if dict != []:
                    #per excel upload only 100 lead
                    if len(dict) <= 100:
                        print('call-first-funct',datetime.now())
                        lead_data=check_user_check_error(dict,camp_id,camp_alloc_id,userid,is_validate)
                        if lead_data['success'] == 1:
                            percenta = Lead_Uploaded_Error.objects.filter(campaign_id=camp_id, user_id=request.session['userid']).latest('exact_time')
                            percenta.vendor_percentage = vendorper(camp_alloc_id)
                            percenta.client_percentage = percentage(camp_id)
                            percenta.save()
                            if request.session['usertype'] == '2':
                                client = percenta.campaign.user_id
                                super = [i.id for i in user.objects.filter(usertype_id=4)]
                                super.append(client)
                                noti= noti_via_mail(super, 'Lead Upload', 'Leads uploaded by '+request.session['username'], mail_lead_upload)
                            elif request.session['usertype'] == '1':
                                super = [i.id for i in user.objects.filter(usertype_id=4)]
                                noti = noti_via_mail(super, 'Lead Upload', 'Leads uploaded by '+request.session['username'], mail_lead_upload)
                            datamsg = {'success': 1,'msg': 'Lead Uploaded Successfully','lead_data':lead_data}
                    else:
                        datamsg = {'success': 2, 'msg': 'Can\'t upload More then 100 Lead  Per excel file.'}
                else:
                    datamsg = {'success': 2, 'msg': 'Can\'t upload empty excel file.'}
            else:
                datamsg = {'success': 2, 'msg': 'You Are Using A Wrong Template.'}
        else:
            datamsg = {'success': 2, 'msg': 'You Are Using A Wrong Template.'}
        return JsonResponse(datamsg)
    else:
        datamsg = {'success': 2, 'msg': 'File Not Found, Please try again'}
    return JsonResponse(datamsg)

# === Check Template ===
def checkTemplate(camp_id, fields):
    #checket Template is valid or not
    # Checket Template is valid or not
    camp_lead = Delivery.objects.get(campaign_id=camp_id)
    if camp_lead.custom_header_status == 0 :
        labels = camp_lead.data_header
        labels = labels.split(',')
        labels +=join_custom_question_header(camp_id)
    else:
        labels = camp_lead.custom_header
        labels = labels.split(',')
        labels +=join_custom_question_header(camp_id)
    # strip & replace empty space with '_' ...Akshay G...date - 15th Nov, 2019
    fields = list(map(lambda x: '_'.join(re.split('\s+', x.strip().lower())), fields))
    labels = list(map(lambda x: '_'.join(re.split('\s+', x.strip().lower())), labels))
    if set(fields) == set(labels):
        return 1
    else:
        return 0

# === Convert excel data into ditionary ===
def create_csv_dict(data,header,submited_lead):
    #convert excel data into dictionary
    # Convert excel data into ditionary
    lead_data=[]
    current_data={}
    date = datetime.now()
    currant_date = date.strftime("%B %d %Y")
    time = date.strftime('%H:%M:%S')
    for i in range(len(data)):
        for head in header:
            if data[head][i] != 'non' and data[head][i] != 'nan':
                # strip & replace empty space with '_' ...Akshay G...date - 15th Nov, 2019
                current_data['_'.join(re.split('\s+', head.strip()))]=data[head][i]
            else:
                current_data['_'.join(re.split('\s+', head.strip()))]='-'
        submited_lead += 1
        new_data = {'id':submited_lead,'time':time, 'date': currant_date, 'status': 0}
        current_data.update(new_data)
        lead_data.append(current_data)
        current_data={}
    return lead_data

# === Upload Lead Database ===
def upload_lead_database(dict, camp_alloc_id, userid,is_upload,last_batch_id):
    #upload excel lead data into database
    # Upload excel lead data into database
    print('call-validate_lead_data-funct',datetime.now())
    upload_lead_cnt=len(dict)
    data = campaign_allocation.objects.get(id=camp_alloc_id, client_vendor_id=userid)
    lead_count=1
    if int(data.volume) > 0 :
        lead_count=check_lead_uploadable(int(data.volume),int(data.submited_lead),int(data.return_lead))
        if len(dict) > lead_count:
            dict=dict[0:lead_count]
            upload_lead_cnt=len(dict)

    # check how much lead need to upload for vendor(superadmin allocated lead match with remain.. lea
    # IF user select upload with reject lead or with rejected lead
    # Only that time upload data
    if lead_count > 0 and is_upload == 1:
        if data.submited_lead == 0:
            data.upload_leads = dict
            data.submited_lead = len(dict)
            data.batch_count = last_batch_id
            data.save()
        else:
            old_data=ast.literal_eval(data.upload_leads)
            custom_field=Campaign_custom_field.objects.filter(campaign_id=data.campaign_id,add_header_by_internal_team=True)
            if custom_field.count() > 0 :
                qa_headers=custom_field[0].internal_QA_headers
                dict=checkQAHeaderExist(dict,qa_headers)
            data.upload_leads = old_data + dict
            data.submited_lead = len(old_data + dict)
            data.batch_count = last_batch_id
            data.save()
        # suryakant(11_oct_2019)
        # to raise notification to client on lead upload by vendor
        super = [i.id for i in user.objects.filter(usertype_id=4)]
        super.append(data.campaign.user.id)
        [RegisterNotification(userid,j,f"Leads uploaded by {data.client_vendor.user_name}", 'Lead Upload' , 2,data.campaign,None) for j in super]    
        noti_via_mail(super, 'Lead Upload', 'Leads uploaded by '+data.client_vendor.user_name, mail_lead_upload)
        
    if upload_lead_cnt > 0:
        return {'success':1,'upload_lead_cnt':upload_lead_cnt}
    return {'success':1,'upload_lead_cnt':0}

# === Check Lead Uploadable  ===
def check_lead_uploadable(volume,submited_lead,return_lead):
    #check bulk lead  uploadable or not if yes how many lead can vendor upload
    #following function give exact number of lead
    # check bulk lead  uploadable or not if yes how many lead can vendor upload
    if submited_lead >= volume :
        more_leads_uploads=submited_lead - volume
        if return_lead >= more_leads_uploads:
            more_leads_uploads = return_lead - more_leads_uploads
    else:
        more_leads_uploads=(volume-submited_lead)+(return_lead)
    return more_leads_uploads

# === Submited Add Lead ===
def submited_add_lead(request):
    #manualy add lead data into database
    # Manual add lead data into database
    dict={}
    list1=[]
    all_leads = []
    date=datetime.now()
    date1=datetime.today().strftime('%Y-%m-%d')
    userid=request.session['userid']
    currant_date=date.strftime("%B %d %Y")
    time = date.strftime('%H:%M:%S')
    camp_alloc_id=request.POST.get('camp_alloc_id')
    num_leads=campaign_allocation.objects.get(id=camp_alloc_id)
    all_camp = campaign_allocation.objects.filter(campaign_id=request.POST.get('camp_id'))
    for lead in all_camp:
        if lead.upload_leads != None and lead.upload_leads != '':
            all_leads.extend(ast.literal_eval(lead.upload_leads))
    labels=get_lead_header(request.POST.get('camp_id'))
    labels +=join_custom_question_header(request.POST.get('camp_id'))
    custom_field=Campaign_custom_field.objects.filter(campaign_id=request.POST.get('camp_id'),add_header_by_internal_team=True)
    if num_leads.submited_lead > 0:
        lead_limit_status=1 #add manual time
        validate=check_manualy_lead_data(labels,all_leads,request.POST.get('camp_id'),request,lead_limit_status)
        if validate['success']==1:
            last_lead_id=get_last_id_of_lead(int(request.POST.get('camp_alloc_id')))
            for header in labels:
                new_data={header:request.POST.get(header)}
                dict.update(new_data)
            dict.update({'id':last_lead_id + 1,'time':time,'date':currant_date,'status':0,'batch':0,'TC_lead_status':'valid lead'})
            list1.append(dict)

            if custom_field.count() > 0 :
                qa_headers=custom_field[0].internal_QA_headers
                list1=checkQAHeaderExist(list1,qa_headers)
            if num_leads.submited_lead > 0 :
                old_data=ast.literal_eval(num_leads.upload_leads)
                num_leads.submited_lead=num_leads.submited_lead + 1
                num_leads.upload_leads=old_data + list1
                num_leads.save()
            else:
                num_leads.submited_lead=1
                num_leads.upload_leads=list1
                num_leads.save()
            Lead_Uploaded_Error.objects.create(campaign_id=request.POST.get('camp_id'),created_date=date1,user_id=userid,old_uploaded_lead=old_data,uploaded_lead_count=1,uploaded_lead=list1,lead_upload_status=0)
            # suryakant(11_oct_2019)
            # to raise notification to client on lead upload by vendor
            super = [i.id for i in user.objects.filter(usertype_id=4)]
            super.append(all_camp[0].campaign.user.id)
            [RegisterNotification(request.session['userid'],j,f"Lead uploaded by {request.session['username']}", 'Lead Upload' , 2,all_camp[0].campaign,None) for j in super]
            data={'success':1,'msg':'upload lead Successfully!...'}
            return JsonResponse(data)
        else:
            data={'success':0,'validate':validate['header']}
            return JsonResponse(data)
    else:
        lead_limit_status=1 #add manual time
        validate=check_manualy_lead_data(labels,all_leads,request.POST.get('camp_id'),request,lead_limit_status)
        if validate['success']==1:
            for header in labels:
                new_data={header:request.POST.get(header)}
                dict.update(new_data)
            dict.update({'id':1,'time':time,'date':currant_date,'status':0,'batch':0,'TC_lead_status':'valid lead'})
            list1.append(dict)
            if custom_field.count() > 0 :
                qa_headers=custom_field[0].internal_QA_headers
                list1=checkQAHeaderExist(list1,qa_headers)
            if num_leads.submited_lead > 0 :
                old_data=ast.literal_eval(num_leads.upload_leads)
                num_leads.submited_lead=num_leads.submited_lead + 1
                num_leads.upload_leads=old_data + list1
                num_leads.save()
            else:
                num_leads.submited_lead=1
                num_leads.upload_leads=list1
                num_leads.save()
            Lead_Uploaded_Error.objects.create(campaign_id=request.POST.get('camp_id'),created_date=date1,user_id=userid,old_uploaded_lead=[],uploaded_lead_count=1,uploaded_lead=list1,lead_upload_status=0)
            # suryakant (11_oct_2019)
            # to raise notification to client on lead upload by vendor
            super = [i.id for i in user.objects.filter(usertype_id=4)]
            super.append(all_camp[0].campaign.user.id)
            [RegisterNotification(request.session['userid'],j,f"Lead uploaded by {request.session['username']}", 'Lead Upload' , 2,all_camp[0].campaign,None) for j in super]
            data={'success':1,'msg':'upload lead Successfully!...'}
            return JsonResponse(data)
        data={'success':0,'validate':validate['header']}
        return JsonResponse(data)

# === check QA Header Exist ===
def checkQAHeaderExist(new_leads,qa_headers):
    #abhi - 2-09-2019
    #this function used for add extra header into leads which is created by internal QA team.
    qa_headers=ast.literal_eval(qa_headers)
    for lead in new_leads:
        if 'lead_dict' not in lead.keys():
            lead.update(qa_headers['lead_dict'])
    return new_leads



# === Check Manualy Lead Data ===
def check_manualy_lead_data(labels,list,camp_id,request,lead_limit_status):
    # Validate manual lead data
    import datetime
    match_pattern=['LinkedIn_URL','Phone','Email']
    match_pattern=custom_header_mapping(match_pattern,camp_id)

    lead_validate_data=CampaignMappData.objects.get(campaign_id=camp_id)
    lead_validate_data=ast.literal_eval(lead_validate_data.mapp_date)

    #get default system validate header
    tc_fields=SystemDefaultFields.objects.all()[0]
    tc_fields=ast.literal_eval(tc_fields.tc_default_validate_header)

    custom_validation_list=[]
    if SelectedLeadValidation.objects.filter(campaign=camp_id).count() > 0:
        custom_valid=SelectedLeadValidation.objects.get(campaign=camp_id)
        custom_validation_list=ast.literal_eval(custom_valid.component_list)

    #match dupliacte lead with or other vendor
    if lead_limit_status != 2 :
        for header in match_pattern:
            if header in labels:
                if any(key[header] == request.POST[header] for key in list):
                    return {'success':0,'header':header+str(' Already Exists!...')}
    else:
        # when vendor edit lead that time check duplicate leads  except currant lead
        
        if checkHeaderExists(camp_id,'Email'):
            for lead in range(len(list) - 1, -1, -1):
                if int(list[lead]['id']) == int(request.POST['edit_lead_id']):
                    list.pop(lead)

        for header in match_pattern:
            if header in labels:
                if any(key[header] == request.POST[header] for key in list):
                    return {'success':0,'header':header+str(' Already Exists!...')}



    #headers formating
    match_pattern=['LinkedIn_URL','Zip','Phone','Directline','Email','First_name','Last_name','job_title','Country','Industry','Sub_Industry','Social']
    match_pattern=custom_header_mapping(match_pattern,camp_id)

    if 'header_validate' in custom_validation_list:
        for header in match_pattern:
            if checkHeaderExists(camp_id,header):
                if verify_data_header(camp_id,header,request.POST[header]) != True:
                    return {'success':0,'header':'Please Enter Valid '+header}

    #match abm list
    if 'abm_list_validation' in custom_validation_list:
        abm_list=Specification.objects.get(campaign=camp_id)
        if abm_list.abm_count > 0 and (checkHeaderExists(camp_id,'Company_Name') or checkHeaderExists(camp_id,'Email')):
            abm_list=eval(abm_list.abm_file_content)
            header_label=defaultToCustom('Email',camp_id)
            if header_label !=0 and '@' in request.POST[header_label] :
                domain=request.POST[header_label].split('@')[1]
                # domain=domain.split('.')[0]
                if any(str(d['DOMAIN_NAME']).lower() == str(domain).lower() for d in abm_list) != True:
                    header_label=defaultToCustom('Company_Name',camp_id)
                    # if header_label !=0 :
                    #     if any(str(d['COMPANY_NAME']).lower() == str(request.POST[header_label]).lower() for d in abm_list) != True:
                            # return {'success':0,'header':'Lead Not Match With ABM List'}
                    return {'success':0,'header':'Lead Not Matched With ABM List'}

    #match suppression list
    if 'suppression_list_validation' in  custom_validation_list:
        supp_list=Specification.objects.get(campaign=camp_id)
        if supp_list.suppression_count > 0 and (checkHeaderExists(camp_id,'Company_Name') or checkHeaderExists(camp_id,'Email')):
            supp_list=eval(supp_list.suppression_file_content)
            header_label=defaultToCustom('Email',camp_id)
            if header_label !=0 :
                domain=request.POST[header_label].split('@')[1]
                # domain=domain.split('.')[0]
                if any(str(d['DOMAIN_NAME']).lower() == str(domain).lower() for d in supp_list) == True:
                    header_label=defaultToCustom('Company_Name',camp_id)
                    if header_label !=0 and header_label in request.POST.keys() :
                        if any(str(d['COMPANY_NAME']).lower() == str(request.POST[header_label]).lower() for d in supp_list) == True:
                            return {'success':0,'header':'Lead Matched With suppression List'}
                    return {'success':0,'header':'Lead Matched With suppression List'}

    UsAsTextlist=creatUsAsTextList(camp_id)
    #validate company_size
    if 'company_size' in custom_validation_list:
        company_size=lead_validate_data[0]['Employee_size']
        if checkHeaderExists(camp_id,'Employee_size') and sum(company_size) > 0:
            header_label=defaultToCustom('Employee_size',camp_id)
            if header_label !=0 :
                if companySizeValidate(company_size,request.POST[header_label]) != True:
                    return {'success':0,'header': header_label+' Not match with campaign'}
    
    #validate company revenue
    if 'company_revenue' in custom_validation_list:
        company_revenue=lead_validate_data[0]['Revenue']
        if checkHeaderExists(camp_id,'Revenue') and sum(company_revenue) > 0:
            header_label=defaultToCustom('Revenue',camp_id)
            if header_label !=0 :
                if companyRevenueValidate(company_revenue,request.POST[header_label]) != True:
                    return {'success':0,'header':header_label+' Not matched with campaign'}

    #Industry validate
    if 'industry_type_validation' in custom_validation_list:
        industryList=lead_validate_data[0]['Industry']
        # industryList=' '.join(industryList).split()

        if checkHeaderExists(camp_id,'Industry') and len(industryList) > 0:
            header_label=defaultToCustom('Industry',camp_id)
            if header_label !=0 :
                if IndustryValidate(industryList,request.POST[header_label]) != True:
                    return {'success':0,'header':header_label+' Not matched with campaign'}

    #country validate
    if 'country_validation' in custom_validation_list:
        country_list=lead_validate_data[0]['Country']
        if checkHeaderExists(camp_id,'Country'):
            header_label=defaultToCustom('Country',camp_id)
            if header_label !=0 :
                if CountryValidate(country_list,request.POST[header_label]) != True:
                    return {'success':0,'header':header_label+' Not matched with campaign'}

    #Job Title validate
    # if 'job_title_validation' in custom_validation_list:
    #     job_list=lead_validate_data[0]['Designation']
    #     if checkHeaderExists(camp_id,'job_title'):
    #         header_label=defaultToCustom('job_title',camp_id)
    #         if header_label !=0 :
    #             if JonTitleValidate(job_list,request.POST[header_label]) != True:
    #                 return {'success':0,'header':header_label+' Not Match with campaign.'}

    #custome question validate
    if 'custom_question_validation' in custom_validation_list:
        cust_qus_list=join_custom_question_header(camp_id)
        if len(cust_qus_list) > 0:
            valid_qus_list=lead_validate_data[0]['valid_qus_list']
            for header in cust_qus_list:
                if CustQusValidate(valid_qus_list,request.POST[header],header) != True:
                    return {'success':0,'header':'Answer Not Matched with '+header}



    #check not exceed lead limit
    if 'company_limitation' in custom_validation_list:
        if lead_limit_status != 2:
            if HeaderSpecsValidation.objects.filter(campaign=camp_id).count() > 0:
                company_limit = HeaderSpecsValidation.objects.get(campaign=camp_id)
                company_limit = company_limit.company_limit
            else:
                company_limit=4
            real_data=[]
            if checkHeaderExists(camp_id,'Email'):
                header_label=defaultToCustom('Email',camp_id)
                if header_label !=0 :
                    data={header_label:request.POST[header_label]}
                    real_data.append(data)
                    real_data=count_campany_lead(real_data,camp_id)
                    user_campany_count=real_data[0]['campany_count']

                if lead_limit_status != 3: #not equal to excel time lead update
                    if checkHeaderExists(camp_id,'Email'):
                        try:
                            if user_campany_count - 1 > int(company_limit) :
                                return {'success':0,'header':'exceed lead limits'}
                        except Exception :
                            return False        
                else:
                    header_label=defaultToCustom('Email',camp_id)
                    if header_label !=0 :
                        domain1=request.POST[header_label].split('@')[1]
                        userid=request.session['userid']
                        date=datetime.datetime.today().strftime('%Y-%m-%d')
                        if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).exists():
                            lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
                            lead_data=ast.literal_eval(lead_data.all_lead)
                            for lead in lead_data:
                                header_label=defaultToCustom('Email',camp_id)
                                if header_label !=0 :
                                    if bool(re.search(r"^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$", lead[header_label])):
                                        domain2=lead[header_label].split('@')[1]
                                        if domain1 == domain2:
                                            user_campany_count +=1
                            if user_campany_count > int(company_limit) :
                                return {'success':0,'header':'exceed lead limits'}

    return validate_custom_specs_manual(request.POST,lead_validate_data,tc_fields)

# === Delete Vendor Lead ===
def delete_vendor_lead(request):
    #delete lead
    # Delete vendor lead
    return_lead=0
    camp_alloc_id=request.POST.get('camp_alloc_id')
    lead_id=int(request.POST.get('id'))
    camp_desc=campaign_allocation.objects.get(id=camp_alloc_id)
    lead_desc=ast.literal_eval(camp_desc.upload_leads)
    for lead in range(len(lead_desc) - 1, -1, -1):
        if lead_desc[lead]['id'] == lead_id:
            deleted_lead_batch = lead_desc[lead]['batch']
            lead_desc.pop(lead)
    batchcount = update_batches(lead_desc,deleted_lead_batch)
    if int(camp_desc.return_lead) > 0:
        return_lead=int(camp_desc.return_lead - 1)
    else:
        return_lead=int(camp_desc.return_lead)

    camp_desc.submited_lead=len(lead_desc)
    camp_desc.return_lead =return_lead
    camp_desc.upload_leads=lead_desc
    camp_desc.batch_count = batchcount
    camp_desc.save()
    data={'success':1}
    return JsonResponse(data)

# === Delete Vendor Error Lead ===
def delete_vendor_error_lead(request):
    # Delete vendor lead  from lead error list
    return_lead=0
    camp_alloc_id=request.POST.get('camp_alloc_id')
    camp_id=int(request.POST.get('camp_id'))
    lead_id=int(request.POST.get('id'))
    userid=request.session['userid']
    date=datetime.today().strftime('%Y-%m-%d')
    lead_data={'success':1}
    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).exists():
        lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
        leads=[]

        lead_desc=ast.literal_eval(lead_data.all_lead)
        for lead in range(len(lead_desc) - 1, -1, -1):
            if lead_desc[lead]['id'] == lead_id:
                lead_desc.pop(lead)
        lead_data.all_lead=lead_desc
        lead_data.all_lead_count=len(lead_desc)
        lead_data.save()
    data={'success':1}
    return JsonResponse(data)

# === Campaign Json ===
'''#abhi-30/07/2019.'''
'''#save all required specification for campaign.'''
def campaign_json(request,camp_id):
    camapign_json_data,dict_data=[],{}
    useastext_list=creatUsAsTextList(camp_id)
    
    valid_qus_list=[]
    data=[]
    #Default Validate Fields By System
    Employee_size=getCompanySizeList(camp_id,useastext_list)
    Revenue=getCompanyRevenueList(camp_id,useastext_list)
    Industry=getCampaignIndustry(camp_id,useastext_list)
    Country=getCampaignCountry(camp_id,useastext_list)
    Designation=getCampaignJobTitle(camp_id)

    cust_qus_list=join_custom_question_header(camp_id)
    if len(cust_qus_list) > 0:
        valid_qus_list=Question_forms.objects.get(campaign_id=camp_id)
        valid_qus_list=ast.literal_eval(valid_qus_list.questions)

    #get default system validate header
    tc_fields=SystemDefaultFields.objects.all()[0]
    tc_fields=ast.literal_eval(tc_fields.tc_default_validate_header)

    #generate Dynamic Dict
    for filed in tc_fields:
        curr_dict={filed:eval(filed)}
        dict_data.update(curr_dict)

    data.append(dict_data)
    if CampaignMappData.objects.filter(campaign_id=camp_id).count() == 0:
        CampaignMappData.objects.create(campaign_id=camp_id,client_id=request.user.id,mapp_date=data)
    else:
        camp_map_data=CampaignMappData.objects.get(campaign=camp_id)
        camp_map_data.mapp_date=data
        camp_map_data.save()

# === Edit Vendor Lead ===
def edit_vendor_lead(request):
    #display data for edit lead
    # Display data for edit lead
    camp_alloc_id=request.POST.get('camp_alloc_id')
    camp_id=request.POST.get('camp_id')
    lead_id=int(request.POST.get('id'))
    camp_desc=campaign_allocation.objects.get(id=camp_alloc_id)
    lead_desc=ast.literal_eval(camp_desc.upload_leads)
    #header as label

    labels=get_lead_header(camp_id)
    labels +=join_custom_question_header(camp_id)
    lead_data={}
    for lead in lead_desc:
        if lead['id'] == lead_id:
            lead_data=lead
    data={'success':1,'header':labels,'lead_data':lead_data}
    return JsonResponse(data)

# === Edit Vendor Error Lead ===
def edit_vendor_error_lead(request):
    #display data for edit error lead
    # Display data for edit lead
    camp_alloc_id=request.POST.get('camp_alloc_id')
    camp_id=request.POST.get('camp_id')
    lead_id=int(request.POST.get('id'))
    userid=request.session['userid']
    date=datetime.today().strftime('%Y-%m-%d')
    lead_data={'success':1}
    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).exists():
        lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
        leads=[]
        lead_desc=ast.literal_eval(lead_data.all_lead)

        #header as label
        labels=get_lead_header(camp_id)
        labels +=join_custom_question_header(camp_id)

        lead_data={}
        for lead in lead_desc:
            if lead['id'] == lead_id:
                lead_data=lead
        lead_data={'success':1,'header':labels,'lead_data':lead_data}
    return JsonResponse(lead_data)

# === Get Lead Header ===
def get_lead_header(camp_id):
    # Get lead header
    camp_lead=Delivery.objects.get(campaign_id=camp_id)
    if camp_lead.custom_header_status == 0 :
        labels = camp_lead.data_header
        labels = list(labels.split(','))
    else:
        labels = camp_lead.custom_header
        labels = labels.split(',')
    return labels

# === Submit Lead Edit Data ===
def submit_lead_edit_data(request):
    #update data of lead
    # Updata data of lead
    #post data

    camp_alloc_id=request.POST.get('camp_alloc_id')
    camp_id=request.POST.get('camp_id')
    lead_id=request.POST.get('edit_lead_id')

    # get leads header to update data
    labels=get_lead_header(camp_id)
    labels +=join_custom_question_header(camp_id)
    #get all lead of vendors
    camp_desc=campaign_allocation.objects.get(id=camp_alloc_id)
    lead_desc=[]
    camp_lead_data=campaign_allocation.objects.filter(campaign_id=camp_id)
    for row in camp_lead_data:
        if row.submited_lead > 0:
            lead_desc +=ast.literal_eval(row.upload_leads)

    title ="Reactify lead updated On "+str(camp_desc.campaign.name)


    #match lead #ID and update lead data
    lead_limit_status=2 #edit lead that time
    validate=check_manualy_lead_data(labels,lead_desc,camp_id,request,lead_limit_status)
    lead_desc=ast.literal_eval(camp_desc.upload_leads)
    # import pdb;pdb.set_trace()
    if validate['success']==1:
        for lead in lead_desc:
            if int(lead['id']) == int(lead_id):
                if lead['status'] == 3: # this condition for client status
                    desc = f"#{str(lead_id)} reactify lead updated by {str(camp_desc.client_vendor.user_name)} in batch-{lead['batch']}"
                    # noti_via_mail(camp_desc.campaign.user_id, title, desc, 1)
                    RegisterNotification(request.session['userid'], camp_desc.campaign.user_id, desc, title, 1,None,camp_desc)
                    lead['status'] = int(4)
                else:
                    if 'lead_dict' in lead.keys() or 'qa_headers' in lead.keys():
                        if lead[lead['qa_headers'][0]] == 3 : # this condition for internal qa team
                            desc = f"#{str(lead_id)} reactify lead updated by {str(camp_desc.client_vendor.user_name)} in batch-{lead['batch']}"
                            # noti_via_mail(camp_desc.campaign.user_id, title, desc, 1)
                            RegisterNotification(request.session['userid'], camp_desc.campaign.user_id, desc, title, 1,None,camp_desc)
                            lead[lead['qa_headers'][0]] = int(4)

                for key in labels:
                    lead[key]=request.POST.get(key)
                    # if key == status and status is in str format, convert it to int...Akshay G...Date - 15th Nov, 2019
                    if key == 'status' and type(lead[key]) == str:
                        if lead[key].isdigit():
                            lead[key] = eval(lead[key])
                    lead['TC_lead_status']="valid lead"
                break
        #store lead data bact to database
        camp_desc.submited_lead=len(lead_desc)
        camp_desc.upload_leads=lead_desc
        camp_desc.save()
        data={'success':1,'msg':'Lead Data updated Successfully!...'}
    else:
        data={'success':0,'msg':validate['header']}
    return JsonResponse(data)

# === Submit Lead Error Edit Data ===
def submit_lead_error_edit_data(request):
    # update data of lead
    # Updata data of lead
    # post data
    camp_alloc_id=request.POST.get('camp_alloc_id')
    camp_id=request.POST.get('camp_id')
    lead_id=int(request.POST.get('edit_error_lead_id'))
    userid=request.session['userid']
    date=datetime.today().strftime('%Y-%m-%d')

    # get leads header to update data
    labels=get_lead_header(camp_id)
    labels +=join_custom_question_header(camp_id)
    #get all lead of vendors
    lead_desc=[]
    camp_lead_data=campaign_allocation.objects.filter(campaign_id=camp_id)
    for row in camp_lead_data:
        if row.submited_lead > 0:
            lead_desc +=ast.literal_eval(row.upload_leads)

    #match lead #ID and update lead data
    lead_limit_status=3 #update lead of excel sheet that time
    validate=check_manualy_lead_data(labels,lead_desc,camp_id,request,lead_limit_status)
    if validate['success']==1:
        lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
        lead_desc=ast.literal_eval(lead_data.all_lead)
        for lead in lead_desc:
            if lead['id'] == lead_id:
                for key in labels:
                    lead[key]=request.POST.get(key)
                lead['TC_lead_status']="valid lead"
                break
        #store lead data bact to database
        lead_data.all_lead_count = len(lead_desc)
        lead_data.all_lead = lead_desc
        lead_data.uploaded_lead_count = 1 + int(lead_data.uploaded_lead_count)
        lead_data.save()
        data={'success':1,'msg':'upload lead Successfully!...'}
    else:
        data={'success':0,'validate':validate['header']}
    return JsonResponse(data)

# === Check Lead Data ===
def check_lead_data(request):
    #validate all campaign lead data
    # Validate all campaign lead data
    filehandle = request.FILES['file1']
    camp_id=int(request.POST.get('camp_id'))
    camp_alloc_id=int(request.POST.get('camp_alloc_id'))
    userid = request.session['userid']
    header=[]
    lead_data=[]
    current_data={}
    jsondata={'success':0}
    data = pandas.read_csv(filehandle)
    if filehandle.name.endswith('.csv'): #check template formate .csv
        for row in data: #get Template header
            header.append(row)
        if checkTemplate(camp_id, header) == 1: #check valid template or not for campaign
             last_lead_id=get_last_id_of_lead(camp_alloc_id) # get last id of campaign vendor's lead id
             lead_data = create_csv_dict(data,header,last_lead_id)
             lead_status=validate_lead_data(lead_data,camp_id,camp_alloc_id,userid)
             if lead_status['success'] > 0:
                 jsondata={'success':1}
    return JsonResponse(jsondata)

# === Get Last Id of Lead ===
def get_last_id_of_lead(camp_alloc_id):
    #get last id of lead data campaign
    # Get last id of lead
    camp_desc=campaign_allocation.objects.get(id=camp_alloc_id)
    if camp_desc.upload_leads:
        lead_desc=ast.literal_eval(camp_desc.upload_leads)
        if len(lead_desc) > 0:
            return lead_desc[-1]['id']
        return 0
    else:
        return 1

# === Validat lead data ===
def validate_lead_data(real_data,camp_id,camp_alloc_id,userid,is_validate):
    #validate lead  data lead_status = 1 (duplicate_with_out),2(duplicate_with_vendor)
    # Validat lead data
   
    user_lead_cnt=len(real_data)
    match_pattern=['LinkedIn_URL','Phone','Directline','Zip','Email','First_name','Last_name','job_title','Country','Industry','Sub_Industry','Social']
    unique_record_match_pattern=['Email']

    match_pattern=custom_header_mapping(match_pattern,camp_id)
    unique_record_match_pattern=custom_header_mapping(unique_record_match_pattern,camp_id)

    custom_validation_list=[]
    if SelectedLeadValidation.objects.filter(campaign=camp_id).count() > 0:
        custom_valid=SelectedLeadValidation.objects.get(campaign=camp_id)
        custom_validation_list=ast.literal_eval(custom_valid.component_list)

    lead_validate_data=CampaignMappData.objects.get(campaign_id=camp_id)
    lead_validate_data=ast.literal_eval(lead_validate_data.mapp_date)

    tc_fields=SystemDefaultFields.objects.all()[0]
    tc_fields=ast.literal_eval(tc_fields.tc_default_validate_header)

    
    # check custom validation list accordint to campaign
    UsAsTextlist=creatUsAsTextList(camp_id)
    camp_lead=Delivery.objects.get(campaign_id=camp_id)
    data_header_status=camp_lead.custom_header_status

    print('call-start_validation-funct',datetime.now())
    
    if 'header_validate' in custom_validation_list:
        real_data = header_validate(real_data,camp_id,camp_alloc_id,userid,match_pattern,3,data_header_status)
    print('call-header_validate-funct',datetime.now())

    if 'abm_list_validation' in custom_validation_list:
        real_data = abm_list_validation(real_data,camp_id,camp_alloc_id,userid,match_pattern,3)

    print('call-abm_list_validation-funct',datetime.now())        
    if 'suppression_list_validation' in  custom_validation_list:
        real_data = suppression_list_validation(real_data,camp_id,camp_alloc_id,userid,match_pattern,3)

    print('call-suppression_list_validation-funct',datetime.now())            
    if 'match_lead_with_currant_vendor' in custom_validation_list:
        real_data = match_lead_with_currant_vendor(real_data,camp_id,camp_alloc_id,userid,unique_record_match_pattern,1,data_header_status)
    
    print('call-match_lead_with_currant_vendor-funct',datetime.now())            
    if 'Duplicate_entry_match_with_vendor' in custom_validation_list:
        real_data = Duplicate_entry_match_with_vendor(real_data,camp_id,camp_alloc_id,userid,unique_record_match_pattern,2,data_header_status)

    print('call-Duplicate_entry_match_with_vendor-funct',datetime.now())          
    if 'Duplicate_entry_match_with_months' in custom_validation_list:
        real_data = Duplicate_entry_match_with_months(real_data,camp_id,camp_alloc_id,userid,unique_record_match_pattern,2,data_header_status)

    print('call-Duplicate_entry_match_with_months-funct',datetime.now())                  
    if 'company_size' in custom_validation_list:
        real_data = company_size(real_data,camp_id,camp_alloc_id,userid,match_pattern,3,UsAsTextlist,data_header_status,lead_validate_data)

    print('call-company_size-funct',datetime.now())                   
    if 'company_revenue' in custom_validation_list:
        real_data = company_revenue(real_data,camp_id,camp_alloc_id,userid,match_pattern,3,UsAsTextlist,data_header_status,lead_validate_data)

    print('call-company_revenue-funct',datetime.now())                               
    if 'industry_type_validation' in custom_validation_list:
        real_data = industry_type_validation(real_data,camp_id,camp_alloc_id,userid,match_pattern,3,UsAsTextlist,data_header_status,lead_validate_data)
            
    print('call-industry_type_validation-funct',datetime.now())                                           
    if 'country_validation' in custom_validation_list:
        real_data = country_validation(real_data,camp_id,camp_alloc_id,userid,match_pattern,3,UsAsTextlist,data_header_status,lead_validate_data)

    print('call-country_validation-funct',datetime.now())         
    # if 'job_title_validation' in custom_validation_list:
    #     real_data = job_title_validation(real_data,camp_id,camp_alloc_id,userid,match_pattern,3,data_header_status,lead_validate_data)

    if 'company_limitation' in custom_validation_list:
        real_data = count_campany_lead(real_data,camp_id)
        real_data = company_limitation(real_data,camp_id,camp_alloc_id,userid,match_pattern,3,data_header_status)

    print('call-company_limitation-funct',datetime.now())     
    if 'custom_question_validation' in custom_validation_list:
        real_data = custom_question_validation(real_data,camp_id,camp_alloc_id,userid,match_pattern,3,lead_validate_data)

    print('call-custom_question_validation-funct',datetime.now())    
    #validate custom header of client which is not match with tc
    real_data = validate_custom_specs(real_data,lead_validate_data,tc_fields,camp_id,userid)
    print('call-validate_custom_specs-funct',datetime.now()) 
    
    # save right lead into database
    
    if len(real_data) > 0:
        real_data=remove_duplicated_lead(real_data,unique_record_match_pattern,camp_id,userid)
        print('call-remove_duplicated_lead-funct',datetime.now())

        actual_lead=save_rejected_lead_gets_right_leads(real_data,camp_id,userid)
        print('call-save_rejected_lead_gets_right_leads-funct',datetime.now())

        if len(actual_lead) > 0:
            save_right_lead_database(actual_lead,camp_id,userid,camp_alloc_id)
            print('call-save_right_lead_database-funct',datetime.now())
            return get_lead_uploads_count(camp_id,userid,user_lead_cnt)
    return get_lead_uploads_count(camp_id,userid,user_lead_cnt)



def save_rejected_lead_gets_right_leads(data,camp_id,userid):
    real_data = []
    rejecte_lead = []
    for lead in data:
        if 'TC_lead_status' in lead.keys():
            rejecte_lead.append(lead)
        else:
            lead['TC_lead_status'] = 'valid lead'
            real_data.append(lead)
    store_lead_upload_error_into_database(camp_id,userid,rejecte_lead,3)
    return real_data


# === Validate Custom Specs ===
'''# abhi-2-8-2019 '''
'''# custom campaign spec validate vie excel '''
def validate_custom_specs(data,lead_validate_data,tc_fields,camp_id,userid):
    flag=0
    rejected_data,real_data=[],[]
    for lead in data:
        for row in lead_validate_data[0]:
            if row not in tc_fields:
                lead_validate_data[0][row] = list(filter(None, lead_validate_data[0][row]))
                #key exists in lead,value not in  valid list,key value should be list
                if row in lead and lead[row] not in lead_validate_data[0][row] and type(lead_validate_data[0][row]) == list and len(lead_validate_data[0][row]) > 0:
                    if 'TC_lead_status' in lead.keys():
                        lead['TC_lead_status'] +=row+' Not Matched. ,'
                    else:
                        lead['TC_lead_status'] =row+' Not Matched. ,'
                    flag = 1
                    # break
        if flag == 1:
            # rejected_data.append(lead)
            real_data.append(lead)
            # store_lead_upload_error_into_database(camp_id,userid,lead,3)
        else:
            # lead['TC_lead_status']='valid lead'
            real_data.append(lead)
        flag=0    
    return real_data

# === Validate Custom Specs Manual ===
'''#abhi-2-8-2019 '''
'''#custom campaign spec validate vie manual '''
def validate_custom_specs_manual(data,lead_validate_data,tc_fields):
    for row in lead_validate_data[0]:
        test_list = [i for i in lead_validate_data[0][row] if i] #remove empty values from list
        if row not in tc_fields and len(test_list) > 0:
            #key exists in lead,value not in  valid list,key value should be list
            if row in data and data[row] not in lead_validate_data[0][row] and type(lead_validate_data[0][row]) == list:
               return  {'success':0,'header':row+' Not Matched.'}
    return {'success':1}

# === Delivery ===
def delivery(request):
    import csv
    import json
    delivery_data=[]
    headers=[]
    reader = csv.DictReader(open("/home/trigen/Documents/delivery.csv"))
    headerFlag=0
    for raw in reader:

        if raw['Headers'].strip():
            headers.append(raw['Headers'])


# === Creat use as Text List ===
def creatUsAsTextList(camp_id):
    # This Fuction used for create list for  use as text lead validation
    # Ex. united states ==> Us    (validate us)
    # Ex. Technology    ==> Tech  (validate)
    # Ex. 0-$999,999    ==> 0-$100
    useastext_list=[]
    useastext_list.append({
                            'company_size_list':company_size_use_as_text(camp_id),
                            'company_revenue_list':company_revenue_use_as_text(camp_id),
                            'industry_type':industry_type_use_as_text(camp_id),
                            'country_list':country_list_use_as_text(camp_id)
                        })
    return useastext_list

# === Company Size use as Text ===
def company_size_use_as_text(camp_id):
    # This Fuction used for create company size list accordint tp  use as text
    # Ex. 1-15    ==> 1-10 or 5-10
    # Ex. 1-15    ==> asd-asd that time take default value means = 1-15
    #if checkHeaderExists(camp_id,'Employee_size'):
    sizes = []
    size=Mapping.objects.get(campaign=camp_id)
    if size.company_size:
        if len(str(size.company_size)) > 0 :
            sizes=list(size.company_size.split(','))
            useAsText=UseAsTxtMapping.objects.filter(campaign=camp_id).exclude(alternative_txt='')
            for text in useAsText:
                if text.original_txt in sizes:
                    f=1
                    checks=list(text.alternative_txt.split('-'))
                    for check in checks:
                        if check.isdigit() != True :
                            f=0
                            break
                    if f==1:
                        sizes.append(text.alternative_txt)
                        sizes.remove(text.original_txt)
    return sizes
    # return []

# === Company Revenue use as Text ===
def company_revenue_use_as_text(camp_id):
    # This Fuction used for create company revenue list accordint tp  use as text
    # Ex. $1 mill - $ 15 mill    ==> $1 mill - $10 mill
    sizes_range,sizes=[],[]
    # if checkHeaderExists(camp_id,'Revenue'):
    size=Mapping.objects.get(campaign=camp_id)
    useAsText=UseAsTxtMapping.objects.filter(campaign=camp_id).exclude(alternative_txt='')
    if size.revenue_size:
        if len(str(size.revenue_size)) > 0 :
            sizes=list(re.split(''',(?=(?:[^'"]|'[^']*'|"[^"]*")*$)''',size.revenue_size))
            if len(sizes) > 2:
                if '999' in sizes: # when client select 0-$999,999 or more values that time
                    sizes.remove('999')
                    sizes.remove('0 - $999')
                    sizes.append('0 - $999,999')
            else:
                if '999' in sizes: # when client select only 0-$999,999  values that time
                    return ['0 - $999,999']
            for text in useAsText:
                if text.original_txt in sizes:
                    textRevenue = replaceMultiple(text.alternative_txt, [' ',',','$','mill','m','M','b','B','billion','-'] , "")
                    if textRevenue.isdigit() == True:
                        sizes.append(text.alternative_txt)
                        sizes.remove(text.original_txt)
    return sizes
    # return []

# === Industry Type use as Text ===
def industry_type_use_as_text(camp_id):
    # This Fuction used for create Industry list accordint tp  use as text
    # Ex. Technology    ==> tech
    # if checkHeaderExists(camp_id,'Industry'):
    industry_type_list = []
    size=Mapping.objects.get(campaign=camp_id)
    # industry_type_list=list(re.split('\/|,',size.industry_type.lower()))
    if size.industry_type is not None:
        if len(size.industry_type) > 0:
            industry_type_list=list(re.split('\/|,',size.industry_type.lower())) if size.industry_type is not None else []
            useAsText=UseAsTxtMapping.objects.filter(campaign=camp_id).exclude(alternative_txt='')
            for text in useAsText:
                ind_list_org=re.split('\/|,',text.original_txt.lower())
                if len(ind_list_org) > 0:
                    for text_org in ind_list_org:
                        if text_org in industry_type_list:
                            ind_list=re.split('\/|,',text.alternative_txt.lower())
                            industry_type_list +=ind_list
                            industry_type_list.remove(text_org)
    return industry_type_list
    # return []

# === Country List use as Text ===
def country_list_use_as_text(camp_id):
    # This Fuction used for create Country list accordint tp  use as text
    # Ex. United States    ==> US
    # if checkHeaderExists(camp_id,'Industry'):
    size=Mapping.objects.get(campaign=camp_id)
    country_list = []
    if size.country:
        country_list=list(size.country.split(','))
    useAsText=UseAsTxtMapping.objects.filter(campaign=camp_id)
    for text in useAsText:
        if text.original_txt in country_list:
            country_list.append(text.alternative_txt.lower())
            country_list.remove(text.original_txt)
    return country_list
    # return []

# === ABM List Validation ===
def abm_list_validation(data,camp_id,camp_alloc_id,userid,match_pattern,lead_status):
    # ABM means lead should  match with ABM list
    # This Fuction used for validate lead to abm list
    real_data,flag=[],0
    import datetime
    abm_list=Specification.objects.get(campaign=camp_id)
    if abm_list.abm_count > 0 and (checkHeaderExists(camp_id,'Company_Name') or checkHeaderExists(camp_id,'Email')):
        abm_list=eval(abm_list.abm_file_content)
        for row in data:
            for lead in row:
                if checkCustomDefault('Email',lead,camp_id) and '@' in row[lead] :
                    domain=row[lead].split('@')[1]
                    # domain=domain.split('.')[0]
                    if any(d['DOMAIN_NAME'].lower() == domain.lower() for d in abm_list) == True:
                        flag=1
                        break
                # elif checkCustomDefault('Company_Name',lead,camp_id):
                #     if any(d['COMPANY_NAME'].lower() != row[lead].lower() for d in abm_list) == True:
                #         flag=1
                #         break
            if flag == 1:
                # row['TC_lead_status']="valid lead"
                real_data.append(row)
            else:
                if 'TC_lead_status' in row.keys():
                    row['TC_lead_status'] +="ABM LIST not matched. ,"
                else:
                    row['TC_lead_status'] ="ABM LIST not matched. ,"
                
                real_data.append(row)
                # store_lead_upload_error_into_database(camp_id,userid,row,3)
            flag=0
        return real_data
    return data

# === Suppression List Validation ===
def suppression_list_validation(data,camp_id,camp_alloc_id,userid,match_pattern,lead_status):
    # suppression means lead should not match with suppression list
    # This Fuction used for validate lead to Suppression list
    real_data,flag=[],0
    import datetime
    supp_list=Specification.objects.get(campaign=camp_id)
    if supp_list.suppression_count > 0 and (checkHeaderExists(camp_id,'Company_Name') or checkHeaderExists(camp_id,'Email')):
        supp_list=eval(supp_list.suppression_file_content)
        for row in data:
            for lead in row:
                if checkCustomDefault('Email',lead,camp_id) and '@' in row[lead] :
                    domain=row[lead].split('@')[1]
                    # domain=domain.split('.')[0]
                    if any(d['DOMAIN_NAME'].lower() == domain.lower() for d in supp_list) != True:
                        flag=1
                        break
                # elif checkCustomDefault('Company_Name',lead,camp_id):
                #     if any(d['COMPANY_NAME'].lower() == row[lead].lower() for d in supp_list) != True:
                #         flag=1
                #         break
            if flag == 1:
                # row['TC_lead_status']="valid lead"
                real_data.append(row)
            else:
                if 'TC_lead_status' in row.keys():
                    row['TC_lead_status'] +="Lead Matched With suppression List. ,"
                else:
                    row['TC_lead_status'] = "Lead Matched With suppression List. ,"

                real_data.append(row)
                # store_lead_upload_error_into_database(camp_id,userid,row,3)
            flag=0
        return real_data
    return data

# === Get Lead Uploads Count ===
def get_lead_uploads_count(camp_id,userid,user_lead_cnt):
    # get all lead type count this last step of lead upload
    # Get all lead type count this last step of lead upload """

    print('call-get_lead_uploads_count-funct',datetime.now())

    date=datetime.today().strftime('%Y-%m-%d')
    data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,status=1).order_by('-id')[:1]
    if data.count() > 0 :
        data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,status=1,id=data[0].id).latest('exact_time')
        #check lead available or not if not then store 0
        if data.duplicate_with_our == None:
            duplicate_with_our=0
        else:
            duplicate_with_our=len(ast.literal_eval(data.duplicate_with_our))
            data.duplicate_with_our_cnt=len(ast.literal_eval(data.duplicate_with_our))

        if data.duplicate_with_vendor == None:
            duplicate_with_vendor=0
        else:
            duplicate_with_vendor=len(ast.literal_eval(data.duplicate_with_vendor))
            data.duplicate_with_vendor_cnt=len(ast.literal_eval(data.duplicate_with_vendor))

        if data.remove_lead_header == None:
            remove_lead_header=0
        else:
            remove_lead_header=len(ast.literal_eval(data.remove_lead_header))
            data.remove_lead_header_cnt=len(ast.literal_eval(data.remove_lead_header))

        if data.remove_duplicate_lead_csv == None:
            remove_duplicate_lead_csv=0
        else:
            remove_duplicate_lead_csv=len(ast.literal_eval(data.remove_duplicate_lead_csv))
            data.remove_duplicate_lead_csv_cnt=len(ast.literal_eval(data.remove_duplicate_lead_csv))

        data.save()

        return {'success':1,'upload_lead_cnt':data.uploaded_lead_count,'user_lead_cnt':user_lead_cnt,'duplicate_with_our':duplicate_with_our,'duplicate_with_vendor':duplicate_with_vendor,'remove_duplicated_lead':remove_lead_header + remove_duplicate_lead_csv}

# === Remove Dublicate Lead ===
def remove_duplicated_lead(leads,match_pattern,camp_id,userid):
    #remove duplicated lead_status
    # Remove dublicate lead """
    data=leads
    for pattern in match_pattern:
        if any(pattern in lead for lead in leads):
            leads=list({lead[pattern]:lead for lead in leads}.values())
    if len(data) > 0 :
        duplicate_lead = list(itertools.filterfalse(lambda x: x in data, leads)) + list(itertools.filterfalse(lambda x: x in leads, data))
        if len(duplicate_lead) > 0:
            for row in duplicate_lead:
                if 'TC_lead_status' in row.keys():
                    row['TC_lead_status'] +="Lead Duplicate in CSV"
                else:
                    row['TC_lead_status'] ="Lead Duplicate in CSV. ,"                    
            # store_lead_upload_error_into_database(camp_id,userid,duplicate_lead,4) #store duplicate leads in csv
    return data

# === Match Lead With Current Vendor ===
def match_lead_with_currant_vendor(data,camp_id,camp_alloc_id,userid,match_pattern,lead_status,data_header_status):
    #first check our lead Duplicate or not
    # First check with current vendor """
    camp_lead_data=campaign_allocation.objects.get(id=camp_alloc_id)
    if camp_lead_data.submited_lead > 0:
        prev_data=ast.literal_eval(camp_lead_data.upload_leads)
        #store prev lead data in database
        if store_old_lead_database(prev_data,camp_id,userid):
            return remove_match_lead(data,prev_data,match_pattern,lead_status,camp_id,userid,data_header_status)

    else:
        return first_time_upload_lead_on_camp(data,match_pattern,camp_id,data_header_status)

# === First Time Upload Lead On Camp ===
def first_time_upload_lead_on_camp(currant_vendor_leads,match_pattern,camp_id,data_header_status):
    #following function use for match two same dictionary
    # function use for match two same ditionary """
    real_data=[]
    header_wrong=[]
    f=0

    lead_pattern=['reason','lead_desc','date','id','time']
    for currant_vendor_lead in currant_vendor_leads:
        for currant_vendor_dict in currant_vendor_lead:
            if currant_vendor_dict not in lead_pattern:
                if currant_vendor_dict in match_pattern:
                    f=1
        if f == 1:
            # currant_vendor_lead['TC_lead_status']="valid lead"
            real_data.append(currant_vendor_lead)
    if len(real_data) > 0 and data_header_status == 1:
        return real_data
    elif len(real_data) == 0   and data_header_status == 1 :
        return currant_vendor_leads
    else:
        return real_data

# === Count Campany Lead ===
def count_campany_lead(data,camp_id):
    # This function used for set count of each and every company lead .
    lead,lead_list,real_data,vendor_lead_list={},[],[],[]
    camp_lead_data=campaign_allocation.objects.filter(campaign_id=camp_id)
    for row in camp_lead_data:
        if row.submited_lead > 0:
            vendor_lead_list +=ast.literal_eval(row.upload_leads)

    # header_label=defaultToCustom('Email',camp_id)
    # header_label_cmp_name=defaultToCustom('Company_Name',camp_id)


    # if header_label == 0 :
    #     header_label='Email'

    # if header_label_cmp_name == 0 :
    #     header_label_cmp_name='Company_Name'


    if checkHeaderExists(camp_id,"Email"):
        for currant_vendor_lead in data:
            for all_vendor_list in currant_vendor_lead:
                if checkCustomDefault('Email',all_vendor_list,camp_id):
                    lead_list.append(currant_vendor_lead)
                    vendor_lead_list +=lead_list
                    lead=set_campany_lead_count(currant_vendor_lead,vendor_lead_list,'Email',camp_id)
                    lead_list=[]
                    break
            real_data.append(lead)
        return real_data
    elif checkHeaderExists(camp_id, 'Company_Name'):
        for currant_vendor_lead in data:
            for all_vendor_list in currant_vendor_lead:
                if checkCustomDefault('Company_Name',all_vendor_list,camp_id):
                    lead=set_campany_lead_count(currant_vendor_lead,vendor_lead_list,'Company_Name',camp_id)
                    break
            real_data.append(lead)
        return real_data
    return data

# === Set Campaign Lead Count ===
def set_campany_lead_count(lead,data,header,camp_id):
    # This function used for match same company name.
    header=defaultToCustom(header,camp_id)
    if header !=0 :

        if checkCustomDefault('Company_Name',header,camp_id):
            if any(d[header].lower() == lead[header].lower() for d in data):
                if 'campany_count' not in lead.keys():
                    lead['campany_count']=1
                else:
                    lead['campany_count'] +=1
            else:
                lead['campany_count']=1
        elif checkCustomDefault('Email',header,camp_id):
            if 'campany_count' not in lead.keys():
                lead['campany_count']=0
                for d in data:
                    domain1=d[header].split('@')
                    domain2=lead[header].split('@')
                    if len(domain1) == 2 and len(domain2) == 2:
                        if domain1[1].lower() == domain2[1].lower():
                            if 'campany_count' not in lead.keys():
                                lead['campany_count']=1
                            else:
                                lead['campany_count'] +=1
    return lead

# === Store Old Lead Database ===
def store_old_lead_database(prev_data,camp_id,userid):
    #store prev lead in database
    # Store old lead in database """
    date=datetime.today().strftime('%Y-%m-%d')
    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,status=1).exists():
        save_lead=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,status=1).latest('exact_time')
        save_lead.old_uploaded_lead=prev_data
        save_lead.save()
    else:
        Lead_Uploaded_Error.objects.create(campaign_id=camp_id,created_date=date,user_id=userid,old_uploaded_lead=prev_data)
    return True

# === Dublicate Entry Match With Vendor ===
def Duplicate_entry_match_with_vendor(data,camp_id,camp_alloc_id,userid,match_pattern,lead_status,data_header_status):
    #check lead is not same with another vendor lead if same reject that lead
    # Duplicate entry match with vendor """
    vendor_lead_list=[]
    camp_lead_data=campaign_allocation.objects.filter(campaign_id=camp_id).exclude(client_vendor_id=userid)
    for row in camp_lead_data:
        if row.submited_lead > 0:
            vendor_lead_list +=ast.literal_eval(row.upload_leads)
    if  len(vendor_lead_list) > 0:
        return remove_match_lead(data,vendor_lead_list,match_pattern,lead_status,camp_id,userid,data_header_status)
    else:
        return  data

# === Dublicate Entry Match With Particolur Months ===
#Abhi 24-Sept-2019
def Duplicate_entry_match_with_months(data,camp_id,camp_alloc_id,userid,match_pattern,lead_status,data_header_status):
    from datetime import datetime as dt
    currentMonth = dt.now().month
    #get dynamically no.of mnth to validate lead.
    header_specs = HeaderSpecsValidation.objects.filter(campaign=camp_id)
    if header_specs.count() > 0:
        x = header_specs[0].lead_validate_latest_mnth
    else:
        x = 4

    vendor_lead_list,month_list,real_data = [],[],[]
    client_id=Campaign.objects.get(id=camp_id)
    client_id=client_id.user_id # get client id for get all campaign of client
    campaign_data = Campaign.objects.filter(user_id=client_id)
    camp_lead_data=campaign_allocation.objects.filter(campaign_id__in=campaign_data,status=1)
    for row in camp_lead_data:
        if row.submited_lead > 0:
            vendor_lead_list +=ast.literal_eval(row.upload_leads)
        
    vendor_lead_list.sort(key=lambda item:item['date'])
    vendor_lead_list = vendor_lead_list
    
    for i in range(x):
        month_list.append(currentMonth-i)

    for row in vendor_lead_list:
        row['date']=dt.strptime(row['date'],'%B %d %Y')
        month=row['date'].month
        if(month in month_list):
            real_data.append(row)
    
    if len(real_data) > 0:
        return remove_match_lead(data,real_data,match_pattern,lead_status,camp_id,userid,data_header_status)
    else:
        return  data            


# === Remove Match Lead ===
def remove_match_lead(data,vendor_lead_list,match_pattern,lead_status,camp_id,userid,data_header_status):
    #pass to list of lead and reject same lead by following function
    # pass to list of lead and reject same lead by following function """
    real_data=[]
    header_wrong=[]
    rejected_data=[]
    flag=2
    for currant_vendor_lead in data:
        for all_vendor_list in vendor_lead_list:
            flag=remove_match_with_dict(currant_vendor_lead,all_vendor_list,match_pattern)
            if flag == 0 or flag == 2:
                break

        if flag == 1:
            # currant_vendor_lead['TC_lead_status']="valid lead"
            real_data.append(currant_vendor_lead)
        else:
            if lead_status == 1:
                # rejected_data.append(currant_vendor_lead)
                if 'TC_lead_status' in currant_vendor_lead.keys():
                    currant_vendor_lead['TC_lead_status'] +="Duplicated lead with same vendor. ,"
                else:
                    currant_vendor_lead['TC_lead_status'] ="Duplicated lead with same vendor. ,"    
                real_data.append(currant_vendor_lead)
            elif lead_status == 2:
                # rejected_data.append(currant_vendor_lead)
                if 'TC_lead_status' in currant_vendor_lead.keys():
                    currant_vendor_lead['TC_lead_status'] +="Duplicated lead with other vendor. ,"
                else:
                    currant_vendor_lead['TC_lead_status'] ="Duplicated lead with other vendor. ,"
                real_data.append(currant_vendor_lead)    

            # store_lead_upload_error_into_database(camp_id,userid,currant_vendor_lead,lead_status)

    if len(real_data) > 0 and data_header_status == 1:
        return real_data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 0 :
        return data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 1 :
        return data
    else:
        return real_data

# === Remove Match With Dictionary ===
def remove_match_with_dict(currant_vendor_lead,all_vendor_list,match_pattern):
    #following function use for match two same dictionary
    # Fuction use for match two same dictionary """
    lead_pattern=['reason','lead_desc','date','id','time']
    for currant_vendor_dict in currant_vendor_lead:
        if currant_vendor_dict not in lead_pattern:
            if currant_vendor_dict in match_pattern:
                try:
                    if currant_vendor_lead[currant_vendor_dict] == all_vendor_list[currant_vendor_dict] :
                        return 0 #duplicate lead
                except Exception :
                    return 1
    return 1

# === Header Validate ===
def header_validate(data,camp_id,camp_alloc_id,userid,match_pattern,lead_status,data_header_status):
    # This function used for validate email or linkedInURL or contact header from CSV file.
    flag=0
    real_data=[]
    rejected_data=[]
    for currant_vendor_lead in data:
        for all_vendor_list in currant_vendor_lead:
            # if all_vendor_list in match_pattern:
            if checkHeaderExists(camp_id,all_vendor_list):
                if verify_data_header(camp_id,all_vendor_list,currant_vendor_lead[all_vendor_list]) != True:
                    if 'TC_lead_status' in currant_vendor_lead.keys():
                        currant_vendor_lead['TC_lead_status'] +=all_vendor_list +' Not Matched with Header. ,'
                    else:
                        currant_vendor_lead['TC_lead_status'] =all_vendor_list +' Not Matched with Header. ,'    
                    flag=1
                    break
                    
        if flag==1:
            # rejected_data.append(currant_vendor_lead)
            # store_lead_upload_error_into_database(camp_id,userid,currant_vendor_lead,3)
            real_data.append(currant_vendor_lead)
        else:
            # currant_vendor_lead['TC_lead_status']='valid lead'
            real_data.append(currant_vendor_lead)
        flag=0
    if len(real_data) > 0 and data_header_status == 1:
        return real_data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 0 :
        return data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 1 :
        return data
    else:
        return real_data

# === Custom Header Mapping ===
def custom_header_mapping(match_pattern,camp_id):
    data=Delivery.objects.get(campaign_id=camp_id)
    custom_header=[]
    if data.custom_header_status == 0:
        return match_pattern
    else:
        header_list=ast.literal_eval(data.custom_header_mapping)
        if len(header_list) > 0 :
            for header in match_pattern:
                for head in header_list:
                    if 'default_header' in head:
                        if head['default_header'] == header:
                            custom_header.append(head['user_header'])
        return custom_header

# === Default To Custome ===
def defaultToCustom(header,camp_id):
    data=Delivery.objects.get(campaign_id=camp_id)
    custom_header=[]
    if data.custom_header_status == 0:
        return header
    else:
        header_list=ast.literal_eval(data.custom_header_mapping)
        if len(header_list) > 0 :
            for head in header_list:
                if 'default_header' in head:
                    if head['default_header'] == header:
                        return head['user_header']
        return 0

# === Check Custome Default ===
def checkCustomDefault(deafult_header,cust_header,camp_id):
    data=Delivery.objects.get(campaign_id=camp_id)
    custom_header=[]
    if data.custom_header_status == 0:
        if deafult_header == cust_header:
            return True
    else:
        header_list=ast.literal_eval(data.custom_header_mapping)
        for head in header_list:
            if 'default_header' in head :
                if head['default_header'] == deafult_header and head['user_header'] == cust_header :
                    return True
    return False

# === Company Size ===
def company_size(data,camp_id,camp_alloc_id,userid,match_pattern,lead_status,useastext_list,data_header_status,lead_validate_data):
    # This function used for validate company size header from CSV file.
    company_size,real_data,flag=[],[],0
    rejected_data=[]
    company_size=lead_validate_data[0]['Employee_size']
    if checkHeaderExists(camp_id,'Employee_size') and sum(company_size) > 0:
        for currant_vendor_lead in data:
            for all_vendor_list in currant_vendor_lead:
                if checkCustomDefault('Employee_size',all_vendor_list,camp_id):
                    if companySizeValidate(company_size,currant_vendor_lead[all_vendor_list]) != True:
                        if 'TC_lead_status' in currant_vendor_lead.keys():
                            currant_vendor_lead['TC_lead_status'] +=all_vendor_list+' Not Matched. ,'
                        else:   
                            currant_vendor_lead['TC_lead_status'] =all_vendor_list+'Not Matched. ,' 
                        flag=1
                        break

            if flag==1:
                # rejected_data.append(currant_vendor_lead)
                real_data.append(currant_vendor_lead)
                # store_lead_upload_error_into_database(camp_id,userid,currant_vendor_lead,3)
            else:
                # currant_vendor_lead['TC_lead_status']='valid lead'
                real_data.append(currant_vendor_lead)
            flag=0
    if len(real_data) > 0 and data_header_status == 1:
        return real_data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 0 :
        return data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 1 :
        return data
    else:
        return real_data

# === Company Revenue ===
def company_revenue(data,camp_id,camp_alloc_id,userid,match_pattern,lead_status,UsAsTextlist,data_header_status,lead_validate_data):
    # This function used for validate company revenue header from CSV file.
    company_revenue,real_data,flag=[],[],0
    rejected_data=[]
    company_revenue=lead_validate_data[0]['Revenue']
    
    if checkHeaderExists(camp_id,'Revenue') and sum(company_revenue) > 0:
        for currant_vendor_lead in data:
            for all_vendor_list in currant_vendor_lead:
                if checkCustomDefault('Revenue',all_vendor_list,camp_id):
                    if companyRevenueValidate(company_revenue,currant_vendor_lead[all_vendor_list]) != True:
                        if 'TC_lead_status' in currant_vendor_lead.keys():
                            currant_vendor_lead['TC_lead_status'] +='Company revenue Not Matched. ,'
                        else:
                            currant_vendor_lead['TC_lead_status'] ='Company revenue Not Matched.'
                        flag=1
                        break

            if flag==1:
                # rejected_data.append(currant_vendor_lead)
                real_data.append(currant_vendor_lead)
                # store_lead_upload_error_into_database(camp_id,userid,currant_vendor_lead,3)
            else:
                # currant_vendor_lead['TC_lead_status']='valid lead'
                real_data.append(currant_vendor_lead)
            flag=0

    if len(real_data) > 0 and data_header_status == 1:
        return real_data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 0 :
        return data
    elif len(real_data) == 0  and len(rejected_data) == 0 and data_header_status == 1 :
        return data
    else:
        return real_data

# === Industry Type Validation ===
def industry_type_validation(data,camp_id,camp_alloc_id,userid,match_pattern,lead_status,useastext_list,data_header_status,lead_validate_data):
    # This function used for validate industry_type  header from CSV file.
    real_data,flag=[],0
    rejected_data=[]
    industryList=lead_validate_data[0]['Industry']
    #ndustryList=' '.join(industryList).split()
    if checkHeaderExists(camp_id,'Industry') and len(industryList) > 0:
        for currant_vendor_lead in data:
            for all_vendor_list in currant_vendor_lead:
                if checkCustomDefault('Industry',all_vendor_list,camp_id):
                    if IndustryValidate(industryList,currant_vendor_lead[all_vendor_list],) != True:
                        if 'TC_lead_status' in currant_vendor_lead.keys():
                            currant_vendor_lead['TC_lead_status'] +='Industry not matched.,'
                        else:    
                            currant_vendor_lead['TC_lead_status'] ='Industry not matched.,'
                        flag=1
                        break

            if flag==1:
                # rejected_data.append(currant_vendor_lead)
                # store_lead_upload_error_into_database(camp_id,userid,currant_vendor_lead,3)
                real_data.append(currant_vendor_lead)
            else:
                # currant_vendor_lead['TC_lead_status']='valid lead'
                real_data.append(currant_vendor_lead)
            flag=0

    if len(real_data) > 0 and data_header_status == 1:
        return real_data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 0 :
        return data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 1 :
        return data
    else:
        return real_data

# === Job Title Validation ===
def job_title_validation(data,camp_id,camp_alloc_id,userid,match_pattern,lead_status,data_header_status,lead_validate_data):
    real_data,flag=[],0
    rejected_data=[]
    job_list=lead_validate_data[0]['Designation']
    if checkHeaderExists(camp_id,'job_title'):
        for currant_vendor_lead in data:
            for all_vendor_list in currant_vendor_lead:
                if checkCustomDefault('job_title',all_vendor_list,camp_id):
                    if JonTitleValidate(job_list,currant_vendor_lead[all_vendor_list]) != True:
                        if 'TC_lead_status' in currant_vendor_lead.keys():
                            currant_vendor_lead['TC_lead_status'] +='Job Title not matched. ,'
                        else:    
                            currant_vendor_lead['TC_lead_status'] ='Job Title not matched. ,'
                        flag=1
                        break

            if flag==1:
                # rejected_data.append(currant_vendor_lead)
                real_data.append(currant_vendor_lead)
                # store_lead_upload_error_into_database(camp_id,userid,currant_vendor_lead,3)
            else:
                # currant_vendor_lead['TC_lead_status']='valid lead'
                real_data.append(currant_vendor_lead)
            flag=0
    if len(real_data) > 0 and data_header_status == 1:
        return real_data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 0 :
        return data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 1 :
        return data
    else:
        return real_data

# === Country Validation ===
def country_validation(data,camp_id,camp_alloc_id,userid,match_pattern,lead_status,useastext_list,data_header_status,lead_validate_data):
    real_data,flag=[],0
    rejected_data=[]
    if checkHeaderExists(camp_id,'Country'):
        country_list=lead_validate_data[0]['Country']
        for currant_vendor_lead in data:
            for all_vendor_list in currant_vendor_lead:
                if checkCustomDefault('Country',all_vendor_list,camp_id):
                    if CountryValidate(country_list,currant_vendor_lead[all_vendor_list]) != True:
                        if 'TC_lead_status' in currant_vendor_lead.keys():
                            currant_vendor_lead['TC_lead_status'] +='Country not matched. ,'
                        else:
                            currant_vendor_lead['TC_lead_status'] ='Country not matched. ,'
                        flag=1
                        break
            if flag==1:
                # rejected_data.append(currant_vendor_lead)
                real_data.append(currant_vendor_lead)
                # store_lead_upload_error_into_database(camp_id,userid,currant_vendor_lead,3)
            else:
                # currant_vendor_lead['TC_lead_status']='valid lead'
                real_data.append(currant_vendor_lead)
            flag=0

    if len(real_data) > 0 and data_header_status == 1:
        return real_data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 0 :
        return data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 1 :
        return data
    else:
        return real_data

# === Company Limitation ===
def company_limitation(data,camp_id,camp_alloc_id,userid,match_pattern,lead_status,data_header_status):
    # This function used for campany limition .
    # means count how many lead is valid for each and every campany.
    # that count is fixed by client.
    # if client want maximum 4 lead from each and every campany.
    real_data,rejected_data,company_limit = [],[],4
    if HeaderSpecsValidation.objects.filter(campaign=camp_id).count() > 0:
        company_limit = HeaderSpecsValidation.objects.get(campaign=camp_id)
        company_limit = company_limit.company_limit
   
    for currant_vendor_lead in data:
        if bool(currant_vendor_lead):
            if 'campany_count' in currant_vendor_lead:
                
                if int(currant_vendor_lead['campany_count']) <= int(company_limit) :
                    # currant_vendor_lead['TC_lead_status']='valid lead'
                    real_data.append(currant_vendor_lead)
                else:
                    if 'TC_lead_status' in currant_vendor_lead.keys():
                        currant_vendor_lead['TC_lead_status'] +='Domain lead exceed. ,'
                    else:    
                        currant_vendor_lead['TC_lead_status'] ='Domain lead exceed. ,'
                    currant_vendor_lead['lead_limit_status']='1'
                    # rejected_data.append(currant_vendor_lead)
                    real_data.append(currant_vendor_lead)
                    # store_lead_upload_error_into_database(camp_id,userid,currant_vendor_lead,3)
    if len(real_data) > 0 and data_header_status == 1:
        return real_data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 1 :
        return data
    elif len(real_data) == 0 and len(rejected_data) == 0 and data_header_status == 1 :
        return data
    else:
        return real_data

# === Custom Question Validation ===
def custom_question_validation(data,camp_id,camp_alloc_id,userid,match_pattern,lead_status,lead_validate_data):
    # This Funstion Used for validate Custom Question
    real_data=[]
    flag=0
    cust_qus_list=join_custom_question_header(camp_id)

    if len(cust_qus_list) > 0:
        #get valid qustion options
        valid_qus_list=lead_validate_data[0]['valid_qus_list']
        for currant_vendor_lead in data:
            for all_vendor_list in currant_vendor_lead:
                if all_vendor_list in cust_qus_list:
                    if CustQusValidate(valid_qus_list,currant_vendor_lead[all_vendor_list],all_vendor_list) != True:
                        if 'TC_lead_status' in currant_vendor_lead.keys():
                            currant_vendor_lead['TC_lead_status'] +='Answer Not match with ' +all_vendor_list +'. ,'
                        else:   
                            currant_vendor_lead['TC_lead_status'] ='Answer Not match with ' +all_vendor_list +'. ,'
                        flag=1
                        break
            if flag==1:
                # store_lead_upload_error_into_database(camp_id,userid,currant_vendor_lead,3)
                real_data.append(currant_vendor_lead)
            else:
                # currant_vendor_lead['TC_lead_status']='valid lead'
                real_data.append(currant_vendor_lead)
            flag=0
        return real_data
    return data

# === Custome Question Validate ===
def CustQusValidate(validList,userOption,header):
    # validate valid answer of question
    userOption=str(userOption)
    if len(str(userOption)) > 1:
        userOption=list(userOption.split(','))
        if userOption[0].isdigit():
            for option in validList:
                if option['id']==header:
                    if option['type'] == 'single_answer':
                        return True
                    for userop in userOption:
                        if userop in option['qualified']:
                            return True
        else:
            for option in validList:
                if option['id']==header:
                    if option['type'] == 'single_answer':
                        return True
            validalist=getCustOptionList(validList,header)
            for userop in userOption:
                if userop.strip().lower() in validalist:
                    return True
    else:
        for option in validList:
            if option['id']==header:
                if option['type'] == 'single_answer':
                    return True
                if userOption in option['qualified']:
                    return True
    return False

# === get Custome Option List ===
def getCustOptionList(validList,header):
    quelifiedList=[]
    for option in validList:
        if option['id']==header:
            for ans in option['qualified']:
                quelifiedList.append(option['options'][0][ans].strip().lower())
    return  quelifiedList

# === Jon Title Validate ===
def JonTitleValidate(job_list,vendorJob):
    # This function used for validate vendor job_title  with client giving Job Title list. """
    vendorJob=list(vendorJob.lower().split(','))
    job_list = [x.strip(' ') for x in job_list]
    if len(job_list) > 0 :
        for job in vendorJob:
            if job in job_list:
                return True
        return False
    return True

# === Country Validate ===
def CountryValidate(country_list,vendorCountry):
    # This function used for validate vendor country  with client giving country list.
    vendorCountry=list(vendorCountry.lower().split(','))
    country_list = [x.lower() for x in country_list]
    if len(country_list) > 0:
        for country in vendorCountry:
            if country.lower() in country_list:
                return True
        return False
    return True

# === Get Campaign Job Title ===
def getCampaignJobTitle(camp_id):
    # This function used for return list of client given job title.
    job_title_suggest=job_title.objects.all()
    mapp_job_title=Mapping.objects.get(campaign=camp_id)
    job_titles=[]
    if mapp_job_title.job_title:
        mapp_job_title=mapp_job_title.job_title.replace(" ", "") if mapp_job_title.job_title is not None else ''
        if mapp_job_title :
            job_titles=list(mapp_job_title.lower().split(',')) # collect all job title from client
            for title in job_title_suggest:  #system can understande manager+ means manager,director,vp,c-level etc.
                if title.title.lower() in job_titles:
                    job_titles +=eval(title.type)
            return job_titles        
    return job_titles

# === Get Campaign Country ===
def getCampaignCountry(camp_id,useastext_list):
    # This function used for return list of client selected country.
    country_list=Mapping.objects.get(campaign=camp_id)
    usetext_count = 0
    if len(useastext_list) > 0:
        usetext_count=len(useastext_list[0]['country_list'])
    if country_list.country :
        if usetext_count > 0:
            country_list=useastext_list[0]['country_list']
        else:
            country_list=list(country_list.country.lower().split(','))
        return country_list
    return []

# === Get Campaign Industry ===
def getCampaignIndustry(camp_id,useastext_list):
    # This function used for return list of client selected Industry.
    industry_type=Mapping.objects.get(campaign=camp_id)
    usetext_count = 0
    if len(useastext_list) > 0:
        usetext_count=len(useastext_list[0]['industry_type'])
    if industry_type.industry_type :
        if usetext_count > 0:
            industry_type_list=useastext_list[0]['industry_type']
        else:
            industry_type_list=list(re.split('\/|,',industry_type.industry_type.lower()))
        return industry_type_list
    return []

# === Industry Validate ===
def IndustryValidate(industryList,vendorIndustry):
    # This function used for validate vendor industry_type  with client giving industryList.
    # vendorIndustry=list(vendorIndustry.lower().split(','))
    # spliting vendor industry type with '/'|','
   
    try:
        vendorIndustry=list(re.split('\/|,',vendorIndustry.lower()))
        for industry in vendorIndustry:
            if industry.lower() in industryList:
                return True
        return False
    except Exception :
        return False    

# === Company Revenue Validate ===
def companyRevenueValidate(company_rev,userRevenue):
    userRevenue = str(userRevenue)
    otherStr = replaceMultiple(userRevenue, [' ', '$','+'] , "")
    size_range=list(otherStr.split('-'))
    regex = re.compile('[0-9$b|BM|m]')
    company_revenue = []
    try:
        match = re.match(regex, userRevenue) is not None
        if len(company_rev) == 1:
            company_revenue.append(0)
            company_revenue.append(max(company_rev))
        else:
            company_revenue = company_rev    
    except Exception :
        return False   
       
    if company_revenue[1] > 0  and match:
        maxRevenue,minRevenue=0,0
        if len(size_range) == 2:
            
            if 'm' in userRevenue:
                minRevenue = re.sub("\D", "",size_range[0])
                maxRevenue = re.sub("\D", "",size_range[1])
                maxRevenue = int(maxRevenue) * 1000000
                minRevenue = int(minRevenue) * 1000000
                
            elif 'b' in userRevenue:
                minRevenue = re.sub("\D", "",size_range[0])
                maxRevenue = re.sub("\D", "",size_range[1])
                maxRevenue = int(maxRevenue) * 1000000000
                minRevenue = int(minRevenue) * 1000000000
                
            if minRevenue >= company_revenue[0] and minRevenue <= company_revenue[1]:
                return True        

        elif len(size_range) == 1:
            if 'm' in userRevenue:
                minRevenue = re.sub("\D", "",size_range[0])
                minRevenue = int(minRevenue) * 1000000
            elif 'b' in userRevenue:
                minRevenue = re.sub("\D", "",size_range[0])
                minRevenue = int(minRevenue) * 1000000000
            else:
                minRevenue = int(re.sub("\D", "",size_range[0]))

            if minRevenue >= company_revenue[0] and minRevenue <= company_revenue[1] :
                return True
    return False

# === Replace Multiple ===
def replaceMultiple(mainString, toBeReplaces, newString):

    # Iterate over the strings to be replaced
    mainString=str(mainString)
    for elem in toBeReplaces :
        # Check if string is in the main string
        if elem in mainString :
            # Replace the string
            mainString = mainString.replace(elem, newString)
    return  mainString

# === Company Size Validate ===
def companySizeValidate(company_size,size):
    #  check company size between campaign specs .
    #  campaign specs=[501-1000],vendor uploade=[501-600]  == True
    #  campaign specs=[501-1000],vendor uploade=[501-5000] == True
    #  campaign specs=[501-1000],vendor uploade=[401-600]  == False
    #  campaign specs=[501-1000],vendor uploade=[550]      == True
    if company_size[0] == 0 and len(company_size) == 1: # when client fill company_size blank that time By default set True
        return True
    company_size=list(map(int,company_size))
    size=str(size)
    
    regex = re.compile('[0-9b|BM|m]')
    if re.match(regex,size) is not None:
        #if 1000+ remove + sign from str 
        size_range=size.replace('+', '')
        if '-' in size_range:
            size_range=list(map(int,size_range.split('-')))
        elif size_range.isdigit():
            size_range=list(map(int,size_range.split()))
        else:
            return False   
        if min(size_range) >= min(company_size) and max(size_range) <= max(company_size):
            return True

    return False

# === Convert Company revenue into List ===
def getCompanyRevenueList(camp_id,useastext_list):
    # Convert Company revenue into List.
    company_revenue_size=[0,0]
    sizes_range=[]
    revenue_size=Mapping.objects.get(campaign=camp_id)
    usetext_count = 0
    if len(useastext_list) > 0:
        usetext_count=len(useastext_list[0]['company_revenue_list'])
    if revenue_size.revenue_size:
        if usetext_count > 0 :
            sizes=[x for xs in useastext_list[0]['company_revenue_list'] for x in xs.split(',')]
        else:
            sizes=list(re.split(''',(?=(?:[^'"]|'[^']*'|"[^"]*")*$)''',revenue_size.revenue_size))

        if len(sizes) >= 2:
            if '999' in sizes and '0 - $999' in sizes: # when client select 0-$999,999 or more values that time
                sizes.remove('999')
                sizes.remove('0 - $999')
                sizes.append('0-$999999')
        else:
            if '999' in sizes: # when client select only 0-$999,999  values that time
                return [0,999999]
        for size in sizes:
            sizes_range +=list(size.split('-'))
        company_revenue_size=[]

        sizes_range=[s.replace(' ', '') for s in sizes_range]
        sizes_range=[s.replace('million', '000000') for s in sizes_range]
        sizes_range=[s.replace('mill', '000000') for s in sizes_range]
        sizes_range=[s.replace('m', '000000') for s in sizes_range]
        sizes_range=[s.replace('$', '') for s in sizes_range]
        sizes_range=[s.replace('billion','000000000')for s in sizes_range]
        sizes_range=[s.replace('bill','000000000')for s in sizes_range]
        sizes_range=[s.replace('b','000000000')for s in sizes_range]
        if len(sizes_range) == 1 and '10000000000+' in sizes_range:
            sizes_range=[s.replace('+','00000')for s in sizes_range]
            sizes_range.append(1000000000)
        else:
            sizes_range=[s.replace('+','00000')for s in sizes_range]
        sizes_range = list(map(int, sizes_range))
        if len(sizes_range) > 1 :
            company_revenue_size.append(min(sizes_range))
            company_revenue_size.append(max(sizes_range))
        else:
            company_revenue_size.append(0)
            company_revenue_size.append(max(sizes_range))
    return company_revenue_size

# === Convert Company Size into List. ===
def getCompanySizeList(camp_id,useastext_list):
    # Convert Company Size into List.
    company_size=[0,0]
    usetext_count = 0
    if len(useastext_list) > 0:
        usetext_count=len(useastext_list[0]['company_size_list'])
    size=Mapping.objects.get(campaign=camp_id)
    if size.company_size:
        if usetext_count > 0 :
            sizes=useastext_list[0]['company_size_list']
        else:
            sizes=list(size.company_size.split(','))
        listSize=[]
        company_size=[]
        for size in sizes:
            listSize +=list(size.split('-'))
            listSize = [x.strip(' ') for x in listSize]
        listSize=[s.replace('+', '00000000000') for s in listSize]    
        listSize = list(map(int, listSize))
        if len(listSize) > 1 :
            company_size.append(min(listSize))
            company_size.append(max(listSize))
        else:
            company_size.append(0)
            company_size.append(max(listSize))
                
    return company_size

# === Check Header exists or not ===
def checkHeaderExists(camp_id,header):
    # Check Header exists or not.
    data=Delivery.objects.get(campaign=camp_id)
    if data.custom_header_status == 0 and data.tc_header_status == 1 :
        headers = list(data.data_header.split(','))
        if header in headers:
            return True
    elif data.custom_header_status == 1:
        header_list=ast.literal_eval(data.custom_header_mapping)
        if len(header_list) > 0:
            for head in header_list:
                if 'default_header' in head:
                    if head['user_header'] == header or head['default_header'] == header:
                        return True
    return False


# === Veryfy data header ===
def verify_data_header(camp_id,header,data):
    #verify data header link LinkedIn_URL or Email
    # Veryfy data header link linkedIn URL or Email """
    domainlist=HeadersValidation.objects.filter()[0]
    domainlist=ast.literal_eval(domainlist.email_list)
    # if checkCustomDefault('LinkedIn_URL',header,camp_id) :
    #     regex = re.compile(r'((http(s?)://)*([www])*\.|[linkedin])[linkedin/~\-]+\.[a-zA-Z0-9/~\-_,&=\?\.;]+[^\.,\s<]')
    #     if re.match(regex,data) is not None:
    #         return True
    #     else:
    #         return False
    if checkCustomDefault('Email',header,camp_id):
        domain=data.split('@')
        if len(domain) == 2:
            if domain[1].lower() not in domainlist:
                return bool(re.search(r"^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$",data))
        return False
    # elif checkCustomDefault('First_name',header,camp_id):
    #     return bool(re.search(r"^([ .A-Za-z])+$",data))
    # elif checkCustomDefault('Last_name',header,camp_id):
    #     return bool(re.search(r"^([ .A-Za-z])+$",data))
    # elif checkCustomDefault('job_title',header,camp_id):
    #     return bool(re.search(r"^([ .A-Za-z,-])+$",data))
    # elif checkCustomDefault('Country',header,camp_id):
    #     return bool(re.search(r"^([ .A-Za-z,])+$",data))
    # elif header == 'Industry':
    #     return bool(re.search(r"^([ .A-Za-z])+$",data))
    # elif checkCustomDefault('Social',header,camp_id):
    #     if len(data) > 0 :
    #         return bool(re.search(r"^([ .A-Za-z])+$",data))
    #     return True
    # elif checkCustomDefault('Phone',header,camp_id):
    #     return bool(re.search(r"^([ .0-9])+$",str(data)))
    # elif checkCustomDefault('Directline',header,camp_id):
    #     return bool(re.search(r"^([ .0-9])+$",str(data)))
    # elif checkCustomDefault('Zip',header,camp_id):
    #     return bool(re.search(r"^([ .0-9,-])+$",str(data)))
    else:
        return True

# === Save Right Lead Database ===
def save_right_lead_database(csv_data,camp_id,userid,camp_alloc_id):
    #when user click on check error that time right lead save in tempararay
    # When use click on check error that time right same in temparary """
    date=datetime.today().strftime('%Y-%m-%d')
    data = campaign_allocation.objects.get(id=camp_alloc_id, client_vendor_id=userid)

    if int(data.volume) > 0 :
        lead_count=check_lead_uploadable(int(data.volume),int(data.submited_lead),int(data.return_lead))
        if len(csv_data) > lead_count:
            csv_data=csv_data[0:lead_count]

    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,status=1).exists():
        save_lead=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,status=1).latest('exact_time')
        save_lead.uploaded_lead=csv_data
        save_lead.uploaded_lead_count=len(csv_data)
        save_lead.save()
    else:
        Lead_Uploaded_Error.objects.create(campaign_id=camp_id,created_date=date,user_id=userid,uploaded_lead=csv_data,uploaded_lead_count=len(csv_data))
    return True

# === Check User Check Error ===
def check_user_check_error(data,camp_id,camp_alloc_id,userid,is_validate):
    #check if user click upload button then only
    # Check if user click on checkerror button then only save right lead into database """
    date=datetime.today().strftime('%Y-%m-%d')
    Lead_Uploaded_Error.objects.create(campaign_id=camp_id,created_date=date,user_id=userid)
    print('call-validate_lead_data-funct',datetime.now())
    valid_lead_data=validate_lead_data(data,camp_id,camp_alloc_id,userid,is_validate)

    if valid_lead_data['success'] > 0:
        save_lead=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,status=1).latest('exact_time')
        if save_lead.uploaded_lead_count > 0 :
            data=ast.literal_eval(save_lead.uploaded_lead)
            save_lead.status=0
            save_lead.save()
            last_id_lead=get_last_id_of_lead(camp_alloc_id)
            last_batch_id = get_last_batch_id(camp_alloc_id)
            for lead in data:
                last_id_lead +=1
                lead['id']=last_id_lead
                lead['batch']=last_batch_id
            #we get data like data={'user_lead_cnt','upload_lead_cnt'}
            lead_data=upload_lead_database(data,camp_alloc_id,userid,0,last_batch_id)
            if lead_data['success'] == 1:
                return merge_two_dicts(lead_data,valid_lead_data)
    if 'upload_lead_cnt' not in valid_lead_data:
        valid_lead_data.update({'upload_lead_cnt':0})
    return valid_lead_data

# === Merge two Dicts ===
def merge_two_dicts(x, y):
    #following function public for marge two dict
    # Following function public for marge two dict """
    z = x.copy()   # start with x's keys and values
    z.update(y)    # modifies z with y's keys and values & returns None
    if 'upload_lead_cnt' not in z:
        z.update({'upload_lead_cnt':0})
    return z

# === Store Lead Upload Error into Database ===
def store_lead_upload_error_into_database(camp_id,userid,currant_vendor_lead,lead_status):
    #store rejected lead to databse for tract status = 0 (read) 1(unread)
    # Store rejected lead to databse for tract status = 0 (read) 1(unread) """
    date=datetime.today().strftime('%Y-%m-%d')
    reject_list=[]
    duplicate_with_our_lead=[]
    duplicate_with_vendor=[]
    if isinstance(currant_vendor_lead,list) :
        reject_list += currant_vendor_lead
    else:
        reject_list.append(currant_vendor_lead)

    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,status=1).exists():
        duplicate_our=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,status=1).latest('exact_time')

        if lead_status == 1:
            if duplicate_our.duplicate_with_our != None:
                duplicate_with_our_lead=ast.literal_eval(duplicate_our.duplicate_with_our)
                data=duplicate_with_our_lead+reject_list
                leads=list({lead['id']:lead for lead in data}.values())
                duplicate_our.duplicate_with_our=leads
                duplicate_our.duplicate_with_our_cnt=len(leads)
                #duplicate_our.duplicate_with_our=reject_list
                duplicate_our.save()
            else:
                duplicate_our.duplicate_with_our=reject_list
                duplicate_our.duplicate_with_our_cnt=len(reject_list)
                duplicate_our.save()
        elif lead_status == 2:
            if duplicate_our.duplicate_with_vendor:
                duplicate_with_vendor=ast.literal_eval(duplicate_our.duplicate_with_vendor)
                data=duplicate_with_vendor+reject_list
                leads=list({lead['id']:lead for lead in data}.values())
                duplicate_our.duplicate_with_vendor=leads
                duplicate_our.duplicate_with_vendor_cnt=len(leads)
                #duplicate_our.duplicate_with_vendor=reject_list
                duplicate_our.save()
            else:
                duplicate_our.duplicate_with_vendor=reject_list
                duplicate_our.duplicate_with_vendor_cnt=len(reject_list)
                duplicate_our.save()
        elif lead_status == 3:
            if duplicate_our.remove_lead_header:
                remove_lead_header=ast.literal_eval(duplicate_our.remove_lead_header)
                data=remove_lead_header+reject_list
                leads=list({lead['id']:lead for lead in data}.values())
                duplicate_our.remove_lead_header=leads
                duplicate_our.remove_lead_header_cnt=len(leads)
                #duplicate_our.duplicate_with_vendor=reject_list
                duplicate_our.save()
            else:

                duplicate_our.remove_lead_header=reject_list
                duplicate_our.remove_lead_header_cnt=len(reject_list)
                duplicate_our.save()
        elif lead_status == 4:
            if duplicate_our.remove_duplicate_lead_csv:
                remove_duplicate_lead_csv=ast.literal_eval(duplicate_our.remove_duplicate_lead_csv)
                data=remove_duplicate_lead_csv+reject_list

                leads=list({lead['id']:lead for lead in data}.values())
                duplicate_our.remove_duplicate_lead_csv=leads
                duplicate_our.remove_duplicate_lead_csv_cnt=len(leads)
                duplicate_our.save()
            else:
                duplicate_our.remove_duplicate_lead_csv=reject_list
                duplicate_our.remove_duplicate_lead_csv_cnt=len(reject_list)
                duplicate_our.save()
    else:
        if lead_status == 1:
            Lead_Uploaded_Error.objects.create(campaign_id=camp_id,created_date=date,user_id=userid,duplicate_with_our=reject_list)
        elif lead_status == 2:
            Lead_Uploaded_Error.objects.create(campaign_id=camp_id,created_date=date,user_id=userid,duplicate_with_vendor=reject_list)
        elif lead_status == 3:
            Lead_Uploaded_Error.objects.create(campaign_id=camp_id,created_date=date,user_id=userid,remove_lead_header=reject_list)
        elif lead_status == 4:
            Lead_Uploaded_Error.objects.create(campaign_id=camp_id,created_date=date,user_id=userid,remove_duplicate_lead_csv=reject_list)
    return True

# === Upload with Rejected Lead ===
def upload_with_rejected_lead(request):
    camp_id=request.POST.get('camp_id')
    camp_alloc_id=request.POST.get('camp_alloc_id')
    userid=request.session['userid']
    date=datetime.today().strftime('%Y-%m-%d')
    lead_data={'success':1}
    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).exists():
        lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
        leads=[]
            # store all leads in leads
        if lead_data.uploaded_lead_count > 0:
            leads=ast.literal_eval(lead_data.uploaded_lead)
        if lead_data.remove_duplicate_lead_csv_cnt > 0:
            leads=leads+ast.literal_eval(lead_data.remove_duplicate_lead_csv)
        if lead_data.remove_lead_header_cnt > 0:
            leads=leads+ast.literal_eval(lead_data.remove_lead_header)
        if lead_data.duplicate_with_vendor_cnt > 0:
            leads=leads+ast.literal_eval(lead_data.duplicate_with_vendor)
        if lead_data.duplicate_with_our_cnt > 0:
            leads=leads+ast.literal_eval(lead_data.duplicate_with_our)
            
        all_lead =int(lead_data.uploaded_lead_count)+int(lead_data.duplicate_with_our_cnt) + int(lead_data.duplicate_with_vendor_cnt) + int(lead_data.remove_lead_header_cnt) + int(lead_data.remove_duplicate_lead_csv_cnt)
        lead_data.lead_upload_status=0
        lead_data.all_lead_count = all_lead
        lead_data.status=0
        lead_data.save()
        last_id_lead=get_last_id_of_lead(camp_alloc_id)
        last_batch_id = get_last_batch_id(camp_alloc_id)
        # last_batch_id += 1
        if len(leads)>0:
            for lead in leads:
                last_id_lead +=1
                lead['id']=last_id_lead
                lead['batch']=last_batch_id
            lead_data=upload_lead_database(leads,camp_alloc_id,userid,1,last_batch_id)
    return JsonResponse(lead_data)

# === Upload with Rejected Lead Web ===
def upload_with_rejected_lead_web(request,camp_id,camp_alloc_id):
    userid=request.session['userid']
    date=datetime.today().strftime('%Y-%m-%d')
    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).exists():
        lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
        leads=[]
            # store all leads in leads
        # Abhi 12-10-2019
        # get all type leads
        all_lead =int(lead_data.uploaded_lead_count)+int(lead_data.duplicate_with_our_cnt) + int(lead_data.duplicate_with_vendor_cnt) + int(lead_data.remove_lead_header_cnt) + int(lead_data.remove_duplicate_lead_csv_cnt)
        

        leads=ast.literal_eval(lead_data.all_lead)
        lead_data.lead_upload_status=0
        lead_data.all_lead_count = all_lead
        lead_data.status=0
        lead_data.save()
        last_id_lead=get_last_id_of_lead(camp_alloc_id)
        last_batch_id = get_last_batch_id(camp_alloc_id)
        if len(leads)>0:
            for lead in leads:
                last_id_lead +=1
                lead['id']=last_id_lead
                lead['batch']=last_batch_id
            lead_data=upload_lead_database(leads,camp_alloc_id,userid,1,last_batch_id)
    camp_detail=campaign_allocation.objects.get(id=camp_alloc_id)
    return redirect('lead_list', camp_id=camp_alloc_id,status=camp_detail.status)

# === Upload without ===
def upload_without_rejected_lead(request):
    camp_id=request.POST.get('camp_id')
    camp_alloc_id=request.POST.get('camp_alloc_id')
    userid=request.session['userid']
    date=datetime.today().strftime('%Y-%m-%d')
    lead_data={'success':1}
    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).exists():
        lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
        leads=[]
            # store all leads in leads
        if lead_data.uploaded_lead_count > 0:
            leads=ast.literal_eval(lead_data.uploaded_lead)
        
        all_lead =int(lead_data.uploaded_lead_count)+int(lead_data.duplicate_with_our_cnt) + int(lead_data.duplicate_with_vendor_cnt) + int(lead_data.remove_lead_header_cnt) + int(lead_data.remove_duplicate_lead_csv_cnt)
        lead_data.lead_upload_status=0
        lead_data.all_lead=all_lead
        lead_data.all_lead_count = all_lead
        lead_data.status=0
        lead_data.save()
        last_id_lead=get_last_id_of_lead(camp_alloc_id)
        last_batch_id = get_last_batch_id(camp_alloc_id)
        if len(leads)>0:
            for lead in leads:
                last_id_lead +=1
                lead['id']=last_id_lead
                lead['batch']=last_batch_id
            lead_data=upload_lead_database(leads,camp_alloc_id,userid,1,last_batch_id)
        else:
            lead_data.delete()
    return JsonResponse(lead_data)

# === Upload Without Rejected Lead Web ===
def upload_without_rejected_lead_web(request,camp_id,camp_alloc_id):
    userid=request.session['userid']
    date=datetime.today().strftime('%Y-%m-%d')
    lead_data={'success':1}
    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).exists():
        lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
        real_data,leads=[],[]
            # store all leads in leads
        
        
        # all_lead =int(lead_data.uploaded_lead_count)+int(lead_data.duplicate_with_our_cnt) + int(lead_data.duplicate_with_vendor_cnt) + int(lead_data.remove_lead_header_cnt) + int(lead_data.remove_duplicate_lead_csv_cnt)
        all_lead = len(ast.literal_eval(lead_data.all_lead))
        leads=ast.literal_eval(lead_data.all_lead)
        lead_data.lead_upload_status=0
        lead_data.all_lead_count = all_lead
        lead_data.status=0
        lead_data.save()

        for lead in leads:
            if lead['TC_lead_status'] == 'valid lead':
                real_data.append(lead)

        last_id_lead=get_last_id_of_lead(camp_alloc_id)
        last_batch_id = get_last_batch_id(camp_alloc_id)
        if len(real_data)>0:
            for lead in real_data:
                last_id_lead +=1
                lead['id']=last_id_lead
                lead['batch']=last_batch_id
            lead_data=upload_lead_database(real_data,camp_alloc_id,userid,1,last_batch_id)
        else:
            lead_data.delete()    
    camp_detail=campaign_allocation.objects.get(id=camp_alloc_id)
    return redirect('lead_list', camp_id=camp_alloc_id,status=camp_detail.status)

# === Upload Lead With Rejected History ===
def upload_lead_with_rejected_history(request,lead_id,camp_alloc_id):
    userid=request.session['userid']
    lead_data=Lead_Uploaded_Error.objects.get(id=lead_id)
    camp_id=lead_data.campaign_id
    leads=[]
        # store all leads in leads
    leads=ast.literal_eval(lead_data.all_lead)
    lead_data.lead_upload_status=0
    lead_data.status=0
    lead_data.save()
    last_id_lead=get_last_id_of_lead(camp_alloc_id)
    last_batch_id = get_last_batch_id(camp_alloc_id)
    if len(leads)>0:
        for lead in leads:
            last_id_lead +=1
            lead['id']=last_id_lead
            lead['batch']=last_batch_id
        lead_data=upload_lead_database(leads,camp_alloc_id,userid,1,last_batch_id)
    camp_detail=campaign_allocation.objects.get(id=camp_alloc_id)
    return  redirect('lead_history', camp_id=camp_id,camp_alloc_id=camp_alloc_id)

# === Upload Lead Without Rejected ===
def upload_lead_without_rejected_history(request,lead_id,camp_alloc_id):
    userid=request.session['userid']
    lead_data=Lead_Uploaded_Error.objects.get(id=lead_id)
    camp_id=lead_data.campaign_id
    leads=[]
        # store all leads in leads
    if lead_data.uploaded_lead_count > 0:
        leads=ast.literal_eval(lead_data.uploaded_lead)
    lead_data.lead_upload_status=0
    lead_data.status=0
    lead_data.save()
    last_id_lead=get_last_id_of_lead(camp_alloc_id)
    last_batch_id = get_last_batch_id(camp_alloc_id)
    if len(leads)>0:
        for lead in leads:
            last_id_lead +=1
            lead['id']=last_id_lead
            lead['batch']=last_batch_id
        lead_data=upload_lead_database(leads,camp_alloc_id,userid,1,last_batch_id)
    return  redirect('lead_history', camp_id=camp_id,camp_alloc_id=camp_alloc_id)

# === Dublicate With Our Lead ===
def duplicate_with_our_lead(request):
    #display error when click on check error (duplicate_with_our)
    # Display error when click on check error (duplicate_with_our) """
    camp_id=request.POST.get('camp_id')
    date=datetime.today().strftime('%Y-%m-%d')
    userid = request.session['userid']
    duplicate_our=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,created_date=date,user_id=userid,status=1).latest('exact_time')
    return JsonResponse(json.dumps(ast.literal_eval(duplicate_our.duplicate_with_our)),safe=False)

# === Dublicate With Other Vndors ===
def duplicate_with_other_vendors(request):
    #display error when click on check error (duplicate_with_other_vendors)
    # Display error when click on check error (duplicate_with_other_vendors) """
    camp_id=request.POST.get('camp_id')
    date=datetime.today().strftime('%Y-%m-%d')
    userid = request.session['userid']
    duplicate_our=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,created_date=date,user_id=userid,status=1).latest('exact_time')
    return JsonResponse(json.dumps(ast.literal_eval(duplicate_our.duplicate_with_vendor)),safe=False)

# === Dublicate With Our Lead Action ===
def duplicate_with_our_lead_action(request,camp_id):
    # action on lead error like(edit,delete)
    # Action on lead error like(edit,delete) """
    date=datetime.today().strftime('%Y-%m-%d')
    userid = request.session['userid']
    duplicate_our=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,created_date=date,user_id=userid,status=1).latest('exact_time')
    lead_data=ast.literal_eval(duplicate_our.duplicate_with_our)

    #if action edit then execute following code
    if request.POST.get('action') == 'edit':
        for dict in lead_data:
            if dict['id'] == int(request.POST.get('id')):
                for key in dict:
                    if key != 'status' and key != 'date' and key != 'time' and key != 'id':
                        dict[key]=request.POST.get(key)
        #save lead into database
        duplicate_our.duplicate_with_our = lead_data
        duplicate_our.save()

    #if action delete then execute following code
    elif request.POST.get('action') == 'delete':
        for lead in range(len(lead_data) - 1, -1, -1):
            if lead_data[lead]['id'] == int(request.POST.get('id')):
                lead_data.pop(lead)
        #save remaining lead into database
        duplicate_our.duplicate_with_our = lead_data
        duplicate_our.save()
    data={'success':1}
    return JsonResponse(data)

# === Dublicate With Other Vendor Lead Action ===
def duplicate_with_other_vendors_lead_action(request,camp_id):
    #action on lead error like(edit,delete)
        # Action on lead error like(edit,delete) """
        date=datetime.today().strftime('%Y-%m-%d')
        userid = request.session['userid']
        duplicate_our=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,created_date=date,user_id=userid,status=1).latest('exact_time')
        lead_data=ast.literal_eval(duplicate_our.duplicate_with_vendor)

        #if action edit then execute following code
        if request.POST.get('action') == 'edit':
            for dict in lead_data:
                if dict['id'] == int(request.POST.get('id')):
                    for key in dict:
                        if key != 'status' and key != 'date' and key != 'time' and key != 'id':
                            dict[key]=request.POST.get(key)
            #save lead into database
            duplicate_our.duplicate_with_vendor = lead_data
            duplicate_our.save()

        #if action delete then execute following code
        elif request.POST.get('action') == 'delete':
            for lead in range(len(lead_data) - 1, -1, -1):
                if lead_data[lead]['id'] == int(request.POST.get('id')):
                    lead_data.pop(lead)
            #save remaining lead into database
            duplicate_our.duplicate_with_vendor = lead_data
            duplicate_our.save()
        data={'success':1}
        return JsonResponse(data)

# === Send detail on email ===
def send_detail_on_email(data,camp_id,camp_alloc_id,filename):
    # Send detail on email """
    from django.template.loader import render_to_string
    from django.utils.html import strip_tags
    from django.core.mail import send_mail

    camp_alloc=campaign_allocation.objects.get(campaign_id=camp_id,id=camp_alloc_id)
    subject='Techconnectr Send Lead Data'
    from_email = settings.EMAIL_HOST_USER

    #send mail to client
    to=[camp_alloc.campaign.user.email]
    html_message = render_to_string('email_templates/export_lead.html', {'data':data,'username':camp_alloc.campaign.user.user_name,'file_name':filename})
    plain_message = strip_tags(html_message)
    send_mail(subject,plain_message,from_email,to,fail_silently=True,html_message=html_message)

    #send mail to vendor
    to=[camp_alloc.client_vendor.email]
    html_message = render_to_string('email_templates/export_lead.html', {'data':data,'username':camp_alloc.client_vendor.user_name,'file_name':filename})
    plain_message = strip_tags(html_message)
    send_mail(subject,plain_message,from_email,to,fail_silently=True,html_message=html_message)

    return True

#Abhi - 9 Nov 2019
#Vendor delete lead as bulk
def bulk_lead_delete(request):
    
    return_lead=0
    camp_alloc_id=request.POST.get('camp_alloc_id')
    batch_ids = eval(request.POST.get('id'))
    
    camp_desc=campaign_allocation.objects.get(id=camp_alloc_id)
    lead_desc=ast.literal_eval(camp_desc.upload_leads)
    delete_lead_cnt=0
    
    for batch in batch_ids:
        for lead in range(len(lead_desc) - 1, -1, -1):
            if lead_desc[lead]['id'] in batch['lead_ids']:
                deleted_lead_batch = lead_desc[lead]['batch']
                lead_desc.pop(lead)
                delete_lead_cnt +=1
    
        batchcount = update_batches(lead_desc,deleted_lead_batch)
        if int(camp_desc.return_lead) > 0:
            return_lead=int(camp_desc.return_lead - delete_lead_cnt)
        else:
            return_lead=int(camp_desc.return_lead)

        camp_desc.submited_lead=len(lead_desc)
        camp_desc.return_lead =return_lead
        camp_desc.upload_leads=lead_desc
        camp_desc.batch_count = batchcount
        camp_desc.save()
    data={'success':1}
    return JsonResponse(data)

# === Send Lead Data From Email ===
def send_lead_data_frm_email(request):
    #send lead data on email to live campaign client
    # Send lead data on email to live campaign client """

    camp_details=Campaign.objects.filter(status=1)
    camp_alloc=campaign_allocation.objects.filter(approve_leads__gt=0,status=1,campaign_id__in=camp_details)
    for camp in camp_alloc:
        camp_name=camp.campaign.name
        to=[camp.campaign.user.email]
        rows = convert_data_list(camp.campaign_id,camp.client_vendor_id,1)
        csvfile = StringIO()
        fieldnames = list(rows[0].keys())
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for row in rows:
             writer.writerow(row)
        subject='Techconnectr Send Lead Data'
        from_email = settings.EMAIL_HOST_USER
        html_message = render_to_string('email_templates/export_lead.html', {'camp_id':camp.campaign_id,'camp_name':camp.campaign.name,'vendor_name':camp.client_vendor.user_name})
        plain_message = strip_tags(html_message)
        email = EmailMessage(subject,plain_message,from_email,to)
        email.attach('file.csv', csvfile.getvalue(), 'text/csv')
        email.send()
    return HttpResponse('/')

# === Convert Data List ===
def convert_data_list(camp_id,vendor_id,status):
    # convert lead data as per header lead
    # Convert lead data as per header lead
    vendor_lead_list=[]
    data=[]
    lead_dict={}
    if Delivery.objects.filter(campaign_id=camp_id).count() > 0:
        camp_lead = Delivery.objects.get(campaign_id=camp_id)
        labels = camp_lead.data_header
        labels = labels.split(',')

        #status=1 means all vendors leads and 2 means particoluar vendor leads
        if status == 1:
            camp_lead_data=campaign_allocation.objects.filter(campaign_id=camp_id,status=1)
            for row in camp_lead_data:
                if row.submited_lead > 0:
                    vendor_lead_list +=ast.literal_eval(row.upload_leads)

            if  len(vendor_lead_list) > 0:
                for dict in vendor_lead_list:
                    if dict['status'] == 1:
                        for key in dict:
                            if key in labels:
                                lead_dict.update({key:dict[key]})
                        data.append(lead_dict)
                        lead_dict={}
                return data
            else:
                return  labels
        else:
            camp_lead_data=campaign_allocation.objects.filter(campaign_id=camp_id,status=1,client_vendor_id=vendor_id)
            for row in camp_lead_data:
                if row.submited_lead > 0:
                    vendor_lead_list +=ast.literal_eval(row.upload_leads)

            if  len(vendor_lead_list) > 0:
                for dict in vendor_lead_list:
                    if dict['status'] == 1:
                        for key in dict:
                            if key in labels:
                                lead_dict.update({key:dict[key]})
                        data.append(lead_dict)
                        lead_dict={}
                return data
            else:
                return  labels

# === Get Last Batch id ===
def get_last_batch_id(camp_alloc_id):
    # get batch id for upload
    camp_desc=campaign_allocation.objects.get(id=camp_alloc_id)
    if camp_desc.batch_count > 0:
        return camp_desc.batch_count + 1
    return 1

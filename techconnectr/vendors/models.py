from django.db import models
from user.models import user
from setupdata.models import *
from campaign.models import *



class VendorType(models.Model):
	type=models.CharField(max_length=30)
	is_active=models.IntegerField(default=1)

	def __str__(self):
		return self.type

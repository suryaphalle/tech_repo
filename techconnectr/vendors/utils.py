from campaign.models import *


def vendorper(camp_all_id):

    percen = campaign_allocation.objects.filter(id=camp_all_id)
    per = 0
    for p in percen:
        if p.client_vendor.usertype.id != 1:
            if p.approve_leads is not None and p.volume is not None:
                per = (per + (p.approve_leads/p.volume)*100)
                return int(per)
        else:
            return 0


def batch_list(leadlist):
    batchlist = []
    for lead in leadlist:
        if lead['batch'] not in batchlist:
            batchlist.append(lead['batch'])
    batchlist.sort()
    count_list = []
    for batch in batchlist:
        batch_count =0
        for lead in leadlist:
            if lead['batch'] == batch:
                lead['rowIndex'] = batch_count
                batch_count += 1
                batch_date = lead['date']
        count_list.append({batch:batch_count,'date':batch_date})
    # print(count_list)
    return (batchlist,count_list,leadlist)

def update_batches(leadlist,del_batch_id):
    new_list = []
    batchlist = [d['batch'] for d in leadlist]
    data = tuple(batchlist)
    newdata = set(data)
    bl = list(newdata)
    # print(bl)
    if del_batch_id not in bl:
        if del_batch_id != 0:
            for batch in bl:
                for lead in leadlist:
                    if lead['batch'] != 0:
                        if lead['batch'] > del_batch_id:
                            if lead['batch'] == batch and batch-1 != 0:
                                lead['batch'] = batch-1

    new_list = batch_list(leadlist)
    newcount = 0
    if new_list[0] != []:
        newcount = new_list[0][-1]
    return newcount


import re
def isValidEmail(email):
    if re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', email) != None:
        return True
    else: 
        return False
from django.shortcuts import render,redirect
from django.views.decorators.csrf import csrf_exempt
from .models import Raised_Tickets, Ticket_Category
from user.models import *
from django.http import HttpResponse, JsonResponse, Http404
from django.core.files.storage import FileSystemStorage
from .utils import *


# Create your views here.
@csrf_exempt
def send_ticket(request):
    """ raised ticket by users"""
    userid = request.session['userid']
    files = []
    if userid:
        ticket = Raised_Tickets.objects.create(ticket_category_id=int(request.POST.get('ticket_category')),
                                               ticket_owner_id=userid, ticket_sub=request.POST.get('ticket_subject'), ticket_status=1,
                                               ticket_description=request.POST.get('ticket_desc'))
        if request.FILES:
            for tc_file in request.FILES.getlist('ticket_image'):
                fs = FileSystemStorage()
                filename = fs.save(tc_file.name, tc_file)
                uploaded_file_url = fs.url(filename)
                files.append(
                    {'filename': filename, 'uploaded_file_url': uploaded_file_url})
        if len(files) > 0:
            ticket.ticket_image = files
        ticket.save()
        to = ticket.ticket_owner.email
        send_tiket_raise_mail(to, ticket)
        data = {'status': 1, 'msg': 'Sit Back, We will contact you ASAP.'}
    return JsonResponse(data)


def send_reply(request):
    """ send quick reply """
    ticket = Raised_Tickets.objects.filter(id=request.POST.get('ticket_id'))
    reciver = user.objects.get(id=request.POST.get('ticket_owner'))
    subject = request.POST.get('mail_subject')
    mail_body = request.POST.get('mail_body')
    if ticket:
        ticket[0].ticket_status = 2
        ticket[0].save()
        ticket_quick_reply_email(reciver.email,ticket[0],subject,mail_body)
        data = {'status': 1, 'msg': 'Reply sent.'}
    return JsonResponse(data)

def to_in_process(request,ticket_id):
    ticket = Raised_Tickets.objects.filter(id=ticket_id)
    if ticket:
        ticket[0].ticket_status = 2
        ticket[0].save()
    return redirect('/superadmin/tech/')


def reolve_ticket(request,ticket_id):
    ticket = Raised_Tickets.objects.filter(id=ticket_id)
    if ticket:
        ticket[0].ticket_status = 3
        ticket[0].save()
    return redirect('/superadmin/tech/')

def close_issue(request,ticket_id):
    ticket = Raised_Tickets.objects.filter(id=ticket_id)
    if ticket:
        ticket[0].ticket_status = 0
        ticket[0].save()
    return redirect('/superadmin/tech/')
from django.db import models

# Create your models here.

from campaign.models import Campaign
from user.models import user


class Ticket_Category(models.Model):
    category = models.CharField(max_length=100)
    is_client = models.BooleanField(default=False)
    is_vendor = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    def __str__(self):
        return self.category


class Raised_Tickets(models.Model):
    ticket_category = models.ForeignKey(Ticket_Category,on_delete=models.CASCADE,null=True,blank=True)
    ticket_owner = models.ForeignKey(user,on_delete=models.CASCADE)
    ticket_sub = models.CharField(max_length=100)
    ticket_description = models.TextField()
    ticket_status = models.CharField(max_length=10,null=True,blank=True)
    ticket_date = models.DateField(auto_now_add=True,null='true')
    ticket_time = models.TimeField(auto_now_add=True,null='true')
    ticket_modified = models.DateTimeField(auto_now=True,null='true')
    ticket_image = models.TextField( blank='true', null='true')

    def __str__(self):
        return f'{self.ticket_owner} - {self.ticket_category} '

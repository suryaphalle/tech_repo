from django.urls import path
from . import views


urlpatterns = [
    path('send_ticket/', views.send_ticket,name="send_ticket"),
    path('send_reply/', views.send_reply,name="send_reply"),

    path('to_in_process/<int:ticket_id>/', views.to_in_process,name="to_in_process"),
    path('reolve_ticket/<int:ticket_id>/', views.reolve_ticket,name="reolve_ticket"),
    path('close_issue/<int:ticket_id>/', views.close_issue,name="close_issue"),

]
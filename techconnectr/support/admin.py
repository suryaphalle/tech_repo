from django.contrib import admin
from .models import *

# Register your models here.

class TicketCategory(admin.ModelAdmin):
    list_display = ('category',)

admin.site.register(Ticket_Category, TicketCategory)

class RaisedTickets(admin.ModelAdmin):
    list_display = ('ticket_category','ticket_owner')
    list_filter = ('ticket_category',)

admin.site.register(Raised_Tickets,RaisedTickets)
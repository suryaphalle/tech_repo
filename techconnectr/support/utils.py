from django.conf import settings
from django.utils.html import strip_tags
from django.template.loader import render_to_string
from django.core.mail import send_mail



def send_tiket_raise_mail(to,ticket):
    subject='New Ticket Added'
    from_email = settings.EMAIL_HOST_USER
    to=['anuprit@techconnectr.com','bob@techconnectr.com','support@trigensoft.com']
    html_message = render_to_string('email_templates/raised_ticket_template.html', {'ticket':ticket})
    plain_message = strip_tags(html_message)
    send_mail(subject,plain_message,from_email,to,fail_silently=True,html_message=html_message)

def ticket_quick_reply_email(to,ticket,subject,reply_text):
    # subject = "Tictet Response"
    from_email = settings.EMAIL_HOST_USER
    to=[ticket.ticket_owner.email,'support@trigensoft.com']
    html_message = render_to_string('email_templates/ticket_reply.html', {'ticket':ticket,'reply_text':reply_text})
    plain_message = strip_tags(html_message)
    send_mail(subject,plain_message,from_email,to,fail_silently=True,html_message=html_message)
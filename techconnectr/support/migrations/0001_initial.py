# Generated by Django 2.1.4 on 2019-07-12 07:39

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Raised_Tickets',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ticket_sub', models.CharField(max_length=100)),
                ('ticket_description', models.TextField()),
                ('ticket_status', models.CharField(blank=True, max_length=10, null=True)),
                ('ticket_date', models.DateField(auto_now_add=True, null='true')),
                ('ticket_time', models.TimeField(auto_now_add=True, null='true')),
                ('ticket_modified', models.DateTimeField(auto_now=True, null='true')),
                ('ticket_image', models.TextField(blank='true', null='true')),
            ],
        ),
        migrations.CreateModel(
            name='Ticket_Category',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('category', models.CharField(max_length=100)),
                ('is_client', models.BooleanField(default=False)),
                ('is_vendor', models.BooleanField(default=False)),
                ('is_active', models.BooleanField(default=True)),
            ],
        ),
        migrations.AddField(
            model_name='raised_tickets',
            name='ticket_category',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='support.Ticket_Category'),
        ),
        migrations.AddField(
            model_name='raised_tickets',
            name='ticket_owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]

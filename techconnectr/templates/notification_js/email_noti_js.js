<script>
function changeInfo(str) {
    if (str == '28')
        leadPullOutNote()
    else if (str == '27')
        campaignAssetsNote()
    else if (str == '26')
        campaignUpdatesNote()
    else if (str == '25')
        vendorApprovalNote()
    else if (str == '24')
        vendorAssignmentNote()
    else if (str == '23')
        cancelAssignedLeadsNote()
    else if (str == '22')
        assignMoreLeadsNote()
    else if (str == '21')
        leadRectifyNote()
    else if (str == '20')
        leadRejectNote()
    else if (str == '19')
        leadApprovalNote()
    else if (str == '18')
        leadUploadNote()
    else if (str == '17')
        rfqQuotationCounterNote()
    else if (str == '16')
        rfqQuotationrejectNote()
    else if (str == '15')
        rfqQuotationapprovalNote()
    else if (str == '14')
        rfqVendorQuotationNote()
    else if (str == '13')
        rfqQuotationRequest()
    else if (str == '12')
        newGroupNote()
    else if (str == '11')
        OnBoardingApprovalNote()
    else if (str == '10')
        newRfqCampaignNote()
    else if (str == '9')
        newCampaignNote()
    else if (str == '8')
        vendorOnBoardingNote()
    else if (str == '7')
        clientOnBoardingNote()
    else if (str == '6')
        externalUserRegister()
    else if (str == '5')
        externalVendorRegister()
    else if (str == '4')
        vendorRegister()
    else if (str == '3')
        clientRegister()
}
function campaignAssetsNote() {
    var str = ""
    $('#field_info').text('Info : Campaign Assets');
    str = "<li>Info regarding Assets uploaded by clients</li>";
    $('#field_desc').empty().append(str);
}
function campaignUpdatesNote() {
    var str = ""
    $('#field_info').text('Info : Campaign Update');
    str = "<li>Specifications update news</li><li>Tells changes made in specifications by client</li>";
    $('#field_desc').empty().append(str);
}
function vendorApprovalNote() {
    var str = ""
    $('#field_info').text('Info : Vendor Approval');
    str = "<li>Vendor approval for campaign </li><li>Info regarding campaign request approved by vendor</li>";
    $('#field_desc').empty().append(str);
}
function vendorAssignmentNote() {
    var str = ""
    $('#field_info').text('Info : Vendor Assignment');
    str = "<li>Assigning vendor to a campaign </li>";
    $('#field_desc').empty().append(str);
}
function cancelAssignedLeadsNote() {
    var str = ""
    $('#field_info').text('Info : Cancel assigned Leads');
    str = "<li>Info regarding cancellation of assigned lead</li>";
    $('#field_desc').empty().append(str);
}
function assignMoreLeadsNote() {
    var str = ""
    $('#field_info').text('Info : Assigning more Leads');
    str = "<li>Info regarding more leads assigned to a vendor</li>";
    $('#field_desc').empty().append(str);
}
function leadRectifyNote() {
    var str = ""
    $('#field_info').text('Info : Lead Rectification');
    str = "<li>Mail notification for client actions on lead</li>";
    $('#field_desc').empty().append(str);
}
function leadPullOutNote() {
    var str = ""
    $('#field_info').text('Info : Pull Out Leads From Allocations');
    str = "<li>Mail notifications from client on pull out leads from allocations</li>";
    $('#field_desc').empty().append(str);
}
function leadRejectNote() {
    var str = ""
    $('#field_info').text('Info : Lead Rejection');
    str = "<li>Mail notification for client actions on lead</li>";
    $('#field_desc').empty().append(str);
}
function leadApprovalNote() {
    var str = ""
    $('#field_info').text('Info : Lead Approval');
    str = "<li>Mail notification for client actions on lead</li>";
    $('#field_desc').empty().append(str);
}
function leadUploadNote() {
    var str = ""
    $('#field_info').text('Info : Lead Upload');
    str = "<li>Mail notification for vendor lead upload on portal</li>";
    $('#field_desc').empty().append(str);
}
function rfqQuotationCounterNote() {
    var str = ""
    $('#field_info').text('Info : Client Counter Action');
    str = "<li>Info regarding client actions on RFQ Quotations</li>";
    $('#field_desc').empty().append(str);
}
function rfqQuotationrejectNote() {
    var str = ""
    $('#field_info').text('Info : Client Reject Action');
    str = "<li>Info regarding client actions on RFQ Quotations</li>";
    $('#field_desc').empty().append(str);
}
function rfqQuotationapprovalNote() {
    var str = ""
    $('#field_info').text('Info : Client approval on RFQ Campaign');
    str = "<li>Info regarding client actions on RFQ Quotations</li>";
    $('#field_desc').empty().append(str);
}
function rfqVendorQuotationNote() {
    var str = ""
    $('#field_info').text('Info : Vendor Quotations');
    str = "<li>Quotations made by vendors for RFQ Campaign</li>";
    $('#field_desc').empty().append(str);
}
function rfqQuotationRequest() {
    var str = ""
    $('#field_info').text('Info : Quotation Requests');
    str = "<li>Quotation requests made by techconnectr for campaign</li>";
    $('#field_desc').empty().append(str);
}
function newGroupNote() {
    var str = ""
    $('#field_info').text('Info : New Group creation');
    str = "<li>New Groups created by client</li>";
    $('#field_desc').empty().append(str);
}
function OnBoardingApprovalNote() {
    var str = ""
    $('#field_info').text('Info : OnBoarding Approval');
    str = "<li>Approval of OnBoarding</li>";
    $('#field_desc').empty().append(str);
}
function newRfqCampaignNote() {
    var str = ""
    $('#field_info').text('Info : New RFQ Campaign');
    str = "<li>New RFQ Campaign notifications</li>";
    $('#field_desc').empty().append(str);
}
function newCampaignNote() {
    var str = ""
    $('#field_info').text('Info : New Campaign (Campaign Set-Up)');
    str = "<li>New Campaign created by client</li>";
    $('#field_desc').empty().append(str);
}
function vendorOnBoardingNote() {
    var str = ""
    $('#field_info').text('Info : OnBoarding Vendors');
    str = "<li>OnBoarding notifications of Vendors</li>";
    $('#field_desc').empty().append(str);
}
function clientOnBoardingNote() {
    var str = ""
    $('#field_info').text('Info : OnBoarding Clients');
    str = "<li>OnBoarding notifications of Clients</li>";
    $('#field_desc').empty().append(str);
}
function externalUserRegister() {
    var str = ""
    $('#field_info').text('Info : New External User');
    str = "<li>New external user created by client</li>";
    $('#field_desc').empty().append(str);
}
function vendorRegister() {
    var str = ""
    $('#field_info').text('Info : New Vendor');
    str = "<li>New Vendor user to Techconnectr</li>";
    $('#field_desc').empty().append(str);
}
function externalVendorRegister() {
    var str = ""
    $('#field_info').text('Info : New External Vendor');
    str = "<li>New External Vendor created by client</li>";
    $('#field_desc').empty().append(str);
}
function clientRegister() {
    var str = ""
    $('#field_info').text('Info : New Client');
    str = "<li>New Client user to Techconnectr</li>";
    $('#field_desc').empty().append(str);
}
</script>

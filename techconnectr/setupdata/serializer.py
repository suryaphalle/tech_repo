from rest_framework import serializers
from setupdata.models import *
from campaign.models import *

class Stateserializers(serializers.ModelSerializer):
    class Meta:
        model=states
        fields=('id','name','country_id')


class State1serializers(serializers.ModelSerializer):
    class Meta:
        model=states1
        fields=('id','name','country_id')



class Cityserializers(serializers.ModelSerializer):
    class Meta:
        model=cities
        fields=('id','name','state_id')

class City1serializers(serializers.ModelSerializer):
    class Meta:
        model=cities1
        fields=('id','name','state_id')


class Chatserializers(serializers.ModelSerializer):
    class Meta:
        model=Campaign_chats
        fields=('id','campaign_id','data')

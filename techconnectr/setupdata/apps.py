from django.apps import AppConfig


class SetupdataConfig(AppConfig):
    name = 'setupdata'

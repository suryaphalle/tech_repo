from rest_framework import serializers
from campaign.models import campaign_allocation

class vendorserielize(serializers.ModelSerializer):
    class Meta:
        model=campaign_allocation
        fields=('id','campaign','client_vendor','level_intent','volume','cpl','status')

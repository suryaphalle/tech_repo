from django.db import models
from user.models import *

# Create your models here.

# Industry Speciality
class industry_speciality(models.Model):
    name = models.CharField(max_length=50)
    order = models.IntegerField(blank='true', null='true')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Industry Speciality'
        verbose_name_plural = 'Industry Speciality'
        ordering = ['order']

class conversation(models.Model):
    user_id2 = models.ForeignKey(user, null=True,on_delete=models.CASCADE, related_name='user_id2')
    user_id1 = models.ForeignKey(user, null=True,on_delete=models.CASCADE, related_name='user_id1')
    data = models.TextField(blank=True, null=True)
    created_date = models.DateField(auto_now_add=True)
    created_time = models.TimeField(auto_now_add=True)



# Industry Type
class industry_type(models.Model):
    type = models.CharField(max_length=100, blank=True)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")
    client = models.ForeignKey(user, on_delete=models.CASCADE, blank=True, null=True)
    status = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'Industry Type'
        verbose_name_plural = 'Industry Types'
        ordering = ['order']

# Campaign_category

class Campaign_category(models.Model):
    type = models.CharField(max_length=50)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'Campaign_category'
        verbose_name_plural = 'Campaign_category'
        ordering = ['order']

class Days(models.Model):
    name = models.CharField(max_length=50)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Day'
        verbose_name_plural = 'Days'
        ordering = ['order']


# Campaign_Spacing

class Campaign_pacing(models.Model):
    type = models.CharField(max_length=100)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'Campaign Pacing'
        verbose_name_plural = 'Campaign Pacings'
        ordering = ['order']


# Campaign Calling
class campaign_calling(models.Model):
    first_name = models.CharField(max_length=100)
    order = models.IntegerField(blank='true', null='true')
    last_name = models.CharField(max_length=100)
    email = models.EmailField()
    comapny_name = models.CharField(max_length=100)
    is_callable = models.NullBooleanField(blank=True,null=True)
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")

    def __str__(self):
        return self.first_name

    class Meta:
        verbose_name = 'Campaign Calling'
        verbose_name_plural = 'Campaign Callings'
        ordering = ['order']


#job Title
class job_title(models.Model):
    title = models.CharField(max_length=50)
    type  = models.TextField(null = True, blank =True)
    order = models.IntegerField(blank='true', null='true')
    
    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'job_title'
        verbose_name_plural = 'job_title'
        ordering = ['order']

# Job Level
class job_level(models.Model):
    type = models.CharField(max_length=50)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")
    client = models.ForeignKey(user, on_delete=models.CASCADE, blank=True, null=True)
    status = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'Job_level'
        verbose_name_plural = 'Job_level'
        ordering = ['order']

# Job Function


class job_function(models.Model):
    type = models.CharField(max_length=50)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")
    client = models.ForeignKey(user, on_delete=models.CASCADE, blank=True, null=True)
    status = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'Job Function'
        verbose_name_plural = 'Job Function'
        ordering = ['order']


# company_size or Employee size
class company_size(models.Model):
    range = models.CharField(max_length=30)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")
    status = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.range

    class Meta:
        verbose_name = 'Company Size'
        verbose_name_plural = 'Company Size'
        ordering = ['order']


# Revenue Size
class RevenueSize(models.Model):
    range = models.CharField(max_length=30)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.BooleanField()
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")
    status = models.PositiveIntegerField(default=0)
    
    def __str__(self):
        return self.range

    class Meta:
        verbose_name = 'Revenue Size'
        verbose_name_plural = 'Revenue Sizes'
        ordering = ['order']


# source in touches
class source_touches(models.Model):
    type = models.CharField(max_length=50)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'Source Touches'
        verbose_name_plural = 'Source Touches'
        ordering = ['order']


# Level of Intent
class level_intent(models.Model):
    type = models.CharField(max_length=50)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'Intent Level'
        verbose_name_plural = 'Intent Level'
        ordering = ['order']


# waterfall
'''class waterfall(models.Model):
	campaign=models.ForeignKey(campaign,on_delete=models.CASCADE)
	level_intent=models.ForeignKey(level_intent,on_delete=models.CASCADE)
	volume=models.IntegerField()
	cpl=models.IntegerField()
	is_active=models.NullBooleanField(blank=True,null=True)
	created_date=models.DateTimeField(auto_now_add=True)
	updated_date=models.DateTimeField(auto_now_add=True)
'''
# Assets


class assets(models.Model):
    type = models.CharField(max_length=50)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")
    client = models.ForeignKey(user, on_delete=models.CASCADE, blank=True, null=True)
    status = models.PositiveIntegerField(default=0)

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'Assets'
        verbose_name_plural = 'Assets'
        ordering = ['order']

# Data header


class data_header(models.Model):
    type = models.CharField(max_length=50)
    custom_headers = models.TextField(blank=True, null=True)  # store custom_header in list
    backend_values = models.CharField(blank='true', null='true',max_length=50)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'Data Header'
        verbose_name_plural = 'Data Header'
        ordering = ['order']


# Custom Data Header
'''
class custom_dataheader(models.Model):
	user=models.ForeignKey(user,on_delete=models.CASCADE)
	data_header=models.ForeignKey(data_header,on_delete=models.CASCADE)
	custom_name=models.CharField(max_length=100)
	is_active=models.NullBooleanField(blank=True,null=True)
	created_date=models.DateTimeField(auto_now_add=True)
	updated_date=models.DateTimeField(auto_now_add=True)


#Custom Field

class custom_question(models.Model):
	campaign=models.ForeignKey(campaign,on_delete=models.CASCADE)
	question=models.TextField()
	rank=models.IntegerField()
	is_active=models.NullBooleanField(blank=True,null=True)
	created_date=models.DateTimeField(auto_now_add=True)
	updated_date=models.DateTimeField(auto_now_add=True)
'''
# Delivery Method


class delivery_method(models.Model):
    type = models.CharField(max_length=50)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'Delivery Method'
        verbose_name_plural = 'Delivery Method'
        ordering = ['order']

# delivery Timing


class delivery_time(models.Model):
    type = models.CharField(max_length=50)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'Delivery Time'
        verbose_name_plural = 'Delivery Time'
        ordering = ['order']

# delivery timing bach
class delivery_time_batch(models.Model):
    day = models.CharField(max_length=20)
    order = models.IntegerField(blank='true', null='true')
    seletect_time = models.TimeField()
    selected_timezont = models.CharField(max_length=20)
    is_active = models.NullBooleanField(blank=True,null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now_add=True,verbose_name="Last Updated Date")

    def __str__(self):
        return self.day

    class Meta:
        verbose_name = 'delivery_time_batch'
        verbose_name_plural = 'delivery_time_batch'
        ordering = ['order']

#region
class region(models.Model):
    name = models.CharField(max_length=20)
    order = models.IntegerField(blank='true', null='true')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'region'
        verbose_name_plural = 'region'
        ordering = ['order']

class region1(models.Model):
    name = models.CharField(max_length=20)
    order = models.IntegerField(blank='true', null='true')

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'region1'
        verbose_name_plural = 'region1'
        ordering = ['order']

# country
class countries(models.Model):
    sortname = models.CharField(max_length=5)
    order = models.IntegerField(blank='true', null='true')
    name = models.CharField(max_length=20)
    phonecode = models.IntegerField()
    region=models.ForeignKey(region,on_delete=models.CASCADE,blank=True,null=True)

    def __str__(self):
        return self.sortname

    class Meta:
        verbose_name = 'countries'
        verbose_name_plural = 'countries'
        ordering = ['order']

class countries1(models.Model):
    name = models.CharField(max_length=100)
    sortname = models.CharField(max_length=5)
    order = models.IntegerField(blank='true', null='true')
    phonecode = models.CharField(max_length=100)
    region=models.ForeignKey(region1,on_delete=models.CASCADE,blank=True,null=True)

    def __str__(self):
        return self.sortname

    class Meta:
        verbose_name = 'countries1'
        verbose_name_plural = 'countries1'
        ordering = ['order']


# state


class states(models.Model):
    name = models.CharField(max_length=20)
    order = models.IntegerField(blank='true', null='true')
    country = models.ForeignKey(countries, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'states'
        verbose_name_plural = 'states'
        ordering = ['order']

class states1(models.Model):
    name = models.CharField(max_length=100)
    order = models.IntegerField(blank='true', null='true')
    country = models.ForeignKey(countries1, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'states1'
        verbose_name_plural = 'states1'
        ordering = ['name']


# City


class cities(models.Model):
    name = models.CharField(max_length=20)
    order = models.IntegerField(blank='true', null='true')
    state = models.ForeignKey(states, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'cities'
        verbose_name_plural = 'cities'
        ordering = ['order']

class cities1(models.Model):
    name = models.CharField(max_length=100)
    order = models.IntegerField(blank='true', null='true')
    state = models.ForeignKey(states1, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'cities1'
        verbose_name_plural = 'cities1'
        ordering = ['name']


# Testimonials
'''
class testimonial(models.Model):
	client_vendor=models.ForeignKey(client_vendor,on_delete=models.CASCADE)
	campaign=models.ForeignKey(campaign,on_delete=models.CASCADE)
	is_active=models.NullBooleanField(blank=True,null=True)
	created_date=models.DateTimeField(auto_now_add=True)
	updated_date=models.DateTimeField(auto_now_add=True)
	satisfaction_level=models.CharField(max_length=30)
	description=models.TextField()
	'''


class notification_type(models.Model):
    type = models.CharField(max_length=25)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.NullBooleanField(blank=True,null=True)

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'notification_type'
        verbose_name_plural = 'notification_type'
        ordering = ['order']

class Notification(models.Model):
    title = models.CharField(max_length=500) # increased char limit - bug fix - amey 19 June 2019
    order = models.IntegerField(blank='true', null='true')
    description = models.CharField(max_length=1000) # increased char limit - bug fix - amey 19 June 2019
    sender = models.ForeignKey(user, on_delete=models.CASCADE)
    created = models.DateTimeField(auto_now_add=True)
    notification_type = models.ForeignKey(
        notification_type, on_delete=models.CASCADE)
    campaign_id = models.ForeignKey('campaign.Campaign',on_delete=models.CASCADE,blank=True,null=True)
    camp_alloc = models.ForeignKey('campaign.campaign_allocation',on_delete=models.CASCADE,blank=True,null=True)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = 'Notification'
        verbose_name_plural = 'Notification'
        ordering = ['order']


class Notification_Reciever(models.Model):
    Notification = models.ForeignKey(Notification, on_delete=models.CASCADE)
    order = models.IntegerField(blank='true', null='true')
    receiver = models.ForeignKey(user, on_delete=models.CASCADE)
    status = models.IntegerField()
    updated = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.Notification.title

    class Meta:
        verbose_name = 'Notification_Reciever'
        verbose_name_plural = 'Notification_Reciever'
        ordering = ['order']

class company_background(models.Model):
    type = models.CharField(max_length=25, blank=True)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.IntegerField(default='1')

    def __str__(self):
        return self.type

    class Meta:
        verbose_name = 'company_background'
        verbose_name_plural = 'company_background'
        ordering = ['order']


class pricing_flexibility(models.Model):
    range = models.CharField(max_length=25, blank=True)
    order = models.IntegerField(blank='true', null='true')
    is_active = models.IntegerField(default='1')

    def __str__(self):
        return self.pricing_flexibility

    class Meta:
        verbose_name = 'pricing_flexibility'
        verbose_name_plural = 'pricing_flexibility'
        ordering = ['order']


class feedback_details(models.Model):
    vendor_rating = models.CharField(max_length=200, blank=True)
    feedback = models.CharField(max_length=200, blank=True)


class MailNotification(models.Model):
    noti_field = models.CharField(max_length=50, blank=True, null=True)
    user = models.ManyToManyField(user, blank=True)
    usertype = models.ManyToManyField(usertype, blank=True)
    parent = models.CharField(max_length=50, blank=True, null=True)

rfq_fields = "[{'display':'Method','status':1,'db_name':'method'},{'display':'Job Title/Function','status':1,'db_name':'job_title_function'},{'display':'ABM','status':1,'db_name':'abm_status'},{'display':'Suppression','status':1,'db_name':'suppression_status'},{'display':'Geo','status':1,'db_name':'geo'},{'display':'Industry Type','status':1,'db_name':'industry_type'},{'display':'Special Instructions','status':0,'db_name':'special_instructions'},{'display':'Company Size','status':0,'db_name':'company_size'},{'display':'Revenue Size','status':0,'db_name':'revenue_size'}]"
profile_view = "{'Acceptance_rate':1,'average_month_cov':1,'vendor_type':1,'industry_type':1,'speciality':1,'sweets_spot_geo':1,'year_incorporate':1,'language_supported':1,'lead_gen_mehtod':1,'complex_program':1,'HQ_loc':1,'call_centre_loc':1,'data_processing_loc':1,'overview':1,'campaign_deliver':1,'latest_connection':1,'logo':1}"
class ClientCustomFields(models.Model):
    client = models.ForeignKey(user, on_delete=models.CASCADE, blank=True, null=True)
    custom_header = models.TextField(blank=True, null=True)
    rfq_popup_fields = models.TextField(blank=True, null=True)
    dashboard_view = models.TextField(blank=True,null=True)
    rfq_rev_share = models.FloatField(default=10)
    normal_rev_share = models.FloatField(default=25)
    profile_view = models.TextField(blank=True,null=True)
    lead_process = models.CharField(max_length=1200,blank=True,null=True)
    tier_level = models.CharField(max_length=10,null=True,blank=True)
    # store mapped values for specs by client for bulk create campaign...Akshay G.
    client_default_mapping = models.TextField(blank=True,null=True)
    # client agreements for each campaign to be signed by vendor...Akshay G.
    client_agreements = models.TextField(blank = True, null = True)
    def __str__(self):
        return self.client.user_name if self.client is not None else ''


dashboard_view_data = '''[{"element": "Counter_container", "class": "col-md-12", "order": "first", "index": 0,"status":1},
{"element": "Latest_campaigns", "class": "col-md-8", "order": "", "index": 1,"status":1},
{"element": "Live_campaigns", "class": "col-md-8", "order": "", "index": 2,"status":1},
{"element": "Client_top_vendors", "class": "col-md-8", "order": "", "index": 3,"status":1},
{"element": "Statistics_project", "class": "col-md-8", "order": "", "index": 4,"status":1},
{"element": "To_do_list", "class": "col-md-4", "order": "", "index": 5,"status":1},
{"element": "Campaign_progress", "class": "col-md-4", "order": "", "index": 6,"status":1},
{"element": "User_activity", "class": "col-md-4", "order": "", "index": 7,"status":1},
{"element": "TC_Top_vendors", "class": "col-md-12", "order": "second", "index": 8,"status":1}]'''
class SystemDefaultFields(models.Model):
    tc_default_validate_header=models.TextField(blank='true', null='true')
    rfq_fields = models.TextField(blank='true', null='true')
    default_dashboard_view = models.TextField(blank='true', null='true')
    rfq_rev_share = models.IntegerField(default=10)
    normal_rev_share = models.IntegerField(default=25)
    profile_view = models.TextField(blank=True,null=True)
    

    def __str__(self):
        return 'System-Data'

# store user default logo...Akshay G...Date - 21st Oct, 2019
class User_default_logo(models.Model):
    title = models.CharField(max_length = 50, blank = True, null = True)
    logo = models.ImageField(upload_to='default_logo/', blank=True, max_length=500, null=True)
    def __str__(self):
        return self.title
from django.contrib import admin
from django.apps import apps
from setupdata.models import *

# Register your models here.
"""
app= apps.get_app_config('setupdata')
for model_name,model in app.models.items():
	admin.site.register(model)
"""
@admin.register(industry_speciality)
class industry_specialityAdmin(admin.ModelAdmin):
	list_display = ("name","order")


@admin.register(industry_type)
class industry_typeAdmin(admin.ModelAdmin):
	list_display = ("type","status","order","is_active","created_date","updated_date")


@admin.register(conversation)
class conversationAdmin(admin.ModelAdmin):
	list_display = ("user_id1","user_id2","created_date","created_time")


@admin.register(Campaign_category)
class Campaign_categoryAdmin(admin.ModelAdmin):
	list_display = ("type","order","is_active","created_date","updated_date")


@admin.register(Days)
class DaysAdmin(admin.ModelAdmin):
	list_display = ("name","order","is_active","created_date","updated_date")


@admin.register(Campaign_pacing)
class Campaign_pacingAdmin(admin.ModelAdmin):
	list_display = ("type","order","is_active","created_date","updated_date")


@admin.register(campaign_calling)
class campaign_callingAdmin(admin.ModelAdmin):
	list_display = ("first_name","order","last_name","email","comapny_name","is_callable","is_active","created_date","updated_date")

@admin.register(job_title)
class job_titleAdmin(admin.ModelAdmin):
	list_display = ("title","type")

@admin.register(job_level)
class job_levelAdmin(admin.ModelAdmin):
	list_display = ("type","order","is_active","created_date","updated_date")


@admin.register(job_function)
class job_functionAdmin(admin.ModelAdmin):
	list_display = ("type","order","is_active","created_date","updated_date")


@admin.register(company_size)
class company_sizeAdmin(admin.ModelAdmin):
	list_display = ("range","status","order","is_active","created_date","updated_date")


@admin.register(RevenueSize)
class RevenueSizeAdmin(admin.ModelAdmin):
	list_display = ("range","order","is_active","created_date","updated_date")



@admin.register(source_touches)
class source_touchesAdmin(admin.ModelAdmin):
	list_display = ("type","order","is_active","created_date","updated_date")

@admin.register(level_intent)
class level_intentAdmin(admin.ModelAdmin):
	list_display = ("type","order","is_active","created_date","updated_date")


@admin.register(assets)
class assetsAdmin(admin.ModelAdmin):
	list_display = ("type","order","is_active","created_date","updated_date")


@admin.register(data_header)
class data_headerAdmin(admin.ModelAdmin):
	list_display = ("type","custom_headers","is_active","created_date","updated_date")


@admin.register(delivery_method)
class delivery_methodAdmin(admin.ModelAdmin):
	list_display = ("type","order","is_active","created_date","updated_date")


@admin.register(delivery_time)
class delivery_timeAdmin(admin.ModelAdmin):
	list_display = ("type","order","is_active","created_date","updated_date")


@admin.register(delivery_time_batch)
class delivery_time_batchAdmin(admin.ModelAdmin):
	list_display = ("day","order","seletect_time","selected_timezont","is_active","created_date","updated_date")


@admin.register(countries)
class countriesAdmin(admin.ModelAdmin):
	list_display = ("sortname","order","name","phonecode")


@admin.register(countries1)
class countries1Admin(admin.ModelAdmin):
	list_display = ("id","name","order","sortname","phonecode")
	search_fields = ('name', 'id')
	list_filter = ('region',)

@admin.register(states)
class statesAdmin(admin.ModelAdmin):
	list_display = ("name","order","country")

@admin.register(states1)
class states1Admin(admin.ModelAdmin):
	list_display = ("id", "name","order","country")
	search_fields = ('name', 'id')

@admin.register(region)
class regionAdmin(admin.ModelAdmin):
	list_display = ("id","name","order")

@admin.register(region1)
class region1Admin(admin.ModelAdmin):
	list_display = ("id","name","order")
	search_fields = ('name', 'id')

@admin.register(cities)
class citiesAdmin(admin.ModelAdmin):
	list_display = ("name","order","state")

@admin.register(cities1)
class cities1Admin(admin.ModelAdmin):
	list_display = ("name","order","state")
	search_fields = ('name', 'id')

@admin.register(notification_type)
class notification_typeAdmin(admin.ModelAdmin):
	list_display = ("type","order","is_active")


@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
	list_display = ("title","order","description","sender","created","notification_type")


@admin.register(Notification_Reciever)
class Notification_RecieverAdmin(admin.ModelAdmin):
	list_display = ("Notification","order","receiver","status","updated")


@admin.register(company_background)
class company_backgroundAdmin(admin.ModelAdmin):
	list_display = ("type","order","order","is_active")


@admin.register(pricing_flexibility)
class pricing_flexibilityAdmin(admin.ModelAdmin):
	list_display = ("range","order","is_active")


@admin.register(feedback_details)
class feedback_detailsAdmin(admin.ModelAdmin):
	list_display = ("vendor_rating","feedback")

@admin.register(MailNotification)
class MailNotificationAdmin(admin.ModelAdmin):
	list_display = ("id", 'noti_field')
@admin.register(SystemDefaultFields)
class SystemDefaultFieldsAdmin(admin.ModelAdmin):
	list_display =("id","tc_default_validate_header")

admin.site.register(ClientCustomFields)
admin.site.register(User_default_logo)
# Generated by Django 2.1.4 on 2019-08-10 13:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('setupdata', '0009_systemdefaultfields_tc_rev_share'),
    ]

    operations = [
        migrations.AddField(
            model_name='company_size',
            name='status',
            field=models.PositiveIntegerField(default=0),
        ),
    ]

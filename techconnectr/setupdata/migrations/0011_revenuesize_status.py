# Generated by Django 2.1.4 on 2019-08-10 13:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('setupdata', '0010_company_size_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='revenuesize',
            name='status',
            field=models.PositiveIntegerField(default=0),
        ),
    ]

# Generated by Django 2.1.4 on 2019-08-05 17:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('setupdata', '0006_auto_20190801_1134'),
    ]

    operations = [
        migrations.AddField(
            model_name='clientcustomfields',
            name='dashboard_view',
            field=models.TextField(blank=True, null=True),
        ),
    ]

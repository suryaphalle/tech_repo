# Generated by Django 2.1.4 on 2019-08-16 13:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('setupdata', '0013_clientcustomfields_profile_view'),
    ]

    operations = [
        migrations.AddField(
            model_name='systemdefaultfields',
            name='profile_view',
            field=models.TextField(blank=True, null=True),
        ),
    ]

# Generated by Django 2.1.4 on 2019-08-19 12:18

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('setupdata', '0015_conversation'),
    ]

    operations = [
        migrations.AddField(
            model_name='conversation',
            name='user_id2',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_id2', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='conversation',
            name='user_id1',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='user_id1', to=settings.AUTH_USER_MODEL),
        ),
    ]

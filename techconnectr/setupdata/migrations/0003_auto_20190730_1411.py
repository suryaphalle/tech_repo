# Generated by Django 2.1.4 on 2019-07-30 14:11

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('setupdata', '0002_clientcustomheaders'),
    ]

    operations = [
        migrations.CreateModel(
            name='ClientCustomFields',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('custom_header', models.TextField(blank=True, null=True)),
                ('rfq_popup_fields', models.TextField(default="[{'Method':1,'db_name':'method'},{'Job Title/Function':1,'db_name':'job_title_function'},{'ABM':1,'db_name':'abm_status'},{'Suppression':1,'db_name':'suppression_status'},{'Geo':1,'db_name':'geo'},{'Industry Type':1,'db_name':'industry_type'},{'Special Instructions':0,'db_name':'special_instructions'},{'Company Size':0,'db_name':'company_size'},{'Revenue Size':0,'db_name':'revenue_size'}]")),
                ('client', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.RemoveField(
            model_name='clientcustomheaders',
            name='client',
        ),
        migrations.DeleteModel(
            name='ClientCustomHeaders',
        ),
    ]

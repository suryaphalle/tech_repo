import os
from django.urls import path, include, re_path
from . import views
from leads.views import *
from django.urls import path

# from rest_framework.urlpatterns import format_suffix_patterns
from django.views.generic import TemplateView

urlpatterns = [
    path('lead-delivery-report/', views.lead_delivery_report, name='lead_delivery_report'),

    path('lead-report/', views.lead_report, name='lead_report'),
    
    path('ajax/lead_report_data/', views.lead_report_data, name='lead_report_data'),
]
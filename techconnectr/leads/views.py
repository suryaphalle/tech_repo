from django.shortcuts import render
from client.views.report_view import *
from client.views.views import *
from django.http import HttpResponse, JsonResponse, Http404
import json

# === Lead Delivery Report ===
def lead_delivery_report(request):
	# to return lead delivery report page
	campaign_list = client_manage_campaign(request)
	return render(request, 'reports/lead_delivery/leads_delivery_Report.html', {'campaign_list': campaign_list})

# === Lead Report ===
def lead_report(request):
	# to return lead report page 
	camp_vendors = []
	total_leads = []
	chart_data = []

	client = request.session['userid']
	campaigns = Campaign.objects.filter(user_id=client,status=1)
	leads = campaign_allocation.objects.filter(campaign__in=campaigns)
	for vendor in leads:
		if vendor.submited_lead > 0:
			camp_vendors.append({'id': str(vendor.client_vendor.id),
                            'name': str(vendor.client_vendor.user_name)})
			total_leads += eval(vendor.upload_leads)
	
	approved_leads = len([d for d in total_leads if d['status'] == 1])
	rejected_leads = len([d for d in total_leads if d['status'] == 2])

	for camp in campaigns:
		vendors = campaign_allocation.objects.filter(campaign_id=camp.id, status=1)
		ap_leads = 0
		for lead in vendors:
			ap_leads += lead.submited_lead
		if camp.status == 1:
			chart_data.append({"campaign":camp.name,"approve":ap_leads})
			
	context = {
			'campaigns':campaigns,
			'chart_data':json.dumps(chart_data),
			'leads':leads,
			'vendors':[dict(t) for t in {tuple(d.items()) for d in camp_vendors}],
			'total_leads':len(total_leads),
			'approved_leads':approved_leads,
			'rejected_leads':rejected_leads
		}
	return render(request, 'reports/leads_report/lead_report.html',context)

# === Lead Report Data ===
def lead_report_data(request):
	# to return lead data on filters 
	# print(request.POST)
	camp_vendors, total_leads, filterd_leads, chart_data = [],[],[],[]
	camp_ids = request.POST.getlist('camp_id[]') if 'camp_id[]' in request.POST.keys() else Campaign.objects.filter(user_id=request.session['userid']).values_list('id',flat=True)
	status = [int(n) for n in request.POST.getlist('status[]')] if 'status[]' in request.POST.keys() else []
	filter_vendors = [int(n) for n in request.POST.getlist('vendor[]')] if 'vendor[]' in request.POST.keys() else []
	leads = campaign_allocation.objects.filter(campaign_id__in=camp_ids)

	for vendor in leads:
		if vendor.submited_lead > 0:	
			camp_vendors.append({'id': str(vendor.client_vendor.id),
                            'name': str(vendor.client_vendor.user_name)})
			if filter_vendors != []:
				total_leads += eval(vendor.upload_leads) if vendor.client_vendor.id in filter_vendors else []
			else:
				total_leads += eval(vendor.upload_leads)
	
	# date filter
	if request.POST.get('start_date') != '0' and request.POST.get('end_date') != '0' and request.POST.get('start_date') != '' and request.POST.get('end_date') != '':
		total_leads = apply_date_filter(total_leads,request.POST.get('start_date'),request.POST.get('end_date'))	

	# status filter
	if status != []:
		filterd_leads = [d for d in total_leads if d['status'] in status]
		approved_leads = 0 ; rejected_leads = 0
		if 1 in status:
			approved_leads = len([d for d in filterd_leads if d['status'] ==  1])
		if 2 in status:
			rejected_leads = len([d for d in filterd_leads if d['status'] == 2])
		
	else:
		approved_leads = len([d for d in total_leads if d['status'] == 1])
		rejected_leads = len([d for d in total_leads if d['status'] == 2])
	
	campaigns = Campaign.objects.filter(id__in=camp_ids)
	for camp in campaigns:
		camp_alloc = leads.filter(campaign_id=camp.id, status=1)
		ap_leads = 0
		if status != []:
			for lead in camp_alloc:
				if filter_vendors == []:
					if 1 in status:
						ap_leads += lead.approve_leads
					if 2 in status:
						ap_leads += lead.return_lead
				else:
					if lead.client_vendor.id in filter_vendors:
						if 1 in status:
							ap_leads += lead.approve_leads
						if 2 in status:
							ap_leads += lead.return_lead
			if camp.status == 1:
				chart_data.append({"campaign":camp.name,"approve":ap_leads})
		else:
			if camp.status == 1:
				for lead in camp_alloc:
					if filter_vendors == []:
						ap_leads += lead.submited_lead
					else:
						if lead.client_vendor.id in filter_vendors:
							ap_leads += lead.submited_lead
				chart_data.append({"campaign":camp.name,"approve":ap_leads})
	data  = {
		'chart_data':json.dumps(chart_data),
		'vendors':[dict(t) for t in {tuple(d.items()) for d in camp_vendors}],
		'total_leads':len(total_leads),
		'approved_leads':approved_leads,
		'rejected_leads':rejected_leads
		}
	# print(data)
	return JsonResponse(data)

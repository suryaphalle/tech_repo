from django.db import models

from campaign.models import Campaign
from client_vendor.models import client_vendor
import datetime
from setupdata.models import *

# Create your models here.
class leads(models.Model):
	client_vendor=models.ForeignKey(client_vendor,on_delete=models.CASCADE)
	campaign=models.ForeignKey(Campaign,on_delete=models.CASCADE)
	data_header=models.ForeignKey(data_header,on_delete=models.CASCADE)
	submited_date=models.DateField()
	count=models.IntegerField()
	status=models.CharField(max_length=200) #approved or dispute
	def __str__(self):
		return self.client_vendor.user


# favorite Lead
class favorite_leads_header(models.Model):
	user=models.ForeignKey(user,on_delete=models.CASCADE)
	campaign=models.ForeignKey(Campaign,on_delete=models.CASCADE)
	header=models.TextField( null='true')

	def __str__(self):
		return self.user.user_name

class leads_rejected_reson(models.Model):
	reason=models.CharField(max_length=200)
	user=models.ForeignKey(user,on_delete=models.CASCADE,null='true', blank='true')
	status=models.IntegerField(default=1)
	is_active=models.IntegerField(default=1)

	def __str__(self):
		return self.reason

#for lead rectify reason
class Leads_Rectify_Reason(models.Model):
	reason=models.CharField(max_length=200)
	user=models.ForeignKey(user,on_delete=models.CASCADE,null='true', blank='true')
	status=models.IntegerField(default=1)
	is_active=models.IntegerField(default=1)
	def __str__(self):
		return self.reason

#when lead upload that time rejected lead store here to track
class Lead_Uploaded_Error(models.Model):
	campaign=models.ForeignKey(Campaign,on_delete=models.CASCADE)
	user=models.ForeignKey(user,on_delete=models.CASCADE)
	created_time=models.TimeField(auto_now_add=True,null='true')
	created_date=models.CharField(max_length=300, null='true')

# for campaign life cycle only
	exact_time = models.DateTimeField(null=True, blank=True, auto_now=True)

	updated_date=models.DateField(auto_now_add=True, null='true')
	duplicate_with_our=models.TextField(null='true', blank='true')
	duplicate_with_vendor=models.TextField(null='true', blank='true')
	remove_lead_header=models.TextField(null='true', blank='true')
	remove_duplicate_lead_csv=models.TextField(null='true', blank='true')
	old_uploaded_lead=models.TextField(null='true', blank='true')
	uploaded_lead=models.TextField(null='true', blank='true')
	all_lead=models.TextField(null='true', blank='true')
	all_lead_count=models.IntegerField(default=0)
	uploaded_lead_count=models.IntegerField(default=0)
	duplicate_with_our_cnt=models.IntegerField(default=0)
	duplicate_with_vendor_cnt=models.IntegerField(default=0)
	remove_lead_header_cnt=models.IntegerField(default=0)
	remove_duplicate_lead_csv_cnt=models.IntegerField(default=0)
	status=models.IntegerField(default=1)
	lead_upload_status=models.IntegerField(default=1) #check lead upload or not 0 =upload 1=remaining to upload
	vendor_percentage = models.IntegerField(default=0)
	client_percentage = models.IntegerField(default=0)

	def __str__(self):
		return self.campaign.name + str(user.user_name)

	class Meta:
		get_latest_by = 'exact_time'

import ast
from datetime import datetime
from user.models import *

from django.conf import settings
from django.contrib import messages
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.template import loader
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from django.urls import resolve, reverse
from campaign.forms import CounterCPLForm
from campaign.models import *
from client.decorators import *
from client.utils import RegisterNotification, noti_via_mail
from login_register.views import *
from setupdata.models import *
from setupdata.models import countries1 as countries_list
from vendors.models import *
from vendors.views import *
from vendors.views.views import (create_custom_header, create_header,
                                 get_lead_header, getCplNLead,checkCampaignLive,checkCampaignAssigned)
from campaign.choices import *
from django.contrib.auth import login, authenticate, logout as auth_logout, update_session_auth_hash
from client.views.camp_status import *

# === Vendor Portal ===
def vendor_portal(request):
    # external vendor login
    username = request.POST.get("username")
    password = request.POST.get("password")
    user_obj = authenticate(request, email=username, password=password)
    if user_obj is not None:
        user_obj.is_login = 1
        user_obj.save()
        login(request, user_obj)
        if user_obj.usertype_id == 5 or user_obj.usertype_id == 2:
            request.session['username'] = username
            request.session['is_login'] = True
            request.session['usertype'] = user_obj.usertype_id
            request.session['userid'] = user_obj.id

            return HttpResponseRedirect('/vendor-portal/dashboard/')
        else:
            error = "your using wrong url!..."
            return render(request, 'vendor_portal/vendor-portal.html', {"error": error})

    else:
        error = "Username and password Missmatch!..."
        return render(request, 'vendor_portal/vendor-portal.html', {"error": error})

# === Enternal Vendor Dashboard ===
@login_required
@csrf_exempt
def cdashboard(request):
    # external vendor dashboard
    if request.session['is_login'] == True and (request.session['usertype'] in [5,2,9]):
        topcampaigns = campaign_allocation.objects.filter(
            client_vendor=request.session['userid']).order_by('-id')[:5]
        client_list = []
        for data in topcampaigns:
            client = user.objects.filter(id=data.campaign.user_id)
            country = countries_list.objects.filter(id=client[0].country)
            # country = countries.objects.filter(id=client[0].country)
            client_list.append({
                'name': client[0].user_name,
                'email': client[0].email,
                'contact': client[0].contact,
                'country': country[0].name if country else '',
            })
        # return render(request, 'dashboard/vendor_portal_dashboard.html', {'topcampaigns': topcampaigns, 'client_list': client_list})
        return render(request, 'dashboard/vendor_portal_dashboard.html', {'topcampaigns': topcampaigns, 'client_list': client_list})
    else:
        return redirect('/login/')

# === Logout  ===
def logout(request):
    # logout 
    for key in list(request.session.keys()):
        del request.session[key]
    auth_logout(request)
    return HttpResponseRedirect('/')

# === External Vendor Manage Camapaign ===
@login_required
def cv_manage_campaign(request):
    ''' external vendor campaign notebook '''
    counter = campaign_allocation.objects.filter(
        client_vendor_id=request.session['userid']).count()
    data = []
    CPL_form = CounterCPLForm(request.POST)
    if counter > 0:
        campaign_allocation_datails = campaign_allocation.objects.filter(
            client_vendor_id=request.session['userid'])
        for row in campaign_allocation_datails:
            campaign_details = Campaign.objects.get(id=row.campaign_id)
            delivery_obj = Delivery.objects.get(campaign_id=row.campaign_id)
            counter_obj = Counter_cpl.objects.filter(campaign_allocation=row.id, campaign=row.campaign_id)
            data1 = getCplNLead(row, campaign_details)
            if data1['camp'] == 1:
                if campaign_details.adhoc:
                    type = 'adhoc'
                else:
                    if campaign_details.rfq == 1:
                        type = 'RFQ'
                    else:
                        type = 'Normal'
                
                data.append(
                    {
                        'id': campaign_details.id,
                        'rfq_timer': campaign_details.rfq_timer,
                        'name': campaign_details.name,
                        'description': campaign_details.description,
                        'io_number': campaign_details.io_number,
                        'cpl': data1['cpl'],
                        'leads': data1['lead'],
                        'start_date': campaign_details.start_date,
                        'end_date': campaign_details.end_date,
                        'status': row.status,
                        'type': type,
                        'rfq': campaign_details.rfq,
                        'camp_alloc_id': row.id,
                        'counter_status':row.counter_status,
                        'counter_cpl':counter_obj[0].req_cpl if counter_obj.count() == 1 else 0,
                        'counter_vol':counter_obj[0].volume if counter_obj.count() == 1 else 0,
                        'comment':counter_obj[0].comment if counter_obj.count() == 1 else '',
                        'priority': campaign_details.priority,
                        'assign_more_lead_status':row.assign_more_lead_status,
                        'user':campaign_details.user,
                        'reasons':eval(row.reasons)['flag'] if row.reasons != None else 1,
                        'delivery_status': 1 if delivery_obj.tc_header_status == 1 or delivery_obj.custom_header_status == 1 else 0
                    }
                )
        '''# return render(request,'campaign/manage_campaign.html',{'camps':data})'''
        import operator
        data.sort(key=operator.itemgetter('priority'))
        return data

    else:
        return render(request, 'vendor_portal/manage_campaign.html', )

# === Return Live Campaign List ===
@login_required
@is_vendor
def cv_vendor_live_campagin(request):
    # return live campaign list
    data = cv_manage_campaign(request)
    CPL_form = CounterCPLForm(request.POST)
    return render(request, 'vendor_portal/cv_live_campaign.html', {'camps': data, 'CPL_form': CPL_form})

# ===  Return Paused Campaign List ===
@login_required
@is_vendor
def cv_vendor_paused_campagin(request):
    # return pause campaign list
    data = cv_manage_campaign(request)
    CPL_form = CounterCPLForm(request.POST)
    return render(request, 'vendor_portal/cv_paused_campaign.html', {'camps': data, 'CPL_form': CPL_form})

# === Return Completed Campaign List  ===
@login_required
@is_vendor
def cv_vendor_completed_campagin(request):
    # return completed campaign list
    data = cv_manage_campaign(request)
    CPL_form = CounterCPLForm(request.POST)
    return render(request, 'vendor_portal/cv_complete_campaign.html', {'camps': data, 'CPL_form': CPL_form})

# === Return Assigned Campaign List ===
@login_required
@is_vendor
def cv_vendor_assigned_campagin(request):
    # return assigned campaign list
    data = cv_manage_campaign(request)
    CPL_form = CounterCPLForm(request.POST)
    return render(request, 'vendor_portal/cv_assigned_campaign.html', {'camps': data, 'CPL_form': CPL_form})

# === Retun Pending Camapign List ===
@login_required
@is_vendor
def cv_pending_campaign(request):
    # return pending campaign list
    data = cv_manage_campaign(request)
    # suryakant 18-09-2019
    # displayed more allocated lead on external vendor side
    more = More_allocation.objects.filter(client_vendor_id=request.session['userid'])
    CPL_form = CounterCPLForm(request.POST)
    return render(request, 'vendor_portal/cv_pending_campaign.html', {'camps': data,'more':more, 'CPL_form': CPL_form})


# === Stroe Vendor Account Details ===
@login_required
@is_vendor
def cv_vendor_account(request):
    # store vendor account details
    userid = request.session['userid']
    marketing_method = source_touches.objects.all()
    company_size_data = company_size.objects.all()
    capacity_type = source_touches.objects.all()
    company_backgrounds = company_background.objects.all()
    pricing_flexibilitys = pricing_flexibility.objects.all()
    countrow = client_vendor.objects.filter(user_id=userid)
    if countrow:
        users = user.objects.filter(id=userid)
        clients = [item for item in client_vendor.objects.filter(
            user_id__in=users)][0]
        data_acqusitions_data = data_acqusitions.objects.filter(
            user_id_id=userid)
        built = opt_in = bought = bought_values = opt_in_values = built_values = 0
        built_display = opt_in_display = bought_display = 'none'
        for row in data_acqusitions_data:
            if row.type == 'built':
                built = 'checked'
                built_values = row.amt_millions
                built_display = 'block'
            elif row.type == 'opt_in':
                opt_in = 'checked'
                opt_in_values = row.amt_millions
                opt_in_display = 'block'
            elif row.type == 'bought':
                bought = 'checked'
                bought_values = row.amt_millions
                bought_display = 'block'

        data_acqusitions_list = {'built_display': built_display, 'opt_in_display': opt_in_display, 'bought_display': bought_display, 'built': built,
                                 'opt_in': opt_in, 'bought': bought, 'bought_values': bought_values, 'opt_in_values': opt_in_values, 'built_values': built_values}
        return render(request, 'dashboard/vendor_account.html', {'pricing_flexibilitys': pricing_flexibilitys, 'data_acqusitions_data': data_acqusitions_list, 'company_backgrounds': company_backgrounds, 'capacity_type': capacity_type, 'company_size': company_size_data, 'marketing_method': marketing_method, 'user_info': clients, 'user': users, 'client_vendor': countrow[0]})
    else:
        return render(request, 'dashboard/vendor_account.html', {'pricing_flexibilitys': pricing_flexibilitys, 'company_backgrounds': company_backgrounds, 'capacity_type': capacity_type, 'company_size': company_size_data, 'marketing_method': marketing_method})


# === Counter CPL on Campaign Allocation ===
def Counter_CPL_form(request):
    # counter cpl on campaign allocation
    campaign = campaign_allocation.objects.filter(id=request.POST.get('camp_all_id'))
    counter_obj = Counter_cpl.objects.filter(campaign_allocation=request.POST.get('camp_all_id'), campaign=campaign[0].campaign.id,user_id=request.session['userid'])
    if counter_obj.count() == 0:
        obj = Counter_cpl.objects.create()
        obj.volume = request.POST.get('volume')
        obj.req_cpl = request.POST.get('req_cpl') if request.POST.get('req_cpl') else None
        obj.user_id = user.objects.get(id=request.session['userid'])
        obj.campaign = campaign[0].campaign
        obj.campaign_allocation = campaign[0]
        obj.comment = request.POST.get('desc')
        obj.save()
        title = "Counter Request on "+str(campaign[0].campaign.name)
        desc = f'Counter request raised by {campaign[0].client_vendor.user_name} on {str(campaign[0].campaign.name)} campaign.'
        camp = campaign_allocation.objects.get(
            id=request.POST.get('camp_all_id'))
        camp.counter_status = 1
        camp.save()
        # suryakant 16-10-2019
        # raised mail and refactored code
        super = [i.id for i in user.objects.filter(usertype_id=4)]
        super.append(campaign[0].campaign.user.id)
        for j in super:
            RegisterNotification(campaign[0].client_vendor.id, j, desc, title, 2,campaign[0].campaign,camp)
        noti_via_mail(super, title, desc, mail_counter_action_by_vendor)
        data = {'success': 1, }
            
    else:
        data = {'success': 3}
    return JsonResponse(data)

# === Chat Screen Data === 
@login_required
def campaign_chat_screen(request, camp_id):
    # display chat screen
    camp = Campaign.objects.get(id=camp_id)
    camp_name = Campaign.objects.all()
    campaign_list = campaign_allocation.objects.filter(
        campaign_id__in=camp_name, client_vendor_id=request.session['userid'], status__in=[1])
    return render(request, 'campaign_chat/vendor_chat_screen.html', {'sender_id': request.session['userid'], 'campaign_list': campaign_list, 'campaign': camp})


# === Accept Campaign Request ===
@login_required
def accpet_campaign_request(request, camp_alloc_id):
    # accept campaign request
    from datetime import datetime
    sender = request.session['userid']
    camp_alloc = campaign_allocation.objects.get(id=camp_alloc_id)
    camp = Campaign.objects.get(id=camp_alloc.campaign_id)
    campaign_id=camp_alloc.campaign_id

    #if assigned more lead check campaign allready running then ,mearge leads
    camp_alloc.status = 5
    camp_alloc.save()

    # check if camp has filled all specs to send it to assigned..Akshay G.
    pending_camp_specs = pending_camp_status(camp)
    camp.approveleads = camp.approveleads+camp_alloc.volume

    if all(pending_camp_specs[camp.id].values()) and camp.status != 1:
        camp.status = 5
    camp.save()
    today = datetime.now().date()

    '''
    camp_alloc.status = 5
    camp_alloc.save()

    camp.approveleads = int(camp.approveleads) + int(camp_alloc.volume)
    target_quntity = camp.approveleads
    if target_quntity == camp.target_quantity:
        camp.status = 5

    today = datetime.now().date()
    if today == camp.start_date or today > camp.start_date:
        camp_alloc.status = 1
        camp_alloc.save()
        camp.status = 1
    camp.save()
    '''

    client = Campaign.objects.get(id=campaign_id)
    superadmin = user.objects.filter(usertype_id=4)

    title = "Campaign Request Accept"
    desc = request.session['username']

    sender1 = user.objects.get(id=sender)
    '''
    from client.utils import noti_via_mail
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    noti_via_mail(super.append(client.user_id), title, desc, mail_vendor_approval_campaign)
    '''
    for j in superadmin:
        RegisterNotification(sender, j.id, desc, title, 2,client,None)
    RegisterNotification(sender, client.user_id, desc, title, 2,client,None)
    if camp.status == 1 and camp_alloc.status == 1:
        return redirect('/vendor-portal/live-campaign/')
    else:
        return redirect('/vendor-portal/assigned-campaign/')


# === Submit RFQ CPL Volume ===
def rfqcpl(request):
    #external vendor give the Suggestions to client about rfq campaign
    # submit rfq cpl and volume
    vendor_id=request.session['userid']
    camp_alloc_id=request.POST.get('camp_alloc_id')
    t=campaign_allocation.objects.get(id=camp_alloc_id,campaign_id=request.POST.get('camp_id'),status=3,cpl=-1,client_vendor_id=vendor_id)
    if t.rfqcpl != 0 and t.rfqcpl:
        data={'success':2,'msg':'you have already submitted cpl and volume'}
    else:
        t.rfqcpl=request.POST.get('cpl')
        t.rfqvolume=request.POST.get('volume')
        t.cpl=0
        t.save()
        data={'success':1}
        title = "RFQ Suggestions On "+str(t.campaign.name)
        desc = "External Vendor "+str(t.client_vendor.user_name)+" Suggested : CPL as "+str(request.POST.get('cpl'))+" & Volume as "+str(request.POST.get('volume'))
        super = [i.id for i in user.objects.filter(usertype_id=4)]
        client_i = t.campaign.user.id
        super.append(client_i)
        noti_via_mail(super, title, desc, mail_vendor_rfq_quotaion)
        RegisterNotification(t.client_vendor.id, t.campaign.user.id, desc, title, 2,t.campaign,None)
    return JsonResponse(data)


# === Remove Campaign From Pending ===
@login_required
def remove_campaign_from_pending(request,camp_id,leads):
    #-----following code about remove campaign from pending session:kishor
    # remove campaign
    vendor_id=request.session['userid']
    campaign_details=campaign_allocation.objects.get(id=camp_id)
    data=Campaign.objects.get(id=campaign_details.campaign_id)
    data.raimainingleads=data.raimainingleads+leads
    client_id = data.user_id
    data.save()
    campaign_allocation.objects.get(id=camp_id).delete()
    vendor = user.objects.get(id=vendor_id)
    title = "Campaign Request Reject"
    desc = f"Campaign request has been rejected by {vendor.user_name} for {data.name}"
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    super.append(data.user.id)
    for j in super:
        RegisterNotification(vendor.id, j, desc, title, 2,data,None)
    # suryakant 17-10-2019 # raised email notification on reject allocations
    from client.utils import noti_via_mail
    noti_via_mail(super, title, desc, mail_vendor_approval_campaign)
    return redirect('/vendor-portal/pending-campaign/')


# === Campaign Notebook ===
@login_required
@is_vendor
def cv_campaign_notebook(request):
    # campaign notebook
    campaign_details = campaign_allocation.objects.filter(client_vendor=request.session['userid'])
    vendor = user.objects.filter(id=request.session['userid'])
    if campaign_details:
        return render(request,'vendor_portal/cv_campaign_notebook.html',{'camps':campaign_details,'user':vendor[0].user_name if vendor else None})
    else:
        return render(request,'vendor_portal/cv_campaign_notebook.html',{'user':vendor[0].user_name if vendor else None})


# === RFQ Campaign ===
def rfq_campaign(request):
    # rfq campaign list
    data = RFQ_campaign_quotes.objects.filter(vendor_id=request.session['userid'])
    return render(request, 'vendor_portal/cv_rfq_campaign.html', {'camps': data})


# === Vendor Lead List ===
@login_required
def vendor_leadlist(request, camp_id, status):
    # return vendor list

    list = []
    all_header = []
    all_lead_header = []
    labels=[]
    batchlist = []
    leadlist =[]
    count = []
    camp_alloc = campaign_allocation.objects.filter(id=camp_id)
    Custom_question_status = Mapping.objects.filter(campaign_id=camp_alloc[0].campaign_id)[0].custom_status
    if camp_alloc.count() == 1 and status == 1:
        camp_alloc = campaign_allocation.objects.get(id=camp_id)
        header = create_custom_header(
            camp_alloc.campaign_id, request.session['userid'])
        is_upload=check_lead_uploadable(int(camp_alloc.volume),int(camp_alloc.submited_lead),int(camp_alloc.return_lead))
        data = {'camp_id': camp_alloc.campaign_id,'client_name':camp_alloc.campaign.user.user_name, 'camp_alloc_id': camp_id, 'camp_name': camp_alloc.campaign.name, 'cpl': camp_alloc.cpl,
                'lead': camp_alloc.volume,'is_upload':is_upload, 'submited_lead': camp_alloc.submited_lead, 'return_lead': camp_alloc.return_lead}
        if camp_alloc.submited_lead > 0:
            list = ast.literal_eval(camp_alloc.upload_leads)
            batchlist = batch_list(list)
            if len(header) == 0:
                for dict in list:
                    count=0
                    if len(dict.keys()) > count :
                        count = len(dict.keys())
                        all_header,all_lead_header=[],[]
                        for key in dict:
                            all_header.append(key)
                            all_lead_header.append(key)
                all_header = create_header(all_header)
        labels=get_lead_header(camp_alloc.campaign_id)
        labels +=join_custom_question_header(camp_alloc.campaign_id)
        if batchlist != []:
            count = batchlist[1]
            batchlist = batchlist[0]
        if len(all_header) == 0:
            all_header=labels
        return render(request, 'campaign/ext_vendor_leadlist.html', {'Custom_question_status':Custom_question_status,'edit_lead_header':labels,'campaigns': data, 'batch':batchlist, 'leadlist': list, 'all_lead_header': all_lead_header, 'all_header': all_header, 'header': header, 'status': camp_alloc.status, 'camp_id': camp_id,'count':count})
    elif camp_alloc.count() == 1 and status == 4:
        camp_alloc = campaign_allocation.objects.get(id=camp_id)
        header = create_custom_header(
            camp_alloc.campaign_id, request.session['userid'])
        data = {'camp_id': camp_alloc.campaign_id, 'camp_alloc_id': camp_id, 'camp_name': camp_alloc.campaign.name, 'cpl': camp_alloc.old_cpl,
                'lead': camp_alloc.old_volume, 'submited_lead': camp_alloc.submited_lead, 'return_lead': camp_alloc.return_lead}
        if camp_alloc.upload_leads != None:
            list = ast.literal_eval(camp_alloc.upload_leads)

            if len(header) == 0:
                for dict in list:
                    count=0
                    if len(dict.keys()) > count :
                        count = len(dict.keys())
                        all_header,all_lead_header=[],[]
                        for key in dict:
                            all_header.append(key)
                            all_lead_header.append(key)
                all_header = create_header(all_header)

        labels=get_lead_header(camp_alloc.campaign_id)
        labels +=join_custom_question_header(camp_alloc.campaign_id)
        if len(all_header) == 0:
            all_header=labels
        return render(request, 'campaign/ext_vendor_leadlist.html', {'Custom_question_status':Custom_question_status,'edit_lead_header':labels,'campaigns': data, 'leadlist': list, 'all_lead_header': all_header, 'all_header': all_header, 'header': header, 'status': status, 'camp_id': camp_id})
    return render(request, 'campaign/ext_vendor_leadlist.html', {'Custom_question_status': Custom_question_status, 'camp_id': camp_id, 'status': status})


# === upload With Rejected Lead Web ===
def upload_with_rejected_lead_web(request,camp_id,camp_alloc_id):
    import datetime
    userid=request.session['userid']
    date=datetime.datetime.today().strftime('%Y-%m-%d')
    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).exists():
        lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
        leads=[]
        # store all leads in leads

        leads=ast.literal_eval(lead_data.all_lead)
        lead_data.lead_upload_status=0
        lead_data.status=0
        lead_data.save()
        last_id_lead=get_last_id_of_lead(camp_alloc_id)
        last_batch_id = get_last_batch_id(camp_alloc_id)
        if len(leads)>0:
            for lead in leads:
                last_id_lead +=1
                lead['id']=last_id_lead
                lead['batch'] = last_batch_id
            lead_data = upload_lead_database(
                leads, camp_alloc_id, userid, 1, last_batch_id)
    camp_detail=campaign_allocation.objects.get(id=camp_alloc_id)
     # return redirect('vendor_portal_leadlist', camp_id=camp_id,status=camp_detail.status)
    return HttpResponseRedirect('/vendor-portal/vendor_leadlist/'+str(camp_alloc_id)+'/'+str(camp_detail.status))


# === Upload Without Rejected Lead Web ===
def upload_without_rejected_lead_web(request,camp_id,camp_alloc_id):
    userid=request.session['userid']
    import datetime
    date=datetime.datetime.today().strftime('%Y-%m-%d')
    lead_data={'success':1}
    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).exists():
        lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
        real_data,leads=[],[]
        # store all leads in leads

        leads=ast.literal_eval(lead_data.all_lead)
        lead_data.lead_upload_status=0
        lead_data.status=0
        lead_data.save()

        for lead in leads:
            if lead['TC_lead_status'] == 'valid lead':
                real_data.append(lead)

        last_id_lead=get_last_id_of_lead(camp_alloc_id)
        last_batch_id = get_last_batch_id(camp_alloc_id)
        if len(real_data)>0:
            for lead in real_data:
                last_id_lead +=1
                lead['id']=last_id_lead
                lead['batch'] = last_batch_id
            lead_data = upload_lead_database(
                real_data, camp_alloc_id, userid, 1, last_batch_id)
    camp_detail=campaign_allocation.objects.get(id=camp_alloc_id)
    # return redirect('vendor_portal_leadlist', camp_id=camp_id,status=camp_detail.status)
    return HttpResponseRedirect('/vendor-portal/vendor_leadlist/'+str(camp_alloc_id)+'/'+str(camp_detail.status))


# === Get Camapign Data Vendor ===
def get_ex_camp_data_vendor(request):
    # Get Camppaign Data Vendor
    camp_details = campaign_allocation.objects.get(id=request.POST.get('camp_alloc_id'))
    # check if user has already uploaded script file for this campaign...Akshay G.
    script_query = Scripts.objects.filter(
        campaign_id= camp_details.campaign.id, user_id = request.session['userid'])
    script_name = ''
    if script_query:
        script_name = script_query[0].client_script.name
    # fetch vendor agreement data of this campaign..Akshay g. 
    queryset = ClientCustomFields.objects.filter(client_id = camp_details.campaign.user_id)
    vendor_agreements = []
    new_agreement_data = {}
    old_agreement_data = {}
    if camp_details.vendor_agreements:
        vendor_agreements = eval(camp_details.vendor_agreements)
    if queryset:
        if queryset[0].client_agreements:
            if queryset[0].client_agreements.strip() != '':
                data = ast.literal_eval(queryset[0].client_agreements)[0]
                new_agreement_data = data['new_vendor_agreements']
                old_agreement_data = data['old_vendor_agreements']
    client_agreements_data = []
    agreement_paths = {}
    if len(vendor_agreements) > 0:
        import os
        for d in vendor_agreements:
            for agr_name in d.keys():
                if d[agr_name] == '':
                    if agr_name in new_agreement_data:
                        client_agreements_data.append({agr_name: os.path.join(settings.BASE_URL, new_agreement_data[agr_name])})
                    elif agr_name in old_agreement_data:
                        client_agreements_data.append({agr_name: os.path.join(settings.BASE_URL, old_agreement_data[agr_name])})
                else:
                    agreement_paths[agr_name] = os.path.join(settings.BASE_URL, d[agr_name])
                    client_agreements_data.append({agr_name:''})
    data = {
        'agreement_paths': agreement_paths,
        'client_agreements_data': client_agreements_data,
        'script_name': script_name,
        'name': camp_details.campaign.name,
        'cpl': '$ '+format(camp_details.cpl, '.2f') if camp_details.cpl else '',
        'volume': camp_details.volume,
        'camp_io':camp_details.campaign.io_number,
        'start_date': camp_details.campaign.start_date,
        'end_date': camp_details.campaign.end_date,
        'expiry_date': camp_details.campaign.rfq_timer,
        'desc': camp_details.campaign.description,
        'camp_id': request.POST.get('id'),
        'sender_id': request.session['userid'],
        'client_id': camp_details.campaign.user_id,
    }
    return JsonResponse(data)

def ext_mail_noti(request):
    # suryakant 22-10-2019
    # Added email notifications for external and internal vendors
    mail = MailNotification.objects.all().order_by('-id')
    access = []
    user_instance = user.objects.get(id=request.session['userid'])
    type = usertype.objects.get(id=request.session['usertype'])
    for m in mail:
        if type in m.usertype.all():
            if user_instance in m.user.all():
                access.append({
                    'id': m.id,
                    'access': m.noti_field,
                    'value': 1,
                    'parent': m.parent,
                })
            else:
                access.append({
                    'id': m.id,
                    'access': m.noti_field,
                    'value': 0,
                    'parent': m.parent,
                })
    print(access)
    return render(request, 'vendor_portal/ext_notisettings.html', {'mail': access})
from campaign.models import *
from superadmin.models import *
from setupdata.models import *
from django.conf import settings
import ast, re
from django.http import HttpResponse, JsonResponse, Http404
from operator import itemgetter
from user.models import usertype
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

def collect_campaign_details(camp_id):
    """ combine campaign data to display on pop up """
    details = []
    map_data = Mapping.objects.get(campaign_id=camp_id)
    specifiaction_data = Specification.objects.get(campaign_id=camp_id)
    details.append({
        'industry_type': map_data.industry_type if map_data.industry_type else None,
        'job_title_function': map_data.job_title_function if map_data.job_title_function else None,
        'special_instructions': map_data.special_instructions if map_data.special_instructions else None,
        'abm': specifiaction_data.abm_status,
        'suppression': specifiaction_data.suppression_status,
    })
    return details


def cpl_increase(cpl):
    """ increase 25% cpl increase """
    cpl = cpl + (cpl*25/100)
    return cpl


def cpl_decrease(vcpl, ccpl):
    """ increase 25% cpl decrease """
    vcpl = vcpl - (ccpl*25/100)
    return vcpl


def set_user_access(user):
    """ grant default access to user according to usertype """
    roles = User_Configuration.objects.filter(
        is_superadmin=True, user_type=user.usertype.id)
    for role in roles:
        if user not in role.user.all():
            role.user.add(user)
            if User_Configuration.objects.filter(parent_id=role.id).count() > 0:
                child = User_Configuration.objects.filter(parent_id=role.id)
                for grand_child in child:
                    if user not in grand_child.user.all():
                        grand_child.user.add(user)
                    grand_child.save()
        role.save()
    return True


def grand_child_access_call(data, access, access_id, for_user):
    for access_id in data:
        access = User_Configuration.objects.get(id=access_id)
        if (access.parent == None):
            child = User_Configuration.objects.filter(parent_id=access_id)
            for role in child:
                if str(role.id) in data:
                    grand_child = User_Configuration.objects.filter(
                        parent_id=role.id)
                    for gc in grand_child:
                        if str(gc.id) in data:
                            if for_user not in gc.user.all():
                                gc.user.add(for_user)
                                gc.user_type.add(for_user.usertype)
                            else:
                                gc.user.remove(for_user)
                                # gc.user_type.remove(for_user.usertype)
                            gc.save()
                    if User_Configuration.objects.filter(parent_id=role.id, user__in=[for_user]).count() == 0:
                        role.user.remove(for_user)
                        # role.user_type.remove(for_user.usertype)
                        role.save()
                    else:
                        role.user.add(for_user)
                        role.user_type.add(for_user.usertype)
                        role.save()

    return True


def no_state_country_script(request):
    ''' script for countries with no states at all -Amey Raje'''
    from setupdata.models import countries1, states1
    countries = countries1.objects.filter(states1__isnull=True)
    for i in countries:
        s = states1.objects.create(name=i.name, country=i)
        s.save()
    pass

def no_city_state_script(request):
    ''' script for states with no cities at all -Suryakant'''
    from setupdata.models import countries1, states1, cities1
    countries = states1.objects.filter(cities1__isnull=True)
    for i in countries:
        s = cities1.objects.create(name=i.name, state=i)
        s.save()
    pass

def continent_changes(request):
    ''' script for changing continents of countries - Amey Raje'''
    # FIRST CREATE CONTINENT NAME US territories IF NOT EXISTS
    from setupdata.models import countries1, region1
    country = countries1.objects.filter(region_id=5).exclude(id__in=[238, 44])
    region = region1.objects.get(id=8)
    for i in country:
        i.region = region
        i.save()
    pass


def set_all_default_mailchoices(userid):
    '''To set all email choice by default - by suryakant'''
    noti = MailNotification.objects.filter(usertype__id=userid.usertype.id)
    for emc in noti:
        emc.user.add(userid)
        emc.save()
    data = {'success': 1}
    return JsonResponse(data)


def add_mail_noti_script(Request):
    """ to set the prev users mail choices """
    users = user.objects.filter(usertype__id__in=[1, 2])
    for u in users:
        print(u)
        set_all_default_mailchoices(u)
    pass


def user_activity(request):
    import os
    import datetime
    from django.urls import resolve
    from django.conf import settings

    today = datetime.date.today()
    yesterday = today - datetime.timedelta(days=1)
    raw = []
    duplicates = []

    if request.session['usertype'] == 1:
        users = user.objects.filter(id=request.session['userid']) | user.objects.get(id=request.session['userid']).child_user.all()
        users = [ d for d in  users.values_list('id',flat=True)]
        data = UserTracking.objects.filter(user__id__in=users, date__in=[str(today), str(yesterday)])
    else:
        data = UserTracking.objects.filter(user__usertype_id__in=[1,2,5,6], date__in=[str(today), str(yesterday)])   
    for i in data:
        client_obj = client_vendor.objects.filter(user=i.user)

        if client_obj:
            if client_obj[0].company_logo is None or client_obj[0].company_logo == '':
                logo_url = os.path.join(settings.BASE_URL, 'media', 'default.jpg')
            else:
                logo_url = client_obj[0].company_logo.url
        else:
            logo_url = os.path.join(settings.BASE_URL, 'media', 'default.jpg')

        j = eval(i.data)
        for k in j:
            try:
                if resolve(k['path']).url_name is not None:
                    if raw:
                        if raw[-1]['url'] != resolve(k['path']).url_name:
                            value = {
                                'user': i.user.user_name if i.user.user_name != "" else i.user.email,
                                'logo': logo_url,
                                'time': k['exact_time'],
                                'url': resolve(k['path']).url_name,
                            }
                            raw.append(value)
                    else:
                        value = {
                            'user': i.user.user_name if i.user.user_name != "" else i.user.email,
                            'logo': logo_url,
                            'time': k['exact_time'],
                            'url': resolve(k['path']).url_name,
                        }
                        raw.append(value)
            except Exception as e:
                pass
    sorted_list = sorted(raw, key=itemgetter('time'), reverse=True)
    return sorted_list

def set_superclient_access(request):
    ''' set client access to superclient - Suryakant'''
    client_access = User_Configuration.objects.filter(is_client=True)
    for access in client_access:
        access.user_type.add(usertype.objects.get(id=8))
        access.save()
    pass

def overdue_reminder(request):
    """ send overdue reminder to the client after crossing campaign start date """
    from datetime import datetime, timedelta
    yesterday = datetime.now().date() - timedelta(1)
    campaigns = Campaign.objects.filter(start_date=yesterday)
    for camp in campaigns:
        subject='Campaign overdue reminder'
        from_email = settings.EMAIL_HOST_USER
        to=[camp.user.email]
        html_message = render_to_string('email_templates/overdue_reminder.html', {'username':camp.user.user_name,'campaign':camp.name})
        plain_message = strip_tags(html_message)
        send_mail(subject,plain_message,from_email,to,fail_silently=True,html_message=html_message)
    pass

# fetch database size value with multiplier for vendor onboarding...Akshay G..Date 6th Nov, 2019
def vendor_database_size_multiplier(data_assesment1):
    d = {
        'us_size_value': None,
        'us_size_multiplier': None,
        'overall_size_value': None,
        'overall_size_multiplier': None,
        'opt_in_size_value': None,
        'opt_in_size_multiplier': None,
    }
    if data_assesment1.database_size_us is not None:
        if data_assesment1.database_size_us != '':
            if len(re.findall(r'\.', data_assesment1.database_size_us)) in [0, 1]:
                database_size_val = data_assesment1.database_size_us.replace('.','') if '.' in data_assesment1.database_size_us else data_assesment1.database_size_us 
                if database_size_val.isdigit():
                    if eval(data_assesment1.database_size_us) > 0:
                        # if value is less than 1 million, convert it to thousand
                        if eval(data_assesment1.database_size_us) < 1000000:
                            d['us_size_value'] = eval(data_assesment1.database_size_us)/1000
                            d['us_size_multiplier'] = 'thousand'
                        # if value is equal or greater than 1 million, convert it to million
                        else:
                            d['us_size_value'] = eval(data_assesment1.database_size_us)/1000000
                            d['us_size_multiplier'] = 'million'
                else:
                    data_assesment1.database_size_us = 0
            else:
                data_assesment1.database_size_us = 0
    if data_assesment1.database_overall_size is not None:
        if data_assesment1.database_overall_size != '':
            if len(re.findall(r'\.', data_assesment1.database_overall_size)) in [0, 1]:
                database_size_val = data_assesment1.database_overall_size.replace('.','') if '.' in data_assesment1.database_overall_size else data_assesment1.database_overall_size
                if database_size_val.isdigit():
                    if eval(data_assesment1.database_overall_size) > 0:
                        # if value is less than 1 million, convert it to thousand
                        if eval(data_assesment1.database_overall_size) < 1000000:
                            d['overall_size_value'] = eval(data_assesment1.database_overall_size)/1000
                            d['overall_size_multiplier'] = 'thousand'
                        # if value is equal or greater than 1 million, convert it to million
                        else:
                            d['overall_size_value'] = eval(data_assesment1.database_overall_size)/1000000
                            d['overall_size_multiplier'] = 'million'
                else:
                    data_assesment1.database_overall_size = 0
            else:
                data_assesment1.database_overall_size = 0
    if data_assesment1.database_opt_in is not None:
        if data_assesment1.database_opt_in != '':
            if len(re.findall(r'\.', data_assesment1.database_opt_in)) in [0, 1]:
                database_size_val = data_assesment1.database_opt_in.replace('.','') if '.' in data_assesment1.database_opt_in else data_assesment1.database_opt_in
                if database_size_val.isdigit():
                    if eval(data_assesment1.database_opt_in) > 0:
                        # if value is less than 1 million, convert it to thousand
                        if eval(data_assesment1.database_opt_in) < 1000000:
                            d['opt_in_size_value'] = eval(data_assesment1.database_opt_in)/1000
                            d['opt_in_size_multiplier'] = 'thousand'
                        # if value is equal or greater than 1 million, convert it to million
                        else:
                            d['opt_in_size_value'] = eval(data_assesment1.database_opt_in)/1000000
                            d['opt_in_size_multiplier'] = 'million'
                else:
                    data_assesment1.database_opt_in = 0
            else:
                data_assesment1.database_opt_in = 0
    data_assesment1.save()
    return d
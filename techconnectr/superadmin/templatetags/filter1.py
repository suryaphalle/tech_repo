from django import template
import datetime
import ast
from campaign.models import *

register = template.Library()

@register.filter
def change_type(dictionary,key):
    dict=ast.literal_eval(dictionary)
    return dict.get(str(key))
@register.filter
def get_key_from_dict(dictionary):
    dict=ast.literal_eval(dictionary)
    return dict.keys()

@register.filter()
def get_data_frm_object(data_assesment,id=0):
    for row in data_assesment:
        if row.user_id == id:
            dict=ast.literal_eval(row.lead_gen_capacity)
            return dict.keys()

@register.filter()
def rfqcpl_exist(camp_id,user_id):
    counter=campaign_allocation.objects.filter(campaign_id=camp_id,client_vendor_id=user_id,status=3,volume=-1).count()
    if counter==1:
        return 'checked'

@register.filter()
def vendor_names(camp_id,status):
    vendornames=[]
    vendoralloc=campaign_allocation.objects.filter(campaign_id=camp_id,status=status)
    for vendors in vendoralloc:
        vendornames.append({'name':vendors.client_vendor.user_name})
    return vendornames

@register.filter()
def get_vendor_names(camp_id,status):
    vendoralloc=campaign_allocation.objects.filter(campaign_id=camp_id,status=status)
    if vendoralloc.count() >= 1 :
        for vendors in vendoralloc:
            return vendors.client_vendor.user_name
    else:
        return 'None'

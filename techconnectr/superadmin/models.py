from django.db import models
from user.models import user
from campaign.models import *
# Create your models here.


class Group_Collection(models.Model):
    Group_Name = models.CharField(max_length=15)
    is_active = models.IntegerField(default=1)
    created_date = models.DateTimeField(auto_now_add=True)


class Group_Master(models.Model):
    Group_Id = models.ForeignKey(Group_Collection, on_delete=models.CASCADE)
    User_Id = models.ForeignKey(user, on_delete=models.CASCADE)


class Chat(models.Model):
    Group_Id = models.ForeignKey(Group_Collection, on_delete=models.CASCADE)
    data = models.TextField()


class Client_User_Group(models.Model):
    group_name = models.CharField(max_length=100)
    is_active = models.BooleanField(default=False)
    group_users = models.ManyToManyField(
        user, blank=True, related_name="group_users")
    group_owner = models.ForeignKey(
        user, on_delete=models.CASCADE, related_name="group_owner")
    created_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{self.group_name}'


class User_Configuration(models.Model):
    user = models.ManyToManyField(user, blank=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    # parent = models.IntegerField(default=0)
    parent = models.ForeignKey('self', null=True, blank=True, related_name='parent_name', on_delete=models.CASCADE)
    url = models.CharField(max_length=100, verbose_name="redirect_url", blank=True, null=True)
    user_type = models.ManyToManyField(usertype, related_name='role', blank=True)
    position = models.IntegerField(default=0)
    is_superadmin = models.BooleanField(default=True)
    is_client = models.BooleanField(default=True)
    group = models.ManyToManyField(Client_User_Group,related_name='group_access',blank=True)
    default_page_level_access = models.TextField(null=True,blank=True)
    page_level_access = models.TextField(null=True,blank=True)

    def __str__(self):
        return f'{self.name}'

class Campaign_access(models.Model):
    user = models.ForeignKey(user, blank=True,on_delete=models.CASCADE,related_name='campaign_users')
    campaign = models.ManyToManyField(Campaign,blank=True)
    rfq_campaign = models.ManyToManyField(RFQ_Campaigns,blank=True)

    def __str__(self):
        return f'{self.user.user_name}'
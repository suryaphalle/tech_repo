from django.contrib import admin
from django.apps import apps
from .models import *

# Register your models here.
#app= apps.get_app_config('superadmin')
#for model_name,model in app.models.items():
#	admin.site.register(model)

class UserConfiguration(admin.ModelAdmin):
    list_display = ('name','parent','position','url')
    list_filter =('user_type',)


admin.site.register(User_Configuration, UserConfiguration)

class ClientGroup(admin.ModelAdmin):
    list_display = ('group_name','group_owner')

admin.site.register(Client_User_Group,ClientGroup)

class CampaignAccess(admin.ModelAdmin):
    pass
admin.site.register(Campaign_access,CampaignAccess)
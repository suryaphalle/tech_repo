# Generated by Django 2.1.4 on 2019-09-27 12:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaign', '0037_merge_20190924_0519'),
        ('superadmin', '0002_campaign_access'),
    ]

    operations = [
        migrations.AddField(
            model_name='campaign_access',
            name='rfq_campaign',
            field=models.ManyToManyField(blank=True, to='campaign.RFQ_Campaigns'),
        ),
    ]

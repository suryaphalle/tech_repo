# Generated by Django 2.1.4 on 2019-11-12 09:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('superadmin', '0003_campaign_access_rfq_campaign'),
    ]

    operations = [
        migrations.AddField(
            model_name='user_configuration',
            name='page_level_access',
            field=models.TextField(blank=True, null=True),
        ),
    ]

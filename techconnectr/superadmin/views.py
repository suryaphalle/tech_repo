from django.shortcuts import get_object_or_404, render, redirect
from setupdata.models import countries,countries1 as countries_list, states,states1, industry_type, industry_speciality
from django.conf import settings
from client_vendor.models import *
from user.models import user, usertype
from django.http import HttpResponse, JsonResponse
from setupdata.models import *
from login_register.views import RegisterNotification
from client.utils import noti_via_mail
from superadmin.models import *
from campaign.models import *
from setupdata.serializer import *
from campaign.models import *
import datetime
from campaign.choices import *

# === Read Notification  ===
def readnotification(request):
    # send Notification
    data = request.POST.get('id')
    stat = Notification_Reciever.objects.get(Notification=data)
    stat.status = 1
    stat.save()
    print(data)
    data = {'success': 1}
    return JsonResponse(data)

# === Notification ===
def notification(userid):
    # Notification count
    ids = Notification_Reciever.objects.filter(
        receiver=userid, status='0').values_list('Notification', flat=True)
    notification_details = Notification.objects.filter(id__in=ids)
    countrow1 = notification_details.count()
    return {'notify': notification_details, 'notifycount': countrow1}

# === Dashboard ===
def dashboard(request):
    # Superadmin Dashboard
    if 'is_login' in request.session:
        vendor = user.objects.filter(usertype_id=2)
        client = user.objects.filter(usertype_id=1)
        topclient = topClientList(client)
        topvendor = topVendorList(vendor)
        topclientcharts = topClientCharts(client)
        campaignListCharts = CampaignCharts(topclientcharts)
        campaignVendorChar = CampaignVendorChar(campaignListCharts)
        clientcount = user.objects.filter(usertype_id=1).count()
        vendorcount = user.objects.filter(usertype_id=2).count()
        campcount = Campaign.objects.all().count()
        countries1 = countries_list.objects.all()
        # countries1 = countries.objects.all()
        industry = industry_type.objects.all()
        expert = industry_speciality.objects.all()
        userid = request.session['userid']
        notifydic = notification(userid)
        return render(request, 'dashboard/dashboard.html', {'campaignVendorChart': campaignVendorChar, 'campaignListCharts': campaignListCharts, 'clientcharts': topclientcharts, 'topvendor': topvendor, 'topclient': topclient, 'speciality': expert, 'industry': industry, 'campcount': campcount, 'vendorcount': vendorcount, 'expert': expert, 'industry': industry, 'countries': countries1, 'vendor': vendor, 'client': client, 'clientcount': clientcount, 'notifycount': notifydic["notifycount"], 'notify': notifydic["notify"]})
    else:
        return redirect('/login/')

# === Top Client List ===
def topClientList(client):
    # Top client list
    cam_details = Campaign.objects.filter(user_id__in=client).order_by('-cpl')
    client_ids = []
    topclient = []
    for row in cam_details:
        if len(client_ids) <= 5:
            if row.user_id not in client_ids:
                client_ids.append(row.user_id)
                revenue = row.cpl * row.target_quantity
                counter = client_vendor.objects.filter(
                    user_id=row.user_id).count()
                if counter == 1:
                    client = client_vendor.objects.get(user_id=row.user_id)
                    topclient.append({'user_name': row.user.user_name, 'cpl': row.cpl, 'lead': row.target_quantity,
                                      'revenue': revenue, 'logo': client.company_logo, 'is_login': row.user.is_login})
                else:
                    topclient.append({'user_name': row.user.user_name, 'cpl': row.cpl, 'lead': row.target_quantity,
                                      'revenue': revenue, 'logo': 'placeholder.jpg', 'is_login': row.user.is_login})
    return topclient

# === Top Vendor List ===
def topVendorList(vendor):
    # Top vendor list"""
    cam_details = campaign_allocation.objects.filter(
        client_vendor_id__in=vendor, status=4).order_by('-cpl')
    vendor_ids = []
    topvendor = []
    for row in cam_details:
        if len(vendor_ids) <= 5:
            if row.client_vendor_id not in vendor_ids:
                vendor_ids.append(row.client_vendor_id)
                revenue = row.old_cpl * row.old_volume
                counter = client_vendor.objects.filter(
                    user_id=row.client_vendor_id).count()
                if counter == 1:
                    client = client_vendor.objects.get(
                        user_id=row.client_vendor_id)
                    topvendor.append({'user_name': row.client_vendor.user_name, 'cpl': row.cpl, 'lead': row.volume,
                                      'revenue': revenue, 'logo': client.company_logo, 'is_login': row.client_vendor.is_login})
                else:
                    topvendor.append({'user_name': row.client_vendor.user_name, 'cpl': row.cpl, 'lead': row.volume,
                                      'revenue': revenue, 'logo': 'placeholder.jpg', 'is_login': row.client_vendor.is_login})
    return topvendor

# === Top Client Charts ===
def topClientCharts(client):
    # Top client charts
    client_ids = []
    topClientCharts = []
    date = datetime.datetime.now()
    currant_date = datetime.datetime(date.year, date.month, 31)
    prev_date = datetime.datetime(date.year, date.month-1, 31)
    cam_details = Campaign.objects.filter(
        user_id__in=client, updated_date__gt=prev_date, updated_date__lt=currant_date, status__in=[1, 3, 4])
    for row in cam_details:
        revenue = row.cpl * row.target_quantity
        if row.user_id not in client_ids:
            client_ids.append(row.user_id)
            topClientCharts.append({
                'name': row.user.user_name,
                'y': revenue,
                'drilldown': row.user.user_name,
                'user_id': row.user_id,
                'color': '#4286f4'})
    return topClientCharts

# === Campaign Charts ===
def CampaignCharts(topclientcharts):
    # Campaign charts
    CampaignData = []
    data = []
    status = ''
    for row in topclientcharts:
        cam_details = Campaign.objects.filter(user_id=row['user_id'])
        for cam in cam_details:
            if cam.status == 1:
                status = 'Live'
            elif cam.status == 3:
                status = 'Pending'
            elif cam.status == 4:
                status = 'Complete'
            data.append({
                'name': cam.name,
                'y': cam.cpl * cam.target_quantity,
                'drilldown': cam.id,
                'color': '#50396d'
            })
        CampaignData.append({
            'id': row['user_id'],
            'name': row['name'],
            'data': data,
        })
        data = []
    return CampaignData

# === Campaign Vendor Char ===
def CampaignVendorChar(campaignListCharts):
    # Campaign Vendor Chart
    vendorData = []
    data = []
    for row in campaignListCharts:
        for cam in row['data']:
            vendorlist = campaign_allocation.objects.filter(
                campaign_id=cam['drilldown'], status=4)
            for vendor in vendorlist:
                data.append({'name': vendor.client_vendor.user_name,
                             'y': vendor.old_cpl * vendor.old_volume, 'color': '#21c44c'})
            if len(data) > 0:
                vendorData.append({
                    'id': cam['drilldown'],
                    'data': data,
                })
                data = []
    return vendorData

# === Client Approve ===
def clientapprove(request, client_id):
    # Client Approve
    t1 = user.objects.get(id=client_id)
    t1.approve = 1
    t1.save()
    title = "Client approve"
    desc = "Client Approve by Superadmin"
    # noti_via_mail(client_id,title, desc, 1)
    RegisterNotification(int(request.session['userid']), client_id, desc, title, 1)
    return redirect('/superadmin/')

# === User On Bording ===
def User_On_Bording(request):
    # Use On Boarding
    vendor = user.objects.filter(usertype_id=2)
    client = user.objects.filter(usertype_id=1)
    return render(request, 'dashboard/onboarding.html', {'vendor': vendor, 'client': client})

# === Client On Bording ===
def client_onBording(request, client_id):
    # Client On Boarding
    industry = industry_type.objects.all()
    expert = industry_speciality.objects.all()
    countrow = client_vendor.objects.filter(user_id=client_id).count()
    if countrow == 1:
        client_vendor_detail = client_vendor.objects.get(user_id=client_id)
        userdetails = user.objects.get(id=client_id)
        country = countries_list.objects.get(id=userdetails.country)
        # country = countries.objects.get(id=userdetails.country)
        state = states1.objects.get(id=userdetails.state)
        # state = states.objects.get(id=userdetails.state)
        industry = client_vendor_detail.industry_type.all()
        speciality = client_vendor_detail.industry_speciality.all()
        return render(request, 'dashboard/client_onBoarding.html', {'speciality': speciality, 'industry': industry, 'state': state, 'country': country, 'userdetails': userdetails, 'client_vendor': client_vendor_detail, 'expert': expert, 'industry': industry})
    else:
        return render(request, 'dashboard/client_onBoarding.html', {})

# === Vendor On Boarding ===
def vendor_onBoarding(request, vendor_id):
    # Vendor On Boarding
    industry = industry_type.objects.all()
    expert = industry_speciality.objects.all()
    marketing_method = source_touches.objects.filter(is_active=1).all()
    company_size_data = company_size.objects.all()
    capacity_type = source_touches.objects.all()
    countrow = client_vendor.objects.filter(user_id=vendor_id).count()
    delivery_time1 = delivery_time.objects.all()
    delivery_method1 = delivery_method.objects.all()
    complex_program = level_intent.objects.filter(is_active=1).all()
    registrtions_process1 = registration_process.objects.filter(
        is_active=1).all()
    database_attribute1 = database_attribute.objects.filter(is_active=1).all()
    if countrow == 1:
        client_vendor_detail = client_vendor.objects.get(user_id=vendor_id)
        userdetails = user.objects.get(id=vendor_id)
        # country = countries.objects.get(id=userdetails.country)
        country = countries_list.objects.get(id=userdetails.country)
        state = states1.objects.get(id=userdetails.state)
        # state = states.objects.get(id=userdetails.state)
        industry = client_vendor_detail.industry_type.all()
        speciality = client_vendor_detail.industry_speciality.all()
        counter = data_assesment.objects.filter(user_id=vendor_id).count()
        if counter == 1:
            data_assesment1 = data_assesment.objects.get(user_id=vendor_id)
        else:
            data_assesment1 = {}
        return render(request, 'dashboard/vendor_onBoarding.html', {'data_assesment': data_assesment1, 'database_attribute': database_attribute1, 'registrtions_process': registrtions_process1, 'complex_program': complex_program, 'delivery_time': delivery_time1, 'delivery_method': delivery_method1, 'capacity_type': capacity_type,'countries': countries1, 'company_size': company_size_data, 'marketing_method': marketing_method, 'speciality': speciality, 'industry': industry, 'state': state, 'country': country, 'userdetails': userdetails, 'client_vendor': client_vendor_detail, 'expert': expert, 'industry': industry})
    else:
        return render(request, 'dashboard/vendor_onBoarding.html', {'database_attribute': database_attribute1, 'registrtions_process': registrtions_process1, 'complex_program': complex_program, 'delivery_time': delivery_time1, 'delivery_method': delivery_method1, 'capacity_type': capacity_type, 'company_size': company_size_data, 'marketing_method': marketing_method, 'expert': expert, 'countries': countries1, 'industry': industry})

# === Vendor Approve ===
def vendorapprove(request, vendor_id):
    # Vendor Approve By Superadmin
    print(vendor_id)
    t1 = user.objects.get(id=vendor_id)
    t1.approve = 1
    t1.save()
    title = "vendor approve"
    desc = "vendor approve by superadmin"
    # noti_via_mail(vendor_id, title, desc, 1)
    RegisterNotification(int(request.session['userid']), vendor_id, desc, title, 1)
    return redirect('/superadmin/')

# === Client List ===
def clientlist(request):
    # Client List
    users = user.objects.filter(usertype_id=1, status=2)
    clients = client_vendor.objects.filter(user_id__in=users)
    return render(request, 'client/clientlist.html', {'clients': clients})

# === Vendor List ===
def vendorlist(request):
    # Vendor List
    vendors = user.objects.filter(usertype_id=2, status=2)
    vendors1 = client_vendor.objects.filter(user_id__in=vendors)
    return render(request, 'vendor/super_vendorlist.html', {'vendors': vendors1})

# === Vendor Campaign List ===
def vendor_campaign_list(request, vendor_id):
    # Vendor Campaign List
    counter = campaign_allocation.objects.filter(
        client_vendor_id=vendor_id).count()
    data = []
    if counter > 0:
        campaign_allocation_datails = campaign_allocation.objects.filter(
            client_vendor_id=vendor_id)
        for row in campaign_allocation_datails:
            campaign_details = Campaign.objects.get(id=row.campaign_id)
            if campaign_details.adhoc:
                type = 'adhoc'
            else:
                if row.cpl == -1 or row.volume == -1:
                    type = 'RFQ'
                else:
                    type = 'Normal'

            data.append(
                {
                    'id': campaign_details.id,
                    'name': campaign_details.name,
                    'description': campaign_details.description,
                    'io_number': campaign_details.io_number,
                    'cpl': row.old_cpl if row.cpl == -1 and row.volume == -1 else row.cpl,
                    'leads': row.old_volume if row.cpl == -1 and row.volume == -1 else row.volume,
                    'start_date': campaign_details.start_date,
                    'end_date': campaign_details.end_date,
                    'status': row.status,
                    'type': type,
                }
            )
        site_url = request.META.get('HTTP_REFERER')
        print(site_url)
        return render(request, 'vendor/campaign_list.html', {'camps': data, 'site_url': site_url})
    else:
        return render(request, 'vendor/campaign_list.html', {'site_url': site_url})

# === Partner List ===
def partnerlist(request):
    # Partner Liat
    return render(request, 'partner/partnerlist.html', {})

# === Campaign List ===
def campaignlist(request):
    # Campaign list
    campaigns = Campaign.objects.all()
    users = user.objects.all()
    return render(request, 'campaign/campaignlist.html', {'campaigns': campaigns, 'users': users})

# === Tech Ticket ===
def tech(request):
    # Tech ticket
    return render(request, 'tech/ticket.html', {})

# === Client Reports ===
def clientreports(request):
    # Client Report
    return render(request, 'reports/clientreports.html', {})

# === Vendor Reports ===
def vendorreports(request):
    # Vendor Report
    return render(request, 'reports/vendorreports.html', {})

# === Campaign Reports ===
def campaignreports(request):
    # Campaign Report
    return render(request, 'reports/campaignreports.html', {})

# === Campaing Descrption ===
def campaingdescrption(request, campaign_id):
    # Campaign Description
    camp = Campaign.objects.get(id=campaign_id)
    speci = Specification.objects.get(campaign_id=campaign_id)
    mapping = Mapping.objects.get(campaign_id=campaign_id)
    terms = Terms.objects.get(campaign_id=campaign_id)
    delivery = Delivery.objects.get(campaign_id=campaign_id)

    return render(request, 'campaign/campdesc.html', {'delivery': delivery, 'terms': terms, 'camp': camp, 'speci': speci, 'mapping': mapping})

# === On-Bording ===
def onBording(request):
    # On Boarding
    userid = request.POST.get('id', None)
    data = {'success': 2}
    countrow = client_vendor.objects.filter(user_id=userid).count()
    if countrow == 1:
        client_vendor_detail = client_vendor.objects.get(user_id=userid)
        userdetails = user.objects.get(id=userid)
        country = countries_list.objects.get(id=userdetails.country)
        # country = countries.objects.get(id=userdetails.country)
        # state = states.objects.get(id=userdetails.state)
        state = states1.objects.get(id=userdetails.state)
        industry = client_vendor_detail.industry_type.all()
        speciality = client_vendor_detail.industry_speciality.all()
        data = {
            'comp_name': userdetails.user_name,
            'website': client_vendor_detail.website,
            'add1': userdetails.address_line1,
            'add2': userdetails.address_line2,
            'country_name': country.name,
            'country_id': country.id,
            'state_name': state.name,
            'state_id': state.id,
            'phone': userdetails.contact,
            'primary_name': client_vendor_detail.primary_name,
            'primary_designation': client_vendor_detail.primary_designation,
            'primary_email': client_vendor_detail.primary_email,
            'primary_contact': client_vendor_detail.primary_contact,
            'secondary_name': client_vendor_detail.secondary_name,
            'secondary_email': client_vendor_detail.secondary_email,
            'secondary_contact': client_vendor_detail.secondary_contact,
            'secondary_designation': client_vendor_detail.secondary_designation,
            'address': userdetails.address_line1 + userdetails.address_line2,
            'industry_id': industry.id,
            'industry_name': industry.type,
            'speciality_id': speciality.id,
            'speciality_name': speciality.name,
        }
        return JsonResponse(data)
    else:
        return JsonResponse(data)

# === Relation ===
def Relation(request):
    # Relation group list
    group_collection = Group_Collection.objects.all()
    users = user.objects.all()
    return render(request, 'relation/grouplist.html', {'group': group_collection, 'users': users})

# === Create Group ===
def create_grp(request):
    # Create group
    grp_name = request.POST.get('grp_name')
    client_ids = request.POST.get('client_ids')
    vendor_ids = request.POST.get('vendor_ids')

# === Get Vendor List ===
def getVendorList(request):
    # Get Vendor
    data = campaign_allocation.objects.filter(
        campaign_id=request.POST.get('id'))
    serializer = Campaign_allocation_serializers(data, many=True)
    return JsonResponse(serializer.data, safe=False)

# === getPricing===
def getPricing(request):
    # Get Pricing
    id = request.POST.get('id', None)
    campaign_details = Campaign.objects.get(id=id)
    data = {
        'cpl': campaign_details.cpl,
        'targat_quantity': 100,
    }
    return JsonResponse(data)

# === Vendor Allocation ===
def vendorAllocation(request, camp_id):
    # Vendor Allocation
    id = camp_id
    campaign_details = Campaign.objects.get(id=id)
    vendor_data = []
    ids = []
    data = {
        'cpl': campaign_details.cpl,
        'targat_quantity': campaign_details.target_quantity,
        'ramaining_quantity': campaign_details.raimainingleads,
    }
    vendor_sugg = campaign_allocation.objects.filter(campaign_id=id)
    for row in vendor_sugg:
        if row.status == 0 and row.client_vendor_id not in ids:
            ids.append(row.client_vendor_id)
            vendor_data.append({
                'checked': 'checked',
                'display': 'none',
                'vendor_name': row.client_vendor.user_name,
                'vendor_id': row.client_vendor_id,
                'camp_id': row.campaign_id,
            })
        elif row.status == 4 and row.cpl != -1 and row.volume != -1 and row.client_vendor_id not in ids:
            ids.append(row.client_vendor_id)
            vendor_data.append({
                'checked': '0',
                'display': 'none',
                'vendor_name': row.client_vendor.user_name,
                'vendor_id': row.client_vendor_id,
                'camp_id': row.campaign_id,
            })
        elif row.status == 3 and row.volume == -1 and row.client_vendor_id not in ids:
            ids.append(row.client_vendor_id)
            vendor_data.append({
                'checked': '0',
                'display': 'none',
                'vendor_name': row.client_vendor.user_name,
                'vendor_id': row.client_vendor_id,
                'camp_id': row.campaign_id,
            })
        elif row.status == 3 and row.volume > 0 and row.cpl > 0 and row.client_vendor_id not in ids:
            ids.append(row.client_vendor_id)
        elif row.status == 1 and row.volume > 0 and row.cpl > 0 and row.client_vendor_id not in ids:
            ids.append(row.client_vendor_id)

    vendor_sugg = match_campaign_vendor.objects.filter(
        campaign_id=camp_id).exclude(client_vendor_id__in=ids)
    for row in vendor_sugg:
        ids.append(row.client_vendor_id)
        vendor_data.append({
            'checked': '0',
            'display': 'none',
            'vendor_name': row.client_vendor.user_name,
            'vendor_id': row.client_vendor_id,
            'camp_id': row.campaign_id,
        })

    users = user.objects.filter(usertype_id=2).exclude(id__in=ids)
    vendor_sugg = client_vendor.objects.filter(user_id__in=users)
    for row in vendor_sugg:
        if row.user_id not in ids:
            vendor_data.append({
                'checked': '0',
                'display': 'none',
                'vendor_name': row.user.user_name,
                'vendor_id': row.user_id,
                'camp_id': camp_id,
            })
    return render(request, 'campaign/vendor_allocation.html', {'data': data, 'vendor_data': vendor_data})

# === RFQ Campaign ===
def RFQ_Campaign(request, camp_id):
    # RFQ Campaign
    campaign_details = Campaign.objects.get(id=camp_id)
    data = {
        'cpl': campaign_details.cpl,
        'targat_quantity': campaign_details.target_quantity,
        'ramaining_quantity': campaign_details.raimainingleads,
        'camp_id': camp_id,
    }
    users = user.objects.filter(usertype_id=2)
    vendor_sugg = client_vendor.objects.filter(user_id__in=users)
    data_assesment1 = data_assesment.objects.filter(user_id__in=users)
    marketing_method = source_touches.objects.filter(is_active=1)
    cpl_counter = campaign_allocation.objects.filter(
        campaign_id=camp_id, status=3, cpl=0, volume=-1).count()
    cpl_list = campaign_allocation.objects.filter(
        campaign_id=camp_id, status=3, cpl=0, volume=-1)
    return render(request, 'campaign/rfq_camp_allocation.html', {'cpl_counter': cpl_counter, 'cpl_list': cpl_list, 'data': data, 'vendor_data': vendor_sugg, 'data_assesment': data_assesment1, 'marketing_method': marketing_method})

# === Update RFQ CPL ===
def update_rfq_cpl(request):
    # Update RFQ CPL
    t = Campaign.objects.get(id=request.POST.get('camp_id'))
    t.cpl = request.POST.get('cpl')
    t.save()
    data = {'success': 1}
    return JsonResponse(data)

# === RFQ CPL Lead Allocation===
def rqfcplLeadAllocation(camp_id, vendor_id, cpl, leads):
    # RFQ Lead Allocation
    rfqcounter = campaign_allocation.objects.filter(
        client_vendor_id=vendor_id, campaign_id=camp_id, status=3, volume=-1).count()
    ids = []
    if rfqcounter == 1:
        t1 = campaign_allocation.objects.get(
            client_vendor_id=vendor_id, campaign_id=camp_id, status=3, volume=-1)
        t1.cpl = cpl
        t1.volume = leads
        t1.status = 3
        t1.save()
        ids.append(vendor_id)
    return ids

# === CPL Lead Allocation ===
def cplLeadAllocation(request):
    # CPL Lead Allocation
    vendor_id = request.POST.get('vendor_id')
    camp_id = request.POST.get('camp_id', None)
    userid = request.session['userid']
    ids = rqfcplLeadAllocation(camp_id, vendor_id, request.POST.get(
        'vendorcpl'), request.POST.get('vendorlead'))
    counter = campaign_allocation.objects.filter(
        client_vendor_id=vendor_id, campaign_id=camp_id, status=0).count()
    if counter == 1:
        t1 = campaign_allocation.objects.get(
            client_vendor_id=vendor_id, campaign_id=camp_id, status=0)
        t1.cpl = request.POST.get('vendorcpl')
        t1.volume = request.POST.get('vendorlead')
        t1.status = 3
        t1.save()
    else:
        print(vendor_id)
        print(ids)
        if vendor_id not in ids:
            campaign_allocation.objects.create(client_vendor_id=vendor_id, campaign_id=camp_id, cpl=request.POST.get(
                'vendorcpl'), volume=request.POST.get('vendorlead'), status=3)

    campaign_commission.objects.create(client_vendor_id=vendor_id, campaign_id=camp_id, cpl=request.POST.get(
        'admincpl'), lead=request.POST.get('vendorlead'), commision_mode=request.POST.get('com_mode'))
    t1 = Campaign.objects.get(id=camp_id)
    t1.raimainingleads = request.POST.get('remaininglead')
    t1.save()
    title = "New Campaign allocated by"
    desc = "Techconnectr"
    # from client.utils import noti_via_mail
    # noti = noti_via_mail(vendor_id, title, desc, mail_noti_new_campaign)
    RegisterNotification(userid, vendor_id, desc, title, 2)
    data = {'success': '1'}
    return JsonResponse(data)

# === RFQ Vendor Allocation ===
def rfq_vendor_allocation(request):
    # RFQ Vendor Allocation
    ids = request.POST.getlist('ids[]')
    camp_id = request.POST.get('camp_id')
    userid = request.session['userid']
    title = "New RFQ Campaign allocated by"
    desc = "Superadmin"
    from client.utils import noti_via_mail
    for id in ids:
        counter = campaign_allocation.objects.filter(
            campaign_id=camp_id, client_vendor_id=id, cpl=-1, volume=-1, status=3).count()
        if counter != 1:
            campaign_allocation.objects.create(
                campaign_id=camp_id, client_vendor_id=id, cpl=-1, volume=-1, status=3)
        # noti = noti_via_mail(id, title, desc, mail_noti_new_campaign)
        RegisterNotification(userid, id, desc, title, 2)
    data = {'success': 1}
    return JsonResponse(data)

# === Client Campaign List ===
def client_campaign_list(request, client_id):
    # Cient campaign List
    print(client_id)
    counter = Campaign.objects.filter(user_id=client_id).count()
    data = []
    if counter > 0:
        campaign_details = Campaign.objects.filter(user_id=client_id)
        return render(request, 'client/campaign_list.html', {'camps': campaign_details})
    else:
        return render(request, 'client/campaign_list.html', {})

# === Campaign Vendor ===
def campaign_vendor(request, camp_id):
    # Campaign Vendor  Decription """
    camp = Campaign.objects.get(id=camp_id)
    vendoralloc = campaign_allocation.objects.filter(campaign_id=camp_id)
    site_url = request.META.get('HTTP_REFERER')
    return render(request, 'client/campdescription.html', {'site_url': site_url, 'camp': camp, 'vendoralloc': vendoralloc})

# === Lead Rejected List ===
def lead_rejected_list(request):
    # Lead rejected list"""
    vendors = user.objects.filter(usertype_id=2, status=2)
    vendors1 = client_vendor.objects.filter(user_id__in=vendors)
    return render(request, 'lead/lead_rejected_list.html', {'vendors': vendors1})

# === Register Notification ===
def RegisterNotification(sender_id, receiver_id, desc, title, notification_type_id):
    # Registration Notification
    Notification.objects.create(title=title, description=desc,
                                notification_type_id=notification_type_id, sender_id=sender_id)
    Nlast_id = Notification.objects.latest('id')
    Notification_Reciever.objects.create(
        status='0', Notification_id=Nlast_id.id, receiver_id=receiver_id)

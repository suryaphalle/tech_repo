import datetime
import json
from user.models import user

from django.conf import settings
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import resolve
from django.views import View

from campaign.forms import *
from campaign.models import *
from client.decorators import *
from client_vendor.models import *
from setupdata.models import *
from setupdata.models import (countries, countries1 as countries_list, industry_speciality, industry_type,
                              states,states1)
from setupdata.serializer import *
from superadmin.models import *

from .views import RegisterNotification
from client.utils import *
from ..utils import collect_campaign_details, cpl_increase, cpl_decrease, grand_child_access_call
from campaign.choices import *

# === Vendor Allocation approve leads ===
@login_required
@is_superadmin
def vendorAllocation(request, camp_id):
    # Vendor Allocation approve leads
    # import pdb; pdb.set_trace()
    id = camp_id
    campaign_details = Campaign.objects.get(id=id)

    value1 = float(campaign_details.cpl)/4
    value = float(campaign_details.cpl) - value1

    vendor_data = []
    ids = []
    username = campaign_details.user.user_name
    data = {
        'username': username,
        'userid': campaign_details.user_id,
        'cpl': campaign_details.cpl,
        'targat_quantity': campaign_details.target_quantity,
        'ramaining_quantity': campaign_details.raimainingleads,
        'approveleads': campaign_details.approveleads,
    }
    print(data)
    vendor_sugg = campaign_allocation.objects.filter(campaign_id=id)
    for row in vendor_sugg:
        if user.objects.filter(id=row.client_vendor_id, status=3).count() > 0:
            if row.suggest_status == 1 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
                vendor_data.append({
                    'checked': 'checked',
                    'display': 'none',
                    'vendor_name': row.client_vendor.user_name,
                    'vendor_id': row.client_vendor_id,
                    'camp_id': row.campaign_id,
                })
            elif row.status == 4 and row.cpl != -1 and row.volume != -1 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
                vendor_data.append({
                    'checked': '0',
                    'display': 'none',
                    'vendor_name': row.client_vendor.user_name,
                    'vendor_id': row.client_vendor_id,
                    'camp_id': row.campaign_id,
                })
            elif row.status == 3 and row.volume == -1 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
                vendor_data.append({
                    'checked': '0',
                    'display': 'none',
                    'vendor_name': row.client_vendor.user_name,
                    'vendor_id': row.client_vendor_id,
                    'camp_id': row.campaign_id,
                })
            elif row.status == 5 and row.volume > 0 and row.cpl > 0 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
            elif row.status == 3 and row.volume > 0 and row.cpl > 0 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
            elif row.status == 1 and row.volume > 0 and row.cpl > 0 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)

    vendor_sugg = match_campaign_vendor.objects.filter(
        campaign_id=camp_id).exclude(client_vendor_id__in=ids)
    for row in vendor_sugg:
        ids.append(row.client_vendor_id)
        vendor_data.append({
            'checked': '0',
            'display': 'none',
            'vendor_name': row.client_vendor.user_name,
            'vendor_id': row.client_vendor_id,
            'camp_id': row.campaign_id,
        })

    users = user.objects.filter(
        usertype_id=2, status=3).exclude(id__in=ids)
    vendor_sugg = client_vendor.objects.filter(user_id__in=users)
    for row in vendor_sugg:
        if row.user_id not in ids:
            vendor_data.append({
                'checked': '0',
                'display': 'none',
                'vendor_name': row.user.user_name,
                'vendor_id': row.user_id,
                'camp_id': camp_id,
            })
    cpl_counter = campaign_allocation.objects.filter(
        campaign_id=camp_id, status=3, cpl=0, volume=-1).count()
    cpl_list = campaign_allocation.objects.filter(
        campaign_id=camp_id, status=3, cpl=0, volume=-1)

    return render(request, 'campaign/vendor_allocation.html', {'cpl_counter': cpl_counter, 'cpl_list': cpl_list, 'data': data, 'vendor_data': vendor_data, 'value': value})

# === View campaign details ===
@login_required
@is_superadmin
def view_campaign_details(request, campaign_id) :
    # View campaign details
    campaign = Campaign.objects.get(id=campaign_id)
    specification = Specification.objects.get(campaign=campaign)
    mapping = Mapping.objects.get(campaign=campaign)
    terms = Terms.objects.get(campaign=campaign)
    delivery = Delivery.objects.get(campaign=campaign)
    campaignform = CampaignForm()
    specificationform = SpecificationForm()
    mappingform = MappingForm()
    termsform = TermsForm()
    deliveryform = DeliveryForm()

    # data filler
    campaign_category = Campaign_category.objects.all()
    campaign_pacing = Campaign_pacing.objects.all()
    days = Days.objects.all()
    industry_types = industry_type.objects.all()
    job_levels = job_level.objects.all()
    job_functions = job_function.objects.all()
    # countries_list = countries.objects.all()
    countries_list = countries_list.objects.all()
    company_sizes = company_size.objects.all()
    source_touch = source_touches.objects.all()
    level_intents = level_intent.objects.all()
    assets_all = assets.objects.all()

    data_headers = data_header.objects.all()
    delivery_methods = delivery_method.objects.all()

    context = {
        'campaign': campaign,
        'campaignform': campaignform,
        'specification': specification,
        'specificationform': specificationform,
        "mappingform": mappingform,
        "mapping": mapping,
        "termsform": termsform,
        "terms": terms,
        "deliveryform": deliveryform,
        "delivery": delivery,
        # data filler
        # set up data
        "campaign_category": campaign_category,
        "campaign_pacing": campaign_pacing,
        "days": days,
        "industry_types": industry_types,
        "job_levels": job_levels,
        "job_functions": job_functions,
        "company_sizes": company_sizes,
        "countries": countries_list,
        "source_touches": source_touch,
        "level_intent": level_intents,
        "assets": assets_all,

        "data_headers": data_headers,
        "delivery_methods": delivery_methods,
    }
    return render(request, 'campaign/view_campaign_details.html', context)

# === get client rfq and cpl ===
def get_client_rfq_cpl(request):
    # get client rfq and cpl
    data = {}
    camp_id = request.POST.get('id')
    campaign = Campaign_rfq.objects.get(campaign_id=camp_id)
    data = {'cpl': campaign.cpl, 'volume': campaign.volume,
            'remark': campaign.remark, 'success': 1}
    return JsonResponse(data)

# === client rfq Sumbitted Succesfully ===
def action_of_client_rfq(request):
    # Action of client rfq Sumbitted Succesfully
    camp_id = request.POST.get('id')
    cpl = request.POST.get('cpl')
    volume = request.POST.get('volume')
    choose = request.POST.get('action_id')
    user_id = request.session['userid']
    if choose == '1':
        accept_rfq_cpl_by_superadmin(camp_id, volume, cpl, user_id)
    elif choose == '2':
        rejected_rfq_cpl_by_superadmin(camp_id, cpl, user_id)
    data = {'success': 1, 'msg': 'Action Submitted Successfully!...'}
    return JsonResponse(data)

# === Accept RFQ CPL Request by Superadmin ===
def accept_rfq_cpl_by_superadmin(camp_id, volume, cpl, user_id):
    # Accept RFQ CPL Request by Superadmin on client
    # status =0 means superadmin accepted cpl
    campaign = Campaign_rfq.objects.get(campaign_id=camp_id)
    campaign.status = 3
    campaign.save()

    campaign = Campaign.objects.get(id=camp_id)
    name = campaign.name
    client_id = campaign.user_id
    campaign.cpl = cpl
    campaign.tc_target_quantity = volume
    campaign.tc_raimainingleads = volume
    campaign.client_raimainingleads= int(campaign.client_raimainingleads) - int(volume)
    campaign.rfq_status = 1
    campaign.save()
    title = 'Accept RFQ CPL'
    desc = 'Accept RFQ CPL Request by Superadmin on '+str(name)
    # from client.utils import noti_via_mail
    # noti = noti_via_mail(client_id, title, desc, mail_noti_superadmin_action)
    RegisterNotification(user_id, client_id, desc, title, 1, campaign, None)
    return True

# === Reject RFQ CPL By Superadmin on client  ===
def rejected_rfq_cpl_by_superadmin(camp_id, cpl, user_id):
    # Reject RFQ CPL By Superadmin on client
    campaign = Campaign.objects.get(id=camp_id)
    name = campaign.name
    client_id = campaign.user_id
    title = 'Reject RFQ CPL'
    desc = 'Reject RFQ CPL  by Superadmin on '+str(name)
    # from client.utils import noti_via_mail
    # noti = noti_via_mail(client_id, title, desc, mail_noti_superadmin_action)
    RegisterNotification(user_id, client_id, desc, title, 1, campaign, None)
    return True

# === return cpl list ===
def get_cpl_list(request):
    # return cpl list to superadmin
    camp_id = request.GET.get('camp_id')
    vendor_list = []
    ids=[]
    get_details = collect_campaign_details(camp_id)
    rfq_status = list(Campaign_rfq.objects.filter(
        campaign_id=camp_id).values())

    '''
        following code for get vendor's which is got request of rfq.
        abhi 1-5-2019
    '''
    cpl_list = campaign_allocation.objects.filter(
        campaign_id=camp_id, status=3, cpl__in=[0, -1], volume=-1).order_by('-cpl')
    for vendor in cpl_list:
        if vendor.client_vendor.usertype_id == 2:
            vendor_list.append({
                'checked':'checked',
                'id': vendor.client_vendor_id,
                'v_cpl': vendor.rfqcpl if vendor.rfqcpl else -1,
                'c_cpl': cpl_increase(vendor.rfqcpl) if vendor.rfqcpl else -1,
                'volume': vendor.rfqvolume if vendor.rfqvolume else -1,
                'name': vendor.client_vendor.user_name,
            })
            ids.append(vendor.client_vendor_id)
    '''
        following code for get vendor's which is not  get request of rfq.
        abhi 1-5-2019
    '''
    users=user.objects.exclude(id__in=ids).filter(usertype_id=2,approve=1,status=3)
    for us in users:
        vendor_list.append({
                'checked':'unchecked',
                'id': us.id,
                'v_cpl': -1,
                'c_cpl': -1,
                'volume':-1,
                'name': us.user_name,
            })
    data = {'success': 1, 'cpl_list': vendor_list,
            'details': get_details, 'rfq_status': rfq_status}
    return JsonResponse(data)

# === Send RFQ Request ===
def send_rfq_request(request):
    # send request to venodr according to campaign outrich method RFQ Vendor Allocation
    userid = request.session['userid']
    id_list=request.POST.getlist('vendor_id[]')
    camp_id=request.POST.get('camp_id')
    title = "New RFQ Campaign Arrived"
    desc = ""
    for id in id_list:
        allocation = RFQ_campaign_quotes.objects.filter(campaign_id=camp_id, vendor_id=id)
        if allocation.count() != 1:
            RFQ_campaign_quotes.objects.create(campaign_id=camp_id, vendor_id=id)
        else:
            if allocation:
                allocation[0].cpl = -1
                allocation[0].volume = -1
                allocation[0].status = 3
                allocation[0].save()
        camp_alloc = RFQ_campaign_quotes.objects.latest('id') if allocation[0] != RFQ_campaign_quotes.objects.latest('id') else allocation[0]
        RegisterNotification(userid, id, desc, title, 2, None, None)
    from client.utils import noti_via_mail
    noti_via_mail(id_list, title, desc, mail_rfq_quotation_request)
    data = {'success': 1}
    return JsonResponse(data)

# === User Access ===
def user_access(request):
    # return user access modules to superadmin
    usertypes = usertype.objects.filter(is_superadmin=True).values('type')
    users = user.objects.all()
    return render(request, 'tech/user_access.html', {'type': usertypes, 'users': users})

# === Get User Access ===
def get_user_access(request):
    # return selected user access
    current_user = user.objects.get(id=request.POST.get('userid'))
    roles = User_Configuration.objects.filter(
        is_superadmin=True, user_type__in=request.POST.get('usertype_id')).order_by('position')
    access = []
    for role in roles:
        if current_user in role.user.all():
            access.append({
                'id': role.id,
                'name': role.name,
                'url': role.url,
                'parent': role.parent.id if role.parent else None,
                'checked': 1,
                'usertype': role.user_type.filter(id=request.POST.get('usertype_id'))[0].type,
            })
        else:
            access.append({
                'id': role.id,
                'name': role.name,
                'url': role.url,
                'parent': role.parent.id if role.parent else None,
                'checked': 0,
                'usertype': role.user_type.filter(id=request.POST.get('usertype_id'))[0].type,
            })
    data = {'success': 1, 'user_roles': access}
    return JsonResponse(data)

# === grant access to user ===
def grant_access(request):
    # grant access to user
    data = request.POST.getlist('access_id[]')
    # print(data)
    for_user = user.objects.get(id=request.POST.get('userid'))
    for access_id in data:
        access = User_Configuration.objects.get(id=access_id)
        if (access.parent == None):
            child = User_Configuration.objects.filter(parent_id=access_id)
            if request.POST.get('parent_value') == 'true':
                if for_user.usertype.id == 6:
                    for role in child:
                        if str(role.id) in data:
                            role.user.add(for_user)
                            role.user_type.add(for_user.usertype)
                            role.save()
                            if User_Configuration.objects.filter(parent_id=role.id).count() > 0:
                                grand_child = User_Configuration.objects.filter(parent_id=role.id)
                                for gc in grand_child:
                                    if str(gc.id) in data:
                                        if for_user not in gc.user.all():
                                            gc.user.add(for_user)
                                            gc.user_type.add(for_user.usertype)
                                        else:
                                            gc.user.remove(for_user)
                                            # gc.user_type.remove(for_user.usertype)
                                        gc.save()
                    access.user.add(for_user)
                    access.user_type.add(for_user.usertype)
                    access.save()
                else:
                    for role in child:
                        if str(role.id) in data:
                            role.user.add(for_user)
                            role.save()
                    access.user.add(for_user)
                    access.save()
            else:
                if for_user.usertype.id == 6:
                    for role in child:
                        if str(role.id) in data:
                            role.user.remove(for_user)
                            #role.user_type.remove(for_user.usertype)
                            role.save()
                            if User_Configuration.objects.filter(parent_id=role.id).count() > 0:
                                grand_child = User_Configuration.objects.filter(parent_id=role.id)
                                for gc in grand_child:
                                    if str(gc.id) in data:
                                        # if for_user not in gc.user.all():
                                        #     gc.user.add(for_user)
                                        #     gc.user_type.add(for_user.usertype)
                                        # else:
                                        gc.user.remove(for_user)
                                            #gc.user_type.remove(for_user.usertype)
                                        gc.save()
                    access.user.remove(for_user)
                    # access.user_type.remove(for_user.usertype)
                    access.save()
                else:
                    for role in child:
                        if str(role.id) in data:
                            role.user.remove(for_user)
                            role.save()
                    access.user.remove(for_user)
                    access.save()
    data = {'success': 1, 'msg': f'Access Permission Changed For {for_user.user_name}'}
    return JsonResponse(data)

# === child grant access ===
def child_grant_access(request):
    # child grant access to user
    data = request.POST.getlist('access_id[]')
    # print(data)
    for_user = user.objects.get(id=request.POST.get('userid'))
    for access_id in data:
        access = User_Configuration.objects.get(id=access_id)
        if (access.parent == None):
            child = User_Configuration.objects.filter(parent_id=access_id)
            if for_user.usertype.id == 6:
                for role in child:
                    if str(role.id) in data:
                        if for_user not in role.user.all():
                            role.user.add(for_user)
                            role.user_type.add(for_user.usertype)
                            role.save()
                            if User_Configuration.objects.filter(parent_id=role.id).count() > 0:
                                grand_child = User_Configuration.objects.filter(parent_id=role.id)
                                for gc in grand_child:
                                    if str(gc.id) in data:
                                        if for_user not in gc.user.all():
                                            gc.user.add(for_user)
                                            gc.user_type.add(for_user.usertype)
                        else:
                            role.user.remove(for_user)
                            # role.user_type.remove(for_user.usertype)
                            role.save()
                            if User_Configuration.objects.filter(parent_id=role.id).count() > 0:
                                grand_child = User_Configuration.objects.filter(parent_id=role.id)
                                for gc in grand_child:
                                    if str(gc.id) in data:
                                        gc.user.remove(for_user)
                                        gc.save()
                if User_Configuration.objects.filter(parent_id=access_id, user__in=[for_user]).count() == 0:
                    access.user.remove(for_user)
                    # access.user_type.remove(for_user.usertype)
                    access.save()
                    print(access,'ext_user-remove','(access.user_type.remove(for_user.usertype))')
                else:
                    access.user.add(for_user)
                    access.user_type.add(for_user.usertype)
                    access.save()
                    print(access,'ext_user-add','(access.user_type.add(for_user.usertype))')
            else:
                for role in child:
                    if str(role.id) in data:
                        if for_user not in role.user.all():
                            role.user.add(for_user)
                            # role.user_type.add(for_user.usertype)
                        else:
                            role.user.remove(for_user)
                            # role.user_type.remove(for_user.usertype)
                        role.save()
                if User_Configuration.objects.filter(parent_id=access_id, user__in=[for_user]).count() == 0:
                    access.user.remove(for_user)
                    access.save()
                else:
                    access.user.add(for_user)
                    access.save()
    data = {'success': 1, 'msg': f'Access Permission Changed For {for_user.user_name}'}
    return JsonResponse(data)

# === child grant access to user ===
def grand_child_grant_access(request):
    # child grant access to user
    data = request.POST.getlist('access_id[]')
    for_user = user.objects.get(id=request.POST.get('userid'))
    for access_id in data:
        access = User_Configuration.objects.get(id=access_id)
        if (access.parent == None):
            # import pdb;pdb.set_trace()
            child = User_Configuration.objects.filter(parent_id=access_id)
            if for_user.usertype.id == 6:
                for role in child:
                    if str(role.id) in data:
                        if for_user not in role.user.all():
                            role.user.add(for_user)
                            role.user_type.add(for_user.usertype)
                            role.save()
                            if User_Configuration.objects.filter(parent_id=role.id).count() > 0:
                                grand_child_access_call(data, access, access_id, for_user)
                        else:
                            role.user.remove(for_user)
                            # role.user_type.remove(for_user.usertype)
                            role.save()
                            if User_Configuration.objects.filter(parent_id=role.id).count() > 0:
                                grand_child_access_call(data, access, access_id, for_user)

            if User_Configuration.objects.filter(parent_id=access_id, user__in=[for_user]).count() == 0:
                access.user.remove(for_user)
                # access.user_type.remove(for_user.usertype)
                access.save()
            else:
                access.user.add(for_user)
                access.user_type.add(for_user.usertype)
                access.save()
    data = {'success': 1, 'msg': f'Access Permission Changed For {for_user.user_name}'}
    return JsonResponse(data)

# === Campaign Revenue ===
def campaign_revenue(request,camp_id):
    # import pdb;pdb.set_trace()
    campaign_details = Campaign_commission.objects.filter(campaign_id=camp_id)
    camp = Campaign.objects.get(id=camp_id)
    return render(request,'campaign/campaign_revenue.html',{'campaign':campaign_details,'camp1':camp})

# === Submit User Access ===
def submit_user_access(request):
    '''View for submitting user access - by Amey Raje'''
    list_of_access = eval(request.POST.get('array'))
    usr = user.objects.get(id=request.POST.get('user_id'), usertype__type=request.POST.get('usertype'))
    if list_of_access:
        for i in list_of_access:
            config = User_Configuration.objects.get(id=i['id'])
            if i['check'] == 1:
                config.user.add(usr)
                config.save()
            elif i['check'] == 0:
                config.user.remove(usr)
                config.save()
    data = {'success': 1}
    return JsonResponse(data)

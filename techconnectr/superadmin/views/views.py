import ast
import time
import os

from datetime import date, datetime, time, timedelta
from io import BytesIO
from operator import itemgetter

from django.conf import settings
from django.contrib import messages
from django.core.files.storage import FileSystemStorage
from django.core.mail import send_mail
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render, render_to_response
from django.template import loader
from django.template.loader import get_template, render_to_string
from django.utils.html import strip_tags

import xhtml2pdf.pisa as pisa
from campaign.choices import *
from campaign.models import *
from client.decorators import *
from client.utils import *
from client_vendor.models import *
from leads.models import *
from setupdata.models import *
from setupdata.models import MailNotification, countries
from setupdata.models import countries1 as countries_list
from setupdata.models import (industry_speciality, industry_type, states,
                              states1)
from setupdata.serializer import *
from superadmin.models import *
from support.models import *
from user.models import *
from vendors.models import *

from ..utils import cpl_decrease, cpl_increase, set_user_access, set_all_default_mailchoices,user_activity

# === Read Notification count ===
def readnotification(request):
    # Read Notification count
    data = request.POST.get('id')
    if data == None:
        notifydic = notification(request.session['userid'])
        data = {'notifycount': notifydic["notifycount"]
            }
        return JsonResponse(data)
    flag = request.POST.get('flag')
    stat = Notification_Reciever.objects.get(Notification=data)
    # flag = 2...status=2..notifs removed
    if flag == '2':
        stat.status = 2
    # flag = 1..status=1...notifs just being read
    else:
        stat.status = 1
    stat.save()
    notifydic = notification(request.session['userid'])
    data = {'notifycount': notifydic["notifycount"]
            }

    return JsonResponse(data)

# === Notification ===
def notification(userid):
    # Notification Count
    # notifs details of unread & read notifs..with status=0 & 1 resp.
    ids = Notification_Reciever.objects.filter(
        receiver=userid, status__in=['0','1']).values_list('Notification', flat=True)
    notification_details = Notification.objects.filter(
        id__in=ids).order_by('-created')
    # 'ids_' get ids of unread notifs..i.e. having status=0
    ids_ = Notification_Reciever.objects.filter(
        receiver=userid, status = '0').values_list('Notification', flat=True)
    notification_details_ = Notification.objects.filter(
        id__in=ids_).order_by('-created')
    # get count of unread notifs..i.e. having status=0
    countrow1 = notification_details_.count()
    return {'notify': notification_details, 'notifycount': countrow1}


# === Dashboard ===
@is_superadmin
def dashboard(request):
    # campaign vendor chart on Dashboard

    vendor = user.objects.filter(usertype_id=2)
    client = user.objects.filter(usertype_id=1)
    topclient = topClientList(client)
    topvendor = topVendorList(vendor)
    topclientcharts = topClientCharts(client)
    campaignListCharts = CampaignCharts(topclientcharts)
    campaignVendorChar = CampaignVendorChar(campaignListCharts)
    clients = user.objects.filter(usertype_id=1, status=3)
    clientcount = client_vendor.objects.filter(user_id__in=clients).count()
    vendors = user.objects.filter(usertype_id=2, status=3)
    vendorcount = client_vendor.objects.filter(user_id__in=vendors).count()
    campcount = Campaign.objects.all().exclude(status = 0).count()
    # countries1 = countries.objects.all()
    countries1 = countries_list.objects.all()
    industry = industry_type.objects.all()
    expert = industry_speciality.objects.all()
    userid = request.session['userid']
    notifydic = notification(userid)
    return render(request, 'dashboard/dashboard.html', {'campaignVendorChart': campaignVendorChar, 'campaignListCharts': campaignListCharts, 'clientcharts': topclientcharts, 'topvendor': topvendor, 'topclient': topclient, 'speciality': expert, 'industry': industry, 'campcount': campcount, 'vendorcount': vendorcount, 'expert': expert, 'industry': industry, 'countries': countries1, 'vendor': vendor, 'client': client, 'clientcount': clientcount, 'notifycount': notifydic["notifycount"], 'notify': notifydic["notify"]})

# === Top client list ===
def topClientList(client):
    # Top client list

    revenue = 0
    camp_count = 0
    topclient = []
    for user in client:
        if client_vendor.objects.filter(user_id=user.id).count() == 1:
            client_details = client_vendor.objects.get(user_id=user.id)
            if Campaign.objects.filter(user_id=user.id).count() > 0:
                cam_details = Campaign.objects.filter(
                    user_id=user.id, status__in=(1, 4)).order_by('-cpl')
                for row in cam_details:
                    if row.cpl and row.target_quantity:
                        camp_count += 1
                        revenue += int(row.cpl) * int(row.target_quantity)

                        for i in topclient:
                            if i['user_name'] == user.user_name:
                                topclient.remove(i)
                        topclient.append({'user_name': user.user_name, 'camp_count': camp_count, 'revenue': revenue, 'logo': client_details.company_logo, 'is_login': user.is_login})

                revenue = 0
                camp_count = 0
    return sorted(topclient, key=itemgetter('revenue'), reverse=True)[:5]

# === Top Vendor List ===
def topVendorList(vendor):
    # Top Vendor List
    revenue = 0
    camp_count = 0
    topvendor = []
    for user in vendor:
        if client_vendor.objects.filter(user_id=user.id).count() == 1:
            vendor_details = client_vendor.objects.get(user_id=user.id)
            if campaign_allocation.objects.filter(client_vendor_id=user.id, status__in=[1, 4]).count() > 0:
                campaign_details = campaign_allocation.objects.filter(
                    client_vendor_id=user.id, status__in=[1, 4]).order_by('-cpl')
                for row in campaign_details:
                    if row.cpl != -1 and row.volume != -1 and row.old_volume != -1:
                        camp_count += 1
                        revenue += (row.cpl * row.volume)
                from client.utils import fetch_default_logo
                logo = fetch_default_logo(user.id)
                try:
                    if vendor_details.company_logo and  vendor_details.company_logo.storage.exists(vendor_details.company_logo.name):
                        logo = vendor_details.company_logo 
                except Exception as e:
                    pass
                topvendor.append({'id': user.id, 'user_name': user.user_name, 'camp_count': camp_count, 'revenue': revenue,
                                  'logo': logo, 'is_login': user.is_login})
                revenue = 0
                camp_count = 0

    return sorted(topvendor, key=itemgetter('revenue'), reverse=True)[:5]

# === Client Approve by superadmin ===
@login_required
@is_superadmin
def clientapprove(request, client_id):
    # Client Approve by superadmin
    t1 = user.objects.get(id=client_id)
    t1.approve = 1
    t1.save()
    title = "Client approved"
    desc = "Client Approved by Superadmin"
    # noti_via_mail(client_id, title, desc, 1)
    RegisterNotification(int(request.session['userid']), client_id, desc, title, 1, None, None)
    return redirect('/superadmin/')

# === Vendor Approve ===
def vendorapprove(request, vendor_id):
    # vendor approve by upersadmin

    t1 = user.objects.get(id=vendor_id)
    t1.approve = 1
    t1.save()
    title = "vendor approved"
    desc = "vendor approved by superadmin"
    # noti_via_mail(vendor_id, title, desc, 1)
    RegisterNotification(int(request.session['userid']), vendor_id, desc, title, 1, None, None)
    return redirect('/superadmin/')

# === Last day of Current Month ===
def last_day_of_cur_month(date):
    # Last day of current month
    if date.month == 12:
        return date.replace(day=31)
    return date.replace(month=date.month+1, day=1) - timedelta(days=1)

# === Last Day of previous Month ===
def last_day_of_prev_month(date):
    # LAst day of previous month
    if date.month-1 == 12:
        return date.replace(day=31)
    return date.replace(month=(date.month-1)+1, day=1) - timedelta(days=1)

# === Top Client Charts ===
def topClientCharts(client):
    # Top Client Chart
    # import pdb; pdb.set_trace()
    client_ids = []
    topClientCharts = []
    date = datetime.today()
    revenue = 0
    currant_date = last_day_of_cur_month(date)
    prev_date = last_day_of_prev_month(date)
    cam_details = Campaign.objects.filter(user_id__in=client, updated_date__gt=prev_date, updated_date__lt=currant_date, status__in=(1, 4))
    for row in cam_details:
        if row.cpl and row.target_quantity:
            revenue = int(row.cpl) * int(row.target_quantity)
            if row.user_id not in client_ids:
                client_ids.append(row.user_id)
                topClientCharts.append({
                    'name': row.user.user_name,
                    'y': revenue,
                    'drilldown': row.user.user_name,
                    'user_id': row.user_id,
                    'color': '#4286f4'})
# added else part for if same client has two or more campaign
            else:
                for i in topClientCharts:
                    if i['user_id'] == row.user_id:
                        revenue = revenue + int(i['y'])
                        topClientCharts.remove(i)
                topClientCharts.append({
                    'name': row.user.user_name,
                    'y': revenue,
                    'drilldown': row.user.user_name,
                    'user_id': row.user_id,
                    'color': '#4286f4'})
    return topClientCharts

# === Campaign Charts ===
def CampaignCharts(topclientcharts):
    # Campaign Charts
    CampaignData = []
    data = []
    status = ''
    for row in topclientcharts:
        cam_details = Campaign.objects.filter(user_id=row['user_id'], status__in=(1,4))
        for cam in cam_details:
            if cam.status == 1:
                status = 'Live'
            elif cam.status == 3:
                status = 'Pending'
            elif cam.status == 4:
                status = 'Complete'
            data.append({
                'name': cam.name,
                'y': (int(cam.cpl) if cam.cpl else 0) * (int(cam.target_quantity) if cam.target_quantity else 0),
                'drilldown': cam.id,
                'color': '#50396d'
            })
        CampaignData.append({
            'id': row['user_id'],
            'name': row['name'],
            'data': data,
        })
        data = []
    return CampaignData

# === Campaign Vendor Char ===
def CampaignVendorChar(campaignListCharts):
    # Campaign vendor charts
    vendorData = []
    data = []
    for row in campaignListCharts:
        for cam in row['data']:
            vendorlist = campaign_allocation.objects.filter(campaign_id=cam['drilldown'], status__in=(1, 4))
            for vendor in vendorlist:
                if vendor.status == 4:
                    if vendor.cpl and vendor.old_volume:
                        data.append({'name': vendor.client_vendor.user_name,
                                     'y': (vendor.cpl * vendor.old_volume), 'color': '#2ac16c'})
                elif vendor.status == 1:
                    if vendor.cpl and vendor.volume:
                        data.append({'name': vendor.client_vendor.user_name,
                                     'y': (vendor.cpl * vendor.volume), 'color': '#2ac16c'})
            if len(data) > 0:
                vendorData.append({
                    'id': cam['drilldown'],
                    'data': data,
                })
                data = []
    return vendorData

# === User On Bording ===
@login_required
@is_superadmin
def User_On_Bording(request):
    # use On Boarding on dashboard
    vendor = user.objects.filter(usertype_id=2)
    client = user.objects.filter(usertype_id=1)
    return render(request, 'dashboard/onboarding.html', {'vendor': vendor, 'client': client})

# === Client On-Bording ===
@login_required
@is_superadmin
def client_onBording(request, client_id):
    # Client on boarding
    industry = industry_type.objects.all()
    expert = industry_speciality.objects.all()
    countrow = client_vendor.objects.filter(user_id=client_id).count()
    if countrow == 1:
        client_vendor_detail = client_vendor.objects.get(user_id=client_id)
        logo = client_vendor_detail.company_logo if client_vendor_detail.company_logo else None
        # check if logo file exists in media
        import os
        if logo:
            if logo in os.listdir(settings.MEDIA_ROOT):
                company_logo = logo
            else:
                company_logo = None
        else:
            company_logo = None
        userdetails = user.objects.get(id=client_id)
        country = countries_list.objects.get(id=userdetails.country)
        # country = countries.objects.get(id=userdetails.country)
        # state = states.objects.get(id=userdetails.state)
        state = states1.objects.get(id=userdetails.state)
        if userdetails.city:
            city=cities1.objects.get(id=userdetails.city)
        else:
            city=cities1.objects.filter(state_id=userdetails.state)
        industry = client_vendor_detail.industry_type.all()
        share_obj,created = ClientCustomFields.objects.get_or_create(client_id=client_id)
        # speciality = industry_speciality.objects.get(
        #     id=client_vendor_detail.industry_speciality_id)'speciality': speciality,
        return render(request, 'dashboard/client_onBoarding.html', {'company_logo':company_logo,
                'industry': industry,
                'state': state,
                'cities':city,
                'country': country,
                'userdetails': userdetails,
                'client_vendor': client_vendor_detail,
                'expert': expert,
                'industry': industry,
                'share_obj':share_obj
            })
    else:
        return render(request, 'dashboard/client_onBoarding.html', {})

# === Download PDF Client  Boarding ===
@login_required
@is_superadmin
def download_pdf_client_boarding(request, client_id, client_name):
    # This function used for display onBoarding form in PDF formate
    industry = industry_type.objects.all()
    expert = industry_speciality.objects.all()
    countrow = client_vendor.objects.filter(user_id=client_id).count()
    if countrow == 1:
        client_vendor_detail = client_vendor.objects.get(user_id=client_id)
        userdetails = user.objects.get(id=client_id)
        country = countries_list.objects.get(id=userdetails.country)
        # country = countries.objects.get(id=userdetails.country)
        # state = states.objects.get(id=userdetails.state)
        state = states1.objects.get(id=userdetails.state)
        industry = client_vendor_detail.industry_type.all()
        speciality = client_vendor_detail.industry_speciality.all()
    template = get_template('campaign/client_boarding_pdf.html')
    html = template.render({'speciality': speciality, 'industry': industry, 'state': state, 'country': country, 'userdetails': userdetails, 'client_vendor': client_vendor_detail, 'expert': expert, 'industry': industry})
    response = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)

    if not pdf.err:
        return HttpResponse(response.getvalue(), content_type='application/pdf')
    else:
        return HttpResponse("Error Rendering PDF", status=400)

# === Vendor On-Boarding ===
@login_required
@is_superadmin
def vendor_onBoarding(request, vendor_id):
    # vendor On boarding
    industry = industry_type.objects.all()
    expert = industry_speciality.objects.all()
    marketing_method = source_touches.objects.filter(is_active=1).all()
    company_size_data = company_size.objects.all()
    capacity_type = source_touches.objects.all()
    countrow = client_vendor.objects.filter(user_id=vendor_id).count()
    delivery_time1 = delivery_time.objects.all()
    delivery_method1 = delivery_method.objects.all()
    complex_program = level_intent.objects.filter(is_active=1).all()
    registrtions_process1 = registration_process.objects.filter(
        is_active=1).all()
    networks_and_publishers1 = networks_and_publishers.objects.filter(
        is_active=1).all()
    database_attribute1 = database_attribute.objects.filter(is_active=1).all()
    language_supported1 = language_supported.objects.filter(
        is_active=1).all()
    vendor_type = VendorType.objects.all()
    temp_dict = {
        'us_size_value': None,
        'us_size_multiplier': None,
        'overall_size_value': None,
        'overall_size_multiplier': None,
        'opt_in_size_value': None,
        'opt_in_size_multiplier': None,
    }
    if countrow == 1:
        client_vendor_detail = client_vendor.objects.get(user_id=vendor_id)
        userdetails = user.objects.get(id=vendor_id)
        # country = countries.objects.get(id=userdetails.country)
        country = countries_list.objects.get(id=userdetails.country)
        state = states1.objects.get(id=userdetails.state)
        if userdetails.city:
            city=cities1.objects.get(id=userdetails.city)
        else:
            city=cities1.objects.filter(state_id=userdetails.state)
        # state = states.objects.get(id=userdetails.state)
        industry = client_vendor_detail.industry_type.all()
        speciality = client_vendor_detail.industry_speciality.all()
        counter = data_assesment.objects.filter(user_id=vendor_id).count()
        share_obj,created = ClientCustomFields.objects.get_or_create(client_id=vendor_id)
        if counter == 1:
            data_assesment1 = data_assesment.objects.get(user_id=vendor_id)
            from superadmin.utils import vendor_database_size_multiplier
            # fetch database size value with mutiplier...Akshay G..Date 6th Nov, 2019
            temp_dict = vendor_database_size_multiplier(data_assesment1)
        else:
            data_assesment1 = {}
        context= {
            'language_supported': language_supported1,
            'vendor_type': vendor_type,
            'networks_and_publishers': networks_and_publishers1,
            'data_assesment': data_assesment1,
            'database_attribute': database_attribute1,
            'registrtions_process': registrtions_process1,
            'complex_program': complex_program,
            'delivery_time': delivery_time1,
            'delivery_method': delivery_method1,
            'capacity_type': capacity_type,
            'company_size': company_size_data,
            'marketing_method': marketing_method,
            'speciality': speciality,
            'industry': industry,
            'cities':city,
            'state': state,
            'country': country,
            'userdetails': userdetails,
            'client_vendor': client_vendor_detail,
            'expert': expert,
            'industry': industry,
            'share_obj':share_obj,
            **temp_dict,
            }
        return render(request, 'dashboard/vendor_onBoarding.html', context)
    else:
        return render(request,'dashboard/vendor_onBoarding.html', {
            'language_supported': language_supported1,
            'vendor_type': vendor_type,
            'networks_and_publishers': networks_and_publishers1,
            'database_attribute': database_attribute1,
            'registrtions_process': registrtions_process1,
            'complex_program': complex_program,
            'delivery_time': delivery_time1,
            'delivery_method': delivery_method1,
            'capacity_type': capacity_type,
            'company_size': company_size_data,
            'marketing_method': marketing_method,
            'expert': expert,
            'cities':city,
            'countries': countries1,
            **temp_dict,
            'industry': industry})

# === Download PDF Vendor Boarding ===
@login_required
@is_superadmin
def download_pdf_vendor_boarding(request, vendor_id, vendor_name):
    # This function used for display onBoarding form in PDF formate
    industry = industry_type.objects.all()
    expert = industry_speciality.objects.all()
    marketing_method = source_touches.objects.filter(is_active=1).all()
    company_size_data = company_size.objects.all()
    capacity_type = source_touches.objects.all()
    countrow = client_vendor.objects.filter(user_id=vendor_id).count()
    delivery_time1 = delivery_time.objects.all()
    delivery_method1 = delivery_method.objects.all()
    complex_program = level_intent.objects.filter(is_active=1).all()
    registrtions_process1 = registration_process.objects.filter(
        is_active=1).all()
    networks_and_publishers1 = networks_and_publishers.objects.filter(
        is_active=1).all()
    database_attribute1 = database_attribute.objects.filter(is_active=1).all()
    language_supported1 = language_supported.objects.filter(is_active=1).all()
    userdetails = user.objects.get(id=vendor_id)
    country = countries_list.objects.get(id=userdetails.country)
    # country = countries.objects.get(id=userdetails.country)
    state = states1.objects.get(id=userdetails.state)
    # state = states.objects.get(id=userdetails.state)
    vendor_type = VendorType.objects.all()
    if countrow == 1:
        client_vendor_detail = client_vendor.objects.get(user_id=vendor_id)
        industry = client_vendor_detail.industry_type.all()
        speciality = client_vendor_detail.industry_speciality.all()
        counter = data_assesment.objects.filter(user_id=vendor_id).count()

        if counter == 1:
            data_assesment1 = data_assesment.objects.get(user_id=vendor_id)
        else:
            data_assesment1 = {}
        template = get_template('campaign/vendor_boarding_pdf.html')
        html = template.render({'language_supported': language_supported1, 'vendor_type': vendor_type, 'networks_and_publishers': networks_and_publishers1, 'data_assesment': data_assesment1, 'database_attribute': database_attribute1, 'registrtions_process': registrtions_process1, 'complex_program': complex_program, 'delivery_time': delivery_time1, 'delivery_method': delivery_method1, 'capacity_type': capacity_type, 'company_size': company_size_data, 'marketing_method': marketing_method, 'speciality': speciality, 'industry': industry, 'state': state, 'country': country, 'userdetails': userdetails, 'client_vendor': client_vendor_detail, 'expert': expert, 'industry': industry})
    else:
        template = get_template('campaign/vendor_boarding_pdf.html')
        html = template.render({'language_supported': language_supported1, 'vendor_type': vendor_type, 'networks_and_publishers': networks_and_publishers1, 'database_attribute': database_attribute1, 'registrtions_process': registrtions_process1, 'complex_program': complex_program, 'delivery_time': delivery_time1, 'delivery_method': delivery_method1, 'capacity_type': capacity_type, 'company_size': company_size_data, 'marketing_method': marketing_method,  'industry': industry, 'state': state, 'country': country, 'userdetails': userdetails,  'expert': expert})
    response = BytesIO()
    pdf = pisa.pisaDocument(BytesIO(html.encode("UTF-8")), response)
    if not pdf.err:
        return HttpResponse(response.getvalue(), content_type='application/pdf')
    else:
        return HttpResponse("Error Rendering PDF", status=400)

# === On-Boarding Aapprove ===
@login_required
@is_superadmin
def onboarding_approve(request, vendor_id):
    # on boarding approve
    t1 = user.objects.get(id=vendor_id)
    t1.approve = 1
    t1.status = 3
    email = t1.email
    t1.save()
    # to set all default access
    set_user_access(t1)
    # to set all email noti
    set_all_default_mailchoices(t1)
    title = "OnBoarding Status"
    desc = "Welcome To Techconnectr Marketplace."
    RegisterNotification(int(request.session['userid']), vendor_id, desc, "Hello, "+t1.user_name, 1, None, None)
    site_url=settings.BASE_URL
    subject = "OnBoarding Status From Techconnectr"
    html_message = render_to_string('email_templates/onboarding_email_template.html', {'username':email,'site_url':site_url})
    # html_message = "OnBoarding Status Approved by superadmin . Now You can access Portal"
    plain_message = strip_tags(html_message)
    from_email = settings.EMAIL_HOST_USER
    to_list = [email]
    send_mail(subject, plain_message, from_email, to_list, fail_silently=True,html_message=html_message)
    return redirect('/superadmin/User_On_Bording/')

# === On-Boarding Status Hold ===
@login_required
@is_superadmin
def onboarding_status_hold(request, vendor_id):
    # On boarding status  hold
    t1 = user.objects.get(id=vendor_id)
    t1.status = 4
    t1.approve = 0
    email = t1.email
    t1.save()
    # title = "OnBoarding Status"
    # desc = "OnBoarding Status Hold by superadmin"
    # RegisterNotification(int(request.session['userid']), vendor_id, desc, title, 1, None, None)
    subject = "OnBoarding Status From Techconnectr"
    html_message = "OnBoarding Status Hold by superadmin"
    plain_message = strip_tags(html_message)
    from_email = settings.EMAIL_HOST_USER
    to_list = [email]
    send_mail(subject, plain_message, from_email, to_list, fail_silently=True)
    return redirect('/superadmin/User_On_Bording/')

# === On-Boarding Status Rrejecte ===
@login_required
@is_superadmin
def onboarding_status_rejecte(request, vendor_id):
    # On Boarding status rejecte
    t1 = user.objects.get(id=vendor_id)
    t1.status = 5
    email = t1.email
    t1.save()
    # title = "OnBoarding Status"
    # desc = " OnBoarding Status Rejected by superadmin"
    # RegisterNotification(int(request.session['userid']), vendor_id, desc, title, 1, None, None)
    subject = "OnBoarding Status From Techconnectr"
    html_message = "OnBoarding Status Rejected by superadmin"
    plain_message = strip_tags(html_message)
    from_email = settings.EMAIL_HOST_USER
    to_list = [email]
    send_mail(subject, plain_message, from_email, to_list, fail_silently=True)
    return redirect('/superadmin/User_On_Bording/')

# === Client List ===
@login_required
@is_superadmin
def clientlist(request):
    # Client List
    users = user.objects.filter(usertype_id=1, status=3)
    clients = client_vendor.objects.filter(user_id__in=users)
    return render(request, 'client/clientlist.html', {'clients': clients})

# === Vendor List ===
@login_required
@is_superadmin
def vendorlist(request):
    # Vendor List
    vendors = user.objects.filter(usertype_id=2, status=3)
    vendors1 = client_vendor.objects.filter(user_id__in=vendors)
    return render(request, 'vendor/super_vendorlist.html', {'vendors': vendors1})

# === External Users ===
@is_superadmin
def ex_users(request):
    # Ex users by user List
    final_users = []
    seperator = []
    id=request.POST.get('user_id')
    users = user.objects.get(id=id).child_user.all().filter(usertype_id=6)
    groups = Client_User_Group.objects.filter(group_owner_id=id)
    for i in users:
        for j in groups:
            if i in j.group_users.all():
                seperator.append(i)
                final_users.append({'name':i.user_name, 'group': j.group_name})
        if i not in seperator:
            final_users.append({'name':i.user_name, 'group': 'No group connected'})

    data = {'users': final_users, 'groups': [d for d in groups.values_list('group_name', flat=True)]}
    return JsonResponse(data)

# === External Vendors ===
@is_superadmin
def ex_vendors(request):
    # Ex vendors by user List
    users = [d for d in user.objects.get(id=request.POST.get('user_id')).child_user.all().filter(usertype_id=5).values_list('user_name', 'email')]
    data = {'name': users}
    return JsonResponse(data)

# === Vendor Campaign List ===
@login_required
@is_superadmin
def vendor_campaign_list(request, vendor_id):
    # Vendor Campaign List
    counter = campaign_allocation.objects.filter(
        client_vendor_id=vendor_id).count()
    site_url = request.META.get('HTTP_REFERER')
    data = []
    if counter > 0:
        campaign_allocation_datails = campaign_allocation.objects.filter(
            client_vendor_id=vendor_id)
        for row in campaign_allocation_datails:
            campaign_details = Campaign.objects.get(id=row.campaign_id)
            if campaign_details.adhoc:
                type = 'adhoc'
            else:
                if row.cpl == -1 or row.volume == -1:
                    type = 'RFQ'
                else:
                    type = 'Normal'

            data.append(
                {
                    'id': campaign_details.id,
                    'name': campaign_details.name,
                    'description': campaign_details.description,
                    'io_number': campaign_details.io_number,
                    'cpl': row.cpl if row.cpl == -1 and row.volume == -1 else row.cpl,
                    'leads': row.old_volume if row.cpl == -1 and row.volume == -1 else row.volume,
                    'start_date': campaign_details.start_date,
                    'end_date': campaign_details.end_date,
                    'status': row.status,
                    'type': type,
                }
            )
        return render(request, 'vendor/campaign_list.html', {'camps': data, 'site_url': site_url})
    else:
        return render(request, 'vendor/campaign_list.html', {'site_url': site_url})

# === Partner List ===
@login_required
@is_superadmin
def partnerlist(request):
    # Partner List
    return render(request, 'partner/partnerlist.html', {})

# === Campaign List ===
def campaignlist(request):
    # Campaign list
    campaigns = Campaign.objects.all().order_by('-created_date')
    users = user.objects.all()
    return campaigns, users
    # return render(request, 'campaign/campaignlist.html', {'campaigns': campaigns, 'users': users})

# === Live Campaign Lists ===
@login_required
@is_superadmin
def live_campaign_lists(request):
    # Live campaign list
    data = campaignlist(request)
    return render(request, 'campaign/live_campaign.html', {'campaigns': data[0], 'users': data[1]})

# === Paused Campaign List ===
@login_required
@is_superadmin
def paused_campaign_list(request):
    # Paused campaign list
    data = campaignlist(request)
    return render(request, 'campaign/pause_campaign.html', {'campaigns': data[0], 'users': data[1]})

# === Completed Campaign List ===
@login_required
@is_superadmin
def completed_campaign_list(request):
    # Completed campaign list
    data = campaignlist(request)
    return render(request, 'campaign/complete_campaign.html', {'campaigns': data[0], 'users': data[1]})

# === Pending Campaign List===
@login_required
@is_superadmin
def pending_campaign_list(request):
    # pending campaign list
    data = campaignlist(request)
    return render(request, 'campaign/pending_campaign.html', {'campaigns': data[0], 'users': data[1]})

# === Assigned Campaign List ===
@login_required
@is_superadmin
def assigned_campaign_list(request):
    # Assigned campaign list
    data = campaignlist(request)
    return render(request, 'campaign/assigned_campaign.html', {'campaigns': data[0], 'users': data[1]})

# === Tech ===
@login_required
@is_superadmin
def tech(request):
    # tech of ticket
    tickets = Raised_Tickets.objects.all()
    return render(request, 'tech/ticket.html', {'tickets':tickets,'count':tickets.count()})

# === Client Reports ===
@login_required
@is_superadmin
def clientreports(request):
    # client Reports
    return render(request, 'reports/clientreports.html', {})

# === Vendor Reports ===
@login_required
@is_superadmin
def vendorreports(request):
    # vendor Reports
    return render(request, 'reports/vendorreports.html', {})

# === Campaign Reports ===
@login_required
@is_superadmin
def campaignreports(request):
    # Campaign Reports
    import datetime
    result = []
    clients = []
    for client in user.objects.filter(usertype_id=1):
        clients.append({'id':client.id,'name':client.user_name})
    all_camp = Campaign.objects.filter(status__in=[1,2,3,4,5]).order_by('end_date')
    for i in all_camp:
        target = 0
        if i.target_quantity is not None:
            target = int(i.target_quantity)
        approve = 0
        camp_alloc = campaign_allocation.objects.filter(campaign_id=i.id)

        for j in camp_alloc:
            if j.approve_leads is not None:
                approve += int(j.approve_leads)

        start = datetime.datetime.strptime(str(i.start_date), '%Y-%m-%d')
        end = datetime.datetime.strptime(str(i.end_date), '%Y-%m-%d')
        today = datetime.datetime.now()

        # for finding whether campaign is ahead or behind
        lapsed = (today-start).days
        if lapsed < 0:
            lapsed = 0

        total_days = (end-start).days
        if target > 0:
            per_day = int(target)/int(total_days)
        else:
            per_day = 0
        # per_day = int(target)/int(total_days)

        if lapsed > total_days:
            till_date_to_be_approved = per_day*total_days
        else:
            till_date_to_be_approved = per_day*lapsed

        # conditions for checking campaign status and sending colors based on progress
        if till_date_to_be_approved > int(approve):
            color = '#ff1a1a'
        elif till_date_to_be_approved == int(approve):
            color = '#3399ff'
        else:
            color = '#2eb82e'

        # finding progress percentage for campaign
        # progress = percentage(camp_id=i.id)
        if target > 0:
            progress = percentage(camp_id=i.id)
        else:
            progress = 0

        result.append(
            {
                'id': i.id,
                'name': i.name,
                'client_id': i.user.id,
                'leads': target,
                'assigned': i.approveleads,
                'start_date': i.start_date,
                'end_date': i.end_date,
                'progress': progress,
                'color': color,
                'status': i.status,
            }
        )
    return render(request, 'reports/campaignreports.html', {'result':result,'clients':clients})

# === Campaign Description  ===
@login_required
@is_superadmin
def campaingdescrption(request, campaign_id):
    # Campaign Description
    camp = Campaign.objects.get(id=campaign_id)
    speci = Specification.objects.get(campaign_id=campaign_id)
    mapping = Mapping.objects.get(campaign_id=campaign_id)
    terms = Terms.objects.get(campaign_id=campaign_id)
    delivery = Delivery.objects.get(campaign_id=campaign_id)

    return render(request, 'campaign/campdesc.html', {'delivery': delivery, 'terms': terms, 'camp': camp, 'speci': speci, 'mapping': mapping})

# === On Boarding ===
def onBording(request):
    # On Boarding
    userid = request.POST.get('id', None)
    data = {'success': 2}
    countrow = client_vendor.objects.filter(user_id=userid).count()
    if countrow == 1:
        client_vendor_detail = client_vendor.objects.get(user_id=userid)
        userdetails = user.objects.get(id=userid)
        country = countries_list.objects.get(id=userdetails.country)
        # country = countries.objects.get(id=userdetails.country)
        state = states1.objects.get(id=userdetails.state)
        # state = states.objects.get(id=userdetails.state)
        industry = client_vendor_detail.industry_type.all()
        speciality = client_vendor_detail.industry_speciality_id.all()
        data = {
            'comp_name': userdetails.user_name,
            'website': client_vendor_detail.website,
            'add1': userdetails.address_line1,
            'add2': userdetails.address_line2,
            'country_name': country.name,
            'country_id': country.id,
            'state_name': state.name,
            'state_id': state.id,
            'phone': userdetails.contact,
            'primary_name': client_vendor_detail.primary_name,
            'primary_designation': client_vendor_detail.primary_designation,
            'primary_email': client_vendor_detail.primary_email,
            'primary_contact': client_vendor_detail.primary_contact,
            'secondary_name': client_vendor_detail.secondary_name,
            'secondary_email': client_vendor_detail.secondary_email,
            'secondary_contact': client_vendor_detail.secondary_contact,
            'secondary_designation': client_vendor_detail.secondary_designation,
            'address': userdetails.address_line1 + userdetails.address_line2,
            'industry_id': industry.id,
            'industry_name': industry.type,
            'speciality_id': speciality.id,
            'speciality_name': speciality.name,
        }
        return JsonResponse(data)
    else:
        return JsonResponse(data)


@login_required
@is_superadmin
def Relation(request):
    # Relation for group list
    group_collection = Group_Collection.objects.all()
    users = user.objects.all()
    return render(request, 'relation/grouplist.html', {'group': group_collection, 'users': users})

# === Create group list ===
def create_grp(request):
    # Create group list
    grp_name = request.POST.get('grp_name')
    client_ids = request.POST.get('client_ids')
    vendor_ids = request.POST.get('vendor_ids')

# === Get vendor list ===
def getVendorList(request):
    # Get vendor list
    data = campaign_allocation.objects.filter(
        campaign_id=request.POST.get('id'))
    serializer = Campaign_allocation_serializers(data, many=True)
    return JsonResponse(serializer.data, safe=False)

# === get pricing ===
def getPricing(request):
    # get pricing for CPL and target quantity
    id = request.POST.get('id', None)
    campaign_details = Campaign.objects.get(id=id)
    data = {
        'cpl': campaign_details.cpl,
        'targat_quantity': 100,
    }
    return JsonResponse(data)

# === RFQ Campaign ===
@login_required
@is_superadmin
def RFQ_Campaign(request, camp_id):
    # RFQ Campaign
    ids = []
    vendor_data = []
    campaign_details = Campaign.objects.get(id=camp_id)
    data = {
        'cpl': campaign_details.cpl,
        'targat_quantity': campaign_details.target_quantity,
        'ramaining_quantity': campaign_details.raimainingleads,
        'camp_id': camp_id,
        'camp_name': campaign_details.name,
        'client_name': campaign_details.user.user_name,
    }

    # check vendor suggested by client or not | vendor is suitable or not for this campaign
    vendor_sugg = campaign_allocation.objects.filter(campaign_id=camp_id)
    for row in vendor_sugg:
        if user.objects.filter(id=row.client_vendor_id, status=3).count() > 0:
            vendor_detail = client_vendor.objects.get(
                user_id=row.client_vendor_id)
            if row.suggest_status == 1 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
                vendor_data.append({
                    'checked': 'checked',
                    'display': 'none',
                    'vendor_name': row.client_vendor.user_name,
                    'vendor_id': row.client_vendor_id,
                    'user_id': row.client_vendor_id,
                    'camp_id': row.campaign_id,
                    'website': vendor_detail.website,
                    'company_size': vendor_detail.company_size,
                    'industry_speciality': vendor_detail.industry_speciality.name,
                })
            elif row.status == 4 and row.cpl != -1 and row.volume != -1 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
                vendor_data.append({
                    'checked': '0',
                    'display': 'none',
                    'vendor_name': row.client_vendor.user_name,
                    'vendor_id': row.client_vendor_id,
                    'user_id': row.client_vendor_id,
                    'camp_id': row.campaign_id,
                    'website': vendor_detail.website,
                    'company_size': vendor_detail.company_size,
                    'industry_speciality': vendor_detail.industry_speciality.name,
                })
            elif row.status == 3 and row.volume == -1 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
                vendor_data.append({
                    'checked': '0',
                    'display': 'none',
                    'vendor_name': row.client_vendor.user_name,
                    'vendor_id': row.client_vendor_id,
                    'user_id': row.client_vendor_id,
                    'camp_id': row.campaign_id,
                    'website': vendor_detail.website,
                    'company_size': vendor_detail.company_size,
                    'industry_speciality': vendor_detail.industry_speciality.name,
                })
            elif row.status == 3 and row.volume > 0 and row.cpl > 0 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
            elif row.status == 1 and row.volume > 0 and row.cpl > 0 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)

    vendor_sugg = match_campaign_vendor.objects.filter(
        campaign_id=camp_id).exclude(client_vendor_id__in=ids)
    for row in vendor_sugg:
        if client_vendor.objects.filter(user_id=row.client_vendor_id).count() > 0:
            vendor_detail = client_vendor.objects.get(
                user_id=row.client_vendor_id)
            ids.append(row.client_vendor_id)
            vendor_data.append({
                'checked': '0',
                'display': 'none',
                'vendor_name': row.client_vendor.user_name,
                'vendor_id': row.client_vendor_id,
                'user_id': row.client_vendor_id,
                'camp_id': row.campaign_id,
                'website': vendor_detail.website,
                'company_size': vendor_detail.company_size,
                'industry_speciality': vendor_detail.industry_speciality.name,
            })

    users = user.objects.filter(usertype_id=2, status=3).exclude(id__in=ids)
    vendor_sugg = client_vendor.objects.filter(user_id__in=users)
    for row in vendor_sugg:
        vendor_detail = client_vendor.objects.get(user_id=row.user_id)
        if row.user_id not in ids:
            vendor_data.append({
                'checked': '0',
                'display': 'none',
                'vendor_name': row.user.user_name,
                'vendor_id': row.user_id,
                'user_id': row.user_id,
                'camp_id': camp_id,
                'website': vendor_detail.website,
                'company_size': vendor_detail.company_size,
                'industry_speciality': vendor_detail.industry_speciality.name,
            })
    data_assesment1 = data_assesment.objects.filter(user_id__in=users)
    marketing_method = source_touches.objects.filter(is_active=1)
    cpl_counter = campaign_allocation.objects.filter(
        campaign_id=camp_id, status=3, cpl=0, volume=-1).count()
    cpl_list = campaign_allocation.objects.filter(
        campaign_id=camp_id, status=3, cpl=0, volume=-1)
    return render(request, 'campaign/rfq_camp_allocation.html', {'cpl_counter': cpl_counter, 'cpl_list': cpl_list, 'data': data, 'vendor_data': vendor_data, 'data_assesment': data_assesment1, 'marketing_method': marketing_method})

# === Update RFQ and CPL ===
def update_rfq_cpl(request):
    # Update RFQ and CPL
    if Campaign_rfq.objects.filter(campaign_id=request.POST.get('camp_id')).count() > 0:
        t = Campaign_rfq.objects.get(campaign_id=request.POST.get('camp_id'))
        if t.status != 1:
            t.cpl = request.POST.get('cpl')
            t.volume = request.POST.get('volume')
            t.status = 1
            t.save()
        else:
            data = {'success': 1, 'msg': 'You already submitted RFQ CPL'}
    else:
        Campaign_rfq.objects.create(campaign_id=request.POST.get(
            'camp_id'), volume=request.POST.get('volume'), cpl=request.POST.get('cpl'), status=1)
        campaign = Campaign.objects.get(id=request.POST.get('camp_id'))
        title = "RFQ CPL"
        desc = "Quote Recieved for  RFQ "+str(campaign.name)
        # from client.utils import noti_via_mail
        # noti = noti_via_mail([campaign.user_id], title, desc, mail_noti_superadmin_action)
        RegisterNotification(
            request.session['userid'], campaign.user_id, desc, title, 1, campaign, None)
        campaign.rfq_status = 1
        campaign.save()
        data = {'success': 1, 'msg': 'Action Successfully Submitted!...'}
    return JsonResponse(data)

# ===  RFQ and CPL Lead Allocation ===
def rqfcplLeadAllocation(camp_id, vendor_id, cpl, leads):
    # RFQ and CPL Lead Allocation
    rfqcounter = campaign_allocation.objects.filter(
        client_vendor_id=vendor_id, campaign_id=camp_id, status=3, volume=-1).count()
    ids = []
    if rfqcounter == 1:
        t1 = campaign_allocation.objects.get(
            client_vendor_id=vendor_id, campaign_id=camp_id, status=3, volume=-1)
        t1.cpl = cpl
        t1.volume = leads
        t1.status = 3
        t1.suggest_status = 0
        t1.save()
        ids.append(vendor_id)
    return ids

# === bulk CPL Lead Allocation ===
def bulk_cplLeadAllocation(request):
    data = ast.literal_eval(request.POST.get('data'))
    for vendor_data in data:
        cplLeadAllocation(request, vendor_data)
    return JsonResponse({})
def cplLeadAllocation(request, bulk_data = {}):
    # CPL Lead Allocation
    from client.utils import noti_via_mail
    userid = request.session['userid']
    if len(bulk_data) == 0:
        vendor_id = request.POST.get('vendor_id')
        camp_id = int(request.POST.get('camp_id'))
        vcpl = request.POST.get('vcpl', None)
        vendorlead = request.POST.get('vendorlead')
        rev_ref = request.POST.get('rev_ref')
        remaininglead = request.POST.get('remaininglead')
        ccpl = request.POST.get('ccpl')
    else:
        vendor_id = bulk_data.get('vendor_id')
        camp_id = int(bulk_data.get('camp_id'))
        vcpl = bulk_data.get('vcpl', None)
        vendorlead = bulk_data.get('vendorlead')
        rev_ref = bulk_data.get('rev_ref')
        remaininglead = bulk_data.get('remaininglead')
        ccpl = bulk_data.get('ccpl')
    # following condition check client allocate lead to self that time direct that campaign goes in live stage
    # abhi 31-5-2019
    if int(userid) == int(vendor_id) :
        camp_alloc_status = 1
    else:
        camp_alloc_status = 3
    vendor_user = user.objects.get(id = vendor_id)
    # check if it's a internal vendor to make camp status 'live' on vendor side..Akshay G.
    if vendor_user.usertype_id == 9:
        camp_alloc_status = 1
    ids = rqfcplLeadAllocation(camp_id, vendor_id, vcpl, vendorlead)
    camp_alloc = campaign_allocation.objects.filter(client_vendor_id=vendor_id, campaign_id=camp_id)
    v_cpl=round(float(vcpl)-(float(vcpl)*float(rev_ref)/100),2)
    if camp_alloc:
        camp_alloc[0].cpl = v_cpl
        camp_alloc[0].client_cpl = round(float(vcpl),2)
        camp_alloc[0].volume = vendorlead
        camp_alloc[0].suggest_status = 0
        camp_alloc[0].status = camp_alloc_status
        camp_alloc[0].save()
        alloc_id = camp_alloc[0]
        vendor_agreement(vendor_id, userid, alloc_id)
    else:
        if vendor_id not in ids:
            campaign_allocation.objects.create(client_vendor_id=vendor_id,campaign_id=camp_id, cpl=v_cpl, client_cpl=vcpl, volume=vendorlead, status=camp_alloc_status)
            alloc_id = campaign_allocation.objects.latest('id')
            vendor_agreement(vendor_id, userid, alloc_id)
    uv = user.objects.get(id = vendor_id)
    t1 = Campaign.objects.get(id=camp_id)

    # following condition check vendor is tc or external vendor or client
    # abhi 31-5-2019
    if uv.usertype.id not in [5,9]:
        if int(userid) == int(vendor_id):
            t1.raimainingleads = int(remaininglead)
        else:
            t1.raimainingleads = int(remaininglead)
    else:
        t1.raimainingleads = int(remaininglead)
    if uv.usertype.id == 9:
        t1.approveleads = int(vendorlead) + t1.approveleads
    t1.save()
    #End

    # to take the campaign live or assigned on allocations
    if t1.target_quantity == t1.approveleads and t1.status != 1:
        t1.status = 5

    today = datetime.now().date()
    # if today == t1.start_date or today > t1.start_date:
    #     t1.status = 1
    t1.save()

    title = "New Campaign Allocated by"
    desc = "Techconnectr"
    noti = noti_via_mail([vendor_id], title, desc, mail_vendor_assignment)
    RegisterNotification(userid, vendor_id, desc, title, 2, None,alloc_id)
    data = {'success': '1'}
    Campaign_commission.objects.create(vendor_id=vendor_id, campaign_id=camp_id,camp_alloc_id = alloc_id.id,
        ccpl=ccpl, allocated_lead=vendorlead,
        rev_share_percentage=rev_ref,vcpl=vcpl)
    return JsonResponse(data)

# === RFQ Vendor Allocation ===
def rfq_vendor_allocation(request):
    # RFQ Vendor Allocation
    ids = request.POST.getlist('ids[]')
    camp_id = request.POST.get('camp_id')
    userid = request.session['userid']
    title = "New RFQ Campaign Allocated by"
    desc = "Superadmin"
    for id in ids:
        counter = campaign_allocation.objects.filter(
            campaign_id=camp_id, client_vendor_id=id, cpl=-1, volume=-1, status=3).count()
        allocation = campaign_allocation.objects.filter(
            campaign_id=camp_id, client_vendor_id=id)
        if counter != 1 and allocation.count() != 1:
            campaign_allocation.objects.create(
                campaign_id=camp_id, client_vendor_id=id, cpl=-1, volume=-1, status=3)
        else:
            if allocation:
                allocation[0].cpl = -1
                allocation[0].volume = -1
                allocation[0].status = 3
                allocation[0].save()
        camp_alloc = campaign_allocation.objects.latest('id') if allocation[0] != campaign_allocation.objects.latest('id') else allocation[0]
        # from client.utils import noti_via_mail
        # noti = noti_via_mail(id, title, desc, mail_noti_rfq_quotes)
        RegisterNotification(userid, id, desc, title, 2, None, camp_alloc)
    data = {'success': 1}
    return JsonResponse(data)

# === Client Campaign list ===
@login_required
@is_superadmin
def client_campaign_list(request, client_id):
    # Client Campaign list
    counter = Campaign.objects.filter(user_id=client_id).count()
    data = []
    if counter > 0:
        campaign_details = Campaign.objects.filter(user_id=client_id)
        return render(request, 'client/campaign_list.html', {'camps': campaign_details})
    else:
        return render(request, 'client/campaign_list.html', {})

# === Campaign vendor ===
@login_required
@is_superadmin
def campaign_vendor(request, camp_id):
    # Campaign vendor
    camp = Campaign.objects.get(id=camp_id)
    delivery_status= Delivery.objects.get(campaign_id=camp_id)
    if delivery_status.tc_header_status + delivery_status.custom_header_status == 1:
        delivery_status = True
    else:
        delivery_status = False
    vendoralloc = campaign_allocation.objects.filter(campaign_id=camp_id)
    morealloc = More_allocation.objects.filter(campaign_id=camp_id)
    site_url = request.META.get('HTTP_REFERER')
    return render(request, 'client/campdescription.html', {'delivery_status':delivery_status,'site_url': site_url, 'camp': camp, 'vendoralloc': vendoralloc, 'more_alloc': morealloc})

# === User delete ===
def userDelete(request):
    # User delete
    userid = request.session['userid']
    id = request.POST.get('id')
    pwd = request.POST.get('pwd')
    data = {'success': 0}
    counter = user.objects.filter(id=userid, password=pwd,).count()
    if counter == 1:
        user.objects.get(pk=id).delete()
        client_vendor.objects.filter(user_id=id).delete()
        data = {'success': 1}
        return JsonResponse(data)
    else:
        return JsonResponse(data)

# === Lead List for vendor ===
@login_required
@is_superadmin
def leadlist(request, camp_id, status):
    # Lead List for vendor
    list1 = []
    all_header = []
    all_lead_header = []
    header = []
    site_url = request.META.get('HTTP_REFERER')
    camp = campaign_allocation.objects.filter(
        id=camp_id, status=status).count()
    if camp == 1 and status == 1:
        camp_alloc = campaign_allocation.objects.get(
            id=camp_id, status=status)
        header = create_custom_header(
            camp_alloc.campaign_id, request.session['userid'])
        data = {'camp_id': camp_alloc.campaign_id, 'camp_name': camp_alloc.campaign.name, 'cpl': camp_alloc.cpl,
                'lead': camp_alloc.volume, 'submited_lead': camp_alloc.submited_lead, 'return_lead': camp_alloc.return_lead}
        if camp_alloc.upload_leads:
            list1 = ast.literal_eval(camp_alloc.upload_leads)
            if len(header) == 0:
                for dict in list1[0]:
                    all_header.append(dict)
                    all_lead_header.append(dict)
                all_header = create_header(all_header)
            else:
                all_header = header
                for dict in list1[0]:
                    all_lead_header.append(dict)
                all_header = create_header(all_header)
        labels = get_lead_header(camp_alloc.campaign_id)
        if len(all_header) == 0:
            all_header = labels
        return render(request, 'vendor/vendor_leadlist.html', {'site_url': site_url, 'campaigns': data, 'leadlist': list1, 'all_lead_header': all_lead_header, 'all_header': all_header, 'header': header, 'status': status})
    elif camp == 1 and status == 4:
        camp_alloc = campaign_allocation.objects.get(
            id=camp_id, status=status)
        header = create_custom_header(
            camp_alloc.campaign_id, request.session['userid'])
        data = {'camp_id': camp_alloc.camp_id, 'camp_name': camp_alloc.campaign.name, 'cpl': camp_alloc.cpl,
                'lead': camp_alloc.old_volume, 'submited_lead': camp_alloc.submited_lead, 'return_lead': camp_alloc.return_lead}
        if camp_alloc.upload_leads != None:
            list1 = ast.literal_eval(camp_alloc.upload_leads)
            if len(header) == 0:
                for dict in list1[0]:
                    all_header.append(dict)
                    all_lead_header.append(dict)
                    all_header = create_header(all_header)
            else:
                all_header = header
                for dict in list1[0]:
                    all_lead_header.append(dict)
                all_header = create_header(all_header)
        labels = get_lead_header(camp_alloc.campaign_id)
        if len(all_header) == 0:
            all_header = labels
        return render(request, 'vendor/vendor_leadlist.html', {'site_url': site_url, 'lead_rejected': lead_rejected, 'campaigns': data, 'leadlist': list1, 'all_lead_header': all_lead_header, 'all_header': all_header, 'header': header, 'status': status})
    return render(request, 'vendor/vendor_leadlist.html', {'site_url': site_url, 'camp_id': camp_id, 'status': status})

# === create header ===
def create_header(dict):
    # create header
    list = []
    for row in dict:
        if row == 'reason':
            list.append('Status Code')
        else:
            list.append(row)
    return list

# === create custom header ===
def create_custom_header(camp_id, userid):
    # create custom header
    fav_lead = []
    fav_lead_details = favorite_leads_header.objects.filter(
        campaign_id=camp_id, user_id=userid)
    if fav_lead_details.count() == 1:
        fav_lead_details = favorite_leads_header.objects.get(
            campaign_id=camp_id, user_id=userid)
        return ast.literal_eval(fav_lead_details.header)
    else:
        return fav_lead

# === Lead Assign ===
def lead_assign(request):
    # assign more leads for allocation --suryakant
    data = {'success': 0, 'msg': 'Request failed for assign more leads, Please contact admin'}
    lead = request.POST.get('lead')
    campaign_id = request.POST.get('campaign_id')
    vendor_id = request.POST.get('vendor_id')
    status = request.POST.get('status')
    cpl = request.POST.get('cpl')
    userid = request.session['userid']
    camp = Campaign.objects.get(id=campaign_id)
    camp_alloc = campaign_allocation.objects.get(id=request.POST['camp_alloc'])
    if camp_alloc.status == 3:
        if float(cpl) == float(camp_alloc.client_cpl):
            old_volume = camp_alloc.volume
            camp_alloc.volume = int(lead)
            camp_alloc.save()
            camp.raimainingleads = (camp.raimainingleads+old_volume) - int(lead)
            camp.save()
        else:
            try:
                share_obj = ClientCustomFields.objects.get(client_id=request.session['userid'])
            except Exception as e:
                share_obj = SystemDefaultFields.objects.first()
            share = share_obj.rfq_rev_share if camp.rfq == 1 else share_obj.normal_rev_share
            vcpl=round(float(cpl)-(float(cpl)*float(share)/100),2)            
            camp_alloc.pk = None
            camp_alloc.save()
            new_camp_alloc = camp_alloc
            new_camp_alloc.cpl = vcpl
            new_camp_alloc.client_cpl = round(float(cpl),2)
            new_camp_alloc.volume = int(lead)
            new_camp_alloc.save()
            camp.raimainingleads -= int(lead)
            camp.save()
            Campaign_commission.objects.create(vendor_id=vendor_id, campaign_id=camp.id, camp_alloc_id = new_camp_alloc.id,
                ccpl=cpl, allocated_lead=lead,
                rev_share_percentage=share,vcpl=vcpl)
        data = {'success': 1, 'msg': 'Request sent for assigned more leads'}
        desc = "Client updated allocation on campaign '"+camp.name+"'"
    elif camp_alloc.status in [1,2,5]:
        if float(cpl) == float(camp_alloc.client_cpl):
            more_alloc, created = More_allocation.objects.get_or_create(campaign_id=campaign_id, campaign_allocation_id=camp_alloc.id, client_vendor_id=vendor_id, cpl=cpl)
            if created:
                more_alloc.volume = int(lead)
            else:
                more_alloc.volume += int(lead)
            more_alloc.save()
            camp.raimainingleads -= int(lead)
            camp.save()
        else:
            try:
                share_obj = ClientCustomFields.objects.get(client_id=request.session['userid'])
            except Exception as e:
                share_obj = SystemDefaultFields.objects.first()
            share = share_obj.rfq_rev_share if camp.rfq == 1 else share_obj.normal_rev_share
            vcpl=round(float(cpl)-(float(cpl)*float(share)/100),2)
            camp_alloc.pk = None
            camp_alloc.save()
            new_camp_alloc = camp_alloc
            new_camp_alloc.cpl = vcpl
            new_camp_alloc.client_cpl = round(float(cpl),2)
            new_camp_alloc.volume = int(lead)
            new_camp_alloc.status = 3
            new_camp_alloc.save()
            camp.raimainingleads -= int(lead)
            camp.save()
            Campaign_commission.objects.create(vendor_id=vendor_id, campaign_id=camp.id, camp_alloc_id = new_camp_alloc.id,
                ccpl=cpl, allocated_lead=lead,
                rev_share_percentage=share,vcpl=vcpl)
        data = {'success': 1, 'msg': 'Request sent for assigned more leads'}
        desc = "Client wants to assign "+lead+" additional leads for campaign '"+camp.name+"'"
    else:
        data = {'success': 2, 'msg': 'Can\'t assign more leads'}
    # suryakant 03-10-2019
    # raised notification on additional lead allocation 
    if data['success'] == 1:
        super = [camp.user.id]
        super += [i.id for i in user.objects.filter(usertype_id=4)]
        super += [vendor_id]
        [RegisterNotification(super[0], j, desc,'Additional leads allocation', 1, camp, camp_alloc) for j in super]
        from client.utils import noti_via_mail
        noti = noti_via_mail(super,'Additional leads allocation', desc, mail_assign_more_leads)
    return JsonResponse(data)

# === Pull Assigned Lead ===
def pull_assigned_lead(request):
    # pull out leads assigned to vendors
    lead = request.POST.get('lead')
    camp = Campaign.objects.get(id=request.POST.get('campaign_id'))
    camp_alloc = campaign_allocation.objects.get(id=request.POST['camp_alloc'])
    vendor_id = request.POST.get('vendor_id')
    if camp_alloc.status in [3,5]:
        if camp_alloc.volume >= int(lead):
            camp_alloc.volume -= int(lead)
            camp_alloc.save()
            camp.raimainingleads += int(lead)
            # camp.approveleads -= int(lead)
            camp.save()
            desc = f"Client has pull out {lead} leads from campaign '{camp.name}'"
            data = {'success': 1, 'msg': 'Leads Pull out successfully'}
        else:
            data = {'success': 2, 'msg': 'Can\'t Pull out this much leads '}
    elif camp_alloc.status in [1,2]:
        if int(lead) <= (camp_alloc.volume - camp_alloc.approve_leads) and int(lead) <= camp_alloc.volume:
            old_volume = camp_alloc.volume
            camp_alloc.volume -= int(lead)
            camp_alloc.save()
            camp.raimainingleads += int(lead)
            camp.approveleads -= int(lead)
            camp.save()
            data = {'success': 1, 'msg': 'Leads Pull out successfully'}
            # suryakant 01-10-2019
            # update leads on commision table
            comm_obj = Campaign_commission.objects.get(camp_alloc_id = camp_alloc.id)
            comm_obj.allocated_lead = int(camp_alloc.volume)
            comm_obj.save()

            if camp_alloc.volume == 0:
                camp_alloc.delete()
                data = {'success': 1, 'msg': 'Leads Pull out successfully'}

            desc = f"Client has pull out {lead} leads from campaign '{camp.name}'"
        else:
            data = {'success': 2, 'msg': 'Can\'t Pull out this much leads '}
    else:
        data = {'success': 2, 'msg': 'Can\'t Pull out this much leads '}
    # suryakant 12-10-2019
    # raised notification on pull out lead allocation 
    if data['success'] == 1:
        super = [camp.user.id]
        super += [i.id for i in user.objects.filter(usertype_id=4)]
        super += [vendor_id]
        [RegisterNotification(super[0], j, desc,'Pull out leads from allocation', 1, camp, camp_alloc) for j in super]
        from client.utils import noti_via_mail
        noti = noti_via_mail(super,'Pull out leads from allocation', desc, mail_pull_out_leads)
    return JsonResponse(data)

# === Lead rejected list ===
@login_required
@is_superadmin
def lead_rejected_list(request):
    # Lead rejected list
    lead_reason_list = leads_rejected_reson.objects.filter()
    return render(request, 'lead/lead_rejected_list.html', {'lead_reason_list': lead_reason_list})

# === Lead rejected list ===
@login_required
@is_superadmin
def lead_rectify_list(request):
    # Lead rejected list
    lead_reason_list = Leads_Rectify_Reason.objects.filter()
    return render(request, 'lead/lead_rectify_reason.html', {'lead_reason_list': lead_reason_list})

# === Add lead reason successfully ===
def add_lead_reason(request):
    # Add lead reason successfully
    reason = request.POST.get('reason')
    user_id = request.session['userid']
    if request.session['usertype'] == 4:
        leads_rejected_reson.objects.create(
            reason=reason, is_active=1, status=0, user_id=user_id)
    elif request.session['usertype'] == 1:
        leads_rejected_reson.objects.create(
            reason=reason, is_active=1, status=1, user_id=user_id)
    data = {'success': 1, 'msg': 'Reason Added Successfully!..'}
    return JsonResponse(data)

# === Add rectify lead reason Successfully ===
def add_rectify_lead_reason(request):
    # Add rectify lead reason Successfully
    reason = request.POST.get('reason')
    user_id = request.session['userid']
    if request.session['usertype'] == 4:
        Leads_Rectify_Reason.objects.create(
            reason=reason, is_active=1, status=0, user_id=user_id)
    elif request.session['usertype'] == 1:
        Leads_Rectify_Reason.objects.create(
            reason=reason, is_active=1, status=1, user_id=user_id)
    data = {'success': 1, 'msg': 'Reason Added Successfully!..'}
    return JsonResponse(data)

# === Get rectify lead reason ===
def get_rectify_lead_reason(request):
    # Get rectify lead reason
    id = request.POST.get('id')
    lead_reason = Leads_Rectify_Reason.objects.get(id=id)
    data = {'success': 1, 'reason': lead_reason.reason}
    return JsonResponse(data)

# === Get lead reason ===
def get_lead_reason(request):
    # Get lead reason
    id = request.POST.get('id')
    flag = request.POST.get('flag')
    if flag == 'rectify_reason':
        lead_reason = Leads_Rectify_Reason.objects.get(id=id)
        data = {'success': 1, 'reason': lead_reason.reason}
    else:
        lead_reason = leads_rejected_reson.objects.get(id=id)
        data = {'success': 1, 'reason': lead_reason.reason}
    return JsonResponse(data)

# === Update rectify lead reason ===
def update_rectify_lead_reason(request):
    # Update rectify lead reason
    id = request.POST.get('id')
    reason = request.POST.get('reason')
    lead_reason = Leads_Rectify_Reason.objects.get(id=id)
    lead_reason.reason = reason
    lead_reason.save()
    data = {'success': 1, 'msg': 'Reason Updated Successfully!..'}
    return JsonResponse(data)

# === update lead reason ===
def update_lead_reason(request):
    # update lead reason
    id = request.POST.get('id')
    reason = request.POST.get('reason')
    lead_reason = leads_rejected_reson.objects.get(id=id)
    lead_reason.reason = reason
    lead_reason.save()
    data = {'success': 1, 'msg': 'Reason Updated Successfully!..'}
    return JsonResponse(data)

# === Delete rectify lead reason ===
def delete_rectify_lead_reason(request):
    # Delete rectify lead reason
    id = request.POST.get('id')
    data = {'success': 0, 'msg': 'Reason Not Deleted Successfully!..'}
    if Leads_Rectify_Reason.objects.get(id=id).delete():
        data = {'success': 1, 'msg': 'Reason Deleted Successfully!..'}
    return JsonResponse(data)

# === Delete lead reason ===
def delete_lead_reason(request):
    # Delete lead reason
    id = request.POST.get('id')
    data = {'success': 0, 'msg': 'Reason Not Deleted Successfully!..'}
    if leads_rejected_reson.objects.get(id=id).delete():
        data = {'success': 1, 'msg': 'Reason Deleted Successfully!..'}
    return JsonResponse(data)

# === Deactivate Rectify reason ===
def rectify_reason_deactive(request, reason_id):
    # Deactivate Rectify reason
    lead_reason = Leads_Rectify_Reason.objects.get(id=reason_id)
    lead_reason.is_active = 0
    lead_reason.save()
    if request.session['usertype'] == 4:
        return redirect('/superadmin/lead-rectify-list/')
    elif request.session['usertype'] == 1:
        return redirect('/client/Rectify-Reason-List/')

# === Deactivate reason ===
def reason_deactive(request, reason_id):
    # Deactivate reason
    lead_reason = leads_rejected_reson.objects.get(id=reason_id)
    lead_reason.is_active = 0
    lead_reason.save()
    if request.session['usertype'] == 4:
        return redirect('/superadmin/lead-rejected-list/')
    elif request.session['usertype'] == 1:
        return redirect('/client/Rejected-Reason-List/')

# === Activate  rectify reason ===
def rectify_reason_active(request, reason_id):
    # Activate  rectify reason
    lead_reason = Leads_Rectify_Reason.objects.get(id=reason_id)
    lead_reason.is_active = 1
    lead_reason.save()
    if request.session['usertype'] == 4:
        return redirect('/superadmin/lead-rectify-list/')
    elif request.session['usertype'] == 1:
        return redirect('/client/Rectify-Reason-List/')

# === Active Reason ===
def reason_active(request, reason_id):
    # Active Reason
    lead_reason = leads_rejected_reson.objects.get(id=reason_id)
    lead_reason.is_active = 1
    lead_reason.save()
    if request.session['usertype'] == 4:
        return redirect('/superadmin/lead-rejected-list/')
    elif request.session['usertype'] == 1:
        return redirect('/client/Rejected-Reason-List/')

# === Register Notificaton ===
def RegisterNotification(sender_id, receiver_id, desc, title, notification_type_id, campaign, camp_alloc):
    # Register Notificaton
    try:
        Notification.objects.create(title=title, description=desc, notification_type_id=notification_type_id,
                                    sender_id=sender_id, campaign_id=campaign, camp_alloc=camp_alloc)
    except:
        if camp_alloc:
            Notification.objects.create(title=title, description=desc,
                                        notification_type_id=notification_type_id, sender_id=sender_id, camp_alloc=camp_alloc)
        if campaign:
            Notification.objects.create(title=title, description=desc,
                                        notification_type_id=notification_type_id, sender_id=sender_id, campaign_id=campaign)
        else:
            Notification.objects.create(title=title, description=desc,
                                        notification_type_id=notification_type_id, sender_id=sender_id)
    Nlast_id = Notification.objects.latest('id')
    Notification_Reciever.objects.create(
        status='0', Notification_id=Nlast_id.id, receiver_id=receiver_id)


# === Campaign Chat Screen ===
@login_required
@is_superadmin
def campaign_chat_screen(request, camp_id):
    # if tc vendor then see campaign chats
    # Campaign Chat Screen
    chat_list = []
    camp = Campaign.objects.get(id=camp_id)
    camp_list = get_user_live_campaign(camp.user_id, camp)
    vendor_list = get_vendor_list_of_campaign(camp_id)
    from client.utils import fetch_default_logo
    for dict_ in vendor_list:
        query = client_vendor.objects.filter(user_id = dict_['id'])
        if query:
            # fetch logo for various users..Akshay G...Date - 22nd Oct, 2019
            company_Logo = return_logo(query[0], dict_['id'])
            dict_['logo'] = company_Logo
        else:
            dict_['logo'] = fetch_default_logo(dict_['id'])
    if Campaign_chats.objects.filter(campaign_id=camp_id).count() > 0:
        vendor_ = client_vendor.objects.get(user_id = camp.user_id)
        chats = Campaign_chats.objects.get(campaign_id=camp_id)
        chat_list = ast.literal_eval(chats.data)
        # fetch logo for various users..Akshay G...Date - 22nd Oct, 2019
        company_Logo = return_logo(vendor_, camp.user_id)
        client_data={'id':camp.user_id,'vendor_name':camp.user,'logo': company_Logo}
        vendor_list.append(client_data)
    return render(request, 'campaign_chat/superadmin_chat_screen.html', {'campaign_list': camp_list, 'sender_id': request.session['userid'], 'chat': chat_list, 'vendor_list': vendor_list, 'campaign': camp})

# return logo for various users..Akshay G...Date - 22nd Oct, 2019
def return_logo(vendor_, user_id):
    from client.utils import fetch_default_logo
    default_logo = fetch_default_logo(user_id)
    if vendor_.company_logo_smartthumbnail != '':    
        try:
            company_Logo = str(vendor_.company_logo_smartthumbnail.url)
            if company_Logo == '/media/':
                company_Logo = default_logo    
        except Exception as e:
            company_Logo = default_logo
    else:
        company_Logo = default_logo
    return company_Logo

# === Get user live Campaign ===
def get_user_live_campaign(userid, camp):
    # Get user live Campaign
    camp_id = []
    camp_list = []
    camp = Campaign.objects.filter(user_id=userid, status__in=[1,3,4,5])
    # camp_allc = campaign_allocation.objects.filter(
    #     campaign_id__in=camp, status=1)
    for row in camp:
        if row.id not in camp_id:
            camp_id.append(row.id)
            camp_list.append({
                'id': row.id,
                'name': row.name
            })
    return camp_list

# === Get vndor list of campaign  ===
def get_vendor_list_of_campaign(camp_id):
    # get distinct vendors details
    # Get vndor list of campaign
    vendor_id = []
    vendor_data = []
    vendor_list = campaign_allocation.objects.filter(
        campaign_id=camp_id, status__in=[1,3,4,5])
    for vendor in vendor_list:
        if vendor.client_vendor_id not in vendor_id:
            if vendor.client_vendor.usertype.id != 1:
                vendor_id.append(vendor.client_vendor_id)
                vendor_data.append({
                    'id': vendor.client_vendor_id,
                    'vendor_name': vendor.client_vendor
                })
    return vendor_data

# === sending message ===
def send_msg(request):
    # sending message
    ids = []
    media=""
    title=""
    image = ''
    doc = ''
    link = ''
    sender_name=request.session['username']
    msg1 = request.POST.get('message')
    msg = msg1.replace("<","&lt;");
        # print(msg)
    camp_id = int(request.POST.get('camp_id'))
    client = Campaign.objects.get(id=camp_id)
    receiver_ids = list(campaign_allocation.objects.filter(campaign_id=camp_id, status__in=[
                        1, 4, 3, 5]).values_list('client_vendor', flat=True).distinct())
    if len(Find(msg)) > 0:
        link = msg
    if len(request.POST.getlist('vendor_names')) == 0:
        ids.append(request.session['userid'])
        ids.append(client.user_id)
        ids.extend(receiver_ids)
        from client.utils import get_logos

        # involving superadmin to receiver -amey
        superadmins = [i.id for i in user.objects.filter(usertype_id=4)]
        ids.extend(superadmins)
        ids = list(set(ids))

        result = store_msg(request.session['userid'], ids, msg,camp_id,media,image,doc,link,title,sender_name)
        if result['status'] == 'true':
            data = {'success': 1,'time':result['time'],'profilepic':result['profilepic'],'msg':msg,'sender_name':result['sender_name'],'receiver_logos':[d for d in result['receiver_logos'] if d['id'] != request.session['userid']],'camp_id':camp_id,'receiver_ids':ids}
    else:
        ids = list(map(int, request.POST.getlist('vendor_names')))
        ids.append(request.session['userid'])
        ids.append(client.user_id)
        from client.utils import get_logos

         # involving superadmin to receiver -amey
        superadmins = [i.id for i in user.objects.filter(usertype_id=4)]
        ids.extend(superadmins)
        ids = list(set(ids))
        result = store_msg(request.session['userid'], ids, msg,camp_id,media,image,doc,link,title,sender_name)
        if result['status'] == 'true':
            data = {'success': 1,'time':result['time'],'profilepic':result['profilepic'],'msg':msg,'sender_name':result['sender_name'],'camp_id':camp_id,'receiver_logos':[d for d in result['receiver_logos'] if d['id'] != request.session['userid']],'receiver_ids':ids}
    return JsonResponse(data)

# === Send  Attchment ===
def send_attchment(request):
    #send attachment to users
    image = ''
    doc = ''
    link = ''
    if request.FILES:
        sender_id=request.session['userid']
        sender_name=request.session['username']
        camp_id = request.POST.get('camp_id')
        title=request.POST.get('title')
        msg="0"
        receiver_ids=request.POST.getlist('ids[]')
        receiver_ids.append(sender_id)
        receiver_ids = list(map(int, receiver_ids))

        # involving superadmin to receiver -amey raje
        superadmins = [i.id for i in user.objects.filter(usertype_id=4)]
        receiver_ids.extend(superadmins)
        receiver_ids = list(set(receiver_ids))

        myfile = request.FILES['filename']
        if myfile.name.split('.')[-1].lower() in ['jpg','png','jpeg','pdf','docx','doc','txt','csv','xlsx','xls']:
            fs = FileSystemStorage()
            filename = fs.save("chat_data/" + myfile.name,  myfile)
            media='/media/'+filename
            if myfile.name.split('.')[-1].lower() in ['pdf','doc','docx','txt','csv','xlsx','xls']:
                doc = '/media/'+filename
            if myfile.name.split('.')[-1].lower() in ['jpg','png','jpeg']:
                image = '/media/'+filename
            store_msg(sender_id,receiver_ids,msg,camp_id,media,image,doc,link,title,sender_name)
            data = {'status': 1}
        else:
            data = {'status': 2,'msg':"Only 'jpg','png','jpeg','pdf','docx','doc','txt','csv','xlsx','xls' formats are allowed. "}
    return JsonResponse(data)

# === Store Message ===
def store_msg(sender_id,receiver_ids,msg,camp_id,media,image,doc,link,title,sender_name):
    # Store Message
    last_dict = {}
    list_count = 0
    time = datetime.now().strftime('%H:%M:%S')
    from client.utils import fetch_default_logo
    profilepic = fetch_default_logo(sender_id)
    if Campaign_chats.objects.filter(campaign_id=camp_id).count() == 1:
        chats_details = Campaign_chats.objects.get(campaign_id=int(camp_id))
        list = ast.literal_eval(chats_details.data)
        last_dict = list[-1]
        from client.utils import get_logos
        # suryakant 05-10-2019
        # collect recivers profile icons for chat.
        receiver_logos = get_logos(receiver_ids)
        list.append({'sender_id': sender_id, 'sender_name':sender_name, 'receiver_ids': receiver_ids, 'receiver_logos':receiver_logos,'profilepic': profilepic,
                     'message': msg,'media':media,'image':image,'doc':doc,'link':link,'title':title, 'time': time, 'date': str(datetime.today().date()), 'rank': int(last_dict['rank'])+1})
        list_count = len(list)
        chats_details.data = list
        # update sent message count for receiver user ...Akshay G.
        if chats_details.send_msg_count != None:
            d = eval(chats_details.send_msg_count)
            for id in receiver_ids:
                if id != sender_id:
                    if id in d:
                        if sender_id in d[id]:
                            d[id][sender_id][0] += 1
                            d[id][sender_id].append(int(last_dict['rank'])+1)
                        else:
                            d[id][sender_id] = [1, int(last_dict['rank'])+1]
                        d[id]['count'] += 1
                    else:
                        d[id] = {sender_id: [1, int(last_dict['rank'])+1]}
                        d[id]['count'] = 1
            chats_details.send_msg_count = d
        else:
            d = {}
            for id in receiver_ids:
                if id != sender_id:
                    d[id] = {sender_id: [1,int(last_dict['rank'])+1], 'count': 1}
            chats_details.send_msg_count = d
        chats_details.save()
    else:
        list = []
        # suryakant 05-10-2019
        # collect recivers profile icons for chat.
        from client.utils import get_logos
        receiver_logos = get_logos(receiver_ids)
        list.append({'sender_id': sender_id, 'sender_name':sender_name, 'receiver_ids': receiver_ids,'receiver_logos':receiver_logos, 'profilepic': profilepic,
                     'message': msg, 'time': time, 'media':media,'image':image,'doc':doc,'link':link,'title':title,'date': str(datetime.today().date()), 'rank': 1})
        d = {}
        for id in receiver_ids:
            if id != sender_id:
                d[id] = {sender_id: [1, 1], 'count': 1}
        Campaign_chats.objects.create(campaign_id=camp_id, data=list, send_msg_count = d)
    send_campaign_notification(receiver_ids, sender_id, camp_id, list_count)
    data = {'status':'true','time':time,'profilepic':profilepic,'receiver_logos': receiver_logos,'sender_name':sender_name}
    return data

# === Send Campaign Notification ===
def send_campaign_notification(receiver_ids, sender_id, camp_id, list_count):
    # Send Campaign Notification
    data = []
    receiver_ids.remove(sender_id)
    user_data = user_activities.objects.filter(user_id__in=receiver_ids)
    for row1 in user_data:
        if row1.active_page != 'chat':
            if Campaign_notification.objects.filter(campaign_id=camp_id).count() > 0:
                notification = Campaign_notification.objects.get(
                    campaign_id=camp_id)
                list = ast.literal_eval(notification.data)
                userid = create_userid_list(list)
                if row1.user_id in userid:
                    for row in list:
                        row['all_msg_cnt'] = list_count
                        row['start'] = row['end']
                        row['end'] = list_count
                        row['check'] = 0
                        row['read'] = 0
                else:
                    list.append({'user_id': row1.user_id, 'all_msg_cnt': list_count,
                                 'start': 0, 'end': list_count, 'check': 0, 'read': 0})
                    notification.data = list
                    notification.save()
            else:
                data.append({'user_id': row1.user_id, 'all_msg_cnt': list_count,
                             'start': 0, 'end': list_count, 'check': 0, 'read': 0})
                Campaign_notification.objects.create(
                    campaign_id=camp_id, data=data)

# === Find ===
def Find(string):
    # findall() has been used
    # with valid conditions for urls in string
    import re
    url = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', string)
    return url

# === Create user id list ===
def create_userid_list(list):
    # Create user id list
    userid = []
    for row in list:
        userid.append(row['user_id'])
    return userid

# === Get counter CPL ===
def get_counter_cpl(request):
    # Get counter CPL
    vendor_id = request.POST.get('vendor_id')
    camp_alloc_id = request.POST.get('camp_alloc_id')
    if Counter_cpl.objects.filter(user_id=vendor_id, campaign_allocation_id=camp_alloc_id).count() == 1:
        counter = Counter_cpl.objects.get(user_id=vendor_id, campaign_allocation_id=camp_alloc_id)
        rev_share = ClientCustomFields.objects.get(client_id=request.session['userid'])
        share = rev_share.rfq_rev_share if counter.campaign.rfq == True else rev_share.normal_rev_share
        cpl = counter.req_cpl + (counter.req_cpl *share/100)
        data = {'success': 1, 'cpl': cpl,'volume':counter.volume,'comments':counter.comment}
    else:
        data = {'success': 2}
    return JsonResponse(data)

# === Counter Action On CPL ===
def counter_action_on_cpl(request):
    # Counter Action On CPL
    vendor_id = request.POST.get('vendor_id')
    camp_alloc_id = request.POST.get('camp_alloc_id')
    if Counter_cpl.objects.filter(user_id=vendor_id, campaign_allocation_id=camp_alloc_id).count() == 1:
        counter = Counter_cpl.objects.get(user_id=vendor_id, campaign_allocation_id=camp_alloc_id)
        id = request.POST.get('id')
        userid = request.session['userid']
        cpl = cpl_decrease(float(request.POST.get('cpl')),counter.req_cpl)
        if id == '1':
            counter_action_accept_vendor(camp_alloc_id, vendor_id, userid, cpl)
        elif id == '2':
            counter_action_reject_vendor(camp_alloc_id, vendor_id, userid)
    data = {'success': 1, 'msg': 'Action Submitted Successfully!...'}
    return JsonResponse(data)

# === Counter action accept vendor ===
def counter_action_accept_vendor(camp_alloc_id, vendor_id, userid, cpl):
    # Counter action accept vendor
    camp = campaign_allocation.objects.get(id=camp_alloc_id)
    name = camp.campaign.name
    camp.cpl = cpl
    camp.counter_status = 2
    camp.save()
    title = "Accept Counter by Superadmin"
    desc = "Accept CPL Counter on " + str(name)
    # from client.utils import noti_via_mail
    # noti_via_mail(vendor_id,title, desc, mail_noti_rfq_cpl_req_by_superadmin)
    RegisterNotification(userid, vendor_id, desc, title, 1, None, camp)
    return True

# === Reject counter by client ===
def counter_action_reject_vendor(camp_alloc_id, vendor_id, userid):
    # Reject counter by client
    camp = campaign_allocation.objects.get(id=camp_alloc_id)
    name = camp.campaign.name
    camp.counter_status = 2
    camp.save()
    title = "Reject Counter by Superadmin"
    desc = "Reject CPL Counter on " + str(name)
    # from client.utils import noti_via_mail
    # noti_via_mail(vendor_id, title, desc, mail_noti_rfq_cpl_req_by_superadmin)
    RegisterNotification(userid, vendor_id, desc, title, 1, None, camp)
    return True

# === Superadmin campaign notebook ===
@login_required
@is_superadmin
def superadmin_campaign_notebook(request):
    # Superadmin campaign notebook
    counter = Campaign.objects.all()
    if counter:
        return render(request, 'campaign/superadmin_campaign_notebook.html', {'camps': counter})
    else:
        return render(request, 'campaign/superadmin_campaign_notebook.html', {})

# === returs leads orginal headers ===
def get_lead_header(camp_id):
    # returs leads orginal headers
    lead_details = Delivery.objects.get(campaign_id=camp_id)
    labels = lead_details.data_header
    labels = list(labels.split(','))
    return labels

# === CPL Lead Allocation ===
def rfq_cpl_LeadAllocation(request):
    # CPL Lead Allocation
    vendor_id = request.POST.get('vendor_id')
    camp_id = request.POST.get('camp_id', None)
    userid = request.session['userid']
    campaign = Campaign.objects.get(id=camp_id)
    campaign.raimainingleads = int(request.POST.get('remaininglead'))
    campaign.save()
    camp_alloc = campaign_allocation.objects.filter(client_vendor_id=vendor_id, campaign_id=camp_id, status=3)
    if camp_alloc:
        camp_alloc[0].rfqcpl = camp_alloc[0].rfqcpl - (cpl_increase(camp_alloc[0].rfqcpl)-float(request.POST.get('vendorcpl')))
        camp_alloc[0].cpl = camp_alloc[0].rfqcpl
        camp_alloc[0].volume = request.POST.get('vendorlead')
        camp_alloc[0].status = 3
        camp_alloc[0].save()
    title = "New Campaign Allocated by"
    desc = "Techconnectr"
    # from client.utils import noti_via_mail
    # noti_via_mail(vendor_id,title, desc, mail_noti_new_campaign)
    RegisterNotification(userid, vendor_id, desc, title, 2, None,camp_alloc[0])
    data = {'success': '1'}
    # Campaign_commission.objects.create(client_vendor_id=vendor_id, campaign_id=camp_id,
    #     ccpl=request.POST.get('clientcpl'), lead=request.POST.get('vendorlead'),
    #     rev_share_percentage=request.POST.get('rev_ref'),rev_diff=request.POST.get('rev_diff') ,vcpl=request.POST.get('vcpl'))
    Campaign_commission.objects.create(client_vendor_id=vendor_id, campaign_id=camp_id, ccpl=request.POST.get(
        'admincpl'), lead=request.POST.get('vendorlead'),commision_mode=1)
    return JsonResponse(data)

# === CPL Lead Allocation ===
def rfq_cpl_LeadAllocation_external(request):
    # CPL Lead Allocation
    vendor_id = int(request.POST.get('vendor_id'))
    camp_id = request.POST.get('camp_id', None)
    userid = int(request.session['userid'])
    campaign = Campaign.objects.get(id=camp_id)
    camp_alloc = campaign_allocation.objects.filter(client_vendor_id=vendor_id, campaign_id=camp_id, status=3)
    if camp_alloc:
        camp_alloc[0].cpl = request.POST.get('vendorcpl')
        camp_alloc[0].volume = request.POST.get('vendorlead')
        camp_alloc[0].status = 3
        camp_alloc[0].save()
        title = "New Campaign Allocated by"
        desc = "Techconnectr"
        RegisterNotification(userid, vendor_id, desc, title, 2, None,camp_alloc[0])
    else:
        # check user is client : abhi 4 June 2019
        if vendor_id == userid:

            #if user is client then create one more row for client & that allocation direct goes to live.
            campaign_allocation.objects.create(client_vendor_id=vendor_id, campaign_id=camp_id, cpl=request.POST.get(
                 'vendorcpl'), volume=request.POST.get('vendorlead'), status=1)
            campaign.approveleads = int(campaign.approveleads) + int(request.POST.get('vendorlead'))
            if campaign.target_quantity == campaign.approveleads:
                campaign.status = 5
                campaign.save()
    campaign.raimainingleads = int(request.POST.get('remaininglead'))
    campaign.save()
    return JsonResponse({'success': '1'})

# === view for showing type of notication ===
def super_noti(request):
    ''' view for showing type of notication according to user type- by Amey Raje '''
    mail = MailNotification.objects.all().order_by('-id')
    access = []
    user_instance = user.objects.get(id=request.session['userid'])
    type = usertype.objects.get(id=request.session['usertype'])
    for m in mail:
        # check whether usertype is already subscribed for notification
        if type in m.usertype.all():
            if user_instance in m.user.all():
                access.append({
                    'id': m.id,
                    'access': m.noti_field,
                    'value': 1,
                    'parent': m.parent,
                })
            else:
                access.append({
                    'id': m.id,
                    'access': m.noti_field,
                    'value': 0,
                    'parent': m.parent,
                })
    return render(request, 'campaign/super_noti.html', {'mail': access})

# === Cancel Assigned Leads ===
def cancel_assigned_leads(request):
    ''' superadmin cancelsCancele assigned more leads - by Amey Raje'''
    if request.method == 'POST':

        id = request.POST.get('id')
        more_status = request.POST.get('more_status')

        if more_status == '1':
            more_alloc = More_allocation.objects.get(id=id)
            campaign = Campaign.objects.get(id=more_alloc.campaign_id)
            campaign.raimainingleads += int(more_alloc.volume)
            campaign.save()
            vendor = more_alloc.client_vendor.user_name
            more_alloc.delete()

            from client.utils import noti_via_mail
            super = [more_alloc.client_vendor.id]
            noti_via_mail(super, 'Cancelled Assigned Leads', 'Client unassigned '+ vendor + ' from '+campaign.name+ ' due to inactivity', mail_cancel_assigned_leads)
            desc = "Client has cancelled allocation for campaign '"+campaign.name+"'"
            [RegisterNotification(campaign.user.id, j, desc,'Cancelled Allocation', 1, campaign, None) for j in super]
            data = {'success': 1}
            return JsonResponse(data)
        else:
            camp_al = campaign_allocation.objects.get(id=id)
            vendor_name = camp_al.client_vendor.user_name
            campaign = Campaign.objects.get(id=camp_al.campaign.id)
            campaign.raimainingleads += int(camp_al.volume)
            campaign.save()
            camp_al.delete()

            from client.utils import noti_via_mail
            super = [camp_al.client_vendor.id]
            noti_via_mail(super, 'Cancelled Allocation', 'Techconnectr Unassigned '+vendor_name + ' from '+campaign.name+' due to inactivity', mail_cancel_assigned_leads)
            data = {'success': 1}
            desc = "Client has cancelled allocation for campaign '"+campaign.name+"'"
            [RegisterNotification(campaign.user.id, j, desc,'Cancelled Allocation', 1, campaign, None) for j in super]
            return JsonResponse(data)
    data = {'success': 0}
    return JsonResponse(data)

# === Percentage ===
def percentage(camp_id):
    '''calculate campaign completion based on leads approved - by Amey Raje'''
    campaign = campaign_allocation.objects.filter(campaign_id=camp_id)
    percentage_val = 0
    for camp in campaign:
        if camp.approve_leads is not None and camp.campaign.target_quantity is not None:
            percentage_val = percentage_val + (camp.approve_leads/camp.campaign.target_quantity)*100
    return int(percentage_val)

# === User Tracking ===
def user_tracking(request):
    # return all users activity
    sorted_list = user_activity(request)
    html = render_to_response('dashboard/activity_html.html', {'data': sorted_list})
    return html

# === Campaign Shares ===
def campaign_shares(request):
    # Add RFQ Share And Normal Share camapaign on in superadmin onBoarding Form ...Kishor
    obj,create = ClientCustomFields.objects.get_or_create(client_id=request.POST.get('user_id'))
    if request.session['usertype'] == 1:
        obj.rfq_rev_share = request.POST.get('share1')
        obj.normal_rev_share = request.POST.get('share2')
    obj.tier_level = request.POST.get('tier')
    obj.save()
    data = {'success': 1}
    return JsonResponse(data)

# === Vendor Agreement ===
def vendor_agreement(vendor_id, client_id, camp_alloc):
    # /
    # saved agreements to be signed by vendor for the given campaign in camp_allocation ..Akshay G.
    # also check whether vendor has worked with this client before
    query = Vendor_agreement_status.objects.filter(client_id = client_id, vendor_id = vendor_id)
    vendor_agr_data = []
    queryset = ClientCustomFields.objects.filter(client_id = client_id)
    new_agreement_data = []
    old_agreement_data = []
    new_vendor_agr_data = []
    old_vendor_agr_data = []
    if queryset:
        if queryset[0].client_agreements:
            if queryset[0].client_agreements.strip() != '':
                data = ast.literal_eval(queryset[0].client_agreements)[0]
                new_agreement_data = data['new_vendor_agreements']
                old_agreement_data = data['old_vendor_agreements']
    if len(new_agreement_data) > 0:
        new_vendor_agr_data = [dict.fromkeys(new_agreement_data.keys(), '')]
    if len(old_agreement_data) > 0:
        old_vendor_agr_data = [dict.fromkeys(old_agreement_data.keys(), '')]
    data = [*new_vendor_agr_data, *old_vendor_agr_data]
    if len(query) == 0:
        Vendor_agreement_status.objects.create(client_id = client_id, vendor_id = vendor_id, agreements = new_vendor_agr_data)
        camp_alloc.vendor_agreements = data
    else:
        query[0].agreements = new_vendor_agr_data
        query[0].save()
        if query[0].status:
            camp_alloc.vendor_agreements = old_vendor_agr_data
        else:
            camp_alloc.vendor_agreements = data
    camp_alloc.save()

@login_required
@is_superadmin
def rfq_campaigns(request):
    # show client's rfq campaign list to superadmin...Akshay G...4th Nov, 2019
    rfq_camps = RFQ_Campaigns.objects.all()
    status_list = {}
    for camp in rfq_camps:
        if RFQ_campaign_quotes.objects.filter(campaign_id=camp.id).exists():
            wait_for_req = 1
            WFR_count = RFQ_campaign_quotes.objects.filter(campaign_id=camp.id).count()
            RCMP = [ d  for d in RFQ_campaign_quotes.objects.filter(campaign_id=camp.id).values_list('cpl',flat = True) if d != 0 ]
            wait_for_quote = 1 if RCMP != [] else 0
            WFQ_count = len(RCMP)
            status_list[camp.id] = {'wait_for_quote':wait_for_quote,'WFQ_count':WFQ_count,'WFR_count':WFR_count,'wait_for_req':wait_for_req}
        else:
            wait_for_quote = wait_for_req = WFQ_count = WFR_count = 0
            status_list[camp.id] = {'wait_for_quote':wait_for_quote,'WFQ_count':WFQ_count,'WFR_count':WFR_count,'wait_for_req':wait_for_req}
    return render(request, 'campaign/RFQ_campaign_list.html', {'rfq_camps': rfq_camps,"status_list":status_list})
    
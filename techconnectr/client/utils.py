import ast
from datetime import datetime

from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from campaign.models import *
from client.models import *
from form_builder.models import *
from setupdata.models import (MailNotification, Notification,
                              Notification_Reciever)
from setupdata.models import countries1 as countries_list
from setupdata.models import data_header, notification_type
from superadmin.models import *
from superadmin.views.views import topVendorList
from user.models import user
from vendors.models import *


def check_all_data_available_in_post(data, post):
    """ check all data available in post request """
    missing_parameters = []

    if data:
        for parameter in data:
            # check is exist in post
            if parameter not in post:
                missing_parameters.append(parameter)

        if missing_parameters:
            success = False
            message = "Missing parameters in request are : " + \
                (', '.join(missing_parameters))
        else:
            success = True
            message = "All parameters are available in post"
    else:
        success = False
        message = "No data supplied to check in post"

    response = {
        "success": success,
        "message": message,
    }
    return response


def saving_assets(request, assets):
    ''' saaving assets into data base '''
    asset_dict = {}
    file_list = []
    files = request.FILES
    if files:
        for asset in assets.split(','):
            if asset in request.FILES:
                for file_name in request.FILES.getlist(asset):
                    file_list.append(save_asset_file(file_name))
                    if request.POST.get(asset):
                        links = request.POST.getlist(asset)
                        links.remove('') if ('') in links else links
                        asset_dict[asset] = {'files':file_list,'link': links}
                    else:
                        asset_dict[asset] = {'files':file_list}
    for asset in assets.split(","):
        if request.POST.get(asset):
            links = request.POST.getlist(asset)
            links.remove('') if ('') in links else links
            if asset not in asset_dict:
                asset_dict[asset] = {'link': links}
            else:
                asset_dict[asset].update({'link': links})

    return asset_dict

def save_asset_file(file_name):
    import os
    myfile = file_name
    fs = FileSystemStorage()
    filename = fs.save('campaign_assets/'+myfile.name, myfile)
    uploaded_file_url = fs.url(filename)
    # get filename using basename property....Akshay G...Date - 13th Nov, 2019
    return {'filename': os.path.basename(uploaded_file_url), 'url': uploaded_file_url}

def update_assets(request, assets, assets_type):
    """ update assets """
    files = request.FILES
    if files:
        for asset in assets.split(','):
            if asset in request.FILES:
                for file_name in request.FILES.getlist(asset):
                    file_list = []
                    file_list.append(save_asset_file(file_name))
                    if asset in assets_type:
                        if 'files' in assets_type[asset]:
                            newlist = assets_type[asset]['files']+file_list
                            assets_type[asset].update({'files':newlist})
                        else:
                            assets_type[asset].update({'files': file_list})
                    else:
                        if request.POST.get(asset):
                            links = request.POST.getlist(asset)
                            links.remove('') if ('') in links else links
                            assets_type[asset] = {'files':file_list,'link': links}
                        else:
                            assets_type[asset] = {'files':file_list}
    for asset in assets.split(","):
        if request.POST.get(asset):
            links = request.POST.getlist(asset)
            links.remove('') if ('') in links else links
            if asset in assets_type:
                if 'link' in assets_type[asset]:
                    newlist = assets_type[asset]['link']+links
                    assets_type[asset].update({'link': newlist})
                else:
                    assets_type[asset].update({'link': links})
            else:
                assets_type[asset] = {'link': links}

    return assets_type


def client_top_vendor(userid):
    """ retrn client top vendors working on campaigns """
    vendordetail = []
    cpl = 0
    volume = 0
    camps = Campaign.objects.filter(user_id=userid)
    for row in camps:
        campaignallocationdetails = campaign_allocation.objects.filter(
            campaign_id=row.id, status__in=[1, 4])
        for row1 in campaignallocationdetails:
            number_of_campaign = 1
            revenue = 0
            if row1.status == 1:
                cpl += float(row1.cpl) if row1.cpl else 0
                volume += row1.volume if row1.volume else 0
                revenue += float(cpl * volume) if row1.cpl and row1.volume else 0
            else:
                cpl += float(row1.client_cpl) if row1.client_cpl else 0
                volume += row1.old_volume if row1.old_volume else 0
                revenue += float(cpl * volume) if row1.client_cpl and row1.old_volume else 0

            if client_vendor.objects.filter(user_id=row1.client_vendor_id, user__usertype_id=2).count() == 1:
                clientvendordetail = client_vendor.objects.get(
                    user_id=row1.client_vendor_id)
                country = countries_list.objects.filter(
                    id=clientvendordetail.user.country)
                # country = countries.objects.filter(
                #     id=clientvendordetail.user.country)
                speciality = clientvendordetail.industry_speciality.all()
                for i in vendordetail:
                    if i['id'] == clientvendordetail.user.id:
                        number_of_campaign += 1
                        cpl += i['CPL']
                        volume += i['leads']
                        revenue += i['revenue']
                        vendordetail.remove(i)
                vendordetail.append({
                    "id": clientvendordetail.user.id,
                    "vendorname": clientvendordetail.user.user_name,
                    "number_of_camp": number_of_campaign,
                    "lead": clientvendordetail.lead_per_month,
                    "speciality": speciality if speciality else '',
                    "revenue": revenue,
                    "CPL": cpl,
                    "leads": volume,
                })
        number_of_campaign = 1
        revenue = 0
        cpl = 0
        volume = 0
    # L = []
    # for i in vendordetail:
    #     for j in vendordetail:
    #         if vendordetail.index(i) == vendordetail.index(j):
    #             pass
    #         elif i['id'] == j['id']:
    #             L.append(vendordetail.index(j))
    #             i["revenue"] += j["revenue"]
    # L1 = []
    # for i in L:
    #     if i not in L1:
    #         L1.append(i)
    #
    # for i in L:
    #     del vendordetail[i]
    return vendordetail


def tc_top_vendors_types():
    """ TC top vendors """
    vendor = user.objects.filter(usertype_id=2)
    topvendor = topVendorList(vendor)
    for tc_vendor in topvendor:
        if client_vendor.objects.filter(user_id=tc_vendor['id']).count() == 1:
            vendor_details = data_assesment.objects.get(
                user_id=tc_vendor['id'])
            if vendor_details.vendor_type:
                for cvid in ast.literal_eval(vendor_details.vendor_type):
                    v_type = VendorType.objects.get(id=int(cvid))
                    if 'vendor_type' not in tc_vendor.keys():
                        tc_vendor.update({'vendor_type': [v_type.type]})
                    else:
                        tc_vendor['vendor_type'].append(v_type.type)
    return topvendor


def RegisterNotification(sender_id, receiver_id, desc, title, notification_type_id, campaign, camp_alloc):
    """ raise notifications for venodrs and superadmins """
    try:
        Notification.objects.create(title=title, description=desc, notification_type_id=notification_type_id,
                                    sender_id=sender_id, campaign_id=campaign, camp_alloc=camp_alloc)
    except:
        if camp_alloc:
            Notification.objects.create(title=title, description=desc,
                                        notification_type_id=notification_type_id, sender_id=sender_id, camp_alloc=camp_alloc)
        if campaign:
            Notification.objects.create(title=title, description=desc,
                                        notification_type_id=notification_type_id, sender_id=sender_id, campaign_id=campaign)

    Nlast_id = Notification.objects.latest('id')
    Notification_Reciever.objects.create(
        status='0', Notification_id=Nlast_id.id, receiver_id=receiver_id)


def get_external_vendors(user_id):
    vendor_list_id = []
    user_obj = user.objects.get(id=user_id).child_user.all()
    vendor_list = [i for i in user_obj if i.usertype_id == 5]
    return vendor_list

def get_internal_vendors(user_id):
    vendor_list_id = []
    user_obj = user.objects.get(id=user_id).child_user.all()
    vendor_list = [i for i in user_obj if i.usertype_id == 9]
    return vendor_list



def check_is_logged_in(request):
    """ logout when session get expire  """
    message, success = "", False
    if request:
        if request.session:
            if 'userid' in request.session:
                message = "User_id available in session"
                success = True
            else:
                success = False
                message = "Error ... User_id not exists inside request.session so calling /logout/ url"
        else:
            message += "Session not exists for current request"
    else:
        message = "Request is not valid"

    response = {
        "success": success,
        "message": message,
    }
    return response


def grand_child_access_call(sub_menu, access_id, usergroup, data, access):
    ''' grant grand child menu access of sub menu '''
    grand_child = User_Configuration.objects.filter(parent_id=sub_menu.id)
    for grand_menu in grand_child:
        if str(grand_menu.id) in data:
            if usergroup not in grand_menu.group.all():
                grand_menu.group.add(usergroup)
            else:
                grand_menu.group.remove(usergroup)
            grand_menu.save()
    # for sub menu
    if User_Configuration.objects.filter(parent_id=sub_menu.id, group__in=[usergroup]).count() == 0:
        sub_menu.group.remove(usergroup)
    else:
        sub_menu.group.add(usergroup)
    # # form main menu
    # if User_Configuration.objects.filter(parent_id=access_id,group__in=[usergroup]).count() == 0:
    #     access.group.remove(usergroup)
    # else:
    #     access.group.add(usergroup)
    # access.save()
    return True



def email_domain_check(email):
    """ to check email is only bussiness domain """
    email_list = HeadersValidation.objects.all()
    email_splited = email.split('@')
    if email_splited[1] in eval(email_list[0].email_list):
        data = {'status':0,'msg':"Only Bussiness Domains are allowed."}
    else:
        data = {'status':1,'msg':"Successful"}
    return data


def percentage(camp_id):
    '''calculate campaign completion based on leads approved -Amey Raje'''
    campaign = campaign_allocation.objects.filter(campaign_id=camp_id)
    percentage_val = 0
    for camp in campaign:
        if camp.approve_leads is not None and camp.campaign.target_quantity is not None:
            percentage_val = percentage_val + (camp.approve_leads/camp.campaign.target_quantity)*100
    return int(percentage_val)


def noti_via_mail(userid, subject, description, type):
    '''send mail for notifications - by Amey Raje'''
    user_list = user.objects.filter(id__in=userid)
    final_users = []
    for recvr in user_list:
        # suryakant 16-10-2019 # refactored code
        if MailNotification.objects.filter(id=type,user__id=recvr.id).exists():
            final_users.append(recvr.email)
            print(recvr)
    # changes to make program execution fast - amey -18 June 2019
    html_message = render_to_string('email_templates/email_notification.html', {'subject': subject, 'message': description})
    plain_message = strip_tags(html_message)
    if len(final_users) > 0:
        from_email = settings.EMAIL_HOST_USER
        send_mail(
            subject,
            plain_message,
            from_email,
            final_users,
            fail_silently=True,
            html_message=html_message,
        )
        print('sent')
    return 0


def update_header_mapping_data(mapped_list,userid):
    """ update newly mapped header for particular client"""
    data = []
    c_header = []
    newCMH = ClientCustomFields.objects.get_or_create(client_id=userid)[0]
    for CMH in data_header.objects.all():
        if CMH.custom_headers != None and len(CMH.custom_headers) > 0:
            data += eval(CMH.custom_headers)
    if newCMH.custom_header != '' and newCMH.custom_header != None:
        c_header = collect_client_headers(newCMH.custom_header)
        data += c_header
    for header_dict in mapped_list:
        if 'default_header' in header_dict.keys():
            new = []
            print(header_dict['user_header'])
            if header_dict['user_header'] not in data and header_dict['default_header'] != '':
                if newCMH.custom_header != '' and newCMH.custom_header != None:
                    mdata = eval(newCMH.custom_header) if isinstance(newCMH.custom_header, str) else newCMH.custom_header
                    if header_dict['default_header'] not in [d['default_header'] for d in mdata if 'default_header' in d.keys()]:
                        new.append({'user_header': [header_dict['user_header']], 'default_header': header_dict['default_header']})
                        new += mdata
                        newCMH.custom_header = new
                    else:
                        for element in mdata:
                            if element['default_header'] == header_dict['default_header']:
                                element['user_header'].append(header_dict['user_header'])
                            new.append(element)
                        newCMH.custom_header = new
                        newCMH.save()
                else:
                    new.append({'user_header': [header_dict['user_header']], 'default_header': header_dict['default_header']})
                    newCMH.custom_header = new                
                newCMH.save()
            else:
                if header_dict['user_header'] in c_header and c_header != []:
                    mdata = eval(newCMH.custom_header) if isinstance(newCMH.custom_header, str) else newCMH.custom_header
                    for element in mdata:
                        if element['default_header'] != header_dict['default_header'] and header_dict['user_header'] in element['user_header']:
                            element['user_header'].append(header_dict['default_header'])
                        new.append(element)
                    newCMH.custom_header = new
                elif newCMH.custom_header == '' and newCMH.custom_header == None:
                    new.append({'user_header': [header_dict['user_header']], 'default_header': header_dict['default_header']})
                    newCMH.custom_header = new
                newCMH.save()

    pass

def collect_client_headers(custom_header):
    data = []
    if custom_header != '' and custom_header != None:
        for d in eval(custom_header):
            for c in d['user_header']:
                data.append(c)

    return data

def remove_custom_lead_headers(leads,camp_id):
    # suryakant 09-10-2019
    # remove custom lead headers and add mandate headers to lead
    updated_leads = []
    labels = []
    camp_lead=Delivery.objects.get(campaign_id=camp_id)
    if camp_lead.custom_header_status == 0 :
        labels = camp_lead.data_header
        labels = list(labels.split(','))
    else:
        labels = camp_lead.custom_header
        labels = labels.split(',')

    if Question_forms.objects.filter(campaign_id=camp_id).count() > 0:
        cust_header=Question_forms.objects.get(campaign_id=camp_id)
        for i in range(cust_header.question_numbers):
            labels.append('Q'+str(i+1))    
    labels += ['id','time','date','status','TC_lead_status']
    for lead in leads:
        updated_leads.append({k: lead[k] for k in lead.keys() if k in labels})
    return updated_leads
    # if Campaign_custom_field.objects.filter(campaign_id=camp_id).exists():
    #     fields = Campaign_custom_field.objects.get(campaign_id=camp_id).internal_QA_headers
    #     dictKey = [d.keys() for d in eval(fields).values()]
    #     for lead in leads:
    #         if "lead_dict" in  lead.keys():
    #             for header in [e for e in dictKey[0]]:
    #                 del lead[header]
    #             del lead['lead_dict']
    #             changed = lead
    #             updated_leads.append(changed)
    #         else:
    #             updated_leads.append(lead)
    #     return updated_leads
    # else:
    #     return leads

def get_logos(ids):
    # suryakant 05-10-2019
    # return list of dict with user id,name and logos
    import os
    logo_list = []
    for id in ids:
        user_details=client_vendor.objects.filter(user_id=id)
        if user_details.count() > 0:
            profilepic = fetch_default_logo(id)
            if user_details[0].company_logo:
                try:
                    profilepic = str(user_details[0].company_logo_smartthumbnail.url)
                except Exception as e:
                    pass
            logo_list.append({'id':user_details[0].user.id,'name':user_details[0].user.user_name,'logo':profilepic})        
        else:
            users=user.objects.get(id=id)
            # suryakant 08-10-2019
            # sending first letter of name if profile pic is not present
            # profilepic = os.path.join(settings.BASE_URL, 'media', 'default.jpg')
            profilepic = fetch_default_logo(id)
            logo_list.append({'id':users.id,'name':users.user_name,'logo':profilepic})         
    return logo_list

# validate rfq timer for weekends..Akshay G.
def validate_weekends(datetime_object, Rfq_Timer):
    import datetime
    if datetime_object.weekday() >= 5:
        day_index = datetime_object.weekday()
        # if it's saturday
        if day_index == 5:
            Rfq_Timer = datetime_object + datetime.timedelta(hours = 48)
        # if it's sunday
        elif day_index == 6:
            Rfq_Timer = datetime_object + datetime.timedelta(hours = 24)
    return Rfq_Timer.strftime("%Y-%m-%d %H:%M:%S")

# fetch and return default logo for user...Akshay G...Date - 23-10-2019
def fetch_default_logo(user_id):
    username = user.objects.get(id  = user_id).user_name
    import re, os
    if username is not None:
        s = re.search(r'[a-zA-Z]+', username)
    else:
        s = None
    if s is not None:
        alpha_char = s.group()
        query = User_default_logo.objects.filter(title = alpha_char[0].lower())
        if query:
            company_logo = os.path.join(settings.BASE_URL, 'media', query[0].logo.name)
        else:
            company_logo = os.path.join(settings.BASE_URL, 'media', 'default.jpg')
    else:
        company_logo = os.path.join(settings.BASE_URL, 'media', 'default.jpg')
    return company_logo
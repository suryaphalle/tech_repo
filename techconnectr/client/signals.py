from client.models import Campaign, campaign_allocation, CampaignTrack
from django.db.models.signals import post_save
from django.dispatch import receiver
import datetime
from client.utils import percentage
from vendors.utils import *
from django.contrib.admin.models import LogEntry

@receiver(post_save, sender=Campaign)
def campaign_change(sender, instance, created, **kwargs):
    from datetime import datetime
    if created:
        if not isinstance(sender, LogEntry):
            track = CampaignTrack.objects.create(campaign=instance, created_date=datetime.now(), start_date=instance.start_date)
            track.save()
    # create instance of script for client..Akshay G.
    if instance.user_id:
        Scripts.objects.get_or_create(campaign_id = instance.id, user_id = instance.user_id)


@receiver(post_save, sender=campaign_allocation)
def cam_alloc_change(sender, instance, created, **kwargs):
    '''for saving data of newly allocated vendor in table camapign track for life cycle'''
    from datetime import datetime
    if CampaignTrack.objects.filter(campaign_id=instance.campaign_id).exists():
        track = CampaignTrack.objects.get(campaign_id=instance.campaign_id)
        camp_all = campaign_allocation.objects.get(id=instance.id)
        if instance.status == 5:
            if track.data_vendor_assign_count > 0:
                list = eval(track.data_vendor_assign)
                # does not allow repeatation
                if any(d['camp_alloc_id'] == instance.id for d in list) != True:
                    print('hello')
                    d = {}
                    d['type'] = 'vendor'
                    d['vendor_id'] = instance.client_vendor.id
                    d['Name'] = instance.client_vendor.user_name
                    t = datetime.now()
                    d['date'] = t.isoformat()
                    d['camp_alloc_id'] = instance.id
                    d['assigned_leads'] = instance.volume
                    d['cpl'] = instance.cpl
                    d['vendor_percentage'] = vendorper(camp_all.id)
                    d['client_percentage'] = percentage(instance.campaign_id)
                    # d['percentage'] = percentage(instance.campaign_id)
                    list.append(d)
                    track.data_vendor_assign = list
                    track.data_vendor_assign_count += 1
                    track.save()
            else:
                # executes if this is first vendor to be assigned
                L = []
                d = {}
                d['type'] = 'vendor'
                d['vendor_id'] = instance.client_vendor.id
                d['Name'] = instance.client_vendor.user_name
                t = datetime.now()
                d['date'] = t.isoformat()
                d['camp_alloc_id'] = instance.id
                d['assigned_leads'] = instance.volume
                d['cpl'] = instance.cpl
                d['vendor_percentage'] = vendorper(camp_all.id)
                d['client_percentage'] = percentage(instance.campaign_id)
                L.append(d)
                track.data_vendor_assign = L
                track.data_vendor_assign_count += 1
                track.save()
    # create instance of script for vendor..Akshay G.
    Scripts.objects.get_or_create(campaign_id = instance.campaign_id, user_id = instance.client_vendor.id, campaign_allocation_id = instance.id)
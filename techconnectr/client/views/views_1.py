import ast
import json
from user.models import user
from zipfile import *

from django.conf import settings
from django.contrib import messages
from django.core import serializers
from django.core.mail import send_mail
from django.http import HttpResponse, JsonResponse
from django.shortcuts import (get_object_or_404, redirect, render,
                              render_to_response)
from django.template import loader
from django.urls import resolve, reverse
from django.template.loader import render_to_string
from django.urls import resolve
from django.utils.html import strip_tags
from client.models import ApiLinks
from campaign.forms import Script_Form
from campaign.models import *
from campaign.choices import *
from client.decorators import *
from client.models import *
from client.utils import RegisterNotification, saving_assets, update_assets,get_external_vendors,grand_child_access_call,email_domain_check, percentage, noti_via_mail, get_external_vendors
from leads.models import *
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from setupdata.models import *
from vendors.views.views import *
from superadmin.utils import collect_campaign_details
from .chat_view import get_vendor_list_of_campaign
from superadmin.models import *
from django.template import RequestContext
from client.views.views import *
from login_register.form import *

# === Vendor List ===
@is_client
def vendor_list(request):
    ''' Return vendor list '''

    vendor_list_details = []
    user_id = request.session['userid']
    apilinks = ApiLinks.objects.all()
    vendor_list = user.objects.get(id=user_id).child_user.all()
    for row in vendor_list:
        # if row.usertype_id == 5:
        vendor_list_details.append({'email': row.email, 'user_name': row.user_name})
    return render(request, 'vendor1/add_vendorlist.html', {'ex_vendor_form': ExternalVendorSignUpForm, 'vendor_list': vendor_list_details, 'apilinks': apilinks})

# === Campign Description ===
@is_users_campaign
def campaingdesc(request, camp_id):
    # campaign desciption includes vendors working on campaign
    camp = Campaign.objects.get(id=camp_id)
    delivery_status= Delivery.objects.get(campaign_id=camp_id)
    if delivery_status.tc_header_status + delivery_status.custom_header_status == 1:
        delivery_status = True
    else:
        delivery_status = False

    vendoralloc = campaign_allocation.objects.filter(campaign_id=camp_id)
    more_alloc = More_allocation.objects.filter(campaign_id=camp_id)
    try:
        share_obj = ClientCustomFields.objects.get(client_id=request.session['userid'])
    except Exception as e:
        share_obj = SystemDefaultFields.objects.first()
    share = share_obj.rfq_rev_share if camp.rfq == 1 else share_obj.normal_rev_share
    return render(request, 'campaign/campdescription.html', {'camp': camp, 'vendoralloc': vendoralloc, 'more_alloc':more_alloc,'share':share,'delivery_status':delivery_status})

# === Add Custom Header ===
'''# abhi 2-09-2019'''
'''# this function used for create custom header by internal qa team.'''
@login_required
def addCustomHeader(request):
    header = request.POST['header']
    camp_id =request.POST['camp_id']
    camp_alloc_id=request.POST['camp_alloc_id']
    if Campaign_custom_field.objects.filter(campaign_id=camp_id).count() > 0 :
        if Campaign_custom_field.objects.filter(campaign_id=camp_id,add_header_by_internal_team=True).count() > 0 :
            data={'success':2,'msg':'Your Team already added Header!...'}
        else:
            if add_custom_header_in_lead(camp_alloc_id,header):
                camp_data=Campaign_custom_field.objects.get(campaign_id=camp_id)
                camp_data.add_header_by_internal_team=True
                camp_data.user_id_add_header_id =request.session['userid']
                camp_data.save()
                data={'success':1,'msg':'Custom Header Added Successfully!...'}
            else:
                data={'success':3,'msg':'Somethings is Wrong Please contact with technical team!...'}
    else:
        camp_data=campaign_allocation.objects.filter(campaign_id=camp_id,status=1)
        dict1={}
        for lead in camp_data:
            if lead.submited_lead > 0 :
                vendor_lead_list=ast.literal_eval(lead.upload_leads)
                dict1=add_custom_header_in_lead(lead.id,header)
        Campaign_custom_field.objects.create(campaign_id=camp_id,add_header_by_internal_team=True,user_id_add_header_id=request.session['userid'],internal_QA_headers=dict1)
        data={'success':1,'msg':'Custom Header Added Successfully!...'}
    return JsonResponse(data)

# === Remove Custom Delivery Templates From Campaign ===
'''# abhi 23-09-2019'''
'''# this function used for remove custom header by client.'''
def remove_custome_header_templates(request):
    camp_id =request.POST['camp_id']
    camp_details=Delivery.objects.filter(campaign_id=camp_id)
    if camp_details.count() > 0:
        camp_details.update(custom_header="",custom_header_status=0,custom_header_mapping="")
        return  JsonResponse({'success':1,'msg':'Custom Header Remove Successfully.'})
    return JsonResponse({'success':2,'msg':'Somethings  happened wrong please contact with technical team.'})


# === Add Custom headers in Lead ===
'''# abhi 2-09-2019'''
'''# this function used for create custom header by internal qa team.'''
def add_custom_header_in_lead(camp_alloc_id,header):
    camp_data=campaign_allocation.objects.get(id=camp_alloc_id)
    vendor_lead_list=ast.literal_eval(camp_data.upload_leads)
    header = header.replace(" ", "_")
    qa_headers=[]
    reason_list=[] # this veriable  store all type status and reason which is created by internal or external user
    qa_headers.append(header)
    reason_list.append(header+'reason_desc')
    reason_list.append('reason_list')
    reason_list.append(header)
    reason_list.append('lead_dict')
    for lead in vendor_lead_list:
        if int(lead['status']):
            dict={header:int(lead['status']),header+'reason_desc':'','qa_headers':qa_headers,'reason_list':reason_list}
            lead.update(dict)
            dict1={'lead_dict':{header:0,header+'reason_desc':'','qa_headers':qa_headers,'reason_list':reason_list}}
            lead.update(dict1)
        else:
            dict={header:0,header+'reason_desc':'','qa_headers':qa_headers,'reason_list':reason_list}
            lead.update(dict)
            dict1={'lead_dict':dict}
            lead.update(dict1)
    camp_data.upload_leads=vendor_lead_list
    camp_data.save()
    return dict1


# === Feedback ===
@login_required
def feedback(request):
    # feedback from client for vendor
    # print(request.POST)
    vendor_rating = request.POST.get('rating')
    # vendor_rating = GenericRelation(Rating, related_query_name='foos')
    feedback = request.POST.get('feed')
    if feedback:
        feed_back = feedback_details.objects.create(feedback=feedback)
        feed_back.save()
    else:
        print("feedback is empty")
    return render(request, 'campaign/feedback.html', {})

# === Lead List ===
'''# @is_users_campaign'''
def leadlist(request, camp_id, status):
    # return single vendor leads to the client
    list1 = []
    all_header = []
    all_lead_header = []
    batchlist = []
    count = []
    camp_alloc = campaign_allocation.objects.filter(id=camp_id)
    
    global_rejected_reason = leads_rejected_reson.objects.all()
    global_rectify_reason = Leads_Rectify_Reason.objects.all()
    
    if camp_alloc.count() == 1 and status == 1:
        camp_alloc = campaign_allocation.objects.get(id=camp_id)
        data = {'campaign_id': camp_alloc.campaign_id, 'camp_status': status, 'camp_id': camp_id, 'status': status, 'camp_name': camp_alloc.campaign.name,
                'cpl': camp_alloc.cpl, 'lead': camp_alloc.volume, 'submited_lead': camp_alloc.submited_lead, 'return_lead': camp_alloc.return_lead, 'vendor_id': camp_alloc.client_vendor_id, 'user_name': user.objects.filter(id=camp_alloc.client_vendor_id)[0].user_name}
        if camp_alloc.submited_lead > 0:
            list1 = ast.literal_eval(camp_alloc.upload_leads)
            batchlist = batch_list(list1)
            list1 = batchlist[2]
            count=0
            for dict in list1:
                if len(dict.keys()) > count :
                    count = len(dict.keys())
                    all_header,all_lead_header=[],[]
                    for key in dict:
                        all_header.append(key)
                        all_lead_header.append(key)
                all_header = create_header(all_header)

        labels = get_lead_header(camp_alloc.campaign_id)
        labels +=join_custom_question_header(camp_alloc.campaign_id)

        if batchlist != []:
            count = batchlist[1]
            batchlist = batchlist[0]
        if len(all_header) == 0:
            all_header = labels
           
        export_file_name=export_data(camp_alloc.campaign.name,camp_alloc.campaign_id,request.session['userid'],1,camp_alloc.client_vendor.password) # download  zipfile and save
        return render(request, 'campaign/client_leadlist.html', {'export_file_name':export_file_name,'approve_leads':check_approve_lead(list1),
                'global_rectify_reason': global_rectify_reason, 'global_rejected_reason': global_rejected_reason, 'campaigns': data, 'leadlist': list1,
                'all_lead_header': all_lead_header, 'all_header': all_header,  'status': status,'batchlist':batchlist,'count':count})
    elif camp_alloc.count() == 1 and status == 4:
        camp_alloc = campaign_allocation.objects.get(id=camp_id)
        data = {'campaign_id': camp_alloc.campaign_id, 'camp_status': status, 'camp_id': camp_id, 'status': status, 'camp_name': camp_alloc.campaign.name,
                'cpl': camp_alloc.cpl, 'lead': camp_alloc.volume, 'submited_lead': camp_alloc.submited_lead, 'return_lead': camp_alloc.return_lead, 'vendor_id': camp_alloc.client_vendor_id, 'user_name': user.objects.filter(id=camp_alloc.client_vendor_id)[0].user_name}
        if camp_alloc.submited_lead > 0:
            list1 = ast.literal_eval(camp_alloc.upload_leads)
            batchlist = batch_list(list1)
            list1 = batchlist[2]
            count=0
            for dict in list1:
                if len(dict.keys()) > count :
                    count = len(dict.keys())
                    all_header,all_lead_header=[],[]
                    for key in dict:
                        all_header.append(key)
                        all_lead_header.append(key)
            all_header = create_header(all_header)

        labels = get_lead_header(camp_alloc.campaign_id)
        labels +=join_custom_question_header(camp_alloc.campaign_id)
        if batchlist != []:
            count = batchlist[1]
            batchlist = batchlist[0]
        if len(all_header) == 0:
            all_header = labels
        export_file_name=export_data(camp_alloc.campaign.name,camp_alloc.campaign_id,request.session['userid'],4,camp_alloc.client_vendor.password)
        print(export_file_name)
        return render(request, 'campaign/client_leadlist.html', {'export_file_name':export_file_name,'approve_leads':check_approve_lead(list1),
                'global_rectify_reason': global_rectify_reason, 'global_rejected_reason': global_rejected_reason, 'campaigns': data, 'leadlist': list1,
                'all_lead_header': all_lead_header, 'all_header': all_header,'status': status,'batchlist':batchlist,'count':count})
    return render(request, 'campaign/client_leadlist.html', {'camp_id': camp_id, 'status': status,'approve_leads':0,})

# === Client Lead List ===
@is_users_campaign
def client_lead_list(request, camp_id, status):
    # leadlist display when client upload leads

    leadlist = []
    all_header = []
    all_lead_header = []
    list=[]
    labels,data,header,camp_alloc = [],[],[],[]
    
    if campaign_allocation.objects.filter(campaign_id=camp_id, status=status, client_vendor_id=request.session['userid']).count() == 1:
        camp_id = campaign_allocation.objects.get(
            campaign_id=camp_id, status=status, client_vendor_id=request.session['userid'])
        camp_id = camp_id.id
        camp_alloc = campaign_allocation.objects.filter(id=camp_id)
        if camp_alloc.count() == 1:
            camp_alloc = campaign_allocation.objects.get(id=camp_id)
            header = create_custom_header(
                camp_alloc.campaign_id, request.session['userid'])
            is_upload=check_lead_uploadable(int(camp_alloc.volume),int(camp_alloc.submited_lead),int(camp_alloc.return_lead))
            data = {'camp_id': camp_alloc.campaign_id, 'camp_alloc_id': camp_id, 'camp_name': camp_alloc.campaign.name, 'cpl': camp_alloc.campaign.cpl,
                    'lead': camp_alloc.volume, 'is_upload':is_upload,'submited_lead': camp_alloc.submited_lead, 'client_name':camp_alloc.campaign.user.user_name,'return_lead': camp_alloc.return_lead}
            if camp_alloc.submited_lead > 0:
                list = ast.literal_eval(camp_alloc.upload_leads)
                if len(header) == 0:
                    count=0
                    for dict in list:
                        if len(dict.keys()) > count :
                            count = len(dict.keys())
                            all_header,all_lead_header=[],[]
                            for key in dict:
                                all_header.append(key)
                                all_lead_header.append(key)
                    all_header = create_header(all_header)
            labels=get_lead_header(camp_alloc.campaign_id)
            labels +=join_custom_question_header(camp_alloc.campaign_id)
            if len(all_header) == 0:
                all_header=labels

    return render(request, 'campaign/client_lead_upload.html', {'edit_lead_header':labels,'approve_leads': check_approve_lead(list), 'campaigns': data, 'leadlist': list, 'all_lead_header': all_lead_header, 'all_header': all_header, 'header': header, 'status': camp_alloc.status, 'camp_id': camp_id})
    '''
    else:
        if campaign_allocation.objects.create(campaign_id=camp_id,volume=0,cpl=0,status=status, client_vendor_id=request.session['userid']):
            camp_id = campaign_allocation.objects.get(
            campaign_id=camp_id, status=status, client_vendor_id=request.session['userid'])
            camp_id = camp_id.id
            camp_alloc = campaign_allocation.objects.filter(id=camp_id)
            if camp_alloc.count() == 1:
                camp_alloc = campaign_allocation.objects.get(id=camp_id)
                header = create_custom_header(
                    camp_alloc.campaign_id, request.session['userid'])
                data = {'camp_id': camp_alloc.campaign_id, 'camp_alloc_id': camp_id, 'camp_name': camp_alloc.campaign.name, 'cpl': camp_alloc.campaign.cpl,
                        'lead': camp_alloc.campaign.target_quantity, 'submited_lead': camp_alloc.submited_lead,'client_name':camp_alloc.campaign.user.user_name, 'return_lead': camp_alloc.return_lead}
                if camp_alloc.submited_lead > 0:
                    list = ast.literal_eval(camp_alloc.upload_leads)
                    if len(header) == 0:
                        count=0
                        for dict in list:
                            if len(dict.keys()) > count :
                                count = len(dict.keys())
                                all_header,all_lead_header=[],[]
                                for key in dict:
                                    all_header.append(key)
                                    all_lead_header.append(key)
                        all_header = create_header(all_header)
                labels=get_lead_header(camp_alloc.campaign_id)
                labels +=join_custom_question_header(camp_alloc.campaign_id)
                if len(all_header) == 0:
                    all_header=labels
    return render(request, 'campaign/client_lead_upload.html', {'camp_id': camp_id, 'status': status})
    return render(request, 'campaign/client_lead_upload.html', {'approve_leads': check_approve_lead(list), 'campaigns': data, 'leadlist': list, 'all_lead_header': all_lead_header, 'all_header': all_header, 'header': header, 'status': camp_alloc.status, 'camp_id': camp_id})
    '''

# === Lead error List ===
@login_required
@is_client
def lead_error_list(request,camp_id,camp_alloc_id):
    import datetime
    userid=request.session['userid']
    date=datetime.datetime.today().strftime('%Y-%m-%d')
    lead_data={'success':1}
    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).exists():
        lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
        # store all leads in leads

        leads=[]
        if lead_data.all_lead_count == 0:
            if lead_data.uploaded_lead_count > 0:
                leads=ast.literal_eval(lead_data.uploaded_lead)
            if lead_data.remove_duplicate_lead_csv_cnt > 0:
                leads=leads+ast.literal_eval(lead_data.remove_duplicate_lead_csv)
            if lead_data.remove_lead_header_cnt > 0:
                leads=leads+ast.literal_eval(lead_data.remove_lead_header)
            if lead_data.duplicate_with_vendor_cnt > 0:
                leads=leads+ast.literal_eval(lead_data.duplicate_with_vendor)
            if lead_data.duplicate_with_our_cnt > 0:
                leads=leads+ast.literal_eval(lead_data.duplicate_with_our)
            lead_data.all_lead=leads
            lead_data.all_lead_count=len(leads)
            lead_data.save()
        else:
            leads=ast.literal_eval(lead_data.all_lead)

        list = []
        all_header = []
        all_lead_header = []
        labels=get_lead_header(camp_id)
        labels +=join_custom_question_header(camp_id)
        if len(leads) > 0:
            for dict in leads[0]:
                all_header.append(dict)
                all_lead_header.append(dict)
        return render(request, 'campaign/lead_error_list.html', {'edit_lead_header':labels,'leadlist': leads, 'all_lead_header': all_lead_header, 'all_header': all_header,'camp_id': camp_id,'camp_alloc_id':camp_alloc_id})

# === Upload With Rejected Lead Web ===
def upload_with_rejected_lead_web(request,camp_id,camp_alloc_id):
    from datetime import  datetime
    userid=request.session['userid']
    date=datetime.today().strftime('%Y-%m-%d')
    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).exists():
        lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
        leads=[]
        # store all leads in leads

        leads=ast.literal_eval(lead_data.all_lead)
        lead_data.lead_upload_status=0
        lead_data.status=0
        lead_data.save()
        last_id_lead=get_last_id_of_lead(camp_alloc_id)
        last_batch_id = get_last_batch_id(camp_alloc_id)
        if len(leads)>0:
            for lead in leads:
                last_id_lead +=1
                lead['id']=last_id_lead
                lead['batch']=last_batch_id
            lead_data=upload_lead_database(leads,camp_alloc_id,userid,1,last_batch_id)
    camp_detail=campaign_allocation.objects.get(id=camp_alloc_id)
    return redirect('client_lead_list', camp_id=camp_id,status=camp_detail.status)

# === Upload Without Rejected Lead Web ===
@login_required
@is_client
def upload_without_rejected_lead_web(request,camp_id,camp_alloc_id):
    from datetime import  datetime
    userid=request.session['userid']

    date=datetime.today().strftime('%Y-%m-%d')
    lead_data={'success':1}
    if Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).exists():
        lead_data=Lead_Uploaded_Error.objects.filter(campaign_id=camp_id,user_id=userid,created_date=date,lead_upload_status=1).latest('exact_time')
        real_data,leads=[],[]
        # store all leads in leads

        leads=ast.literal_eval(lead_data.all_lead)
        lead_data.lead_upload_status=0
        lead_data.status=0
        lead_data.save()

        for lead in leads:
            if lead['TC_lead_status'] == 'valid lead':
                real_data.append(lead)

        last_id_lead=get_last_id_of_lead(camp_alloc_id)
        last_batch_id = get_last_batch_id(camp_alloc_id)
        if len(real_data)>0:
            for lead in real_data:
                last_id_lead +=1
                lead['id']=last_id_lead
                lead['batch']=last_batch_id
            lead_data=upload_lead_database(real_data,camp_alloc_id,userid,1,last_batch_id)
    camp_detail=campaign_allocation.objects.get(id=camp_alloc_id)
    return redirect('client_lead_list', camp_id=camp_id,status=camp_detail.status)


# === Upload Lead Database ===
def upload_lead_database(dict, camp_alloc_id, userid,is_upload,last_batch_id):
    #upload excel lead data into database
    # Upload excel lead data into database

    upload_lead_cnt=len(dict)
    data = campaign_allocation.objects.get(id=camp_alloc_id, client_vendor_id=userid)

    # IF user select upload with reject lead or with rejected lead
    # Only that time upload data

    if is_upload == 1:
        if data.submited_lead == 0:
            data.upload_leads = dict
            data.submited_lead = len(dict)
            data.batch_count = last_batch_id
            data.save()
        else:
            old_data = ast.literal_eval(data.upload_leads)
            dict=checkQAHeaderExist(dict,old_data)
            data.upload_leads = old_data + dict
            data.submited_lead = len(old_data + dict)
            data.batch_count = last_batch_id
            data.save()
    if upload_lead_cnt > 0:
        return {'success':1,'upload_lead_cnt':upload_lead_cnt}
    return {'success':1,'upload_lead_cnt':0}

# === Get Last id of Lead ===
def get_last_id_of_lead(camp_alloc_id):
    # get last id of lead data campaign
    # Get last id of lead
    camp_desc=campaign_allocation.objects.get(id=camp_alloc_id)
    if camp_desc.upload_leads:
        lead_desc=ast.literal_eval(camp_desc.upload_leads)
        if len(lead_desc) > 0:
            return lead_desc[-1]['id']
        return 0
    else:
        return 1

# === Get Last Batch id for Upload ===
def get_last_batch_id(camp_alloc_id):
    # get batch id for upload
    camp_desc=campaign_allocation.objects.get(id=camp_alloc_id)
    if camp_desc.batch_count > 0:
        return camp_desc.batch_count + 1
    return 1

# === check approvr leads ===
def check_approve_lead(list):
    # approve leads
    approve = 0
    for dict in list:
        if dict['status'] == 1:
            approve += 1
    return approve

# === Get Lead Header ===
def get_lead_header(camp_id):
    # returs leads orginal headers
    camp_lead=Delivery.objects.get(campaign_id=camp_id)
    if camp_lead.custom_header_status == 0 :
        labels = camp_lead.data_header
        labels = list(labels.split(','))
    else:
        labels = camp_lead.custom_header
        labels = labels.split(',')
    return labels

# === Export Data ===
def export_data(camp_name,camp_id,vendor_id,status,password):
#lead data export into excel
    import unicodecsv as csv
    import os
    import shutil
    from os import path
    from shutil import make_archive
    from openpyxl import Workbook
    from django.core.management import call_command
    call_command('collectstatic', verbosity=0, interactive=False)

    toCSV = []
    toCSV = convert_data_list(camp_id, vendor_id, status)
    if len(toCSV) > 0:
        if type(toCSV[0]) is dict:
            keys = toCSV[0].keys()
            with open('export_approve_leads.csv', 'wb') as output_file:
                dict_writer = csv.DictWriter(output_file, keys)
                dict_writer.writeheader()
                dict_writer.writerows(toCSV)
            with open('export_approve_leads.csv', 'rb') as myfile:

                response = HttpResponse(myfile, content_type='text/csv')
                response['Content-Disposition'] = 'attachment; filename=export_approve_leads.csv'

                src = path.realpath("export_approve_leads.csv")
                root_dir, tail = path.split(src)
                
                camp_name =''.join(e for e in camp_name if e.isalnum()) #remove all special char
                camp_name = camp_name + '.csv'
                
                file_name='/static/'+str(camp_name)+'_'+str(camp_id)+'_'+str(vendor_id)+'.zip'

                import pyminizip
                compression_level = 5 # 1-9
                pyminizip.compress(root_dir+'/export_approve_leads.csv',None,root_dir+file_name,'', compression_level)
                return file_name


# === convert lead data as per header lead ===
def convert_data_list(camp_id, vendor_id, status):
    # convert lead data as per header lead
    vendor_lead_list = []
    data = []
    lead_dict = {}
    labels = get_lead_header(camp_id)
    labels +=join_custom_question_header(camp_id)

    # status=1 means all vendors leads and 2 means particoluar vendor leads
    if status == 1:
        camp_lead_data = campaign_allocation.objects.filter(
            campaign_id=camp_id, status=1)
        for row in camp_lead_data:
            if row.submited_lead > 0:
                vendor_lead_list += ast.literal_eval(row.upload_leads)
        
        if len(vendor_lead_list) > 0:
            for dict in vendor_lead_list:
                if int(dict['status']) >= 0 :
                    for key in dict:
                        if key in labels:
                            lead_dict.update({key: dict[key]})
                    data.append(lead_dict)
                    lead_dict = {}
            return data
        else:
            return labels
    else:
        camp_lead_data = campaign_allocation.objects.filter(
            campaign_id=camp_id, status=1, client_vendor_id=vendor_id)
        for row in camp_lead_data:
            if row.submited_lead > 0:
                vendor_lead_list += ast.literal_eval(row.upload_leads)

        if len(vendor_lead_list) > 0:
            for dict in vendor_lead_list:
                if dict['status'] == 1:
                    for key in dict:
                        if key in labels:
                            lead_dict.update({key: dict[key]})
                    data.append(lead_dict)
                    lead_dict = {}
            return data
        else:
            return labels

# === Create Header ===
def create_header(dict):
    # creates header list
    list = []
    for row in dict:
        if row == 'reason':
            list.append('Status Code')
        else:
            list.append(row)
    return list

# === create Custom Header ===
def create_custom_header(camp_id, userid):
    # create custome header list
    fav_lead = []
    fav_lead_details = favorite_leads_header.objects.filter(
        campaign_id=camp_id, user_id=userid)
    if fav_lead_details.count() == 1:
        fav_lead_details = favorite_leads_header.objects.get(
            campaign_id=camp_id, user_id=userid)
        return ast.literal_eval(fav_lead_details.header)
    else:
        return fav_lead

# === Lead Approve ===
def lead_approve(request):
    # approve selected lead
    return_lead = 0
    results = [int(i) for i in request.POST.getlist('id[]')]
    status = request.POST.get('status')
    camp_id = request.POST.get('camp_id')
    usertype = request.session['usertype']
    camp_alloc = campaign_allocation.objects.get(
        id=camp_id, status=status)
    list = ast.literal_eval(camp_alloc.upload_leads)

    if usertype == 1 :
        for dict in list:
            if dict['id'] in results:
                dict['status'] = 1
            elif dict['status'] == 2:
                return_lead += 1

        action_on_camp_by_client(1, results, camp_alloc.campaign.id, camp_alloc.client_vendor.id, camp_alloc.client_vendor.user_name)
        from client.utils import noti_via_mail
        super = [i.id for i in user.objects.filter(usertype_id=4)]
        super.append(camp_alloc.client_vendor.id)
        noti = noti_via_mail(super, 'Lead Approval', 'Client '+request.session['username']+' approved '+str(len(results))+ ' leads of '+ camp_alloc.client_vendor.user_name, mail_lead_approval)
        title = 'Lead Approved'
        desc = f"{str(len(results))} lead approved on '{str(camp_alloc.campaign.name)}'"
        RegisterNotification(request.session['userid'], camp_alloc.client_vendor_id, desc, title, 1, None, camp_alloc)
    else:
        #abhi - 31-08-2019 # internal QA team approve leads
        qa_header = list[0]['qa_headers']
        qa_header = qa_header[0]
        for dict in list:
            if dict['id'] in results:
                dict[qa_header] = 1

    data = {'success': 1, 'msg': 'Leads Approved Successfully!..'}
    camp_alloc.upload_leads = list
    camp_alloc.return_lead = return_lead
    camp_alloc.approve_leads = check_approve_lead(list)
    camp_alloc.save()

    return JsonResponse(data)

# === Lead Rejected ===
def lead_rejected(request):
    # reject selected lead
    return_lead = 0
    results = [int(i) for i in request.POST.getlist('id[]')]
    status = request.POST.get('status')
    camp_id = request.POST.get('camp_id')
    Reason = request.POST.get('Reason')
    lead_desc = request.POST.get('lead_desc')
    usertype = request.session['usertype']
    camp_alloc = campaign_allocation.objects.get(
        id=camp_id)
    list = ast.literal_eval(camp_alloc.upload_leads)
    if usertype == 1 :
        for dict in list:
            if dict['id'] in results:
                dict['status'] = int(request.POST.get('lead_status'))
                dict['reason'] = Reason
                dict['lead_desc'] = lead_desc
            if dict['status'] == 2:
                return_lead += 1
    else:
        #abhi - 31-08-2019 # internal QA team rejected leads
        qa_header = list[0]['qa_headers']
        qa_header = qa_header[0]
        for dict in list:
            if dict['id'] in results:
                dict[qa_header] = int(request.POST.get('lead_status'))
                dict[qa_header+'reason_desc'] = Reason+': '+lead_desc

    camp_alloc.upload_leads = list
    camp_alloc.return_lead = return_lead
    camp_alloc.save()
    action_on_camp_by_client(2, results, camp_alloc.campaign.id, camp_alloc.client_vendor.id, camp_alloc.client_vendor.user_name, Reason, lead_desc)
    data = {'success': 1, 'msg': 'Leads Rejected Successfully!..'}
    # send notification when rectify lead
    title = 'Lead Rejected'
    desc = str(len(results))+' More leads Rejected on ' + \
        str(camp_alloc.campaign.name)
    from client.utils import noti_via_mail
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    super.append(camp_alloc.client_vendor.id)
    noti = noti_via_mail(super, title, 'Client '+request.session['username']+' rejected '+str(len(results))+ ' leads of '+ camp_alloc.client_vendor.user_name, mail_lead_reject)
    # noti_via_mail(camp_alloc.client_vendor_id, title, desc, mail_noti_client_action_on_leads)
    # added superadmin id notification # suryakant 12-10-2019
    noti_rec = [i.id for i in user.objects.filter(usertype_id=4)] + [camp_alloc.client_vendor.id]
    [RegisterNotification(request.session['userid'], j, desc, title, 1, None, camp_alloc) for j in noti_rec]
    return JsonResponse(data)

# === Lead Rectify ===
def lead_rectify(request):
    # rectify selected leads
    results = [int(i) for i in request.POST.getlist('id[]')]
    status = request.POST.get('status')
    camp_id = request.POST.get('camp_id')
    Reason = request.POST.get('Reason')
    lead_desc = request.POST.get('lead_desc')
    usertype = request.session['usertype']
    camp_alloc = campaign_allocation.objects.get(
        id=camp_id)
    list = ast.literal_eval(camp_alloc.upload_leads)
    if usertype == 1 :
        for dict in list:
            if dict['id'] in results:
                dict['status'] = 3
                dict['reason'] = Reason
                dict['lead_desc'] = lead_desc
                batch = dict['batch']
    else:
        #abhi - 31-08-2019 # internal QA team Rectified leads
        qa_header = list[0]['qa_headers']
        qa_header = qa_header[0]
        for dict in list:
            if dict['id'] in results:
                dict[qa_header] = int(3)
                dict[qa_header+'reason_desc'] = Reason+': '+lead_desc
                batch = dict['batch']

    camp_alloc.upload_leads = list
    camp_alloc.save()
    action_on_camp_by_client(3, results, camp_alloc.campaign.id, camp_alloc.client_vendor.id, camp_alloc.client_vendor.user_name, Reason, lead_desc)
    data = {'success': 1, 'msg': 'Leads Rectify Successfully!..'}

    # send notification when rectify lead
    title = 'Lead Rectified'

    desc = f'#{str(len(results))}  leads Rectified in batch-{batch} on {str(camp_alloc.campaign.name)} by client.'

    desc = str(len(results))+'  More leads Rectified on ' + str(camp_alloc.campaign.name)
    '''# desc = str(len(results))+'  More leads Rectified on ' + str(camp_alloc.campaign.name)+' by '+camp_alloc.client_vendor.user_name'''
    from client.utils import noti_via_mail
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    super.append(camp_alloc.client_vendor.id)
    noti = noti_via_mail(super, title, desc, mail_lead_rectify)
    # added superadmin id notification # suryakant 12-10-2019
    noti_rec = [i.id for i in user.objects.filter(usertype_id=4)] + [camp_alloc.client_vendor.id]
    [RegisterNotification(request.session['userid'], j, desc, title, 1, None, camp_alloc) for j in noti_rec]
    return JsonResponse(data)

# === Campaign Vendor List ===
@is_users_campaign
def campaign_vendor_list(request, camp_id):
    # campaign vendor list
    ''' vendor suggest changes - 25 June 2019 - Amey '''

    users = user.objects.filter(usertype_id=2).values_list('id', flat=True)  # all vendor ids
    current_vendors = campaign_allocation.objects.filter(campaign_id=camp_id).values_list("client_vendor_id", flat=True)  # vendors assigned to campaign
    users = list(set(users)^set(current_vendors))  # vendors which are not Associated with campaign
    clients = match_campaign_vendor.objects.filter(client_vendor_id__in=users, campaign_id=camp_id)  # vendor list final

    return render(request, 'vendor1/vendorlist.html', {'clients': clients, 'camp': camp_id})

# === Suggest ===
def Suggest(request):
    # suggest vendor list
    results = [int(i) for i in request.POST.getlist('id[]')]
    data = {'success': 0}
    for id in results:
        campaign_id = request.POST.get('camp_id', None)
        vendor_id = id
        counter = campaign_allocation.objects.filter(
            campaign_id=campaign_id, client_vendor_id=vendor_id, status=0, suggest_status=1).count()
        if counter == 1:
            t = campaign_allocation.objects.get(
                campaign_id=campaign_id, client_vendor_id=vendor_id)
            t.status = 0
            t.suggest_status = 1
            t.save()
            data = {'success': 1, 'msg': "Vendor Suggest Successfully!.."}
        else:
            campaign_allocation.objects.create(
                status=0, client_vendor_id=vendor_id, campaign_id=campaign_id, suggest_status=1)
            data = {'success': 1, 'msg': "Vendor Suggest Successfully!.."}

    return JsonResponse(data)

# === Create Demo Campaign ===
def create_demo_campaign(request):
    return render(request, 'campaign/create_demo_campaign.html', {})

# === Password Check ===
def password_check(passwd):
    SpecialSym =['$', '@', '#', '%', '^', '&', '*', '+', '-', '!']

    passwd = passwd.strip()
    if len(passwd) < 8:
        return False

    if any(char.isspace() for char in passwd) is True:
        return False

    if any(char.isspace() for char in passwd):
        return False

    if not any(char.isdigit() for char in passwd):
        return False

    if not any(char.isupper() for char in passwd):
        return False

    if not any(char.islower() for char in passwd):
        return False

    if not any(char in SpecialSym for char in passwd):
        return False

    else:
        return True

# === Add Vendor ===
def add_venodr(request):
    # add vendors
    form = ExternalVendorSignUpForm(request.POST)
    if form.is_valid():
        password = form.cleaned_data['password1']
        email = form.cleaned_data['email'].lower()

        email_list = HeadersValidation.objects.all()
        email_splited = email.split('@')
        if email_splited[-1] in eval(email_list[0].email_list):
            return JsonResponse({'success':2, 'message': 'Email addresses with bussiness domains are allowed only'})
        usertype = ast.literal_eval(request.POST.get('vendor_type'))
        user_obj = form.save(commit=False)
        user_obj.is_active = True
        user_obj.email = email
        user_obj.usertype_id = usertype
        user_obj.save()

        site_url = settings.BASE_URL
        user_id = request.session['userid']
        client = user.objects.get(id=user_id)
        client.child_user.add(user_obj)
        client.save()

        subject = "Invitation from " + client.user_name+ " on Techconnectr portal"
        html_message = render_to_string('email_templates/external_user_register_template.html', {'username': user_obj.email,'client': client.user_name,'site_url': site_url, 'password': password})
        plain_message = strip_tags(html_message)
        from_email = settings.EMAIL_HOST_USER
        to_list = [user_obj.email]
        send_mail(subject, plain_message, from_email, to_list, fail_silently=True,html_message=html_message)
        print('mail sent')
        from client.utils import noti_via_mail
        # sending notifications to all users
        user_type = 'External Vendor' if usertype == 5 else "Internal Vendor"
        sender_id = request.session['userid']
        receiver_ids = [d for d in user.objects.filter(usertype_id=4)] + [d for d in user.objects.filter(id__in=[sender_id,user_obj.id])]
        # raise super admin notification
        for superad in receiver_ids:
            if superad.usertype_id in [1,4]:
                title = f'{user_type} Created.'
                desc = f'Vendor "{user_obj.user_name}" added as {user_type} for {client.user_name}.'
            else:
                title = f'{user_type} Invite.'
                desc = f'Welcome to TechConnectr "{user_obj.user_name}". Our valuable Client "{client.user_name}" added you as {user_type}.'
            RegisterNotification(sender_id, superad.id, desc, title, 1, None, None)
        
        noti = noti_via_mail([i.id for i in user.objects.filter(usertype_id=4)], 'New '+ user_type, f'{user_type} ' + user_obj.user_name+ ' created by '+client.user_name, mail_external_vendor_register)
        if usertype == 5:
            data = {'success': 1, 'message': "External vendor '" + user_obj.user_name +"' created successfully", 'type': 'External vendor'}
        else:
            data = {'success': 1, 'message': "Internal vendor '" + user_obj.user_name +"' created successfully", 'type': 'Internal vendor'}
        return JsonResponse(data)

    else:
        if 'email' in form.errors and len(form.errors) == 1:
            if len(form.errors['email']) == 1:
                email = form.data['email'].lower()
                if user.objects.filter(email=email, usertype_id__in=[2, 5]).exists():
                    user_id = request.session['userid']
                    client = user.objects.get(id=user_id)
                    client.child_user.add(user.objects.get(email=email))
                    client.save()
                    user_type = 'External Vendor' if usertype == 5 else "Internal Vendor"
                    from client.utils import noti_via_mail
                    noti = noti_via_mail([i.id for i in user.objects.filter(usertype_id=4)], f'{user_type} ', 'Vendor '+user.objects.get(email=email).user_name+ ' added as '+ user_type +' for '+client.user_name, mail_external_vendor_register)
                    data = {'success': 1, 'message': 'Vendor '+ user.objects.get(email=email).user_name + ' is now added as '+ user_type +' for you'}
                    return JsonResponse(data)

        data = {'success': 0, 'form_errors': form.errors}
        return JsonResponse(data)

# === Vendor Allocation ===
@is_users_campaign
def vendor_allocation(request, camp_id):
    # vendor allocation by to client to his external vendor
    campaign_details = Campaign.objects.get(id=camp_id)
    ids = []
    vendor_list_details = []
    try:
        rev_value = ClientCustomFields.objects.get(client_id=request.session['userid'])
    except Exception as e:
        rev_value = SystemDefaultFields.objects.first()
    data = {
        'username': campaign_details.user.user_name,
        'userid': campaign_details.user_id,
        'cpl': campaign_details.cpl,
        'rev_share':rev_value.normal_rev_share if campaign_details.rfq == 0 else rev_value.rfq_rev_share,
        'targat_quantity': campaign_details.target_quantity,
        'ramaining_quantity': campaign_details.raimainingleads,
        'approveleads': campaign_details.approveleads,
    }
    user_id = request.session['userid']
    vendor_sugg = campaign_allocation.objects.filter(campaign_id=camp_id)
    for row in vendor_sugg:
        if user.objects.filter(id=row.client_vendor_id,status=3).count() > 0:
            if row.suggest_status == 1 and row.cpl != -1 and row.volume != -1 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
                vendor_list_details.append({
                    'fav': False,
                    'suggest': False,
                    'checked': 'checked',
                    'display': 'none',
                    'vendor_name': row.client_vendor.user_name,
                    'vendor_id': row.client_vendor_id,
                    'camp_id': row.campaign_id,
                })
            elif row.status == 4 and row.cpl != -1 and row.volume != -1 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
                vendor_list_details.append({
                    'fav': False,
                    'suggest': False,
                    'checked': '0',
                    'display': 'none',
                    'vendor_name': row.client_vendor.user_name,
                    'vendor_id': row.client_vendor_id,
                    'camp_id': row.campaign_id,
                })
            elif row.status == 3 and row.volume == -1 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
                vendor_list_details.append({
                    'checked': '0',
                    'fav': False,
                    'suggest': False,
                    'display': 'none',
                    'vendor_name': row.client_vendor.user_name,
                    'vendor_id': row.client_vendor_id,
                    'camp_id': row.campaign_id,
                })
            elif row.status == 5 and row.volume > 0 and row.cpl > 0 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
            elif row.status == 3 and row.volume > 0 and row.cpl > 0 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)
            elif row.status == 1 and row.volume > 0 and row.cpl > 0 and row.client_vendor_id not in ids:
                ids.append(row.client_vendor_id)

    vendor_sugg = match_campaign_vendor.objects.filter(campaign_id=camp_id).exclude(client_vendor_id__in=ids)
    for row in vendor_sugg:
        ids.append(row.client_vendor_id)
        vendor_list_details.insert(0, {
            'fav': False,
            'suggest': True,
            'checked': '0',
            'display': 'none',
            'vendor_name': row.client_vendor.user_name,
            'vendor_id': row.client_vendor_id,
            'camp_id': row.campaign_id,
        })
    for i in user.objects.get(id=user_id).favorite_vendor.all():
        if i.id not in ids and i.id not in [d.client_vendor.id for d in campaign_allocation.objects.filter(campaign_id=camp_id)]:
            ids.append(i.id)
            vendor_list_details.insert(1, {
                'fav': True,
                'checked': '0',
                'display': 'none',
                'vendor_name': i.user_name,
                'vendor_id': i.id,
                'camp_id': campaign_details.id,
            })

    for i in user.objects.get(id=user_id).child_user.all().exclude(usertype=6):
        if i.id not in ids and i.id not in [d.client_vendor.id for d in campaign_allocation.objects.filter(campaign_id=camp_id)]:
            ids.append(i.id)
            vendor_list_details.append({
                'fav': False,
                'checked': '0',
                'display': 'none',
                'vendor_name': i.user_name,
                'vendor_id': i.id,
                'camp_id': campaign_details.id,
            })
    users = user.objects.filter(usertype_id__in=[2],status=3).exclude(id__in=ids,usertype=6)
    # vendor_sugg = client_vendor.objects.filter(user_id__in=users)
    for row in users:
        if row.id not in ids:
            vendor_list_details.append({
                'fav': True if row in user.objects.get(id=request.session['userid']).favorite_vendor.all() else False,
                'suggest': False,
                'checked': '0',
                'display': 'none',
                'vendor_name': row.user_name,
                'vendor_id': row.id,
                'camp_id': camp_id,
            })
    # show vendor quotation lead & cpl..Akshay G.
    client_custom_obj = ClientCustomFields.objects.get(client_id = request.session['userid'])
    vendor_quotation_data = eval(campaign_details.rfq_quotation) if campaign_details.rfq_quotation else []
    rfq_quote_vendor_ids = []
    total_lead = 0
    quotation_cpl = 0
    for d in vendor_quotation_data:
        rfq_quote_vendor_ids.append(d['vendor_id'])
        if client_custom_obj.rfq_rev_share:
            d['cpl'] = d['cpl'] + d['cpl']*(client_custom_obj.rfq_rev_share)/100
        total_lead += d['lead']
        # suryakant 12-10-2019
        # send checkbox value to mark as checked if rfq quotations received
        d['checked'] = 1 if float(d['cpl']) > 0.0 and d['lead'] > 0 else 0
        quotation_cpl = d['cpl'] if d['cpl'] > quotation_cpl else quotation_cpl
    total_quotation = {'cpl': quotation_cpl, 'total_lead': total_lead }
    if request.session['usertype'] == 1:
        return render(request, 'campaign/client_vendor_allocation.html', {'campdata': data, 'vendor_data': vendor_list_details, 'vendor_quotation_data': vendor_quotation_data, 'rfq_quote_vendor_ids':rfq_quote_vendor_ids, 'total_quotation': total_quotation})
    else:
        return render(request, 'superclient/sc_vendor_allocation.html', {'campdata': data, 'vendor_data': vendor_list_details})

# === Insert Lead ===
@is_users_campaign
def insert_lead(request, camp_id, camp_alloc_id):
    # display lead header to upload indivdual lead
    labels = get_lead_header(camp_id)
    labels +=join_custom_question_header(camp_id)
    camp_alloc = campaign_allocation.objects.get(id=camp_alloc_id)
    if camp_alloc.submited_lead >= camp_alloc.volume :
        more_leads_uploads= camp_alloc.submited_lead - camp_alloc.volume
        if camp_alloc.return_lead >= more_leads_uploads:
            more_leads_uploads = camp_alloc.return_lead - more_leads_uploads
    else:
        more_leads_uploads=(camp_alloc.volume-camp_alloc.submited_lead)+(camp_alloc.return_lead)
    return render(request, 'campaign/addlead.html', {'lead_header': labels,'more_leads_uploads':more_leads_uploads, 'camp_id': camp_id, 'camp_alloc_id': camp_alloc_id,'camp_alloc':camp_alloc})


# === Asset Submit ===
def asset_submit(request):
    # following code is submit asset data in to database kishor
    # to submit assets for campaign
    if request.POST:
        t = Terms.objects.filter(campaign=request.POST.get('campaign'))
        if t:
            t[0].assets_name = request.POST.get('assets_name')
            t[0].sponsers = request.POST.get('sponsers')
            t[0].asset_distributor = request.POST.get('asset_distributor')
            t[0].add_assetslink = request.POST.get('add_assetslink')
            if t[0].assets_type:
                t[0].assets_type = update_assets(
                    request, t[0].assets, ast.literal_eval(t[0].assets_type))
            else:
                file_dict = saving_assets(request, t[0].assets)
                t[0].assets_type = file_dict
            t[0].save()

            # amey changes for notification of new assets
            from client.utils import noti_via_mail
            receivers = [i.client_vendor.id for i in campaign_allocation.objects.filter(status__in=[1,5], campaign_id=request.POST.get('campaign'))]
            super = [i.id for i in user.objects.filter(usertype_id=4)]
            receivers.extend(super)
            title = 'New Asset Added'
            desc  = 'New assests has been added by '+user.objects.get(id=request.session['userid']).user_name+' for campaign '+Campaign.objects.get(id=request.POST.get('campaign')).name
            for j in super:
                RegisterNotification(request.session['userid'], j, desc, title, 2,None,None)
            noti = noti_via_mail(receivers, title,desc, mail_assets_and_scripts)

            data = {'status_code': 1}
            return JsonResponse(data)
        else:
            data = {'status_code': 2, 'message': 'Camapign Not Found'}
            return JsonResponse(data)
    data = {'status_code': 2, 'message': 'Please Fill the detail'}
    return JsonResponse(data)

# === Remove Asset ===
def remove_asset(request):
    # remove links from campaign assets"""
    term_data = Terms.objects.get(campaign_id=request.POST.get('camp_id'))
    asset_type = request.POST.get('asset_type')
    data = ast.literal_eval(term_data.assets_type)
    if data:
        if data[asset_type]['link']:
            if request.POST.get('asset') in data[asset_type]['link']:
                data[asset_type]['link'].remove(request.POST.get('asset'))
                new_list = data[asset_type]['link']
                if len(new_list) > 0:
                    data[asset_type].update({'link': new_list})
                    term_data.assets_type = data
                    term_data.save()
                else:
                    if bool(data[asset_type]):
                        data[asset_type].pop('link',None)
                        if not bool(data[asset_type]):
                            data.pop(asset_type,None)
                    else:
                        data.pop(asset_type,None)
                    term_data.assets_type = data
                    term_data.save()

                # amey changes for notification of new scripts
                from client.utils import noti_via_mail
                receivers = [i.client_vendor.id for i in campaign_allocation.objects.filter(status__in=[1,5], campaign_id=request.POST.get('camp_id'))]
                super = [i.id for i in user.objects.filter(usertype_id=4)]
                receivers.extend(super)
                desc = 'Some assets has been removed by '+user.objects.get(id=request.session['userid']).user_name+' for campaign '+Campaign.objects.get(id=request.POST.get('camp_id')).name
                for j in super:
                    RegisterNotification(request.session['userid'], j, desc, 'Assets Changed', 2,None,None)
                noti = noti_via_mail(receivers, 'Assets Changed', desc, mail_assets_and_scripts)

                data = {'status_code': 1, 'message': 'asset removed'}
            else:
                data = {'status_code': 1, 'message': 'asset remove error'}
    return JsonResponse(data)

# === Remove File Asset ===
def remove_file_asset(request):
    # remove files from campaign assets"""
    term_data = Terms.objects.get(campaign_id=request.POST.get('camp_id'))
    asset_type = request.POST.get('asset_type')
    data = ast.literal_eval(term_data.assets_type)
    if data:
        if len(data[asset_type]['files']) > 0:
            for file_dict in data[asset_type]['files']:
                if request.POST.get('asset') == file_dict['url']:
                    data[asset_type]['files'].remove(file_dict)
                    new_list = data[asset_type]['files']
                    if len(new_list) > 0:
                        data[asset_type].update({'files': new_list})
                        term_data.assets_type = data
                        term_data.save()
                    else:
                        if bool(data[asset_type]):
                            data[asset_type].pop('files',None)
                            if not bool(data[asset_type]):
                                data.pop(asset_type,None)
                        else:
                            data.pop(asset_type,None)
                        term_data.assets_type = data
                        term_data.save()

                    # amey changes for notification of assets remove
                    from client.utils import noti_via_mail
                    receivers = [i.client_vendor.id for i in campaign_allocation.objects.filter(status__in=[1,5], campaign_id=request.POST.get('camp_id'))]
                    super = [i.id for i in user.objects.filter(usertype_id=4)]
                    receivers.extend(super)
                    desc = 'Some assets has been removed by '+user.objects.get(id=request.session['userid']).user_name+' for campaign '+Campaign.objects.get(id=request.POST.get('camp_id')).name
                    for j in super:
                        RegisterNotification(request.session['userid'], j, desc, 'Assets Changed', 2,None,None)
                    noti = noti_via_mail(receivers, 'Assets Changed', desc, mail_assets_and_scripts)

            data = {'status_code': 1, 'message': 'asset removed'}
        else:
            data = {'status_code': 1, 'message': 'asset remove error'}
    return JsonResponse(data)

# === Script Submit ===
def script_submit(request):
    # to submit scripts for campaign
    campaign_id = request.POST.get('campaign')
    camp_id = Scripts.objects.get_or_create(
        campaign_id= campaign_id, user_id = request.session['userid'])
    if request.FILES:
        myfile = request.FILES['client_script']
        if myfile.name.endswith('.pdf'):
            fs = FileSystemStorage()
            filename = fs.save("Scripts/" + myfile.name, myfile)
            uploaded_file_url = fs.url(filename)
            camp_id[0].client_script = filename
            camp_id[0].save()

            # amey changes for notification of new scripts
            from client.utils import noti_via_mail
            receivers = [i.client_vendor.id for i in campaign_allocation.objects.filter(status__in=[1,5], campaign_id=request.POST.get('campaign'))]
            super = [i.id for i in user.objects.filter(usertype_id=4)]
            receivers.extend(super)
            noti = noti_via_mail(receivers, 'New Scripts Added', 'New scripts has been added by '+user.objects.get(id=request.session['userid']).user_name+' for campaign '+Campaign.objects.get(id=request.POST.get('campaign')).name, mail_assets_and_scripts)

            data = {'status': 1}
            return JsonResponse(data)
        return JsonResponse({'status': 3})
    data = {'status': 2}
    return JsonResponse(data)

# === Vendor Script Submit ===
def vendor_script_submit(request):
    # upload vendor scripts
    campaign_id = request.POST.get('campaign')
    # save script file uploaded by vendor...Akshay G.
    camp_id = Scripts.objects.get_or_create(
        campaign_id= campaign_id, user_id = request.session['userid'])
    if request.FILES:
        myfile = request.FILES['script_file']
        if myfile.name.endswith('.pdf'):
            fs = FileSystemStorage()
            filename = fs.save("Scripts/" + myfile.name, myfile)
            uploaded_file_url = fs.url(filename)
            camp_id[0].client_script = filename
            camp_id[0].save()
            receivers = [i.campaign.user.id for i in campaign_allocation.objects.filter(status__in=[1,5], campaign_id=request.POST.get('campaign'))]
            super = [i.id for i in user.objects.filter(usertype_id=4)]
            receivers.extend(super)
            receivers = list(set(receivers))
            #send mail to client and superadmin about new script uploaded by vendor
            title = 'New Scripts Added'
            desc = 'New scripts has been added by '+user.objects.get(id=request.session['userid']).user_name+' for campaign'+Campaign.objects.get(id=request.POST.get('campaign')).name
            from client.utils import noti_via_mail
            noti = noti_via_mail(receivers, title,desc, mail_assets_and_scripts)
            # Abhi 12-10-2019
            # send notify to client or superadmin about script uploaded by vendor
            for j in receivers:
                RegisterNotification(request.session['userid'], j, desc, title, 1,Campaign.objects.get(id=campaign_id), None)
            data = {'status': 1}
            return JsonResponse(data)
        return JsonResponse({'status': 3})
    data = {'status': 2}
    return JsonResponse(data)

# === Get Asset Specs ===
def get_asset_specs(request):
    # to fetch uploaded assets
    asset = Terms.objects.get(campaign_id=request.POST.get('id'))
    data = {
        'name':asset.campaign.name,
        'assets_name': asset.assets_name if asset.assets_name else 'None',
        'assets': asset.assets if asset.assets else 'None',
        'sponsers': asset.sponsers if asset.sponsers else 'None',
        'assets_type': ast.literal_eval(asset.assets_type) if asset.assets_type else 'None',
        'asset_distributor': asset.asset_distributor if asset.asset_distributor else 'None',
        'add_assetslink': asset.add_assetslink if asset.add_assetslink else 'None',
    }
    return JsonResponse(data)

# === get Scripts ===
def get_scripts(request):
    # return uploaded scripts
    term_data = Scripts.objects.filter(campaign_id=request.POST.get('id'))
    data = {}
    if term_data:
        data = {
            'url': term_data[0].client_script.url if term_data[0].client_script.url else 'None', }

    return JsonResponse(data)

# === Get Agreements ===
def get_agreements(request):
    pass

# === Campaign Notebook ===
@login_required
def campaign_notebook(request):
    # return all campaigns to the campaign notebook
    counter = Campaign.objects.filter(
        user_id=request.session['userid']).count()
    data = []
    if counter > 0:
        campaign_details = Campaign.objects.filter(
            user_id=request.session['userid'])
        pending_camp_count = Campaign.objects.filter(
            user_id=request.session['userid'], status=3).count()
        live_camp_count = Campaign.objects.filter(
            user_id=request.session['userid'], status=1).count()
        assigned_camp_count = Campaign.objects.filter(
            user_id=request.session['userid'], status=5).count()
        complete_camp_count = Campaign.objects.filter(
            user_id=request.session['userid'], status=4).count()
        return render(request, 'campaign/campaign_notebook.html', {'camps': campaign_details, 'pending_count': pending_camp_count,
                                                                   'live_count': live_camp_count, 'assigned_count': assigned_camp_count, 'complete_count': complete_camp_count})
    else:
        return render(request, 'campaign/campaign_notebook.html', {})

# === Get Campaign Data ===
def get_camp_data(request):
    """ return campaign data to display on campaign notebook """
    delivery_header_status = True
    camp_details = Campaign.objects.get(id=request.POST.get('id'))
    camp_alloc_query = campaign_allocation.objects.filter(campaign_id = camp_details.id)
    # show vendor agreements status to client..Akshay G.
    agreements_data = {}
    agreements_path = {}
    for obj in camp_alloc_query:
        if obj.submited_lead > 0 :
            delivery_header_status = False # After leads upload on campaign client can't able to modified Delivery Template.
        if obj.vendor_agreements:
            data = eval(obj.vendor_agreements)
            vendor_data = {}
            path = {}
            if len(data) > 0:
                for d in data:
                    for agr_name in d.keys():
                        if d[agr_name] == '':
                            vendor_data[agr_name] = 0
                        else:
                            vendor_data[agr_name] = 1
                            path[agr_name] = os.path.join(settings.BASE_URL, d[agr_name])
            agreements_data[obj.client_vendor.user_name] = vendor_data
            agreements_path[obj.client_vendor.user_name] = path
    mapping = Mapping.objects.get(campaign_id=request.POST.get('id'))
    aggrement = data_assesment.objects.get(user_id=request.session['userid'])
    # check if any vendor has uploaded script file for this campaign..Akshay G.
    script_query = Scripts.objects.filter(
        campaign_id= camp_details.id).exclude(user_id = request.session['userid'])
    
    script_data = []
    if script_query:
         for obj in script_query:
            if obj.client_script:
                script_data.append({'vendor_name': obj.user.user_name, 'file_path': os.path.join(settings.BASE_URL, 'media',obj.client_script.name)})
            else:
                script_data.append({'vendor_name': obj.user.user_name, 'file_path': ''})
    data = {
        'agreements_path': agreements_path,
        'agreements_data': agreements_data,
        'script_data': script_data,
        'rfq_status': camp_details.rfq,
        'name': camp_details.name,
        'cpl': camp_details.cpl,
        'volume': camp_details.target_quantity,
        'camp_io':camp_details.io_number,
        'rfq': camp_details.rfq,
        'status': camp_details.status,
        'rfq_timer': camp_details.rfq_timer,
        'start_date': camp_details.start_date if camp_details.start_date  else '',
        'end_date': camp_details.end_date if camp_details.end_date  else '',
        'desc': camp_details.description,
        'camp_id': request.POST.get('id'),
        'sender_id': request.session['userid'],
        'special_instr': mapping.special_instructions,
        'nda': aggrement.nda_aggrement,
        'msa': aggrement.msa_aggrement,
        'gdpr': aggrement.gdpr_aggrement,
        'dpa': aggrement.dpa_aggrement,
        'io': aggrement.io_aggrement,
        'delivery_header_status':delivery_header_status,
        'nda_path': aggrement.nda_aggrement_path.url if aggrement.nda_aggrement_path != '' else '',
        'msa_path': aggrement.msa_aggrement_path.url if aggrement.msa_aggrement_path != '' else '',
        'gdpr_path': aggrement.gdpr_aggrement_path.url if aggrement.gdpr_aggrement_path != '' else '',
        'dpa_path': aggrement.dpa_aggrement_path.url if aggrement.dpa_aggrement_path != '' else '',
        'io_path': aggrement.io_aggrement_path.url if aggrement.io_aggrement_path != '' else '',
    }
    return JsonResponse(data)

# === Return Asssigned Vendor List ===
def get_vendor_list(request):
    # return assigned vendor list
    import os
    camp_details = Campaign.objects.get(id=request.POST.get('camp_id'))
    vendor_list = campaign_allocation.objects.filter(
        campaign_id=camp_details.id, status__in=[1,4,5]).distinct()
    vendor_id = []
    vendor_data = []
    for vendor in vendor_list:
        if vendor.client_vendor_id not in vendor_id:
            from client.utils import fetch_default_logo
            default_logo = fetch_default_logo(vendor.client_vendor_id)
            vendor_ = client_vendor.objects.filter(user_id = vendor.client_vendor_id)
            if vendor_:
                vendor_id.append(vendor.client_vendor_id)
                # fetch company logo for various users...Akshay G...Date - 22nd Oct, 2019
                if vendor_[0].company_logo_smartthumbnail != '':
                    try:
                        company_logo = str(vendor_[0].company_logo_smartthumbnail.url)
                        if company_logo == '/media/':
                            company_logo = default_logo    
                    except Exception as e:
                        company_logo = default_logo
                else:
                    company_logo = default_logo
                vendor_data.append({
                        'id': vendor.client_vendor_id,
                        'vendor_name': vendor.client_vendor.user_name,
                        'logo': company_logo
                    })
            else:
                vendor_id.append(vendor.client_vendor_id)
                vendor_data.append({
                    'id': vendor.client_vendor_id,
                    'vendor_name': vendor.client_vendor.user_name,
                    'logo': default_logo
                })
    return JsonResponse(vendor_data, safe=False)

# === All Lead Display ===
@is_users_campaign
def all_lead_display(request, camp_id):
    # return all uploaded leads for a campaign
    all_list = []
    all_header = []
    all_lead_header = []
    submitLead = 0
    rejectedLead = 0
    camp_alloc = campaign_allocation.objects.filter(campaign_id=camp_id)
    if camp_alloc.count() > 0:
        header = create_custom_header(camp_id, request.session['userid'])
        campaign_details = Campaign.objects.get(id=camp_id)
        for row in camp_alloc:
            submitLead = submitLead + int(row.submited_lead)
            rejectedLead = rejectedLead+int(row.return_lead)
            data = {'camp_id': camp_id, 'camp_alloc_id': camp_id, 'camp_name': campaign_details.name, 'cpl': campaign_details.cpl,
                    'lead': campaign_details.target_quantity, 'submited_lead': submitLead, 'return_lead': rejectedLead}
            if row.upload_leads != None:
                list = ast.literal_eval(row.upload_leads)
                if len(header) == 0:
                    for dict in list[0]:
                        all_header.append(dict)
                        all_lead_header.append(dict)
                    all_header = create_header(all_header)
                else:
                    all_header = header
                    for dict in list[0]:
                        all_lead_header.append(dict)
                    all_header = create_header(all_header)
                all_list.extend(list)

    return render(request, 'campaign/client_lead_upload.html', {'campaigns': data, 'leadlist': all_list, 'all_lead_header': all_lead_header, 'all_header': all_header, 'header': header, 'camp_id': camp_id})

# === Get RFQ CPL ===
def get_rfq_cpl(request):
    # return rfq and cpl sent by superadmin
    campaign = Campaign_rfq.objects.get(campaign_id=request.POST.get('camp_id'))
    data = {'cpl': campaign.cpl, 'volume': campaign.volume}
    return JsonResponse(data)

# === Update Statsu RFQ ===
def update_status_rfq(request):
    # process actions on rfq resopnce

    camp_id = request.POST.get('camp_id')
    cpl = request.POST.get('cpl')
    volume = request.POST.get('volume')
    choose = request.POST.get('choose')
    user_id = request.session['userid']
    superadmin = user.objects.filter(usertype_id=4)
    if choose == '1':
        accept_rfq_cpl_by_client(camp_id, cpl, volume, user_id, superadmin[0].id)
    elif choose == '2':
        remark = request.POST.get('remark')
        counter_rfq_cpl_by_client(
            camp_id, cpl, volume, user_id, remark, superadmin[0].id)
    elif choose == '3':
        remark = request.POST.get('remark')
        rejected_rfq_cpl_by_client(
            camp_id, cpl, volume, user_id, remark, superadmin[0].id)
    data = {'success': 1, 'msg': 'Action Submitted Successfully!...'}
    return JsonResponse(data)

# === Accept RFQ CPL by Client ===
def accept_rfq_cpl_by_client(camp_id, cpl, volume, user_id, superadmin_id):
    # accept cpl and volume sent from superadmin

    # changes for rfq datatable
    campaign = Campaign_rfq.objects.get(campaign_id=camp_id)
    campaign.status = 3
    campaign.save()

    campaign = Campaign.objects.get(id=camp_id)
    name = campaign.name
    campaign.cpl = cpl
    campaign.tc_target_quantity = volume
    campaign.tc_raimainingleads = volume

    campaign.client_raimainingleads= int(campaign.client_raimainingleads) - int(volume)

    campaign.rfq_status = 1
    campaign.save()
    title = 'Client RFQ CPL'
    desc = f'Accept RFQ CPL Request by client on campaign "{name}".'
    from client.utils import noti_via_mail
    noti = noti_via_mail([i.id for i in user.objects.filter(usertype_id=4)], title, desc, mail_rfq_quotation_approval)
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    for j in super:
        RegisterNotification(user_id, j, desc, title, 1, campaign, None)
    return True

# === Counter RFQ CPL by Client ===
def counter_rfq_cpl_by_client(camp_id, cpl, volume, user_id, remark, superadmin_id):
    # counter cpl or volume to superadmin
    campaign = Campaign_rfq.objects.get(campaign_id=camp_id)
    name = campaign.campaign.name
    campaign.cpl = campaign.cpl
    campaign.volume = campaign.volume
    campaign.cpl = cpl
    campaign.volume = volume
    campaign.remark = remark
    campaign.status = 2
    campaign.save()
    campaign = Campaign.objects.get(id=camp_id)
    campaign.rfq_status = 2
    campaign.save()
    title = 'Client RFQ CPL'
    desc = f'Counter RFQ CPL by client on campaign "{name}".'
    # noti_via_mail(sender_id,title, desc, 1)
    from client.utils import noti7_via_mail
    noti = noti_via_mail([i.id for i in user.objects.filter(usertype_id=4)], title, desc, mail_rfq_quotation_counter)
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    for j in super:
        RegisterNotification(user_id, j, desc, title, 1, campaign, None)
    return True

# === Rejected RFQ CPL by Client ===
def rejected_rfq_cpl_by_client(camp_id, cpl, volume, user_id, remark, superadmin_id):
    # reject cpl and volume sent from superadmin
    campaign = Campaign_rfq.objects.get(campaign_id=camp_id)
    campaign.status = 4
    campaign.remark = remark
    campaign.save()
    campaign = Campaign.objects.get(id=camp_id)
    name = campaign.name
    title = 'Client RFQ CPL'
    desc = f'Reject RFQ CPL by client on campaign "{name}" for reason "{remark}".'
    # noti_via_mail(sender_id, title, desc, 1)
    from client.utils import noti_via_mail
    noti = noti_via_mail([i.id for i in user.objects.filter(usertype_id=4)], title, desc, mail_rfq_quotation_reject)
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    for j in super:
        RegisterNotification(user_id, j, desc, title, 1, campaign, None)
    return True

# === RFQ Campaign ===
@is_users_campaign
def RFQ_Campaign(request, camp_id):
    # to request rfq suggestion from external vendors
    campaign_details = Campaign.objects.get(id=camp_id)
    data = {
        'cpl': campaign_details.cpl,
        'targat_quantity': campaign_details.client_target_quantity,
        'ramaining_quantity': campaign_details.client_raimainingleads,
        'camp_id': camp_id,
        'camp_name': campaign_details.name,
        'client_name': campaign_details.user.user_name,
    }
    list = []
    vendor_list_details = []
    user_id = request.session['userid']
    marketing_method = source_touches.objects.filter(is_active=1)
    cpl_counter = campaign_allocation.objects.filter(
        campaign_id=camp_id, status=3, cpl=0, volume=-1).count()
    cpl_list = campaign_allocation.objects.filter(
        campaign_id=camp_id, status=3, cpl=0, volume=-1)
    vendor_list_obj = get_external_vendors(user_id)
    vendor_list = [obj.id for obj in vendor_list_obj]
    for row in vendor_list:
        user_details = user.objects.get(id=row)
        vendor_list_details.append({'userid': row, 'email': user_details.email, 'user_name': user_details.user_name})
    return render(request, 'campaign/client_rfq_campaign.html', {'cpl_counter': cpl_counter, 'cpl_list': cpl_list, 'data': data, 'vendor_list': vendor_list_details})

# === RFQ Vendor Allocation ===
def rfq_vendor_allocation(request):
    # to send request to external vendors
    ids = request.POST.getlist('ids[]')
    camp_id = request.POST.get('camp_id')
    userid = request.session['userid']
    title = "New RFQ Campaign Allocated By"
    desc = "Client"
    for id in ids:
        counter = campaign_allocation.objects.filter(
            campaign_id=camp_id, client_vendor_id=id, cpl=-1, volume=-1, status=3).count()
        if counter != 1:
            campaign_allocation.objects.create(
                campaign_id=camp_id, client_vendor_id=id, cpl=-1, volume=-1, status=3)
        RegisterNotification(userid, id, desc, title, 2,None, campaign_allocation.objects.latest('id'))
    from client.utils import noti_via_mail
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    if ids:
        super.extend(ids)
    noti = noti_via_mail(super, title, desc, mail_rfq_quotation_request)
    data = {'success': 1}
    return JsonResponse(data)

# === Update RFQ CPL ===
def update_rfq_cpl(request):
    # update rfq and cpl
    t = Campaign.objects.get(id=request.POST.get('camp_id'))
    t.cpl = request.POST.get('cpl')
    t.save()
    data = {'success': 1, 'msg': 'CPL updated Successfully!...'}
    return JsonResponse(data)

# === Counter Action on CPL ===
def counter_action_on_cpl(request):
    # counter cpl
    vendor_id = request.POST.get('vendor_id')
    camp_alloc_id = request.POST.get('camp_alloc_id')
    id = request.POST.get('id')
    userid = request.session['userid']
    rev_share = ClientCustomFields.objects.get(client_id=request.session['userid'])
    if id == '1':
        counter = Counter_cpl.objects.get(user_id=vendor_id, campaign_allocation_id=camp_alloc_id)
        share = rev_share.rfq_rev_share if counter.campaign.rfq == True else rev_share.normal_rev_share
        ccpl = counter.req_cpl + (counter.req_cpl *share/100)
        vcpl = counter.req_cpl
        counter_action_accept_vendor(camp_alloc_id, vendor_id, userid, vcpl,ccpl,counter.volume)
        data = {'success': 1, 'msg': 'Counter Request accepted...'}
    elif id == '2':
        counter_action_reject_vendor(camp_alloc_id, vendor_id, userid)
        data = {'success': 1, 'msg': 'Counter Request Rejected...'}
    return JsonResponse(data)

# === Counter Action Accept by External Vendor ===
def counter_action_accept_vendor(camp_alloc_id, vendor_id, userid, vcpl,ccpl,volume):
    # accept counter by external vendor
    camp = campaign_allocation.objects.get(id=camp_alloc_id)
    name = camp.campaign.name
    camp.cpl = vcpl
    camp.volume = volume
    camp.client_cpl = ccpl
    camp.counter_status = 2
    camp.save()
    title = "Accept Counter"
    desc = f"Accept CPL Counter on {str(name)}."
    #Abhi 12-10-2019
    #send mail and notify to superadmin and vendors
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    super.append(camp.client_vendor.id)
    for j in super:
        RegisterNotification(userid, j, desc,title, 1, None, camp)       
    from client.utils import noti_via_mail
    noti = noti_via_mail(super,title, desc, mail_counter_action_by_client)
    
    return True

# === Counter Action Request ===
def counter_action_reject_vendor(camp_alloc_id, vendor_id, userid):
    # reject counter request
    camp = campaign_allocation.objects.get(id=camp_alloc_id)
    name = camp.campaign.name
    camp.counter_status = 2
    camp.save()
    title = "Reject Counter"
    desc = f"Reject CPL Counter on {str(name)}."
    # suryakant 16-10-2019
    # raised notification and send mail to superadmin and vendors
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    super.append(camp.client_vendor.id)
    for j in super:
        RegisterNotification(userid, j,desc,title, 1, None, camp)       
    from client.utils import noti_via_mail
    noti = noti_via_mail(super,title, desc, mail_counter_action_by_client)
    return True

# === get Camp Specs ===
def get_camp_specs(request):
    # get all campaign specifications
    camp_id = request.POST.get('id')
    camp_data = Campaign.objects.get(id=camp_id)
    # print(camp_data.__dict__)
    term_data = Terms.objects.get(campaign_id=camp_id)
    delivary_data = Delivery.objects.get(campaign_id=camp_id)
    mapping_data = Mapping.objects.get(campaign_id=camp_id)
    spec_data = Specification.objects.get(campaign_id=camp_id)

    # use as text mapping
    useAsTxtMapping = []
    txt_mapping_exist = UseAsTxtMapping.objects.count()
    if(txt_mapping_exist):  # if records exist
        useAsTxtMapping = UseAsTxtMapping.objects.filter(
            campaign=camp_data).values()  # list of dicts
        # print("useAsTxtMapping : ", useAsTxtMapping.values())
        for i in useAsTxtMapping:
            # if i['original_txt'] == 'C- Level ':
            i['original_txt'] = i['original_txt'].strip()

    context = {
        'campaign_id': camp_data.id,
        'stat': camp_data.status,
        'target_quantity': camp_data.target_quantity if camp_data.target_quantity else '',
        'campaign_type': camp_data.get_type_display() if camp_data.type else '',
        'outrich_method': camp_data.method.all().values_list('type',flat=True) if camp_data.method.all() else '',
        'indursty_type': mapping_data.industry_type if mapping_data.industry_type else '',
        'job_title': mapping_data.job_title if mapping_data.job_title else '',
        'job_level': mapping_data.job_level if mapping_data.job_level else '',
        'assets': term_data.assets if term_data.assets else '',
        'delivery_method': delivary_data.delivery_method if delivary_data.delivery_method else '',
        'country': mapping_data.country if mapping_data.country else '',
        'company_size': mapping_data.company_size if mapping_data.company_size else '',
        'revenue': mapping_data.revenue_size if mapping_data.revenue_size else '',
        'data_field': delivary_data.data_header if delivary_data.data_header else '',
        'custom_data_fields':delivary_data.custom_header if delivary_data.custom_header else '',
        'tc_header_status':delivary_data.tc_header_status,
        'custom_header_status':delivary_data.custom_header_status,
        'pacing': spec_data.campaign_pacing if spec_data.campaign_pacing else '',
        'abm_file': spec_data.abm_count,
        'suppression_file': spec_data.suppression_count,
        'instructions': mapping_data.special_instructions if mapping_data.special_instructions else '',
        'useAsTxtMapping': useAsTxtMapping,
        'rfq_status': camp_data.rfq,
        'campaign':camp_data,
        'mapping':mapping_data,
        'Custom_question_status':mapping_data.custom_status,
        'usertype':request.session['usertype']
    }
    # return JsonResponse(data)
    html = render_to_response('campaign/show_campaign_specs.html', context)
    
    return html

# === Save PDF Specs Data ===
# @is_users_campaign
def save_pdf_specs_data(request,camp_id):
    # Kishor P
    # Save pdf Formate data on show camapign specs (Date: 27 May 2019)
    # get all campaign specifications
    # camp_id = request.POST.get('id')
    camp_data = Campaign.objects.get(id=camp_id)
    term_data = Terms.objects.get(campaign_id=camp_id)
    delivary_data = Delivery.objects.get(campaign_id=camp_id)
    mapping_data = Mapping.objects.get(campaign_id=camp_id)
    spec_data = Specification.objects.get(campaign_id=camp_id)

    # use as text mapping
    useAsTxtMapping = []
    txt_mapping_exist = UseAsTxtMapping.objects.count()
    if(txt_mapping_exist):  # if records exist
        useAsTxtMapping = UseAsTxtMapping.objects.filter(campaign=camp_data).values()  # list of dicts
        for i in useAsTxtMapping:
            # if i['original_txt'] == 'C- Level ':
            i['original_txt'] = i['original_txt'].strip()
    # kishor
    # convert data in string
    country_tuple = mapping_data.country if mapping_data.country else '',
    country_list_single_str = [ele for ele in country_tuple]
    country = ', '.join(format_string_for_pdf(country_list_single_str[0])) if country_list_single_str else ''

    indursty_type_tuple = mapping_data.industry_type if mapping_data.industry_type else '',
    indursty_type_single_str = [ele for ele in indursty_type_tuple]
    it = ', '.join(format_string_for_pdf(indursty_type_single_str[0])) if indursty_type_single_str else ''

    job_title_tuple = mapping_data.job_title if mapping_data.job_title else '',
    job_title_list_single_str = [ele for ele in job_title_tuple]
    jt = ', '.join(format_string_for_pdf(job_title_list_single_str[0])) if job_title_list_single_str else ''


    joblevel = mapping_data.job_level if mapping_data.job_level else '',
    joblevel_list_single_str = [ele for ele in joblevel]
    joblevel =', '.join(format_string_for_pdf(joblevel_list_single_str[0])) if joblevel_list_single_str else ''

    data_field_tuple = delivary_data.data_header if delivary_data.data_header else delivary_data.custom_header,
    data_field_list_single_str = [ele for ele in data_field_tuple]
    data_field = format_string_for_pdf(data_field_list_single_str[0]) if data_field_list_single_str[0] else ''

    assets = term_data.assets if term_data.assets else '',
    assets_single_str = [ele for ele in assets]
    assets =', '.join(format_string_for_pdf(assets_single_str[0])) if assets_single_str else ''

# add vendor type on rfq and normal campign save data in PDF file
    vendor_type = []
    if camp_data.tc_vendor == 1:
        vendor_type.append('TC Vendor')
    if camp_data.internal_vendor == 1:
        vendor_type.append('Internal Vendor')
    if camp_data.external_vendor == 1:
        vendor_type.append('External Vendor')
    comp_size = []
    if mapping_data.company_size:
        if ',' in mapping_data.company_size:
            comp_size.extend(mapping_data.company_size.split(','))
        else:
            comp_size.append(mapping_data.company_size)
    revenue_sizes = []
    if mapping_data.revenue_size:
        if ',' in mapping_data.revenue_size:
            revenue_sizes.extend(mapping_data.revenue_size.split(','))
        else:
            revenue_sizes.append(mapping_data.revenue_size)
    context = {
        'io_number': camp_data.io_number if camp_data.io_number else '',
        'campaign_name': camp_data.name if camp_data.name else '',
        'description': camp_data.description if camp_data.description else '',
        'cpl': '$ ' + str(camp_data.cpl) if camp_data.cpl else '',
        'start_date': camp_data.start_date if camp_data.start_date else '',
        'end_date': camp_data.end_date if camp_data.end_date else '',
        'vendor': vendor_type,
        # 'internal_vendor': vendor_type,
        # 'external_vendor': vendor_type,
        'department': mapping_data.job_function if mapping_data.job_function else '',
        'campaign_id': camp_data.id,
        'stat': camp_data.status,
        'target_quantity': camp_data.target_quantity if camp_data.target_quantity else '',
        'campaign_type': camp_data.get_type_display() if camp_data.type else 'RFQ',
        'outrich_method': camp_data.method.all().values_list('type',flat=True) if camp_data.method.all() else '',
        'job_level': joblevel,
        'assets': assets,
        'country': country,
        # 'delivery_method': dm,
        'delivery_method' : delivary_data.campaign_delivery_time if delivary_data.campaign_delivery_time else '',
        'pacing': spec_data.campaign_pacing if spec_data.campaign_pacing else '',
        'company_size': comp_size,
        'indursty_type': it,
        'job_title': jt,
        'revenue': revenue_sizes,
        'data_field': data_field,
        'custom_data_fields':delivary_data.custom_header if delivary_data.custom_header else '',
        'tc_header_status':delivary_data.tc_header_status,
        'custom_header_status':delivary_data.custom_header_status,
        'instructions': mapping_data.special_instructions if mapping_data.special_instructions else '',
        'useAsTxtMapping': useAsTxtMapping,
        'rfq_status': camp_data.rfq,
        'campaign':camp_data,
        'mapping':mapping_data,
        'Custom_question_status':mapping_data.custom_status,
        'usertype':request.session['usertype'],
    }

    from xhtml2pdf import pisa
    template = get_template('campaign/save_show_campaign_specs_data.html')
    html  = template.render(context)
    with open(os.path.join(settings.MEDIA_ROOT, 'campaign_specs_files/Campaign_Specs.pdf'), 'w+b') as file:
    # file = open('Campaign_Specs.pdf', "w+b")
        pisaStatus = pisa.CreatePDF(html.encode('utf-8'), dest=file,encoding='utf-8')
        file.seek(0)
        pdf = file.read()
        file.close()

    return HttpResponse(pdf, 'application/pdf')
    # return render(request, 'campaign/save_show_campaign_specs_data.html', context)

# === Formate String For PDF  ===
def format_string_for_pdf(string):
    # preapre string for PDF"""
    if string != '' and string != 'None':
        return string.split(",")
    else:
        return ''


# === save CSV Campaign Specs ===
def save_csv_camp_specs(request):
    # for save pdf and csv file formate ...Kishor
    # get all campaign specifications
    camp_id = request.POST.get('id')
    context = campaign_export_formatting_data(request, camp_id)
    import csv
    with open(os.path.join(settings.MEDIA_ROOT, 'campaign_specs_files/Campaign_Specs.csv'),'w') as file:
        for key in context.keys():
            if key == 'TARGET QUANTITY:' or key == 'CPL:':
                file.write("%s,%s\n"%(str(key),str(context[key])))
            else:
                file.write("%s,%s\n"%(key,context[key]))
    path = {'path': '/media/' + 'campaign_specs_files/Campaign_Specs.csv'}
    return JsonResponse(path)

# === ABM File Contents ===
def abm_file_contents(request):
    #Kishor Date 2-8-2019
    #ABM File Contents Save in CSV file formate by kishor
    camp_id = request.POST.get('camp_id')
    specs_obj = Specification.objects.get(campaign_id=camp_id)
    from client.views.rfq_views import create_suppression_abm_file
    # create downloadable file of abm data...Akshay G..Date - 21st Nov, 2019
    file_path = create_suppression_abm_file(None, specs_obj.abm_file_content)
    abm_file = file_path['abm_path'] if 'abm_path' in file_path else ''
    return JsonResponse({'path': abm_file})
# end /ABM File Contents Save in CSV file formate by kishor

# === Suppression File Content ===
def suppression_file_content(request):
# Suppression File Contents Save in CSV file formate by kishor
    camp_id = request.POST.get('camp_id')
    specs_obj = Specification.objects.get(campaign_id=camp_id)
    from client.views.rfq_views import create_suppression_abm_file
    # create downloadable file of suppr data...Akshay G..Date - 21st Nov, 2019
    file_path = create_suppression_abm_file(specs_obj.suppression_file_content, None)
    suppr_file = file_path['suppression_path'] if 'suppression_path' in file_path else ''
    return JsonResponse({'path': suppr_file})

# === Campaign Export Formatting Data ===
def campaign_export_formatting_data(request,camp_id):
    export_type = request.POST.get('type')
    camp_data = Campaign.objects.get(id=camp_id)
    term_data = Terms.objects.get(campaign_id=camp_id)
    delivary_data = Delivery.objects.get(campaign_id=camp_id)
    mapping_data = Mapping.objects.get(campaign_id=camp_id)
    spec_data = Specification.objects.get(campaign_id=camp_id)

    # use as text mapping
    useAsTxtMapping = []
    txt_mapping_exist = UseAsTxtMapping.objects.count()
    if(txt_mapping_exist):  # if records exist
        useAsTxtMapping = UseAsTxtMapping.objects.filter(campaign=camp_data).values()  # list of dicts

    tc_vendor = "Yes" if camp_data.tc_vendor == 1 else "No"
    in_vendor = "Yes" if camp_data.internal_vendor == 1 else "No"
    ex_vendor = "Yes" if camp_data.external_vendor == 1 else "No"

    data_field = delivary_data.data_header if delivary_data.data_header else delivary_data.custom_header
    if data_field:
        data_field = format_string_for_csv(data_field,export_type)

    data_map = mapping_data.job_level if mapping_data.job_level else '--',
    data_map_list_single_str = [ele for ele in data_map]
    if data_map_list_single_str:
        data_map = format_string_for_csv(data_map_list_single_str[0],export_type)

    job_function = mapping_data.job_function if mapping_data.job_function else '--',
    job_function_list_single_str = [ele for ele in job_function]
    if job_function_list_single_str:
        job_function = format_string_for_csv(job_function_list_single_str[0],export_type)

    assets = term_data.assets if term_data.assets else '--',
    assets_list_single_str = [ele for ele in assets]
    if assets_list_single_str:
        assets = format_string_for_csv(assets_list_single_str[0],export_type)

    delivary_data = delivary_data.campaign_delivery_time if delivary_data.campaign_delivery_time else '--',
    delivary_data_single_str = [ele for ele in delivary_data]
    if delivary_data_single_str:
        delivary_data = format_string_for_csv(delivary_data_single_str[0],export_type)

    company_size = mapping_data.company_size if mapping_data.company_size else '--',
    company_size_single_str = [ele for ele in company_size]
    if company_size_single_str:
        company_size = format_string_for_csv(company_size_single_str[0],export_type)

    revenue = mapping_data.revenue_size if mapping_data.revenue_size else '--',
    revenue_single_str = [ele for ele in revenue]
    if revenue_single_str :
        str1 = revenue_single_str[0].replace("999,999", "999999")
        revenue = format_string_for_csv(str1,export_type)

    pacing = spec_data.campaign_pacing if spec_data.campaign_pacing else '--',
    pacing_single_str = [ele for ele in pacing]
    if pacing_single_str:
        pacing = format_string_for_csv(pacing_single_str[0],export_type)

    io_number = camp_data.io_number if camp_data.io_number else '--',
    io_number_str = [ele for ele in io_number]
    if io_number_str:
        io_number = format_string_for_csv(io_number_str[0],export_type)

    job_title = mapping_data.job_title if mapping_data.job_title else '--',
    job_title_single_str = [ele for ele in job_title]
    if job_title_single_str:
        job_title = format_string_for_csv(job_title_single_str[0],export_type)

    country = mapping_data.country if mapping_data.country else '--',
    country_single_str = [ele for ele in country]
    if country_single_str:
        country = format_string_for_csv(country_single_str[0],export_type)

    industry_type =  mapping_data.industry_type if mapping_data.industry_type else '--',
    industry_type_single_str = [ele for ele in industry_type]
    if industry_type_single_str:
        industry_type = format_string_for_csv(industry_type_single_str[0],export_type)

    outreach_Method = camp_data.method.all().values_list('type',flat=True) if camp_data.method.all() else ''
    if outreach_Method:
        outreach_method = ' / '.join(outreach_Method)
    else:
        outreach_method = '--'

    context = {
        'IO NUMBER:': io_number if camp_data.io_number else '--',
        'CAMPAIGN NAME:': camp_data.name if camp_data.name else '--',
        'DESCRIPTION:': camp_data.description if camp_data.description else '--',
        'OUTREACH METHOD:': outreach_method,
        'TARGET QUANTITY:': str(camp_data.target_quantity) if camp_data.target_quantity else '--',
        'START DATE:': camp_data.start_date if camp_data.start_date else '--',
        'END DATE:': camp_data.end_date if camp_data.end_date else '--',
        'TC VENDOR:': tc_vendor,
        'INTERNAL VENDOR:': in_vendor,
        'EXTERNAL VENDOR:': ex_vendor,
        'DEPARTMENT:': job_function,
        'INDUSTRY TYPE:': industry_type,
        'JOB TITLE:': job_title,
        'JOB LEVEL:': data_map,
        'ASSETS:': assets,
        'DELIVERY METHOD' : delivary_data,
        'COUNTRY:': country,
        'COMPANY SIZE:': company_size,
        'REVENUE:': revenue,
        'DATA FIELD:': data_field,
        'PACING:': pacing if pacing else '--',
        'INSTRUCTION:': mapping_data.special_instructions if mapping_data.special_instructions else '--',
    }
    if camp_data.rfq == 1:
        context['CPL:'] = camp_data.cpl if camp_data.cpl else '--'
        context['CAMPAIGN TYPE:'] = 'RFQ'
    else:
        context['CPL:'] = camp_data.cpl if camp_data.cpl else '--'
        context['CAMPAIGN TYPE:'] = camp_data.get_type_display() if camp_data.type else '--'
    return context

# === Format String for CSV ===
def format_string_for_csv(string,export_type):
    # preapre string for csv"""
    if export_type == 'csv':
        if string != '' and string != 'None':
            return string.replace(","," / ")
        else:
            return '--'

# === Rejected Reason List ===
@login_required
def rejected_reason_list(request):
    # return reject lead reasons
    user_id = request.session['userid']
    # global_reason=leads_rejected_reson.objects.filter(status=0)
    client_reason = leads_rejected_reson.objects.all()
    return render(request, 'lead/rejected_reason_list.html', {'reasons': client_reason})

# === Rectify Reason List  ===
@login_required
def rectify_reason_list(request):
    # return rectify lead reasons
    user_id = request.session['userid']
    client_reason = Leads_Rectify_Reason.objects.all()
    return render(request, 'lead/rectify_reason_list.html', {'reasons': client_reason})

# === Individual Campaign Notebook ===
@login_required
def individual_campaign_notebook(request, camp_id):
    # individual campaign notebook
    campaign_details = Campaign.objects.filter(id=camp_id)
    live_camp_count = Campaign.objects.filter(id=camp_id, status=3).count()
    vendor_list = get_vendor_list_of_campaign(camp_id)
    if campaign_details:
        return render(request, 'campaign/individual_campaign_notebook.html', {'camps': campaign_details, 'vendor_list': vendor_list,
                                                                              })
    else:
        return render(request, 'campaign/individual_campaign_notebook.html', {})


# === Update Campaign End Date ===
@login_required
def update_campaign_end_date(request):
    # update campaign end date
    if Campaign.objects.filter(id=request.POST.get('camp_id')).update(end_date=request.POST.get('date')):
        data = {'status': 1, 'message': 'date change successfully'}
    else:
        data = {'status': 2, 'message': 'date change failed'}
    return JsonResponse(data)

# === Update Campaign Start Date ===
@login_required
def update_campaign_start_date(request):
    # update campaign end date
    if Campaign.objects.filter(id=request.POST.get('camp_id')).update(start_date=request.POST.get('date')):
        data = {'status': 1, 'message': 'date change successfully'}
    else:
        data = {'status': 2, 'message': 'date change failed'}
    return JsonResponse(data)

# === TC Vendor List ===
@login_required
def TC_vendor_list(request):
    # TC vendor list
    vendor_list_details = []
    user_id = request.session['userid']
    campaigns = Campaign.objects.filter(user=user_id)
    vendor_list = campaign_allocation.objects.filter(
        campaign_id__in=campaigns, status__in=[1, 5, 4])
    for row in vendor_list:
        user_details = client_vendor.objects.filter(
            user_id=row.client_vendor.id)
        if user_details:
            if user_details[0] not in vendor_list_details:
                vendor_list_details.append(user_details[0])
    return render(request, 'vendor1/TC_vendor_list.html', {'vendor_list': vendor_list_details})

# === Existing Vendor List ===
def existing_vendor_list(request):
    # TC vendor who are currently working with client
    vendor_list_details = []
    user_id = request.session['userid']
    campaigns = Campaign.objects.filter(user=user_id)
    vendor_list = campaign_allocation.objects.filter(
        campaign_id__in=campaigns, status=1)
    for row in vendor_list:
        user_details = user.objects.filter(id=row.client_vendor.id)
        if user_details:
            if user_details[0] not in vendor_list_details:
                vendor_list_details.append(user_details[0])
    return render(request, 'vendor1/existing_vendor_list.html', {'vendor_list': vendor_list_details})

# === Get CPL List Old ===
def get_cpl_list_old(request):
    # return cpl list to client
    camp_id = request.GET.get('camp_id')
    user_id = request.session['userid']
    get_details = collect_campaign_details(camp_id)
    vendor_list = []
    ids=[]
    tc_quote = []
    vendor_list_obj = get_external_vendors(user_id)
    vendor_list_id = [obj.id for obj in vendor_list_obj]
    ext_vendor_list = campaign_allocation.objects.filter(campaign_id=camp_id,client_vendor_id__in=vendor_list_id, status=3, cpl__in=[-1,0], volume=-1)
    '''
    if Campaign.objects.get(id=camp_id).internal_vendor == 1:
        if campaign_allocation.objects.filter(campaign_id=camp_id,client_vendor_id=user_id).count()!=1:
            vendor_list.append({
                'id': user_id,
                'cpl': 0,
                'volume': 0,
                'name': user.objects.get(id=user_id).user_name,
            })
    '''
    for vendor in ext_vendor_list:
        vendor_list.append({
            'checked':'checked',
            'id':vendor.client_vendor_id,
            'cpl':vendor.rfqcpl if vendor.rfqcpl else -1,
            'volume':vendor.rfqvolume if vendor.rfqvolume else -1,
            'name':vendor.client_vendor.user_name,
        })
    '''
    campaign = Campaign_rfq.objects.filter(campaign_id=camp_id)
    for vendor in campaign:
        if vendor.status == 1:
            tc_quote.append({
                'id':vendor.campaign_id,
                'cpl':vendor.cpl,
                'volume':vendor.volume,
                'name':'TC PARTNER',
            })
    '''

    vendor_data = []
    vendorlist = campaign_allocation.objects.filter(campaign_id=camp_id, status=3,cpl=0)
    for vendor in vendorlist:
        vendor_data.append({
            'id': vendor.client_vendor_id,
            'vendor_name': vendor.client_vendor.user_name
        })
    rfq_timer = Campaign.objects.get(id=camp_id)
    rfq_timer = rfq_timer.rfq_timer

    # display all TC vendors rfq quotes to client -suryakant 29 july 2019
    cpl_list = campaign_allocation.objects.filter(
        campaign_id=camp_id, status=3, cpl__in=[0, -1], volume=-1).order_by('-cpl')
    for vendor in cpl_list:
        if vendor.client_vendor.usertype_id == 2:
            vendor_list.append({
                'checked':'checked',
                'id': vendor.client_vendor_id,
                'cpl': vendor.rfqcpl if vendor.rfqcpl else -1,
                'c_cpl': cpl_increase(vendor.rfqcpl) if vendor.rfqcpl else -1,
                'volume': vendor.rfqvolume if vendor.rfqvolume else -1,
                'name': vendor.client_vendor.user_name,
            })
            ids.append(vendor.client_vendor_id)

    users=user.objects.exclude(id__in=ids).filter(usertype_id=2,approve=1,status=3)
    for us in users:
        vendor_list.append({
                'checked':'unchecked',
                'id': us.id,
                'cpl': -1,
                'c_cpl': -1,
                'volume':-1,
                'name': us.user_name,
            })
    # print(vendor_data)
    data = {'rfq_timer': rfq_timer,'success': 1,'ext_vendor_list':vendor_list,'details':get_details,'tc_quote':tc_quote,'vendor_data':vendor_data }
    return JsonResponse(data)

# === Client User Access ===
@login_required
def client_user_access(request):
    # return user access modules to client
    usertypes = usertype.objects.filter(type__in=["external_vendor","internal_vendor"])
    users = []
    user_id = request.session['userid']
    vendor_list_obj = get_external_vendors(user_id)
    int_vendor_list_obj = get_internal_vendors(user_id)
    vendor_list = [obj.id for obj in vendor_list_obj] + [obj.id for obj in int_vendor_list_obj]
    for row in vendor_list:
        user_details = user.objects.get(id=row)
        users.append(user_details)
    groups = Client_User_Group.objects.filter(group_owner_id=user_id)
    return render(request, 'dashboard/client_user_access.html', {'type':usertypes,'groups':groups,'users':users})

# === Get User Access ===
@login_required
def get_user_access(request):
    # return selected user access
    current_user = user.objects.get(id=request.POST.get('userid'))
    if request.POST.get('groupid'):
        roles = User_Configuration.objects.filter(is_client =True,group__in=[request.POST.get('groupid')]).order_by('position')
        access = []
        for role in roles:
            if current_user in role.user.all():
                access.append({
                    'id': role.id,
                    'name': role.name,
                    'url': role.url,
                    'parent': role.parent.id if role.parent else None,
                    'checked': 1,
                    'groupname':role.group.filter(id=request.POST.get('groupid'))[0].group_name,
                    'groupid':role.group.filter(id=request.POST.get('groupid'))[0].id,
                })
            else:
                access.append({
                    'id': role.id,
                    'name': role.name,
                    'url': role.url,
                    'parent': role.parent.id if role.parent else None,
                    'checked': 0,
                    'groupname':role.group.filter(id=request.POST.get('groupid'))[0].group_name,
                    'groupid':role.group.filter(id=request.POST.get('groupid'))[0].id,
                })
        data = {'success': 1, 'user_roles': access}
    else:
        roles = User_Configuration.objects.filter(is_client =True,user_type__in=request.POST.get('usertype_id')).order_by('position')
        access = []
        for role in roles:
            if current_user in role.user.all():
                access.append({
                    'id': role.id,
                    'name': role.name,
                    'url': role.url,
                    'parent': role.parent.id if role.parent else None,
                    'checked': 1,
                    'usertype':role.user_type.filter(id=request.POST.get('usertype_id'))[0].type if request.POST.get('usertype_id') else None,
                })
            else:
                access.append({
                    'id': role.id,
                    'name': role.name,
                    'url': role.url,
                    'parent': role.parent.id if role.parent else None,
                    'checked': 0,
                    'usertype':role.user_type.filter(id=request.POST.get('usertype_id'))[0].type if request.POST.get('usertype_id') else None,
                })
        data = {'success': 1, 'user_roles': access}
    return JsonResponse(data)

# === User and Groups ===
def user_and_groups(request):
    # display create group page
    form = ExternalUserSignUpForm()
    groups = Client_User_Group.objects.filter(group_owner_id=request.session['userid']).order_by('-created_date')
    roles = User_Configuration.objects.filter(is_client=True,user=request.session['userid']).order_by('position')
    external_user_list = []
    ids_of_external_user = []
    ids_of_external_user1 = []
    user_ = user.objects.get(id=request.session['userid']).child_user.all().order_by('-created_date')
    ids_of_external_user = [i_.id for i_ in user_ if i_.usertype_id == 6]

    if ids_of_external_user:
        for i in ids_of_external_user:
            temp = i
            user_obj = user.objects.get(id=i)
            for j in groups:
                if user_obj in j.group_users.all():
                    external_user_list.append({'name': user_obj.user_name, 'group_id': j.id, 'group': j.group_name, 'id': i,'created_date':user_obj.updated_date})
                    ids_of_external_user1.append(i)
            if temp not in ids_of_external_user1:
                external_user_list.append({'name': user_obj.user_name, 'group_id': 0, 'group': 'No group associated with', 'id': i,'created_date':user_obj.updated_date})
    del ids_of_external_user1
    #Abhi 12 Nov 2019
    # sort desc accordint to date
    external_user_list = sorted(external_user_list, key = lambda i: i['created_date'],reverse=True)
    
    return render(request,'client/user_groups.html',{'form': form, 'ex_user': external_user_list, 'groups':groups, 'roles':roles})

# === Add Group ===
def add_group(request):
    # group add by client
    client_id = user.objects.get(id=request.session['userid'])
    group_type = Client_User_Group.objects.create(group_name=request.POST.get('group'),group_owner=client_id)
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    super.append(client_id.id)
    noti = noti_via_mail(super, 'New Group Created', 'New Group '+request.POST.get('group')+' created by '+client_id.user_name, mail_new_group)
    data = {'success': 1, 'id': group_type.id}
    return JsonResponse(data)

# === Delete Group ===
def delete_group(request):
    # group delete by client
    client_id = user.objects.get(id=request.session['userid'])
    group_type = Client_User_Group.objects.get(id=request.POST.get('group_id'),group_owner=client_id)
    user_with_grp = group_type.group_users.all()
    user_con_obj = User_Configuration.objects.filter(user__in=user_with_grp)
    for obj in user_con_obj:
        for j in user_with_grp:
            obj.user.remove(j)
            obj.save()
    Client_User_Group.objects.get(id=request.POST.get('group_id'),group_owner=client_id).delete()
    data = {'success': 1}
    return JsonResponse(data)

# === Edit Group ===
def edit_group(request):
    # group add by client
    client_id = user.objects.get(id=request.session['userid'])
    if request.method == 'POST':
        client_grp = Client_User_Group.objects.get(id=request.POST.get('group_id'),group_owner=client_id)
        if client_grp:
            client_grp.group_name = request.POST.get('group')
            client_grp.save()
        data = {'success': 1}
    else:
        client_grp = Client_User_Group.objects.get(id=request.GET.get('group_id'),group_owner=client_id)
        if client_grp:
            data = {'success': 1,'group_name':client_grp.group_name}
        else:
            data = {'success': 2}
    return JsonResponse(data)


# === Password Check ===
def password_check(passwd):
    SpecialSym =['$', '@', '#', '%', '^', '&', '*', '+', '-', '!']

    passwd = passwd.strip()
    if len(passwd) < 8:
        return False

    if any(char.isspace() for char in passwd) is True:
        return False

    if not any(char.isdigit() for char in passwd):
        return False

    if not any(char.isupper() for char in passwd):
        return False

    if not any(char.islower() for char in passwd):
        return False

    if not any(char in SpecialSym for char in passwd):
        return False

    else:
        return True

# === Add User to Group ===
@is_client
def add_user_to_group(request):
    # add user to group

    form = ExternalUserSignUpForm(request.POST)
    if form.is_valid():
        user_name = form.cleaned_data['user_name']
        email = form.cleaned_data['email'].lower()
        password1 = form.cleaned_data['password1']
        group_id = form.data['group_selector']

        email_list = HeadersValidation.objects.all()
        email_splited = email.split('@')
        if email_splited[-1] in eval(email_list[0].email_list):
            return JsonResponse({'success':2, 'message': 'Email addresses with bussiness domains are allowed only'})

        new_user = form.save(commit=False)
        new_user.is_active = True
        new_user.email = email
        new_user.usertype_id = 6
        new_user.save()

        client_id = user.objects.get(id=request.session['userid'])
        client_id.child_user.add(new_user)
        client_id.save()

        if group_id != 'select_group':
            client_group = Client_User_Group.objects.filter(id=group_id,group_owner=client_id)
            if client_group:
                client_group[0].group_users.add(new_user)
                client_group[0].save()
                user_config = User_Configuration.objects.filter(group=client_group[0])
                for con in user_config:
                    con.user.add(new_user)
                    con.save()

        site_url = settings.BASE_URL
        subject = "Invitation from " + client_id.user_name +"on Techconnectr portal"
        html_message = render_to_string('email_templates/external_user_register_template.html', {'username': email, 'client':client_id.user_name,'site_url': site_url, 'password': password1})
        plain_message = strip_tags(html_message)
        from_email = settings.EMAIL_HOST_USER
        to_list = [email]
        send_mail(subject, plain_message, from_email, to_list, fail_silently=True,html_message=html_message)

        super = [i.id for i in user.objects.filter(usertype_id=4)]
        super.append(request.session['userid'])
        noti = noti_via_mail(super, 'New External User', 'External user '+user_name+ ' created by '+user.objects.filter(id=request.session['userid'])[0].user_name, mail_external_user_register)

        data = {'success': 1, 'message': 'External user created successfully'}
        return JsonResponse(data)

    data = {'success': 0, 'form_errors': form.errors}
    return JsonResponse(data)

# === Get Group Access ===
@login_required
def get_group_access(request):
    # return selected user access
    user_group = Client_User_Group.objects.get(id=request.POST.get('groupid'),group_owner=request.session['userid'])
    roles = User_Configuration.objects.filter(is_client =True,user=request.session['userid']).order_by('position')
    access = []
    for role in roles:
        if user_group in role.group.all():
            access.append({
                'id': role.id,
                'name': role.name,
                'url': role.url,
                'parent': role.parent.id if role.parent else None,
                'checked': 1,
                'usertype':request.session['usertype'],
            })
        else:
            access.append({
                'id': role.id,
                'name': role.name,
                'url': role.url,
                'parent': role.parent.id if role.parent else None,
                'checked': 0,
                'usertype':request.session['usertype'],
            })
    data = {'success': 1, 'user_roles': access}
    return JsonResponse(data)

# === Grant Group Access ===
def grant_group_access(request):
    # grant access to user
    data = request.POST.getlist('access_id[]')
    usergroup = Client_User_Group.objects.get(id=request.POST.get('groupid'),group_owner=request.session['userid'])
    for access_id in data:
        access = User_Configuration.objects.get(id=access_id)
        if (access.parent == None):
            child = User_Configuration.objects.filter(parent_id=access_id)
            if child.count() > 0:
                if request.POST.get('parent_value') == 'true':
                    for role in child:
                        role.group.add(usergroup)
                        role.save()
                        if User_Configuration.objects.filter(parent_id=role.id).count() > 0:
                            grand_child_access_call(role,access_id,usergroup,data,access)
                else:
                    for role in child:
                        role.group.remove(usergroup)
                        role.save()
                        if User_Configuration.objects.filter(parent_id=role.id).count() > 0:
                            grand_child_access_call(role,access_id,usergroup,data,access)
                
                if User_Configuration.objects.filter(parent_id=access_id,group__in=[usergroup]).count() == 0:
                    access.group.remove(usergroup)
                else:
                    access.group.add(usergroup)
                access.save()
            else:
                if usergroup not in access.group.all():
                    access.group.add(usergroup)
                else:
                    access.group.remove(usergroup)
                access.save()
    data = {'success':1,'msg':f'Access Permission Changed For {usergroup.group_name}'}
    return JsonResponse(data)

# === Grant Child Group Access ===
def grant_child_group_access(request):
    # child grant access to user
    data = request.POST.getlist('access_id[]')
    usergroup = Client_User_Group.objects.get(id=request.POST.get('groupid'),group_owner=request.session['userid'])
    for access_id in data:
        access = User_Configuration.objects.get(id=access_id)
        if (access.parent == None):
            child = User_Configuration.objects.filter(parent_id=access_id)
            for role in child:
                if str(role.id) in data:
                    if usergroup not in role.group.all():
                        role.group.add(usergroup)
                    else:
                        role.group.remove(usergroup)
                    role.save()
                if User_Configuration.objects.filter(parent_id=role.id).count() > 0:
                    grand_child_access_call(role,access_id,usergroup,data,access)
            if User_Configuration.objects.filter(parent_id=access_id,group__in=[usergroup]).count() == 0:
                access.group.remove(usergroup)
            else:
                access.group.add(usergroup)
            access.save()
    data = {'success':1,'msg':f'Access Permission Changed For {usergroup.group_name}'}
    return JsonResponse(data)

# === Grant Grand Child Access Permission to the group ===
def grant_grand_child_access(request):
    # grant grand child access permision to the group
    data = request.POST.getlist('access_id[]')
    usergroup = Client_User_Group.objects.get(id=request.POST.get('groupid'),group_owner=request.session['userid'])
    for access_id in data:
        access = User_Configuration.objects.get(id=access_id)
        if (access.parent == None):
            child = User_Configuration.objects.filter(parent_id=access_id)
            for sub_menu in child:
                if str(sub_menu.id) in data:
                    grand_child = User_Configuration.objects.filter(parent_id=sub_menu.id)
                    for grand_menu in grand_child:
                        if str(grand_menu.id) in data:
                            if usergroup not in grand_menu.group.all():
                                grand_menu.group.add(usergroup)
                            else:
                                grand_menu.group.remove(usergroup)
                            grand_menu.save()
                    if User_Configuration.objects.filter(parent_id=sub_menu.id,group__in=[usergroup]).count() == 0:
                        sub_menu.group.remove(usergroup)
                    else:
                        sub_menu.group.add(usergroup)
            if User_Configuration.objects.filter(parent_id=access_id,group__in=[usergroup]).count() == 0:
                access.group.remove(usergroup)
            else:
                access.group.add(usergroup)
            access.save()
    data = {'success':1,'msg':f'Access Permission Changed For {usergroup.group_name}'}
    return JsonResponse(data)


# === Get Group Users ===
def get_group_users(request):
    # return members in a group
    group_users = []
    group = Client_User_Group.objects.get(id=request.GET.get('group_id'),group_owner=request.session['userid'])
    for users in group.group_users.all().order_by('-created_date'):
        print(users)
        group_users.append({
            'userid': users.id,
            'name': users.user_name,
            'email': users.email,
            'status':users.is_active,
            'grp_id':request.GET.get('group_id'),
        })
    data = {'success':1,'group_users':group_users}
    return JsonResponse(data)

# === Remove Group User ===
def remove_group_user(request):
    # remove user from platform
    user.objects.get(id=request.POST.get('user_id')).delete()
    data = {'success':1}
    return JsonResponse(data)

# === Remove user from Group ===
def remove_user_from_grp(request):
    '''remove user from group'''
    get_group = Client_User_Group.objects.get(id=request.POST.get('group_id'))
    get_group.group_users.remove(user.objects.get(id=request.POST.get('user_id')))
    usr_config = User_Configuration.objects.filter(group=get_group)
    for usr in usr_config:
        usr.user.remove(user.objects.get(id=request.POST.get('user_id')))
        usr.save()
    get_group.save()
    data = {'success':1}
    return JsonResponse(data)

# === Add User to Group ===
def add_user_to_grp(request):
    '''add user to a group'''
    get_group = Client_User_Group.objects.get(id=request.POST.get('group_id'))
    get_group.group_users.add(user.objects.get(id=request.POST.get('user_id')))
    usr_config = User_Configuration.objects.filter(group=get_group)
    for usr in usr_config:
        usr.user.add(user.objects.get(id=request.POST.get('user_id')))
        usr.save()
    get_group.save()
    data = {'success':1}
    return JsonResponse(data)

# === Submit group Access ===
def submit_group_access(request):
    '''group access on submit button'''
    list_of_access = eval(request.POST.get('array'))
    group_obj = Client_User_Group.objects.get(id=request.POST.get('grp_id'))
    grp_users = group_obj.group_users.all()
    for i in list_of_access:
        config = User_Configuration.objects.get(id=i['id'])
        if i['check'] == 1:
            config.group.add(group_obj)
            for usr in grp_users:
                config.user.add(usr)
            config.save()
        elif i['check'] == 0:
            config.group.remove(group_obj)
            for usr in grp_users:
                config.user.remove(usr)
            config.save()
    data = {'success':1}
    return JsonResponse(data)

# === Submit User group Access ===
def submit_user_group_access(request):
    list_of_access = eval(request.POST.get('array'))
    usr = user.objects.get(id=request.POST.get('user_id'), usertype_id=6)
    for i in list_of_access:
        config = User_Configuration.objects.get(id=i['id'])
        if i['check'] == 1:
            config.user.add(usr)
            config.user_type.add(usertype.objects.get(id=6))
            config.save()
        elif i['check'] == 0:
            config.user.remove(usr)
            config.user_type.remove(usertype.objects.get(id=6))
            config.save()
    data = {'success': 1}
    return JsonResponse(data)

def set_camp_access(requset):
    # suryakant 27-09-2019
    # get campaign to set campaign wise access
    campaigns = [{'id':k,'name':v,'status':s,'check':0} for k,v,s in Campaign.objects.filter(user_id=requset.session['userid'],status__in=[1,2,3,4,5]).values_list('id','name','status')]
    rfq = [{'id':k,'name':v,'check':0} for k,v in RFQ_Campaigns.objects.filter(user_id=requset.session['userid']).values_list('id','name')]
    status_ = list(set([d['status'] for d in campaigns]))
    try:
        camp_access_obj = Campaign_access.objects.get(user_id=requset.GET['ext_id'])
        for camp in campaigns:
            if camp['id'] in camp_access_obj.campaign.all().values_list('id',flat=True):
                camp['check'] = 1
        for camp in rfq:
            if camp['id'] in camp_access_obj.rfq_campaign.all().values_list('id',flat=True):
                camp['check'] = 1
    except Exception as e:
        pass
    campaigns = {'campaigns':campaigns,'status_list':status_,'rfq':rfq}
    return JsonResponse(campaigns)


def submit_camp_access(requset):
    # suryakant 27-09-2019
    # saving campaign access
    campaigns_id =  [d['id'] for d in eval(requset.POST['campaign']) if d['check'] == 1] 
    rfq_campaigns_id =  [d['id'] for d in eval(requset.POST['rfq']) if d['check'] == 1]
    campaign_obj_list =  Campaign.objects.filter(id__in=campaigns_id)
    rfq_campaign_obj_list =  RFQ_Campaigns.objects.filter(id__in=rfq_campaigns_id)
    camp_access_obj,created = Campaign_access.objects.get_or_create(user_id=requset.POST['ext_id'])
    if len(campaigns_id)> 0 :
        camp_access_obj.campaign.clear()
        camp_access_obj.campaign.add(*campaign_obj_list)
    if len(rfq_campaigns_id) > 0 :
        camp_access_obj.rfq_campaign.clear()
        camp_access_obj.rfq_campaign.add(*rfq_campaign_obj_list)
    camp_access_obj.save()
    data = {'success': 1}
    return JsonResponse(data)

# === Action on Campaign by Client ===
def action_on_camp_by_client(status, lead_list, campaign_id, vendor_id, vendor, reason=None, status_code=None):
    '''for life cycle client actions'''
    if CampaignTrack.objects.filter(campaign_id=campaign_id).exists():
        camp_alloc = campaign_allocation.objects.get(campaign_id=campaign_id, client_vendor_id=vendor_id)
        track = CampaignTrack.objects.get(campaign_id=campaign_id)
        if status == 1:
            status = 'approved'
        elif status == 2:
            status = 'rejected'
        elif status == 3:
            status = 'rectify'

        if track.client_action_count > 0:
            list = eval(track.client_action)

            d = {}
            d['type'] = 'action'
            d['vendor_id'] = vendor_id
            d['vendor'] = vendor
            d['status'] = status
            d['lead_id'] = lead_list
            t = datetime.now()
            d['date'] = t.isoformat()
            d['reason'] = reason
            d['status_code'] = status_code
            d['vendor_percentage'] = vendorper(camp_alloc.id)
            d['client_percentage'] = percentage(campaign_id)
            list.append(d)
            track.client_action = list
            track.client_action_count += 1
            track.save()
            # print(list)
            # print('hello')
        else:
            list = []
            d = {}
            d['type'] = 'action'
            d['vendor_id'] = vendor_id
            d['vendor'] = vendor
            d['status'] = status
            d['lead_id'] = lead_list
            t = datetime.now()
            d['date'] = t.isoformat()
            d['reason'] = reason
            d['status_code'] = status_code
            d['vendor_percentage'] = vendorper(camp_alloc.id)
            d['client_percentage'] = percentage(campaign_id)
            list.append(d)
            track.client_action = list
            track.client_action_count += 1
            track.save()


# === Get Suppression File ===
def get_suppression_file(request):
# kishor Add this code
# download suppression csv file for client from database specification table
    camp_id = request.POST.get('id') # campaign id
    spec_data = Specification.objects.get(campaign_id=camp_id)
    suppression_file_data = None
    # check if, there is suppression file content in database table 'suppression'
    if spec_data.suppression_count:
        suppression_file_data = eval(spec_data.suppression_file_content)
        # get headers for csv file
        headers = []
        for i in range(len(suppression_file_data)):
            if i == 0:
                for header in suppression_file_data[i]:
                    if header == 'DOMAIN_NAME' or header == 'COMPANY_NAME' or header == 'id':
                        headers.append(header)
        headers.insert(0,headers.pop())
        # create suppression csv file in 'media' folder
        with open(os.path.join(settings.MEDIA_ROOT, 'suppression_file.csv'),'w') as file:
            # write headers
            writer = csv.writer(file)
            writer.writerow(headers)
            # copy content of specification table field 'suppression_file_content' into file
            for data in suppression_file_data:
                L = []
                for i in data:
                    if i == 'DOMAIN_NAME' or i == 'COMPANY_NAME' or i == 'id':
                        L.append(data[i])
                L.insert(0,L.pop())
                writer = csv.writer(file)
                writer.writerow(L)
    # return file path to download file
    path = {'path': '/media/' + 'suppression_file.csv'}
    return JsonResponse(path)

# === Get ABM File ===
def get_ABM_file(request):
# Akshay G.
# download ABM csv file for client from database specification table
    camp_id = request.POST.get('id') # campaign id
    spec_data = Specification.objects.get(campaign_id=camp_id)
    abm_file_data = None
    # check if, there is ABM file content in database table 'suppression'
    if spec_data.abm_count:
        abm_file_data = eval(spec_data.abm_file_content)
        # get headers for csv file
        headers = []
        for i in range(len(abm_file_data)):
            if i == 0:
                for header in abm_file_data[i]:
                    if header == 'DOMAIN_NAME' or header == 'COMPANY_NAME' or header == 'id':
                        headers.append(header)
        headers.insert(0,headers.pop())
        # create ABM csv file in 'media' folder
        with open(os.path.join(settings.MEDIA_ROOT, 'abm_file.csv'),'w') as file:
            # write headers
            writer = csv.writer(file)
            writer.writerow(headers)
            # copy content of specification table field 'abm_file_content' into file
            for data in abm_file_data:
                L = []
                for i in data:
                    if i == 'DOMAIN_NAME' or i == 'COMPANY_NAME' or i == 'id':
                        L.append(data[i])
                L.insert(0,L.pop())
                writer = csv.writer(file)
                writer.writerow(L)
    # return file path to download file
    path = {'path': '/media/' + 'abm_file.csv'}
    return JsonResponse(path)

# === Docusign ===
def  docusign(request):
    return render(request, 'docusignpdf.html', {})

# === All Vendor List ===
@is_client
def all_vendor_list(request):
# show all vendors in Explore vendor-vendor list..Akshay G.
    extr_vendorlist = []
    user_id = request.user.id
    apilinks = ApiLinks.objects.all()
    ext_vendor_list = user.objects.get(id=user_id).child_user.all()
    extr_vendorlist = ext_vendor_list.filter(usertype_id__in = [5,2,9])
    tc_vendorlist = []
    campaigns = Campaign.objects.filter(user=user_id)
    vendor_list = campaign_allocation.objects.filter(
        campaign_id__in=campaigns, status__in=[1, 5, 4])
    # corrected to remove superadmin id from vendor list # suryakant 15-10-2019
    ids = [d.client_vendor.id for d in vendor_list]    
    ids += [d.id for d in user.objects.filter(usertype_id=2, status=3).exclude(id__in=ids)]
    for row in ids:
        if row != request.user.id:
            user_details = client_vendor.objects.filter(
                user_id=row)
            if user_details:
                if user_details[0] not in tc_vendorlist:
                    tc_vendorlist.append(user_details[0])
    favorite_vendorlist = request.user.favorite_vendor.values_list('id', flat = True)
    return render(request, 'vendor1/all_vendorlist.html', {'tc_vendorlist': tc_vendorlist,
                'apilinks': apilinks,'extr_vendorlist':extr_vendorlist, 'favorite_vendorlist':favorite_vendorlist})

# === Add Faviorite Vendor ===
@is_client
def add_favorite_vendor(request):
# add or remove vendor in/from favorite vendor list of client..Akshay G.
    vendor_id = request.POST.get('vendor_id')
    vendor_obj = user.objects.get(id = vendor_id)
    if request.user.favorite_vendor.filter(id = vendor_id).exists():
        request.user.favorite_vendor.remove(vendor_obj)
    else:
        request.user.favorite_vendor.add(vendor_obj)
    return HttpResponse()

# === Save Rejected Reason ===
def save_rejected_reason(request):
# add Rejected Reason Leads Kishor...!
    rejected_reason = request.POST.get('name')
    if rejected_reason:
        obj = leads_rejected_reson.objects.create(user_id=request.session['userid'])
        obj.reason = rejected_reason
        obj.save()
    data = { 'rejected_reason': rejected_reason}
    return JsonResponse(data)

# === Save Rectify Reason ===
def save_rectify_reason(request):
#add Rectify Reason Leads Kishor...!
    rectify_reason = request.POST.get('name')
    print(rectify_reason)
    if rectify_reason:
        obj = Leads_Rectify_Reason.objects.create(user_id=request.session['userid'])
        obj.reason = rectify_reason
        obj.save()
    data = { 'rectify_reason': rectify_reason}
    return HttpResponse()

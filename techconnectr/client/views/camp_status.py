from campaign.models import *
from form_builder.models import *

# check filled/unfilled campaign specs for draft campaigns..Akshay G. 2nd Aug, 2019
def draft_camp_status(camp_obj):
    camp_parameter = {
                        'cpl':False,
                        'target_quantity': False,
                        'start_date': False,
                        'end_date': False,
                        'suppression_status': False,
                        'abm_status': False,
                        'custom_question': False,
                        'company_size': False,
                        # 'revenue_size': False,
                        'industry_type': False,
                        'job_title': False,
                        'type': False,
                    }
    camp_status = {camp_obj.id: camp_parameter}
    if camp_obj.cpl > 0:
        camp_parameter['cpl'] = True
    if camp_obj.target_quantity != None:
        if camp_obj.target_quantity > 0:
            camp_parameter['target_quantity'] = True
    if camp_obj.start_date != None:
        camp_parameter['start_date'] = True
    if camp_obj.end_date != None:
        camp_parameter['end_date'] = True
    camp_specs = Specification.objects.get(campaign = camp_obj)
    if camp_specs.suppression_status == 1:
        if camp_specs.suppression_count > 0:
            camp_parameter['suppression_status'] = True
    else:
        del camp_parameter['suppression_status']
    if camp_specs.abm_status == 1:
        if camp_specs.abm_count > 0:
            camp_parameter['abm_status'] = True
    else:
        del camp_parameter['abm_status']
    camp_mapping = Mapping.objects.get(campaign = camp_obj)
    camp_question = camp_obj.question_forms_set.all()
    if camp_mapping.custom_question > 0:
        if camp_question:
            camp_question = camp_question[0]
            if camp_question.questions != None:
                if len(eval(camp_question.questions)) > 0:
                    camp_parameter['custom_question'] = True
    else:
        del camp_parameter['custom_question']
    if camp_mapping.company_size != '0':
        camp_parameter['company_size'] = True
    # if camp_mapping.revenue_size != '0':
    #     camp_parameter['revenue_size'] = True
    if camp_mapping.industry_type != None and camp_mapping.industry_type != '':
        camp_parameter['industry_type'] = True
    if camp_mapping.job_title != None and camp_mapping.job_title != '':
        camp_parameter['job_title'] = True
    if camp_specs.abm_status == 1:
        del camp_parameter['company_size']
        del camp_parameter['industry_type']
    # validate if camp type is entered...Akshay G...Date - 9th Nov, 2019
    from setupdata.models import source_touches
    ids_list = source_touches.objects.all().values_list('id', flat = True)
    if camp_obj.type:
        if camp_obj.type.isdigit():
            # validate if camp type value is id of any of the source touches objs 
            if int(camp_obj.type) in ids_list:
                camp_parameter['type'] = True
    return camp_status

# check filled/unfilled campaign specs for pending campaigns..Akshay G. 2nd Aug, 2019
def pending_camp_status(camp_obj):
    camp_parameter = {
                        'vendor_allocation': False,
                        'lead_approved_status': False,
                    }
    camp_status = {camp_obj.id: camp_parameter}
    camp_allocate = camp_obj.campaign_allocation_set.all()
    if(camp_allocate):
        camp_parameter['vendor_allocation'] = True
        if (camp_allocate.filter(status__in = [1, 5])):
            camp_parameter['lead_approved_status'] = True
    return camp_status

# check filled/unfilled campaign specs for assigned campaigns..Akshay G. 2nd Aug, 2019
def assigned_camp_status(camp_obj):
    camp_parameter = {
                        'camp_id': camp_obj.id,
                        'delivery_status': False,
                        # 'agreement_status': False,
                    }
    camp_status = {camp_obj.id: camp_parameter}
    camp_delivery = Delivery.objects.get(campaign = camp_obj)
    if camp_delivery.tc_header_status == 1 or camp_delivery.custom_header_status == 1:
        camp_parameter['delivery_status'] = True
    return camp_status

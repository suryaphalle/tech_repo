import difflib  # get ratio of string similarity
import functools
import json
import csv
import re
from xlrd import open_workbook
import operator
import random
from operator import itemgetter
from user.models import *
from user.models import user
import datetime
from django.conf import settings
from django.core import serializers
from django.core.files.storage import FileSystemStorage
from django.db.models import Q
from django.http import HttpResponse, JsonResponse, Http404
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import resolve, reverse
from django.views import View
from django.views.decorators.csrf import csrf_exempt
from campaign.forms import *
from campaign.models import *
from client.decorators import *
from client.models import *
from client.utils import *
from client.views.views import *
from client_vendor.models import *
from client_vendor.models import client_vendor
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from setupdata.models import *
from vendors.views.views import *
from form_builder.views import *
from leads.models import *
from itertools import chain
from operator import itemgetter
from vendors.views.views_1 import companyRevenueValidate,companySizeValidate
from campaign.choices import *

# === Lead Report ===
@login_required
@is_client
def leadreports(request):
    # This Function Used for load data in lead report page and display. 
    campaign_list = client_manage_campaign(request)
    return render(request, 'reports/leadReport/leadsReport.html', {'campaign_list': campaign_list})

# === Get Vendor List Campaign ===
def get_vendor_list_campaign(request):
    # This funtions used for get list of vendor according to campaign ID.
    camp_id = request.POST.get('camp_id')
    vendor_list = campaign_allocation.objects.filter(
        campaign_id=camp_id, status=1)
    vendors = []
    for vendor in vendor_list:
        if vendor.submited_lead > 0:
            vendors.append({'id': str(vendor.client_vendor.id),
                            'name': str(vendor.client_vendor.user_name)})
    data = {'vendors': vendors}
    return JsonResponse(data)

# === Get Geo list Campaign ===
def get_geo_list_campaign(request):
    # This funtions used for get list of Geo according to campaign ID.
    camp_id = request.POST.get('camp_id')
    geo_list = Mapping.objects.get(campaign_id=camp_id, status=1)
    geo_list = geo_list.country.split(',')
    data = '<option value="">Select Geo</option>'
    for geo in geo_list:
        data += '<option value="' + str(geo) + '">' + str(geo) + '</option>'
    return HttpResponse(data)

# === Lead Report Table Data ===
def lead_report_table_data(request):
    # suryakant
    # This funtions used for get report table data.
    camp_id = int(request.POST.get('camp_id'))
    vendor_id = request.POST.getlist('vendor_id[]') if request.POST.getlist('vendor_id[]') else []
    status = request.POST.getlist('status') if request.POST.getlist('status') else []
    vendors = []
    lead_data = []
    vendors_list = []
    all_headers = []
    PieChartData = []
    StackChartData = []
    all_LineChartData = []
    custom_filter = get_custom_filter_data(camp_id)

    # get data as per vendor id
    if camp_id > 0 and len(vendor_id) > 0:
        vendors = campaign_allocation.objects.filter(campaign_id=camp_id, client_vendor_id__in=vendor_id, status=1)
    else:
        vendors = campaign_allocation.objects.filter(campaign_id=camp_id, status=1)

    for lead in vendors:
        vendors_list.append({'id': lead.client_vendor.id,
                             'name': lead.client_vendor.user_name})
        # collect data for pie chart and stack chart
        PieChartData.append(pie_chart_data(lead))
        StackChartData.append(stack_chart_data(lead))
        if lead.submited_lead > 0:
            leads = remove_custom_lead_headers(eval(lead.upload_leads),camp_id) 
            lead_data += leads
            # collect data for line chart
            all_LineChartData += line_chart_data(lead)
            # collect all headers
            if all_headers == [] or all_headers != list(lead_data[0].keys()):
                all_headers = list(lead_data[0].keys())
    
    # get leads according to status
    if status != [''] and status != ['null']:
        lead_data = get_lead_data(lead_data, status)
    
    # get leads as per the date range
    if request.POST.get('start_date') != '0' and request.POST.get('end_date') != '0' and request.POST.get('start_date') != '' and request.POST.get('end_date') != '':
        lead_data = apply_date_filter(lead_data,request.POST.get('start_date'),request.POST.get('end_date'))
    
    # get leads as per custom filters
    if request.POST.get('custom') not in [None,'[]',[],'null'] :
        if len(request.POST.get('custom')) > 0:
            custom_filter_req = eval(request.POST.get('custom'))
            if custom_filter_req != []:
                lead_data = apply_custom_filter(lead_data, custom_filter_req)

    data = {'vendors': vendors_list, 'submitted_leads': lead_data, 'all_headers': all_headers,
            'PieChartData': PieChartData, 'StackChartData': StackChartData, 'LineChartData': all_LineChartData,'custom_filter':custom_filter}
    return JsonResponse(data)

# === Lead Report ===
def pie_chart_data(camp_alloc):
    # sreturn data for pie chart --suryakant
    pc = {
        'name': camp_alloc.client_vendor.user_name,
        'leads': camp_alloc.volume,
    }
    return pc

# === Stack Chart Data Report ===
def stack_chart_data(camp_alloc):
    # return data for stackbar chart --suryakant
    if camp_alloc.return_lead != 0 or camp_alloc.approve_leads != 0 or camp_alloc.submited_lead != 0:
        std = {
            'leads': camp_alloc.volume,
            "Name": camp_alloc.client_vendor.user_name,
            "Approved Leads": camp_alloc.approve_leads,
            "Reject Leads": camp_alloc.return_lead,
            "Submited Leads": camp_alloc.submited_lead,
        }
    else:
        std = {
            'leads': camp_alloc.volume,
            "Name": camp_alloc.client_vendor.user_name,
            # "Submited Leads": camp_alloc.submited_lead,
        }
    return std

# === Line Chart Data ===
def line_chart_data(camp_alloc):
    # return data for line chart --suryakant 
    date = {}
    linedata = []
    if camp_alloc.submited_lead > 0:
        count = 1
        vendor_name = camp_alloc.client_vendor.user_name
        for lead in list(ast.literal_eval(camp_alloc.upload_leads)):
            if date == {}:
                date = {'vname': vendor_name,
                        'date': lead['date'], 'count': count}
            else:
                if linedata == []:
                    linedata.append(date)
                else:
                    if lead['date'] != date['date']:
                        if date['date'] not in [ld['date'] for ld in linedata]:
                            linedata.append(date)
                        count = 1
                        date = {'vname': vendor_name,
                                'date': lead['date'], 'count': count}
                    else:
                        count += 1
                        date.update({'count': count})

        if date['date'] not in [ld['date'] for ld in linedata]:
            linedata.append(date)
        return linedata

# === get Lead Data ===
def get_lead_data(data, status):
    # get lead data as per status --suryakant
    updated_leads = []
    for lead in data:
        if lead['status'] in [int(d) for d in status[0].split(',')]:
            updated_leads.append(lead)
    return updated_leads

# === Apply Date Filter ===
def apply_date_filter(lead_data,start_date,end_date):
    # get lead as per the dates --suryakant
    import datetime
    updated_leads = []
    st_dt = datetime.datetime.strptime(start_date,"%Y-%m-%d")
    en_dt = datetime.datetime.strptime(end_date,"%Y-%m-%d")
    for lead in lead_data:
        ld_dt = datetime.datetime.strptime(lead['date'],"%B %d %Y")
        if st_dt <= ld_dt <= en_dt:
            updated_leads.append(lead)
    return updated_leads

# === Get Custom Filter Data ===
def get_custom_filter_data(camp_id):
    # get custom filter options --suryakant 
    mapping_data = Mapping.objects.get(campaign_id=camp_id)
    data = []
    industry = mapping_data.industry_type.split(',')
    if industry != ['']:
        data.append({'industry':industry})

    job_title = mapping_data.job_title.split(',')
    if job_title != ['']:
        data.append({'job_title':job_title})

    country = mapping_data.country.split(',')
    if country != ['']:
        data.append({'country':country})
        
    company_size = mapping_data.company_size.split(',')
    if company_size != ['']:
        data.append({'company_size':company_size})

    revenue_size = mapping_data.revenue_size
    temp_rev = revenue_size.replace('0 - $999,999','0-9').replace(',',', ').replace('0-9','0 - $999,999')
    revenue_size = temp_rev.split(', ')
    if revenue_size != ['']:
        data.append({'revenue_size':revenue_size})
    return data

# === Apply Custom Filter ===
def apply_custom_filter(data,filter): 
    # return filtered data  with custom filter
    import itertools
    updated_leads = []
    keys = {}
    values = []
    for d in filter:
        for k,v in d.items():
            keys.update({k:v})
        if k == 'job_title':
            values += [d.lower() for d in v]
        if k == 'industry':
            value_list = [d.split('/') for d in v]
            dat = list(itertools.chain.from_iterable(value_list))
            values += [d.lower() for d in dat]
        if k == 'country':
            values += [d.lower() for d in v]
    # print(values)
    for lead in data:
        flag = 0
        for key in keys:
            if key == "job_title":
                if 'job_title' in lead.keys():
                    if lead['job_title'].lower() in [x for x in values]:
                        flag = 1
                    else:
                        flag = 0
                        break
            elif key == 'revenue_size':
                sizes = company_revenue_size(keys[key])
                if 'Revenue' in lead.keys():
                    if companyRevenueValidate(sizes,lead['Revenue']):
                        flag = 1
                    else:
                        flag = 0
                        break
            elif key == 'company_size':
                if 'Employee_size' in lead.keys():
                    sizes = company_size(keys[key])
                    if companySizeValidate(sizes,lead['Employee_size']):
                        flag = 1
                    else:
                        flag = 0
                        break
            else:
                if key.capitalize() in [d for d in lead.keys()]:
                    if lead[key.capitalize()].lower() in [x for x in values]:
                        flag = 1
                    else:
                        flag = 0
                        break
                else:
                    flag = 1
        if flag == 1:
            updated_leads.append(lead)
    return updated_leads

# === Company Revenue Size ===
def company_revenue_size(revenue_size):
    # Convert Company revenue into List.
    company_revenue_size=[0]
    sizes_range=[]
    revenue_size = (', ').join(revenue_size)
    if revenue_size:
        sizes=list(re.split(''',(?=(?:[^'"]|'[^']*'|"[^"]*")*$)''',revenue_size))
        
        if len(sizes) >= 2:
            if '999' in sizes and '0 - $999' in sizes: # when client select 0-$999,999 or more values that time
                sizes.remove('999')
                sizes.remove('0 - $999')
                sizes.append('0-$999999')
        else:
            if '999' in sizes: # when client select only 0-$999,999  values that time
                return [0,999999]
        for size in sizes:
            sizes_range +=list(size.split('-'))
        company_revenue_size=[]

        sizes_range=[s.replace(' ', '') for s in sizes_range]
        sizes_range=[s.replace('million', '000000') for s in sizes_range]
        sizes_range=[s.replace('mill', '000000') for s in sizes_range]
        sizes_range=[s.replace('m', '000000') for s in sizes_range]
        sizes_range=[s.replace('$', '') for s in sizes_range]
        sizes_range=[s.replace('billion','000000000')for s in sizes_range]
        sizes_range=[s.replace('bill','000000000')for s in sizes_range]
        sizes_range=[s.replace('b','000000000')for s in sizes_range]
        if len(sizes_range) == 1 and '10000000000+' in sizes_range:
            sizes_range=[s.replace('+','00000')for s in sizes_range]
            sizes_range.append(1000000000)
        else:
            sizes_range=[s.replace('+','00000')for s in sizes_range]    
        sizes_range = list(map(int, sizes_range))
        company_revenue_size.append(min(sizes_range))
        company_revenue_size.append(max(sizes_range))
    return company_revenue_size

# === Company Size ===
def company_size(size):
    # Convert Company Size into List.
    company_size=[0]
    size = (', ').join(size)
    if size:
        sizes=list(size.split(','))   
        listSize=[]
        company_size=[]
        for size in sizes:
            listSize +=list(size.split('-'))
            listSize = [x.strip(' ') for x in listSize]
        if '10000+' in listSize: # if client set company size 10000+ that time set highest value in list
            if len(listSize) == 1:
                listSize.remove('10000+')
                company_size.append(10000)
            else:
                listSize.remove('10000+')
                listSize = list(map(int, listSize))
                company_size.append(min(listSize))    
            company_size.append(100000000000)
        else:
            listSize = list(map(int, listSize))
            company_size.append(max(listSize))
            company_size.append(min(listSize))        
    return company_size

# === Campaign Reports ===
def camp_reports(request):
    # return all cmapaigns for campaign report page
    all_camp = client_manage_campaign(request)
    campaigns_data = []
    filter_data = []
    camp_geo = []
    cpl = []
    target = []
    custom = []
    for camp in all_camp.filter(status__in=[4,5,1]):
        map_data = Mapping.objects.get(campaign_id = camp.id)
        campaigns_data.append({
            "campaign_name":camp.name,
            "client_name":camp.user.user_name,
            "start_date":camp.start_date,
            "end_date":camp.end_date,
            "custom_qutions":map_data.custom_question,
            "cpl":camp.cpl,
            "quatity":camp.target_quantity,
            "geo":camp.geo,
            "method":(',').join([d.type for d in camp.method.all()]),
            "type":camp.get_type_display(),
            "status":camp.status,
        })
        cpl.append(camp.cpl)
        target.append(camp.target_quantity)
        custom.append(map_data.custom_question)
        if camp.geo:
            camp_geo += camp.geo.split(',')  
    
    campaign_types = [d[1] for d in CAMPAIGN_TYPES_CHOICES if d[0] != '']
    filter_data.append({
        'geo':Remove(camp_geo),
        'campaign_types':campaign_types,
        'min_cpl':min(cpl),
        'max_cpl':max(cpl),
        'min_target':min(target),
        'max_target':max(target),
        'min_custom':min(custom),
        'max_custom':max(custom),
    })
    return render(request, 'reports/campaign_report/campaign_report.html', {'campaign_list': campaigns_data,'all_camp':all_camp,'filter_data':filter_data})

# === Remove ===
def Remove(duplicate): 
# Python code to remove duplicate elements 
    final_list = [] 
    for num in duplicate: 
        if num not in final_list: 
            final_list.append(num) 
    return final_list 

# === Campaign Filters ===
def campaign_filters(request):
    # apply filter on clients campaigns data
    campaigns_data = []
    st_dt, en_dt, date_flag = '', '',''
    cpl = [float(request.POST['min_cpl']),float(request.POST['max_cpl'])]
    vol = [int(request.POST['min_vol']),int(request.POST['max_vol'])]
    custom = [int(request.POST['min_custom']),int(request.POST['max_custom'])]
    status = request.POST.getlist('status')
    camp_type = request.POST.getlist('type[]')
    geo = request.POST.getlist('geo[]')
    print(request.POST)
    if request.POST.get('start_date') != '0' and request.POST.get('end_date') != '0' and request.POST.get('start_date') != '' and request.POST.get('end_date') != '':
        st_dt = datetime.strptime(request.POST['start_date'],"%Y-%m-%d").date()
        en_dt = datetime.strptime(request.POST['end_date'],"%Y-%m-%d").date()
        date_flag = request.POST['date_flag']
    filtered_campaigns = Campaign.objects.filter(user_id=request.session['userid'],cpl__gte=cpl[0],cpl__lte=cpl[1]+1,target_quantity__gte=vol[0],target_quantity__lte=vol[1]+1)
    for index,camp in enumerate(filtered_campaigns):
        flag = 0
        map_data = Mapping.objects.get(campaign_id = camp.id)
        if status[0] != '':
            print('status',index+1,camp.status in status[0].split(','))
            if str(camp.status) in status[0].split(','):
                flag = 1
            else:
                flag = 0
                continue
        if custom:
            print('custom',index+1,map_data.custom_question in range(custom[0],custom[1]+1))
            if map_data.custom_question in range(custom[0],custom[1]+1):
                flag = 1
            else:
                flag = 0
                continue
        if geo:
            if camp.geo != '':
                print(f'geo : {camp.geo}',index+1,set(geo).issubset(set(camp.geo.split(','))))
                if(set(geo).issubset(set(camp.geo.split(',')))): 
                    flag = 1
                else:
                    flag = 0
                    continue
            else:
                flag = 0
                continue
        if camp_type:
            types = [d[0] for d in CAMPAIGN_TYPES_CHOICES if d[1] in camp_type]
            print('type',camp.type in types)
            if camp.type in types:
                flag = 1
            else:
                flag = 0
                continue
        if st_dt != '' and en_dt != '' and date_flag != '':
            if date_flag == 'start_date':
                print(index, date_flag,st_dt <= camp.start_date <= en_dt)
                if st_dt <= camp.start_date <= en_dt:
                    flag = 1   
                else: 
                    flag = 0
                    continue
            if date_flag == 'end_date':
                print(index, date_flag,st_dt <= camp.end_date <= en_dt)
                if st_dt <= camp.end_date <= en_dt:
                    flag = 1  
                else: 
                    flag = 0
                    continue
        if flag == 1:
            campaigns_data.append({
                "campaign_name":camp.name,
                "client_name":camp.user.user_name,
                "start_date":camp.start_date,
                "end_date":camp.end_date,
                "custom_qutions":map_data.custom_question,
                "cpl":camp.cpl,
                "quatity":camp.target_quantity,
                "geo":camp.geo,
                "method":(',').join([d.type for d in camp.method.all()]),
                "type":camp.get_type_display(),
                "status":camp.status,
            })
        ''' campaigns_data = apply_date_filter(campaigns_data,request.POST.get('start_date'),request.POST.get('end_date')) '''
    data = {'status':1,'campaigns_data':campaigns_data}
    return JsonResponse(data)
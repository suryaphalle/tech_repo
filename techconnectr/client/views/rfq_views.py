import ast
import difflib  # get ratio of string similarity
import functools
import json
import datetime
import operator
import random
from operator import itemgetter
from user.models import *
from user.models import user

from django.conf import settings
from django.core import serializers
from django.core.files.storage import FileSystemStorage
from django.db.models import Q
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import resolve, reverse
from django.views import View
from django.views.decorators.csrf import csrf_exempt

from campaign.forms import *
from campaign.models import *
from client.decorators import *
from client.models import *
from client.utils import *
from client.views.views import *
from client_vendor.models import *
from client_vendor.models import client_vendor
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from setupdata.models import *
from setupdata.models import (cities, countries, countries1 as countries_list, industry_speciality,
                              industry_type, states, states1 as state_list)
from setupdata.serializer import Cityserializers, Stateserializers
from vendors.views.views import *
from campaign.choices import *
from setupdata.models import (cities, countries,countries1 as countries_list1, industry_speciality,
                              industry_type, states, states1)

# === RFQ Campaign ===
@is_client
def RFQ_campaign(request):
    # create rfq new function
    import datetime
    countries1 = countries_list.objects.all()
    region_list = region1.objects.all()
    # set tomorrow as min date for rfq timer date..Akshay G.
    datetime_obj = datetime.datetime.now() + datetime.timedelta(hours = 24)
    rfq_date = datetime_obj.date().strftime("%d/%m/%Y")
    # 'data_dict' - To send selected camp data..Akshay G.
    data_dict = {
                'outreach_method' : [],
                'selected_industry' : [],
                'selected_company_size' : [],
                'selected_revenue_size' : [],
                'selected_country' : [],
                'selected_region' : [],
                'selected_country_list': [],
                'total_selected_countries': '',
            }
    if request.method == 'POST':
        form = RFQCampaignForm(request.POST, request.FILES)
        abm_Status = form['abm_status'].value()
        # fetch selected camp data..Akshay G..Date - 18th Oct, 2019
        data_dict = create_or_edit_rfq_camp(form, data_dict)
        if form.is_valid():

            expiry = datetime.datetime.strptime(form.cleaned_data['rfq_timer'], '%d/%m/%Y %H:%M')
            expiry = datetime.datetime.isoformat(expiry)
            RFQ = form.save(commit=False)
            RFQ.user_id = request.session['userid']
            status, form, d, RFQ = validate_form(RFQ, form, abm_Status)
            RFQ.rfq_timer = expiry
            if form.cleaned_data['abm_file'] is not None:
                RFQ.abm_status = True
                RFQ.abm_file_content = form.cleaned_data['abm_file']
            if form.cleaned_data['suppression_file'] is not None:
                RFQ.suppression_status = True
                RFQ.suppression_file_content = form.cleaned_data['suppression_file']
            if status:
                RFQ.save()
                suggest_vendors(request,RFQ)
                title = 'New RFQ Campaign'
                desc = "A RFQ Campaign '" + str(RFQ.name) + "' created by " + str(RFQ.user.user_name)

                sender_id = request.session['userid']
                superadmins = user.objects.filter(usertype__id=4)

                from client.utils import noti_via_mail
                for superad in superadmins:
                    RegisterNotification(sender_id, superad.id, desc, title, 1, None, None)

                noti = noti_via_mail([superad.id for superad in superadmins], title, desc, mail_new_rfq_campaign)

                return redirect('client_rfq_campaign')
        else:
            company_size = form.cleaned_data.get('company_size')
            revenue_size = form.cleaned_data.get('revenue_size')
            industry_types = form.cleaned_data['industry_type']
            if company_size is not None and revenue_size is not None and abm_Status == False:
                if len(company_size) == 0 and len(revenue_size) == 0:
                    msg = "Either company size or revenue size is required."
                    form.add_error('revenue_size', msg) 
            if len(industry_types) == 0 and abm_Status == False:
                form.add_error('industry_type', "This field is required.")
    else:
        form = RFQCampaignForm()
    context = {
        'campaignform': form,
        'countries': countries1,
        'region_list': region_list,
        'rfq_date': rfq_date,
        **data_dict,
    }
    return render(request, 'campaign/create_rfq_campaign.html', context)

# === Create RFQ Campaign ===
@is_client
def create_rfq_campaign(request):
    # countries1 = countries.objects.all()
    countries1 = countries_list.objects.all()
    region_list = region1.objects.all()
    # region_list=region.objects.all()
    industry_types = industry_type.objects.filter(status=0)
    company_sizes = company_size.objects.all()
    vendor_type = VendorType.objects.all()
    revenue_size = RevenueSize.objects.all()
    message, success, title = "", 0, "error"
    abm_status, suppression_status, lead_validation_list = 0, 0, []
    if request.method == "POST":
        form = CampaignForm(request.POST)
        if form.is_valid():
            form.save()
            campaign = Campaign.objects.latest("id")
            if request.POST.get('target_quantity') == '':
                campaign.target_quantity = 0
                campaign.client_raimainingleads = 0
                campaign.client_target_quantity = 0
                campaign.save()
            campaign_id = campaign.id

            # converting normal datetime to iso format for passing to js function
            import datetime
            expiry = datetime.datetime.strptime(request.POST.get('rfq_timer'), '%Y/%m/%d %H:%M')
            expiry = datetime.datetime.isoformat(expiry)
            expiry_timer = Campaign.objects.get(id=campaign_id)
            expiry_timer.rfq_timer = expiry
            expiry_timer.save()

            # create other table with empty data
            # so we can open edit case paer page directly

            if 'abm_status' not in request.POST:
                abm_status = 0
            else:
                abm_status = 1

            if 'suppression_status' not in request.POST:
                suppression_status = 0
            else:
                suppression_status = 1

            specification = Specification.objects.create(
                campaign_id=campaign_id, abm_status=abm_status, suppression_status=suppression_status)

            # to save abm and supp file on rfq campaign - amey raje
            if request.FILES:
                for filename, file in request.FILES.items():
                    name = filename
                    myfile = request.FILES[filename]
                    fs = FileSystemStorage()
                    filename = fs.save("campaign/" + myfile.name,  myfile)
                    from client.views.views import upload_file_specs
                    upload_file_specs(campaign, filename, name)

            mapping = Mapping.objects.create(campaign_id=campaign_id, industry_type=','.join(request.POST.getlist(u'industry_type')), special_instructions=request.POST.get('special_instructions'), job_title=request.POST.get('job_title_function'), job_title_function=request.POST.get('job_title_function'),
                                             country=','.join(request.POST.getlist('geo')), company_size=','.join(request.POST.getlist('company_size')), revenue_size=','.join(request.POST.getlist('revenue_size')), custom_question=int(request.POST.get('custom_question', 0)))
            terms = Terms.objects.create(campaign_id=campaign_id)
            delivery = Delivery.objects.create(campaign_id=campaign_id)

            # store default lead validation in database
            lead_validation = LeadValidationComponents.objects.filter(
                is_default=1)

            for lead in lead_validation:
                lead_validation_list.append(lead.function_name)

            SelectedLeadValidation.objects.create(
                campaign=campaign, component_list=lead_validation_list)
            HeaderSpecsValidation.objects.create(
                campaign=campaign, company_limit=4)

            message = "Campaign Created Successfully"

            # add notification
            title = 'New RFQ Campaign Created'
            desc = "Campaign named '" + \
                str(campaign.name) + "' created successfully"
            sender_id = request.session['userid']
            receiver_id = request.session['userid']
            superadmins = user.objects.filter(usertype__id=4)
            from client.utils import noti_via_mail
            for superad in superadmins:
                RegisterNotification(sender_id, superad.id, desc,
                                     title, 1, campaign, None)
            # send request to vendors for rfq campaign
            noti = noti_via_mail([superad.id for superad in superadmins], title, desc, mail_new_rfq_campaign)

            '''
            rfq_vendor_allocation(campaign_id, sender_id)
            add set as default data
            objects_list = [campaign, specification, mapping, terms, delivery]
            hook_add_campaign_data(campaign.user, objects_list)
            '''
            return redirect('client_rfq_campaign')
        else:
            message += str(form.errors)
            specificationform = SpecificationForm()
            print(message)
        # return HttpResponse(message)
        context = {
            'campaignform': RFQCampaignForm,
            'region_list': region_list,
            'countries': countries1,
            "industry_types": industry_types,
            'mappingform': MappingForm(),
            "specificationform": specificationform,
            "company_sizes": company_sizes,
            "vendor_type": vendor_type,
            "revenue_size": revenue_size,
            "company_limit": 4,
        }
        # send with errors
        return render(request, 'campaign/create_rfq_campaign.html', context)

    # get method
    else:
        campaignform = RFQCampaignForm()
        specificationform = SpecificationForm()
        context = {
            'campaignform': campaignform,
            'region_list': region_list,
            'countries': countries1,
            "industry_types": industry_types,
            'mappingform': MappingForm(),
            "specificationform": specificationform,
            "company_sizes": company_sizes,
            "vendor_type": vendor_type,
            "revenue_size": revenue_size,
            "company_limit": 4,
        }
        # return render(request,'campaign/createcampaign.html', context)
        return render(request, 'campaign/create_rfq_campaign.html', context)

# === RFQ Vendor Allocation ===
def rfq_vendor_allocation(camp_id, userid):
    # send request all venodr according to campaign outrich method
    # RFQ Vendor Allocation
    ids = get_ID_OutrichMethod(camp_id, userid)
    title = "New RFQ Campaign Arrived"
    desc = ""
    mail_list = []
    for id in ids:
        counter = campaign_allocation.objects.filter(
            campaign_id=camp_id, client_vendor_id=id, cpl=-1, volume=-1, status=3).count()
        allocation = campaign_allocation.objects.filter(
            campaign_id=camp_id, client_vendor_id=id)
        if counter != 1 and allocation.count() != 1:
            campaign_allocation.objects.create(
                campaign_id=camp_id, client_vendor_id=id, cpl=-1, volume=-1, status=3)
        else:
            if allocation:
                allocation[0].cpl = -1
                allocation[0].volume = -1
                allocation[0].status = 3
                allocation[0].save()
        camp_alloc = campaign_allocation.objects.latest('id') if allocation[0] != campaign_allocation.objects.latest('id') else allocation[0]
        mail_list.append(camp_alloc.client_vendor_id)
        RegisterNotification(userid, id, desc, title, 2, None, camp_alloc)
    from client.utils import noti_via_mail
    noti = noti_via_mail(mail_list, title, 'New RFQ campaign '+Campaign.objects.get(id=camp_id).name +' is arrived. Techconnectr requests quotation from your side', mail_rfq_quotation_request)
    return True

# === Get ID Outrichmethod ===
def get_ID_OutrichMethod(camp_id, userid):
    # get all IDS according to VendorType
    method = []
    method_id = []
    ids = []
    vendor_dict = {}

    camp_detail = Campaign.objects.get(id=camp_id)
    for row in camp_detail.method.all():
        method.append(row.type)
    # ////
        method_id.append(row.id)
        # method_id.append(row.id)
    if Hybrid in method_id:
        if email_out not in method_id:
            method.append(source_touches.objects.get(id=email_out).type)
        if tele_out not in method_id:
            method.append(source_touches.objects.get(id=tele_out).type)
    '''
    if "Hybrid" in method_id:
        if email_out not in method_id:
            method.append("email")
        if tele_out not in method_id:
            method.append("Tele")
    ////
    '''

    vendortype = VendorType.objects.all()
    for type in vendortype:
        vendor_dict[type.id] = type.type

    if camp_detail.tc_vendor == 1:
        user_data = user.objects.filter(usertype=2, status=3, approve=1)
        data = data_assesment.objects.filter(user__in=user_data)
        for row in data:
            vendor_type = list(map(int, ast.literal_eval(row.vendor_type)))
            if len(vendor_type) > 0:
                for vendorId in vendor_type:
                    if vendorId in vendor_dict:
                        if vendor_dict[vendorId] in method:
                            ids.append(row.user_id)
                            break
    if camp_detail.external_vendor == 1:
        vendor_list = get_external_vendors(userid)
        vendor_list_id = [obj.id for obj in vendor_list]
        ids.extend(vendor_list_id)

    return list(set(ids))

# === Edit RFQ Campaigns ===
@is_users_campaign
def edit_rfq_campaign(request, campaign_id, clone=0):
    # edit_rfq_campaigns 
    countries1 = countries_list.objects.all()
    ''' countries1 = countries.objects.all() '''
    region_list = region1.objects.all()
    ''' region_list=region.objects.all() '''
    industry_types = industry_type.objects.filter(status=0)
    custom_industry_types = industry_type.objects.filter(status=1, client_id = request.session['userid'])
    company_sizes = company_size.objects.all()
    vendor_type = VendorType.objects.all()
    revenue_size = RevenueSize.objects.all()
    message, success, title = "", 0, "error"
    abm_status, suppression_status = 0, 0
    # check is logged in
    is_logged_in = check_is_logged_in(request)
    if(is_logged_in['success']):
        if campaign_id:
            if Campaign.objects.count():
                if Campaign.objects.filter(id=campaign_id):
                    campaign = Campaign.objects.filter(id=campaign_id).first()
                else:
                    message += "Error Campaign does not exist with an id '" + \
                        str(campaign_id) + "'"
            else:
                message = "Error.. Campaign table is empty"

            if campaign:
                specification = Specification.objects.filter(
                    campaign=campaign).first()
                mapping = Mapping.objects.filter(campaign=campaign).first()
                terms = Terms.objects.filter(campaign=campaign).first()
                delivery = Delivery.objects.filter(campaign=campaign).first()
                geo_list = str(campaign.geo).split(',')
                campaignform = CampaignForm()
                specificationform = SpecificationForm()
                all_selected_method = []
                for method in campaign.method.all():
                    all_selected_method.append(method.id)

                context = {
                    'custom_industry_types': custom_industry_types,
                    'campaign': campaign,
                    'campaignform': campaignform,
                    'region_list': region_list,
                    'countries': countries1,
                    "industry_types": industry_types,
                    'mapping': mapping,
                    'selected_industry': mapping.industry_type.split(',') if mapping.industry_type is not None else '',
                    'geo_list': geo_list,
                    "specification": specification,
                    'all_selected_method': all_selected_method,
                    "company_sizes": company_sizes,
                    "vendor_type": vendor_type,
                    "revenue_size": revenue_size,
                    'mappingform': MappingForm(),
                    "specificationform": specificationform,
                    'clone': clone,
                }
                return render(request, 'campaign/edit_rfq_campaign.html', context)
            else:
                print(message)
                return False
        else:
            message += "Please pass an campaign id in GET request"
    else:
        print(is_logged_in)
        return redirect("/logout/")

# === Update Campaign Expiry Date ===
@is_client
def update_campaign_expiry_date(request):
    if Campaign.objects.filter(id=request.POST.get('camp_id')).update(rfq_timer=request.POST.get('date')):
        data = {'status': 1, 'message': 'date change successfully'}
    else:
        data = {'status': 2, 'message': 'date change failed'}
    return JsonResponse(data)

# === Remove File ===
def removefile(request=None, camp=None, type1=None):
    # function for removing abm and supp data from db
    # work on both ajax and normal call -Amey
    if request is not None:
        type = request.POST.get('type')
        id = request.POST.get('camp_id')
        spec_obj = Specification.objects.filter(campaign=id)
        if spec_obj:
            try:
                if type == 'abm':
                    spec_obj[0].abm_company_file.delete()
                    spec_obj[0].abm_file_content = ""
                    spec_obj[0].abm_count = 0
                elif type == 'supp':
                    spec_obj[0].suppression_company_file.delete()
                    spec_obj[0].suppression_file_content = ""
                    spec_obj[0].suppression_count = 0
                data = {"success": 2}
            except Exception as e:
                print(e)
                data = {"success": 1}
            spec_obj[0].save()
        else:
            data = {"success": 1}
        return JsonResponse(data)
    else:
        if camp is not None and type1 is not None:
            spec_obj = Specification.objects.filter(campaign=camp)
            if spec_obj:
                try:
                    if type1 == 'abm':
                        spec_obj[0].abm_company_file.delete()
                        spec_obj[0].abm_file_content = ""
                        spec_obj[0].abm_count = 0
                    elif type1 == 'supp':
                        spec_obj[0].suppression_company_file.delete()
                        spec_obj[0].suppression_file_content = ""
                        spec_obj[0].suppression_count = 0
                except Exception as e:
                    print(e)
                spec_obj[0].save()


@is_client
def rfq_campaign_edit(request, camp_id):
    '''' Edit RFQ campaign..Akshay G.'''
    obj = RFQ_Campaigns.objects.get(id = camp_id)
    camp_start_date = obj.start_date
    camp_end_date = obj.end_date
    import datetime
    current_date = datetime.datetime.now().date()
    startdate_error = None
    enddate_error = None
    if camp_start_date:
        if isinstance(camp_start_date, datetime.date) == False:
            camp_start_date = datetime.strptime(camp_start_date, '%Y-%m-%d')
        if current_date > camp_start_date:
            obj.start_date = None
            startdate_error = 'Start date is later than Current date. Please select another date.'
        elif camp_start_date.weekday() in [5, 6]:
            obj.start_date = None
            startdate_error = 'Start date is a weekend. Please select another date.'
    if camp_end_date:  
        if isinstance(camp_end_date, datetime.date) == False:
            camp_end_date = datetime.strptime(camp_end_date, '%Y-%m-%d')
        if camp_start_date:
            if camp_start_date > camp_end_date:
                obj.end_date = None
                enddate_error = 'Start date is further than end date. Please select another date.'
        if current_date > camp_end_date:
            obj.end_date = None
            enddate_error = 'End date is later than Current date. Please select another date.'
        elif camp_end_date.weekday() in [5, 6]:
            obj.end_date = None
            enddate_error = 'End date is a weekend. Please select another date.'
    obj.save()
    countries1 = countries_list.objects.all()
    region_list = region1.objects.all()
    geo_ = obj.geo if obj.geo is not None else ''
    selected_country = []
    if geo_ != '' and ',' in geo_:
        selected_country = geo_.split(',')
    else:
        selected_country = [geo_] if geo_ != '' else []
    selected_country = [i.title() for i in selected_country]
    mapped_country = countries1.filter(name__iregex=r'^(' + '|'.join([re.escape(b) for b in selected_country]) + ')$')
    if len(mapped_country) > 1:
        total_selected_countries = mapped_country[0].name + ' + ' + str(len(mapped_country) - 1) + ' more'
    elif len(mapped_country) == 1:
        total_selected_countries = mapped_country[0].name
    else:
        total_selected_countries = ''
    # 'unmapped_country' - user added new countries which are not in database
    unmapped_country = []
    for i in selected_country:
        if len(mapped_country.filter(name = i.title())) == 0:
            unmapped_country.append(i)
    selected_countries = mapped_country.values_list('id', flat = True) if len(mapped_country) > 0 else []
    selected_regions = mapped_country.values_list('region_id', flat = True) if len(mapped_country) > 0 else []
    selected_industry = eval(str(obj.industry_type)) if obj.industry_type not in [None, '0', ''] else []
    selected_industry1 = selected_industry.copy()
    for i, text in RFQ_CAMPAIGN_INDUSTRY_CHOICES:
        if text in selected_industry:
            selected_industry.remove(text)
    new_industry = selected_industry if len(selected_industry) > 0 else []
    selected_company_size = eval(str(obj.company_size)) if obj.company_size not in [None, '0', ''] else []
    selected_company_size1 = selected_company_size.copy()
    for i, text in RFQ_CAMPAIGN_COMPANY_CHOICES:
        if text in selected_company_size:
            selected_company_size.remove(text)
    new_company_size = selected_company_size if len(selected_company_size) > 0 else []
    selected_revenue_size = eval(str(obj.revenue_size)) if obj.revenue_size not in [None, '0', ''] else []
    selected_revenue_size1 = selected_revenue_size.copy()
    for i, text in RFQ_CAMPAIGN_REVENUE_CHOICES:
        if text in selected_revenue_size:
            selected_revenue_size.remove(text)
    new_revenue_size = selected_revenue_size if len(selected_revenue_size) > 0 else []
    outreach_method = eval(str(obj.method)) if obj.method is not None else []
    d = {}
    d['total_selected_countries'] = total_selected_countries
    suppression_content = obj.suppression_file_content
    abm_content = obj.abm_file_content
    file_data = create_suppression_abm_file(obj.suppression_file_content, obj.abm_file_content)
    if request.method == 'POST':
        form = RFQCampaignForm(request.POST, request.FILES, instance = obj)
        abm_Status = form['abm_status'].value()
        if form.is_valid():
            obj.suppression_file_content = suppression_content
            obj.abm_file_content = abm_content
            obj.save()
            rfq_value = form['rfq_timer'].value()
            rfq_date = rfq_value.split(' ')[0]
            rfq_time = rfq_value.split(' ')[1]
            if len(rfq_time.split(':')) == 2:
                rfq_value = rfq_date + ' ' + rfq_time + ':00'
            if '/' in rfq_date:
                expiry = datetime.datetime.strptime(rfq_value, '%d/%m/%Y %H:%M:%S')
            else:
                expiry = datetime.datetime.strptime(rfq_value, '%d-%m-%Y %H:%M:%S')
            obj.rfq_timer = expiry
            # validate form manually..Akshay G.
            status, form, d, obj = validate_form(obj, form, abm_Status)
            if status:
                # suryakant 04-10-2019
                # to save new data from choices fields
                obj.industry_type = form['industry_type'].value()
                obj.company_size = form['company_size'].value()
                obj.revenue_size = form['revenue_size'].value()
                obj.save()
                RFQ = form.save(commit=False)
                RFQ.save()
                title = 'RFQ Campaign Updated'
                desc = "A RFQ Campaign '" + str(RFQ.name) + "' updated by " + str(RFQ.user.user_name)

                sender_id = request.session['userid']
                superadmins = user.objects.filter(usertype__id=4)

                from client.utils import noti_via_mail
                for superad in superadmins:
                    RegisterNotification(sender_id, superad.id, desc, title, 1, None, None)

                noti = noti_via_mail([superad.id for superad in superadmins], title, desc, mail_new_rfq_campaign)
                suggest_vendors(request,obj)

                return redirect('client_rfq_campaign')
        else:
            obj.suppression_file_content = suppression_content
            obj.abm_file_content = abm_content
            obj.save()
            if form['rfq_timer'].value() != '':
                rfq_value = form['rfq_timer'].value()
                rfq_date = rfq_value.split(' ')[0]
                rfq_time = rfq_value.split(' ')[1]
                if len(rfq_time.split(':')) == 2:
                    rfq_value = rfq_date + ' ' + rfq_time + ':00'
                if '/' in rfq_date:
                    expiry = datetime.datetime.strptime(rfq_value, '%d/%m/%Y %H:%M:%S')
                else:
                    expiry = datetime.datetime.strptime(rfq_value, '%d-%m-%Y %H:%M:%S')
                obj.rfq_timer = expiry
            # make 'geo' empty, if user has made geo empty before submitting form
            if 'geo' not in form.cleaned_data:
                selected_countries = []
                selected_regions = []
                total_selected_countries = ''
            if 'method' not in form.cleaned_data:
                outreach_method = []
            status, form, d, obj = validate_form(obj, form, abm_Status)
            obj.save()
    else:
        form = RFQCampaignForm(instance = obj)
    
    rfq_Timer = ''
    if obj.rfq_timer is not None:
        if type(obj.rfq_timer) != str:
            rfq_Timer = obj.rfq_timer.strftime("%d-%m-%Y %H:%M:%S")
    context = {
        'campaignform': form,
        'countries': countries1,
        'region_list': region_list,
        'outreach_method': d['method'] if 'method' in d else outreach_method,
        'selected_industry': d['selected_industry1'] if 'selected_industry1' in d else selected_industry1,
        'new_industry': d['new_industry'] if 'new_industry' in d else new_industry,
        'selected_company_size': d['selected_company_size1'] if 'selected_company_size1' in d else selected_company_size1,
        'new_company_size': d['new_company_size'] if 'new_company_size' in d else new_company_size,
        'selected_revenue_size': d['selected_revenue_size1'] if 'selected_revenue_size1' in d else selected_revenue_size1,
        'new_revenue_size': d['new_revenue_size'] if 'new_revenue_size' in d else new_revenue_size,
        'selected_country': d['selected_country'] if 'selected_country' in d else selected_countries,
        'selected_country_list': d['selected_country_list'] if 'selected_country_list' in d else [i for i in selected_country if i not in unmapped_country],
        'selected_region': d['selected_region'] if 'selected_region' in d else selected_regions,
        'total_selected_countries': d['total_selected_countries'],
        'rfq_Timer': rfq_Timer,
        'camp_id': camp_id,
        'start_date': obj.start_date,
        'end_date': obj.end_date,
        'file_data': d['file_data'] if 'file_data' in d else file_data,
        'startdate_error': startdate_error,
        'enddate_error': enddate_error,
    }
    return render(request, 'campaign/create_rfq_campaign.html', context)

# manual validation for Edit RFQ campaign form..Akshay G. 
def validate_form(RFQ, form, abm_status):
    # suryakant 04-10-2019
    # to add new choice filed for validations
    industry_choices = form['company_size'].field._choices
    company_choices = form['revenue_size'].field._choices
    revenue_choices = form['industry_type'].field._choices
    company_size = form['company_size'].value()
    revenue_size = form['revenue_size'].value()
    industry_types = form['industry_type'].value()
    method = form['method'].value()
    status = True
    d = {}
    if method not in [[], '', '0', '[]']:
        RFQ.method = eval(str(method))
        d['method'] = eval(str(method))
    if company_size is not None and revenue_size is not None and abm_status == False:
        if len(company_size) == 0 and len(revenue_size) == 0:
            status = False
            msg = "Either company size or revenue size is required."
            form.add_error('revenue_size', msg) 
        else:
            RFQ.company_size = eval(str(company_size))
            RFQ.revenue_size = eval(str(revenue_size))
            
    if len(industry_types) == 0 and abm_status == False:
        status = False
        form.add_error('industry_type', "This field is required.")
    else:
        RFQ.industry_type = eval(str(industry_types))
        
    d['selected_company_size1'] = company_size.copy()
    for i, text in company_choices:
        if text in company_size:
            company_size.remove(text)
    d['new_company_size'] = company_size if len(company_size) > 0 else []
    d['selected_revenue_size1'] = revenue_size.copy()
    for i, text in revenue_choices:
        if text in revenue_size:
            revenue_size.remove(text)
    d['new_revenue_size'] = revenue_size if len(revenue_size) > 0 else []
    
    d['selected_industry1'] = industry_types.copy()
    for i, text in industry_choices:
        if text in industry_types:
            industry_types.remove(text)
    d['new_industry'] = industry_types if len(industry_types) > 0 else []

    countries1 = countries_list.objects.all()
    selected_country = []
    geo_ = ''
    if 'geo' in form.cleaned_data:
        geo_ = form.cleaned_data['geo']
    if geo_ != '' and ',' in geo_:
        selected_country = geo_.split(',')
    else:
        selected_country = [geo_] if geo_ != '' else []
    selected_country = [i.title() for i in selected_country]
    mapped_country = countries1.filter(name__iregex=r'^(' + '|'.join([re.escape(b) for b in selected_country]) + ')$')
    if len(mapped_country) > 1:
        total_selected_countries = mapped_country[0].name + ' + ' + str(len(mapped_country) - 1) + ' more'
    elif len(mapped_country) == 1:
        total_selected_countries = mapped_country[0].name
    else:
        total_selected_countries = ''
    # 'unmapped_country' - user added new countries which are not in database
    unmapped_country = []
    for i in selected_country:
        if len(mapped_country.filter(name = i.title())) == 0:
            unmapped_country.append(i)
    selected_countries = mapped_country.values_list('id', flat = True) if len(mapped_country) > 0 else []
    selected_regions = mapped_country.values_list('region_id', flat = True) if len(mapped_country) > 0 else []
    selected_country_list = [i for i in selected_country if i not in unmapped_country]
    d.update({'selected_country': selected_countries, 'selected_country_list': selected_country_list, 'selected_region': selected_regions, 'total_selected_countries': total_selected_countries})
    if form.cleaned_data['abm_file'] is not None:
        RFQ.abm_status = True
        if len(form.cleaned_data['abm_file']) > 0:
            RFQ.abm_file_content = form.cleaned_data['abm_file']
            file_data = create_suppression_abm_file(form.cleaned_data['suppression_file'], form.cleaned_data['abm_file'])
            d['file_data'] = file_data
    if form.cleaned_data['suppression_file'] is not None:
        RFQ.suppression_status = True
        if len(form.cleaned_data['suppression_file']) > 0:
            RFQ.suppression_file_content = form.cleaned_data['suppression_file']
            file_data = create_suppression_abm_file(form.cleaned_data['suppression_file'], form.cleaned_data['abm_file'])
            d['file_data'] = file_data
    return status, form, d, RFQ

# create file from uploaded data..Akshay G.
def create_suppression_abm_file(suppression_file_content, abm_file_content):
    import datetime
    suppression_content = []
    d = {}
    if suppression_file_content is not None:
        suppression_content = eval(str(suppression_file_content)) if suppression_file_content != '' else []
    if len(suppression_content) > 0:
        headers = []
        with open(os.path.join(settings.MEDIA_ROOT,'uploaded_suppression_file.csv'), 'w') as file:
            d['suppression_path'] = os.path.join(settings.BASE_URL, 'media',os.path.basename(file.name))
            f = csv.writer(file)
            data = [] 
            for i in range(len(suppression_content)):
                values = []
                for header, value in suppression_content[i].items():
                    if 'user_domain' in suppression_content[i] and header == 'DOMAIN_NAME':
                        value = suppression_content[i]['user_domain'] + value
                    if header == 'timestamp' or header == 'user_domain':
                        continue
                    if i == 0:
                        headers.append(header)
                    values.append(value)
                if i == 0:
                    data.append(headers)
                if values not in data:
                    data.append(values)
            f.writerows(data)
    abm_content = []
    if abm_file_content is not None:
        abm_content = eval(str(abm_file_content)) if abm_file_content != '' else []
    if len(abm_content) > 0:
        headers = []
        with open(os.path.join(settings.MEDIA_ROOT,'uploaded_abm_file.csv'), 'w') as file:
            d['abm_path'] = os.path.join(settings.BASE_URL, 'media',os.path.basename(file.name))
            f = csv.writer(file)
            data = [] 
            for i in range(len(abm_content)):
                values = []
                for header, value in abm_content[i].items():
                    if 'user_domain' in abm_content[i] and header == 'DOMAIN_NAME':
                        value = abm_content[i]['user_domain'] + value
                    if header == 'timestamp' or header == 'user_domain':
                        continue
                    if i == 0:
                        headers.append(header)
                    values.append(value)
                if i == 0:
                    data.append(headers)
                if values not in data:
                    data.append(values)
            f.writerows(data)
    return d


# === Suggest Vendor ===
def suggest_vendors(request, campaign):  # campaign_id
    # suryakant 04-10-2019
    # suggest vendors for rfq campaigns according to country and type
    message, success, title = "", 0, "error"
    # campaign_id = 49 # testing
    
    if campaign:
        outreach_method = []
        sweet_spot_geo = []  # country
        complex_program = []  # campaign_type
        industry_types = []

        method_list = [d.id for d in source_touches.objects.filter(type__in=campaign.method)]
        #Kishor Select Hybrid Then Email,Tele Vendor Suggest
        if 'Hybrid' in campaign.method:
            if 'Email' not in campaign.method:
                method_list.append(source_touches.objects.get(type='Email').id)
            if 'Tele' not in campaign.method:
                method_list.append(source_touches.objects.get(type='Tele').id)
        method_list = list(set(method_list))
        
        camp_country_str = campaign.geo
        camp_country_list = []
        if camp_country_str:
            camp_country_list = camp_country_str.split(",")
            matching_countries = countries_list1.objects.filter(
                name__in=camp_country_list,
            )
        else:
            message += "\n Country is null in campaign"
        # Filter Django database for field containing any value in an array
        # query = Q()
        # for letter in camp_country_list:
        #     query = query | Q(sweet_spot_text__icontains=letter)

        # Filter Django database for field containing any value in an array
        query2 = Q()
        # check if camp methods matches with vendor onboarding- data assessment- 'Lead Gen options'...Akshay G..Date - 20th Nov, 2019
        for method_id in method_list:
            query2 = query2 | Q(lead_gen_capacity__icontains = str(method_id))
        vendors = data_assesment.objects.filter(user__usertype__type='vendor').filter(query2).distinct()
        
        match_rfq_campaign_vendor.objects.filter(campaign=campaign).delete()
        for index, vendor in enumerate(vendors):
            user_id = vendor.user.id
            is_user_exists = user.objects.filter(id=user_id,status=3)
            if(is_user_exists):
                User = user.objects.get(id=user_id)
                match_vendor = match_rfq_campaign_vendor.objects.filter(campaign=campaign,client_vendor=User,)
                if not match_vendor:
                    match_rfq_campaign_vendor.objects.create(campaign=campaign,client_vendor=User,is_active=1)
        return True
    else:
        return False

# editing feature for RFQ campaign while creating it...Akshay G...Date - 18th Oct, 2019
def create_or_edit_rfq_camp(form, data_dict):
    import datetime
    if len(form['method'].value()) > 0:
            data_dict['outreach_method'] = form['method'].value()
    if len(form['industry_type'].value()) > 0:
        data_dict['selected_industry'] = form['industry_type'].value()
    if len(form['company_size'].value()) > 0:
        data_dict['selected_company_size'] = form['company_size'].value()
    if len(form['revenue_size'].value()) > 0:
        data_dict['selected_revenue_size'] = form['revenue_size'].value()
    if form['start_date'].value() != '':
        if '-' in form['start_date'].value():
            data_dict['start_date'] = datetime.datetime.strptime(form['start_date'].value(), "%Y-%m-%d") 
        else:
            data_dict['start_date'] = datetime.datetime.strptime(form['start_date'].value(), "%Y/%m/%d")
    if form['end_date'].value() != '':
        if '-' in form['end_date'].value():
            data_dict['end_date'] = datetime.datetime.strptime(form['end_date'].value(), "%Y-%m-%d")
        else:
            data_dict['end_date'] = datetime.datetime.strptime(form['end_date'].value(), "%Y/%m/%d")
    if form['geo'].value() != '':
        countries1 = countries_list.objects.all()
        countries_ = form['geo'].value()
        if ',' in countries_:
            country_list = [i.strip() for i in countries_.split(',')]
        else:
            country_list = [countries_.strip()]
        data_dict['selected_country_list'] = country_list.copy()
        country_query = countries1.filter(name__iregex=r'^(' + '|'.join([re.escape(b) for b in country_list]) + ')$')
        if len(country_query) > 0:
            if len(country_query) == 1:
                data_dict['total_selected_countries'] = country_query[0].name
            else:
                data_dict['total_selected_countries'] = '{} + {} more'.format(country_query[0].name, len(country_query) -1 )
            data_dict['selected_country'] = country_query.values_list('id', flat = True)
            data_dict['selected_region'] = country_query.values_list('region_id', flat = True)
    return data_dict
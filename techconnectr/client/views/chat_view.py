import ast
import datetime
from user.models import *

from django.conf import settings
from django.core import serializers
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.http import HttpResponse, JsonResponse
from django.shortcuts import get_object_or_404, redirect, render
from django.urls import resolve
from django.core.files.storage import FileSystemStorage
from campaign.models import *
from client.models import *
from client_vendor.models import *
from setupdata.models import *
from setupdata.serializer import *
from client.utils import get_logos
import re

# === Save Chat Status ===
def save_chat_status(request):
# update count for sent chat messages on receiving...Akshay G.
    userid = request.session['userid']
    chat_obj_id = request.POST.get('chat_obj_id')
    sender_id = int(request.POST.get('sender_id'))
    camp_chat = Campaign_chats.objects.get(id = chat_obj_id)
    d2 = eval(camp_chat.send_msg_count)
    d2[userid]['count'] -=  d2[userid][sender_id][0] 
    d2[userid][sender_id] = [0]
    camp_chat.send_msg_count = d2
    camp_chat.save()
    return JsonResponse({})

# === Get Campaign Chat ===
def get_campaign_chat(request):
    # get campaign chat
    data = {'success': 1}
    camp_id = request.POST.get('camp_id')
    if Campaign_chats.objects.filter(campaign_id=camp_id).count() > 0:
        chats = Campaign_chats.objects.filter(campaign_id=camp_id)
        data = get_chats_into_dict(chats, request.session['userid'])
        return JsonResponse(data, safe=False)
    return JsonResponse(data)

# === Get Chats into Dict ===
def get_chats_into_dict(chats, userid):
    # insert chat data into dict
    chat_data = []
    for msg in chats:
        data = ast.literal_eval(msg.data)
        for chat_msg in data:
            if userid in chat_msg['receiver_ids']:
                # chat from Other to current logIn user
                sender_query = user.objects.filter(email=chat_msg['sender_name'])
                sender_name = ''
                if sender_query:
                    if sender_query[0].user_name:
                        sender_name = sender_query[0].user_name
                if chat_msg['sender_id'] != userid:
                    receiver_profilepic = {}
                    chat_data.append({
                        'sender_name': sender_name,
                        'profilepic': chat_msg['profilepic'],
                        'sender_id': chat_msg['sender_id'],
                        'message': chat_msg['message'],
                        'time': chat_msg['time'],
                        'date': chat_msg['date'],
                        'media':chat_msg['media'],
                        'title':chat_msg['title'],
                        # suryakant 05-10-2019
                        # return logos without sender and current user logo
                        'receiver_logos':[d for d in chat_msg['receiver_logos'] if d['id'] != userid and d['id'] != chat_msg['sender_id']],
                        'receiver_profilepics':receiver_profilepic,
                        'chat_obj_id': msg.id,
                    })
                # chat from current logIn user to other
                else:
                    chat_msg['receiver_ids'].remove(userid)
                    receiver_ids = chat_msg['receiver_ids']
                    # for i in chat_msg['receiver_ids']:
                    #     if i not in receiver_ids:
                    #         receiver_ids.append(i)
                    receiver_profilepic = {}
                    from client.utils import fetch_default_logo
                    for id in receiver_ids:
                        if id == 3:
                            profilepic = fetch_default_logo(id)
                        else:
                            from pathlib import Path
                            if client_vendor.objects.filter(user_id = id).exists():
                                profile = client_vendor.objects.get(user_id = id)
                                if profile.company_logo:
                                    config = Path(profile.company_logo_smartthumbnail.url)
                                    if config.is_file():
                                        profilepic = profile.company_logo_smartthumbnail.url
                                        receiver_profilepic[id] = profilepic
                                    else:
                                        receiver_profilepic[id] = fetch_default_logo(id)
                                else:
                                    receiver_profilepic[id] = fetch_default_logo(id)
                    sender_obj = user.objects.filter(email=chat_msg['sender_name'])
                    sender_name = ''
                    if sender_obj:
                        if sender_obj[0].user_name:
                            sender_name = sender_obj[0].user_name
                    chat_data.append({
                        'sender_name': sender_name,
                        'profilepic': chat_msg['profilepic'],
                        'sender_id': chat_msg['sender_id'],
                        'message': chat_msg['message'],
                        'time': chat_msg['time'],
                        'date': chat_msg['date'],
                        'media':chat_msg['media'],
                        'title':chat_msg['title'],
                        # suryakant 05-10-2019
                        # return logos without sender
                        'receiver_logos':[d for d in chat_msg['receiver_logos'] if d['id'] != chat_msg['sender_id']],
                        'receiver_profilepics':receiver_profilepic,
                        'chat_obj_id': msg.id,
                    })



    return chat_data

# === Campaign Chat Screen ===
def campaign_chat_screen(request, camp_id):
    # opens chat screen
    chat_list = []
    camp = Campaign.objects.get(id=camp_id)
    camp_list = get_user_live_campaign(camp.user_id, camp)
    vendor_list = get_vendor_list_of_campaign(camp_id)
    if Campaign_chats.objects.filter(campaign_id=camp_id).count() > 0:
        chats = Campaign_chats.objects.get(campaign_id=camp_id)
        chat_list = ast.literal_eval(chats.data)
    return render(request, 'campaign_chat/chat_screen.html', {'campaign_list': camp_list, 'sender_id': request.session['userid'], 'chat': chat_list, 'vendor_list': vendor_list, 'campaign': camp})

# === Get User Live Campaign
def get_user_live_campaign(userid, camp):
    # return users live campaign list
    camp_id = []
    camp_list = []
    camp = Campaign.objects.filter(user_id=userid, status=1)
    camp_allc = campaign_allocation.objects.filter(
        campaign_id__in=camp, status=1)
    for row in camp_allc:
        if row.campaign_id not in camp_id:
            camp_id.append(row.campaign_id)
            camp_list.append({
                'id': row.campaign_id,
                'name': row.campaign
            })
    return camp_list

# === Get Vendor List of Campaign ===
def get_vendor_list_of_campaign(camp_id):
    # return working vendor list on campaign
    vendor_id = []
    vendor_data = []
    vendor_list = campaign_allocation.objects.filter(
        campaign_id=camp_id, status__in=[1,4,5])
    for vendor in vendor_list:
        if vendor.client_vendor_id not in vendor_id:
            vendor_id.append(vendor.client_vendor_id)
            vendor_data.append({
                'id': vendor.client_vendor_id,
                'vendor_name': vendor.client_vendor
            })
    return vendor_data

# === Send Message ===
def send_msg(request):
    # send messages
    # import pdb;pdb.set_trace()
    ids = []
    media=""
    title=""
    image = ''
    doc = ''
    link = ''
    sender_name=request.session['username']
    msg1 = request.POST.get('message')
    msg = msg1.replace("<","&lt;")
        # print(msg)
    camp_id = request.POST.get('camp_id')
    receiver_ids = list(campaign_allocation.objects.filter(campaign_id=camp_id, status__in=[1,4,5]).values_list('client_vendor', flat=True).distinct())
    if len(Find(msg)) > 0:
        link = msg
    if len(request.POST.getlist('vendor_names')) == 0:
        ids.append(request.session['userid'])
        ids.extend(receiver_ids)
        
        # involving superadmin in receiver- amey
        superadmins = [i.id for i in user.objects.filter(usertype_id=4)]
        ids.extend(superadmins)
        ids = list(set(ids))

        result = store_msg(request.session['userid'], ids, msg,media,image,doc,link,title, camp_id,sender_name)
        if result['status'] == 'true':
            data = {'success': 1,'date':result['date'],'time':result['time'],'profilepic':result['profilepic'],'msg':msg,'receiver_logos':[d for d in result['receiver_logos'] if d['id'] != request.session['userid']],'sender_name':result['sender_name'],'receiver_ids':ids,'camp_id':camp_id}
    else:
        ids = list(map(int, request.POST.getlist('vendor_names')))
        ids.append(request.session['userid'])
        # involving superadmin in receiver - amey 
        superadmins = [i.id for i in user.objects.filter(usertype_id=4)]
        ids.extend(superadmins)
        ids = list(set(ids))

        result = store_msg(request.session['userid'], ids, msg,media,image,doc,link,title, camp_id,sender_name)
        if result['status'] == 'true':
            data = {'success': 1,'date':result['date'],'time':result['time'],'profilepic':result['profilepic'],'msg':msg,'receiver_logos':[d for d in result['receiver_logos'] if d['id'] != request.session['userid']],'sender_name':result['sender_name'],'receiver_ids':ids,'camp_id':camp_id}
    return JsonResponse(data)

# === Store Message === 
def store_msg(sender_id, receiver_ids, msg,media,image,doc,link,title,camp_id,sender_name):
    # store messages into dict 
    last_dict = {}
    list_count = 0
    time = datetime.now().strftime('%H:%M:%S')
    profile = client_vendor.objects.get(user_id=sender_id)
    import os
    from client.utils import fetch_default_logo
    # fetch user default logo..Akshay G...23rd Oct, 2019
    default_logo = profilepic = fetch_default_logo(sender_id)
    if profile.company_logo:
        try:
            profilepic = profile.company_logo_smartthumbnail.url
            if profilepic == '/media/':
                profilepic = default_logo  
        except Exception as e:
            pass
            
    if Campaign_chats.objects.filter(campaign_id=camp_id).count() == 1:
        chats_details = Campaign_chats.objects.get(campaign_id=camp_id)
        list = ast.literal_eval(chats_details.data)
        last_dict = list[-1]
        receiver_logos = get_logos(receiver_ids)
        list.append({'sender_id': sender_id,'sender_name':sender_name, 'receiver_ids': receiver_ids,'receiver_logos':receiver_logos, 'profilepic': profilepic,
                     'message': msg,'media':media,'image':image,'doc':doc,'link':link,'title':title,'time': time,'first':1, 'date': str(datetime.today().date()), 'rank': int(last_dict['rank'])+1})
        list_count = len(list)
        chats_details.data = list
        # update sent message count for receiver user ...Akshay G.
        if chats_details.send_msg_count != None:
            d = eval(chats_details.send_msg_count)
            for id in receiver_ids:
                if id != sender_id:
                    if id in d:
                        if sender_id in d[id]:
                            d[id][sender_id][0] += 1
                            d[id][sender_id].append(int(last_dict['rank'])+1)
                        else:
                            d[id][sender_id] = [1, int(last_dict['rank'])+1]
                        d[id]['count'] += 1
                    else:
                        d[id] = {sender_id: [1, int(last_dict['rank'])+1]}
                        d[id]['count'] = 1
            chats_details.send_msg_count = d
        else:
            d = {}
            for id in receiver_ids:
                if id != sender_id:
                    d[id] = {sender_id: [1,int(last_dict['rank'])+1], 'count': 1}
            chats_details.send_msg_count = d
        chats_details.save()
    else:
        list = []
        receiver_logos = get_logos(receiver_ids)
        list.append({'sender_id': sender_id,'sender_name':sender_name,'receiver_ids': receiver_ids,'receiver_logos':receiver_logos, 'profilepic': profilepic,
                     'message': msg,'media':media,'image':image,'doc':doc,'link':link,'title':title,'time': time,'first':0, 'date': str(datetime.today().date()), 'rank': 1})
        d = {}
        for id in receiver_ids:
            if id != sender_id:
                d[id] = {sender_id: [1, 1], 'count': 1}
        Campaign_chats.objects.create(campaign_id=camp_id, data=list, send_msg_count = d)
    send_campaign_notification(receiver_ids, sender_id, camp_id, list_count)
    
    data = {'status':'true','date':str(datetime.today().date()),'time':time,'profilepic':profilepic,'receiver_logos': receiver_logos,'sender_name':profile.user.user_name}
    return data

# === Send Attachment to user ===
def send_attchment(request):
#send attachment to users
    if request.FILES:
        sender_id=request.session['userid']
        sender_name=request.session['username']
        camp_id = request.POST.get('camp_id')
        title=request.POST.get('title')
        msg="0"
        image = ''
        doc = ''
        link = ''
        receiver_ids=request.POST.getlist('ids[]')
        receiver_ids.append(sender_id)
        receiver_ids = list(map(int, receiver_ids))

        # involving superadmin in receiver -amey
        superadmins = [i.id for i in user.objects.filter(usertype_id=4)]
        receiver_ids.extend(superadmins)
        receiver_ids = list(set(receiver_ids))

        myfile = request.FILES['filename']
        if myfile.name.split('.')[-1].lower() in ['jpg','png','jpeg','pdf','docx','doc','txt','csv','xlsx','xls']:
            fs = FileSystemStorage()
            filename = fs.save("chat_data/" + myfile.name,  myfile)
            media='/media/'+filename
            if myfile.name.split('.')[-1].lower() in ['pdf','doc','docx','txt','csv','xlsx','xls']:
                doc = '/media/'+filename
            if myfile.name.split('.')[-1].lower() in ['jpg','png','jpeg']:
                image = '/media/'+filename
            store_msg(sender_id,receiver_ids,msg,media,image,doc,link,title,camp_id,sender_name)
            data = {'status': 1}
            return JsonResponse(data)
        else:
            data = {'status': 2,'msg':"Only files with extension .jpg, .png, .jpeg, .pdf, .docx, .doc, .txt, .csv, .xlsx, .xls are allowed!"}
            return JsonResponse(data)
    data = {'status': 1}
    return JsonResponse(data)

# === Find Valid ULR conditions === 
def Find(string):
    # with valid conditions for urls in string
    url = re.findall('http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\), ]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', string)
    return url

# === Send Campaign Notification ===
def send_campaign_notification(receiver_ids, sender_id, camp_id, list_count):
    # raise messages notification
    data = []
    receiver_ids.remove(sender_id)
    user_data = user_activities.objects.filter(user_id__in=receiver_ids)
    for row1 in user_data:
        if row1.active_page != 'chat':
            if Campaign_notification.objects.filter(campaign_id=camp_id).count() > 0:
                notification = Campaign_notification.objects.get(
                    campaign_id=camp_id)
                list = ast.literal_eval(notification.data)
                userid = create_userid_list(list)
                if row1.user_id in userid:
                    for row in list:
                        row['all_msg_cnt'] = list_count
                        row['start'] = row['end']
                        row['end'] = list_count
                        row['check'] = 0
                        row['read'] = 0
                else:
                    list.append({'user_id': row1.user_id, 'all_msg_cnt': list_count,
                                 'start': 15, 'end': list_count, 'check': 0, 'read': 0})
                    notification.data = list
                    notification.save()
            else:
                data.append({'user_id': row1.user_id, 'all_msg_cnt': list_count,
                             'start': 0, 'end': list_count, 'check': 0, 'read': 0})
                Campaign_notification.objects.create(campaign_id=camp_id, data=data)

# === Create UserId List Reciving Chat ===
def create_userid_list(list):
    # return user id list for reciving chat
    userid = []
    for row in list:
        userid.append(row['user_id'])
    return userid

# === Chat Read Notification ===
def chat_read_notification(request):
    # set message as read message
    userid = request.session['userid']
    camp_id = request.POST.get('id')
    notify = Campaign_notification.objects.get(campaign_id=camp_id)
    list = ast.literal_eval(notify.data)
    for row in list:
        if row['user_id'] == userid:
            row['check'] = 1
            notify.data = list
            notify.save()
    return True

# === Get Media ===
def get_media(request):
    # get media chat media files
    chats = Campaign_chats.objects.filter(campaign_id=request.POST.get('camp_id'))
    userid = request.session['userid']
    chat_data = []
    for msg in chats:
        data = ast.literal_eval(msg.data)
        for chat_msg in data:
            if userid in chat_msg['receiver_ids']:
                if request.POST.get('value') == 'all':
                    chat_data.append({
                        'image':chat_msg['image'],
                        'doc':chat_msg['doc'],
                        'link':chat_msg['link'],
                    })
                else:
                    if (chat_msg[request.POST.get('value')] != ""):
                        chat_data.append({
                            request.POST.get('value'):chat_msg[request.POST.get('value')],
                        })
    # print(chat_data)
    return JsonResponse({'chat_data':chat_data})

import difflib  # get ratio of string similarity
import functools
import json
import csv
import re
from xlrd import open_workbook
import operator
import random
from operator import itemgetter
from user.models import *
from user.models import user
import datetime
from django.conf import settings
from django.core import serializers
from django.core.files.storage import FileSystemStorage
from django.db.models import Q
from django.http import HttpResponse, JsonResponse, Http404
from django.shortcuts import get_object_or_404, redirect, render, render_to_response
from django.urls import resolve, reverse
from django.views import View
from django.views.decorators.csrf import csrf_exempt

import ast

from campaign.forms import *
from campaign.models import *
from client.decorators import login_required, is_client
from client.models import *
from client.utils import *
from client.views.rfq_views import *
from client.views.rfq_views import suggest_vendors as rfq_suggest_vendor
from client_vendor.models import *
from client_vendor.models import client_vendor
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.views import APIView
from setupdata.models import *
from setupdata.models import (cities, countries,countries1 as countries_list1, industry_speciality,
                              industry_type, states, states1)
from setupdata.serializer import Cityserializers, Stateserializers
from vendors.views.views_1 import *

from form_builder.views import *
from leads.models import *
from support.models import *
from itertools import chain
from operator import itemgetter
from campaign.choices import *
from superadmin.utils import user_activity


from PyPDF2 import PdfFileWriter, PdfFileReader
import io
from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter
import base64
from django.conf import settings
import os
import shutil
from os import path
import datetime
import os
from django.conf import settings
from django.http import HttpResponse
from django.template.loader import get_template
from PyPDF2 import PdfFileMerger
import shutil
from xhtml2pdf import pisa
from .camp_status import *
from django.contrib import messages
from campaign_mapping.models import *

# === New Dragable Dashboard ===
@is_client
def Dashboard(request):
    # new dragable dashboard
    favourite = user.objects.get(id=request.user.id).favorite_vendor.all().count()
    current_campaigns = Campaign.objects.filter(user_id=request.user.id).order_by('-created_date')
    topcampaigns = top_campaigns(request.user.id)
    top_tc_vendors = tc_top_vendors_types()
    vendordetail1 = client_top_vendor(request.user.id)
    vendordetail = sorted(vendordetail1, key=itemgetter('revenue'), reverse=True)
    live_chart_data = []
    for camp in current_campaigns.filter(status=1):
        vendors = campaign_allocation.objects.filter(campaign_id=camp.id, status=1)
        ap_leads = 0
        for lead in vendors:
            ap_leads += lead.approve_leads
        if camp.status == 1:
            live_chart_data.append({"campaign":camp.name,"approve":ap_leads})

    sorted_list = user_activity(request)
    context ={
        'favourite':favourite,
        'current_campaigns':current_campaigns.filter(status=1).count(),
        'topcampaigns': topcampaigns,
        'topvendors': top_tc_vendors,
        'vendordetail': vendordetail[:5],
        'chart_data':json.dumps(live_chart_data),
        'activity_list': sorted_list,
    }
    return render(request, 'dashboard/new_dashboard.html',context)

# === Client dashboard Data ===
# """ @login_required """
@is_client
def cdashboard(request):
    # return client dashboard data
    userid = request.session['userid']
    vendordetail1 = client_top_vendor(userid)
    vendordetail = sorted(vendordetail1, key=itemgetter('revenue'), reverse=True)
    top_tc_vendors = tc_top_vendors_types()
    countries1 = countries_list1.objects.all()
    # countries1 = countries.objects.all()
    industry = industry_type.objects.all()
    expert = industry_speciality.objects.all()
    countrow = client_vendor.objects.filter(user_id=userid).count()
    topcampaigns = top_campaigns(userid)
    live_campaign = live_campaigns(userid)

    if countrow == 1:
        client_vendor_detail = client_vendor.objects.get(
            user_id=userid)
        userdetails = user.objects.get(id=userid)
        country = countries_list1.objects.get(id=userdetails.country)
        '''# country = countries.objects.get(id=userdetails.country) '''
        '''# city=cities.objects.filter(id=userdetails.city) '''
        state = states1.objects.get(id=userdetails.state)
        # state = states.objects.get(id=userdetails.state)
        industry = client_vendor_detail.industry_type.all()
    '''
        speciality = industry_speciality.objects.get(
            id=client_vendor_detail.industry_speciality_id)
        'speciality': speciality,
        return render(request, 'dashboard/cdashboard.html', {'onBording': request.session['onBording'], 'live_campaign': live_campaign, 'topvendors': top_tc_vendors, 'vendordetail': vendordetail[:5], 'topcampaigns': topcampaigns,'industry': industry, 'state': state, 'country': country, 'userdetails': userdetails, 'client_vendor': client_vendor_detail, 'expert': expert, 'countries': countries1, 'industry': industry})
    else:
        return render(request, 'dashboard/cdashboard.html', {'onBording': 0, 'vendordetail': vendordetail[:5], 'topvendors': top_tc_vendors, 'live_campaign': live_campaign, 'expert': expert, 'countries': countries1, 'industry': industry, 'topcampaigns': topcampaigns})
    '''
    return redirect('/client/Dashboard/')

# === Top Campaign ===
def top_campaigns(userid):
    result = []
    camp = Campaign.objects.filter(user_id=userid, status__in=[1,2,3,4,5]).order_by('end_date')[:5]
    for i in camp:
        d = {
            'name': i.name,
            'total': i.target_quantity,
            'assigned': i.approveleads,
            'status': i.status,
            'start': i.start_date,
            'end': i.end_date
        }
        result.append(d)
    return result

# === Live Campaigns ===
def live_campaigns(userid):
    import datetime
    result = []
    camp = Campaign.objects.filter(user_id=userid, status=1).order_by('end_date')[:5]
    for i in camp:
        camp_alloc = campaign_allocation.objects.filter(campaign_id=i.id)
        approved = 0
        for c in camp_alloc:
            if c.approve_leads is not None:
                approved += int(c.approve_leads)

        start = i.start_date
        end = i.end_date
        today = datetime.date.today()

        # for finding whether campaign is ahead or behind
        lapsed = (today-start).days
        if lapsed < 0:
            lapsed = 0

        total_days = (end-start).days
        per_day = int(i.target_quantity)/int(total_days)

        if lapsed > total_days:
            till_date_to_be_approved = per_day*total_days
        else:
            till_date_to_be_approved = per_day*lapsed
        progress = percentage(camp_id=i.id)
        # conditions for checking campaign status and sending colors based on progress
        if till_date_to_be_approved > approved:
            # color = 'danger'
            color = '#ff1a1a'
        elif till_date_to_be_approved == approved:
            color = '#3399ff'
            # color = 'info'
        else:
            color = '#2eb82e'
            # color = 'success'

        d = {
            'name': i.name,
            'total': i.target_quantity,
            'assigned': approved,
            'color': color,
            'progress': progress,
            'start': i.start_date,
            'end': i.end_date
        }
        result.append(d)
    return result

# === Load States ===
def loadstates(request):
    # returns states
    countryid = request.POST.get('id', None)
    # important: convert the QuerySet to a list object
    data = states1.objects.filter(country_id=countryid)
    # data = states.objects.filter(country_id=countryid)
    serializer = Stateserializers(data, many=True)
    return JsonResponse(serializer.data, safe=False)

# === Load City ===
def loadcity(request):
    # returs cities
    stateid = request.POST.get('id', None)
    # important: convert the QuerySet to a list object
    if stateid:
        data = cities1.objects.filter(state_id=stateid)
    else:
        data = cities1.objects.all()
    serializer = Cityserializers(data, many=True)
    return JsonResponse(serializer.data, safe=False)

# === On Bordiong ===
def onBording(request):
    """ submit client onbording form """
    userid = request.session['userid']
    countrow = client_vendor.objects.filter(user_id=userid).count()
    if countrow == 1:
        t = client_vendor.objects.get(user_id=userid)
        t.primary_name = request.POST['primary_name']
        t.primary_designation = request.POST['primary_designation']
        t.primary_email = request.POST['primary_email']
        t.primary_directdial = request.POST['primary_directdial']
        t.secondary_name = request.POST['secondary_name']
        t.secondary_designation = request.POST['secondary_designation']
        t.secondary_email = request.POST['secondary_email']
        t.secondary_directdial = request.POST['secondary_directdial']
        t.website = request.POST['website']
        t.industry_speciality_id = request.POST['industry_speciality_id']
        t.industry_type_id = request.POST['industry_type_id']
        t.save()

        t = user.objects.get(id=userid)
        t.user_name = request.POST['user_name']
        t.contact = request.POST['contact']
        t.address_line1 = request.POST['address_line1']
        t.address_line2 = request.POST['address_line2']
        t.country = request.POST['country']
        t.state = request.POST['state']
        t.zip_code = request.POST['zip_code']
        t.save()

        site_url = settings.BASE_URL
        if request.session['usertype'] == 1:
            pages = 'client'
        else:
            pages = 'vendor'
        data = {'success': 1, 'site_url': site_url, 'pages': pages}
        return JsonResponse(data)
    else:
        t = user.objects.get(id=userid)
        t.user_name = request.POST['user_name']
        t.contact = request.POST['contact']
        t.address_line1 = request.POST['address_line1']
        t.address_line2 = request.POST['address_line2']
        t.country = request.POST['country']
        t.state = request.POST['state']
        t.zip_code = request.POST['zip_code']
        t.status = '2'
        t.save()

        client_vendor.objects.create(
            primary_name=request.POST['primary_name'],
            primary_designation=request.POST['primary_designation'],
            primary_email=request.POST['primary_email'],
            primary_directdial=request.POST['primary_directdial'],
            primary_contact=request.POST['primary_directdial'],
            secondary_name=request.POST['secondary_name'],
            secondary_designation=request.POST['secondary_designation'],
            secondary_email=request.POST['secondary_email'],
            secondary_directdial=request.POST['secondary_directdial'],
            secondary_contact=request.POST['secondary_directdial'],
            alt_number1=request.POST['primary_directdial'],
            alt_number2=request.POST['secondary_directdial'],
            website=request.POST['website'],
            industry_speciality_id=request.POST['industry_speciality_id'],
            industry_type_id=request.POST['industry_type_id'],
            user_id=userid,
        )
        site_url = settings.BASE_URL
        if request.session['usertype'] == 1:
            pages = 'client'
        else:
            pages = 'vendor'
        from client.utils import noti_via_mail
        noti = noti_via_mail([i.id for i in user.objects.filter(usertype_id=4)], pages+' OnBoarding', 'New '+pages+ ' '+request.POST['user_name']+' completed his onboarding form', mail_client_onboarding if pages=='client' else mail_vendor_onboarding)
        data = {'success': 1, 'site_url': site_url, 'pages': pages}
        return JsonResponse(data)

# === Hook Add Camapign Data ===
def hook_add_campaign_data(user, objects_list):
    """
    * function Used to
        1) Check is data avaliable in set as default table for user.
            - If data exist then copy it with field names
        2) Copy save as txt data if exist
    * parameters:
        1) objects_list - holds objects like campaign, mapping, specification, delivery
    """
    message, success, title = "", 0, "error"

    if SetDefault.objects.count():
        set_defaults_data = SetDefault.objects.filter(user=user)

        if(set_defaults_data):
            for set_default in set_defaults_data:
                field_name = set_default.field
                field_value = set_default.values

                for obj in objects_list:
                    # check is mapping object has current field
                    if hasattr(obj, field_name):
                        is_attr_set = setattr(
                            obj, field_name, field_value)  # f.foo=bar
                        obj.save()  # this will update only

        else:
            message += "No set as default data exists for user"
    else:
        message += "Set as default table is empty so skipping cloning of data"
    print(message)
    return True

# === Create Campaign ===
@is_client
def create_campaign(request):
    # create campaign
    ''' user_id = request.session['userid'] '''
    internal = 0
    external = 0
    selected_outreach_method = []
    if request.method == "POST":
        form = NormalCampaignForm(request.POST)
        if len(form['method'].value()) > 0:
            selected_outreach_method = [int(i) for i in form['method'].value()]
        if form.is_valid():
            campaign = form.save()
            campaign.user_id = request.session['userid']
            campaign.raimainingleads = campaign.target_quantity
            if (campaign.end_date - campaign.start_date).days < 10:
                campaign.priority = "1"
                campaign.adhoc = True
            campaign.method.add(*form.cleaned_data['method'])
            campaign.save()

            HeaderSpecsValidation.objects.get_or_create(campaign=campaign, company_limit=4)[0]
            specification = Specification.objects.get_or_create(campaign=campaign)[0]
            mapping = Mapping.objects.get_or_create(campaign=campaign)[0]
            terms = Terms.objects.get_or_create(campaign=campaign)[0]
            delivery = Delivery.objects.get_or_create(campaign=campaign)[0]
            
            component_list = [d.function_name for d in LeadValidationComponents.objects.filter(is_default=True)]
            # print(component_list)
            SelectedLeadValidation.objects.get_or_create(campaign=campaign, component_list=component_list)

            objects_list = [campaign, specification, mapping, terms, delivery]
            hook_add_campaign_data(campaign.user, objects_list)
            notif_query = Notification.objects.filter(campaign_id_id = campaign.id)
            # check if notification & mail is already sent for this campaign..Akshay G.
            if len(notif_query) == 0:
                # add notification
                title = 'New Campaign'
                desc = "Normal Campaign '" + str(campaign.name) + "' created successfully"
                sender_id = request.session['userid']

                from client.utils import noti_via_mail
                super = [i.id for i in user.objects.filter(usertype__id=4)]
                super.append(sender_id)
                for superad in super:
                    RegisterNotification(sender_id, superad, "Normal Campaign '" + str(campaign.name) + "' created by "+str(campaign.user.user_name), title, 1, campaign, None)
                noti = noti_via_mail(super, title, desc, mail_new_campaign)

            return redirect('add_campaign_spec', id=campaign.id)
        else:
            user_obj = user.objects.get(id=request.session['userid'])
            internal = 1 if len([d for d in user_obj.child_user.all() if d.usertype_id == 9]) > 0 else 0
            external = 1 if len([d for d in user_obj.child_user.all() if d.usertype_id == 5]) > 0 else 0
    else:
        user_obj = user.objects.get(id=request.session['userid'])
        internal = 1 if len([d for d in user_obj.child_user.all() if d.usertype_id == 9]) > 0 else 0
        external = 1 if len([d for d in user_obj.child_user.all() if d.usertype_id == 5]) > 0 else 0
        form = NormalCampaignForm()
    context = {
        'campaignform': form,
        'internal':internal,
        'external':external,
        'selected_outreach_method': selected_outreach_method,
    }
    return render(request, 'campaign/create_campagin.html', context)



# === Campaiogn Type ===
@login_required
@is_client
def campaigntype(request):
    return render(request, 'campaign/campaign_type.html', {})

# === Add Campaign Specs ===
@is_users_campaign
def add_campaign_spec(request, id):
    # Add campaign specifications
    is_logged_in = check_is_logged_in(request)
    if(is_logged_in['success']):
        campaign_id = id
        message, success, title = "", 0, "error"

        component_list = None
        if ComponentsList.objects.count():
            component_list = ComponentsList.objects.all()
        else:
            message = "Component_List table is empty \n"
        selected_component_list = None
        selected_component_ids = [-1]  # list of ids
        # get selected component list

        campaign = None  # init
        specification = None
        mapping = None
        terms = None
        delivery = None
        lead_validation=None
        lead_validation_list=[]
        useAsTxtMapping = []
        volume_and_cpls = None
        txt_mapping_list = []
        job_titles = []

        if Campaign.objects.count():
            if Campaign.objects.filter(id=campaign_id):
                campaign = Campaign.objects.get(id=campaign_id)
            else:
                message += "Campaign with id '" + \
                    str(campaign_id) + "' not found"
        else:
            message += "Campaign table is empty\n"

        # print("campaign ", campaign)
        if campaign:
            # is status is draft mode i.e. 0  or pending:3
            if campaign.status == 0 or campaign.status == 3:
                if SelectedComponents.objects.count():
                    selected_component_list = SelectedComponents.objects.filter(
                        campaign=campaign,
                    )
                    selected_component_ids = SelectedComponents.objects.filter(
                        campaign=campaign).values_list('component', flat=True)
                else:
                    message += "Selected Component list for campaign" + \
                        str(campaign.id) + " is empty\n"

                specification = Specification.objects.get(campaign=campaign)
                mapping = Mapping.objects.get(campaign=campaign)
                terms = Terms.objects.get(campaign=campaign)
                delivery = Delivery.objects.get(campaign=campaign)
                if SelectedLeadValidation.objects.filter(campaign=campaign).count() > 0 :
                    obj=SelectedLeadValidation.objects.filter(campaign=campaign)
                    for lead_validation_list in obj:
                        lead_validation_list=lead_validation_list.component_list
                # use as text mapping
                txt_mapping_exist = UseAsTxtMapping.objects.count()
                if(txt_mapping_exist):  # if records exist
                    useAsTxtMapping = UseAsTxtMapping.objects.filter(
                        campaign=campaign).values()  # list of dicts
                    # print(useAsTxtMapping.values())
                # CPL and Volume of level of intent
                volume_and_cpls_exist = Volume_CPL.objects.count()
                if(volume_and_cpls_exist):  # if records exist
                    volume_and_cpls = Volume_CPL.objects.filter(
                        campaign=campaign).values()  # list of dicts
                    # print(volume_and_cpls)

                campaignform = CampaignForm()
                specificationform = SpecificationForm()
                mappingform = MappingForm()
                termsform = TermsForm()
                deliveryform = DeliveryForm()

                # data filler
                campaign_category = Campaign_category.objects.all()
                campaign_pacing = Campaign_pacing.objects.all()
                days = Days.objects.all()
                industry_types = industry_type.objects.all()

                #Abhi 23-09-2019
                #collect all job titles from database
                job_titles_from_data = job_title.objects.all()
                for jobs in job_titles_from_data:
                    job_titles +=eval(jobs.type)
                job_titles=list(dict.fromkeys(job_titles))    
                
                if mapping.industry_type != None:
                    unique_ind=find_unique_values([x.lower().strip() for x in list(industry_types.values_list('type',flat=True))],[x.lower().strip() for x in mapping.industry_type.split(',')])
                    industry_types=list(industry_types)+unique_ind

                job_levels = job_level.objects.all()

                if mapping.job_level != None:
                    unique_ind=find_unique_values([x.lower().strip() for x in list(job_levels.values_list('type',flat=True))],[x.lower().strip() for x in mapping.job_level.split(',')])
                    job_levels=list(job_levels)+unique_ind


                job_functions = job_function.objects.all()
                if mapping.job_function != None:
                    unique_ind=find_unique_values([x.lower().strip() for x in list(job_functions.values_list('type',flat=True))],[x.lower().strip() for x in mapping.job_function.split(',')])
                    job_functions=list(job_functions)+unique_ind

                countries_list = countries_list1.objects.all()
                # countries_list = countries.objects.all()

                company_sizes = company_size.objects.all()
                if mapping.company_size != None and len(mapping.company_size) > 0:
                    unique_ind=find_unique_values([x.lower().strip() for x in list(company_sizes.values_list('range',flat=True))],[x.strip() for x in mapping.company_size.split(',')])
                    company_sizes=list(company_sizes)+unique_ind
                company_sizes.remove('0') if '0' in company_sizes else company_sizes

                revenue_sizes = RevenueSize.objects.all()
                if mapping.revenue_size != None and len(mapping.revenue_size) > 0:
                    unique_ind=find_unique_values([x.lower().strip() for x in list(revenue_sizes.values_list('range',flat=True))],[x.strip() for x in mapping.revenue_size.split(',')])
                    revenue_sizes=list(revenue_sizes)+unique_ind
                revenue_sizes.remove('0') if '0' in revenue_sizes else revenue_sizes

                source_touch = source_touches.objects.all()
                level_intents = level_intent.objects.all()
                assets_all = assets.objects.all()
                region_list=region1.objects.all()
                # region_list=region.objects.all()

                data_headers = data_header.objects.all()
                delivery_methods = delivery_method.objects.all()
                delivery_times = delivery_time.objects.all()

                lead_validation=LeadValidationComponents.objects.all()

                set_as_defaults = SetDefault.objects.filter(
                    user=request.session['userid']
                ).values_list('field', flat=True)
                if HeaderSpecsValidation.objects.filter(campaign=campaign).count() > 0:
                    company_limit = HeaderSpecsValidation.objects.get(campaign=campaign)
                    lead_validate_mnth = company_limit.lead_validate_latest_mnth
                    company_limit = company_limit.company_limit    
                else:
                    company_limit = 4
                    lead_validate_mnth = 4
                if Campaign_others_specs.objects.filter(campaign_id=id).count() > 0 :
                    obj = Campaign_others_specs.objects.get(campaign_id=id)
                    other_specs = eval(obj.others_specs) if obj.others_specs != None else []
                else:
                    other_specs = []
                camp_other_specs = other_specs[0] if len(other_specs) > 0 else {}
                all_custom_specs = Client_custom_specs.objects.filter(user_id = request.session['userid'])
                selected_countries = mapping.country 
                all_states = []
                if selected_countries is not None:
                    selected_countries = selected_countries.split(',') if ',' in selected_countries else [selected_countries]
                    all_states = states1.objects.filter(country__name__in = selected_countries)
                selected_states = []
                unmapped_states = []
                if mapping.state is not None:
                    if mapping.state.strip() != '':
                        selected_states = mapping.state.split(',') if ',' in mapping.state else [mapping.state]
                if len(all_states) > 0 and len(selected_states) > 0:
                    for state in selected_states:
                        query = all_states.filter(name__iexact = state.strip())
                        if len(query) == 0 and state.strip() != '':
                            unmapped_states.append(state)
                else:
                    unmapped_states = selected_states
                all_cities = []
                if len(selected_states) > 0:
                    all_cities = cities1.objects.filter(state__name__in = selected_states)
                selected_cities = []
                unmapped_cities = []
                if mapping.city is not None:
                    if  mapping.city.strip() != '':
                        selected_cities = mapping.city.split(',') if ',' in mapping.city else [mapping.city]
                if len(all_cities) > 0 and len(selected_cities) > 0:
                    for city in selected_cities:
                        query = all_cities.filter(name__iexact = city.strip())
                        if len(query) == 0 and city.strip() != '':
                            unmapped_cities.append(city)
                else:
                    unmapped_cities = selected_cities
                file_url = {}
                if specification.abm_count > 0 or specification.suppression_count > 0:
                    file_url = create_suppression_abm_file(specification.suppression_file_content, specification.abm_file_content)
                context = {

                    'default_checked_industry': mapping.industry_type.split(',') if mapping.industry_type is not None else '',
                    'campaign': campaign,
                    'campaignform': campaignform,
                    'specification': specification,
                    'specificationform': specificationform,
                    "mappingform": mappingform,
                    "mapping": mapping,
                    "termsform": termsform,
                    "terms": terms,
                    "deliveryform": deliveryform,
                    "delivery": delivery,
                    # data filler
                    # set up data
                    "lead_validation":lead_validation,
                    "lead_list":lead_validation_list,
                    "company_limit":company_limit,
                    "lead_validate_mnth":lead_validate_mnth,
                    "campaign_category": campaign_category,
                    "campaign_pacing": campaign_pacing,
                    "days": days,
                    "industry_types": industry_types,
                    "job_title":job_titles,
                    "job_levels": job_levels,
                    "job_functions": job_functions,
                    "company_sizes": company_sizes,
                    "countries": countries_list,
                    "source_touches": source_touch,
                    "level_intent": level_intents,
                    "assets": assets_all,
                    "region_list":region_list,
                    "tc_header_status":delivery.tc_header_status,
                    "custom_header_status":delivery.custom_header_status,
                    "data_headers": data_headers,
                    "custom_headers":delivery.custom_header.split(',') if delivery.custom_header else [],
                    "delivery_methods": delivery_methods,
                    "delivery_times": delivery_times,
                    "revenue_sizes": revenue_sizes,
                    "set_as_defaults": set_as_defaults,
                    "useAsTxtMapping": useAsTxtMapping,
                    "volume_and_cpls": volume_and_cpls,
                    "component_list": component_list,
                    "selected_component_list": selected_component_list,
                    "selected_component_ids": selected_component_ids,
                    "i": 0,  # incrementing variable in template
                    "camp_other_specs":camp_other_specs,
                    'all_custom_specs':all_custom_specs,
                    'all_states': all_states,
                    'unmapped_states': unmapped_states,
                    'all_cities': all_cities,
                    'unmapped_cities': unmapped_cities,
                    'uploaded_file_url': file_url,
                }
                return render(request, 'campaign/add_campaign_specs.html', context)
            else:
                message += "Campaign status must be incomplete(i.e in draft mode or pending) to edit"
                print(message)
                return redirect("/client/create-campaign/")
        else:
            message += "No campaign found to edit with id : \n" + \
                str(campaign_id)
            print(message)
            return redirect("/client/create-campaign/")
    else:
        print(is_logged_in)
        return redirect("/logout/")

# === Find Unique Values ===
def find_unique_values(full_list,find_list):
    res=[]
    # print(full_list)
    # print(find_list)
    for key in find_list:
        if key.lower() not in full_list:
            res.append(key)
    return res

# === Change Campaign Status ===
@is_users_campaign
def change_campaign_status(request, id):
    # change camapign status on action
    campaign_id = id
    campaign = Campaign.objects.get(id=campaign_id)
    # change campaign status
    campaign.status = 0
    draft_camp_specs = draft_camp_status(campaign)
    if all(draft_camp_specs[campaign.id].values()):
        campaign.status = 3
    campaign.save()
    client_name = f"Tc-{campaign.user.id}" if campaign.user.anonymous_status == True else str(campaign.user.user_name)
    # add notification
    if campaign.status == 0:
        title = 'Campaign Specifications Updated!'
        desc = client_name+" updated some specifications of campaign " +str(campaign.name)
    if campaign.status == 3:
        title = 'Campaign Specifications Updated!'
        desc = client_name+" updated some specifications of campaign " +str(campaign.name)
    elif campaign.status == 1:
        title = 'Campaign Status Changed!'
        desc = "Status of campaign '" + \
            str(campaign.name) + "' changed to live"
    elif campaign.status == 4:
        title = 'Campaign Completed!'
        desc = "Campaign "+str(campaign.name) + " is now completed."

    sender_id = request.session['userid']
    receiver_id = request.session['userid']

    super = [i.id for i in user.objects.filter(usertype__id=4)]
    vendor = [i.client_vendor_id for i in campaign_allocation.objects.filter(status=5, campaign_id=campaign_id)]
    if vendor:
        super.extend(vendor)

    from client.utils import noti_via_mail
    noti = noti_via_mail(super, 'Campaign Specification Added/Updated', 'Specification has been added or been updated for campaign '+str(campaign.name), mail_campaign_updates)

    RegisterNotification(sender_id, receiver_id, desc, title, 1, campaign, None)
    superadmins = user.objects.filter(usertype__id=4)
    for superad in superadmins:
        RegisterNotification(sender_id, superad.id, desc, title, 1, campaign, None)

    # suggest vendors
    campaign_json(request,campaign_id)
    # import pdb;pdb.set_trace()
    suggest_vendors(request, campaign_id)
    #create json for campaign details
    if campaign.status == 0:
        return redirect('client_draft_campagin')
    return redirect('client_pending_campaign')


# === Save Specifications ===
def save_specifications(request):
    # save records through ajax
    # saving specifications selected values
    message, success, title = "", 0, "error"
    specification = None
    if request.method == 'POST':
        data_in_post = ["campaign", "field_name", "field_value"]
        # defined in utils.py
        is_data_in_post = check_all_data_available_in_post(
            data_in_post, request.POST)

        if is_data_in_post['success']:
            campaign_id = request.POST.get("campaign")  # 26
            field_name = request.POST.get("field_name")
            field_value = request.POST.get("field_value")

            if Campaign.objects.count():
                if Campaign.objects.filter(id=campaign_id):
                    campaign = Campaign.objects.get(id=campaign_id)
                    if Specification.objects.count():
                        if Specification.objects.get(campaign=campaign.id):
                            specification = Specification.objects.get(
                                campaign=campaign.id)
                        else:
                            message += "No Specification exists with campaign id :" + \
                                str(campaign.id)
                    else:
                        message += "Error while getting specification object . Specification table is empty"
                else:
                    message += "No campaign exists with id :" + \
                        str(campaign_id)
            else:
                message += "Campaign Table is empty"

            # update fields
            if specification:
                # check object has property with field name
                if hasattr(specification, field_name):
                    is_attr_set = setattr(
                        specification, field_name, field_value)  # f.foo=bar
                    specification.save()  # this will update only

                    success = 1
                    title = 'success'
                    message = "specification updated successfully"
                else:
                    message += "Error... Specification table has no field '" + field_name + "'"
        else:
            message = is_data_in_post['message']
    else:
        message = "Please post data using post method"

    jsonresponse = {
        "success": success,
        "title": title,
        "message": message,
    }
    return JsonResponse(jsonresponse)

# === Save new Specs Data ===
def save_new_specs_data(request):
    # save new campaign headers
    message, success, title = "", 0, "error"
    element = request.POST.get('field_name')
    values = request.POST.get('field_value')
    value = request.POST.get('value')
    specs_data = Campaign_others_specs.objects.get(campaign_id=request.POST.get('campaign'))
    print(specs_data.others_specs)
    if specs_data:
        data = eval(specs_data.others_specs)
        if values != None:
            data[0][element] = values.split(',')
        if value != None:
            data[0][element] += value.split(',')
        specs_data.others_specs = data
        specs_data.save()
        print(specs_data.others_specs)
        success = 1
        title = 'success'
        message = "specs updated successfully"
    jsonresponse = {
        "success": success,
        "title": title,
        "message": message,
        success:1
    }
    return JsonResponse(jsonresponse)

# === select campaign type ===
def save_lead_validation(request):
    # save lead validation through ajax
    # update lead validations
    campaign_id = request.POST.get("campaign")
    obj = Campaign.objects.get(id=campaign_id)
    # lead=SelectedLeadValidation(campaign=obj)
    lead = SelectedLeadValidation.objects.filter(campaign=obj)
    if lead:
        lead[0].component_list=list(request.POST.get('field_value').split(','))
        lead[0].save()
        jsonresponse = {
            "success": 1,
            "title": 'success',
            "message": 'Lead validation updated successfully',
        }
    else:
        lead = SelectedLeadValidation.objects.create(campaign=obj)
        lead.component_list=list(request.POST.get('field_value').split(','))
        lead.save()
        jsonresponse = {
            "success": 1,
            "title": 'success',
            "message": 'Lead validation updated successfully',
        }
    return JsonResponse(jsonresponse)

# === Save Mapping ===
def save_mapping(request):
    # save records through ajax
    # saving campaign and specufications mapping
    message, success, title = "", 0, "error"
    mapping = None

    if request.method == 'POST':
        data_in_post = ["campaign", "field_name", "field_value"]
        # defined in utils.py
        is_data_in_post = check_all_data_available_in_post(
            data_in_post, request.POST)

        if is_data_in_post['success']:
            campaign_id = request.POST.get("campaign")  # 26
            field_name = request.POST.get("field_name")
            field_value = request.POST.get("field_value").strip()

            if Campaign.objects.count():
                if Campaign.objects.filter(id=campaign_id):
                    campaign = Campaign.objects.get(id=campaign_id)
                    if Mapping.objects.count():
                        if Mapping.objects.filter(campaign=campaign.id):
                            mapping = Mapping.objects.get(campaign=campaign.id)
                        else:
                            message += "No mapping exists with campaign id :" + \
                                str(campaign.id)
                    else:
                        message += "Error while getting mapping object . mapping table is empty"
                else:
                    message += "No campaign exists with id :" + \
                        str(campaign_id)
            else:
                message += "Campaign Table is empty"

            # update fields
            if mapping:
                # check object has property with field name
                if hasattr(mapping, field_name):
                    is_attr_set = setattr(
                        mapping, field_name, field_value)  # f.foo=bar
                    mapping.save()  # this will update only
                    obj = Campaign.objects.get(id = campaign_id)
                    obj.geo = mapping.country
                    obj.save()
                    success = 1
                    title = 'success'
                    message = "mapping updated successfully"
                else:
                    message += "Error... mapping table has no field '" + field_name + "'"
        else:
            message = is_data_in_post['message']
    else:
        message = "Please post data using post method"

    jsonresponse = {
        "success": success,
        "title": title,
        "message": message,
    }
    return JsonResponse(jsonresponse)

# === Save Terms ===
def save_terms(request):
    # save records through ajax
    # savnig campaign terms data
    message, success, title = "", 0, "error"
    mapping = None

    if request.method == 'POST':
        data_in_post = ["campaign", "field_name", "field_value"]
        # defined in utils.py
        is_data_in_post = check_all_data_available_in_post(
            data_in_post, request.POST)

        if is_data_in_post['success']:
            campaign_id = request.POST.get("campaign")  # 26
            field_name = request.POST.get("field_name")
            field_value = request.POST.get("field_value")

            if Campaign.objects.count():
                if Campaign.objects.filter(id=campaign_id):
                    campaign = Campaign.objects.get(id=campaign_id)
                    if Terms.objects.count():
                        if Terms.objects.filter(campaign=campaign.id):
                            terms = Terms.objects.get(campaign=campaign.id)
                        else:
                            message += "No Terms exists with campaign id :" + \
                                str(campaign.id)
                    else:
                        message += "Error while getting Terms object . Terms table is empty"
                else:
                    message += "No campaign exists with id :" + \
                        str(campaign_id)
            else:
                message += "Campaign Table is empty"

            # update fields
            if terms:
                # check object has property with field name
                if hasattr(terms, field_name):
                    is_attr_set = setattr(
                        terms, field_name, field_value)  # equal to f.foo=bar
                    terms.save()  # this will update only

                    success = 1
                    title = 'success'
                    message = "terms updated successfully"
                else:
                    message += "Error... terms table has no field '" + field_name + "'"
        else:
            message = is_data_in_post['message']
    else:
        message = "Please post data using post method"

    jsonresponse = {
        "success": success,
        "title": title,
        "message": message,
    }
    return JsonResponse(jsonresponse)

# === Save Delivery ===
def save_delivery(request):
    # saving delivery details of campaign
    message, success, title = "", 0, "error"
    mapping = None
    if request.method == 'POST':
        data_in_post = ["campaign", "field_name", "field_value"]
        # defined in utils.py
        is_data_in_post = check_all_data_available_in_post(
            data_in_post, request.POST)

        if is_data_in_post['success']:
            campaign_id = request.POST.get("campaign")  # 26
            field_name = request.POST.get("field_name")
            field_value = request.POST.get("field_value")
            print(field_value)

            if Campaign.objects.count():
                if Campaign.objects.filter(id=campaign_id):
                    campaign = Campaign.objects.get(id=campaign_id)
                    if Delivery.objects.count():
                        if Delivery.objects.filter(campaign=campaign.id):
                            delivery = Delivery.objects.get(
                                campaign=campaign.id)
                        else:
                            message += "No delivery exists with campaign id :" + \
                                str(campaign.id)
                    else:
                        message += "Error while getting delivery object . delivery table is empty"
                else:
                    message += "No campaign exists with id :" + \
                        str(campaign_id)
            else:
                message += "Campaign Table is empty"

            # update fields
            if delivery:
                # check object has property with field name
                if hasattr(delivery, field_name):
                    is_attr_set = setattr(delivery, field_name, field_value)  # f.foo=bar
                    # if (field_value !=  ''):
                    #     delivery.tc_header_status = 1
                    # else:
                    #     delivery.tc_header_status = 0
                    delivery.save()  # this will update only

                    # campaign.completion_status = 'completed'
                    # campaign.save()

                    success = 1
                    title = 'success'
                    message = "delivery updated successfully"
                else:
                    message += "Error... delivery table has no field '" + field_name + "'"
        else:
            message = is_data_in_post['message']
    else:
        message = "Please post data using post method"

    jsonresponse = {
        "success": success,
        "title": title,
        "message": message,
    }
    return JsonResponse(jsonresponse)

# === Save Delivery Method Comments ===
@csrf_exempt
def save_delivery_method_comments(request):
    # print(request.POST)
    comments = {}
    if not request.POST.get("data") == '':
        campaign_id = request.POST.get("campaign_id")  # 26
        element = request.POST.get("element")
        data = request.POST.get("data")
        campaign = Delivery.objects.filter(campaign=campaign_id)
        print(campaign[0].__dict__)
        campaign[0].comments = comments.update({element: data})
        campaign[0].save()
    pass

# === Save Alternative text ===
def save_alternative_text(request):
    # save alternative texts
    campaign_id = request.POST.get("campaign_id")  # 26
    original_txt = request.POST.get("original_txt")
    alternative_txt = request.POST.get("alternative_txt").strip()
    # print("campaign id :", campaign_id)
    # print("original_txt :", original_txt)
    # print("alternative_txt :", alternative_txt)
    campaign = Campaign.objects.get(id=campaign_id)
    create_new_record = False

    is_table_records_exist = UseAsTxtMapping.objects.count()
    if is_table_records_exist:
        useAsTxtMapping = UseAsTxtMapping.objects.filter(
            campaign=campaign,
            original_txt=original_txt,
        ).first()
        if(useAsTxtMapping):
            if alternative_txt == '':  # if empty means delete record
                useAsTxtMapping.delete()
            else:
                useAsTxtMapping.alternative_txt = alternative_txt
                # print(useAsTxtMapping.__dict__)
                if (useAsTxtMapping.alternative_txt == ''):
                    useAsTxtMapping.delete()
                else:
                    useAsTxtMapping.save()
        else:
            create_new_record = True
    else:
        create_new_record = True

    if(create_new_record and alternative_txt):
        # create new mapping record
        useAsTxtMapping = UseAsTxtMapping.objects.create(
            campaign=campaign,
            original_txt=original_txt,
            alternative_txt=alternative_txt,
        )
        useAsTxtMapping.save()

    jsonresponse = {
        "success": 1,
        "title": "success",
        "message": "Alternative text saved successfully",
    }
    return JsonResponse(jsonresponse)

# === Save Lead Limit Campaign ===
def save_lead_limit_cmp(request):
    # Save lead limitation
    campaign_id = int(request.POST.get("campaign_id"))
    limit       = int(request.POST.get("lead_limit"))
    lead_validate_mnth = int(request.POST.get("latest_mnth"))
    if HeaderSpecsValidation.objects.filter(campaign=campaign_id).count() > 0 :
        lead_limit_update=HeaderSpecsValidation.objects.get(campaign=campaign_id)
        lead_limit_update.company_limit=limit
        lead_limit_update.lead_validate_latest_mnth=lead_validate_mnth
        lead_limit_update.save()
    else:
        HeaderSpecsValidation.objects.create(campaign_id=campaign_id,company_limit=limit)

    jsonresponse = {
        "success": 1,
        "title": "success",
        "message": "lead limit saved successfully",
    }
    return JsonResponse(jsonresponse)

# === Save Volume and CPL ===
def save_volume_and_cpl(request):
    # save campaign volume and cpl
    campaign_id = request.POST.get("campaign_id")  # 26
    level_of_intent = request.POST.get("level_of_intent")
    volume = request.POST.get("volume")
    cpl = request.POST.get("cpl")
    '''
    print("campaign id :", campaign_id)
    print("level_of_intent :", level_of_intent)
    print("volume :", volume)
    print("cpl :", cpl)
    '''

    campaign = Campaign.objects.get(id=campaign_id)
    create_new_record = False

    is_table_records_exist = Volume_CPL.objects.count()
    if is_table_records_exist:
        volume_CPL_list = Volume_CPL.objects.filter(
            campaign=campaign,
            level_of_intent=level_of_intent,
        ).first()
        if volume_CPL_list:
            volume_CPL_list.level_of_intent = level_of_intent
            volume_CPL_list.volume = volume
            volume_CPL_list.cpl = cpl
            volume_CPL_list.save()
        else:
            create_new_record = True
    else:
        create_new_record = True

    if(create_new_record):
        # create new mapping record
        volume_CPL_list = Volume_CPL.objects.create(
            campaign=campaign,
            level_of_intent=level_of_intent,
            volume=volume,
            cpl=cpl,
        )
        volume_CPL_list.save()

    jsonresponse = {
        "success": 1,
        "title": "success",
        "message": "Volume and CPL saved successfully",
    }
    return JsonResponse(jsonresponse)

# === Delete Volume and CPL ===
def delete_volume_and_cpl(request):
    # delete campaign colume and cpl
    campaign_id = request.POST.get("campaign_id")  # 26
    level_of_intent = request.POST.get("level_of_intent")
    '''
    print("level_of_intent :", level_of_intent)
    print("campaign id :", campaign_id)
    '''
    campaign = Campaign.objects.get(id=campaign_id)
    jsonresponse = None

    is_table_records_exist = Volume_CPL.objects.count()
    if is_table_records_exist:
        volume_CPL_list = Volume_CPL.objects.filter(
            campaign=campaign,
            level_of_intent=level_of_intent,
        ).first()
        if(volume_CPL_list):
            volume_CPL_list.delete()
            jsonresponse = {
                "success": 1,
                "title": "success",
                "message": "Volume and CPL deleted successfully",
            }
    else:
        jsonresponse = {
            "success": 0,
            "title": "error",
            "message": "Error while deleting volume and CPL.. record not exist",
        }
    return JsonResponse(jsonresponse, safe=False)

# === Nested SetAttr on Objects ===
def nested_setattr(obj, attr, val):
# getattr and setattr on nested objects?
    pre, _, post = attr.rpartition('.')
    return setattr(nested_getattr(obj, pre) if pre else obj, post, val)

# using wonder's beautiful simplification: https://stackoverflow.com/questions/31174295/getattr-and-setattr-on-nested-objects/31174427?noredirect=1#comment86638618_31174427

# === Nested SetAttr ===
def nested_getattr(obj, attr, *args):
    def _getattr(obj, attr):
        return getattr(obj, attr, *args)
    return functools.reduce(_getattr, [obj] + attr.split('.'))

# === Check ABM Template ===
def checkABMTemplate(header):
    # Checket Template is valid or not
    labels = ['DOMAIN_NAME','COMPANY_NAME']
    if set(header) == set(labels):
        return 1
    else:
        return 0

# === Check Suppression Template ===
def checkSuppressionTemplate(header):
    # Checket Template is valid or not
    labels = ['DOMAIN_NAME','COMPANY_NAME','EMAIL']
    if set(header) == set(labels):
        return 1
    else:
        return 0

# === Create Dict List ===
def creat_dict_list(data,header,last_id,abm_data):
    # Convert List to List of dict.
    from datetime import datetime
    lead_data=[]
    current_data={}
    date = datetime.now()
    currant_date = date.strftime("%B %d %Y")
    time = date.strftime('%H:%M:%S')
    if len(abm_data)>0:
        for i in range(len(data)):
            if any(d['DOMAIN_NAME'] == data[i][0] for d in abm_data) != True:
                for j in range(len(header)):
                    current_data[header[j]]=data[i][j]
                last_id += 1
                new_data = {'id':last_id,'time':time, 'date': currant_date}
                current_data.update(new_data)
                lead_data.append(current_data)
                current_data={}
    else:
        for i in range(len(data)):
            for j in range(len(header)):
                if header[j] == "EMAIL":
                    emails = data[i][j].split(',')
                    current_data[header[j]]=emails
                else:
                    current_data[header[j]]=data[i][j]
            last_id += 1
            new_data = {'id':last_id,'time':time, 'date': currant_date}
            current_data.update(new_data)
            lead_data.append(current_data)
            current_data={}
    return lead_data

# === Get last id of ABM ===
def get_last_id_abm(campaign):
    """ get last ID OF ABM """
    last_id=Specification.objects.get(campaign=campaign)
    return last_id.abm_count

# === Get Last id Suppression ===
def get_last_id_suppression(campaign):
    """ get last ID OF Suppression """
    last_id=Specification.objects.get(campaign=campaign)
    return last_id.suppression_count

# === Upload Single File ===
@csrf_exempt
def upload_single_file(request):
    """ used to upload single file form """
    message, success, title = "", 0, "error"
    is_data_ok = False

    file_paths = {}
    specification = None
    if request.method == 'POST':
        data_in_post = ["id_campaign", "field_name"]
        # defined in utils.py
        is_data_in_post = check_all_data_available_in_post(
            data_in_post, request.POST)

        if is_data_in_post['success']:
            is_data_ok = True
        else:
            message = is_data_in_post['message']

        if is_data_ok:
            for filename, file in request.FILES.items():
                name = request.FILES[filename].name

                # get campaign id
                id_campaign = request.POST.get("id_campaign")

                # read csv file without storing and fetch data into list of dict..Akshay G.
                myfile = request.FILES[filename]
                if myfile.name.endswith('.csv'):
                    try:
                        data = pandas.read_csv(myfile,skip_blank_lines=True,na_filter=False,encoding ='latin1')
                        data = data.dropna()
                    except:
                        jsonresponse = {
                            "success": 0,
                            "title": 'Empty file !',
                            "message": 'Please upload file with data.',
                        }
                        return JsonResponse(jsonresponse, safe=False)
                elif myfile.name.endswith('.xlsx'):
                    try:
                        xl = pandas.ExcelFile(myfile)
                        data = pandas.read_excel(xl,xl.sheet_names[0],dtype=str).fillna('')
                        data = data.dropna()
                    except:
                        jsonresponse = {
                            "success": 0,
                            "title": 'Empty file !',
                            "message": 'Please upload file with data.',
                        }
                        return JsonResponse(jsonresponse, safe=False)
                else:
                    jsonresponse = {
                        "success": 0,
                        "title": 'Unsupported file format!',
                        "message": 'Please upload only .csv or .xlsx file.',
                    }
                    return JsonResponse(jsonresponse, safe=False)
                # data = pandas.read_csv(myfile,skip_blank_lines=True,na_filter=False,encoding ='latin1')
                header, csv_data = fetch_csv_data(data)
            
                # django get campaign object from model
                campaign = Campaign.objects.filter(id=id_campaign).first()
                if campaign:
                    # get specification record
                    specification = Specification.objects.filter(
                        campaign=campaign).first()
                    if specification:

                        # get field name to save
                        field_name = request.POST.get("field_name")
                        file_type = request.POST.get('type')
                        # check object has property with field name
                        if hasattr(specification, field_name):
                            # nested_setattr(object, 'pet.name', 'Sparky')
                            model_field_name = str(field_name) + ".name"
                            model_field_name = model_field_name.replace(
                                " ", "")
                            #abhi 9-2-2019 store abm data in list formate in database
                            if field_name == 'abm_company_file' :
                                if checkABMTemplate(header):
                                    if len(csv_data) > 0:
                                        if file_type == 'merge':
                                            csv_data = merge_abm_suppr_file_data(specification.abm_file_content, csv_data)
                                            message = "specification merged successfully"
                                        else:
                                            message = "specification updated successfully"
                                        specification.abm_file_content = csv_data
                                        specification.abm_count=len(csv_data)
                                        specification.save()
                                        success = 1
                                        title = 'success'
                                    else:
                                        message += "ABM template is empty, Please upload valid template!"
                                        jsonresponse = {
                                            "success": 0,
                                            "title": request.POST,
                                            "message": message,
                                        }
                                        return JsonResponse(jsonresponse, safe=False)
                                else:
                                    message += "ABM template is invalid, Please enter valid template!"
                                    jsonresponse = {
                                        "success": 0,
                                        "title": request.POST,
                                        "message": message,
                                    }
                                    return JsonResponse(jsonresponse, safe=False)
                            elif field_name == 'suppression_company_file':
                                if checkSuppressionTemplate(header):
                                    if len(csv_data) > 0:
                                        if file_type == 'merge':
                                            csv_data = merge_abm_suppr_file_data(specification.suppression_file_content, csv_data)
                                            message = "specification merged successfully"
                                        else: 
                                            message = "specification updated successfully"
                                        specification.suppression_file_content=csv_data
                                        specification.suppression_count=len(csv_data)
                                        specification.save()
                                        success = 1
                                        title = 'success'
                                    else:
                                        message += "Suppression template is empty, Please upload valid template!"
                                        jsonresponse = {
                                            "success": 0,
                                            "title": request.POST,
                                            "message": message,
                                        }
                                        return JsonResponse(jsonresponse, safe=False)
                                else:
                                    message += "Suppression template is invalid, Please upload valid template!"
                                    jsonresponse = {
                                        "success": 0,
                                        "title": request.POST,
                                        "message": message,
                                    }
                                    return JsonResponse(jsonresponse, safe=False)
                                    #abhi
                        else:
                            message += "Error... Specification table has no field '" + field_name + "'"
                    else:
                        message += "Specification not exists with campaign: '", str(
                            campaign), "'"
                else:
                    message += "Campaign not exist with id : '", id_campaign, "'"

                # uploaded_file_url = fs.url(filename)
                success = 1
    else:
        message = "Please post data using post method"
    if specification:
        file_paths.update(create_suppression_abm_file(specification.suppression_file_content, specification.abm_file_content))
    jsonresponse = {
        "success": 1,
        "title": request.POST,
        "message": message,
        'file_paths': file_paths,
    }

    return JsonResponse(jsonresponse, safe=False)

# === Add Default Function ===
def add_default(request):
    # set as default function
    message, success, title, is_data_ok = "", 0, "error", False
    is_logged_in = check_is_logged_in(request)
    if(is_logged_in['success']):
        if request.method == "POST":
            data_in_post = ["invoke_div_name", "value"]
            # defined in utils.py
            is_data_in_post = check_all_data_available_in_post(
                data_in_post, request.POST)

            if is_data_in_post['success']:
                is_data_ok = True
            else:
                message = is_data_in_post['message']
        else:
            message += "Please use post method to post data"
    else:
        message = "Please login to add set as default fields of campaign"

    userid = request.session['userid']
    current_user = None
    if userid:
        if(user.objects.count()):
            if(user.objects.filter(id=userid)):
                current_user = user.objects.get(id=userid)
            else:
                message += "No records found with userid: '", reuest_userid, "' when adding set as default"
        else:
            message += "USer table looks empty when tried to get user for set as default"
    else:
        message += "user_id is not found in session"

    print(message)

    if(is_data_ok and current_user):
        field_name = request.POST.get("invoke_div_name")
        field_value = request.POST.get("value")

        """
        @params:
            field -- used for filter purpose
            defaults -- are used for insertion
        @output:
            object : the object of model in dict format
            created : Boolean value(True/ False)
        """
        obj, created = SetDefault.objects.update_or_create(
            field=field_name, user=current_user,
            defaults={'field': field_name,
                      'values': field_value, 'user': current_user},
        )
        success = 1
        title = "success"
        modified_field_name = field_name.title().replace("_", " ")
        if created:
            message = "'" + modified_field_name + \
                "' field added successfully as 'Default field'. It will automatically added to future campaign"
        else:
            message = "'" + modified_field_name + \
                "' field updated successfully as 'Default field'. It will automatically added to future campaign"

    jsonresponse = {
        "success": success,
        "title": title,
        "message": message,
    }
    return JsonResponse(jsonresponse)

# === Remove Default ===
def remove_default(request):
    # remvoing defaults from database
    message, success, title, is_data_ok = "", 0, "error", False
    is_logged_in = check_is_logged_in(request)
    if(is_logged_in['success']):
        if request.method == "POST":
            data_in_post = ["invoke_div_name", "value"]
            # defined in utils.py
            is_data_in_post = check_all_data_available_in_post(
                data_in_post, request.POST)

            if is_data_in_post['success']:
                is_data_ok = True
            else:
                message = is_data_in_post['message']
        else:
            message += "Please use post method to post data"
    else:
        message = "Please login to add set as default fields of campaign"

    userid = request.session['userid']
    current_user = None
    if userid:
        if(user.objects.count()):
            if(user.objects.filter(id=userid)):
                current_user = user.objects.get(id=userid)
            else:
                message += "No records found with userid: '", reuest_userid, "' when adding set as default"
        else:
            message += "USer table looks empty when tried to get user for set as default"
    else:
        message += "user_id is not found in session"

    print(message)

    if(is_data_ok and current_user):
        field_name = request.POST.get("invoke_div_name")
        field_value = request.POST.get("value")

        if SetDefault.objects.filter(field=field_name, user=current_user):
            # delete record
            SetDefault.objects.get(
                field=field_name, user=current_user).delete()
            success = 1
            title = "success"
            modified_field_name = field_name.title().replace("_", " ")
            message = "'" + modified_field_name + \
                "' field removed successfully from 'Default field' list."

    jsonresponse = {
        "success": success,
        "title": title,
        "message": message,
    }
    return JsonResponse(jsonresponse)

# === Suggest Vendors Old Client Vendor Table ===
def suggest_vendors_old_client_vendor_table(request, campaign_id):
# campaign_id

    message, success, title = "", 0, "error"
    # campaign_id = 49 # testing
    campaign = Campaign.objects.get(id=campaign_id)
    if campaign:
        #json_campaign = serializers.serialize('json', [campaign], indent=4)
        specification = Specification.objects.get(campaign=campaign)
        mapping = Mapping.objects.get(campaign=campaign)
        terms = Terms.objects.get(campaign=campaign)
        delivery = Delivery.objects.get(campaign=campaign)

        industry_types = []
        job_levels = []

        # get matching parameters
        # all() is important for many to many field
        camp_source_in_touches = campaign.method.all()
        if not camp_source_in_touches:
            message = "\n Source In touches in campaign is null"

        # campaign industry type
        camp_industry_type_str = mapping.industry_type
        # print(mapping.__dict__)
        if camp_industry_type_str:
            camp_industry_type_list = camp_industry_type_str.split(",")
            industry_types = industry_type.objects.filter(
                type__in=camp_industry_type_list,
            )
            # print(industry_types)
        else:
            message += "\n Industry Type is null in campaign"

        # campaign job level
        camp_job_level_str = mapping.job_level
        if camp_job_level_str:
            camp_job_level_list = camp_job_level_str.split(",")
            job_levels = job_level.objects.filter(
                type__in=camp_job_level_list,
            )
            # print(job_levels)
        else:
            message += "\n Industry Type is null in campaign"
        # get vendors
        # filter(user__usertype='2') -- filters by foreign key properties
        # here vendor contains user foreign key -- user table has usertype foreign key -- usertype table has type property
        client_vendors = client_vendor.objects.filter(user__usertype__type='vendor').filter(
            Q(marketing_method__in=camp_source_in_touches) |
            Q(industry_type__in=industry_types) |
            Q(job_levels__in=job_levels)
        ).distinct()

        if client_vendors:
            print(client_vendors[0].user.usertype.type)

        for index, vendor in enumerate(client_vendors):
            user_id = vendor.user.id
            # print(user_id)
            is_user_exists = user.objects.filter(id=user_id,status=3)
            if(is_user_exists):
                User = user.objects.get(id=user_id)
                # print(User)

                match_vendor = match_campaign_vendor.objects.filter(
                    campaign=campaign,
                    client_vendor=User,
                )
                if not match_vendor:
                    match_campaign_vendor.objects.create(
                        campaign=campaign,
                        client_vendor=User,
                        is_active=1
                    )
        # print(client_vendors)
        # return HttpResponse("completed")
        return True
    else:
        # return HttpResponse("Campaign is empty of id :", str(camapaign_id))
        return False

# === Suggest Vendor ===
def suggest_vendors(request, campaign_id):  # campaign_id

    message, success, title = "", 0, "error"
    # campaign_id = 49 # testing
    campaign = Campaign.objects.get(id=campaign_id)
    if campaign:
        #json_campaign = serializers.serialize('json', [campaign], indent=4)
        specification = Specification.objects.get(campaign=campaign)
        mapping = Mapping.objects.get(campaign=campaign)
        terms = Terms.objects.get(campaign=campaign)
        delivery = Delivery.objects.get(campaign=campaign)

        outreach_method = []
        sweet_spot_geo = []  # country
        complex_program = []  # campaign_type
        industry_types = []

        # import ipdb; ipdb.set_trace(context=20)
        # get matching parameters # outreach_method
        # all() is important for many to many field
        camp_source_in_touches = campaign.method.all()
        if not camp_source_in_touches:
            message = "\n Source In touches in campaign is null"

        # convert queryset values to list
        method_list = []
        for method in camp_source_in_touches:
            method_list.append(str(method.id))

        #Kishor Select Hybrid Then Email,Tele Vendor Suggest
        if str(Hybrid) in method_list:
            if email_out not in method_list:
                method_list.append(source_touches.objects.get(id=email_out).id)
            if tele_out not in method_list:
                method_list.append(source_touches.objects.get(id=tele_out).id)
        # ////
        method_list = list(set(method_list))
        print(method_list)
        # campaign country
        camp_country_str = mapping.country
        # print(mapping.__dict__)

        if camp_country_str:
            camp_country_list = camp_country_str.split(",")
            matching_countries = countries_list1.objects.filter(
                name__in=camp_country_list,
            )
            # matching_countries = countries.objects.filter(
            #     name__in=camp_country_list,
            # )
            # print(matching_countries)
        else:
            message += "\n Country is null in campaign"

        # get vendors
        # filter(user__usertype='2') -- filters by foreign key properties
        # here vendor contains user foreign key -- user table has usertype foreign key -- usertype table has type property
        # !!! -- temp removed .filter(user__status=3)

        # Filter Django database for field containing any value in an array
        # query1 = Q()
        # for letter in camp_country_list:
        #     query1 = query1 | Q(sweet_spot_text__icontains=letter)

        # Filter Django database for field containing any value in an array
        query2 = Q()
        # check if camp methods matches with vendor onboarding- data assessment- 'Lead Gen options'...Akshay G..Date - 20th Nov, 2019
        for method_id in method_list:
            query2 = query2 | Q(lead_gen_capacity__icontains = str(method_id))
        query3 = Q()
        # check if camp type matches with vendor onboarding 'Vendor Type'..Akshay G..Date - 20th Nov, 2019
        query3 = query3 | Q(vendor_type__icontains=str(campaign.type))
        # print(query)


        vendors = data_assesment.objects.filter(user__usertype__type='vendor').filter(query2,query3).distinct()

        if vendors:
            print(vendors[0].user.usertype.type)

        match_campaign_vendor.objects.filter(campaign=campaign).delete()

        for index, vendor in enumerate(vendors):
            user_id = vendor.user.id
            # print(user_id)
            is_user_exists = user.objects.filter(id=user_id,status=3)
            if(is_user_exists):
                User = user.objects.get(id=user_id)
                # print(User)

                match_vendor = match_campaign_vendor.objects.filter(
                    campaign=campaign,
                    client_vendor=User,
                )
                if not match_vendor:
                    match_campaign_vendor.objects.create(
                        campaign=campaign,
                        client_vendor=User,
                        is_active=1
                    )
        # print(client_vendors)
        # return HttpResponse("completed")
        return True
    else:
        # return HttpResponse("Campaign is empty of id :", str(camapaign_id))
        return False

# === Save Selected Components ===
@csrf_exempt
def save_selected_components(request):
    # save selected components
    # return HttpResponse("save_selected_components() called from ajax")
    message, success, title = "", 0, "error"

    if request.method == 'POST':
        data_in_post = ["id_campaign", "id_selected_component"]
        # defined in utils.py
        is_data_in_post = check_all_data_available_in_post(
            data_in_post, request.POST)

        if is_data_in_post['success']:
            campaign_id = request.POST.get("id_campaign")  # 26
            id_component = request.POST.get("id_selected_component")

            campaign = None
            # check is campaigns are available in table
            campaigns_cnt = Campaign.objects.count()
            if campaigns_cnt:
                is_campaign_exist = Campaign.objects.filter(id=campaign_id)
                if is_campaign_exist:
                    campaign = Campaign.objects.get(id=campaign_id)
                else:
                    message += "Campaign with id " + \
                        str(campaign_id) + " not exist in table\n"
            else:
                message += "Campaign table is empty\n"

            # check is selected component available in original list of components
            if campaign:
                cnt_components = ComponentsList.objects.count()
                if cnt_components:
                    is_component_valid = ComponentsList.objects.filter(
                        id=id_component,
                    ).first()
                    if is_component_valid:
                        # check is already exist
                        # change abm & suppression status, if those components added...Akshay G.
                        camp_specification = Specification.objects.get(campaign = campaign)
                        if is_component_valid.invoke_div == 'account_supression':
                            camp_specification.suppression_status = 1
                        if is_component_valid.invoke_div == 'account_based_marketing':
                            camp_specification.abm_status = 1
                        camp_specification.save()
                        is_already_exist = SelectedComponents.objects.filter(
                            campaign=campaign,
                            component=is_component_valid,
                        )
                        if is_already_exist:
                            message = "Selected component already exist in selected_list table( so skipping record insertion)."
                        else:
                            # add to selected element list
                            SelectedComponents.objects.create(
                                campaign=campaign,
                                component=is_component_valid,
                            )
                            success = 1
                            message = "Selected component added successfully."  # dont append replace message
                    else:
                        message += "Selected component '" + id_component + \
                            "' not exist in Components_List table\n"
                else:
                    message += "It looks like component table is empty\n"
        else:
            message = is_data_in_post['message']
    else:
        message = "Please post data using post method"

    jsonresponse = {
        "success": success,
        "title": title,
        "message": message,
    }
    return JsonResponse(jsonresponse)

# === Remove Selected Components ===
@csrf_exempt
def remove_selected_component(request):
    # remove component through ajax
    message, success, title = "", 0, "error"
    if request.method == 'POST':
        data_in_post = ["id_campaign", "id_selected_component"]
        # defined in utils.py
        is_data_in_post = check_all_data_available_in_post(
            data_in_post, request.POST)

        if is_data_in_post['success']:
            campaign_id = request.POST.get("id_campaign")  # 26
            id_component = request.POST.get("id_selected_component")

            campaign = None
            # check is campaigns are available in table
            campaigns_cnt = Campaign.objects.count()
            if campaigns_cnt:
                is_campaign_exist = Campaign.objects.filter(id=campaign_id)
                if is_campaign_exist:
                    campaign = Campaign.objects.get(id=campaign_id)
                else:
                    message += ("Campaign with id " +
                                str(campaign_id) + " not exist in table\n")
            else:
                message += "Campaign table is empty \n"

            # check is selected component available in original list of components
            if campaign:
                cnt_components = ComponentsList.objects.count()
                if cnt_components:
                    is_component_valid = ComponentsList.objects.filter(
                        id=id_component,
                    ).first()
                    if is_component_valid:
                        # check is already exist
                        selected_component = SelectedComponents.objects.filter(
                            campaign=campaign,
                            component=is_component_valid,
                        ).first()
                        if selected_component:
                            selected_component.delete()
                            # change abm & suppression status, if those components removed..Akshay G.
                            camp_specification = Specification.objects.get(campaign = campaign)
                            if is_component_valid.invoke_div == 'account_supression':
                                camp_specification.suppression_status = 0
                            if is_component_valid.invoke_div == 'account_based_marketing':
                                camp_specification.abm_status = 0
                            camp_specification.save()
                            if is_component_valid.label == 'Custom Question':
                                custom_question = Mapping.objects.get(campaign_id=campaign)
                                custom_question.custom_question = 0
                                if Question_forms.objects.filter(campaign_id=campaign).exists():
                                    Question_forms.objects.get(campaign_id=campaign).delete()
                                custom_question.save()
                            success = 1
                            message = "Selected Component successfully removed from table record"
                        else:
                            message = "Selected component not exist in table."
                    else:
                        message += "Selected component '" + selected_component + \
                            "' not exist in Components_List table\n"
                else:
                    message += "It looks like component table is empty\n"
        else:
            message = is_data_in_post['message']
    else:
        message = "Please post data using post method"

    jsonresponse = {
        "success": success,
        "title": title,
        "message": message,
    }
    return JsonResponse(jsonresponse)

# === select campaign type ===
def test_ajax(request):
    return render(request, 'testing_ajax.html')

# === Normal Target ===
# def normal_target(campaign):
    # setting target quantities to default while cloning normal campaign -Amey 17 June
    # camp = Campaign.objects.filter(id=campaign)
    # if camp:
    #     tc_target = camp[0].tc_target_quantity
    #     camp[0].tc_raimainingleads = tc_target
    #     camp[0].tc_approveleads = 0
    #
    #     client_target = camp[0].client_target_quantity
    #     camp[0].client_raimainingleads = client_target
    #     camp[0].client_approveleads = 0
    #
    #     camp[0].save()

# === RFQ Target ===
# def rfq_target(campaign):
    # setting target quantities to default while cloning RFQ campaign-Amey
    # camp = Campaign.objects.filter(id=campaign)
    # if camp:
    #     main_target = camp[0].target_quantity
    #     camp[0].client_target_quantity = main_target
    #     camp[0].client_raimainingleads = main_target
    #     camp[0].client_approveleads = 0
    #     camp[0].tc_target_quantity = 0
    #     camp[0].tc_approveleads = 0
    #     camp[0].tc_raimainingleads = 0
    #     camp[0].save()

# === Clone Campaign ===
@is_users_campaign
def clone_campaign(request, campaign_id):
    # cloning campaign
    campaign = Campaign.objects.filter(id=campaign_id)
    if campaign:
        old_campaign = Campaign.objects.get(id=campaign_id)
        old_id = old_campaign.id
        methods = old_campaign.method.all()

        specification = Specification.objects.filter(campaign=old_campaign)
        mapping = Mapping.objects.filter(campaign=old_campaign)
        delivery = Delivery.objects.filter(campaign=old_campaign)
        terms = Terms.objects.filter(campaign=old_campaign)
        use_as_text = UseAsTxtMapping.objects.filter(campaign=old_campaign).values()
        cust_question = Question_forms.objects.filter(campaign=old_campaign)
        headerSpecs = HeaderSpecsValidation.objects.filter(campaign=old_campaign)
        obj=SelectedLeadValidation.objects.filter(campaign=old_campaign)


        old_campaign.pk = None
        old_campaign.name = "cloned " + old_campaign.name 
        old_campaign.save()
        old_campaign.status = 0
        old_campaign.method.add(*methods)
        old_campaign.save()

        # get new campaign id
        new_campaign = old_campaign
        new_id = new_campaign.id

        if headerSpecs:
            if headerSpecs[0].company_limit:
                HeaderSpecsValidation.objects.create(campaign=new_campaign,company_limit=headerSpecs[0].company_limit)

        # clone custom lead validation in new campaign
        if obj:
            if obj[0].component_list:
                lead = SelectedLeadValidation.objects.create(campaign=new_campaign)
                lead.component_list=obj[0].component_list
                lead.save()

        if use_as_text:
            for i in use_as_text:
                UseAsTxtMapping.objects.create(campaign=new_campaign, original_txt = i['original_txt'], alternative_txt = i['alternative_txt'])

        if specification:
            specification[0].pk = None
            specification[0].campaign = new_campaign
            specification[0].save()

        if mapping:
            mapping[0].pk = None
            mapping[0].campaign = new_campaign
            mapping[0].save()

        if cust_question:
            cust_question[0].pk = None
            cust_question[0].campaign = new_campaign
            cust_question[0].save()

        if terms:
            terms[0].pk = None
            terms[0].campaign = new_campaign
            terms[0].save()

        if delivery:
            delivery[0].pk = None
            delivery[0].campaign = new_campaign
            delivery[0].save()

        # copy selected components also in draggable campaign
        if SelectedComponents.objects.count():
            selected_component_list = SelectedComponents.objects.filter(campaign_id=old_id)
            modified_selected_component_list = []
            for component in selected_component_list:
                component.pk = None
                component.campaign = new_campaign
                modified_selected_component_list.append(component)

            if modified_selected_component_list:
                SelectedComponents.objects.bulk_create(modified_selected_component_list)

        form = NormalCampaignForm(instance=new_campaign)
        context = {
            'campaign': new_campaign,
            'campaignform': form,
            'outreach_method': [d.type for d in new_campaign.method.all()]
        }
        return render(request, 'campaign/campaign_edit_clone.html', context)
    messages.error(request, 'Campaign not found, Please try again')
    return HttpResponseRedirect('/')

# === Edit Clone Campaign ===
@is_users_campaign
def edit_clone_campaign(request, campaign_id):
   # edit campaign only in case of draft
   # for other state use other function

    campaign_obj = Campaign.objects.get(id=campaign_id)
    form = NormalCampaignForm(instance=campaign_obj)
    if request.method == 'POST':
        form = NormalCampaignForm(request.POST, instance=campaign_obj)
        if form.is_valid():
            campaign = form.save(commit=False)
            campaign.raimainingleads = campaign.target_quantity
            if (campaign.end_date - campaign.start_date).days < 10:
                campaign.priority = "1"
                campaign.adhoc = True
            else:
                campaign.priority = "3"
                campaign.adhoc = False
            campaign.method.clear()
            campaign.method.add(*form.cleaned_data['method'])
            campaign.save()

            return redirect('add_campaign_spec', id=campaign.id)
    context = {
        'campaign': campaign_obj,
        'campaignform': form,
        'outreach_method': [d.type for d in campaign_obj.method.all()]
    }
    return render(request, 'campaign/campaign_edit_clone.html', context)

# === Edit Campaign ===
@is_users_campaign
def edit_campaign(request, campaign_id):
    # edit campaign only in case of draft
    # for other state use other function
    import dateutil.parser
    import datetime
    internal = 0
    external = 0
    campaign_obj = Campaign.objects.get(id=campaign_id)
    camp_start_date = campaign_obj.start_date
    camp_end_date = campaign_obj.end_date
    date = datetime.datetime.now().date()
    #abhi - 3-oct-2019
    #Following condition for check start date is greter then of current date and end date is greter then of start date.
    #if not then save blank startdate and enddate in database.
    startdate_error = None
    enddate_error = None
    if camp_start_date:
        if isinstance(camp_start_date, datetime.date) == False:
            camp_start_date = datetime.strptime(camp_start_date, '%Y-%m-%d')
        if date > camp_start_date:
            campaign_obj.start_date = None
            startdate_error = 'Start date is later than Current date. Please select another date.'
        elif camp_start_date.weekday() in [5, 6]:
            campaign_obj.start_date = None
            startdate_error = 'Start date is a weekend. Please select another date.'
    if camp_end_date:  
        if isinstance(camp_end_date, datetime.date) == False:
            camp_end_date = datetime.strptime(camp_end_date, '%Y-%m-%d')
        if camp_start_date:
            if camp_start_date > camp_end_date:
                campaign_obj.end_date = None
                enddate_error = 'Start date is further than end date. Please select another date.'
        if date > camp_end_date:
            campaign_obj.end_date = None
            enddate_error = 'End date is later than Current date. Please select another date.'
        elif camp_end_date.weekday() in [5, 6]:
            campaign_obj.end_date = None
            enddate_error = 'End date is a weekend. Please select another date.'
    campaign_obj.save()
    #End

    form = NormalCampaignForm(instance=campaign_obj)
    if request.method == 'POST':
        form = NormalCampaignForm(request.POST, instance=campaign_obj)
        if form.is_valid():
            campaign = form.save(commit=False)
            campaign.raimainingleads = campaign.target_quantity
            if (campaign.end_date - campaign.start_date).days < 10:
                campaign.priority = "1"
                campaign.adhoc = True
            else:
                campaign.priority = "3"
                campaign.adhoc = False
            campaign.method.clear()
            campaign.method.add(*form.cleaned_data['method'])
            campaign.save()

            return redirect('add_campaign_spec', id=campaign.id)

    user_obj = user.objects.get(id=request.session['userid'])
    internal = 1 if len([d for d in user_obj.child_user.all() if d.usertype_id == 9]) > 0 else 0
    external = 1 if len([d for d in user_obj.child_user.all() if d.usertype_id == 5]) > 0 else 0
    context = {
        'campaign': campaign_obj,
        'campaignform': form,
        'internal':internal,
        'external':external,
        'outreach_method': [d.type for d in campaign_obj.method.all()],
        'startdate_error': startdate_error,
        'enddate_error': enddate_error,
    }
    return render(request, 'campaign/campaign_edit.html', context)

# === Update Campaign mandatory Fields ===
@is_users_campaign
def update_c_mandatory_fields(request, campaign_id):
    # update campaign manadatory fields
    print("hello")
    lead_validation_list=[]
    campaign = Campaign.objects.get(id=campaign_id)
    form = CampaignForm(request.POST, instance=campaign)
    # print(form)
    if form.is_valid():
        form.save()
        #store all default lead validation specs
        # campaign = Campaign.objects.latest("id")
        # campaign_id = campaign.id
        #store default lead validation in database
        if SelectedLeadValidation.objects.filter(campaign=campaign).count() == 0:
            lead_validation =LeadValidationComponents.objects.filter(is_default='True')
            # ////////
            for lead in lead_validation:
                lead_validation_list.append(lead.function_name)

            SelectedLeadValidation.objects.create(campaign=campaign,component_list=lead_validation_list)


        if campaign.rfq == 1:
            #to save manually campaign data after clone --suryakant
            mapping=Mapping.objects.get(campaign_id=campaign.id)
            mapping.country = ','.join(request.POST.getlist('geo'))
            mapping.company_size = ','.join(request.POST.getlist('company_size'))
            mapping.revenue_size = ','.join(request.POST.getlist('revenue_size'))
            mapping.custom_question = int(request.POST.get('custom_question',0))
            mapping.industry_type=','.join(request.POST.getlist(u'industry_type'))
            mapping.special_instructions=request.POST.get('special_instructions')
            mapping.job_title_function=request.POST.get('job_title_function')
            mapping.save()

            if 'abm_status' not in request.POST:
                abm_status=0
                # normal call for remove abm file not ajax - Amey
                removefile(None, campaign_id, 'abm')
            else:
                abm_status=1

            if 'suppression_status' not in request.POST:
                suppression_status=0
                # normal call for remove supp file not ajax - amey
                removefile(None, campaign_id, 'supp')
            else:
                suppression_status=1

            specification=Specification.objects.get(campaign_id=campaign_id)
            specification.abm_status=abm_status
            specification.suppression_status=suppression_status
            specification.save()

            if request.FILES:
                for filename, file in request.FILES.items():
                    name = filename
                    myfile = request.FILES[filename]
                    fs = FileSystemStorage()
                    filename = fs.save("campaign/" + myfile.name,  myfile)
                    upload_file_specs(campaign, filename, name)

            # go to add camp specs if camp editing - amey 28 june
            cloned = int(request.POST.get('cloned'))
            if cloned == 1:
                return redirect('add_campaign_spec', id=campaign_id)

            message = "Campaign Created Successfully"
            # add notification
            title = 'New RFQ Campaign Created'
            desc = "Campaign named '" + str(campaign.name) + "' created successfully"
            sender_id = request.session['userid']
            receiver_id = request.session['userid']
            superadmins = [i.id for i in user.objects.filter(usertype__id=4)]
            superadmins.append(receiver_id)
            for superad in superadmins:
                RegisterNotification(sender_id, superad, desc, title, 1, campaign, None)

            #send request to vendors for rfq campaign
            rfq_vendor_allocation(campaign_id,sender_id)
            return redirect('client_rfq_campaign')

        else:
            cloned = int(request.POST.get('cloned'))
            if cloned == 1:
                return redirect('add_campaign_spec', id=campaign_id)

            title = 'New Campaign Created'
            desc = "Campaign named '" + str(campaign.name) + "' created successfully"
            sender_id = request.session['userid']
            receiver_id = request.session['userid']
            superadmins = [i.id for i in user.objects.filter(usertype__id=4)]
            superadmins.append(receiver_id)
            for superad in superadmins:
                RegisterNotification(sender_id, superad, desc, title, 1, campaign, None)

            return redirect('add_campaign_spec', id=campaign_id)
    else:
        # return redirect('clonecampaign', id=campaign_id)
        print(form.errors)
        if campaign.rfq == 1:
            return redirect(reverse('edit_rfq_campaign', kwargs={'campaign_id': campaign_id}))
        else:
            return redirect(reverse('edit_campaign', kwargs={'campaign_id': campaign_id}))

# === Pause  Campaign ===
def pause_campaign(request,campaign_id):
    """ pause campaigns and their allocations
        alloc.reasons = 1 (paused by client)
        alloc.reasons = 2 (paused by vendor)
    """
    campaign = Campaign.objects.get(id=campaign_id)
    campaign.status = 2
    campaign.save()
    for alloc in campaign_allocation.objects.filter(campaign_id=campaign.id):
        alloc.status = 2
        alloc.reasons = {'flag':1 ,'reason':'Paused by client'}
        alloc.save()
        desc = f"'{campaign.name}' campaign is paused by client."
        RegisterNotification(campaign.user.id, alloc.client_vendor.id, desc,'Campaign Paused', 1, campaign, None)
    return redirect(reverse('client_paused_campagin'))

# === Resume Campaign ===
def resume_campaign(request,campaign_id):
    """ resume campaigns and their allocations """
    campaign = Campaign.objects.get(id=campaign_id)
    campaign.status = 1
    campaign.save()
    for alloc in campaign_allocation.objects.filter(campaign_id=campaign.id):
        alloc.status = 1
        alloc.reasons = None
        alloc.save()
        desc = f"'{campaign.name}' campaign is resumed by client."
        RegisterNotification(campaign.user.id, alloc.client_vendor.id, desc,'Campaign Resumed', 1, campaign, None)
    return redirect(reverse('client_live_campagin'))

@login_required
@is_client
def client_activity(request):
    # client activity display
    return render(request, 'campaign/client_activity.html', {})

# === Vendor Comparison ===
@login_required
@is_client
def vendor_comparison(request):
    # compair venodrs
    return render(request, 'campaign/vendor-comparison.html', {})

# === Leads ===
@login_required
@is_client
def leads(request):
    context = {
        'days': [1, 2, 3],
    }
    # return render(request,'campaign/demo.html', context)
    return render(request, 'campaign/leads.html', {})

# === Campaign Report ===
@login_required
@is_client
def campaignreports(request):
    # generating campaign reports
    return render(request, 'campaign/campaignreports.html', {})

# === Schedule Reports ===
@login_required
@is_client
def schedulereports(request):
    # set scheduled reports
    return render(request, 'campaign/schedulereports.html', {})

# === Account ===
@login_required
@is_client
def account(request):
    # Get clients TC account details
    userid = request.session['userid']
    countrow = client_vendor.objects.filter(user_id=userid).count()
    if countrow == 1:
        users = user.objects.filter(id=userid)
        client = [item for item in client_vendor.objects.filter(
            user_id__in=users)][0]
        country = countries_list1.objects.filter(id=client.user.country)
        # country = countries.objects.filter(id=client.user.country)
        city = cities.objects.filter(id=client.user.city)
        state = states1.objects.filter(id=client.user.state)
        # state = states.objects.filter(id=client.user.state)
        return render(request, 'campaign/account.html', {'client': client,
                                                         'country': country[0] if country else '',
                                                         'city': city[0] if city else '',
                                                         'state': state[0] if state else ''})
    else:
        return render(request, 'campaign/account.html', {})

# === Raise Ticket ===
@login_required
@is_client
def raise_ticket(request):
    # raise tickets for issue
    tc_cat = Ticket_Category.objects.all()
    tickets = Raised_Tickets.objects.all()
    return render(request, 'campaign/raise_ticket.html', {'tc_cat':tc_cat,'tickets':tickets})

# === Contact US ===
@login_required
@is_client
def contactus(request):
    # return contact us page
    return render(request, 'campaign/contactus.html', {})

# === TC Faq page ===
@login_required
@is_client
def faq(request):
    # returs TC faq page
    return render(request, 'campaign/faq.html', {})

# === TC Terms and Conditions ===
@login_required
@is_client
def terms(request):
    # returs TC terms and conditions
    return render(request, 'campaign/terms.html', {})

# === Campaign Descriptions ===
@is_client
@is_users_campaign
def campaingdesc(request, camp_id):
    # campiaign descriptions
    camp = Campaign.objects.get(id=camp_id)
    vendoralloc = campaign_allocation.objects.filter(campaign_id=camp_id)
    return render(request, 'campaign/campdescription.html', {'camp': camp, 'vendoralloc': vendoralloc})

# === Vendor Profile ===
def vendorprofile(request, vendor_id):
    # return selected vendor profille
    vendor = client_vendor.objects.filter(id=vendor_id)
    return render(request, 'vendor1/vendor_profile.html', {'vendor': vendor})

# === View Campaign Deatils ===
@is_users_campaign
def view_campaign_details(request, campaign_id):
    # campaign details
    campaign = Campaign.objects.get(id=campaign_id)
    specification = Specification.objects.get(campaign=campaign)
    mapping = Mapping.objects.get(campaign=campaign)
    terms = Terms.objects.get(campaign=campaign)
    delivery = Delivery.objects.get(campaign=campaign)
    campaignform = CampaignForm()
    specificationform = SpecificationForm()
    mappingform = MappingForm()
    termsform = TermsForm()
    deliveryform = DeliveryForm()

    # data filler
    campaign_category = Campaign_category.objects.all()
    campaign_pacing = Campaign_pacing.objects.all()
    days = Days.objects.all()
    industry_types = industry_type.objects.all()
    job_levels = job_level.objects.all()
    job_functions = job_function.objects.all()
    countries_list = countries_list1.objects.all()
    # countries_list = countries.objects.all()
    company_sizes = company_size.objects.all()
    source_touch = source_touches.objects.all()
    level_intents = level_intent.objects.all()
    assets_all = assets.objects.all()

    data_headers = data_header.objects.all()
    delivery_methods = delivery_method.objects.all()

    context = {
        'campaign': campaign,
        'campaignform': campaignform,
        'specification': specification,
        'specificationform': specificationform,
        "mappingform": mappingform,
        "mapping": mapping,
        "termsform": termsform,
        "terms": terms,
        "deliveryform": deliveryform,
        "delivery": delivery,
        # data filler
        # set up data
        "campaign_category": campaign_category,
        "campaign_pacing": campaign_pacing,
        "days": days,
        "industry_types": industry_types,
        "job_levels": job_levels,
        "job_functions": job_functions,
        "company_sizes": company_sizes,
        "countries": countries_list,
        "source_touches": source_touch,
        "level_intent": level_intents,
        "assets": assets_all,

        "data_headers": data_headers,
        "delivery_methods": delivery_methods,
    }
    return render(request, 'campaign/view_campaign_details.html', context)

# === Conditional Query Filter ===
def conditional_query_filter(filter_data, key):
# returns query to filter given data...Akshay G.
    if filter_data != '' and filter_data != []:
        if filter_data == 'Select Campaign Type':
            return Q()
        if key == 'name':
            return Q(name__icontains = filter_data) | Q(description__icontains = filter_data)
        elif key == 'date':
            start_date = filter_data.split(',')[0].strip()
            end_date = filter_data.split(',')[1].strip()
            return Q(start_date__gte = start_date) & Q(start_date__lte = end_date)
        elif key == 'geo':
            return Q(geo__icontains = filter_data)
        elif key == 'camp_type':
            for k, v in CAMPAIGN_TYPES_CHOICES:
                if k == '':
                    continue
                if filter_data.lower() in v.lower():
                    return Q(type = k)
            return Q(type = '-1')
        elif key == 'outreach_method':
            return Q(method__type__in = filter_data)
        elif key == 'abm':
            if filter_data.lower() in 'yes':
                return Q(specification__abm_status = 1)
            elif filter_data.lower() in 'no':
                return Q(specification__abm_status = 0)
            else:
                return Q(specification__abm_status = -1)
        elif key == 'suppression':
            if filter_data.lower() in 'yes':
                return Q(specification__suppression_status = 1)
            elif filter_data.lower() in 'no':
                return Q(specification__suppression_status = 0)
            else:
                return Q(specification__suppression_status = -1)
        elif key == 'question':
            if filter_data.lower() in 'yes':
                return Q(mapping__custom_status = 1)
            elif filter_data.lower() in 'no':
                return Q(mapping__custom_status = 0)
            else:
                return Q(mapping__custom_status = -1)
        elif key == 'country':
            from functools import reduce
            return reduce(operator.or_, (Q(mapping__country__icontains = item) for item in filter_data))
    else:
        return Q()


# === Client Manage Campaign ===
@login_required
@is_client
def client_manage_campaign(request, filter_camp = '', filter_camp_date = '', filter_geo = '', filter_camp_type = '',filter_outreach_method='', filter_abm='',filter_suppression="",filter_custom_question='',filter_country=''):
    # return all campaigns of current client on camapign notebook
    counter =[]
    if request.session['usertype'] == 6: 
        if Campaign_access.objects.filter(user_id=request.session['ext_userid']).exists():
            camp_access_obj = Campaign_access.objects.get(user_id=request.session['ext_userid'])
            counter = camp_access_obj.campaign.all()
    else:
        counter = Campaign.objects.filter(user_id=request.session['userid'])
    
    data = []
    
    if len(counter) > 0:
        campaign_details = counter.filter(
            conditional_query_filter(filter_camp, 'name'),
            conditional_query_filter(filter_camp_date, 'date'),
            conditional_query_filter(filter_geo, 'geo'),
            conditional_query_filter(filter_camp_type, 'camp_type'),
            conditional_query_filter(filter_outreach_method, 'outreach_method'),
            conditional_query_filter(filter_suppression, 'suppression'),
            conditional_query_filter(filter_abm, 'abm'),
            conditional_query_filter(filter_custom_question, 'question'),
            conditional_query_filter(filter_country, 'country'),
            user_id=request.session['userid']).order_by('-updated_date', '-updated_time','name','priority')
        return campaign_details.distinct()
    else:
        return data
        # return render(request, 'campaign/managecampaign.html', {})

# === Client Live Camapaign ===
@login_required
@is_client
def client_live_campagin(request):
    # return all live campaigns of current client on camapign notebook
    # get data to be filtered for campaigns..Akshay G.
    searched_data, filter_data = get_filter_data(request)
    data = client_manage_campaign(request, *filter_data)
    camp_status = {}
    agreements_data = {}
    script_data = {}
    for campaign in data:
        if campaign.status == 1:
            d = assigned_camp_status(campaign)
            camp_status.update(d)
            agreements_data[campaign.id] = vendor_agreements_status(campaign.id)
            script_data[campaign.id] = vendor_script_status(campaign.id, request)
    lead_upload = campaign_allocation.objects.filter(client_vendor__id=request.session['userid']).values_list('campaign', flat=True)
    # campaign_details = Campaign.objects.filter(id=camp_id)
    # vendor_list = get_vendor_list_of_campaign(camp_id)
    return render(request, 'campaign/individual_campaign_notebook.html', {'camps': data,'lead_upload':list(lead_upload), **searched_data, 'camp_status':camp_status, 'agreements_data': agreements_data, 'script_data': script_data})

# === Client Paused Campagin ===
@login_required
@is_client
def client_paused_campagin(request):
    # return all pause campaigns of current client on camapign notebook
    data = client_manage_campaign(request)
    return render(request, 'campaign/client_pause_campaign.html', {'camps': data})

# === Client Completed Campagin ===
@login_required
@is_client
def client_completed_campagin(request):
    # return all completed campaigns of current client on camapign notebook
    # get data to be filtered for campaigns..Akshay G.
    searched_data, filter_data = get_filter_data(request)
    data = client_manage_campaign(request, *filter_data)
    return render(request, 'campaign/client_complete_campaign.html', {'camps': data, **searched_data})

# === select campaign type ===
def return_status(element):
# Akshay G.
    if element == 'yes':
        return 1
    elif element == 'no':
        return 0
    return ''

# === select campaign type ===
@login_required
@is_client
def client_pending_campaign(request):
    # return all pending campaigns of current client on camapign notebook
    # fetch data to be filtered for campaigns ..Akshay G.
    filter_camp = request.POST.get('search_campaign','')
    filter_geo = request.POST.get('search_geo','')
    filter_camp_type = request.POST.get('campaign_type','')
    filter_camp_date = request.POST.get('search_date', '').strip()
    filter_abm = request.POST.get('abm_status','')
    filter_suppression = request.POST.get('suppression_status','')
    filter_custom_question = request.POST.get('custom_question_status','')
    filter_outreach_method = request.POST.getlist('outreach_methods','')
    filter_country = request.POST.getlist('country_list','')
    data = client_manage_campaign(request, filter_camp, filter_camp_date, filter_geo,
        filter_camp_type, filter_outreach_method, filter_abm, filter_suppression,
        filter_custom_question, filter_country)
    outreach_method = source_touches.objects.all()
    campaign_type_list = list((v) for k, v in CAMPAIGN_TYPES_CHOICES)
    filter_abm = return_status(filter_abm)
    filter_suppression = return_status(filter_suppression)
    filter_custom_question = return_status(filter_custom_question)
    countries = countries1.objects.all()
    # tc header
    data_headers = data_header.objects.all()

    camp_status = {}
    for campaign in data:
        if campaign.status == 3:
            d = pending_camp_status(campaign)
            if all(d[campaign.id].values()):
                campaign.status = 5
                campaign.save()
            camp_status.update(d)
    context = {
        'data_headers': data_headers,
        'camps': data,
        'filter_camp':filter_camp,
        'filter_camp_date':filter_camp_date,
        'filter_camp_type': filter_camp_type,
        'filter_geo':filter_geo,
        'campaign_types': campaign_type_list,
        'selected_outreach_method': filter_outreach_method,
        'abm_status':filter_abm,
        'suppression_status':filter_suppression,
        'custom_question_status': filter_custom_question,
        'selected_country':filter_country,
        'outreach_methods':outreach_method,
        'countries':countries,
        'camp_status':camp_status}
    return render(request, 'campaign/client_pending_campaign.html', context)

# === Client RFQ Campaign ===
@is_client
def client_rfq_campaign(request, archives=0):
    # return all pending campaigns of current client on camapign notebook
    status_list = {}
    if archives == 1:
        data = RFQ_Campaigns.objects.filter(user_id=request.session['userid'], archive_status=True).order_by('-updated_date', '-updated_time')
        count = RFQ_Campaigns.objects.filter(user_id=request.session['userid'], archive_status=False).count()
    else:
        data = RFQ_Campaigns.objects.filter(user_id=request.session['userid'], archive_status=False).order_by('-updated_date', '-updated_time')
        count = RFQ_Campaigns.objects.filter(user_id=request.session['userid'], archive_status=True).count()
    if request.session['usertype'] == 6: 
        if Campaign_access.objects.filter(user_id=request.session['ext_userid']).exists():
            camp_access_obj = Campaign_access.objects.get(user_id=request.session['ext_userid'])
            data = camp_access_obj.rfq_campaign.all().order_by('-updated_date', '-updated_time')
    else:
        pass
    for camp in data:
        if RFQ_campaign_quotes.objects.filter(campaign_id=camp.id).exists():
            wait_for_req = 1
            WFR_count = RFQ_campaign_quotes.objects.filter(campaign_id=camp.id).count()
            RCMP = [ d  for d in RFQ_campaign_quotes.objects.filter(campaign_id=camp.id).values_list('cpl',flat = True) if d != 0 ]
            wait_for_quote = 1 if RCMP != [] else 0
            WFQ_count = len(RCMP)
            status_list[camp.id] = {'wait_for_quote':wait_for_quote,'WFQ_count':WFQ_count,'WFR_count':WFR_count,'wait_for_req':wait_for_req}
        else:
            wait_for_quote = wait_for_req = WFQ_count = WFR_count = 0
            status_list[camp.id] = {'wait_for_quote':wait_for_quote,'WFQ_count':WFQ_count,'WFR_count':WFR_count,'wait_for_req':wait_for_req}
    return render(request, 'campaign/RFQ_campaigns.html', {'camps': data, 'archives': archives, 'count': count,"status_list":status_list})

# === Get Filter Data ===
def get_filter_data(request):
# fetch data to be filtered for campaigns ..Akshay G.
    filter_camp = request.POST.get('search_campaign','')
    filter_geo = request.POST.get('search_geo','')
    filter_camp_type = request.POST.get('campaign_type','')
    filter_camp_date = request.POST.get('search_date', '').strip()
    filter_abm = request.POST.get('abm_status','')
    filter_suppression = request.POST.get('suppression_status','')
    filter_custom_question = request.POST.get('custom_question_status','')
    filter_outreach_method = request.POST.getlist('outreach_methods','')
    filter_country = request.POST.getlist('country_list','')
    data2 = (filter_camp, filter_camp_date, filter_geo,
            filter_camp_type, filter_outreach_method,
            filter_abm, filter_suppression,
            filter_custom_question, filter_country)
    outreach_method = source_touches.objects.all()
    campaign_type_list = list((v) for k, v in CAMPAIGN_TYPES_CHOICES)
    filter_abm = return_status(filter_abm)
    filter_suppression = return_status(filter_suppression)
    filter_custom_question = return_status(filter_custom_question)
    countries = countries1.objects.all()
    # tc header
    data_headers = data_header.objects.all()
    data1 = {'data_headers': data_headers,
        'filter_camp':filter_camp,'filter_camp_date':filter_camp_date,
        'filter_camp_type': filter_camp_type,
        'filter_geo':filter_geo, 'campaign_types': campaign_type_list,
        'selected_outreach_method': filter_outreach_method,
        'abm_status':filter_abm,'suppression_status':filter_suppression,
        'custom_question_status': filter_custom_question, 'selected_country':filter_country,
        'outreach_methods':outreach_method,'countries':countries}  
    return data1, data2

# === Client Draft Campaign ===
@login_required
@is_client
def client_draft_campagin(request):
    # return all draft campaigns of current client on camapign notebook
    is_logged_in = check_is_logged_in(request)
    if(is_logged_in['success']):
        # get data to be filtered for campaigns..Akshay G.
        searched_data, filter_data = get_filter_data(request)
        data = client_manage_campaign(request, *filter_data)
        camp_status = {}
        for campaign in data:
            if campaign.status == 0:
                d = draft_camp_status(campaign)
                if all(d[campaign.id].values()):
                    campaign.status = 3
                    campaign.save()
                camp_status.update(d)
        return render(request, 'campaign/client_draft_campaign.html', {'camps': data, 'camp_status':camp_status, **searched_data})
    else:
        print(is_logged_in)
        return redirect("/logout/")


# === Client Assigned Campagin ===
@login_required
@is_client
def client_assigned_campagin(request):
    # return all assigned campaigns of current client on camapign notebook
    # get data to be filtered for campaigns..Akshay G.
    searched_data, filter_data = get_filter_data(request)
    data = client_manage_campaign(request, *filter_data)
    camp_status = {}
    agreements_data = {}
    script_data = {}
    for campaign in data:
        if campaign.status == 5:
            d = assigned_camp_status(campaign)
            agreements_data[campaign.id] = vendor_agreements_status(campaign.id)
            script_data[campaign.id] = vendor_script_status(campaign.id, request)
            if all(d[campaign.id].values()) and agreements_data[campaign.id]['agreements_status'] and script_data[campaign.id]['status']:
                campaign.status = 1
                campaign.campaign_allocation_set.all().update(status = 1)
                campaign.save()
            camp_status.update(d)
            
    return render(request, 'campaign/client_assigned_campaign.html', {'camps': data, 'camp_status':camp_status, **searched_data, 'agreements_data': agreements_data, 'script_data': script_data})

# === Timeout ===
def timeout(request):
    print('hello timeout')
    data = {
        'msg': 'Hello timeout!',
    }
    return JsonResponse(data)

# === Campaign Life Cycle ===
@is_users_campaign
def campaign_lifecycle(request, camp_id):
    import datetime
    # campaign lifcycle for client - by Amey Raje
    if CampaignTrack.objects.filter(campaign_id=camp_id).exists():
        # campaign track object
        intro = CampaignTrack.objects.get(campaign_id=camp_id)
        list_for_uploaded = []

        # to find all actions done by vendor and client on lead upload
        uploaded = Lead_Uploaded_Error.objects.filter(campaign_id=camp_id, lead_upload_status=0 )
        for upload in uploaded:
            # total_lead = int(upload.duplicate_with_our_cnt) + int(upload.duplicate_with_vendor_cnt) + int(upload.remove_lead_header_cnt) + int(upload.remove_duplicate_lead_csv_cnt)
            total_lead =int(upload.all_lead_count)
            if total_lead == 0:
                total_lead = upload.uploaded_lead_count

            d = {}
            d['type'] = 'upload'
            d['vendor_id'] = upload.user.id
            d['vendor'] = upload.user.user_name
            t = upload.exact_time
            d['date'] = t.isoformat()
            d['lead_row_id'] = upload.id
            d['all_uploaded_leads'] = total_lead
            d['valid_leads'] = upload.uploaded_lead_count
            d['client_percentage'] = upload.client_percentage
            d['vendor_percentage'] = upload.vendor_percentage
            list_for_uploaded.append(d)

        info = list()
        # getting other campaign data as well like assigning vendor, client actions, complete status
        dic = {'a':intro.data_vendor_assign_count, 'b':intro.client_action_count, 'c':intro.complete_status_count}

        for i in dic:
            if dic[i] > 0:
                if i == 'a':
                    info += list(chain(eval(intro.data_vendor_assign)))
                elif i == 'b':
                    info += list(chain(eval(intro.client_action)))
                elif i == 'c':
                    info += list(chain(eval(intro.complete_status)))
        # merging all lists
        info += list(chain(list_for_uploaded))
        # sorting according to date
        newlist = sorted(info, key=itemgetter('date'))

        # target_quantity
        target = int(intro.campaign.target_quantity)
        camp_all = campaign_allocation.objects.filter(campaign_id=camp_id)
        camp = Campaign.objects.get(id=camp_id)
        approve = 0

        # finding all approved leads of vendors
        for i in camp_all:
            if i.approve_leads is not None:
                approve += int(i.approve_leads)

        start = datetime.datetime.strptime(str(camp.start_date), '%Y-%m-%d')
        # start1 = datetime.date(start)
        end = datetime.datetime.strptime(str(camp.end_date), '%Y-%m-%d')
        # end1 = datetime.date(end)
        today = datetime.datetime.now()

        # finding campaign status as it is ahead or behind
        lapsed = (today-start).days
        if lapsed < 0:
            lapsed = 0
        total_days = (end-start).days
        per_day = int(target)/int(total_days)
        if lapsed > total_days:
            till_date_to_be_approved = per_day*total_days
            day_per = 100
            days = int(total_days)
        else:
            till_date_to_be_approved = per_day*lapsed
            day_per = (lapsed/total_days)*100
            days = int(lapsed)

        # percentage of completed campaign
        per = percentage(camp_id)
        if till_date_to_be_approved > int(approve):
            message = 'Warning: Campaign is lagging behind the Schedule.'
            color = 'bg-danger'
        elif till_date_to_be_approved == int(approve):
            message = 'Good! Campaign is on time.'
            color = 'bg-info'

        else:
            message = 'Great! Campaign is ahead of Schedule.'
            color = 'bg-success'

        submitted_leads = 0
        for i in camp_all:
            submitted_leads += int(i.submited_lead)
        # d list of vendor id's for vendor filter
        d = []
        # for i in newlist:
        #     if i['type'] == 'vendor':
        #         # print(i, i['vendor_id'])
        #         d.append({str(i['vendor_id']):i['vendor_id']})
        for dic in newlist:
            for key, value in dic.items():
                if key == 'vendor' or key == 'Name':
                    if {str(dic['vendor_id']) : dic['vendor_id']} not in d:
                        d.append({str(dic['vendor_id']) : dic['vendor_id']})
        # d1 all campaign dates for date filter
        d1 = []
        for i in newlist:
            d1.append({str(i['vendor_id']):str(i['date'])[0:10]})
        user_ = user.objects.get(id = request.session['userid'])
        # 'vendors_dict' for vendor filter
        vendors_dict = {}
        # for i in newlist:
        #     if i['type'] == 'vendor' and i['Name'] not in vendors_dict:
        #         vendors_dict[i['Name']] = i['vendor_id']
        for dic in newlist:
            for key, value in dic.items():
                if key == 'vendor' or key == 'Name':
                    if dic[key] not in vendors_dict:
                        vendors_dict[dic[key]] = dic['vendor_id']
        context = {
            'vendors_dict': vendors_dict,
            'user_id': request.session['userid'],
            'user_name': user_.user_name,
            'intro': intro,
            'vendor_ids':json.dumps(d),
            'campaign_dates':json.dumps(d1),
            'info': newlist,
            'per':per,
            'color':color,
            'message':message,
            'day_per':int(day_per),
            'days':days,
            'total':int(total_days),
            'Total_leads': int(camp.target_quantity),
            'Submitted_Leads': submitted_leads,
            'approved_leads': int(approve),
        }
        return render(request, 'campaign_life_cycle/campaign_lifecycle.html', context)

    else:
        context = {}
        return render(request, 'errors/raise404.html', context)


# === Custom Data Field ===
@is_users_campaign
def custom_datafield(request,camp_id):
    # add custom data headers  """
    import json
    custom_header_temp_data,custom_mapp_data = [],[]
    header=data_header.objects.all().order_by('created_date')
    defaults = Default_custom_header.objects.filter(user = request.session['userid'])
    camp_status = Campaign.objects.get(id=camp_id).status
    if camp_status == 1:
        url = '/client/live-campaign/'  
    elif camp_status == 5:
        url = '/client/assigned-campaign/'
    else:
        url = '/client/pending-campaign/'
    delivery_status= Delivery.objects.get(campaign_id=camp_id)
    if  delivery_status.custom_header_status == 1: # check custom Delivery Header exist or Not
        camp_data=CampaignMappData.objects.get(campaign_id=camp_id)
        if camp_data.temp_mapp_data != None:
            custom_header_temp_data = eval(camp_data.temp_mapp_data)  # if custom delivery headers availabe then diplay on screen either blank screen will display
        custom_mapp_data = eval(delivery_status.custom_header_mapping)
    if defaults.count() > 0:    # if object exists - amey
        context = {
            'header': header,
            'camp_id': camp_id,
            'defaults': defaults[0],
            'array': [d for d in eval(defaults[0].headers).keys()],
            'custom_header_temp_data' : json.dumps(custom_header_temp_data),
            'custom_mapp_data' :json.dumps(custom_mapp_data),
            'back_url':url,
        }
    else:  #if obj not exists- amey
        context = {
            'header':header,
            'camp_id':camp_id,
            'defaults':defaults,
            'custom_header_temp_data' : json.dumps(custom_header_temp_data),
            'custom_mapp_data' :json.dumps(custom_mapp_data),
            'back_url':url,
        }

    return render(request, 'custom_datafields/create_custom_datafields.html',context)


# === Save Custom Header ===
@login_required
@is_client
@csrf_exempt
def save_custom_header(request):
    # This Function Used for store custom header in database.
    from operator import itemgetter
    if request.method == 'POST' :
        data=ast.literal_eval(request.POST['header'])
        camp_id=request.POST['camp_id']
        base_url=settings.BASE_URL
        camp_status = Campaign.objects.get(id=camp_id).status
        # to redirect page according campaign status
        set_tempdata_for_preview(request.POST['temp_data'],eval(request.POST['header']),request.POST['camp_id'],request.session['userid'])
        if camp_status == 1:
            url = base_url+'client/live-campaign/'  
        elif camp_status == 5:
            url = base_url+'client/assigned-campaign/'
        else:
            url = base_url+'client/pending-campaign/'
        newlist = sorted(data, key=itemgetter('sort'))        
        custom_header=[]
        for header in newlist:
            custom_header.append(header['user_header'])
        custom_header=','.join(custom_header)
        if Delivery.objects.filter(campaign_id=camp_id).count() > 0:
            custom=Delivery.objects.get(campaign_id=camp_id)
            custom.custom_header = custom_header
            custom.custom_header_mapping = newlist
            custom.custom_header_status = 1
            custom.tc_header_status = 0
            custom.data_header = ''
            custom.save()
            update_temp_mapp_data(newlist,camp_id)
            update_header_mapping_data(newlist,request.session['userid'])
            # suryakant 16-10-2019
            # raised mail and refactored code
            title = "Delivery Headers"
            desc = f"Delivery headers uploaded on campaign {custom.campaign.name} by client."
            super = [i.id for i in user.objects.filter(usertype_id=4)]
            super += [d.client_vendor.id for d in campaign_allocation.objects.filter(campaign_id=custom.campaign.id)]
            for j in super:
                camp_alloc = None
                if campaign_allocation.objects.filter(campaign_id=custom.campaign.id,client_vendor_id=j).exists():
                    camp_alloc = campaign_allocation.objects.get(campaign_id=custom.campaign.id,client_vendor_id=j)
                RegisterNotification(custom.campaign.user.id, j, desc, title, 2,None,camp_alloc)
            noti_via_mail(super, title, desc, mail_delivery_data_upload)
            return JsonResponse({'success':1,'msg':'Custom Template Created Successfully!...','url':url})
        return JsonResponse({'success':0,'msg':'Campaign Does Not Exist!...'})
    return JsonResponse({'success':0,'msg':'Without Post Method You not Access this Fuctionality.'})

# === Update Temp Mmapp Data ===
def update_temp_mapp_data(newlist,camp_id):
    """ update temp data values after save """
    camp_data=CampaignMappData.objects.get(campaign_id=camp_id)
    temp_mapp_data=ast.literal_eval(camp_data.temp_mapp_data)
    if camp_data.mapp_date:
        manual_mapp_data=ast.literal_eval(camp_data.mapp_date)
    else:
        manual_mapp_data=0
    header,mapp_data,mapp_list=[],{},[]
    for head in temp_mapp_data:
        header.append(head['Headers'])

    for row in newlist:
        if row['user_header'] in header:
            for head in temp_mapp_data:
                if row['user_header'] == head['Headers'] :
                    if 'default_header' in row:
                        if len(row['default_header'].strip()) > 0:
                            temp_dict={
                                row['default_header']:head['Accepted_values']
                            }
                        else:
                            temp_dict={
                                row['user_header']:head['Accepted_values']
                            }
                    else:
                        temp_dict={
                                row['user_header']:head['Accepted_values']
                            }
                    mapp_data.update(temp_dict)
                    temp_dict={}
                    break
        else:
            temp_dict={
                        row['default_header']:[]
                    }
            mapp_data.update(temp_dict)
            temp_dict={}


    tc_validate=SystemDefaultFields.objects.all()[0]
    tc_validate=ast.literal_eval(tc_validate.tc_default_validate_header)
    for row in tc_validate:
        if row not in mapp_data:
            temp_dict={
                        row:[]
                    }
            mapp_data.update(temp_dict)
            temp_dict={}

    if manual_mapp_data != 0:
        for row in manual_mapp_data[0]:
            if row in mapp_data:
                if row in 'Employee_size' :
                    manual_mapp_data[0][row] = list(filter(None, manual_mapp_data[0][row]))
                    mapp_data[row] = list(filter(None, mapp_data[row]))
                    emp_list=list(map(int, mapp_data[row])) + list(map(int, manual_mapp_data[0][row]))
                    emp_list1=[]
                    if len(list(set(emp_list))) > 1:
                        emp_list1.append(min(list(set(emp_list))))
                        emp_list1.append(max(list(set(emp_list))))
                    else:
                        try:
                            emp_list1.append(0)
                            emp_list1.append(max(list(set(emp_list)))) 
                        except:
                            emp_list1.append(0)
                    mapp_data[row] = emp_list1      
                elif row == 'Revenue':
                    manual_mapp_data[0][row] = list(filter(None, manual_mapp_data[0][row]))
                    mapp_data[row] = list(filter(None, mapp_data[row]))
                    rev= list(map(int,manual_mapp_data[0][row]))+calculate_data(mapp_data[row])
                    rev_list=[]
                    if len(list(set(rev))) > 1:
                        rev_list.append(min(list(set(rev))))
                        rev_list.append(max(list(set(rev))))
                    else:
                        try:
                            rev_list.append(0)
                            rev_list.append(max(list(set(rev))))
                        except:
                            rev_list.append(0)
                                
                    mapp_data[row] = rev_list  
                else:
                    manual_mapp_data[0][row] = list(filter(None, manual_mapp_data[0][row]))
                    mapp_data[row] = list(filter(None, mapp_data[row]))
                    if isinstance(mapp_data[row], list) and len(mapp_data[row]) > 0:
                        mapp_data[row]=list(set(manual_mapp_data[0][row]+mapp_data[row]))
                    else:
                        mapp_data[row]=manual_mapp_data[0][row]

    else:
        for row in mapp_data:
            mapp_data[row] = list(filter(None, mapp_data[row]))
            if row in 'Employee_size' :
                mapp_data[row]=list(map(int, mapp_data[row]))
                
            elif row in 'Revenue':
                mapp_data[row]=calculate_data(mapp_data[row])
                

    mapp_list.append(mapp_data)
    print(mapp_list)
    camp_data.mapp_date=mapp_list
    camp_data.save()
    return True

# === Calculate Data ===
def calculate_data(sizes_range):
    '''# sizes_range = list(map(int, sizes_range)) '''
    if len(sizes_range) > 0 :
        company_revenue_size=[]
        sizes_range=[s.replace(' ', '') for s in sizes_range]
        sizes_range=[s.replace('million', '000000') for s in sizes_range]
        sizes_range=[s.replace('mill', '000000') for s in sizes_range]
        sizes_range=[s.replace('m', '000000') for s in sizes_range]
        sizes_range=[s.replace('$', '') for s in sizes_range]
        sizes_range=[s.replace('billion','000000000')for s in sizes_range]
        sizes_range=[s.replace('bill','000000000')for s in sizes_range]
        sizes_range=[s.replace('b','000000000')for s in sizes_range]
        if len(sizes_range) == 1 and '10000000000+' in sizes_range:
            sizes_range=[s.replace('+','00000')for s in sizes_range]
            sizes_range.append(1000000000)
        else:
            sizes_range=[s.replace('+','00000')for s in sizes_range]
        sizes_range = list(map(int, sizes_range))
        if len(company_revenue_size) > 1 :
            company_revenue_size.append(min(sizes_range))
            company_revenue_size.append(max(sizes_range))
        else:
            company_revenue_size.append(0)
            company_revenue_size.append(max(sizes_range))
        return company_revenue_size
    else:
        return []

# === Get Custom Header CSV ===
@is_client
def get_custom_header_csv(request):
    import pandas
    import re
    header=[]

    if 'file' in request.FILES:
        filehandle = request.FILES['file']
        if filehandle.name.endswith('.csv') or filehandle.name.endswith('.xlsx'):
            if filehandle.name.endswith('.csv'):
                data = pandas.read_csv(filehandle,skip_blank_lines=True,na_filter=False,encoding ='latin1')
                data = data.dropna()
            else:
                xl = pandas.ExcelFile(filehandle)
                data = pandas.read_excel(xl,xl.sheet_names[0],dtype=str).fillna('')
                data = data.dropna()
            for row in data: #get Template header
                if row.startswith('Unnamed:') == False:
                    header.append(row)
            data_mapp = []
            flag=0
            header_wo = [i.replace(' ', '_') for i in header]
            header_wo = [re.sub('[^a-zA-Z0-9\_\n\.]', '', j) for j in header_wo]
            final = []
            for k in header_wo:
                if not re.match('^Q[0-9]', k):
                    final.append(k)
            for row in final:
                mapp_dict={
                            'Headers':row,
                            'Display_values':'',
                            'Accepted_values':'',
                            }
                data_mapp.append(mapp_dict)
                mapp_dict={}
                
    temp_map_data = save_delivery_temp_data(data_mapp,request.POST['camp_id'],request.session['userid'])
    header_wo = [i.replace(' ', '_') for i in header]
    header_wo = [re.sub('[^a-zA-Z0-9\_\n\.]', '', j) for j in header_wo]
    final = []
    for k in header_wo:
        if not re.match('^Q[0-9]', k):
            final.append(k)
    mapped = []
    Tc_headers = data_header.objects.all().order_by('created_date')
    if ClientCustomFields.objects.filter(client_id=request.session['userid']).exists():
        client_headers = ClientCustomFields.objects.get(client_id=request.session['userid'])
    else:
        client_headers = ClientCustomFields.objects.get_or_create(client_id=request.session['userid'])[0]
    for cindex, CH in enumerate(final):
        if CH in collect_client_headers(client_headers.custom_header) :
            for CHM in eval(client_headers.custom_header):
                if CH in CHM['user_header'] and CHM['default_header'] not in [d['default_header'] for d in mapped if 'default_header' in d.keys()] :
                    tindex = [d.type for d in Tc_headers].index(CHM['default_header'])
                    mapped.append({'user_header_index':cindex,'default_header_index':tindex,'user_header':CH,'default_header':CHM['default_header'],'sort':cindex})
                    break
            else:
                mapped.append({'user_header':CH,'sort':cindex})
        else:
            for tindex, TH in enumerate(Tc_headers):
                if TH.custom_headers != None and len(TH.custom_headers) > 0:
                    if TH.type == CH or CH in eval(TH.custom_headers):
                        if mapped == []:
                            mapped.append({'user_header_index':cindex,'default_header_index':tindex,'user_header':CH,'default_header':TH.type,'sort':cindex})
                            break
                        else:
                            if TH.type not in [d['default_header'] for d in mapped if 'default_header' in d.keys()]:
                                mapped.append({'user_header_index':cindex,'default_header_index':tindex,'user_header':CH,'default_header':TH.type,'sort':cindex})
                                break
            else:
                mapped.append({'user_header':CH,'sort':cindex})
    # context = {'header': final,'mapped':mapped}
    # html = render_to_response('custom_datafields/custom_header_csv.html', context)
    # return html
    return JsonResponse({'mapped':mapped,'temp_mapp_data':temp_map_data.temp_mapp_data})


# === Save Delivery Temp Data ===
def save_delivery_temp_data(data_mapp,camp_id,client_id):
    if CampaignMappData.objects.filter(campaign_id=camp_id).count() > 0:
        camp_data=CampaignMappData.objects.get(campaign_id=camp_id)
        camp_data.temp_mapp_data=data_mapp
        camp_data.save()
    else:
        data={
            'temp_mapp_data':data_mapp,
            'client_id':client_id,
            'campaign_id':camp_id
        }
        camp_data=CampaignMappData(**data)
        camp_data.save()
    return camp_data


# === Get Default Template Data ===
@csrf_exempt
def get_default_template_data(request):
    # get default template data
    data = Default_custom_header.objects.get(user_id=request.session['userid'])
    headers =  eval(data.headers)[request.POST.get('temp_name')]
    headers_temp_data =  eval(data.headers_temp_data)[request.POST.get('temp_name')]
    save_delivery_temp_data(headers_temp_data,request.POST.get('camp_id'),request.session['userid'])
    return JsonResponse({'mapped':headers,'temp_mapp_data':headers_temp_data})


# === Add New Custom Header ===
def add_new_custome_header(request):
    # add and map new cutsom header
    header = request.POST.get('header')
    cindex = int(request.POST.get('new_index'))
    mapped = eval(request.POST.get('prev_mapping'))
    Tc_headers = data_header.objects.all().order_by('created_date')
    client_headers = ClientCustomFields.objects.get(client_id=request.session['userid'])
    if header:
        if header not in [d['user_header'] for d in mapped if 'user_header' in d.keys()]:
            if header in collect_client_headers(client_headers.custom_header) :
                for CHM in eval(client_headers.custom_header):
                    if header in CHM['user_header'] and CHM['default_header'] not in [d['default_header'] for d in mapped if 'default_header' in d.keys()] :
                        tindex = [d.type for d in Tc_headers].index(CHM['default_header'])
                        mapped.append({'user_header_index':cindex,'default_header_index':tindex,'user_header':header,'default_header':CHM['default_header'],'sort':cindex})
                        break
                else:
                    mapped.append({'user_header':header,'sort':cindex})
            else:
                for tindex, TH in enumerate(Tc_headers):
                    if TH.custom_headers != None and len(TH.custom_headers) > 0:
                        if TH.type == header or header in eval(TH.custom_headers) :
                            if mapped == []:
                                mapped.append({'user_header_index':cindex,'default_header_index':tindex,'user_header':header,'default_header':TH.type,'sort':cindex})
                                break
                            else:
                                if TH.type not in [d['default_header'] for d in mapped if 'default_header' in d.keys()]:
                                    mapped.append({'user_header_index':cindex,'default_header_index':tindex,'user_header':header,'default_header':TH.type,'sort':cindex})
                                    break
                else:
                    mapped.append({'user_header':header,'sort':cindex})
        else:
            return JsonResponse({'status':2,'msg':'header already mapped'})
    mapped=sorted(mapped, key=itemgetter('sort'))
    return JsonResponse({'mapped':mapped})


# === Vendors Comparison ===
@login_required
@is_client
def vendors_comparison(request):
    # Show list of all vendors in grid format

    # Arguments:
    #     request {WSGIRequest} -- django.core.handlers.wsgi.WSGIRequest

    # Returns:
    #     [type] -- [description]
    final_vendors = []
    # get filter options
    all_sweet_spots = countries_list1.objects.all()
    # all_sweet_spots = countries.objects.all()
    all_marketing_methods = source_touches.objects.all()
    all_languages = language_supported.objects.all()

    vendors = client_vendor.objects.filter(user__usertype__type="vendor")

    for vendor in vendors:
        if data_assesment.objects.filter(user=vendor.user).exists():
            vendor_more_data = data_assesment.objects.filter(user=vendor.user)[0]

            # """ Process languages """
            str_lang_ids = vendor_more_data.language_supported
            # Convert string representation of list to list
            list_lang_ids = ast.literal_eval(str_lang_ids).keys() if str_lang_ids is not None else []

            if list_lang_ids != []:
                lang_ids = [int(float(i)) for i in list_lang_ids]

                supported_languages = language_supported.objects.filter(
                    id__in=lang_ids
                    ).values_list('type', flat=True)
                if supported_languages.count() > 0:
                    supported_languages_str = ', '.join(list(supported_languages))
                else:
                    supported_languages_str = "--"
            else:
                supported_languages_str = "--"
            # """ Process languages the end """

            # Process lead gen capacity
            str_lead_gen_capacity = vendor_more_data.lead_gen_capacity
            dict_lead_gen_capacity = ast.literal_eval(str_lead_gen_capacity) if str_lead_gen_capacity else {}

            final_lead_gen_capacity = ""
            if dict_lead_gen_capacity != {}:
                for key, value in dict_lead_gen_capacity.items():
                    lead_gen = lead_gen_capacity.objects.filter(id=key).first()
                    if lead_gen:
                        source_touches_type = lead_gen.source_touches.type
                        if source_touches_type:
                            final_lead_gen_capacity += f"{source_touches_type} : {value} \n"
                        else:
                            print(f"source_touches_type is null for {lead_gen}")
            else:
                final_lead_gen_capacity = "--"
            print(final_lead_gen_capacity)
            if vendor_more_data:
                vendor_info = {
                    "vendor_id": vendor.id,
                    "name": vendor.user.user_name,
                    "logo_url":vendor.company_logo.url if vendor.company_logo else None,
                    "sweet_spot": vendor_more_data.sweet_spot,
                    "sweet_spot_text": vendor_more_data.sweet_spot_text,
                    "database_overall_size": vendor_more_data.database_overall_size,
                    "marketing_method": vendor.marketing_method,
                    "language_supported": supported_languages_str,
                    "lead_gen_capacity": final_lead_gen_capacity,

                }
                final_vendors.append(vendor_info)

    context = {
        'final_vendors': final_vendors,
        'all_sweet_spots': all_sweet_spots,
        'all_marketing_methods': all_marketing_methods,
        'all_languages': all_languages,
    }
    return render(request, 'vendor1/vendors_list.html', context)
    # return HttpResponse("Hello")


# === Agreement Sign ===
def sign(request):
    # Agreement Sign
    img = request.POST.get("img")
    # print(img)
    imgdata = img.split(',')
    import datetime
    with open('dumy.txt','w+') as f:
        f.write(imgdata[1])
     # I assume you have a way of picking unique filenames
    image = 'sign1.jpg'
    with open(image, 'wb') as f:
        f.write(base64.decodebytes(open('dumy.txt', 'rb').read()))

    packet = io.BytesIO()
    # create a new PDF with Reportlab
    can = canvas.Canvas(packet, pagesize=letter)
    can.drawImage(image, 500, 80, width=100, height=100)
    can.save()

    datapdf = request.POST.get("pdfdata")
    datafile = request.POST.get("pdfFile")
    datafile3 = request.POST.get("pdfFile3")
    datafile4 = request.POST.get("pdfFile4")
    datafile5 = request.POST.get("pdfFile5")

    user_ = user.objects.get(id=request.session['userid'])
    # if user has data_assesment object, return it; else create it and return object in tuple
    obj_ = data_assesment.objects.get_or_create(user_id = request.session['userid'])
    if type(obj_) == tuple:
        id_ = obj_[0].id
    else:
        id_ = obj_.id
    dataurl = {
        'pdf1': datapdf,
        'pdf2': datafile,
        'pdf3': datafile3,
        'pdf4': datafile4,
        'pdf5': datafile5,
        }
    data_ = {}
    for pdf_ in dataurl:
        datafile = dataurl[pdf_]

        url = datafile.split('/')
        url.remove('')
        url.remove('media')
        data =('/').join(url)


        # urldata = os.path.join(os.path.dirname(os.path.dirname(__file__)),'data')
        urldata = os.path.join(settings.MEDIA_ROOT, data)
        # print(urldata)


        newpdf, pdf = path.split(urldata)

        now = str(datetime.datetime.now())[:19]

        newdata = urldata + now + ".pdf"

        L = newdata.split('/')
        new_pdf_path = '/'

        for i in range(len(L)):
            if i > L.index('media'):
                new_pdf_path += L[i] + '/'

        newpath = shutil.copy(urldata, newdata)
        # print(newpath)
        # url = dataurl.lstrip('/media/')
        # pdfFile = os.path.join(settings.MEDIA_ROOT,os.path.abspath(urldata)) #request.POST.get("pdfdata")
        file = newpath


        # file = '
        # file = document
        # move to the beginning of the StringIO buffer
        packet.seek(0)
        new_pdf = PdfFileReader(packet)
        # read your existing PDF
        existing_pdf = PdfFileReader(open(file, 'rb'))
        c = existing_pdf.getNumPages()

        output = PdfFileWriter()
        # addatermark" (which is the new pdf) on the existing page

        for i in range(c):
            page = existing_pdf.getPage(i)
            if i == c-1:
                page.mergePage(new_pdf.getPage(0))
            output.addPage(page)

        # finally, write "output" to a real file
        outputStream = open(file, "ab")
        output.write(outputStream)
        outputStream.close()


        if pdf_ == 'pdf1':
            user_.data_assesment_set.filter(id = id_).update(nda_aggrement_path = new_pdf_path)
            data_["pdf1"] = new_pdf_path

        elif pdf_ == 'pdf2':
            user_.data_assesment_set.filter(id = id_).update(msa_aggrement_path = new_pdf_path)
            data_["pdf2"] = new_pdf_path

        elif pdf_ == 'pdf3':
            user_.data_assesment_set.filter(id = id_).update(gdpr_aggrement_path = new_pdf_path)
            data_["pdf3"] = new_pdf_path

        elif pdf_ == 'pdf4':
            user_.data_assesment_set.filter(id = id_).update(dpa_aggrement_path = new_pdf_path)
            data_["pdf4"] = new_pdf_path

        elif pdf_ == 'pdf5':
            user_.data_assesment_set.filter(id = id_).update(io_aggrement_path = new_pdf_path)
            data_["pdf5"] = new_pdf_path
            print(5)

    return JsonResponse(data_, safe=False)
    # return render(request, 'dashboard/client_boarding.html',{})

    #return render(request, 'dashboard/client_boarding.html',{})

# === Save Header Choice ===
@csrf_exempt
def save_header_choice(request):
    # saving header choice and data
    '''# print(request.POST)'''
    camp = Delivery.objects.get(campaign_id=request.POST.get('campaign_id'))
    if int(request.POST.get('tc_header')) == 1:
        camp.tc_header_status = 1
        camp.custom_header_status = 0
    else:
        camp.tc_header_status = 0
        camp.data_header = ''
    if int(request.POST.get('custom_header')) == 1:
        camp.tc_header_status = 0
        camp.custom_header_status = 1
    else:
        camp.custom_header_status = 0
        camp.custom_header == ''
        camp.custom_header_mapping = ''
    camp.save()
    data = {'msg':'updated','custom_header':camp.custom_header_status,'tc_header':camp.tc_header_status }
    return JsonResponse(data)

# === Notification Via Mail Page ===
def noti_via_mail_page(request):
    # view for showing types of notifications for client type- by amey raje

    mail = MailNotification.objects.all().order_by('-id')
    access = []
    user_instance = user.objects.get(id=request.session['userid'])
    type = usertype.objects.get(id=request.session['usertype'])
    for m in mail:
        if type in m.usertype.all():
            if user_instance in m.user.all():
                access.append({
                    'id': m.id,
                    'access': m.noti_field,
                    'value': 1,
                    'parent': m.parent,
                })
            else:
                access.append({
                    'id': m.id,
                    'access': m.noti_field,
                    'value': 0,
                    'parent': m.parent,
                })
    print(access)
    return render(request, 'campaign/notisettings.html', {'mail': access})

# === Save Mail Choice ===
@csrf_exempt
def save_mail_choice(request):
    # view for saving user notification preferences - by amey raje
    match = eval(request.POST.get('data'))

    for i in match:
        mail = MailNotification.objects.get(id=i['id'])
        if i['check'] == 1:
            mail.user.add(user.objects.get(id=i['user']))
            mail.save()
        elif i['check'] == 0:
            mail.user.remove(user.objects.get(id=i['user']))
            mail.save()
    data = {'success': 1}
    return JsonResponse(data)

# === Check IO ===
@csrf_exempt
def check_io(request):
    # io number check
    if Campaign.objects.filter(io_number=request.GET.get('ionumber')).count() > 0:
        data = {'status': 2}
        return JsonResponse(data)
    else:
        data = {'status': 1}
        return JsonResponse(data)

# === New All Campaign ===
def new_all_campaigns(request):
    # view for list of all campaigns - by amey raje
    import datetime
    result = []
    # all campaign not draft campaigns
    all_camp = Campaign.objects.filter(user_id=request.session['userid'], status__in=[1,2,3,4,5]).order_by('end_date')
    for i in all_camp:
        target = 0
        # for rfq with no target quantity-amey changes 12 june
        if i.target_quantity is not None:
            target = int(i.target_quantity)
        approve = 0
        camp_alloc = campaign_allocation.objects.filter(campaign_id=i.id)

        for j in camp_alloc:
            if j.approve_leads is not None:
                approve += int(j.approve_leads)
        # check if start and end date is of 'datetime.date' or string..Akshay G.
        # convert 'str' date into 'datetime.datetime' object & 'datetime.date' into 'datetime.datetime'
        if isinstance(i.start_date, datetime.date):
            start_ = i.start_date.strftime('%Y-%m-%dT%H:%M:%S.%f')
            start = datetime.datetime.strptime(start_, '%Y-%m-%dT%H:%M:%S.%f')
        else:
            start_ = datetime.datetime.strptime(i.start_date, '%Y-%m-%d').strftime('%Y-%m-%dT%H:%M:%S.%f')
            start = datetime.datetime.strptime(start_, '%Y-%m-%dT%H:%M:%S.%f')
        if isinstance(i.end_date, datetime.date):
            end_ = i.end_date.strftime('%Y-%m-%dT%H:%M:%S.%f')
            end = datetime.datetime.strptime(end_, '%Y-%m-%dT%H:%M:%S.%f')
        else:
            end = datetime.datetime.strptime(i.end_date, '%Y-%m-%d').strftime('%Y-%m-%dT%H:%M:%S.%f')
            end = datetime.datetime.strptime(end_, '%Y-%m-%dT%H:%M:%S.%f')
        today = datetime.datetime.now()

        # for finding whether campaign is ahead or behind
        lapsed = (today-start).days
        if lapsed < 0:
            lapsed = 0

        total_days = (end-start).days
        if target > 0:
            per_day = int(target)/int(total_days)
        else:
            per_day = 0
        if lapsed > total_days:
            till_date_to_be_approved = per_day*total_days
        else:
            till_date_to_be_approved = per_day*lapsed

        # conditions for checking campaign status and sending colors based on progress
        if till_date_to_be_approved > int(approve):
            color = '#ff1a1a'
        elif till_date_to_be_approved == int(approve):
            color = '#3399ff'
        else:
            color = '#2eb82e'

        # finding progress percentage for campaign
        if target > 0:
            progress = percentage(camp_id=i.id)
        else:
            progress = 0
        result.append(
            {
                'id': i.id,
                'name': i.name,
                'leads': target,
                'assigned': i.approveleads,
                'start_date': i.start_date,
                'end_date': i.end_date,
                'progress': progress,
                'color': color,
                'status': i.status,
            }
        )

    return render(request, 'campaign/campaign_notebook_new.html', {'result': result})

# === New Live Campaigns ===
def new_live_campaigns(request):
    # view for showing all live campaigns for client - by amey
    import datetime
    result = []
    # all live campaigns
    all_camp = Campaign.objects.filter(user_id=request.session['userid'], status__in=[1]).order_by('end_date')

    # for creating campaign progress for each campaign
    for i in all_camp:
        target = 0
        # for rfq camp with no target quantity-amey changes 12 jun
        if i.target_quantity is not None:
            target = int(i.target_quantity)
        approve = 0
        camp_alloc = campaign_allocation.objects.filter(campaign_id=i.id)

        for j in camp_alloc:
            if j.approve_leads is not None:
                approve += int(j.approve_leads)

        # for finding camapign status -lagging behind, ahead, ontime etc.
        start = datetime.datetime.strptime(i.start_date, '%Y-%m-%d')
        end = datetime.datetime.strptime(i.end_date, '%Y-%m-%d')
        today = datetime.datetime.now()

        # for finding whether campaign is ahead or behind
        lapsed = (today-start).days
        if lapsed < 0:
            lapsed = 0

        total_days = (end-start).days
        per_day = int(target)/int(total_days)

        if lapsed > total_days:
            till_date_to_be_approved = per_day*total_days
        else:
            till_date_to_be_approved = per_day*lapsed

        # conditions for checking campaign status and sending colors based on progress
        if till_date_to_be_approved > int(approve):
            color = '#ff1a1a'
        elif till_date_to_be_approved == int(approve):
            color = '#3399ff'
        else:
            color = '#2eb82e'
        # finding progress percentage for campaign
        progress = percentage(camp_id=i.id)

        result.append(
            {
                'id': i.id,
                'name': i.name,
                'approved': approve,
                'leads': target,
                'start_date': i.start_date,
                'end_date': i.end_date,
                'progress': progress,
                'color': color,
                'status': i.status,
            }
        )
    return render(request, 'campaign/campaign_notebook_live.html', {'result': result})

# === Custom Types to Specs ===
def custom_types_to_specs(request):
    # Function for creating custom type while adding campaign specifications - by amey
    data = {'success': 0}
    if request.POST:
        # request data
        userid = request.POST.get('id')
        type = request.POST.get('type')
        text = request.POST.get('text')

        # storing data to each model based on type received
        if type == 'industry':
            if industry_type.objects.filter(type=text, client_id=userid).exists() or industry_type.objects.filter(type=text, status=0).exists():
                data = {'success': 0}
                return JsonResponse(data)
            industry_type_obj = industry_type.objects.create(type=text, client_id=userid, status=1)
            industry_type_obj.save()
            new_obj = industry_type.objects.latest('id')

        elif type == 'job_levels':
            if job_level.objects.filter(type=text, client_id=userid).exists() or job_level.objects.filter(type=text, status=0).exists():
                data = {'success': 0}
                return JsonResponse(data)
            job_level_obj = job_level.objects.create(type=text, client_id=userid, status=1)
            job_level_obj.save()
            new_obj = job_level.objects.latest('id')

        elif type == 'job_functions':
            if job_function.objects.filter(type=text, client_id=userid).exists() or job_function.objects.filter(type=text, status=0).exists():
                data = {'success': 0}
                return JsonResponse(data)
            job_function_obj = job_function.objects.create(type=text, client_id=userid, status=1)
            job_function_obj.save()
            new_obj = job_function.objects.latest('id')

        elif type == 'assets':
            if assets.objects.filter(type=text, client_id=userid).exists() or assets.objects.filter(type=text, status=0).exists():
                data = {'success': 0}
                return JsonResponse(data)
            assets_obj = assets.objects.create(type=text, client_id=userid, status=1)
            assets_obj.save()
            new_obj = assets.objects.latest('id')

        data = {'success': 1, 'new_data_id': new_obj.id, 'new_data_name': new_obj.type}
    return JsonResponse(data)

# === Create Report PDF ===
@login_required
def create_report_pdf(request, userid, client_name):
    # Akshay G.
    # client onboarding html into pdf
    # industry = industry_type.objects.all()
    expert = industry_speciality.objects.all()
    client_vendor_detail = client_vendor.objects.get(
            user_id=userid)
    userdetails = user.objects.get(id=userid)
    state = states1.objects.get(id=userdetails.state)
    industry1 = client_vendor_detail.industry_type.all()
    city_ = cities1.objects.filter(id=userdetails.city)
    if city_:
        city = city_[0].name
    else:
        city = None
    zipCode = userdetails.zip_code
    country = countries_list.objects.get(id=userdetails.country).name
    documents = Document.objects.all()
    doc1 = documents[0].nda_mnda_document.url
    doc2 = documents[0].msa_document.url
    doc3 = documents[0].gdpr_document.url
    doc4 = documents[0].dpa_document.url
    doc5 = documents[0].io_document.url
    queryset = userdetails.data_assesment_set.all()
    doc =  {
                'doc1': doc1,
                'doc2': doc2,
                'doc3': doc3,
                'doc4': doc4,
                'doc5': doc5
            }
    for obj in queryset:
        if obj.nda_aggrement == 1:
            pdf1=str(obj.nda_aggrement_path)
            pdf1=pdf1[:-1]
            doc['doc1'] = '/media/'+pdf1

        if obj.msa_aggrement == 1:
            pdf2=str(obj.msa_aggrement_path)
            pdf2=pdf2[:-1]
            doc['doc2'] = '/media/'+pdf2

        if obj.gdpr_aggrement == 1:
            pdf3=str(obj.gdpr_aggrement_path)
            pdf3=pdf3[:-1]
            doc['doc3'] = '/media/'+pdf3

        if obj.dpa_aggrement == 1:
            pdf4=str(obj.dpa_aggrement_path)
            pdf4=pdf4[:-1]
            doc['doc4'] = '/media/'+pdf4

        if obj.io_aggrement == 1:
            pdf5=str(obj.io_aggrement_path)
            pdf5=pdf5[:-1]
            doc['doc5'] = '/media/'+pdf5
    counter = data_assesment.objects.filter(user_id=userid).count()
    if counter == 1:
        data_assesment1 = data_assesment.objects.get(
            user_id=userid)
    else:
        data_assesment1 = {}

    pdfs = []
    # append Agreement pdf's path to 'pdfs'
    for pdf_ in doc:
        datafile = doc[pdf_]
        l = datafile.split('/')
        for i in l:
            if i == '':
                del l[l.index(i)]
        del l[0]
        datafile = '/'.join(l)
        urldata = os.path.join(settings.MEDIA_ROOT, datafile)
        pdfs.append(urldata)
    # get company logo image path
    urldata = os.path.join(settings.MEDIA_ROOT, client_vendor_detail.company_logo.name)
    logo_file = settings.STATICFILES_DIRS[0]
    techconnectr_logo = os.path.join(logo_file,'assets/images/logo/tc.jpg')
    # data to be rendered into html
    data = {'city':city,'zip_code':zipCode,'techconnectr_logo':techconnectr_logo,'company_logo':urldata,'data_assesment': data_assesment1, 'industry1':industry1, 'state': state, 'country': country, 'userdetails': userdetails, 'client_vendor': client_vendor_detail, 'expert': expert}
    template = get_template('dashboard/onboarding_pdf.html')
    html  = template.render(data)
    # create new pdf and write html data into it
    file = open(os.path.expanduser('test_new.pdf'), "w+b")
    pisaStatus = pisa.CreatePDF(html.encode('utf-8'), dest=file,
            encoding='utf-8')
    file.seek(0)
    pdf = file.read()
    file.close()
    pdfs.insert(0,'test_new.pdf')
    pdfs.insert(1,client_vendor_detail.media_kit)
    # try-except block to check, if media kit is there or not
    # "try" executes, if media kit is uploaded
    try:
        merger = PdfFileMerger()
        # merge all pdfs into single pdf
        for pdf in pdfs:
            PDF = pdf
            merger.append(pdf)
        merger.write("onboarding.pdf")
        new_file = open('onboarding.pdf', 'r+b')
        new_file.seek(0)
        pdf = new_file.read()
        return HttpResponse(pdf, 'application/pdf')
    # "except" executes, if media kit is not uploaded
    except:
        # if media kit is not uploaded, remove its path from merging
        pdfs.remove(PDF)
        merger = PdfFileMerger()
        # merge all pdfs into single pdf
        for pdf in pdfs:
            merger.append(pdf)
        merger.write("onboarding.pdf")
        new_file = open('onboarding.pdf', 'r+b')
        new_file.seek(0)
        pdf = new_file.read()
        return HttpResponse(pdf, 'application/pdf')

# === Send Speces Fill Reminder ===
def send_speces_fill_reminder(request):
    # send ampaign specification reminder --suryakant
    from datetime import datetime
    campaigns = Campaign.objects.filter(status=5)
    message_content = []
    for camp in campaigns:
        specification_data = Specification.objects.get(campaign_id=camp)
        delivery_data = Delivery.objects.get(campaign_id=camp)
        mapping_data = Mapping.objects.get(campaign_id=camp)
        temp_dict ={}

        if specification_data:
            temp_dict.update({'abm_status':specification_data.abm_count,'suppression_status':specification_data.suppression_count})

        if delivery_data:
            temp_dict.update({'data_header':len(delivery_data.data_header.split(',')) if delivery_data.data_header else 0})

        if mapping_data:
            temp_dict.update({'industry':len(mapping_data.industry_type.split(',')) if mapping_data.industry_type else 0})
            temp_dict.update({'industry':len(mapping_data.job_level.split(',')) if mapping_data.job_level else 0})
            temp_dict.update({'job_title':len(mapping_data.job_title) if mapping_data.job_title else 0})
            temp_dict.update({'custom_status':0})

        print(temp_dict)
        if datetime.strptime(camp.start_date,"%Y-%m-%d") >= datetime.now():
            diff = datetime.strptime(camp.start_date,"%Y-%m-%d") - datetime.now()
            if diff.days <= 5:
                subject='Campaign Specification Pending'
                from_email = settings.EMAIL_HOST_USER
                to=[camp.user.email]
                html_message = render_to_string('email_templates/campaign_specs_reminder.html', {'data':temp_dict,'username':camp.user.user_name,'campaign':camp})
                plain_message = strip_tags(html_message)
                send_mail(subject,plain_message,from_email,to,fail_silently=True,html_message=html_message)


from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# === All Notifications ===
@login_required
def all_notifications(request):
# show user's all notifications...Akshay G.
    userid = request.session['userid']
    # all notifs details
    ids = Notification_Reciever.objects.filter(
        receiver=userid, status__in = [0,1]).values_list('Notification', flat=True)
    notification_details = Notification.objects.filter(
        id__in=ids).order_by('-created')
    # unread notifs details with status = 0
    ids_ = Notification_Reciever.objects.filter(
        receiver=userid, status = '0').values_list('Notification', flat=True)
    notification_details_ = Notification.objects.filter(
        id__in=ids_).order_by('-created')
    read_notifs_ids = [obj.id for obj in notification_details_]
    # default value for page = 1
    page = request.GET.get('page', 1)
    # Set no of objects to be fetch on each ajax call to 'paginator'..here 3
    paginator = Paginator(notification_details, 3)
    logos = {}
    try:
        notifs_details = paginator.page(page)
        logos = return_company_logo(notifs_details.object_list, userid)
    except PageNotAnInteger:
        notifs_details = paginator.page(1)
        logos = return_company_logo(notifs_details.object_list, userid)
    except EmptyPage:
        notifs_details = paginator.page(paginator.num_pages)
    if len(logos) == 0:
        return redirect('/client/Dashboard/')
    return render(request, 'dashboard/all_notifications.html',{'logo_dict_':logos,'read_notifs_ids':read_notifs_ids,'count':notification_details.count(),'notify_':notifs_details})

# === Return Company Logo ===
def return_company_logo(objects, userid):
    # create dict of company logo ..Akshay G.
    logo_dict = {}
    # current logged in user
    login_user = user.objects.get(id = userid)
    for i in objects:
        # fetch user default logo...Akshay G...Date - 24th Oct, 2019
        from client.utils import fetch_default_logo
        default_logo = fetch_default_logo(i.sender.id)
        user_obj = user.objects.get(id=i.sender.id)
        logo = None
        # for vendor, hide anonymous client's company logo
        if login_user.usertype_id == 2:
            if user_obj.anonymous_status == False and user_obj.usertype_id in [1, 2]:
                client_vendor_detail = client_vendor.objects.filter(user=user_obj)
                if client_vendor_detail:
                    try:
                        logo = client_vendor_detail[0].company_logo
                    except:
                        pass
            elif user_obj.anonymous_status == True and user_obj.usertype_id in [1]:
                default_logo = os.path.join(settings.BASE_URL, 'media', 'default.jpg')
        # check if, notifs is from a client or a vendor
        elif user_obj.usertype_id in [1, 2]:
            client_vendor_detail = client_vendor.objects.filter(user=user_obj)
            if client_vendor_detail:
                try:
                    logo = client_vendor_detail[0].company_logo
                except:
                    pass
        # check if logo file exists in media folder
        import os
        if logo:
            loGo = logo.url.replace('/media/company_logo/','',1)
            if loGo in os.listdir(os.path.join(settings.MEDIA_ROOT, 'company_logo')):
                company_logo = logo
            else:
                company_logo = None
        else:
            company_logo = None
        # set default logo image for superadmin & ext vendor
        if company_logo == None:
            logo_dict[i.id] = default_logo
        else:
            logo_dict[i.id] = company_logo.url
    return logo_dict

# === select campaign type ===
def search_campaign(request):
# filter campaign based on search input & date for client's all campaign notifications...Akshay G.
    # Ajax call
    userid = request.session['userid']
    # all notifs details
    ids = Notification_Reciever.objects.filter(
        receiver=userid, status__in = [0,1]).values_list('Notification', flat=True)
    notification_details = Notification.objects.filter(
        id__in=ids).order_by('-created')
    # unread notifs details
    ids_ = Notification_Reciever.objects.filter(
        receiver=userid, status = '0').values_list('Notification', flat=True)
    notification_details_ = Notification.objects.filter(
        id__in=ids_).order_by('-created')
    read_notifs_ids = [obj.id for obj in notification_details_]
    camp_filter = []
    token = request.POST.get('csrfmiddlewaretoken')
    logos = return_company_logo(notification_details, userid)
    # campaign search input data
    data = request.POST.get('search_campaign', token)
    # selected start date for campaign
    start_date = request.POST.get('start_date','0')
    # selected end date for campaign
    end_date = request.POST.get('end_date','0')
    import datetime
    # convert date strings into datetime objects
    if start_date != '0':
        y1, m1, d1  = start_date.split('-')
        start_date_ = datetime.date(int(y1),int(m1),int(d1))
        y2, m2, d2 = end_date.split('-')
        end_date_ = datetime.date(int(y2),int(m2),int(d2))
    # check if, defalut value is being changed
    # Change in default value of variable means, func has received POST request
    # data == token means user has used only date filter
    if data == token:
        for row in notification_details.order_by('-created'):
            if row.created.date() >= start_date_ and row.created.date() <= end_date_:
                if row.campaign_id:
                    camp_filter.append({row.id: [row.campaign_id.status ,row.title, row.description, 'client', row.created.strftime("%B %d, %Y, %-I:%M%p"),
                        logos[row.id] if row.id in logos else logos['default']]})
                elif row.camp_alloc:
                    camp_filter.append({row.id: [row.camp_alloc.status ,row.title, row.description, 'vendor', row.created.strftime("%B %d, %Y, %-I:%M%p"),
                        logos[row.id] if row.id in logos else logos['default']]})
                else:
                    camp_filter.append({row.id: [None, row.title, row.description, None, row.created.strftime("%B %d, %Y, %-I:%M%p"),
                        logos[row.id] if row.id in logos else logos['default']]})
    # start_date == '0' means user has used only campaign search filter
    elif start_date == '0':
        if data == '':
            for row in notification_details.order_by('-created'):
                if row.campaign_id:
                    camp_filter.append({row.id: [row.campaign_id.status ,row.title, row.description, 'client', row.created.strftime("%B %d, %Y, %-I:%M%p"),
                        logos[row.id] if row.id in logos else logos['default']]})
                elif row.camp_alloc:
                    camp_filter.append({row.id: [row.camp_alloc.status ,row.title, row.description, 'vendor', row.created.strftime("%B %d, %Y, %-I:%M%p"),
                        logos[row.id] if row.id in logos else logos['default']]})
                else:
                    camp_filter.append({row.id: [None, row.title, row.description, None, row.created.strftime("%B %d, %Y, %-I:%M%p"),
                        logos[row.id] if row.id in logos else logos['default']]})
        else:
            for row in notification_details.order_by('-created'):
                if data.lower() in row.title.lower() or data.lower() in row.description.lower():
                    if row.campaign_id:
                        camp_filter.append({row.id: [row.campaign_id.status ,row.title, row.description, 'client', row.created.strftime("%B %d, %Y, %-I:%M%p"),
                            logos[row.id] if row.id in logos else logos['default']]})
                    elif row.camp_alloc:
                        camp_filter.append({row.id: [row.camp_alloc.status ,row.title, row.description, 'vendor', row.created.strftime("%B %d, %Y, %-I:%M%p"),
                            logos[row.id] if row.id in logos else logos['default']]})
                    else:
                        camp_filter.append({row.id: [None, row.title, row.description, None, row.created.strftime("%B %d, %Y, %-I:%M%p"),
                            logos[row.id] if row.id in logos else logos['default']]})
    # executing else, means user has used both search and date filter
    else:
        if data == '':
            for row in notification_details.order_by('-created'):
                if row.created.date() >= start_date_ and row.created.date() <= end_date_:
                    if row.campaign_id:
                        camp_filter.append({row.id: [row.campaign_id.status ,row.title, row.description, 'client', row.created.strftime("%B %d, %Y, %-I:%M%p"),
                            logos[row.id] if row.id in logos else logos['default']]})
                    elif row.camp_alloc:
                        camp_filter.append({row.id: [row.camp_alloc.status ,row.title, row.description, 'vendor', row.created.strftime("%B %d, %Y, %-I:%M%p"),
                            logos[row.id] if row.id in logos else logos['default']]})
                    else:
                        camp_filter.append({row.id: [None, row.title, row.description, None, row.created.strftime("%B %d, %Y, %-I:%M%p"),
                            logos[row.id] if row.id in logos else logos['default']]})
        else:
            for row in notification_details.order_by('-created'):
                if (row.created.date() >= start_date_ and row.created.date() <= end_date_) and (data.lower() in row.title.lower() or data.lower() in row.description.lower()):
                    if row.campaign_id:
                        camp_filter.append({row.id: [row.campaign_id.status ,row.title, row.description, 'client', row.created.strftime("%B %d, %Y, %-I:%M%p"),
                            logos[row.id] if row.id in logos else logos['default']]})
                    elif row.camp_alloc:
                        camp_filter.append({row.id: [row.camp_alloc.status ,row.title, row.description, 'vendor', row.created.strftime("%B %d, %Y, %-I:%M%p"),
                            logos[row.id] if row.id in logos else logos['default']]})
                    else:
                        camp_filter.append({row.id: [None, row.title, row.description, None, row.created.strftime("%B %d, %Y, %-I:%M%p"),
                            logos[row.id] if row.id in logos else logos['default']]})
    return HttpResponse(json.dumps(camp_filter), content_type='json')

# === Update Target Quantity ===
def update_target_quantity(request):
    # This funtion used for update Target quantity

    target_quantity = int(request.POST['target_quantity'])
    camp_id=int(request.POST['camp_id'])
    camp_detail=Campaign.objects.get(id=camp_id)
    client_raimainingleads=0
    approve_leads = camp_detail.client_approveleads + camp_detail.tc_approveleads

    # when all lead approve by vendors(ex,tc) match with new target quantity then campaign go in assign direct.
    if target_quantity == approve_leads:
        camp_detail.target_quantity = target_quantity
        camp_detail.status = 5
    #when target quantity is zero that direct all allocation goes to client
    #techconnectr have lead but client want moere lead that time increase target quantity
    elif camp_detail.target_quantity == 0 :
        camp_detail.client_target_quantity =  target_quantity
        camp_detail.client_raimainingleads =  target_quantity
        camp_detail.target_quantity = camp_detail.target_quantity + target_quantity
    elif camp_detail.tc_target_quantity >= 0 :
        if camp_detail.client_raimainingleads == camp_detail.client_target_quantity:
            camp_detail.client_raimainingleads =  target_quantity
            camp_detail.client_target_quantity = target_quantity
        else:
            camp_detail.client_raimainingleads = camp_detail.client_raimainingleads + target_quantity
            camp_detail.client_target_quantity = camp_detail.client_target_quantity + target_quantity
        camp_detail.target_quantity = camp_detail.target_quantity + target_quantity
    camp_detail.save()
    camp_detail=Campaign.objects.get(id=camp_id)
    data = {
            'success': 1,
            'client_raimainingleads': camp_detail.client_raimainingleads,
            'target_quantity':camp_detail.target_quantity,
            }
    print(data)
    return JsonResponse(data)

# === ABM Check ===
def abm_check(request):
    # ajax call for checking format of abm file - Amey Raje

    data = {'success': 0}
    rows = []
    if "abm_company_file" in request.FILES:
        try:
            filename = request.FILES['abm_company_file'].read()
            rows = filename.splitlines()
            if len(rows) > 1:
                rows = rows[0].decode('utf-8')
                if checkABMTemplate(rows.split(',')) == 1:
                    data = {'success': 1}
                else:
                    data = {'success': 2}
            else:
                data = {'success': 2}
        except Exception as e:
            data = {'success': 2}
    return JsonResponse(data)

# === Supp Check ===
def supp_check(request):
    '''ajax call for checking format of suppression file - Amey Raje'''
    data = {'success': 0}
    rows = []
    if "suppression_company_file" in request.FILES:
        try:
            filename = request.FILES['suppression_company_file'].read()
            rows = filename.splitlines()
            if len(rows) > 1:
                rows = rows[0].decode('utf-8')
                if checkSuppressionTemplate(rows.split(',')) == 1:
                    data = {'success': 1}
                else:
                    data = {'success': 2}
            else:
                data = {'success': 2}
        except Exception as e:
            data = {'success': 2}
    return JsonResponse(data)

# === Upload File Specs ===
def upload_file_specs(id_campaign, filename, field_name):
    # used to upload single file form - Amey Raje

    specification = Specification.objects.get(campaign=id_campaign)
    if field_name == 'abm_company_file':
        if specification.abm_status == 1:
            filename1='media/'+filename
            rows,abm_list=[],[]
            with open(filename1, 'r') as csvfile:
                csvreader = csv.reader(csvfile)
                for row in csvreader:
                    rows.append(row)

            last_id = get_last_id_abm(id_campaign)
            if specification.abm_count > 0:
                abm_list=ast.literal_eval(specification.abm_file_content)
                abm_list+=creat_dict_list(rows[1:],rows[0],last_id,abm_list)
                specification.abm_company_file=filename
                specification.abm_file_content=abm_list
                specification.abm_count=len(abm_list)
            else:
                abm_list = creat_dict_list(rows[1:],rows[0],last_id,abm_list)
                specification.abm_company_file=filename
                specification.abm_file_content=abm_list
                specification.abm_count=len(abm_list)

    elif field_name == 'suppression_company_file':
        if specification.suppression_status == 1:
            filename1='media/'+filename
            rows,supp_list=[],[]
            with open(filename1, 'r') as csvfile:
                csvreader = csv.reader(csvfile)
                for row in csvreader:
                    rows.append(row)

            last_id = get_last_id_suppression(id_campaign)
            if specification.suppression_count > 0:
                supp_list=ast.literal_eval(specification.suppression_file_content)
                supp_list+=creat_dict_list(rows[1:],rows[0],last_id,supp_list)
                specification.suppression_company_file=filename
                specification.suppression_file_content=supp_list
                specification.suppression_count=len(supp_list)
            else:
                supp_list = creat_dict_list(rows[1:],rows[0],last_id,supp_list)
                specification.suppression_company_file=filename
                specification.suppression_file_content=supp_list
                specification.suppression_count=len(supp_list)
    specification.save()

# === Set Default Header ===
def set_default_header(request):
    ''' set headers as defaults for client - amey raje '''
    header, created = Default_custom_header.objects.get_or_create(user_id = request.session['userid'])
    listing = eval(request.POST.get('header'))
    
    temp_data = set_tempdata_for_preview(request.POST['temp_map_data'],eval(request.POST['header']),request.POST['camp_id'],request.session['userid'])
    key = request.POST.get('key')
    final = {}
    if created:
        final.update({key: listing})
        header.headers = final
        header.headers_temp_data = {key:temp_data}
        header.header_count = 1
    else:
        old_data = eval(header.headers)
        old_temp_data = eval(header.headers_temp_data)
        if key not in old_data.keys():
            old_data.update({key: listing})
            old_temp_data.update({key: temp_data})
            header.headers = old_data
            header.headers_temp_data = old_temp_data
            header.header_count = len(old_data)
        else:
            data = {'success': 2}
            return JsonResponse(data)
    header.save()
    data = {'success': 1}
    return JsonResponse(data)


# === Set Tempdata For Preview ===
def set_tempdata_for_preview(temp_data,newlist,camp_id,userid):
    # set temp data according to new headers
    if(len(temp_data) > 0):
        old_temp_data = eval(temp_data)
    else:
        old_temp_data = []
    new_list = newlist
    
    # add new headers to temp_data in its not in it
    headers = [d['user_header'] for d in new_list if 'user_header' in d.keys()]
    for header in headers:
        if header not in [d['Headers'] for d in old_temp_data if 'Headers' in d.keys()]:
            new_header = {'Headers': header, 'Display_values': [''], 'Accepted_values': ['']}
            old_temp_data.append(new_header)

    # del old headers not present in new list
    if (len(old_temp_data) > 0) :
        old_headers = [d['Headers'] for d in old_temp_data if 'Headers' in d.keys()]
        for header in old_headers:
            if header not in headers:
                del_index =old_headers.index(header)
                try:
                    old_temp_data.pop(del_index)
                except:
                    pass
    save_delivery_temp_data(old_temp_data,camp_id,userid)
    return old_temp_data

# === Bulk Upload Suppression ===
def bulk_upload_suppression(request):
# upload suppression file for bulk normal campaigns...Akshay G.
    camp_ids = request.POST.get('camp_id_list')
    csv_file = request.FILES['upload_suppression']
    if csv_file.name.endswith('.csv'):
        try:
            data = pandas.read_csv(csv_file,skip_blank_lines=True,na_filter=False,encoding ='latin1')
            data = data.dropna()
        except:
            return JsonResponse({'status': 'empty'})
    elif csv_file.name.endswith('.xlsx'):
        try:
            xl = pandas.ExcelFile(csv_file)
            data = pandas.read_excel(xl,xl.sheet_names[0],dtype=str).fillna('')
            data = data.dropna()
        except:
            return JsonResponse({'status': 'empty'})
    else:
        return JsonResponse({'status': 'invalid_format'})
    header, csv_data = fetch_csv_data(data)
    if checkSuppressionTemplate(header):
        camp_specs = Specification.objects.filter(campaign_id__in=camp_ids.split(','), suppression_status=1)
        if len(csv_data) > 0:
            if camp_specs:
                camp_specs.update(suppression_file_content = csv_data, suppression_count = len(csv_data))
        else:
            return JsonResponse({'status': 'empty'})
    else:
        return JsonResponse({'status': 'invalid_template'})
    return JsonResponse({'status': 'uploaded'})


# === Bulk Upload ABM ===
def bulk_upload_abm(request):
# upload ABM file for bulk normal campaigns...Akshay G. 1st Aug,2019
    camp_ids = request.POST.get('camp_id_list')
    csv_file = request.FILES['upload_abm']
    if csv_file.name.endswith('.csv'):
        try:
            data = pandas.read_csv(csv_file,skip_blank_lines=True,na_filter=False,encoding ='latin1')
            data = data.dropna()
        except:
            return JsonResponse({'status': 'empty'})
    elif csv_file.name.endswith('.xlsx'):
        try:
            xl = pandas.ExcelFile(csv_file)
            data = pandas.read_excel(xl,xl.sheet_names[0],dtype=str).fillna('')
            data = data.dropna()
        except:
            return JsonResponse({'status': 'empty'})
    else:
        return JsonResponse({'status': 'invalid_format'})
    header, csv_data = fetch_csv_data(data)
    if checkABMTemplate(header):
        camp_specs = Specification.objects.filter(campaign_id__in=camp_ids.split(','), abm_status=1)
        if len(csv_data) > 0:
            if camp_specs:
                camp_specs.update(abm_file_content = csv_data, abm_count = len(csv_data))
        else:
            return JsonResponse({'status': 'empty'})
    else:
        return JsonResponse({'status': 'invalid_template'})
    return JsonResponse({'status': 'uploaded'})

# === Fetch CSV Data ===
def fetch_csv_data(data):
    import datetime
    # fetch csv file data ...Akshay G.
    data = data.dropna()
    # file headers
    header = []
    csv_data = []
    domain_list = ['http://www.','https://www.','www.', 'http://','https://', 'http:', 'https:']
    for row in data:
        if 'Unnamed:' in row:
            continue
        header.append(row)
    for i in range(len(data)):
        temp_data = []
        user_domain = []
        for head in header:
            if data[head][i] != 'non':
                if head.strip() == 'DOMAIN_NAME':
                    user_domain = [s for s in domain_list if data[head][i].lower().startswith(s)]
                    if len(user_domain) > 0:
                        data[head][i] = data[head][i].replace(user_domain[0], '')
                    if '.' not in data[head][i]:
                        data[head][i] = data[head][i] + '.com'
                temp_data.append(data[head][i])
        value = dict(zip(header, temp_data))
        value['timestamp'] = datetime.datetime.now()
        value['user_domain'] = user_domain[0] if len(user_domain) > 0 else ''
        csv_data.append(value)
    return header, csv_data

# === select campaign type ===
def update_dashboard_view(request):
    # updating dashboard view into database
    viewlist = request.POST.get('view_list')

    if viewlist:
        client,created = ClientCustomFields.objects.get_or_create(client_id=request.user.id)
        client.dashboard_view = viewlist
        client.save()
    data = {'status':1,'view':viewlist}
    return JsonResponse(data)

# === Set Dashboard ===
def set_dashboard(request):
    # return dashboard data
    viewlist = []
    client,created = ClientCustomFields.objects.get_or_create(client_id=request.user.id)
    if created:
        client = SystemDefaultFields.objects.first()
        viewlist = client.default_dashboard_view
        data = {'status':2,'view':viewlist}
    else:
        viewlist = client.dashboard_view if client.dashboard_view != None else SystemDefaultFields.objects.first().default_dashboard_view
        data = {'status':1,'view':viewlist}
    return JsonResponse(data)

# === Delivery Header Preview ===
'''#abhi-15-aug-2019'''
'''#showing data in preview'''
def delivery_header_preview(request):
    data=set_tempdata_for_preview(request.POST['temp_map_data'],eval(request.POST['header']),request.POST['camp_id'],request.session['userid'])
    html = render_to_response('custom_datafields/delivery_preview.html', {'data': data,'camp_id':request.POST.get('camp_id'),'header':eval(request.POST['header'])})
    return html

# === Save Delivery Preview Data ===
'''#abhi-15-aug-2019'''
'''#saving data into database when chanegs done in preview  delivery template.'''
def save_delivery_preview_data(request):
    set_tempdata_for_preview(request.POST['temp_map_data'],eval(request.POST['header']),request.POST['camp_id'],request.session['userid'])
    return JsonResponse({'status':1})

# === Go live Campaign ===
def go_live(request, camp_id, loop_status = 0):
    import datetime, pytz
    vendors = []
    campaign = Campaign.objects.get(id=camp_id)
    campaign.status=1
    campaign.start_date = datetime.date.today()
    campaign.save()

    CampaignTrack.objects.filter(campaign_id=camp_id).update(start_date=datetime.datetime.now())

    allocations = campaign_allocation.objects.filter(campaign_id=camp_id, status=5)
    for i in allocations:
        i.status=1
        vendors.append(i.client_vendor.id)
        i.save()

    description_super = 'Client '+campaign.user.user_name+' made his campaign '+campaign.name+' live ahead of schedule'
    description_vendor = 'Campaign '+ campaign.name + ' is now live'
    title = campaign.name+' is live'

    admins = [i.id for i in user.objects.filter(usertype_id=4)]

    from client.utils import noti_via_mail
    noti = noti_via_mail(admins, title, description_super, mail_campaign_updates)
    noti1 = noti_via_mail(vendors, title, description_vendor, mail_campaign_updates)

    for i in vendors:
        RegisterNotification(campaign.user.id, i, description_vendor, title, 1, campaign, None)
    for j in admins:
        RegisterNotification(campaign.user.id, j, description_super, title, 1, campaign, None)
    # this function is not called from another function..Akshay G.
    if loop_status == 0:
        return redirect('client_live_campagin')

# === Save TC Headers ===
def save_tc_headers(request):
    # Save TC HEades By ...Kishor
    match = eval(request.POST.get('data'))
    camp_id = request.POST.get('camp_id')
    obj,created = Delivery.objects.get_or_create(campaign_id=camp_id)
    obj.data_header=','.join(match)
    obj.tc_header_status = 1
    obj.custom_header_status = 0
    obj.custom_header_mapping = str('')
    obj.save()
    # suryakant 16-10-2019
    # raised mail and refactored code
    title = "Delivery Headers"
    desc = f"Delivery headers uploaded on campaign {obj.campaign.name} by client."
    super = [i.id for i in user.objects.filter(usertype_id=4)]
    super += [d.client_vendor.id for d in campaign_allocation.objects.filter(campaign_id=obj.campaign.id)]
    for j in super:
        camp_alloc = None
        if campaign_allocation.objects.filter(campaign_id=obj.campaign.id,client_vendor_id=j).exists():
            camp_alloc = campaign_allocation.objects.get(campaign_id=obj.campaign.id,client_vendor_id=j)
        RegisterNotification(obj.campaign.user.id, j, desc, title, 2,None,camp_alloc)
    noti_via_mail(super, title, desc, mail_delivery_data_upload)
    data = {'success': 1}
    return JsonResponse(data)

# === TC Heders Checked ===
def tc_headers_checked(request):
    # Checked TC Headers by ...Kishor
    camp_id = request.POST.get('camp_id')
    delivery = Delivery.objects.get(campaign_id=camp_id)
    dataheaders = []
    if delivery:
        if delivery.data_header:
            dataheaders = delivery.data_header.split(',')
    data = {'data_header':dataheaders}
    return JsonResponse(data)

# === Bulk Normal Clone Camp ===
def bulk_normal_clone_camp(request):
# bulk clone normal campaigns..Akshay G.
    camp_id_list = ast.literal_eval(request.POST.get('camp_id_list'))
    for campaign_id in camp_id_list:
        campaign = Campaign.objects.filter(id=campaign_id)
        if campaign:
            old_campaign = Campaign.objects.get(id=campaign_id)
            old_id = old_campaign.id
            methods = old_campaign.method.all()

            specification = Specification.objects.filter(campaign=old_campaign)
            mapping = Mapping.objects.filter(campaign=old_campaign)
            delivery = Delivery.objects.filter(campaign=old_campaign)
            terms = Terms.objects.filter(campaign=old_campaign)
            use_as_text = UseAsTxtMapping.objects.filter(campaign=old_campaign).values()
            cust_question = Question_forms.objects.filter(campaign=old_campaign)
            headerSpecs = HeaderSpecsValidation.objects.filter(campaign=old_campaign)
            obj=SelectedLeadValidation.objects.filter(campaign=old_campaign)


            old_campaign.pk = None
            old_campaign.name = "cloned " + old_campaign.name
            old_campaign.approveleads = 0
            old_campaign.save()
            old_campaign.status = 0
            old_campaign.method.add(*methods)
            old_campaign.save()

            # get new campaign id
            new_campaign = old_campaign
            new_id = new_campaign.id
            new_campaign.start_date = None
            new_campaign.end_date = None
            new_campaign.save()

            if headerSpecs:
                if headerSpecs[0].company_limit:
                    HeaderSpecsValidation.objects.create(campaign=new_campaign,company_limit=headerSpecs[0].company_limit)

            # clone custom lead validation in new campaign
            if obj:
                if obj[0].component_list:
                    lead = SelectedLeadValidation.objects.create(campaign=new_campaign)
                    lead.component_list=obj[0].component_list
                    lead.save()

            if use_as_text:
                for i in use_as_text:
                    UseAsTxtMapping.objects.create(campaign=new_campaign, original_txt = i['original_txt'], alternative_txt = i['alternative_txt'])

            if specification:
                specification[0].pk = None
                specification[0].campaign = new_campaign
                specification[0].save()

            if mapping:
                mapping[0].pk = None
                mapping[0].campaign = new_campaign
                mapping[0].save()

            if cust_question:
                cust_question[0].pk = None
                cust_question[0].campaign = new_campaign
                cust_question[0].save()

            if terms:
                terms[0].pk = None
                terms[0].campaign = new_campaign
                terms[0].save()

            if delivery:
                delivery[0].pk = None
                delivery[0].campaign = new_campaign
                delivery[0].save()

            # copy selected components also in draggable campaign
            if SelectedComponents.objects.count():
                selected_component_list = SelectedComponents.objects.filter(campaign_id=old_id)
                modified_selected_component_list = []
                for component in selected_component_list:
                    component.pk = None
                    component.campaign = new_campaign
                    modified_selected_component_list.append(component)

                if modified_selected_component_list:
                    SelectedComponents.objects.bulk_create(modified_selected_component_list)
    return JsonResponse({})

# === select campaign type ===
def bulk_rfq_clone_camp(request):
# bulk clone RFQ campaigns..Akshay G.
    camp_id_list = ast.literal_eval(request.POST.get('camp_id_list'))
    for id in camp_id_list:
        camp_obj = RFQ_Campaigns.objects.get(id = id)
        camp_obj.pk = None
        camp_obj.name = "cloned " + camp_obj.name 
        new = datetime.now() + timedelta(hours=48)
        if new.weekday() in [5,6]:
            new = new + (timedelta(hours=24) if new.weekday() == 6 else timedelta(hours=48))
        camp_obj.rfq_timer = new
        camp_obj.save()
        # suryakant 12-10-2019
        # suggest vendors on rfq cloned
        rfq_suggest_vendor(request,camp_obj)
    return JsonResponse({})


# === Save Client Agreement ===
def save_client_agreement(request):
# save agreements uploaded by client ..Akshay G.
    user_id = request.session['userid']
    flag = request.POST.get('flag')
    file_name = request.POST.get('file_name')
    # flag specifies agreements are for 'new vendor' or 'old vendor'
    if flag == 'new_vendor':
        file = request.FILES['agreement_file']
    else:
        file = request.FILES['agreement_file2']
    client_dir = os.path.join(settings.MEDIA_ROOT, 'agreements', 'tc-client-id-'+str(user_id))
    if 'tc-client-id-'+str(user_id) not in os.listdir(os.path.join(settings.MEDIA_ROOT, 'agreements')):
        os.mkdir(client_dir)
    agreements_dir = os.path.join(client_dir, 'client_agreements')
    if 'client_agreements' not in os.listdir(client_dir):
        os.mkdir(agreements_dir)
    fs = FileSystemStorage()
    obj = ClientCustomFields.objects.get_or_create(client_id = user_id)[0]
    filename = fs.save(agreements_dir+'/'+file.name,  file)
    uploaded_file_name = os.path.basename(fs.url(filename))
    file_path = os.path.join('media', 'agreements', 'tc-client-id-'+str(user_id), 'client_agreements', uploaded_file_name)
    # flag specifies agreements are for 'new vendor' or 'old vendor'
    if flag == 'new_vendor':
        if obj.client_agreements:
            if obj.client_agreements.strip() != '':
                client_agr_data = ast.literal_eval(obj.client_agreements)[0]
                client_data = client_agr_data.pop('new_vendor_agreements')
                if len(client_data) == 0:
                    client_data= {file_name: file_path}
                else:
                    if file_name in client_data:
                        previous_file_path = os.path.join(settings.BASE_DIR, client_data[file_name])
                        if fs.exists(previous_file_path):
                            fs.delete(previous_file_path)
                        client_data[file_name] = file_path
                    else:
                        client_data[file_name] = file_path
                client_agr_data.update({'new_vendor_agreements': client_data})
                data = [client_agr_data]
            else:
                data = [{'new_vendor_agreements': {file_name: file_path}, 'old_vendor_agreements': {}}]
        else:
            data = [{'new_vendor_agreements': {file_name: file_path}, 'old_vendor_agreements': {}}]
        obj.client_agreements = data
        obj.save()
    else:
        # save agreements for old vendor
        client_agreements_old_vendor(obj, file_name, file_path, user_id, fs)
    return JsonResponse({'file_path': os.path.join(settings.BASE_URL, file_path)})

# === Save Client Agreements Uploaded by Old Vendor ===
def client_agreements_old_vendor(obj, file_name, file_path, user_id, fs):
# save agreements uploaded by client to be signed by old vendor..Akshay G.
    if obj.client_agreements:
        if obj.client_agreements.strip() != '':
            client_agr_data = ast.literal_eval(obj.client_agreements)[0]
            client_data = client_agr_data.pop('old_vendor_agreements')
            if len(client_data) == 0:
                client_data= {file_name: file_path}
            else:
                if file_name in client_data:
                    previous_file_path = os.path.join(settings.BASE_DIR, client_data[file_name])
                    if fs.exists(previous_file_path):
                        fs.delete(previous_file_path)
                    client_data[file_name] = file_path
                else:
                    client_data[file_name] = file_path
            client_agr_data.update({'old_vendor_agreements': client_data})
            data = [client_agr_data]
        else:
            data = [{'old_vendor_agreements': {file_name: file_path}, 'new_vendor_agreements': {}}]
    else:
        data = [{'old_vendor_agreements': {file_name: file_path}, 'new_vendor_agreements': {}}]
    obj.client_agreements = data
    obj.save()

# === Client Agreements ===
def client_agreements(request):
# show agreements uploaded by client to client..Akshay G.
    user_id = request.session['userid']
    queryset = ClientCustomFields.objects.filter(client_id = user_id)
    new_vendor_agreements = []
    old_vendor_agreements = []
    if queryset:
        if queryset[0].client_agreements:
            if queryset[0].client_agreements.strip() != '':
                client_agr_data = ast.literal_eval(queryset[0].client_agreements)[0]
                if len(client_agr_data['new_vendor_agreements']) > 0:
                    for name, path in client_agr_data['new_vendor_agreements'].items():
                        new_vendor_agreements.append({name: os.path.join(settings.BASE_URL, path)})
                if len(client_agr_data['old_vendor_agreements']) > 0:
                    for name, path in client_agr_data['old_vendor_agreements'].items():
                        old_vendor_agreements.append({name: os.path.join(settings.BASE_URL, path)})
    data = {
            'new_vendor_agreements': new_vendor_agreements,
            'old_vendor_agreements': old_vendor_agreements
            }
    return render(request, 'client/agreement.html', data)

# ===  check Vendor Scripts Upload Sttatus ===
def vendor_script_status(id, request):
    # check vendor scripts upload status for the given campaign..Akshay G.
    # check if any vendor has uploaded script file for this campaign..Akshay G.
    script_query = Scripts.objects.filter(
        campaign_id= id).exclude(user_id = request.session['userid'])
    vendor_upload_count = 0
    if script_query:
         for obj in script_query:
            if obj.client_script:
                vendor_upload_count += 1
    status = 0
    if script_query:
        status = 1 if len(script_query) == vendor_upload_count else 0
    return {'status': status, 'vendor_upload_count': vendor_upload_count, 'total_vendors': len(script_query)}

# === Check Vendor Agreements Upload Status ===
def vendor_agreements_status(id):
    # check vendor agreements upload status for the given campaign..Akshay G.
    camp_alloc_query = campaign_allocation.objects.filter(campaign_id = id)
    # show vendor agreements status to client..Akshay G.
    agreements_data = []
    for obj in camp_alloc_query:
        if obj.vendor_agreements:
            data = eval(obj.vendor_agreements)
            vendor_data = {}
            if len(data) > 0:
                for d in data:
                    for agr_name in d.keys():
                        if d[agr_name] == '':
                            vendor_data[agr_name] = 0
                        else:
                            vendor_data[agr_name] = 1
            agreements_data.append(vendor_data)
    c = 0
    for data in agreements_data:
        if all(data.values()):
            c += 1
    status = 0
    if camp_alloc_query:
        status = 1 if len(camp_alloc_query) == c else 0
    return {'total_alloc_vendors': len(camp_alloc_query), 'agreements_status': status, 'vendor_upload_agr': c}

# === change bulk campaigns status  ===
def bulk_go_live(request):
    # change bulk campaigns status to 'live' from 'assigned' ..Akshay G.
    camp_ids = ast.literal_eval(request.POST.get('camp_id_list'))
    for id in camp_ids:
        go_live(request, id, 1)
    return JsonResponse({})


def user_sitemap(request):
    # print(recur())
    import os    
    list_allfiles=[]
    dest_path = os.path.join(settings.DOCS_ROOT)
    list_allfiles=os.listdir(dest_path)    
    global_list=[]        
    files={}
    folders=[]
    global_files=[] 
    for dir in list_allfiles: 
        f=os.path.join(dest_path,dir)        
        if(os.path.isfile(f)==True):
            files={"name":dir,"path":settings.DOCS_URL+dir}                         
            global_files.append(files)
        else:
            files=get_folder_list(dir,os.path.join(dest_path,dir))
            folders.append({dir: files})
        files = {"files": global_files,"folders":folders}        
    global_list.append(files)      
    return render(request,'sitemap.html',{'global_list':global_list},)
    

def get_folder_list(dir_name,folder_path):
    global_files=[]
    files = {}
    global_folder_files={}   
    for dir in os.listdir(folder_path):
        global_folder = {}
        f=os.path.join(folder_path,dir)       
        if(os.path.isfile(f)==True):
            file1={"name":dir,"path":f}
            global_files.append(file1)
            file1={}
        else:
            files=get_folder_list(dir,os.path.join(folder_path,dir)) 
            if(len(files) > 0):
                global_folder.update({dir: files}) 
            
        if len(global_folder) > 0 :
            files={"files": global_files}
            global_folder_files.update(global_folder)
        else:
            files={"files": global_files}
    files.update(global_folder_files)
    return files    
        
  
def merge_abm_suppr_file_data(content_field, csv_data):
    import datetime
    merged_data = csv_data.copy()
    if content_field:
        merged_data = eval(content_field)
        for data in csv_data:
            # L = list(map(lambda i: dicts_equal_except(data.copy(), i.copy()), merged_data))/
            # print(L)
            # if not any(L):
            merged_data.append(data)
    return merged_data

def dicts_equal_except(d1, d2):
    dict1 = {str(i).lower(): str(j).lower() for i, j in d1.items()}
    dict2 = {str(i).lower(): str(j).lower() for i, j in d2.items()}
    dict1['domain_name'] = dict1.pop('user_domain') + dict1['domain_name']
    del dict1['timestamp']
    dict2['domain_name'] = dict2.pop('user_domain') + dict2['domain_name']
    del dict2['timestamp']
    return dict1 == dict2
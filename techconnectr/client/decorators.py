from django.shortcuts import redirect,render
from django.http import HttpResponseRedirect
from functools import wraps
from django.contrib import messages
from superadmin.models import *
from campaign.models import *
from user.models import *

def login_required(function):
    """  Check is user logged in
    If user_id not available in session
    Then it will be redirected to root url
    """
    def wrap(request=None, *args, **kwargs):
        if request is not None:
            if request.user.is_authenticated:
                return function(request, *args, **kwargs)
            else:
                messages.error(request, 'Login required, Please login')
                return redirect("/")
        else:
            print("Request data is empty while processing login required decorator")

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def is_client(function):
    """ Check is user type is client or not
        usertypes :
            * 1 - client
            * 2 - vendor
            * 3 - Superadmin
            * 5 - for external vendor

    """
    def wrap(request=None, *args, **kwargs):
        if request is not None:
            if request.user.is_authenticated:
                if request.user.usertype.id == 1 or request.user.usertype.id == 6 or request.user.usertype.id == 8:
                    if request.get_full_path() != '/client/':
                        if User_Configuration.objects.filter(url=request.get_full_path()).exists():
                            try:
                                client_access = User_Configuration.objects.filter(user__in=[request.session['userid']],url=request.get_full_path())
                                print(client_access.count())
                                access = client_access.filter(user=request.session['ext_userid'])
                            except Exception as e:
                                access = User_Configuration.objects.filter(user__in=[request.session['userid']],url=request.get_full_path())
                            if access.count() == 1:
                                return function(request, *args, **kwargs)
                            else:
                                return render(request,'errors/client_permission_denied.html',{})
                        else:
                            return function(request, *args, **kwargs)
                    else:
                        return function(request, *args, **kwargs)
                else:
                    messages.error(request, 'You are not authorized to access this page')
                    return redirect("/logout/")
            else:
                messages.error(request, 'Login required, Please login')
                return redirect("/")
        else:
            return redirect("/")
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def is_superadmin(function):
    """ Check is user type is client or not
    usertypes :
        * 1 - client
        * 2 - vendor
        * 3 - client_vendor
        * 4 - Superadmin
        * 5 - for external vendor

    """
    def wrap(request=None, *args, **kwargs):
        if request is not None:
            if request.user.is_authenticated:
                if request.user.usertype.id == 4:
                    return function(request, *args, **kwargs)
                else:
                    messages.error(request, 'You are not authorized to access this page')
                    return redirect("/logout/")
            else:
                messages.error(request, 'Login required, Please login to continue')
        return redirect("/")

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def is_vendor(function):
    """
    Check is user type is client or not
    usertypes :
        * 1 - client
        * 2 - vendor
        * 3 - client_vendor
        * 4 - Superadmin
        * 5 - for external vendor

    """
    def wrap(request, *args, **kwargs):
        if request is not None:
            if request.user.is_authenticated:
                if request.user.usertype.id in [5, 9, 2]:
                    if request.get_full_path() != '/vendor/':
                        if User_Configuration.objects.filter(url=request.get_full_path()).exists():
                            access = User_Configuration.objects.filter(user__in=[request.user.id],url=request.get_full_path())
                            if access:
                                return function(request, *args, **kwargs)
                            else:
                                return render(request,'errors/vendor_permission_denied.html',{})
                        else:
                            return function(request, *args, **kwargs)
                    else:
                        return function(request, *args, **kwargs)
                else:
                    messages.error(request, 'You are not authorized to access this page')
                    return redirect("/logout/")
            else:
                messages.error(request, 'Login required, Please login')
                return redirect("/")
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def is_users_campaign(function):
    '''
        to check user is accessing his campaigns
    '''
    def wrap(request=None, *args, **kwargs):
        if request is not None:
            if request.user.is_authenticated:
                if request.user.usertype.id in [1,6,8]:
                    if User_Configuration.objects.filter(url=request.get_full_path()).exists():
                        try:
                            client_access = User_Configuration.objects.filter(user__in=[request.session['userid']],url=request.get_full_path())
                            print(client_access.count())
                            access = client_access.filter(user=request.session['ext_userid'])
                        except Exception as e:
                            access = User_Configuration.objects.filter(user__in=[request.session['userid']],url=request.get_full_path())
                        if access.count() == 1:
                            if is_clients_campaign(request) is True:
                                return function(request, *args, **kwargs)
                            messages.error(request, 'You are not authorized to access this page')
                            return redirect("/logout/")
                        else:
                            return render(request,'errors/client_permission_denied.html',{})
                    else:
                        if is_clients_campaign(request) is True:
                            return function(request, *args, **kwargs)
                        messages.error(request, 'You are not authorized to access this page')
                        return redirect("/logout/")
                else:
                    messages.error(request, 'You are not authorized to access this page')
                    return redirect("/logout/")
            else:
                messages.error(request, 'Login required, Please login')
                return redirect("/")
        else:
            return redirect("/")

    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def is_clients_campaign(request):
    url = request.get_full_path()
    url_data = url.split('/')
    for i in url_data[1:]:
        try:
            if i:
                data = eval(i)
                if type(data) == int:
                    campaign = Campaign.objects.filter(id=i, user_id=request.session['userid'])
                    if campaign:
                        return True
                    else:
                        return False
        except NameError:
            pass
    return False


def is_vendors_campaign(request):
    url = request.get_full_path()
    url_data = url.split('/')
    for i in url_data[::-1]:
        try:
            if i:
                data = eval(i)
                if type(data) == int:
                    campaign = campaign_allocation.objects.filter(id=i, client_vendor_id=request.session['userid'])
                    if campaign:
                        return True
                    else:
                        return False
        except NameError:
            pass
    return False


def is_campaign_of_vendor(request):
    url = request.get_full_path()
    url_data = url.split('/')
    for i in url_data[::-1]:
        try:
            if i:
                data = eval(i)
                if type(data) == int:
                    campaign = campaign_allocation.objects.filter(campaign_id=i, client_vendor_id=request.session['userid'])
                    if campaign:
                        return True
                    else:
                        return False
        except NameError:
            pass
    return False

def is_campaigns_vendor(function):
    """
        check if vendor associated with campaign or not
    """
    def wrap(request, *args, **kwargs):
        if request is not None:
            if request.user.is_authenticated:
                if request.user.usertype.id == 2 or request.user.usertype.id in [5, 8]:
                    if request.get_full_path() != '/vendor/':
                        if User_Configuration.objects.filter(url=request.get_full_path()).exists():
                            access = User_Configuration.objects.filter(user__in=[request.user.id],url=request.get_full_path())
                            if access:
                                if is_vendors_campaign(request) is True:
                                    return function(request, *args, **kwargs)
                                messages.error(request, 'You are not authorized to access this page')
                                return redirect("/logout/")
                            else:
                                return render(request,'errors/vendor_permission_denied.html',{})
                        else:
                            if is_vendors_campaign(request) is True:
                                return function(request, *args, **kwargs)
                            messages.error(request, 'You are not authorized to access this page')
                            return redirect("/logout/")
                    else:
                        if is_vendors_campaign(request) is True:
                            return function(request, *args, **kwargs)
                        messages.error(request, 'You are not authorized to access this page')
                        return redirect("/logout/")
                else:
                    messages.error(request, 'You are not authorized to access this page')
                    return redirect("/logout/")
            else:
                messages.error(request, 'Login required, Please login')
                return redirect("/")
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap


def is_vendor_of_campaign(function):
    """
        check if vendor associated with campaign or not
    """
    def wrap(request, *args, **kwargs):
        if request is not None:
            if request.user.is_authenticated:
                if request.user.usertype.id == 2 or request.user.usertype.id in [5, 8]:
                    if request.get_full_path() != '/vendor/':
                        if User_Configuration.objects.filter(url=request.get_full_path()).exists():
                            access = User_Configuration.objects.filter(user__in=[request.user.id],url=request.get_full_path())
                            if access:
                                if is_campaign_of_vendor(request) is True:
                                    return function(request, *args, **kwargs)
                                messages.error(request, 'You are not authorized to access this page')
                                return redirect("/logout/")
                            else:
                                return render(request,'errors/vendor_permission_denied.html',{})
                        else:
                            if is_campaign_of_vendor(request) is True:
                                return function(request, *args, **kwargs)
                            messages.error(request, 'You are not authorized to access this page')
                            return redirect("/logout/")
                    else:
                        if is_campaign_of_vendor(request) is True:
                            return function(request, *args, **kwargs)
                        messages.error(request, 'You are not authorized to access this page')
                        return redirect("/logout/")
                else:
                    messages.error(request, 'You are not authorized to access this page')
                    return redirect("/logout/")
            else:
                messages.error(request, 'Login required, Please login')
                return redirect("/")
    wrap.__doc__ = function.__doc__
    wrap.__name__ = function.__name__
    return wrap

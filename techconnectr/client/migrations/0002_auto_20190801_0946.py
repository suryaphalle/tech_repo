# Generated by Django 2.1.4 on 2019-08-01 09:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('client', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='campaigntrack',
            name='created_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='campaigntrack',
            name='start_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]

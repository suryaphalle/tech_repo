"""techconnectr URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static
from login_register import views
from superadmin import views
from client import views
from vendors import views as cl
from client_vendor import views
from client_users import views
from form_builder import urls
from support import urls
from django.views.generic import TemplateView
from django.conf.urls import url
from login_register.views import activate
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

admin.site.site_header = "Techconnectr"  # default: "Django Administration"
# default: "Site administration"
admin.site.index_title = "Techconnectr administration"
admin.site.site_title = "Techconnectr admin"  # default: "Django site admin"

urlpatterns = [
    path("vendor-portal/", include("client_vendor.url")),
    path("users-portal/", include("client_users.urls")),
    path("", include("login_register.url")),
    path("superadmin/", include("superadmin.url")),
    path("client/", include("client.url")),
    path("utils/", include("campaign.urls")),
    path("vendor/", include("vendors.url")),
    path("form/", include("form_builder.urls")),
    path("support/", include("support.urls")),
    url(
        r"^activate/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$",
        activate,
        name="activate",
    ),
    path("admin/", admin.site.urls),
    path("WelcomeOnBoard", TemplateView.as_view(template_name="welcome.html")),
    path("campaign_mapping/", include("campaign_mapping.url")),
    path("client-report/", include("leads.urls")),
    path("converstion/", include("user.url")),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(settings.DOCS_URL, document_root=settings.DOCS_ROOT)
# urlpatterns += staticfiles_urlpatterns()

from django.shortcuts import get_object_or_404, render, redirect
from django.conf import settings
from django.urls import resolve
from vendors.views import *
from django.http import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from django.urls import reverse
from user.models import *
import ast, os
from superadmin.models import *
from setupdata.models import *
from datetime import datetime, date, time, timedelta

def notify_processer(request):
    current_view = resolve(request.path)
    active_page = current_view.url_name
    camp_id = 0
    if 'camp_id' in current_view.kwargs:
        camp_id = current_view.kwargs['camp_id']
    if 'is_login' in request.session:
        if request.session['is_login'] == True:
            userid = request.session['userid']
            # track user activity_logs
            current_activites(request.session['userid'], active_page, camp_id)
            chat_notify, total_notif = chat_notify_processer(
                userid)  # get chat notification
            notifydic = notification(request.session['userid'])
            notifc_receiver_obj = Notification_Reciever.objects.filter(receiver_id = request.session['userid'])
            # create dict of company logo ..Akshay G.
            logo_dict = {}
            # current logged in user
            login_user = user.objects.get(id = userid)
            for i in notifc_receiver_obj:
                user_obj = user.objects.get(id=i.Notification.sender.id)
                # fetch user's company logo
                company_logo = return_company_logo(login_user, user_obj)
                if 'default.jpg' in company_logo:
                    logo_dict['default'] = company_logo
                else:
                    logo_dict[i.Notification.id] = company_logo        
            return {'logo_dict':logo_dict,'chat_notify': chat_notify, 'count_chat': total_notif, 'notifycount': notifydic["notifycount"], 'notify': notifydic["notify"]}
    else:
        HttpResponseRedirect('/logout/')
        return {'success': 0}

# return chat messages data..Akshay
def chat_notify_processer(userid):
    notify = []
    queryset = Campaign_chats.objects.all()
    for obj in queryset.order_by('id'):
        unread_chat_data = chat_unread_msg(obj.id, userid)
        notify.append(unread_chat_data)
    notify = list(filter(lambda a: a != {}, notify))
    sort = insertion_sort(notify)
    total_notif = 0
    for i in sort:
        total_notif += len(i)
    return sort[::-1], total_notif

def insertion_sort(arr):
	for i in range(1, len(arr)):
		val = arr[i]
		j = binary_search(arr, val, 0, i-1)
		arr = arr[:j] + [val] + arr[j:i] + arr[i+1:]
	return arr

# binary sort algorithm..Akshay G.
def binary_search(arr, val, start, end):
    if start == end:
        for k, v in arr[start].items():
            date1 = v['date']
        for k, v in val.items():
            date2 = v['date']
        if date1 > date2:
            return start
        else:
            return start+1
    if start > end:
        return start
    mid = int((start+end)/2)
    for k, v in arr[mid].items():
        date1 = v['date']
    for k, v in val.items():
        date2 = v['date']
    if date1 < date2:
        return binary_search(arr, val, mid+1, end)
    elif date1 > date2:
        return binary_search(arr, val, start, mid-1)
    else:
        return mid

# return unread chat messages data..Akshay G.
def chat_unread_msg(chat_obj_id, userid):
    user_obj = user.objects.get(id = userid)
    camp_chat = Campaign_chats.objects.get(id = chat_obj_id)
    data = {}
    if camp_chat.send_msg_count != None and camp_chat.send_msg_count.strip() != '' and camp_chat.data != None and camp_chat.data.strip() != '':
        message_data = eval(camp_chat.data)
        d1 = eval(camp_chat.send_msg_count)
        # if user is in chat dict
        if userid in d1:
            for sender_id in d1[userid]:
                if sender_id != 'count':
                    # if sender has sent at least 1 message to login user
                    if len(d1[userid][sender_id]) > 1:
                        sender_user = user.objects.get(id=sender_id)
                        if sender_user.usertype_id == 4 and sender_user.user_name == None:
                            sender_user.user_name = 'superadmin'
                        chat_message = message_data[d1[userid][sender_id][-1] - 1]['message']
                        date = message_data[d1[userid][sender_id][-1] - 1]['date']
                        time = message_data[d1[userid][sender_id][-1] - 1]['time']
                        date_time = date + ' ' + time
                        format = "%Y-%m-%d %H:%M:%S"
                        dateTime_obj = datetime.strptime(date_time, format)
                        logo = return_company_logo(user_obj, sender_user)
                        chat_data = {'count': d1[userid][sender_id][0], 'message':chat_message,
                                    'date':dateTime_obj, 'time': time, 'logo': logo,'campaign_name':camp_chat.campaign.name,
                                    'campaign_id':camp_chat.campaign.id, 'sender_id': sender_id,'chat_obj_id':camp_chat.id}
                        if user_obj.usertype_id == 2:
                            camp_allo_detail = camp_chat.campaign.campaign_allocation_set.filter(client_vendor_id = userid)
                            if sender_user.anonymous_status == True and sender_user.usertype_id in [1]:
                                data['anonymous'] = chat_data
                                if camp_allo_detail:
                                    data['anonymous']['camp_status'] = camp_allo_detail[0].status
                            else:
                                data[sender_user.user_name] = chat_data
                                if camp_allo_detail:
                                    data[sender_user.user_name]['camp_status'] = camp_allo_detail[0].status
                        else:
                            data[sender_user.user_name] = chat_data
                        if user_obj.usertype_id == 1 or user_obj.usertype_id == 5:
                            data[sender_user.user_name]['camp_status'] = camp_chat.campaign.status
    return data

# return notif sender's company logo ...pass login user & sender user ..Akshay G.
def return_company_logo(login_user, sender_user):
    from client.utils import fetch_default_logo
    default_logo = fetch_default_logo(sender_user.id)
    logo = None
    # for vendor, hide anonymous client's company logo
    if login_user.usertype_id == 2:
        if sender_user.anonymous_status == False and sender_user.usertype_id in [1, 2]:
            client_vendor_detail = client_vendor.objects.filter(user=sender_user)
            if client_vendor_detail:
                try:
                    logo = client_vendor_detail[0].company_logo
                except:
                    pass
        elif sender_user.anonymous_status == True and sender_user.usertype_id in [1]:
            default_logo = os.path.join(settings.BASE_URL, 'media', 'default.jpg')
    # check if, notifs is from a client or a vendor
    elif sender_user.usertype_id in [1, 2]:
        client_vendor_detail = client_vendor.objects.filter(user=sender_user)
        if client_vendor_detail:
            try:
                logo = client_vendor_detail[0].company_logo
            except:
                pass
    # check if logo file exists in media-> 'company_logo' folder
    if logo:
        loGo = logo.url.replace('/media/company_logo/','',1)
        if loGo in os.listdir(os.path.join(settings.MEDIA_ROOT, 'company_logo')):
            company_logo = logo
        else:
            company_logo = None
    else:
        company_logo = None
    # set default logo image for superadmin & ext vendor
    if company_logo == None:
        return default_logo
    else:
        return company_logo.url

def current_activites(userid, active_page, camp_id):
    activity_logs = []
    last_dict = {}
    date = datetime.today().strftime('%Y-%m-%d')
    time = datetime.now().strftime('%H:%M:%S')
    if user_activities.objects.filter(user_id=userid).count() > 0:
        if user_activities.objects.filter(user_id=userid, date=date).count() == 1:
            activities = user_activities.objects.get(user_id=userid, date=date)
            list = ast.literal_eval(activities.activity_logs)
            list.append({'time': time, 'url': active_page, 'action': '1'})
            activities.date = date
            activities.arg = camp_id
            activities.active_page = active_page
            activities.activity_logs = list
            activities.save()
        else:
            activity_logs.append(
                {'time': time, 'url': active_page, 'action': '1'})
            user_activities.objects.create(
                user_id=userid, arg=camp_id, date=date, active_page=active_page, activity_logs=activity_logs)
    else:
        activity_logs.append({'time': time, 'url': active_page, 'action': '1'})
        print(activity_logs)
        user_activities.objects.create(
            user_id=userid, arg=camp_id, date=date, active_page=active_page, activity_logs=activity_logs)


def access_specifiers(request):
    if 'is_login' in request.session:
        if request.session['is_login'] == True:
            try:
                group = Client_User_Group.objects.filter(group_users=request.session['ext_userid'])
                client_access = User_Configuration.objects.filter(user=request.session['userid']).order_by('position')
                if group:
                    data = client_access.filter(user=request.session['ext_userid'],user_type=request.session['usertype'],group__in=[group[0]]).order_by('position')
                else:
                    data = client_access.filter(user=request.session['ext_userid'],user_type=request.session['usertype']).order_by('position')
            except Exception as e:
                data = User_Configuration.objects.filter(user=request.session['userid'],user_type=request.session['usertype']).order_by('position')
                usertype = user.objects.get(id=request.session['userid'])
                request.session['onBording']=usertype.status
                from client.utils import fetch_default_logo
                request.session['profilepic'] = fetch_default_logo(usertype.id)
                try:
                    logo = client_vendor.objects.get(user_id=request.session['userid'])
                    if logo.company_logo and logo.company_logo.storage.exists(logo.company_logo.name):
                        request.session['profilepic']=str(logo.company_logo_smartthumbnail.url)
                except Exception as e:
                    pass
            access = []
            for d in data:
                access.append({
                    'id':d.id,
                    'name':d.name,
                    'url':d.url,
                    'parent':d.parent.id if d.parent else None,
                    })
        return {'data':access}
    else:
        HttpResponseRedirect('/logout/')
        return {'success':0}

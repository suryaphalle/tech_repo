from django.urls import path, include, re_path
from campaign_mapping.views import views,campaign_mapp

urlpatterns = [
    path('create_bulk_camp/', views.create_bulk_camp),
    path('raw_data_to_mapp/', campaign_mapp.raw_data_to_mapp),
    path('raw_data_to_create_campaign/', campaign_mapp.raw_data_to_create_campaign),
    path('save_mapped_headers/',views.save_mapped_headers),
    path('update_headers/',views.update_preview_headers),
    path('preview_headers/', views.preview_mapped_headers, name='previews'),
    path('mapping_header/', views.mapping_headers, name='mapping_headers'),
    path('raw_data_to_create_RFQ_campaign/', campaign_mapp.raw_data_to_create_RFQ_campaign, name='mapping_headers'),
    
    path('upload_bulk_rfq/', views.upload_bulk_rfq),
]
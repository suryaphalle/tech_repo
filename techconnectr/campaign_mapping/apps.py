from django.apps import AppConfig


class CampaignMappingConfig(AppConfig):
    name = 'campaign_mapping'

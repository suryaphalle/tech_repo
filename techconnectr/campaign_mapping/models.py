from django.db import models
from campaign.models import *
from user.models import *
# Create your models here.


class Campaign_dataset(models.Model):
    user_header = models.TextField(blank=True, null=True)
    tc_header = models.TextField(blank=True, null=True)
    header_value = models.TextField(blank=True, null=True)
    merge_value = models.TextField(blank=True, null=True)
    tc_field_name = models.TextField(blank=True, null=True)
    tc_table_name = models.TextField(blank=True, null=True)
    tc_header_rank = models.IntegerField(blank=True, null=True)
    dataType = models.CharField(max_length = 100, blank = True, null = True)
    # def __str__(self):
    #     return 'Campaign Dataset'

class rfq_campaign_dataset(models.Model):
    user_header = models.TextField(blank=True, null=True)
    tc_header = models.TextField(blank=True, null=True)
    header_value = models.TextField(blank=True, null=True)
    merge_value = models.TextField(blank=True, null=True)
    tc_field_name = models.TextField(blank=True, null=True)
    tc_table_name = models.TextField(blank=True, null=True)
    tc_header_rank = models.IntegerField(blank=True, null=True)
    dataType = models.CharField(max_length = 100, blank = True, null = True)
    

class Etl_mapp_header(models.Model):
    user = models.ForeignKey(
        user, on_delete=models.CASCADE, blank=True, null=True)
    mapped_headers = models.TextField(blank=True, null=True)
    final_mapp_data = models.TextField(blank=True, null=True)
    client_default_mapp = models.TextField(blank=True, null=True)
    # decides whether mapped data is of rfq or normal..Akshay G. default-normal
    rfq_status = models.BooleanField(default = False)
    # Becomes True when campaigns are created..Akshay G.
    status = models.BooleanField(default = False)

class Client_temp_data(models.Model):
    user = models.ForeignKey(
        user, on_delete=models.CASCADE, blank=True, null=True)
    timestamp = models.DateTimeField(auto_now=True, blank=True, null=True)
    data = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.user.user_name


class Campaign_others_specs(models.Model):
    user = models.ForeignKey(
        user, on_delete=models.CASCADE, blank=True, null=True)
    campaign = models.ForeignKey(
        Campaign, on_delete=models.CASCADE, blank=True, null=True)
    others_specs = models.TextField(blank=True, null=True)

class Tc_header(models.Model):
    tc_headers = models.TextField(blank = True, null = True)
    # decides whether mapped data is of rfq or normal..Akshay G. default-normal
    rfq_status = models.BooleanField(default = False)
    def __str__(self):
        return 'TC Headers'

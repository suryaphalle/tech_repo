from django.contrib import admin
from django.apps import apps
from .models import *


@admin.register(Campaign_dataset)
class Campaign_datasetAdmin(admin.ModelAdmin):
	list_display = ("user_header", "tc_header",  "merge_value", "tc_field_name", "tc_table_name","tc_header_rank")

@admin.register(rfq_campaign_dataset)
class rfq_campaign_dataset(admin.ModelAdmin):
	list_display = ("user_header", "tc_header","dataType", "merge_value", "tc_field_name", "tc_table_name","tc_header_rank")

@admin.register(Etl_mapp_header)
class Etl_mapp_headerAdmin(admin.ModelAdmin):
	list_display = ("mapped_headers","final_mapp_data","client_default_mapp")

@admin.register(Client_temp_data)
class Client_temp_dataAdmin(admin.ModelAdmin):
	list_display = ("data","timestamp")	

@admin.register(Campaign_others_specs)
class Campaign_others_specsAdmin(admin.ModelAdmin):
	list_display = ("campaign","others_specs")	
	
@admin.register(Tc_header)
class Tc_headerAdmin(admin.ModelAdmin):
	list_display = ["tc_headers"]	

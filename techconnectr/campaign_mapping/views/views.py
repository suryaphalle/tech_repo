from django.shortcuts import render, redirect
from user.models import user
from campaign_mapping.models import *
import pandas, re
from django.http import *
import ast
import json
from .campaign_mapp import *
from campaign.choices import *
from client.utils import *

# === Receive bulk campaigns ===
def create_bulk_camp(request):
    # Receive bulk campaigns csv file for creating campaigns...Akshay G..Date - 8-8-2019
    user_id = request.session['userid']
    csv_file = request.FILES['create_bulk_camp']
    camp_type = request.POST.get('camp_type')
    if csv_file:
        if csv_file.name.endswith('.csv'):
            try:
                data = pandas.read_csv(csv_file,skip_blank_lines=True,na_filter=False,encoding ='latin1')
                data = data.dropna()
            except:
                return JsonResponse({'status':'error_1','title':'Empty file','message': 'Please upload valid file.', 'icon':'error'})
        else:
            try:
                xl = pandas.ExcelFile(csv_file)
                data = pandas.read_excel(xl,xl.sheet_names[0],dtype=str).fillna('')
                data = data.dropna()
            except:
                return JsonResponse({'status':'error_1','title':'Empty file','message': 'Please upload valid file.', 'icon':'error'})
        # file headers
        header = []
        for row in data:
            header.append(row)
        # all campaign data in list of lists
        camp_data = fetch_csv_data(data,header)
        if camp_data:
            # list of campaign data dictionaries
            all_camp_data, camp_name_index, flag = create_camp_dict(camp_data, header, user_id)
            if flag == False:
                return JsonResponse(camp_name_index)
        else:
            return JsonResponse({'status':'error_1', 'title':'No campaign data','message':'Please upload valid file with campaign details.','icon':'error'})
        # map csv file headers into ETL mapp table for client
        header_objects = Campaign_dataset.objects.order_by('tc_header_rank')
        status, error_data = mapp_csv_header(header, user_id, all_camp_data, header_objects, camp_type, camp_data, camp_name_index)
        # check if error found in csv data
        if status == False:
            return JsonResponse({'status':'error', 'error_data': error_data})
        else:
            return JsonResponse({'status':'success'})
    else:
        return JsonResponse({'status':'error_1', 'title':'Invalid file format','message': 'Please upload .csv or .xlsx file.', 'icon':'error'})

# === Fetch csv data into list of lists ===
def fetch_csv_data(data, header):
    # fetch data into list
    # fetch csv data into list of lists..Akshay G..Date - 8-8-2019
    camp_data = []
    for i in range(len(data)):
        temp_data = []
        for head in header:
            if data[head][i] != 'nan' and data[head][i] != 'non':
                temp_data.append(data[head][i])
            else:
                temp_data.append('')
        if any(str(i).strip() != '' for i in temp_data):
            camp_data.append(temp_data)
    return camp_data

# === Split String ===
def split_string(str1, str2):
    # split strings using regrex..Akshay G..Date - 8-8-2019
    data = re.split('\/|,-',str2)
    L = []
    for s in data:
        if '\\' in s:
            L.extend(s.split('\\'))
        elif ';' in s:
            L.extend(s.split(';'))
        elif ',' in s:
            L.extend(s.split(','))
        else:
            L.append(s)
    if type(str1) != list and str1.strip() != '':
        return [str1] + L
    return str1 + L if str1[0].strip() != '' else L

# check if header is description or IO_no (don't validate value for desc, IO header)..Akshay G...10-10-2019
def camp_description_validation(header, user_id):
    tc_header_dataset_query = Campaign_dataset.objects.filter(tc_header__in = ['description', 'io_number'])
    for obj in tc_header_dataset_query:
        desc_values = eval(obj.user_header.lower())
        if header.lower() in desc_values:
            return True
    header_objects = Campaign_dataset.objects.filter().exclude(tc_header__in = ['description', 'io_number'])
    for header_obj in header_objects:
        if header.strip().lower() in [i.strip().lower() for i in eval(header_obj.user_header)]:
            return False
    c = 0
    etl_mapp_obj = Etl_mapp_header.objects.filter(user_id = user_id)
    if etl_mapp_obj:
        if etl_mapp_obj[0].client_default_mapp:
            default_headers = eval(etl_mapp_obj[0].client_default_mapp)
            for dict_ in default_headers:
                if dict_['tc_header'] in ['description', 'io_number']:
                    c += 1
                    if header.lower() in [j.lower() for j in dict_['user_header']]:
                        return True
                if c == 2:
                    break
    return False

# === Merge Campaign Data in CSV File===
def return_campData(row, camp_dict, user_id):
# Merge campaign data, if it spans multiple rows in csv file ..Akshay G...Date - 8-8-2019
    for header in row:
        # check if file column has header
        if header != '':
            previous_header = header
            if header not in camp_dict:
                if camp_description_validation(header, user_id):
                    # convert numpy int64 obj to python native int -> str.
                    if type(row[header]) != int and type(row[header]) != str and type(row[header]) != float:
                        camp_dict[header] = [str(row[header].item())]
                    else:
                        camp_dict[header] = [str(row[header])]
                    continue
                # convert numpy int64 obj to python native int -> str.
                if type(row[header]) != int and type(row[header]) != str and type(row[header]) != float:
                    if type(row[header]) == pandas._libs.tslibs.timestamps.Timestamp:
                        row[header] = datetime.strftime(row[header].date(),'%Y-%m-%d')
                    else:
                        row[header] = str(row[header].item())
                value = re.split('\/|,-',str(row[header]))
                L = []
                for s in value:
                    if '\\' in s:
                        L.extend(s.split('\\'))
                    elif ';' in s:
                        L.extend(s.split(';'))
                    elif ',' in s:
                        L.extend(s.split(','))
                    else:
                        L.append(s)
                camp_dict[header] = L
            else:
                if camp_description_validation(header, user_id):
                    # convert numpy int64 obj to python native int -> str.
                    if type(row[header]) != int and type(row[header]) != str and type(row[header]) != float:
                        camp_dict[header] = [camp_dict[header][0] + ' ' + str(row[header].item())]
                    else:
                        camp_dict[header] = [camp_dict[header][0] + ' ' + str(row[header])]
                    continue
                # campaign has spanned multiple rows
                if type(row[header]) != int and type(row[header]) != str and type(row[header]) != float:
                    row[header] = str(row[header].item())
                if row[header].strip() != '':
                    camp_dict[header] = split_string(camp_dict[header], row[header])
        else:
            if row[header].strip() != '':
                if camp_description_validation(previous_header, user_id):
                    camp_dict[previous_header] = [camp_dict[previous_header][0] + ' ' + row[header]]
                    continue
                camp_dict[previous_header] = split_string(camp_dict[previous_header], row[header])
    return camp_dict

# === Create Dictionary ===
def create_dict(L, d):
    # create campaign dict..Akshay G...Date - 8-8-2019
	for k, v in d.items():
		d[k] = L[v]
	return d

# === Create Campaign Dict ===
def create_camp_dict(list_data, headers, user_id):
    # create list of dictionaries of camp data..Date - 8-8-2019
    # store list of camp dict data into temporary client table...Akshay G.
    file = list_data
    camp_header = {}
    camp_name_header_index = None
    tc_header_dataset_obj = Campaign_dataset.objects.filter(tc_header = 'name')[0]
    # read user header 'campaign name' from file to separate campaign data
    user_headers = [i.lower() for i in eval(tc_header_dataset_obj.user_header)]
    for i in headers:
        if i.strip().lower() in user_headers:
            camp_name_header_index = headers.index(i)
        camp_header[i] = headers.index(i)
    if camp_name_header_index == None:
        return '', {'status':'error_1','title':'Invalid template !','message': 'Please upload valid template with campaign name header as campaign_name, name or camp_name.', 'icon':'warning'}, False
    all_camp_data = []
    camp_dict = {}
    for i in range(len(file)):
        d1 = camp_header.copy()
        if all(x == '' for x in file[i]):
            continue
        elif file[i][camp_name_header_index].strip() == '':
            camp_d = create_dict(file[i], d1)
            camp_dict = return_campData(camp_d, camp_dict, user_id)
            if i == len(file) -1:
                all_camp_data.append(camp_dict)
        else:
            if camp_dict != {}:
                all_camp_data.append(camp_dict)
            camp_dict = {}
            camp_d = create_dict(file[i], d1)
            camp_dict = return_campData(camp_d, camp_dict, user_id)
            if i == len(file) -1:
                all_camp_data.append(camp_dict)
    # store campaign data into temporary objects
    temp_object = Client_temp_data.objects.get_or_create(user_id = user_id)[0]
    temp_object.data = all_camp_data
    temp_object.save()
    if len(all_camp_data) > 100:
        return '', {'status':'error_1','title':'Limit exceeded !','message': 'Please upload data of maximum 100 campaigns.', 'icon':'warning'}, False
    return all_camp_data, camp_name_header_index, True

# === Mapp CSV Header ===
def mapp_csv_header(csv_header, user_id, all_camp_data, header_objects, camp_type, camp_data_List, camp_name_index):
    # map csv file headers and values in Etl map table...Akshay G...11-8-2019
    etl_mapp_obj = Etl_mapp_header.objects.get_or_create(user_id = user_id)[0]
    mapped_data = []
    counter = 1
    error_data = {}
    camp_data_list = [List for List in camp_data_List if List[camp_name_index].strip() != '']
    for camp_data in all_camp_data:
        mapped_TcHeaders_list = []
        temp_mapp_data = []
        header_values = [[] for i in range(len(csv_header))]
        # 'header_boolean_list' to check if system couldn't map any header
        header_boolean_list = [False for i in range(len(csv_header))]
        for k, v in camp_data.items():
            if type(v) == str:
                header_values[csv_header.index(k)] = [v]
            else:
                header_values[csv_header.index(k)] = v
        for i in header_values:
            header_values[header_values.index(i)] = list(map(lambda x: x.strip().lower(),i))
        for header_obj in header_objects:
            L = list(header.strip().lower() in [i.strip().lower() for i in eval(header_obj.user_header)] for header in csv_header)
            if any(L):
                header_boolean_list[L.index(True)] = True
                tc_header_value = []
                # 'boolean_list' to check if system couldn't map any header value
                boolean_list = [False for i in range(len(header_values[L.index(True)]))]
                for i in eval(header_obj.header_value):
                    for key, value in i.items():
                        L1 = list(x.strip().lower() in list(map(lambda x:x.strip().lower(),value)) for x in header_values[L.index(True)])
                        if any(L1):
                            boolean_list[L1.index(True)] = True
                            tc_header_value.append(key)
                if all(boolean_list):
                    value = tc_header_value
                else:
                    value = tc_header_value + [header_values[L.index(True)][i] for i in range(len(boolean_list)) if boolean_list[i] == False]
                original_data = camp_data_list[all_camp_data.index(camp_data)][L.index(True)]
                is_valid, status, value = csv_validation(header_obj.tc_field_name, value, header_obj.dataType, original_data)
                # error is being found in csv data
                if is_valid == False:
                    # store error data in 'error_data' dict
                    error_data.update(status)
                # store temp mapped header with values
                mapped_TcHeaders_list.append(header_obj.tc_header)
                temp_mapp_data.append({'user_header': csv_header[L.index(True)],
                                    'tc_header': header_obj.tc_header,
                                    'values': value,
                                    'tc_field_name': header_obj.tc_field_name,
                                    'tc_table_name': header_obj.tc_table_name,
                                    'merge_value':[],
                                    'datatype': header_obj.dataType
                                    })
        # check if any of csv headers is not mapped
        # custom headers
        if any(i == False for i in header_boolean_list):
            custom_headers = [csv_header[i] for i in range(len(header_boolean_list)) if header_boolean_list[i] == False]
            for header in custom_headers:
                mapped_tc_header = None
                value = header_values[csv_header.index(header)]
                if etl_mapp_obj.client_default_mapp:
                    default_headers = eval(etl_mapp_obj.client_default_mapp)
                    for dict_ in default_headers:
                        if header in dict_['user_header']:
                            if dict_['tc_header'] not in mapped_TcHeaders_list:
                                mapped_tc_header = dict_['tc_header']
                            else:
                                dict_['user_header'].remove(header)
                if camp_type == 'rfq':
                    if len(rfq_campaign_dataset.objects.filter(tc_header = mapped_tc_header)) == 0:
                        mapped_tc_header = None
                else:
                    if len(Campaign_dataset.objects.filter(tc_header = mapped_tc_header)) == 0:
                        mapped_tc_header = None
                if mapped_tc_header:
                    if camp_type == 'rfq':
                        dataset_obj = rfq_campaign_dataset.objects.filter(tc_header = mapped_tc_header)
                    else:
                        dataset_obj = Campaign_dataset.objects.filter(tc_header = mapped_tc_header)
                    if len(dataset_obj) == 0 and camp_type == 'rfq':
                        dataset_obj = Campaign_dataset.objects.filter(tc_header = mapped_tc_header)[0]
                    elif len(dataset_obj) == 0:
                        dataset_obj = rfq_campaign_dataset.objects.filter(tc_header = mapped_tc_header)[0]
                    else:
                        dataset_obj = dataset_obj[0]
                    tc_header_value = []
                    # 'boolean_list' to check if system couldn't map any header value
                    boolean_list = [False for i in range(len(value))]
                    for i in eval(dataset_obj.header_value):
                        for key, header_value in i.items():
                            L1 = list(x.strip().lower() in list(map(lambda x:x.strip().lower(),header_value)) for x in value)
                            if any(L1):
                                boolean_list[L1.index(True)] = True
                                tc_header_value.append(key)
                    if all(boolean_list):
                        value = tc_header_value
                    else:
                        value = tc_header_value + [value[i] for i in range(len(boolean_list)) if boolean_list[i] == False]
                    is_valid, status, value = csv_validation(mapped_tc_header, value, dataset_obj.dataType, '')
                    if is_valid == False:
                        # store error data in 'error_data' dict
                        error_data.update(status)
                    # store temp mapped header with values
                    temp_mapp_data.append({'user_header': header,
                                        'tc_header': mapped_tc_header,
                                        'values': value,
                                        'tc_field_name': mapped_tc_header,
                                        'tc_table_name': dataset_obj.tc_table_name,
                                        'merge_value':[],
                                        'datatype': dataset_obj.dataType
                                        })
                else:
                    value = [i.capitalize() for i in value]
                    if 'Unnamed:' in header:
                        pass
                    else:
                        # store temp custom user header with values.
                        temp_mapp_data.append({'user_header': header,
                                            'tc_header': '',
                                            'values': value,
                                            'tc_field_name': '',
                                            'tc_table_name': '',
                                            'merge_value':[],
                                            'datatype': ''
                                            })
        mapped_data.append({counter: temp_mapp_data})
        counter += 1
    # if any error found in csv data
    if len(error_data) > 1:
        return False, error_data
    etl_mapp_obj.mapped_headers = mapped_data
    # update status about whether data is of rfq or normal
    etl_mapp_obj.rfq_status = True if camp_type == 'rfq' else False
    # mark status as False to identify that campaigns are not created using this data
    etl_mapp_obj.status = False
    etl_mapp_obj.save()
    return True, ''

# validate header's values after user manually maps them...Akshay G...Date- 5-9-2019
def validate_after_manual_mapp(header_obj, header_values):
    tc_header_value = []
    if type(header_values) in [int, str, float]:
        header_values = [str(header_values)]
    elif type(header_values) == list:
        header_values = [str(i) for i in header_values]
    elif header_values == None:
        header_values = ['']
    # 'boolean_list' to check if system couldn't map any header value
    boolean_list = [False for i in range(len(header_values))]
    for i in eval(header_obj.header_value):
        for key, value in i.items():
            L1 = list(x.strip().lower() in list(map(lambda x:x.strip().lower(),value)) for x in header_values)
            if any(L1):
                boolean_list[L1.index(True)] = True
                tc_header_value.append(key)
    if all(boolean_list):
        value = tc_header_value
    else:
        value = tc_header_value + [header_values[i] for i in range(len(boolean_list)) if boolean_list[i] == False]
    is_valid, status, value = csv_validation(header_obj.tc_field_name, value, header_obj.dataType, '')
    return is_valid, status, value

# === Save Mapped Headers ===
def save_mapped_headers(request):
# save headers mapped by user..Akshay G.
# start process of creating Normal or RFQ campaigns...Date - 12-8-2019
    user_id = request.POST.get('user_id')
    header_data = eval(request.POST.get('data'))
    etl_mapp_obj = Etl_mapp_header.objects.get(user_id = user_id)
    if etl_mapp_obj.rfq_status:
        camp_type = 'rfq'
    else:
        camp_type = 'normal'
    redirect_url = '/client/rfq-campaign/' if etl_mapp_obj.rfq_status else '/client/draft-campaign/'
    # if data is already used to create campaigns
    if etl_mapp_obj.status:
        return JsonResponse({'redirect_url':redirect_url})
    data = eval(etl_mapp_obj.mapped_headers)
    default_headers = []
    if etl_mapp_obj.client_default_mapp:
        default_headers = eval(etl_mapp_obj.client_default_mapp)
    error_data = {}
    error = []
    for dict_ in data:
        for list_ in dict_.values():
            for mapped_data in list_:
                ''' if mapped_data['tc_header'] != '': '''
                tc_header = header_data[mapped_data['user_header']]['tc_header']
                tc_table_name = header_data[mapped_data['user_header']]['tc_table_name']
                if camp_type == 'rfq':
                    dataset_objs = rfq_campaign_dataset.objects.filter(tc_header = tc_header)
                else:
                    dataset_objs = Campaign_dataset.objects.filter(tc_header = tc_header)
                mapped_val = ''
                if dataset_objs:
                    import datetime
                    current_datetime = datetime.datetime.now()
                    if tc_header == 'rfq_timer':
                        if type(mapped_data['values']) == list:
                            rfq_value = str(mapped_data['values'][0])
                        else:
                            rfq_value = str(mapped_data['values'])
                        if rfq_value.strip() == '':
                            hr = '48'
                            updated_datetime = current_datetime + datetime.timedelta(hours = int(hr))
                            mapped_data['values'] = validate_weekends(updated_datetime, updated_datetime)
                        else:
                            hr = re.search(r'\d+', rfq_value).group()
                            updated_datetime = current_datetime + datetime.timedelta(hours = int(hr))
                            day_index = updated_datetime.weekday()
                            # check for weekends
                            if day_index in [5, 6]:
                                if day_index == 5:
                                    updated_datetime = updated_datetime + datetime.timedelta(hours = 48)
                                elif day_index == 6:
                                    updated_datetime = updated_datetime + datetime.timedelta(hours = 24)
                                # calculate rfq timer in hours upto friday from current date
                                # substract calculated hours from given rfq hours and add it to upcoming monday time...Akshay G...Date - 4th Nov, 2019
                                rfq_hours = int(hr) - ((5 - (current_datetime.weekday() + 1))*24 + (24 - current_datetime.hour)) 
                                new_rfq_timer = datetime.datetime(updated_datetime.year, updated_datetime.month, updated_datetime.day)
                                mapped_data['values'] = (new_rfq_timer + datetime.timedelta(hours = int(rfq_hours))).strftime("%Y-%m-%d %H:%M:%S")
                            else:
                                mapped_data['values'] = updated_datetime.strftime("%Y-%m-%d %H:%M:%S")
                    else:
                        is_valid, error, mapped_val = validate_after_manual_mapp(dataset_objs[0], mapped_data['values'])
                        if is_valid == False:
                            # store error data in 'error_data' dict
                            error_data.update(error)
                if mapped_val != '':
                    mapped_data['values'] = mapped_val
                if tc_header == 'type':
                    s = {k for k, choice in CAMPAIGN_TYPES_CHOICES if choice == mapped_data['values']}
                    if len(s) == 0:
                        pass
                    else:
                        mapped_data['values'] = list(s)[0]
                if mapped_data['tc_header'] != tc_header and all(i not in error for i in ['start_date', 'end_date', 'rfq_timer']):
                    if len(default_headers) > 0:
                        flag = False
                        for dict_ in default_headers:
                            if dict_['tc_header'] == mapped_data['tc_header']:
                                if mapped_data['user_header'] in dict_['user_header']:
                                    dict_['user_header'].remove(mapped_data['user_header'])
                            if dict_['tc_header'] == tc_header:
                                flag = True
                                if mapped_data['user_header'] not in dict_['user_header']:
                                    dict_['user_header'].append(mapped_data['user_header'])
                        if flag == False and tc_header != '':
                            default_headers.append({'user_header':[mapped_data['user_header']], 'tc_header': tc_header})
                    elif tc_header != '':
                        default_headers.append({'user_header':[mapped_data['user_header']], 'tc_header': tc_header})
                mapped_data['tc_header'] = tc_header
                mapped_data['tc_field_name'] = tc_header
                mapped_data['tc_table_name'] = tc_table_name
                if mapped_data['tc_header'] == '':
                    if type(mapped_data['values']) == list:
                        mapped_data['values'] = ','.join(mapped_data['values'])
    etl_mapp_obj.mapped_headers = data
    if len(default_headers) > 0:
        etl_mapp_obj.client_default_mapp = default_headers
    etl_mapp_obj.save()
    error_msg = {}
    error_msg_str = ''
    if 'start_date' in error_data:
        error_msg['start_date'] = error_data['start_date']
        error_msg['start_date_val'] = error_data['start_date_val']
    if 'end_date' in error_data:
        error_msg['end_date'] = error_data['end_date']
        error_msg['end_date_val'] = error_data['end_date_val']
    if 'rfq_timer' in error_data:
        error_msg['rfq_timer'] = error_data['rfq_timer']
        error_msg['rfq_timer_val'] = error_data['rfq_timer_val']
    if len(error_msg) > 0:
        return JsonResponse({'status': 'failure', 'error': json.dumps(error_msg), 'redirect_url':redirect_url})
    raw_data_to_mapp(request)
    ''' if data is of RFQ campaigns'''
    if etl_mapp_obj.rfq_status:
        ''' start process of creating campaigns '''
        raw_data_to_create_RFQ_campaign(request)
        ''' if data is of Normal campaigns '''
    else:
        ''' start process of creating campaigns '''
        raw_data_to_create_campaign(request)
    # mark that campaigns are created using data
    etl_mapp_obj = Etl_mapp_header.objects.get(user_id = user_id)
    etl_mapp_obj.status = True
    etl_mapp_obj.save()
    return JsonResponse({'redirect_url':redirect_url, 'total_campaigns_created': len(data), 'rfq_status':etl_mapp_obj.rfq_status, 'status': 'success'})

# === Validate RFQ/Normal csv header's values ===
def csv_validation(header, value1, datatype, original_data):
# validate RFQ/Normal csv header's values..Akshay G...Date - 13-8-2019
    import datetime
    now = datetime.datetime.now()
    value1 = value1 if len(value1) != 0 else ['']
    if header == 'type':
        value1 = [i for i in value1 if i.strip() != '']
        if len(value1) == 0:
            value1 = ['']
        s = {k for k, choice in CAMPAIGN_TYPES_CHOICES if choice == value1[0]}
        if len(s) == 0:
            value = value1
        else:
            value = list(s)
    else:
        value = [i.capitalize() for i in value1 if i.strip() != '']
        if len(value) == 0:
            value = ['']
    # here, 'header' == mapped tc header for user header, 'value1' == header value
    if header == 'name':
        if len(value) == 1 and value[0].strip() == '':
            return False, {header: 'Campaign name is not entered.', header+'_val': 'Expected input e.g. campaign 1, campaign 2, etc.'}, ''
        elif len(original_data.strip()) > 400:
            return False, {header: 'Enter campaign name of maximum 400 characters.', header+'_val': ''}, ''
        else:
            return True, '', original_data
    if header == 'io_number':
        return True, '', value[0] if len(value) > 0 else ''
    # validate company lead limit...Date - 7th Nov, 2019
    if header == 'company_limit':
        value_ = value[0].replace('$','').strip()
        if re.search(r'^\d+$', value_) or value_.strip() == '':
            if re.search(r'^\d+$', value_):
                if eval(value_) == 0:
                    return False, {header: 'Company limit must be greater than 0.', header+'_val': ''}, ''        
            return True, '', value 
        return False, {header: 'Company limit must be an integer number.', header+'_val': ''}, ''
    if header == 'cpl' or header == 'target_quantity' or header == 'custom_question':
        value_ = value[0].replace('.', '').replace('$','').strip()
        if value_.strip() == '':
            return True, '', 0
        if value_.isdigit():
            if header == 'target_quantity':
                if '.' in value[0]:
                    return False, {header: 'Campaign '+ header.replace('_', ' ') +' must be an integer number.', header+'_val':'Expected input e.g. 150, 211, etc.'}, ''
                if eval(value[0].replace('$','')) > 999999999:
                    return False, {header: 'Campaign '+ header.replace('_', ' ') +' must be equal or less than 999,999,999.', header+'_val':'Expected input e.g. 150, 999999999, etc.'}, ''
            elif header == 'cpl':
                if eval(value[0].replace('$','')) > 9999999:
                    return False, {header: 'Campaign '+ header.replace('_', ' ') +' must be equal or less than 9,999,999.', header+'_val':'Expected input e.g. 150.50, 9999999, etc.'}, ''
            else:
                if '.' in value[0]:
                    return False, {header: 'Campaign '+ header.replace('_', ' ') +' must be an integer number.', header+'_val':'Expected input e.g. 4, 6, etc.'}, ''
                if eval(value[0].replace('$','')) > 10:
                    return False, {header: 'Campaign '+ header.replace('_', ' ') +'s must be in the range of 1 to 10.', header+'_val':'Expected input e.g. 5, 7 etc.'}, ''
            return True, '', eval(value[0].replace('$','').strip())
        else:
            return False, {header: 'Campaign '+ header.replace('_', ' ') +' must be a digit.', header+'_val':'Expected input e.g. 7, 10, etc.'}, ''
    if header == 'abm_status' or header == 'suppression_status':
        if value[0].strip() == '':
            return True, '', 0
        if value[0].isdigit():
            if eval(value[0]) > 0:
                return True, '', 1
            else:
                return True, '', 0
        elif value[0].isalpha():
            if value[0].lower() in ['yes', 'y']:
                return True, '', 1
            elif value[0].lower() in ['no', 'n']:
                return True, '', 0
            else:
                return False, {header: 'Invalid '+ header.replace('_', ' ') +' entered.', header+'_val':'Expected input e.g. 1, 0, yes, no, y, n, etc.'}, ''
        else:
            return False, {header: 'Invalid '+ header.replace('_', ' ') +' entered.', header+'_val':'Expected input e.g. 1, 0, yes, no, y, n, etc.'}, ''
    if header == 'revenue_size' or header == 'company_size':
        if len(value) == 1 and value[0].strip() == '':
            return True, '', ''
        for size_ in value:
            if size_.strip() in ['+', '-', '_', '$']:
                return False, {header: 'Invalid campaign '+ header.replace('_', ' ') +' entered.', header+'_val':''}, ''
            for i in ['+', '-', '_']:
                if size_.count(i) > 1:
                    return False, {header: 'Invalid campaign '+ header.replace('_', ' ') +' entered.', header+'_val':''}, ''
            temp_val = size_.replace('-','').replace('_','').replace(' ','').replace(',', '').replace('+', '')
            for i in ['-', '_']:
                if i in size_:
                    # check for '-67', '776-', etc and remove '-', '_', etc..Akshay G..date - 18th Nov, 2019
                    if not all(re.search(r'\d', j) for j in size_.split(i)):
                        value[value.index(size_)] = size_.replace(i, '')
            if header == 'revenue_size':
                temp_val = temp_val.replace('$','')
                if re.search(r'\d', temp_val):
                    # validate if revenue size has standard units..Akshay G..Date - 8th Nov, 2019
                    all_sub_str = re.findall(r'[a-zA-Z]+',temp_val)
                    for i in all_sub_str:
                        if i.lower() not in ['million', 'm', 'mill', 'billion', 'bill', 'b']:
                            return False, {header: 'Invalid '+ header.replace('_', ' ') +' entered.', header+'_val':"Expected currency texts for revenue size are 'm', 'mill', 'million', 'b', 'bill', 'billion'."}, ''
                    if temp_val.isalnum() == False:
                        return False, {header: 'Campaign '+ header.replace('_', ' ') +' can only contain -, _, + and alphanumeric characters.', header+'_val':''}, ''
                    
                    if size_.replace('$','').strip()[0].isdigit() == False:
                        if '$' in size_:
                            return False, {header: '$ must be followed by digits in campaign revenue size.', header+'_val':'Expected input e.g. $ 2b-$ 3b, 15-100, $ 15, etc.'}, ''    
                        return False, {header: 'Campaign '+ header.replace('_', ' ') +' must start from either digit or $.', header+'_val':'Expected input e.g. $ 4 mill-$ 10 mill, 1 billion-3 billion, $ 150, etc.'}, ''
                else:
                    return False, {header: header.replace('_', ' ').capitalize() +' must contain at least one digit.', header+'_val':''}, ''
            else:
                if temp_val.isdigit() == False:
                    return False, {header: 'Campaign '+ header.replace('_', ' ') +' can only contain -, _, + and digits.', header+'_val':''}, ''
        return True, '', value
    if header == 'start_date' or header == 'end_date':
        if value[0] == '':
            return True, '', None
        L1 = []
        list(map(lambda a: L1.extend(a.split('-')) if '-' in a else L1.append(a), value))
        value = L1
        if len(value) == 1:
            value = value[0].split('-') if '-' in value[0] else value[0].split('_')
        if value[0] == '':
            return False, {header: 'Campaign '+ header.replace('_', ' ') +' not entered.', header+'_val':'Expected input e.g. "MM-DD-YYYY", "YYYY-MM-DD" - 2019-03-07, 12/25/2020 etc.'}, ''
        if len(value) in [1, 2]:
            return False, {header: 'Invalid '+ header.replace('_', ' ') +' entered.', header+'_val':'Expected input e.g. "MM-DD-YYYY", "YYYY-MM-DD", 2019-03-07, 12/25/2020 etc.'}, ''
        value[2] = value[2].split(' ')[0]
        if any(x.isdigit() == False for x in value):
            return False, {header: 'Campaign '+ header.replace('_', ' ') +' can only contain -, \, / and integer values.', header+'_val':'Expected input e.g. "MM-DD-YYYY", "YYYY-MM-DD" - 2019-03-07, 12/25/2020 etc.'}, ''
        if len(value[0]) != 4:
            value.reverse()
            if len(value) == 3:
                value[1], value[2] = value[2], value[1]
        if len(value[0]) != 4:
            return False, {header: 'Invalid '+ header.replace('_', ' ') +' entered.', header+'_val':'Expected input e.g. "MM-DD-YYYY", "YYYY-MM-DD", 2019-03-07, 12/25/2020 etc.'}, ''
        if eval(value[0]) < now.year:
            return False, {header: header.replace('_', ' ') +' must be equal or greater than the current year.', header+'_val':'Expected input e.g. "MM-DD-YYYY", "YYYY-MM-DD" - 2019-03-07, 12/25/2020 etc.'}, ''
        for i in value:
            if len(i) not in [2, 4]:
                value[value.index(i)] = '0' + str(i)
        import datetime
        try:
            datetime.datetime.strptime('-'.join(value), '%Y-%m-%d')
        except ValueError:
            return False, {header: 'Invalid '+ header.replace('_', ' ') +' entered.', header+'_val':'Expected input e.g. "MM-DD-YYYY", "YYYY-MM-DD" - 2019-03-07, 12/25/2020 etc.'}, ''
        return True, '', '-'.join(value)
    if header == 'rfq_timer':
        current_datetime = datetime.datetime.now()
        value[0] = value[0].replace(' ','')
        if value[0].strip() == '':
            # updated_datetime = current_datetime + datetime.timedelta(hours = 48)
            # return True, '', validate_weekends(updated_datetime, updated_datetime)
            return True, '', value
        else:
            # Date- 12-10-2019
            # check if it has at least one digit
            if re.search(r'\d', value[0]):
                # first matched digits
                hr = re.search(r'\d+', value[0]).group()
                # all digits in string
                all_digits = re.findall(r'\d+', value[0])
                # check for float value 
                if len(re.findall(r'\d+\.', value[0])) > 0:
                    return False, {header: 'Invalid '+ header.replace('_', ' ') +' entered. Rfq timer must an integer number.', header+'_val':'Expected input e.g. 45 hrs, 60 hours, 70 etc.'}, ''
                # check for condition like..23hr98
                if len(hr) == len(''.join(all_digits)) and value[0].isalnum():    
                    # updated_datetime = current_datetime + datetime.timedelta(hours = int(hr))
                    # return True, '', validate_weekends(updated_datetime, updated_datetime)
                    return True, '', value
                else:
                    return False, {header: 'Invalid '+ header.replace('_', ' ') +' entered. Enter rfq timer in hours.', header+'_val':'Expected input e.g. 45 hrs, 60 hours, 70 etc.'}, ''
            else:
                return False, {header: 'Campaign '+ header.replace('_', ' ') +' must contain at least one digit.', header+'_val':'Expected input e.g. 45 hrs, 60 hours, 70 etc.'}, ''
    if datatype == 'list':
        return True, '',value
    return True, '', value[0] if len(value) != 0 else ''

# check if campaign rfq timer < = current datetime..Akshay G..Date - 9-10-2019
def rfq_timer_validation(rfq_timer):
    import datetime
    datetime_object = Rfq_Timer = datetime.datetime.strptime(rfq_timer, '%Y-%m-%d %H:%M:%S')
    current_datetime = datetime.datetime.now()
    if datetime_object <= current_datetime:
        datetime_object = Rfq_Timer = datetime.datetime.now() + datetime.timedelta(hours = 48)
    else:
        difference =  (datetime_object - (current_datetime + datetime.timedelta(hours = 24))).total_seconds()
        if (difference // 3600) < 5:
            datetime_object = Rfq_Timer = datetime.datetime.now() + datetime.timedelta(hours = 48)
    return validate_weekends(datetime_object, Rfq_Timer)

# === Update Preview HEaders ===
def update_preview_headers(request):
# update campaign specs after editing in preview header page..Akshay G.
    all_camp_data = ast.literal_eval(request.POST.get('all_camp_data'))
    user_id = request.POST.get('user_id')
    custom_headers = ast.literal_eval(request.POST.get('custom_header'))
    user_headers = ast.literal_eval(request.POST.get('user_headers'))
    for custom_header in custom_headers[0].keys():
        user_headers.append(custom_header)
    for i in range(len(custom_headers)):
        all_camp_data[i].extend(custom_headers[i].values())
    data = create_camp_dict(all_camp_data,user_headers, user_id)
    status, header = mapp_csv_header(user_headers, user_id, data)
    if status:
        return JsonResponse({'status':'success'})
    return JsonResponse({'status': 'error', 'header': header})

# === Mapping Headers ===
def mapping_headers(request):
# show mapped headers to client..Akshay G..Date - 11-8-2019
    user_id = request.session['userid']
    etl_mapp_obj = Etl_mapp_header.objects.filter(user_id = user_id)
    d = {}
    rfq_status = False
    if etl_mapp_obj:
        rfq_status = etl_mapp_obj[0].rfq_status
        mapped_list = eval(etl_mapp_obj[0].mapped_headers)
        # if data is already used to create campaigns
        if etl_mapp_obj[0].status:
            if etl_mapp_obj[0].rfq_status:
                return redirect('/client/rfq-campaign/')
            return redirect('/client/draft-campaign/')
    else:
        return redirect('/client/')
    # fetch tc headers for RFQ or Normal camp depending on rfq_status
    tc_header_obj = Tc_header.objects.filter(rfq_status = rfq_status)
    mapp_headers = {}
    mapped_tc_headers = []
    camp_list = []
    for d in mapped_list:
        for i, list_ in d.items():
            user_header_val = {}
            for dict_ in list_:
                import re
                if type(dict_['values']) == str:
                    changed_value = re.sub('[^A-Za-z0-9-_+,.: ]+', '!', dict_['values'])
                    for x in range(len(dict_['values']) - len(changed_value)):
                        changed_value += '!'
                user_header_val[dict_['user_header']] = changed_value if type(dict_['values']) == str else dict_['values']
                if i == 1:
                    mapp_headers[dict_['user_header']] = dict_['tc_header']
                    if dict_['tc_header'] != '':
                        mapped_tc_headers.append(dict_['tc_header'])
            camp_list.append(user_header_val)
    if tc_header_obj:
        tc_header = eval(tc_header_obj[0].tc_headers)
    else:
        tc_header = []
    unmapped_tc_headers = {}
    for i in tc_header:
        if i['tc_header'] not in mapped_tc_headers:
            unmapped_tc_headers.update({i['tc_header']: i['tc_table_name']})
    data  ={
        'camp_type': json.dumps(rfq_status),
        'tc_headers': tc_header,
        'mapp_headers':mapp_headers,
        'user_id':user_id,
        'mapped_tc_headers':mapped_tc_headers,
        'unmapped_tc_headers':unmapped_tc_headers,
        'j_mapped_tc_headers':json.dumps(mapped_tc_headers),
        'j_unmapped_tc_headers':json.dumps(unmapped_tc_headers),
        'user_header_val': json.dumps(user_header_val),
        'camp_list': json.dumps(camp_list),
    }
    return render(request, 'campaign/mapping_header.html', data)

# === Preview Mapped Headers ===
def preview_mapped_headers(request):
# show preview of mapped headers with campaign data ...Akshay G..14-8-2019
    user_id = request.session['userid']
    query = Client_temp_data.objects.filter(user_id = user_id)
    etl_mapp_obj = Etl_mapp_header.objects.get(user_id = user_id)
    mapp_header = {}
    abm_suppr_header = []
    if etl_mapp_obj.mapped_headers:
        mapp_header = eval(etl_mapp_obj.mapped_headers)
        if len(mapp_header) > 0:
            mapp_header = mapp_header[0]
    for l in mapp_header.values():
        for dict_ in l:
            if dict_['tc_header'] in ['abm_status', 'suppression_status']:
                abm_suppr_header.append(dict_['user_header'])
    csv_data = []
    if query:
        csv_data = eval(query[0].data)
    user_headers = []
    camp_data = []
    for i in range(len(csv_data)):
        temp = {}
        for header, value in csv_data[i].items():
            if 'Unnamed:' in header:
                continue
            if i == 0:
                user_headers.append(header)
            # show abm, suppression status as 'yes' or 'no' in preview table..Akshay G..Date - 25th Nov, 2019
            if header in abm_suppr_header:
                if len(value) > 0:
                    if value[0] in ['yes', 'y']:
                        temp[header] = 'yes'    
                    elif value[0] in ['no', 'n']:
                        temp[header] = 'no'
                    elif value[0].isdigit():
                        if eval(value[0]) > 0:
                            temp[header] = 'yes'
                        else:
                            temp[header] = 'no'
                else:
                    temp[header] = 'no'
            else:
                temp[header] = ', '.join(value)
        camp_data.append(temp)
    data = {
        'mapped_user_headers': user_headers,
        'mapped_camp_data': camp_data,
        'user_id':user_id,
    }
    return JsonResponse(data)

# === Upload RFQ Bulk ===
def upload_bulk_rfq(request):
# RFQ campaign csv file upload to create campaigns...Akshay G..Date - 17-8-2019
    user_id = request.session['userid']
    csv_file = request.FILES['upload_rfq_file']
    camp_type = request.POST.get('camp_type')
    if csv_file:
        if csv_file.name.endswith('.csv'):
            try:
                data = pandas.read_csv(csv_file,skip_blank_lines=True,na_filter=False,encoding ='latin1')
            except:
                return JsonResponse({'status':'error_1','title':'Empty file','message': 'Please upload valid file.', 'icon':'error'})
        else:
            try:
                xl = pandas.ExcelFile(csv_file)
                data = pandas.read_excel(xl,xl.sheet_names[0],dtype=str).fillna('')
            except:
                return JsonResponse({'status':'error_1','title':'Empty file','message': 'Please upload valid file.', 'icon':'error'})
        data = data.dropna()
        # file headers
        header = []
        for row in data:
            header.append(row)
        # all campaign data in list of lists
        camp_data = fetch_csv_data(data,header)
        if camp_data:
            # list of campaign data dictionaries
            all_camp_data, camp_name_index, flag = create_camp_dict(camp_data, header, user_id)
            if flag == False:
                return JsonResponse(camp_name_index)
        else:
            return JsonResponse({'status':'error_1', 'title':'No campaign data','message':'Please upload valid file with campaign details.','icon':'error'})
        # map csv file headers into ETL mapp table for client
        header_objects = rfq_campaign_dataset.objects.order_by('tc_header_rank')
        status, error_data = mapp_csv_header(header, user_id, all_camp_data, header_objects, camp_type, camp_data, camp_name_index)
        # check if error found in csv data
        if status == False:
            return JsonResponse({'status':'error', 'error_data': error_data})
        else:
            return JsonResponse({'status':'success'})
    else:
        return JsonResponse({'status':'error_1', 'title':'Invalid file format','message': 'Please upload .csv or .xlsx file.', 'icon':'error'})

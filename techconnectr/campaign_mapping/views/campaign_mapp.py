import ast
import csv
import datetime
import itertools
import json
import operator
import re
from datetime import timedelta
from io import StringIO

import pandas
from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.core.mail import EmailMessage
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.shortcuts import (get_object_or_404, redirect, render,
                              render_to_response)
from django.template import RequestContext
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from vendors.views.views_1 import *
from campaign.models import *
from campaign.views import *
from campaign_mapping.models import *
from client.models import *
from client.views.rfq_views import *
from client_vendor.models import *
from form_builder.models import *
from user.models import *
from vendors.models import *
from client.utils import *

# Abhi 16-aug-2019
# Following function use for seprated data according to tables
# === Raw Data To Mapp ===
def raw_data_to_mapp(request):
    user_id = request.session["userid"]
    mapp_data_list = []
    mapp_data = Etl_mapp_header.objects.get(user_id=user_id)
    mapp_header = ast.literal_eval(mapp_data.mapped_headers)  #raw data that collect from excel.
    result = 0
    for row in mapp_header:
        result += 1
        mapp_data_list.append(create_mapp_data(result, row[result]))
    mapp_data.final_mapp_data = mapp_data_list
    mapp_data.save()

#Abhi 16-aug-2019
#follwing function use for check which data under which table and create dict for that table
# === Create Mapp Data ===
def create_mapp_data(result, mapp_header):
    campaign, specs, mapping, delivery, terms, others, method, RFQ_Campaign, HeaderSpecsValidation = (
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
        {},
    )
    data, others_list = [], []
    for row in mapp_header:
        if row["tc_table_name"] == "Campaign": # if data from  campaign table then create dict. (Dynamically).
            if row["tc_header"] != "method":

                if type(row["values"]) == list:
                    values = ",".join(row["values"])
                else:
                    values = (
                        eval(str(row["values"]))
                        if str(row["values"]).isdigit()
                        else row["values"]
                    )

                curr_data = {row["tc_header"]: values}
                campaign.update(curr_data)
            else:

                curr_data = {
                    row["tc_header"]: list(map(lambda x: x.capitalize(), row["values"]))
                }
                method.update(curr_data)

        elif row["tc_table_name"] == "Mapping": # if data from  mapping table then create dict. (Dynamically).
            if type(row["values"]) == list:
                values = ",".join(row["values"])
            else:
                values = (
                    eval(str(row["values"]))
                    if str(row["values"]).isdigit()
                    else row["values"]
                )

            curr_data = {row["tc_header"]: values}
            mapping.update(curr_data)
        elif row["tc_table_name"] == "Specification": # if data from  Specification table then create dict. (Dynamically).
            if type(row["values"]) == list:
                values = ",".join(row["values"])
            else:
                values = (
                    eval(str(row["values"]))
                    if str(row["values"]).isdigit()
                    else row["values"]
                )

            curr_data = {row["tc_header"]: values}
            specs.update(curr_data)
        elif row["tc_table_name"] == "Delivery": # if data from  Delivery table then create dict. (Dynamically).
            if type(row["values"]) == list:
                values = ",".join(row["values"])
            else:
                values = (
                    eval(str(row["values"]))
                    if str(row["values"]).isdigit()
                    else row["values"]
                )
            curr_data = {row["tc_header"]: values}
            delivery.update(curr_data)
        elif row["tc_table_name"] == "Terms": # if data from  Terms table then create dict. (Dynamically).

            if type(row["values"]) == list:
                values = ",".join(row["values"])
            else:
                values = (
                    eval(str(row["values"]))
                    if str(row["values"]).isdigit()
                    else row["values"]
                )

            curr_data = {row["tc_header"]: values}
            terms.update(curr_data)
        elif row["tc_table_name"] == "RFQ_Campaigns": # if data from  RFQ_Campaigns table then create dict. (Dynamically).
            # print(row['values'])
            if type(row["values"]) == list and row["tc_header"] not in [
                "method",
                "industry_type",
                "company_size",
                "revenue_size",
            ]:
                values = ",".join(row["values"])
            else:
                values = (
                    eval(str(row["values"]))
                    if str(row["values"]).isdigit()
                    else row["values"]
                )
            curr_data = {row["tc_header"]: values}
            RFQ_Campaign.update(curr_data)
        elif row["tc_table_name"] == "HeaderSpecsValidation":
            # create data for company lead limit...Date - 7th Nov, 2019
            if type(row["values"]) == list:
                value = row["values"][0]
            else:
                value = row["values"]
            if value.isdigit():
                a = 4 if eval(value) == 0 else eval(value)
                curr_data = {row["tc_header"]: a}
            else:
                curr_data = {row["tc_header"]: 4}
            HeaderSpecsValidation.update(curr_data)
        else:
            curr_data = {row["user_header"]: row["values"]}
            others.update(curr_data)
    others_list.append(others)
    others = {"others_specs": others_list}
    data.append({"campaign": campaign})
    data.append({"specs": specs})
    data.append({"mapping": mapping})
    data.append({"delivery": delivery})
    data.append({"terms": terms})
    data.append({"method": method})
    data.append({"others_specs": others})
    data.append({"RFQ_Campaign": RFQ_Campaign})
    data.append({"HeaderSpecsValidation": HeaderSpecsValidation})
    return {result: data}

#Abhi 18-aug-2019
#follwing function use for create object of RFQ_CAMPAIGN using mapp dict data
# === Raw Data to Create RFQ Campaign ===
def raw_data_to_create_RFQ_campaign(request):
    # For creating RFQ campaign
    import datetime
    user_id = request.session["userid"]
    data = Etl_mapp_header.objects.get(user_id=user_id)
    data = ast.literal_eval(data.final_mapp_data)
    result = 0
    for row in data:
        result += 1
        row[result][7]["RFQ_Campaign"].update({"user_id": user_id})
        campaign = RFQ_Campaigns.objects.create(**row[result][7]["RFQ_Campaign"])
        suggest_vendors(request,campaign)       
        if campaign.rfq_timer == None: # if user not giving any rfq timer then system take default 48hrs for getting quotes from vendors
            current_datetime = datetime.datetime.now() + datetime.timedelta(hours = 48)
            campaign.rfq_timer = validate_weekends(current_datetime, current_datetime)
            campaign.save()
    data = {"success": 1}
    return JsonResponse(data)

#Abhi 18-aug-2019
#follwing function use for create multiple table object using mapp dict data
# === Raw Data to Create Campaign ===
def raw_data_to_create_campaign(request):
    # For creating Normal campaign
    user_id = request.session["userid"]
    data = Etl_mapp_header.objects.get(user_id=user_id)
    data = ast.literal_eval(data.final_mapp_data)
    result = 0
    for row in data:
        result += 1
        row[result][0]["campaign"].update({"user_id": user_id})
        campaign = Campaign.objects.create(**row[result][0]["campaign"]) # create campaign object
        campaign.raimainingleads = campaign.target_quantity
        campaign.save()
        #update campaign method this way bcoz method is ManyToManyField
        methods = source_touches.objects.filter(
            type__in=row[result][5]["method"]["method"]
        )
        campaign.method.add(*methods)
        campaign.save()
        #update campaign object with related table(Bcoz it is foreign key)
        row[result][1]["specs"].update({"campaign": campaign}) 
        row[result][2]["mapping"].update({"campaign": campaign})
        row[result][3]["delivery"].update({"campaign": campaign})
        row[result][4]["terms"].update({"campaign": campaign})

        row[result][6]["others_specs"].update(
            {"campaign": campaign, "user_id": user_id}
        )
        row[result][8]["HeaderSpecsValidation"].update({"campaign": campaign})
        
        #create other tables objects runtime.
        specification_obj = Specification.objects.create(**row[result][1]["specs"])
        if specification_obj.abm_status:
            create_selected_component_obj('account_based_marketing', campaign)
        if specification_obj.suppression_status:
            create_selected_component_obj('account_supression', campaign)
        if specification_obj.campaign_pacing:
            create_selected_component_obj('campaign_pacing', campaign)
        mapping_obj = Mapping.objects.create(**row[result][2]["mapping"])
        if mapping_obj.custom_question > 0:
            mapping_obj.custom_status = 1
            mapping_obj.save()
            create_selected_component_obj('custom_field', campaign)
        # if mapping_obj.company_size:
        #     create_selected_component_obj('company_size', campaign)
        if mapping_obj.revenue_size:
            create_selected_component_obj('revenue_size', campaign)
        term_obj = Terms.objects.create(**row[result][4]["terms"])
        if term_obj.assets:
            create_selected_component_obj('assets', campaign)
        delivery_obj = Delivery.objects.create(**row[result][3]["delivery"])
        if delivery_obj.campaign_delivery_time:
            create_selected_component_obj('campaign_delivery_time', campaign)
        HeaderSpecsValidation_obj = HeaderSpecsValidation.objects.create(**row[result][8]["HeaderSpecsValidation"])
        if not HeaderSpecsValidation_obj.company_limit:
            HeaderSpecsValidation_obj.company_limit = 4
            HeaderSpecsValidation_obj.save()
        create_selected_component_obj('lead_validation', campaign)
        other_specs = row[result][6]["others_specs"]
        import re
        camp_custom_specs = {}
        # store other specs in client custom specs with given data..Akshay G.
        for spec in other_specs["others_specs"][0]:
            invoke_div = re.sub('[^A-Za-z0-9-_]+', '_', spec).lower()
            query = ComponentsList.objects.filter(invoke_div = invoke_div.lower())
            if len(query) == 0:
                query = ComponentsList.objects.filter(label = spec.title())
            if len(query) == 0:
                specs_data = {"label": spec.capitalize(), "invoke_div": invoke_div}
                add_new_spec(request, specs_data)
                all_data = other_specs["others_specs"][0][spec]
                camp_custom_specs.update({invoke_div: all_data})
                if all_data:
                    if len(all_data.strip()) > 0:
                        for data in re.split(",", all_data):
                            save_custom_specs_data(
                                request, {"div_id": invoke_div, "specs_data": data}
                            )
        Campaign_others_specs.objects.create(user_id = user_id, campaign = campaign, others_specs = [camp_custom_specs])
        component_list = [
            d.function_name
            for d in LeadValidationComponents.objects.filter(is_default=True)
        ]
        SelectedLeadValidation.objects.create(
            campaign=campaign, component_list=component_list
        )
        campaign_json(request,campaign.id)
        from client.views.views import suggest_vendors
        # suggest vendors for this campaign...Akshay G...Date - 20 Nov, 2019
        suggest_vendors(request, campaign.id)
    data = {"success": 1}
    return JsonResponse(data)

def create_selected_component_obj(invoke_div, campaign):
    is_component = ComponentsList.objects.filter(invoke_div = invoke_div)
    if is_component:
        SelectedComponents.objects.create(
            campaign=campaign,
            component=is_component[0],
        )
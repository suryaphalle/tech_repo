# Generated by Django 2.1.4 on 2019-08-12 07:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('campaign_mapping', '0007_merge_20190810_1440'),
    ]

    operations = [
        migrations.AddField(
            model_name='campaign_dataset',
            name='dataType',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
